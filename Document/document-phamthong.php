<?php 
/**
 * @api {POST} api/upload/file Upload file
 * @apiGroup File
 * @apiParam {file} file 
 * @apiParam {string} [folder=null] 
 * @apiParam {string} [mimes=null] Ex: "pdf,jpeg,png,txt"
 * @apiParam {number} [size=null] Ex: 2048 (Unit Kb)
 * @apiParamExample {json} Response Json:
 * {"data":{"path":"\/59d5ffee09267.docx"}}
 */

/**
 * @api {POST} api/guest-payments/info Create info payment
 * @apiGroup Guest Payment
 * @apiParam {string} [course=null] Ex: "Postgraduate Diploma – Organisational Transformation"
 * @apiParam {string} [name=null] 
 * @apiParam {string} [phone=null] 
 * @apiParam {string} [email=null] 
 * @apiParam {string} [address=null] 
 * @apiParam {string} [surname_name=null] 
 * @apiParam {string} [birthday=null] Format: YYYY-MM-DD
 * @apiParam {string} [country=null] 
 * @apiParam {string} [nationality_of=null] 
 * @apiParam {string} [gender=null] 
 * @apiParam {string} [highest_education_level=null] 
 * @apiParam {string} [path_studen_photo=null] 
 * @apiParam {string} [path_transcript=null] 
 * @apiParamExample {json} Response Json:
 * {"data":{"code":"59d6fe47b1922","payment_method":"unknown","base64":"VGVrLGNwLnBoYTFtdDFob25nQGdtYWlsLmNvbSxQb3N0Z3JhZHVhdGUgRGlwbG9tYSDigJMgT3JnYW5pc2F0aW9uYWwgVHJhbnNmb3JtYXRpb24=","link":"http:\/\/lwc-project.local\/#!\/guest-payment\/59d6fe47b1922","phone":"0986999999","email":"cp.pha1mt1hong@gmail.com","course":"Postgraduate Diploma \u2013 Organisational Transformation","name":"Tek","surname_name":"Mr","birthday":"1994-07-32","country":"VietNam","nationality_of":"VietNam","gender":"Male","highest_education_level":"Professor","path_studen_photo":"http:\/\/via.placeholder.com\/350x150","path_transcript":"http:\/\/via.placeholder.com\/350x150","id":14}}
 */

/**
 * @api {POST} api/guest-payments/info/:code Update info payment
 * @apiGroup Guest Payment
 * @apiParam {string} [course=null] Ex: "Postgraduate Diploma – Organisational Transformation"
 * @apiParam {string} [name=null] 
 * @apiParam {string} [phone=null] 
 * @apiParam {string} [email=null] 
 * @apiParam {string} [address=null] 
 * @apiParam {string} [price=null] 
 * @apiParam {string} [surname_name=null] 
 * @apiParam {string} [birthday=null] Format: YYYY-MM-DD
 * @apiParam {string} [country=null] 
 * @apiParam {string} [nationality_of=null] 
 * @apiParam {string} [gender=null] 
 * @apiParam {string} [highest_education_level=null] 
 * @apiParam {string} [path_studen_photo=null] 
 * @apiParam {string} [path_transcript=null] 
 * @apiParamExample {json} Response Json:
 * {"data":{"id":5,"code":"59d6f2c49d79b","link":"http:\/\/lwc-project.local\/#!\/guest-payment\/59d6f2c49d79b","payment_method":"unknown","base64":"VGVrLGNwLnBoMWExbTF0aG9uZ0BnbWFpbC5jb20sUG9zdGdyYWR1YXRlIERpcGxvbWEg4oCTIE9yZ2FuaXNhdGlvbmFsIFRyYW5zZm9ybWF0aW9u","desc":"","name":"Tek","phone":"0986999999","email":"cp.ph1a1m1thong@gmail.com","course":"Postgraduate Diploma \u2013 Organisational Transformation","price":"5000 1","surname_name":"Mr","birthday":"1994-07-32","country":"VietNam","nationality_of":"VietNam","gender":"Male","highest_education_level":"Professor","path_studen_photo":"http:\/\/via.placeholder.com\/350x150","path_transcript":"http:\/\/via.placeholder.com\/350x150","address":"17\/4 BB"}}
 */

/**
 * @api {GET} api/guest-payments/info Get list info payment
 * @apiGroup Guest Payment
 * @apiParam {string} [code] 
 * @apiParam {string} [payment_method] 
 * @apiParam {number} [phone] 
 * @apiParam {string} [email] 
 * @apiParam {string} [course] 
 * @apiParam {string} [price_from] 
 * @apiParam {string} [price_to] 
 * @apiParam {string} [process] Params: 'pending', 'send_mail', 'payment', 'complete'
 * @apiParam {string} [sort='id'] 
 * @apiParam {string} [order='desc'] 
 * @apiParam {number} [take=20] 
 * @apiParamExample {json} Response Json:
 * {"per_page":"2","current_page":1,"next_page_url":"http:\/\/lwc-project.local\/api\/guest-payments\/info?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjgsImlzcyI6Imh0dHA6Ly9sd2MtcHJvamVjdC5sb2NhbC9hcGkvdXNlci9sb2dpbiIsImlhdCI6MTUwODMwNjkwNiwiZXhwIjoxNTA4MzE0MTA2LCJuYmYiOjE1MDgzMDY5MDYsImp0aSI6IjlqbVlVcjVib09lV2YxZFQifQ.H7WwYnaPdLBIWZF-4r3tFukhptDm-jOQ-9lHUX4ZB9Y&take=2&page=2","prev_page_url":null,"from":1,"to":2,"data":[{"id":27,"code":"59db028bbd971","link":"http:\/\/lwc-project.local\/#!\/guest-payment\/59db028bbd971","payment_method":"unknown","base64":"UGjhuqFtIFRow7RuZyxjcC5waGFtdGhvbmdAZ21haWwuY29tLEZvdW5kYXRpb24gRGlwbG9tYSBCdXNpbmVzcyBNYW5hZ2VtZW50ICggTFdDICk=","desc":"","name":"Ph\u1ea1m Th\u00f4ng","phone":"","email":"cp.phamthong@gmail.com","course":"Foundation Diploma Business Management ( LWC )","price":"","surname_name":"111","birthday":"","country":"\u2013 \u2013 \u2013","nationality_of":"\u2013 \u2013 \u2013","gender":"Nam","highest_education_level":"\u2013 \u2013 \u2013","path_studen_photo":"payment\/59db028ba75f4.jpg","path_transcript":"","address":null,"process":"pending"},{"id":26,"code":"59db011ce900b","link":"http:\/\/lwc-project.local\/#!\/guest-payment\/59db011ce900b","payment_method":"unknown","base64":"UGjhuqFtIFRow7RuZyxjcC5waGFtdGhvbmdAZ21haWwuY29tLE1BIE1hcmtldGluZyAmIElubm92YXRpb24gRnVsbCBkZWdyZWUgKEFuZ2xpYSBSdXNraW4gVW5pdmVyc2l0eSk=","desc":"","name":"Ph\u1ea1m Th\u00f4ng","phone":"","email":"cp.phamthong@gmail.com","course":"MA Marketing & Innovation Full degree (Anglia Ruskin University)","price":"","surname_name":"111","birthday":"","country":"\u2013 \u2013 \u2013","nationality_of":"\u2013 \u2013 \u2013","gender":"Nam","highest_education_level":"\u2013 \u2013 \u2013","path_studen_photo":"","path_transcript":"\/59db011cd43fe.png","address":null,"process":"pending"}]}
 */

/**
 * @api {GET} api/guest-payments/info/:code Get info payment
 * @apiGroup Guest Payment
 * @apiParamExample {json} Response Json:
 * {"data":{"id":27,"code":"59db028bbd971","link":"http:\/\/lwc-project.local\/#!\/guest-payment\/59db028bbd971","payment_method":"unknown","base64":"UGjhuqFtIFRow7RuZyxjcC5waGFtdGhvbmdAZ21haWwuY29tLEZvdW5kYXRpb24gRGlwbG9tYSBCdXNpbmVzcyBNYW5hZ2VtZW50ICggTFdDICk=","desc":"","name":"Ph\u1ea1m Th\u00f4ng","phone":"","email":"cp.phamthong@gmail.com","course":"Foundation Diploma Business Management ( LWC )","price":"","surname_name":"111","birthday":"","country":"\u2013 \u2013 \u2013","nationality_of":"\u2013 \u2013 \u2013","gender":"Nam","highest_education_level":"\u2013 \u2013 \u2013","path_studen_photo":"payment\/59db028ba75f4.jpg","path_transcript":"","address":null,"process":"pending"}}
 */

/**
 * @api {POST} api/guest-payments/info/:code Update Payment Method
 * @apiGroup Payment Method
 * @apiParam {number} course_id 
 * @apiParam {string} payment_method
 * @apiSuccess (204) Null
 */

/**
 * @api {POST} api/guest-payments/send-mail/:code Sendmail guest payment
 * @apiGroup Payment Method
 * @apiParam {string} code 
 * @apiSuccess (200) Null
 */

/**
 * @api {POST} api/guest-payments/process Update Process
 * @apiGroup Payment Method
 * @apiParam {string} code
 * @apiParam {string} process=pending Param: 'pending', 'send_mail', 'payment', 'complete'
 * @apiParamExample {json} Response Json:
 * {"data":{"id":27,"code":"59db028bbd971","link":"http:\/\/lwc-project.local\/#!\/guest-payment\/59db028bbd971","payment_method":"unknown","base64":"UGjhuqFtIFRow7RuZyxjcC5waGFtdGhvbmdAZ21haWwuY29tLEZvdW5kYXRpb24gRGlwbG9tYSBCdXNpbmVzcyBNYW5hZ2VtZW50ICggTFdDICk=","desc":"","name":"Ph\u1ea1m Th\u00f4ng","phone":"","email":"cp.phamthong@gmail.com","course":"Foundation Diploma Business Management ( LWC )","price":"","surname_name":"111","birthday":"","country":"\u2013 \u2013 \u2013","nationality_of":"\u2013 \u2013 \u2013","gender":"Nam","highest_education_level":"\u2013 \u2013 \u2013","path_studen_photo":"payment\/59db028ba75f4.jpg","path_transcript":"","address":null,"process":"pending"}}
 * @apiSuccess (200) Null
 */