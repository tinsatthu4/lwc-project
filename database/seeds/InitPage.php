<?php

use Illuminate\Database\Seeder;

class InitPage extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('posts')->truncate();

        $page = [
            'how-to-learn'  => 'How to learn',
            'tourism'       => 'Tourism',
            'business'      => 'Business',
            'accounting'    => 'Accounting',
            'finance'       => 'Finance',
            'hospitality'   => 'Hospitality',
            'marketing'     => 'Marketing',
            'about-us'      => 'About us'
        ];

        foreach($page as $key => $p) {
            \VuleApps\LwcBackends\Models\Page::create([
                'title'         => $p,
                'slug'          => $key,
                'image'         => 'uploads/san-pham/2016/09/14/bg.jpg',
                'type'          => 'page',
                'status'        => 'publish',
            ]);
        }
    }
}
