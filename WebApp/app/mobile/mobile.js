(function (angular) {
    var app = angular.module('mobileLearnerModule', []);
    app.config(function ($stateProvider) {
        $stateProvider.state('app.learner_mobile', {
            url: 'learner/',
            // component: 'courseWork'
            templateUrl: 'app/mobile/layout/master.html',
            controller: function ($scope,$state, $window) {
                $scope.clickUrl = function (q) {
                    // console.log(q);
                    $state.go(q)
                }
                $scope.clickHref = function (q) {
                    // console.log(q);
                    $window.open(q, '_blank');
                }
            }
        });
        $stateProvider.state('app.learner_mobile.profile', {
            url: 'profile/',
            component: 'profile',
        });
        // $stateProvider.state('app.learner_mobile.e_knowledge', {
        //     url: 'e-knowledge/',
        //     component: 'eKnowledge',
        // });
        $stateProvider.state('app.learner_mobile.eknowledge', {
            url: 'eknowledge',
            templateUrl: 'app/mobile/layout/e-knowledge.html',
        });
        $stateProvider.state('app.learner_mobile.eknowledge.courses', {
            url: '/courses',
            template: '<ek-course courses="$resolve.courses" template-url="app/mobile/layout/ek-course.html"/>',
            resolve: {
                courses: function($http, $stateParams, config) {
                    return $http.get(config.api_learner + 'ek/course')
                        .then(function(rs) {
                            var courses = rs.data.data;
                            return courses;
                        });
                }
            }
        });
        $stateProvider.state('app.learner_mobile.eknowledge.module_detail', {
            url: '/module/:module_id',
            template: '<ek-module-detail module="$resolve.module"/>',
            resolve: {
                module: function($http, $stateParams, config, eKnowledgeService) {
                    return eKnowledgeService.getModule($stateParams.module_id)
                      .then(function(res) {
                          return res.data.data;
                      })
                }
            }
        });
        $stateProvider.state('app.learner_mobile.my_course', {
            url: 'my-course/',
            template: '<my-course on-click="onClick($event)"/>',
            controller: function ($scope, $state) {
                $scope.onClick = function (event) {
                    $state.go('app.learner_mobile.course_work', { course_id: event.id })
                }
            }
        });
        $stateProvider.state('app.learner_mobile.course_work', {
            url: 'course-work/',
            // component: 'courseWork'
            template: '<course-work/>',
            // template: '<course-work id="$resolve.resolve_course_id"/>',
            // resolve: {
            //     resolve_course_id: function ($stateParams) {
            //         return $stateParams.course_id
            //     }
            // }
        });
        $stateProvider.state('app.learner_mobile.referal_ink', {
            url: 'referal-link/',
            template: '<referal-link on-click="onClick($event)"/>',
            controller: function ($scope, $state) {
                $scope.onClick = function (event) {
                    $state.go('app.learner_mobile.reference', { course_id: event.id })
                }
            }
        });
        $stateProvider.state('app.learner_mobile.reference', {
            url: 'reference/:course_id',
            // component: 'courseWork'
            template: '<reference-link id="$resolve.resolve_course_id"/>',
            resolve: {
                resolve_course_id: function ($stateParams) {
                    return $stateParams.course_id
                }
            }
        });
    });

    app.component('courseWork', {
        templateUrl: 'app/mobile/layout/course-work.html',
        // bindings: {
        //     id: '<'
        // },
        controller: function (matchKeywordsFilter, vlHandleErrors, notifications, config, $http, $scope, userService, learnerService, $state, $element, $timeout, $window, guestPaymentService, helperFunc, $stateParams, vlNotify) {
            var scope = this;

            // function load() {
            //     learnerService.myModules().then(function (res) {
            //         scope.courses = res.data.data;
            //         angular.forEach(scope.courses, function (obj) {
            //             if (obj.modules[0]) {
            //                 scope.modules = obj.modules;
            //                 console.log(scope.modules)
            //             }
            //         })
            //     });

            // }

            function load() {
                learnerService.getModule().then(function (res) {
                    scope.module = res.data.data;
                })
                learnerService.allTaskByModule().then(function (res) {
                    scope.tasks.none = res.data.data;
                })
            }

            scope.$onInit = function () {
                scope.module = {};
                scope.tasks = {};
                scope.task = {};
                scope.modules = [];
                scope.showCourse = 'false';
                scope.taskFilter = {};
                scope.task_id = '';
                scope.user_task = {};
                scope.module = null;
                scope.comments = [];
                scope.message = null;
                scope.user_task_id = null;
                // load();
            }

            scope.filter = function () {
                $element.find('#button-search').button('loading');
                $http.get(config.api_learner + 'task/' + scope.taskFilter.task_id)
                    .then(function (rs) {
                        scope.task = rs.data.data;
                    });
                $http.get(config.api_learner + 'user-task/' + scope.taskFilter.task_id)
                    .then(function (rs) {
                        var data = rs.data.data;
                        scope.user_task = {
                            answer: data.answer,
                            status: data.status,
                            files: data.files,
                            reference_box: data.reference_box
                        };
                        scope.user_task_id = data.id;
                        reloadComment(scope.user_task_id);
                        scope.showCourse = 'true'
                        $element.find('#button-search').button('reset');
                    });
            }
            function openModal() {
                $element.find('button#button-modal-Common-module-view').trigger('click');
            }
            scope.save = function () {
                $element.find('button.button-submit').button('loading');
                var form = {
                    answer: scope.user_task.answer,
                    reference_box: scope.user_task.reference_box,
                    files: scope.user_task.files
                };

                $http.post(config.api_learner + 'user-task/' + scope.taskFilter.task_id + '/save', form)
                    .then(function (rs) {
                        var res = rs.data.data;
                        scope.user_task.answer = res.answer;
                        notifications.showSuccess({ message: 'Update Task Successfull' });
                    }, vlHandleErrors)
                    .then(function () {
                        $element.find('button.button-submit').button('reset');
                    });
            };
            scope.submission = function () {
                var match = matchKeywordsFilter(angular.copy(scope.user_task.answer), scope.task.keywords);
                if ($window.confirm('There is ' + match + ' keyword match , do you want continue to submit your work.')) {
                    $element.find('button.button-submit').button('loading');
                    var form = {
                        answer: scope.user_task.answer,
                        reference_box: scope.user_task.reference_box,
                        files: scope.user_task.files
                    };
                    $http.post(config.api_learner + 'user-task/' + scope.task.id + '/submission', form)
                        .then(function (rs) {
                            var res = rs.data.data;
                            scope.user_task.status = res.status;
                            scope.user_task.answer = res.answer;
                            notifications.showSuccess({ message: 'Update Task Successfull' });
                        }, vlHandleErrors)
                        .then(function () {
                            $element.find('button.button-submit').button('reset');
                        });

                }
            };
            scope.clickRefer = function (doc) {
                $timeout(function () {
                    openModal();
                }, 100);
                $http.get(config.api_learner + 'module/' + doc.module_id)
                    .then(function (rs) {
                        scope.module = rs.data.data;
                    });
            };
            scope.send = function () {
                $element.find('button.button-send').button('loading');
                userService.postUserTaskComment(scope.user_task_id, scope.message).then(function (res) {
                    notifications.showSuccess({ message: 'Send Message Success' });
                    reloadComment(scope.user_task_id);
                    scope.message = null;
                }, function (res) {
                    notifications.showError({ message: 'Something Went Wrong?' });
                })
                    .then(function () {
                        $element.find('button.button-send').button('reset');
                    })
            };

            function reloadComment(user_task_id) {
                userService.getUserTaskComment(user_task_id).then(function (res) {
                    scope.comments = res.data.data;
                });
            }
            $scope.$watch(
                function () {
                    return scope.taskFilter;
                },
                function (newValue, oldValue) {
                    if (!newValue.task_id || newValue.task_id === 0) {
                        return
                    }
                    scope.filter();
                },
                true);
        }
    })

    app.component('profile', {
        templateUrl: 'app/mobile/layout/profile.html',
        controller: function ($scope, userService, $state, $element, $timeout, $window, guestPaymentService, helperFunc, $stateParams, vlNotify) {
            var scope = this;

            function load() {
                userService.info().then(function (res) {
                    scope.profile = res.data.data;
                    // console.log(scope.profile);
                })
            }
            scope.$onInit = function () {
                scope.profile = {};
                scope.edit = 'false';
                scope.updateProfile = {};
                load();
            };
            scope.$postLink = function () {
                var i = $window.innerHeight;
                angular.element('.main, .col4Fix').css({ height: i });
            }

            scope.update = function () {
                $element.find('#btnUpdate').button('loading');
                scope.edit = 'true'
                $element.find('#btnUpdate').button('reset');
            }
            scope.save = function () {
                $element.find('#btnSave').button('loading');
                userService.profile(scope.updateProfile).then(function (res) {
                    $scope.$emit('message:success', 'Update Profile Successfull');
                    scope.edit = 'false'
                    load();
                    $element.find('#btnSave').button('reset');
                }, function (res) {
                    if (res.status !== 400) return;
                    var messages = res.data.messages;
                    angular.forEach(messages, function (message) {
                        angular.forEach(message, function (mess) {
                            $scope.$emit('message:error', mess);
                            $element.find('#btnSave').button('reset');
                        });
                    });
                });

            }

        }
    })
    app.component('eKnowledge', {
        templateUrl: 'app/mobile/layout/e-knowledge.html',
        controller: function ($scope, userService, $state, $element, $timeout, $window, guestPaymentService, helperFunc, $stateParams, vlNotify) {
            var scope = this;
        }
    })
    app.component('myCourse', {
        templateUrl: 'app/mobile/layout/my-course.html',
        bindings: {
            onClick: '&'
        },
        controller: function ($scope, learnerService, $state, $element, $timeout, $window, guestPaymentService, helperFunc, $stateParams, vlNotify) {
            var scope = this;
            scope.$onInit = function () {
                scope.filter = '';
                learnerService.myModules().then(function (res) {
                    scope.courses = res.data.data;
                    angular.forEach(scope.courses, function (obj) {
                        if (obj.modules[0]) {
                            scope.filter = obj.id;
                        }

                    })
                    // console.log(scope.courses);
                });
            }

            // scope.filterModule = function () {
            //     console.log(scope.filter);
            //     // var b = 7
            //     learnerService.getModule(scope.filter).then(function (rs) {
            //         var a = res.data.data;
            //         console.log(a);
            //     })
            // }

            scope.details = function (course_id) {
                if (scope.onClick) {
                    scope.onClick({
                        $event: {
                            id: course_id
                        }
                    })

                }
            }

        }
    })
    app.component('referalLink', {
        templateUrl: 'app/mobile/layout/referal-link.html',
        bindings: {
            onClick: '&'
        },
        controller: function ($scope, learnerService, $state, $element, $timeout, $window, guestPaymentService, helperFunc, $stateParams, vlNotify) {
            var scope = this;
            scope.$onInit = function () {
                scope.filter = '';
                learnerService.myModules().then(function (res) {
                    scope.courses = res.data.data;
                    angular.forEach(scope.courses, function (obj) {
                        if (obj.modules[0]) {
                            scope.filter = obj.id;
                        }
                    })
                });
            }
            scope.details = function (course_id) {
                if (scope.onClick) {
                    scope.onClick({
                        $event: {
                            id: course_id
                        }
                    })

                }
            }

        }
    })
    app.component('referenceLink', {
        templateUrl: 'app/mobile/layout/reference.html',
        bindings: {
            id: '<'
        },
        controller: function (matchKeywordsFilter, vlHandleErrors, notifications, config, $http, $scope, userService, learnerService, $state, $element, $timeout, $window, guestPaymentService, helperFunc, $stateParams, vlNotify) {
            var scope = this;

            function load() {
                learnerService.getModule(scope.id).then(function (res) {
                    scope.module = res.data.data;
                })
                learnerService.allTaskByModule(scope.id).then(function (res) {
                    scope.tasks.none = res.data.data;
                })
                learnerService.getCourse(scope.id).then(function (rs) {
                    scope.moduleOfTask = rs.data.data;
                })
            }
            scope.$onInit = function () {
                scope.module = {};
                scope.tasks = {};
                scope.task = {};
                scope.showCourse = 'false';
                scope.moduleOfTask = null;
                scope.taskFilter = {
                    moduler_id: scope.id
                };
                scope.user_task = {};
                scope.module = null;
                scope.comments = [];
                scope.message = null;
                scope.user_task_id = null;
                load();
            }

            scope.filter = function () {
                $element.find('#button-search').button('loading');
                $http.get(config.api_learner + 'task/' + scope.taskFilter.task_id)
                    .then(function (rs) {
                        scope.task = rs.data.data;
                    });
                $http.get(config.api_learner + 'user-task/' + scope.taskFilter.task_id)
                    .then(function (rs) {
                        var data = rs.data.data;
                        scope.user_task = {
                            answer: data.answer,
                            status: data.status,
                            files: data.files,
                            reference_box: data.reference_box
                        };
                        scope.user_task_id = data.id;
                        // reloadComment(scope.user_task_id);
                        scope.showCourse = 'true'
                        $element.find('#button-search').button('reset');
                    });
            }
            function openModal() {
                $element.find('button#button-modal-Common-module-view').trigger('click');
            }
            scope.clickRefer = function (doc) {
                $timeout(function () {
                    openModal();
                }, 100);
                $http.get(config.api_learner + 'module/' + doc.module_id)
                    .then(function (rs) {
                        scope.module = rs.data.data;
                    });
            };

            $scope.$watch(
                function () {
                    return scope.taskFilter;
                },
                function (newValue, oldValue) {
                    if (!newValue.task_id || newValue.task_id === 0) {
                        return
                    }
                    scope.filter();
                },
                true);

        }
    })
    app.component('ekModulesByCourseMobile', {
        templateUrl: 'app/learner/theme/ek-modules.html',
        bindings: {
            modules: '<'
        },
        controller: function(eKnowledgeService, $state) {
            var scope = this;
            scope.$onInit = function() {

            }
            scope.viewDetail = function(module) {
                $state.go('app.learner_mobile.eknowledge.module_detail', {
                    'module_id': module.id
                });
            }
        }
    });

})(window.angular);
