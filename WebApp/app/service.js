(function(angular) {
    'use strict';
    angular.module('MyService',['configModule', 'ngStorage'])
        .factory('Auth', function($http, $localStorage, config, userService) {
            function authentication(email, password) {
                return $http.post(config.api + 'user/login', {
                    email: email,
                    password: password
                });
            }

            return {
                authentication: authentication,
                logout: function userLogout() {
                    return $http.get(config.api + 'user/logout')
                },
                register: function(form) {
                    return $http.post(config.api + 'user/register', form).then(function(rs) {

                        $localStorage.token = rs.data.token;
                        $localStorage.user = {};
                        return userService.info().then(function(res) {
                            return res;
                        });
                    });
                },
                login: function(form) {
                    return $http.post(config.api + 'user/login', form);
                }
            }
        })
        .factory('userService', function($http, $localStorage, $q, config) {
            return {
                logout: function userLogout() {
                    return $http.get(config.api + 'user/logout').then(function(res) {
                        delete $localStorage.token;
                        delete $localStorage.user;
                        delete $localStorage.setting;
                        return res;
                    })
                },
                register: function(form) {
                    return $http.post(config.api + 'user/register', form);
                },
                info: function userInfo() {
                    if($localStorage.user && $localStorage.user.info) {
                        return $q(function(resolve) {
                            return resolve({data: {data: $localStorage.user.info}});
                        });
                    }

                    return $http.get(config.api + 'user/info').then(function(rs) {
                        if(!$localStorage.user)
                            $localStorage.user = {};
                        $localStorage.user.info = rs.data.data;
                        return rs;
                    });
                },
                get: function (id) {
                    return $http.get(config.api + 'users/' + id);
                },
                list: function userList() {
                    return $http.get(config.api + 'users');
                },
                create: function userCreate(form) {
                    return $http.post(config.api + 'users', form);
                },
                update: function userUpdate(form, id) {
                    return $http.put(config.api + 'users/' + id, form);
                },
                delete: function userDelete(id) {
                    return $http.delete(config.api + 'users/' + id);
                },
                profile: function userProfile(form) {
                    return $http.put(config.api + 'common/profile', form);
                },
                postUserTaskComment: function postUserTaskComment(user_task_id, message) {
                    var form = {
                        user_task_id: user_task_id,
                        message: message,
                        parent_id: 0
                    };

                    return $http.post(config.api + 'user-task-comment', form);
                },
                getUserTaskComment: function getUserTaskComment(user_task_id) {
                    return $http.get(config.api + 'user-task-comment/by-user-task-id/' + user_task_id);
                }
            }
        })
        .factory('learnerService', function($http, config) {
            return {
                getMetadata: function() {
                    return $http.get(config.api_learner + 'my-metadata');
                },
                getModule: function(id) {
                    return $http.get(config.api_learner + 'module/' + id);
                },
                getCourse: function(id) {
                    return $http.get(config.api_learner + 'post/' + id);
                },
                getCounrse: function(id) {
                    return $http.get(config.api_learner + 'counrse/' + id);
                },
                myModules: function() {
                    return $http.get(config.api_learner + 'my-modules');
                },
                allCourse: function() {
                    return $http.get(config.api_learner + 'all-courses');
                },
                allTaskByModule: function(module_id) {
                    return $http.get(config.api_learner + 'my-modules/'+module_id+'/tasks');
                },
                allTaskByModuleNoAuth: function(module_id) {
                    return $http.get(config.api_learner + 'module/'+module_id+'/task');
                },
                allTaskMeritByModule: function(module_id) {
                    return $http.get(config.api_learner + 'my-modules/'+module_id+'/tasks?type_of_grade=merit');
                },
                allTaskDictionByModule: function(module_id) {
                    return $http.get(config.api_learner + 'my-modules/'+module_id+'/tasks?type_of_grade=diction');
                },
                checkout: function(module_id) {
                    return $http.post(config.api + 'paypal/make', {post_id: module_id});
                },
                checkoutCourse: function(course_id, form) {
                    return $http.post(config.api + 'paypal/make-course', {post_id: course_id, promotion_code: form.promotion_code});
                },
                activites: function() {
                    return $http.get(config.api_learner + 'my-activities', {limit: 10});
                },
                uploadFileFinalModule: function (moduleID, file) {
                    var form = new FormData();

                    form.append("file", file);

                    return $http.post(config.api + 'learner/user-module/file/' + moduleID, form, {
                        transformRequest: angular.identity,
                        headers: { 'Content-Type': undefined }
                    })
                }
            }
        })
        .factory('learnerWorkService', function($http, config) {
            return {
                list: function(page) {
                    page = page || 1;
                    return $http.get(config.api_learner + 'work?page=' + page);
                },
                post: function(form) {
                    return $http.post(config.api_learner + 'work', form);
                },
                get: function(id) {
                    return $http.get(config.api_learner + 'work/' + id);
                },
                put: function(form, id) {
                    return $http.put(config.api_learner + 'work/' + id, form);
                },
                delete: function(id) {
                    return $http.delete(config.api_learner + 'work/' + id);
                }
            };
        })
        .factory('teacherService', function($http, config) {
            return {
                listTaskByModule: function listTaskByModule(module_id, query) {
                    if(query)
                        return $http.get(config.api_teacher + 'module/' + module_id + '/task?' +  query);
                    return $http.get(config.api_teacher + 'module/' + module_id + '/task');
                }
            };
        })
        .factory('TeacherApi', function($q, $http, $localStorage, config) {
            function getModules(page) {
                return $http.get(config.api_teacher + 'module?page=' + page);
            }

            function getModule(id) {
                return $http.get(config.api_teacher + 'module/' + id);
            }

            function getTask(module_id, id) {
                return $http.get(config.api_teacher + 'module/' + module_id + '/task/' + id);
            }

            function getTeacherUserTaskGroup() {
                return $http.get(config.api_teacher + 'user-task/group');
            }

            return {
                getModules: getModules,
                getModule: getModule,
                getTask: getTask,
                deleteTask: function(module_id, id) {
                    return $http.delete(config.api_teacher + 'module/' + module_id + '/task/' + id);
                },
                getTeacherUserTaskGroup: getTeacherUserTaskGroup,
                getCourseList: function() {
                    return $http.get(config.api_teacher + 'counrse?limit=50');
                },
                getCourse: function(id) {
                    return $http.get(config.api_teacher + 'post/' + id);
                },
                updateCourse: function(id, form) {
                    return $http.put(config.api_teacher + 'counrse/' + id, form);
                },
                getMasterFileList: function() {
                    return $http.get(config.api_teacher + 'master-file/list');
                },
                getLo: function(id) {
                    return $http.get(config.api_teacher + 'lo/' + id);
                },
                listLo: function(module_id) {
                    return $http.get(config.api_teacher + 'lo?module_id=' + module_id);
                },
                createLo: function(form) {
                    return $http.post(config.api_teacher + 'lo', form);
                },
                updateLo: function(form, id) {
                    return $http.put(config.api_teacher + 'lo/' + id, form);
                },
                deleteLo: function(id) {
                    return $http.delete(config.api_teacher + 'lo/' + id);
                },
                listFinalAssignmentSubmission: function (counrseID) {
                    return $http.get(config.api_teacher + 'user-module/final-assignment-submission/counrse/' + counrseID);
                },
                getUrlDownloadFinalAssignmentSubmission: function (userModuleID) {
                    return config.api_teacher + 'user-module/final-assignment-submission/' + userModuleID + '/download?token=' + $localStorage.token;
                }
            }
        })
        .factory('adminService', function($http, config) {
            return {
                listCourses: function listCourses(page) {
                    return $http.get(config.api_admin + 'counrse?limit=50&page=' + page);
                },
                listCoursesV2: function listCourses(q) {
                    q = q || null;
                    return $http.get(config.api_admin + 'counrse?' + q);
                },
                getCourse: function getCourse(id) {
                    return $http.get(config.api_admin + 'counrse/' + id);
                },
                createCourse: function(form) {
                    return $http.post(config.api_admin + 'counrse', form);
                },
                updateCourse: function(id, form) {
                    return $http.put(config.api_admin + 'counrse/' + id, form);
                },
                getListModulesCourse: function getListModulesCourse(course_id) {
                    return $http.get(config.api_admin + 'module?parent_id='+ course_id + '&limit=100&with-tasks-count=1&page=1');
                },
                getModule: function getModule(id) {
                    return $http.get(config.api_admin + 'module/' + id);
                },
                getAllTeachers: function getAllTeachers() {
                    return $http.get(config.api_admin + 'user/get-teachers');
                },
                listUsers: function listUsers(page) {
                    return $http.get(config.api_admin + 'user?limit=50&page=' + page);
                },
                createUser: function(form) {
                    return $http.post(config.api_admin + 'user', form);
                },
                updateUser: function(id, form) {
                    return $http.put(config.api_admin + 'user/' + id, form);
                },
                getUser: function getUser(id) {
                    return $http.get(config.api_admin + 'user/' + id);
                },
                deleteUser: function (id) {
                    return $http.delete(config.api_admin + 'user/' + id);
                },
                getCoursesByLearner: function getCoursesByLearner(id) {
                    return $http.get(config.api_admin + 'learner/'+ id +'/get-course-by-learner');
                },
                getCoursesByLearnerV2: function getCoursesByLearnerV2(id, q) {
                    q = q || null;
                    return $http.get(config.api_admin + 'learner/'+ id +'/get-course-by-learner?' + q);
                },
                listCheckouts: function listCheckouts(page, search) {
                    return $http.get(config.api_admin + 'checkout?limit=10&page=' + page + '&' + search);
                },
                getCheckout: function getCheckout(id) {
                    return $http.get(config.api_admin + 'checkout/' + id);
                },
                listInfoPayment: function(q) {
                    q = q || null;
                    return $http.get(config.api + 'guest-payments/info?' + q);
                },
                detailsPayment: function(code) {
                    return $http.get(config.api + 'guest-payments/info/'+code);
                },
                updatePayment: function(code, form) {
                    return $http.post(config.api + 'guest-payments/info/'+code, form);
                },
                sentEmailPayment: function(code) {
                    return $http.post(config.api + 'guest-payments/send-mail/'+code);
                },
                updateProcessPayment: function(form) {
                    return $http.post(config.api + 'guest-payments/process', form);
                },
                listEkCourses: function listEkCourses(page) {
                    return $http.get(config.api_admin + 'ek-course?limit=50&page=' + page);
                }
            };
        })
        .factory('notifications', function($rootScope) {
            return {
                showError: function(message) {
                    if(typeof message == 'string')
                        $rootScope.$emit('message:error', message);
                    else
                        if(message.message)
                            $rootScope.$emit('message:error', message.message);

                },
                showInfo: function(message) {
                    if(typeof message == 'string')
                        $rootScope.$emit('message:info', message);
                    else
                    if(message.message)
                        $rootScope.$emit('message:info', message.message);

                },
                showWarning: function(message) {
                    if(typeof message == 'string')
                        $rootScope.$emit('message:warning', message);
                    else
                    if(message.message)
                        $rootScope.$emit('message:warning', message.message);
                },
                showSuccess: function(message) {
                    if(typeof message == 'string')
                        $rootScope.$emit('message:success', message);
                    else
                    if(message.message)
                        $rootScope.$emit('message:success', message.message);
                },
                closeAll: function(message) {}
            }
        })
        .factory('guestPaymentService', function ($http, config) {
            return {
                getCode: function(code) {
                    return $http.get(config.api + 'guest/' + code);
                },
                choosePayment: function(code, form) {
                    return $http.post(config.api + 'guest/choose-payment/' + code, form);
                },
                generate: function(form) {
                    return $http.post(config.api + 'guest/generate', form);
                },
                generatePaypalUrl: function(code) {
                    return $http.post(config.api + 'guest/paypal/' + code);
                }
            }
        })
        .factory('eKnowledgeService', function($http, config) {
            return {
                getCourses: function(q) {
                    if(typeof q == undefined) {
                        q = '';
                    }
                    return $http.get(config.api_learner + 'ek/course?' + q);
                },
                getModulesByCourse: function(course_id, q) {
                    if(typeof q == undefined) {
                        q = '';
                    }
                    return $http.get(config.api_learner + 'ek/course/'+ course_id +'/module?' + q);
                },
                getLessonsByModule: function(module_id, q) {
                    if(typeof q == undefined) {
                        q = '';
                    }
                    return $http.get(config.api_learner + 'ek/module/'+ module_id +'/lesson?' + q);
                },
                getSectionsByLesson: function(lesson_id, q) {
                    if(typeof q == undefined) {
                        q = '';
                    }
                    return $http.get(config.api_learner + 'ek/lesson/'+ lesson_id +'/session?' + q);
                },
                getModule: function(module_id) {
                    return $http.get(config.api_learner + 'ek/module/'+ module_id);
                }
            }
        })
        .factory('adminEKnowledgeService', function($http, config) {
            return {
                getCourses: function(q) {
                    if(typeof q == undefined) {
                        q = '';
                    }
                    return $http.get(config.api_admin + 'ek/course?' + q);
                },
                addCourse: function(data) {
                    return $http.post(config.api_admin + 'ek/course', data);
                },
                editCourse: function(id, data) {
                    return $http.put(config.api_admin + 'ek/course/' + id, data);
                },
                showCourse: function(id) {
                    return $http.get(config.api_admin + 'ek/course/' + id);
                },
                deleteCourse: function(id) {
                    return $http.delete(config.api_admin + 'ek/course/' + id);
                },
                getModulesByCourse: function(course_id, q) {
                    if(typeof q == undefined) {
                        q = '';
                    }
                    return $http.get(config.api_admin + 'ek/course/'+ course_id +'/module?' + q);
                },
                addModuleByCourse: function(course_id, data) {
                    return $http.post(config.api_admin + 'ek/course/'+ course_id +'/module', data);
                },
                editModuleByCourse: function(course_id, module_id, data) {
                    return $http.put(config.api_admin + 'ek/course/'+ course_id +'/module/' + module_id, data);
                },
                deleteModuleByCourse: function(course_id, module_id) {
                    return $http.delete(config.api_admin + 'ek/course/'+ course_id +'/module/' + module_id);
                },
                getLessonsByModule: function(module_id, q) {
                    if(typeof q == undefined) {
                        q = '';
                    }
                    return $http.get(config.api_admin + 'ek/module/'+ module_id +'/lesson?' + q);
                },
                addLessonByModule: function(module_id, data) {
                    return $http.post(config.api_admin + 'ek/module/'+ module_id +'/lesson', data);
                },
                deleteLessonByModule: function(module_id, id) {
                    return $http.delete(config.api_admin + 'ek/module/'+ module_id +'/lesson/' + id);
                },
                editLessonByModule: function(module_id, id, data) {
                    return $http.put(config.api_admin + 'ek/module/'+ module_id +'/lesson/'+ id, data);
                },
                getSectionsByLesson: function(lesson_id, q) {
                    if(typeof q == undefined) {
                        q = '';
                    }
                    return $http.get(config.api_admin + 'ek/lesson/'+ lesson_id +'/session?' + q);
                },
                addSectionByLesson: function(lesson_id, data) {
                    return $http.post(config.api_admin + 'ek/lesson/'+ lesson_id +'/session', data);
                },
                deleteSectionByLesson: function(lesson_id, id) {
                    return $http.delete(config.api_admin + 'ek/lesson/'+ lesson_id +'/session/' + id);
                },
                editSectionByLesson: function(lesson_id, id, data) {
                    return $http.put(config.api_admin + 'ek/lesson/'+ lesson_id +'/session/'+ id, data);
                },
                getModule: function(module_id) {
                    return $http.get(config.api_admin + 'ek/module/'+ module_id);
                },
                updateModule: function(module_id, data) {
                    return $http.put(config.api_admin + 'ek/module/'+ module_id, data);
                }
            }
        })
        .factory('commonService', function ($http, $localStorage, $interval, config) {
            return {
                getUrl: function (url) {
                    return $http.get(url);
                }
            }
        });
}) (window.angular);
