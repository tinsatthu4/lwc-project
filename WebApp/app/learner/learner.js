(function(angular) {
    var app = angular.module('learnerModule', []);
    app.config(function($stateProvider) {
        $stateProvider.state('app.guest_payment', {
            url: 'guest-payment/:code?paypal&message',
            template: '<guest-payment code="$resolve.code"/>',
            resolve: {
                code: function ($stateParams) {
                    return $stateParams.code;
                }
            }
        });
        $stateProvider.state('app.payment_success', {
            url: 'payment-success/:payment_method',
            template: '<payment-success method="$resolve.resolve_payments_method"/>',
            resolve:{
                resolve_payments_method: function ($stateParams) {
                    return $stateParams.payment_method
                }
            }
        });

        $stateProvider.state('app.generate_guest_payment', {
            url: 'generate-guest-payment',
            component: 'generateGuestPayment'
        });
        $stateProvider.state('app.learner_payment', {
            url: 'enroll-course/:course_id',
            component: 'enrollCourse'
        });
        $stateProvider.state('app.learner_dashboard', {
            url: 'learner/dashboard',
            component: 'learnerDashboard'
        });
        $stateProvider.state('app.learner_dashboard2', {
            url: 'learner/dashboard2',
            component: 'learnerDashboard2'
        });
        $stateProvider.state('app.learner', {
            url: 'learner',
            templateUrl: 'app/learner/theme/learner-layout.html',
            controller: function($scope, $state, $stateParams) {
                if (!$stateParams.course_id)
                    $state.go('app.learner_dashboard');
            }
        });
        $stateProvider.state('app.learnerNoneCourse', {
            url: 'learner-module',
            templateUrl: 'app/learner/theme/learner-none-course-layout.html',
            controller: function($scope, $state, $stateParams) {
                angular.element('body').addClass('skin-blue sidebar-collapse');
            }
        });
        $stateProvider.state('app.learnerNoneCourse.course_detail', {
            url: '/course/:course_id',
            template: '<common-course-view course="$resolve.course" template="course_more_detail.html" typeuser="learner"/>',
            resolve: {
                course: function(learnerService, $stateParams) {
                    return learnerService.getCounrse($stateParams.course_id).then(function(res) {
                        return res.data.data;
                    })
                }
            }
        });
        $stateProvider.state('app.learnerNoneCourse.allcourses', {
            url: '/all-courses',
            component: 'allCourses'
        });
        $stateProvider.state('app.learnerNoneCourse.myModules', {
            url: '/my-modules?payment',
            component: 'myModules'
        });
        $stateProvider.state('app.learnerNoneCourse.profile', {
            url: '/profile',
            component: 'learnerProfile',
            resolve: {
                user: function(userService) {
                    return userService.info().then(function(res) {
                        return res.data.data;
                    })
                }
            }
        });
        $stateProvider.state('app.learner.calendar', {
            url: '/calendar/:course_id',
            component: 'learnerTodolist'
        });
        $stateProvider.state('app.learner.course', {
            url: '/course/:course_id',
            //template: '<common-course-info course="$resolve.course"/>',
            component: 'learnerCourse',
            resolve: {
                course: function($http, $stateParams, config, learnerCache) {
                    if (angular.isUndefined(learnerCache.learnerSidebarCourses[$stateParams.course_id])) {
                        return $http.get(config.api_learner + 'post/' + $stateParams.course_id)
                            .then(function(rs) {
                                learnerCache.learnerSidebarCourses[$stateParams.course_id] = rs.data.data;
                                return rs.data.data;
                            });
                    } else {
                        return learnerCache.learnerSidebarCourses[$stateParams.course_id];
                    }
                }
            }
        });
        $stateProvider.state('app.learner.user_task_assigment', {
            url: '/course/:course_id/task/:id',
            template: '<user-task-assigment task="$resolve.task"/>',
            resolve: {
                task: function($http, $stateParams, arrayToTextFilter, config) {
                    return $http.get(config.api_learner + 'task/' + $stateParams.id)
                        .then(function(rs) {
                            var task = rs.data.data;
                            task.preference_text = arrayToTextFilter(task.preference);
                            return task;
                        });
                }
            }
        });
        $stateProvider.state('app.learner.user_task_mcq', {
            url: '/course/:course_id/task-mcq/:id',
            template: '<user-task-mcq task="$resolve.userTask.task" user-task="$resolve.userTask"/>',
            resolve: {
                userTask: function($http, $stateParams, config) {
                    return $http.get(config.api_learner + 'user-task/' + $stateParams.id + '?with-task=1')
                        .then(function(rs) {
                            return rs.data.data;
                        });
                }
            }
        });
        $stateProvider.state('app.learner.user_module', {
            url: '/course/:course_id/module/:id',
            template: '<common-module-view module="$resolve.module" />',
            controller: function() {},
            resolve: {
                module: function($http, $stateParams, config) {
                    return $http.get(config.api_learner + 'post/' + $stateParams.id)
                        .then(function(rs) {
                            var module = rs.data.data;
                            return module;
                        });
                }
            }
        });

        //New Module
        $stateProvider.state('app.learner_module', {
            url: 'module/:module_id',
            templateUrl: 'app/learner/theme/module-layout.html',
            resolve: {
                module: function($stateParams, learnerService) {
                    return learnerService.getModule($stateParams.module_id).then(function(res) {
                        return res.data.data;
                    });
                },
                // tasks: function($stateParams, learnerService) {
                //     return learnerService.allTaskByModule($stateParams.module_id).then(function(res) {
                //         return res.data.data;
                //     });
                // },
                // meritTasks: function($stateParams, learnerService) {
                //     return learnerService.allTaskMeritByModule($stateParams.module_id).then(function(res) {
                //         return res.data.data;
                //     });
                // },
                // meritTasks: function($stateParams, learnerService) {
                //     return learnerService.allTaskDictionByModule($stateParams.module_id).then(function(res) {
                //         return res.data.data;
                //     });
                // }

            },
            controller: function(learnerService) {
                angular.element('body').addClass('skin-blue sidebar-collapse');
                var scope = this;
                // scope.$onInit = function() {
                //     learnerService.allTaskByModule(scope.module.id).then(function(res) {
                //         scope.tasks = res.data.data;
                //     });
                //     learnerService.allTaskMeritByModule(scope.module.id).then(function(res) {
                //         scope.meritTasks = res.data.data;
                //     });
                //     learnerService.allTaskDictionByModule(scope.module.id).then(function(res) {
                //         scope.dictionTasks = res.data.data;
                //     });
                // }
            },
        });
        $stateProvider.state('app.learner_module.dashboard', {
            url: '/dashboard',
            templateUrl: 'app/learner/theme/module-dashboard.html'
        });
        $stateProvider.state('app.learner_module_task', {
            url: 'module/:module_id/task/:id',
            templateUrl: 'app/learner/theme/module-task-layout.html',
            controller: function() {
                angular.element('body').addClass('skin-blue sidebar-collapse');
            },
            resolve: {
                module: function($http, $stateParams, config) {
                    return $http.get(config.api_learner + 'post/' + $stateParams.module_id)
                        .then(function(rs) {
                            var module = rs.data.data;
                            return module;
                        });
                },
                task: function($http, $stateParams, arrayToTextFilter, config) {
                    return $http.get(config.api_learner + 'task/' + $stateParams.id)
                        .then(function(rs) {
                            var task = rs.data.data;
                            task.preference_text = arrayToTextFilter(task.preference);
                            return task;
                        });
                }
            }
        });
        $stateProvider.state('app.learner_module_task.task_assigment', {
            url: '/assigment',
            template: '<user-task-assigment task="$resolve.task" module-of-task="$resolve.module"/>'
        });
        $stateProvider.state('app.learner_module_task.task_mcq', {
            url: '/mcq',
            template: '<user-task-mcq task="$resolve.userTask.task" user-task="$resolve.userTask"/>',
            resolve: {
                userTask: function($http, $stateParams, config) {
                    return $http.get(config.api_learner + 'user-task/' + $stateParams.id + '?with-task=1')
                        .then(function(rs) {
                            return rs.data.data;
                        });
                }
            }
        });
        $stateProvider.state('app.eknowledge', {
            url: 'eknowledge',
            templateUrl: 'app/learner/theme/e-knowledge.html',
        });
        $stateProvider.state('app.eknowledge.courses', {
            url: '/courses',
            template: '<ek-course courses="$resolve.courses"/>',
            resolve: {
                courses: function($http, $stateParams, config) {
                    return $http.get(config.api_learner + 'ek/course')
                        .then(function(rs) {
                            var courses = rs.data.data;
                            return courses;
                        });
                }
            }
        });
        $stateProvider.state('app.eknowledge.module_detail', {
            url: '/module/:module_id',
            template: '<ek-module-detail module="$resolve.module"/>',
            resolve: {
                module: function($http, $stateParams, config, eKnowledgeService) {
                    return eKnowledgeService.getModule($stateParams.module_id)
                      .then(function(res) {
                          return res.data.data;
                      })
                }
            }
        });
    });
    app.constant('learnerCache', {
        learnerSidebarModules: {},
        learnerSidebarCourses: {}
    });
    app.component('sidebarUsers', {
        bindings: {
            counrseId: '<'
        },
        templateUrl: 'app/learner/theme/sidebar-user-module-task.html',
        controller: function sidebarUsersController($element, $http, config) {
            var scope = this;
            scope.$onInit = function() {
                $http.get(config.api_learner + 'user-task/modules-and-tasks/' + scope.counrseId)
                    .then(function(rs) {
                        var data = rs.data.data;
                        scope.tasks = data.tasks;
                        scope.modules = data.modules;
                    });
            };
            $element.css('display', 'block');
            $element.slimScroll({
                position: 'right',
                height: '700px',
                railVisible: true,
                //alwaysVisible: true,
                color: '#00f'
            });
        }
    });
    app.component('userTaskAssigment', {
        bindings: {
            task: '<',
            moduleOfTask: '<'
        },
        templateUrl: 'app/learner/theme/user-task-assigment-form.html',
        controller: function($element, $timeout, $window, $http, matchKeywordsFilter, userService, config, notifications, vlHandleErrors) {
            var scope = this;

            function openModal() {
                $element.find('button#button-modal-Common-module-view').trigger('click');
            }

            scope.$onInit = function() {
                scope.module = null;
                scope.comments = [];
                scope.message = null;
                scope.user_task_id = null;
                $http.get(config.api_learner + 'user-task/' + scope.task.id)
                    .then(function(rs) {
                        var data = rs.data.data;
                        scope.user_task = {
                            answer: data.answer,
                            status: data.status,
                            files: data.files,
                            reference_box: data.reference_box
                        };
                        scope.user_task_id = data.id;
                        reloadComment(scope.user_task_id);
                    });
                angular.element('h1#page-title').html(scope.task.title + ' <small>Assigment Task</small>');
            };
            scope.save = function() {
                $element.find('button.button-submit').button('loading');
                var form = {
                    answer: scope.user_task.answer,
                    reference_box: scope.user_task.reference_box,
                    files: scope.user_task.files
                };

                $http.post(config.api_learner + 'user-task/' + scope.task.id + '/save', form)
                    .then(function(rs) {
                        var res = rs.data.data;
                        scope.user_task.answer = res.answer;
                        notifications.showSuccess({ message: 'Update Task Successfull' });
                    }, vlHandleErrors)
                    .then(function() {
                        $element.find('button.button-submit').button('reset');
                    });
            };
            scope.submission = function() {
                // var match = matchKeywordsFilter(angular.copy(scope.user_task.answer), scope.task.keywords);
                if(scope.user_task.files.length < 1 || scope.user_task.files == undefined){
                    notifications.showError({ message: 'No file upload' });
                }else{
                    $element.find('button.button-submit').button('loading');
                    var form = {
                        answer: scope.user_task.answer,
                        reference_box: scope.user_task.reference_box,
                        files: scope.user_task.files
                    };
                    $http.post(config.api_learner + 'user-task/' + scope.task.id + '/submission', form)
                        .then(function(rs) {
                            var res = rs.data.data;
                            scope.user_task.status = res.status;
                            scope.user_task.answer = res.answer;
                            notifications.showSuccess({ message: 'Update Task Successfull' });
                        }, vlHandleErrors)
                        .then(function() {
                            $element.find('button.button-submit').button('reset');
                        });
                }
                // if ($window.confirm('There is ' + match + ' keyword match , do you want continue to submit your work.')) {
                    // $element.find('button.button-submit').button('loading');
                    // var form = {
                    //     answer: scope.user_task.answer,
                    //     reference_box: scope.user_task.reference_box,
                    //     files: scope.user_task.files
                    // };
                    // $http.post(config.api_learner + 'user-task/' + scope.task.id + '/submission', form)
                    //     .then(function(rs) {
                    //         var res = rs.data.data;
                    //         scope.user_task.status = res.status;
                    //         scope.user_task.answer = res.answer;
                    //         notifications.showSuccess({ message: 'Update Task Successfull' });
                    //     }, vlHandleErrors)
                    //     .then(function() {
                    //         $element.find('button.button-submit').button('reset');
                    //     });

                // }
            };
            scope.clickRefer = function(doc) {
                $timeout(function() {
                    openModal();
                }, 100);
                $http.get(config.api_learner + 'module/' + doc.module_id)
                    .then(function(rs) {
                        scope.module = rs.data.data;
                    });
            };
            scope.send = function() {
                $element.find('button.button-send').button('loading');
                userService.postUserTaskComment(scope.user_task_id, scope.message).then(function(res) {
                        notifications.showSuccess({ message: 'Send Message Success' });
                        reloadComment(scope.user_task_id);
                        scope.message = null;
                    }, function(res) {
                        notifications.showError({ message: 'Something Went Wrong?' });
                    })
                    .then(function() {
                        $element.find('button.button-send').button('reset');
                    })
            };

            function reloadComment(user_task_id) {
                userService.getUserTaskComment(user_task_id).then(function(res) {
                    scope.comments = res.data.data;
                });
            }
        }
    });
    app.component('userModule', {
        bindings: { module: '<' },
        templateUrl: 'app/learner/theme/user-module.html',
        controller: function(parseMetadataFilter) {
            var scope = this;
            scope.$onInit = function() {
                // console.log(scope.module);
                var postmeta = parseMetadataFilter(scope.module.postmeta || null);
                if (postmeta.document)
                    scope.docs = JSON.parse(postmeta.document);
                else
                    scope.docs = [];
            }
        }
    });
    app.component('userTaskMcq', {
        templateUrl: 'app/learner/theme/user-task-mcq.html',
        bindings: {
            task: '<',
            userTask: '<'
        },
        controller: function($http, $window, $timeout, $element, config, notifications, vlNotify) {
            var scope = this;

            function loadMcqs() {
                $http.get(config.api_learner + 'mcq/' + scope.userTask.task_id)
                    .then(function(rs) {
                        scope.mcqs = rs.data.data;
                    });
                scope.answers = {};
            }

            function openModal() {
                $element.find('button#button-modal-Common-module-view').trigger('click');
            }

            scope.$onInit = function() {
                loadMcqs();
                angular.element('h1#page-title').html(scope.task.title + ' <small>Multichoice Question Task</small>');
            };

            scope.submission = function() {
                $http.post(config.api_learner + 'mcq-submission/' + scope.task.id, { answer: scope.answers })
                    .then(function(rs) {
                        notifications.showSuccess({ message: 'Submission Successfull!' });
                    }, function(rs) {
                        if (rs.data.messages) {
                            var message = rs.data.messages;
                            if (message.answer)
                                notifications.showError({ message: 'Your Answer is required' });
                        }
                        if (rs.data.error) {
                            notifications.showError({ message: 'Your Answer does not match' });
                        }
                    });
            };

            scope.check = function() {
                $http.post(config.api_learner + 'mcq-check/' + scope.task.id, { answer: scope.answers })
                    .then(function(rs) {
                        var data = rs.data.data;
                        $window.alert('Total Match : ' + data.match);
                    }, function(rs) {
                        if (rs.data.messages) {
                            var message = rs.data.messages;
                            if (message.answer)
                                notifications.showError({ message: 'Your Answer is required' });
                        }
                        if (rs.data.error) {
                            notifications.showError({ message: 'Your Answer does not match' });
                        }
                    })
            };
            scope.recheck = function() {
                scope.answers = {};
            };
            scope.clickRefer = function(doc) {
                $http.get(config.api_learner + 'module/' + doc.module_id)
                    .then(function(rs) {
                        scope.module = rs.data.data;
                    });
                $timeout(function() {
                    openModal();
                }, 100);
            };
        }
    });
    app.component('learnerNav', {
        templateUrl: 'app/learner/theme/learner-nav.html',
        controller: function(userService) {
            var scope = this;
            scope.$onInit = function() {
                userService.info().then(function(rs) {
                    scope.user = rs.data.data;
                });
            }
        }
    });
    app.component('learnerSidebar', {
        templateUrl: 'app/learner/theme/learner-sidebar.html',
        controller: function sidebarUsersController($timeout, $element, $stateParams, $http, config, learnerCache, userService) {
            var scope = this;

            function loadModuleAndTask() {
                if (angular.isUndefined(learnerCache.learnerSidebarModules[$stateParams.course_id]))
                    $http.get(config.api_learner + 'user-task/modules-and-tasks/' + $stateParams.course_id)
                    .then(function(rs) {
                        var data = rs.data.data;
                        learnerCache.learnerSidebarModules[$stateParams.course_id] = data;
                        scope.tasks = angular.copy(data.tasks);
                        scope.modules = angular.copy(data.modules);
                    });
                else {
                    scope.tasks = angular.copy(learnerCache.learnerSidebarModules[$stateParams.course_id].tasks);
                    scope.modules = angular.copy(learnerCache.learnerSidebarModules[$stateParams.course_id].modules);
                }
            }

            function loadUserInfo() {
                userService.info().then(function(res) {
                    scope.user = res.data.data;
                })
            }

            function loadCourse() {
                if (angular.isUndefined(learnerCache.learnerSidebarCourses[scope.course_id])) {
                    $http.get(config.api_learner + 'post/' + $stateParams.course_id)
                        .then(function(rs) {
                            scope.course = rs.data.data;
                            learnerCache.learnerSidebarCourses[$stateParams.course_id] = rs.data.data;
                            angular.element('h1#page-title span').text(scope.course.title);
                        });
                } else {
                    scope.course = learnerCache.learnerSidebarCourses[scope.course_id];
                    angular.element('h1#page-title span').text(scope.course.title);
                }
            }
            $element.css('display', 'block');
            $element.slimScroll({
                position: 'right',
                height: '700px',
                railVisible: true,
                //alwaysVisible: true,
                color: '#00f'
            });

            scope.$onInit = function() {
                loadUserInfo();
                loadModuleAndTask();
                loadCourse();
            };
        }
    });
    app.component('learnerDashboard', {
        templateUrl: 'app/learner/theme/learner-dashboard.html',
        controller: function($http, $anchorScroll, config, $element, $window) {
            var scope = this;
            scope.$onInit = function() {
                angular.element('body').addClass('skin-blue sidebar-collapse').removeClass('sidebar-mini');
                angular.element($window).bind("scroll", function() {
                    if ($window.pageYOffset >= 80) {
                        $element.find('#pchat').removeClass('padding-top');
                    } else {
                        $element.find('#pchat').addClass('padding-top');
                    }
                });

                scope.postView = null;

            };

            scope.extendChat = function() {
                $element.find('#pchat').toggleClass('extend');
                $element.find('.put-content').toggleClass('extend');
            }

            scope.backChat = function() {
                $element.find('#contentChat').toggleClass('out');
            }

            scope.showchat = function() {
                $element.find('#contentChat').toggleClass('out');
            }

            scope.viewPost = function(event) {
                $http.get(config.api_learner + 'post/' + event.id)
                    .then(function(rs) {
                        scope.postView = rs.data.data;
                        setTimeout(function() {
                            $anchorScroll('common-post-view');
                        }, 100);
                    }, function() {
                        scope.postView = null;
                    });
            };

            scope.closeViewPost = function() {
                scope.postView = null;
            }
        }
    });
    app.component('learnerDashboard2', {
        controller: function($http, $anchorScroll, config) {
            var scope = this;
            scope.$onInit = function() {
                angular.element('body').addClass('skin-blue sidebar-collapse').removeClass('sidebar-mini');
                scope.postView = null;
            };

            scope.viewPost = function(event) {
                $http.get(config.api_learner + 'post/' + event.id)
                    .then(function(rs) {
                        scope.postView = rs.data.data;
                        setTimeout(function() {
                            $anchorScroll('common-post-view');
                        }, 100);
                    }, function() {
                        scope.postView = null;
                    });
            };

            scope.closeViewPost = function() {
                scope.postView = null;
            }
        }
    });
    app.component('learnerActivies', {});
    app.component('learnerTodolist', {
        templateUrl: 'app/learner/theme/learner-todolist.html',
        controller: function($rootScope, $window, learnerWorkService, helperFunc) {
            function handleError(res) {
                if (res.status == 404) {
                    $rootScope.$emit('message:error', 'Todo Not Found');
                    return;
                }

                if (res.status !== 400)
                    return;
                var messages = res.data.messages;
                angular.forEach(messages, function(message, name) {
                    $rootScope.$emit('message:error', message[0]);
                });
            }

            function load() {
                learnerWorkService.list().then(function(res) {
                    scope.works = res.data.data;
                });
            }
            var scope = this;
            scope.$onInit = function() {
                scope.modal = null;
                scope.works = [];
                scope.work = {
                    todo: null,
                    start_date: null,
                    end_date: null
                };
                scope.workUpdate = {
                    todo: null,
                    start_date: null,
                    end_date: null
                };
                scope.table = {
                    actions: [{ name: 'edit', label: 'Edit' }, { name: 'delete', label: 'Delete' }],
                    fields: [
                        { label: 'Todo', value: 'todo' },
                        { label: 'Start Date', value: 'start_date', filter: { formatDate: null } },
                        { label: 'End Date', value: 'end_date', filter: { formatDate: null } }
                    ]
                };
                load();
                angular.element('h1#page-title').html('Calendar <small>List Todo</small>');
            };

            scope.action = function(event) {
                switch (event.action) {
                    case 'edit':
                        learnerWorkService.get(event.id).then(function(rs) {
                            scope.workUpdate = rs.data.data;
                            scope.workUpdate.start_date = moment(scope.workUpdate.start_date, 'YYYY-MM-DD H:I:S').format('YYYY-MM-DD');
                            scope.workUpdate.end_date = moment(scope.workUpdate.end_date, 'YYYY-MM-DD H:I:S').format('YYYY-MM-DD');
                            scope.modal = 'edit';
                            helperFunc.showModal('#learner-todolist-modal');
                        });
                        break;
                    case 'delete':
                        if ($window.confirm('Confirm Delete Todo'))
                            learnerWorkService.delete(event.id)
                            .then(function() {
                                $rootScope.$emit('message:success', 'Delete Todo Successfull');
                                load();
                            }, handleError);
                        break;
                }
            };

            scope.createWork = function(event) {
                var work = event.item;
                learnerWorkService.post(work)
                    .then(function(res) {
                        scope.works.push(res.data.data);
                        scope.work = {};
                        $rootScope.$emit('message:success', 'Create Todo Successfull');
                    }, handleError);

            };

            scope.updateWork = function(event) {
                var work = event.item;
                learnerWorkService.put(work, work.id)
                    .then(function(res) {
                        $rootScope.$emit('message:success', 'Update Todo Successfull');
                        load();
                        scope.modal = null;
                        scope.work = {};
                        helperFunc.hideModal('#learner-todolist-modal');
                    }, handleError);
            }
        }
    });
    app.component('learnerCourses', {
        templateUrl: 'app/learner/theme/learner-courses.html',
        controller: function($http, config, vlNotify, learnerCache) {
            var scope = this;
            scope.$onInit = function() {
                loadList();
            };

            function loadList() {
                if (learnerCache.courses) {
                    scope.courses = learnerCache.courses;
                } else {
                    vlNotify.show('#learner-courses');
                    $http.get(config.api_learner + 'courses?limit=40')
                        .then(function(rs) {
                            scope.courses = rs.data.data;
                            learnerCache.courses = rs.data.data;
                            vlNotify.hide('#learner-courses');
                        });
                }
            }
        }
    });
    app.component('learnerCourse', {
        bindings: {
            course: '<'
        },
        templateUrl: "app/learner/theme/learner-course.html",
        controller: function($sce) {
            var scope = this;
            scope.$onInit = function() {
                scope.content = $sce.trustAsHtml(scope.course.content);
                angular.element('h1#page-title').html(scope.course.title + ' <small>Overview Course</small>');
            }
        }
    });
    app.component('learnerMasterFileConfig', {
        templateUrl: "app/learner/theme/learner-master-file-config.html",
        controller: function($http, config, vlNotify, $stateParams) {
            var scope = this;
            scope.$onInit = function() {
                scope.config = [];
                loadConfig();
            };
            scope.save = function() {
                vlNotify.show('#learner-courses');
                $http.post(config.api_learner + 'master-file/config', scope.config)
                    .then(function(rs) {
                        scope.config = rs.data.data;
                        vlNotify.hide('#learner-courses');
                    });
            };

            function loadConfig() {
                vlNotify.show('#learner-courses');
                $http.get(config.api_learner + 'master-file/config')
                    .then(function(rs) {
                        scope.config = rs.data.data;
                        vlNotify.hide('#learner-courses');
                    });
            }
        }
    });
    app.component('learnerInfo', {
        templateUrl: 'app/learner/theme/learner-info.html',
        controller: function(userService, learnerService) {
            var scope = this;
            scope.$onInit = function() {
                userService.info().then(function(res) {
                    scope.user = res.data.data;
                });
                learnerService.getMetadata().then(function(res) {
                    scope.learner_info = res.data.data;
                });
            }
        }
    });
    app.component('learnerProfile', {
        bindings: { user: '<' },
        template: '<item-form template="app/learner/theme/profile.html" item="::$ctrl.user" on-submit="$ctrl.submit($event)"/>',
        controller: function($scope, userService) {
            var scope = this;
            scope.$onInit = function() {
                angular.element('h1#page-title').html(scope.user.name + ' <small>Update Profile</small>');
            };
            scope.submit = function(event) {
                var user = event.item;
                userService.profile(user).then(function(res) {
                    $scope.$emit('message:success', 'Update Profile Successfull');
                }, function(res) {
                    if (res.status !== 400) return;
                    var messages = res.data.messages;
                    angular.forEach(messages, function(message) {
                        angular.forEach(message, function(mess) {
                            $scope.$emit('message:error', mess);
                        });
                    });
                });
            };
        }
    });
    app.component('allCourses', {
        templateUrl: 'app/learner/theme/all-courses.html',
        controller: function($scope, $element, $location, $window, learnerService, helperFunc, userService) {
            var scope = this;
            scope.$onInit = function() {
                scope.courses = [];
                scope.modal = 'none';
                scope.module = {};
                scope.user = {};
                learnerService.allCourse().then(function(res) {
                    var rs = res.data;
                    scope.courses = rs.data;
                });
            };

            scope.action = function(event) {
                switch (event.action) {
                    case 'create-payment':
                        scope.modal = 'none';
                        learnerService.getModule(event.id).then(function(res) {
                            scope.module = res.data.data;
                            scope.modal = 'create-payment';
                            scope.module.current_date = moment().format('DD-MM-YY');
                            helperFunc.showModal('#learner-allcourses-modal');
                        });
                        break;

                    case 'create-course-payment':
                        scope.modal = 'none';
                        userService.info().then(function(rs) {
                            scope.user = rs.data.data;
                            // console.log(scope.user);
                        });
                        learnerService.getCourse(event.id).then(function(res) {
                            scope.course = res.data.data;
                            // console.log(scope.course);
                            scope.modal = 'create-course-payment';
                            scope.course.current_date = moment().format('DD-MM-YY');
                            helperFunc.showModal('#learner-allcourses-modal');
                        });
                        break;
                }
            };

            scope.createPayment = function(event) {
                var id = event.item.id;

                learnerService.checkout(id).then(function(res) {
                    var redirect = res.data.redirect;
                    $window.location.href = redirect;
                }, function(res) {
                    $scope.$emit('message:error', 'Request Incorrect');
                });
            };

            scope.createCoursePayment = function(event) {
                var id = event.item.id;
                var form = {
                    promotion_code: event.item.promotion_code || null
                };

                $element.find("button.btn-success").button('loading');
                $element.find(".checkPay").button('loading');
                // btn btn-primary
                learnerService.checkoutCourse(id, form).then(function(res) {
                    var redirect = res.data.redirect;
                    $window.location.href = redirect;
                }, function(res) {
                    $element.find("button.btn-success").button('reset');
                    $element.find(".checkPay").button('reset');
                    $scope.$emit('message:error', 'Request Incorrect');
                });
            }
        }
    });
    app.component('myModules', {
        templateUrl: 'app/learner/theme/my-modules.html',
        controller: function($scope, learnerService, $stateParams, helperFunc) {
            var scope = this;
            scope.$onInit = function() {
                learnerService.myModules().then(function(res) {
                    scope.courses = res.data.data;
                });
                if ($stateParams.payment && parseInt($stateParams.payment) == 1) {
                    $scope.$emit('message:success', 'Payment is accepted');
                }
            };
        }
    });
    app.component('myActivities', {
        templateUrl: 'app/learner/theme/my-activities.html',
        controller: function(learnerService, helperFunc) {
            var scope = this;
            scope.$onInit = function() {
                learnerService.activites().then(function(res) {
                    scope.activities = res.data.data;
                });
            };
        }
    });
    app.component('tasksByModule', {
        bindings: { module: '<' },
        templateUrl: 'app/learner/theme/tasks-by-module.html',
        controller: function(learnerService) {
            var scope = this;
            scope.$onInit = function() {
                learnerService.allTaskByModule(scope.module.id).then(function(res) {
                    scope.tasks = res.data.data;
                });
                learnerService.allTaskMeritByModule(scope.module.id).then(function(res) {
                    scope.meritTasks = res.data.data;
                });
                learnerService.allTaskDictionByModule(scope.module.id).then(function(res) {
                    scope.dictionTasks = res.data.data;
                });
            }
        }
    });
    app.component('enrollCourse', {
        templateUrl: 'app/learner/theme/enroll-course.html',
        bindings: { course_id: '<' },
        controller: function($scope, $rootScope, $element, $window, $localStorage, $stateParams, Auth, userService, learnerService, helperFunc, vlNotify) {
            var scope = this;
            scope.$onInit = function() {
                scope.modal = null; // payment register login
                scope.isAuth = false;
                scope.item = {};
                scope.user ={};
                scope.userInfo ={};
                userService.logout();
                angular.element('body').addClass('hold-transition login-page');
            };

            scope.$postLink = function () {
                var i = $window.innerHeight;
                angular.element('.main, .col4Fix').css({ height: i });
            }

            scope.$onDestroy = function() {
                angular.element('body').removeClass('hold-transition login-page')
            };

            scope.action = function(event) {
                scope.modal = 'none';
                switch (event.action) {
                    case 'payment':
                        // data-loading-text="Loading..."
                        helperFunc.hideModal('#learner-enroll-modal');
                        userService.info().then(function(rs) {
                            scope.userInfo = rs.data.data;
                            console.log(scope.userInfo);
                        });
                        learnerService.getCourse($stateParams.course_id).then(function(res) {
                            scope.course = res.data.data;
                            scope.modal = 'payment';
                            scope.course.current_date = moment().format('DD-MM-YY');
                        }, function() {
                            $scope.$emit('message:success', 'Course Not Found Or You paid this course');
                        });
                        break;
                    case 'login':
                        scope.item = {};
                        scope.modal = 'login';
                        helperFunc.showModal('#learner-enroll-modal');

                        break;
                    case 'register':
                        scope.item = {};
                        scope.modal = 'register';
                        helperFunc.showModal('#learner-enroll-modal');
                        break;
                }
            };

            scope.createCoursePayment = function(event) {
                var id = event.item.id;
                var form = {
                    promotion_code: event.item.promotion_code || null
                };
                $element.find(".checkPay").button('loading');
                learnerService.checkoutCourse(id, form).then(function(res) {
                    var redirect = res.data.redirect;
                    $scope.$emit("message:success", "Redirecting Paypal. Do not close page");
                    $window.location.href = redirect;
                }, function(res) {
                    $element.find(".checkPay").button('reset');
                    $scope.$emit('message:error', 'Request Incorrect');
                });
            };

            scope.register = function(event) {
                var form = event.item;
                $element.find("button.btn-success").button('loading');
                Auth.register(form).then(function(res) {
                    $element.find('.authentication-form').remove();
                    $scope.$emit('message:success', 'Register Success');
                    scope.action({ action: 'payment' });
                    scope.isAuth = true;
                }, function(res) {
                    $element.find("button.btn-success").button('reset');
                    vlNotify.errors(res);
                });
            };

            scope.login = function(event) {
                var form = event.item;
                // console.log(form);
                $element.find("button.btn-success").button('loading');
                Auth.login(form).then(function(res) {
                    $element.find('.authentication-form').remove();
                    $scope.$emit('message:success', 'Login Success');
                    $localStorage.token = res.data.token;
                    scope.action({ action: 'payment' });
                    scope.isAuth = true;
                }, function(res) {
                    $element.find("button.btn-success").button('reset');
                    vlNotify.errors(res);
                });
            };

            scope.loginEnroll = function () {
                Auth.login(scope.user).then(function(res) {
                    $scope.$emit('message:success', 'Login Success');
                    $localStorage.token = res.data.token;
                    scope.action({ action: 'payment' });
                    scope.isAuth = true;
                }, function(res) {
                    vlNotify.errors(res);
                });
            }
        }
    });
    app.component('generateGuestPayment', {
        templateUrl: 'app/learner/theme/generate-guest-payment.html',
        controller: function($element, $window, guestPaymentService) {
            var scope = this;

            scope.$onInit = function () {
                scope.courses = ["Choose Program","Postgraduate Diploma – Organisational Transformation","Foundation Diploma Business Management ( LWC )","Level 3 Diploma in Business Management","Level 4 Extended Diploma in Management","Level 4/5 Hotel & Hospitality","Level 5 Extended Diploma in Management","Level 6 Diploma Business and Administrative","Level 7 Diploma in Strategic Management","MA Marketing & Innovation Top up (Anglia Ruskin University)","MA Marketing & Innovation Full degree (Anglia Ruskin University)","MBA Top up (Anglia Ruskin University)","BA (Hons) Marketing Top up (Anglia Ruskin University)","BA (Hons) Business Management Top up (Anglia Ruskin University)","MBA Top up (University of Northampton)","LLM International Commercial Law (University of Northampton)","MA in Education (University of Bedfordshire)","BA (Hons) Hospitality & Tourism management Top up (University of Bedfordshire)"];
                scope.guest = null;
                scope.form = {
                    name: "",
                    phone: "",
                    email: "",
                    course: "Choose Program",
                    price: 1000
                }
            }

            scope.$postLink = function () {
                var i = $window.innerHeight;
                angular.element('.main, .col4Fix').css({ height: i });
            }

            scope.generate = function() {
                $element.find('#btn-generate').button('loading');
                scope.guest = null;
                guestPaymentService.generate(scope.form)
                    .then(function (res) {
                        scope.guest = res.data.data;
                    }, function () {

                    })
                    .then(function () {
                        $element.find('#btn-generate').button('reset');
                    })
            }
        }
    });
    app.component('paymentSuccess',{
        templateUrl: 'app/learner/theme/payment-success.html',
        bindings:{
            method:'<',
        },
        controller: function (adminService) {
            var scope = this;
            scope.$onInit = function () {
                // userService.info().then(function(rs) {
                //     scope.user = rs.data.data;
                // });
                scope.listInfoPayment = {};
                var q = ''
                adminService.listInfoPayment(q).then(function (res) {
                    angular.forEach(res.data.data, function(data) {
                        if(data.code == scope.method){
                            scope.listInfoPayment.name = data.name;
                            scope.listInfoPayment.payment_method = data.payment_method;
                            console.log(scope.listInfoPayment)
                        }
                    });

                    // console.log(res.data.data)
                })


            }
        }
    })

    app.component('guestPayment', {
        templateUrl: 'app/learner/theme/guest-payment.html',
        bindings: {
            code: '<',
        },
        controller: function( $state,$element, $timeout, $window, guestPaymentService, helperFunc, $stateParams, vlNotify) {
            var scope = this;
            scope.$onInit = function() {
                scope.guest = null;
                guestPaymentService.getCode(scope.code)
                    .then(function (res) {
                        scope.guest = res.data.data;
                        console.log(scope.guest);
                        if(scope.guest.payment_method === 'bank_transfer' || scope.guest.payment_method === 'payment-at-office' || scope.guest.payment_method === 'paypal'){
                            $state.go('app.payment_success', {payment_method: scope.code})
                        };

                    }, function () {
                        helperFunc.notifyError("This Payment Link Not Valid");
                    });
                angular.element('body').addClass('hold-transition login-page');
            };

            function redirectToInfoAward(pay_method) {
                console.log(pay_method);
                helperFunc.notifySuccess("Redirecting to Success Page");
                $timeout(function () {
                    $state.go('app.payment_success', {payment_method: scope.code});
                    // $window.location = 'http://info-awards.com/';
                }, 1000);
            }

            scope.$postLink = function () {
                if($stateParams.paypal) {
                    helperFunc.notifySuccess("Thank you ! Our administration will confirm with your success payment.");
                    redirectToInfoAward();
                }
            };

            scope.choosePaymenMethod = function (method) {
                $element.find('.button-choose-payment').button('loading');
                if(method == 'bank_transfer' || method == 'payment_at_office') {
                    guestPaymentService.choosePayment(scope.code, {payment_method: method})
                        .then(function (res) {
                            helperFunc.notifySuccess("Thank you ! Our administration will confirm with your pending payment.");
                            redirectToInfoAward(method);
                        }, function () {
                            helperFunc.notifyError("Something went wrong");
                            $element.find('.button-choose-payment').button('reset');
                        })
                        .then(function () {
                            //$element.find('.button-choose-payment').button('reset');
                        });

                    return;
                }

                guestPaymentService.generatePaypalUrl(scope.code)
                    .then(function(res) {
                        if(!res.data.redirect) return;
                        var redirect = res.data.redirect;
                        helperFunc.notifySuccess("Redirecting to Paypal Site.");
                        $window.location = redirect;
                    }, function () {

                    })
                    .then(function () {

                    });
            }
        }
    });
    app.component('ekCourse', {
        templateUrl: function ($attrs) {
            return $attrs.templateUrl || 'app/learner/theme/ek-course.html'
        },
        bindings: {
            courses: '<',
        },
        controller: function() {
            var scope = this;
            scope.$onInit = function() {

            }
        }
    });
    app.component('ekModulesByCourse', {
        templateUrl: 'app/learner/theme/ek-modules.html',
        bindings: {
            modules: '<'
        },
        controller: function(eKnowledgeService, $state) {
            var scope = this;
            scope.$onInit = function() {

            }
            scope.viewDetail = function(module) {
                $state.go('app.eknowledge.module_detail', {
                    'module_id': module.id
                });
            }
        }
    });
    app.component('ekModuleDetail', {
        templateUrl: 'app/learner/theme/ek-module-detail.html',
        bindings: {
            module: '<'
        },
        controller: function(eKnowledgeService, $state, commonService) {
            var scope = this;
            function loadLessons() {
                eKnowledgeService.getLessonsByModule(scope.module.id).then(function(res) {
                   scope.lessons = res.data.data;
                   if(scope.lessons[0])
                      scope.chooseLesson(scope.lessons[0]);
                });
            }
            scope.$onInit = function() {
                scope.lessons = [];
                scope.lesson = {};
                scope.lesson_sections = [];
                scope.lesson_section_next_page_url = null;
                scope.lesson_section_prev_page_url = null;
                loadLessons();
            }
            scope.chooseLesson = function(lesson) {
              scope.lesson = lesson;
              eKnowledgeService.getSectionsByLesson(lesson.id, 'limit=1').then(function(res) {
                 scope.lesson_sections = res.data.data;
                 scope.lesson_section_next_page_url = res.data.next_page_url;
                 scope.lesson_section_prev_page_url = res.data.prev_page_url;
              });
            }
            function loadUrl(url) {
                commonService.getUrl(url)
                    .then(function (res) {
                        scope.lesson_sections = res.data.data;
                        scope.lesson_section_next_page_url = res.data.next_page_url;
                        scope.lesson_section_prev_page_url = res.data.prev_page_url;
                    });
            }
            scope.prevPage = function() {
                loadUrl(scope.lesson_section_prev_page_url);
            }
            scope.nextPage = function() {
                loadUrl(scope.lesson_section_next_page_url)
            }
        }
    });
    
    function moduleFinalFileController($element, learnerService) {
        var scope = this;
        
        scope.$onInit = function () {}

        scope.uploadFile = function () {
            $element.find('input[type=file]').trigger("click");
        }

        scope.$postLink = function () {
            $element.find('input[type=file]').on('change', function vlUploadFileOnUpload(e) {
                var files = e.target.files;
                $element.find('button').button('loading');
                learnerService.uploadFileFinalModule(scope.userModule.post_id, files[0])
                    .then(function (res) {
                        alert("Submit Success");
                        scope.userModule = res.data.data;
                    }, function (reason) {
                        alert("Submit Fail");
                    })
                    .then(function () {
                        $element.find('button').button('reset');
                    });
            });
        }
    }
    app.component('moduleFinalFile', {
        templateUrl: function ($attrs) {
            return $attrs.templateUrl || 'app/learner/theme/module-final-file.html';
        },
        bindings: {
            userModule: '<'
        },
        controller: moduleFinalFileController
    });
})(window.angular);
