(function(angular) {
    var app = angular.module('adminModule', []);
    app.config(function($stateProvider) {
        $stateProvider.state('app.admin', {
            url: 'admin',
            templateUrl: 'app/admin/theme/admin-layout.html',
            controller: function($scope, $state, $stateParams) {
                angular.element('body').addClass('skin-blue sidebar-mini');
            }
        });
        $stateProvider.state('app.admin.admin_dashboard', {
            url: '/dashboard',
            component: 'adminDashboard'
        });
        $stateProvider.state('app.admin.courses', {
            url: '/courses',
            component: 'adminCourses'
        });
        $stateProvider.state('app.admin.course_edit', {
            url: '/courses/:course_id/edit',
            component: 'adminCoursesEdit'
        });
        $stateProvider.state('app.admin.course_create', {
            url: '/courses/create',
            component: 'adminCoursesCreate'
        });
        $stateProvider.state('app.admin.course_view', {
            url: '/courses/:course_id/detail',
            templateUrl: 'app/admin/theme/admin-course-dashboard.html',
            resolve: {
                course: function($stateParams, adminService) {
                    return adminService.getCourse($stateParams.course_id).then(function(res) {
                        return res.data.data;
                    });
                }
            }
        });
        $stateProvider.state('app.admin.module_create', {
            url: '/course/:course_id/module/create',
            component: 'adminModuleCreate'
        });
        $stateProvider.state('app.admin.module_edit', {
            url: '/courses/:course_id/module/edit/:id',
            template: '<admin-module-edit id="$resolve.id"></admin-module-edit>',
            resolve: {
                id: function($stateParams) {
                    console.log($stateParams);
                    return $stateParams.id;
                }
            }
        });
        $stateProvider.state('app.admin.users', {
            url: '/users',
            component: 'adminUsers'
        });
        $stateProvider.state('app.admin.user_edit', {
            url: '/users/:user_id/edit',
            component: 'adminUserEdit'
        });
        $stateProvider.state('app.admin.user_create', {
            url: '/users/create',
            component: 'adminUserCreate'
        });
        $stateProvider.state('app.admin.checkouts', {
            url: '/checkouts',
            component: 'adminCheckouts'
        });
        $stateProvider.state('app.admin.checkout_view', {
            url: '/checkouts/:checkout_id/detail',
            component: 'adminCheckoutView'
        });
        $stateProvider.state('app.admin.user', {
            url: '/user',
            component: 'adminUsers'
        });
        $stateProvider.state('app.admin.payments_list', {
            url: '/list',
            template: '<admin-payments-list on-click="onClick($event)"/>',
            controller: function ($scope, $state) {
                $scope.onClick = function (event) {
                    $state.go('app.admin.payments_details', {payments_code: event.code})
                }
            }
        });
        $stateProvider.state('app.admin.payments_details', {
            url: '/details/:payments_code',
            template: '<admin-payments-details code="$resolve.resolve_payments_code"/>',
            resolve:{
                resolve_payments_code: function ($stateParams) {
                    return $stateParams.payments_code
                }
            }
            
        });
        $stateProvider.state('app.admin.eknowledge', {
            url: '/eknowledge',
            template: '<ui-view></ui-view>',
        });
        $stateProvider.state('app.admin.eknowledge.courses', {
            url: '/courses',
            template: '<admin-ek-course courses="$resolve.courses"/>',
            resolve: {
                courses: function($http, $stateParams, config) {
                    return $http.get(config.api_admin + 'ek/course')
                        .then(function(rs) {
                            var courses = rs.data.data;
                            return courses;
                        });
                }
            }
        });
        $stateProvider.state('app.admin.eknowledge.course_add', {
            url: '/course/add',
            template: '<admin-ek-course-form course="$resolve.course"/>',
            resolve: {
                course: function($http, $stateParams, config, adminEKnowledgeService) {
                    return {}
                }
            }
        });
        $stateProvider.state('app.admin.eknowledge.course_edit', {
            url: '/course/:course_id',
            template: '<admin-ek-course-form course="$resolve.course"/>',
            resolve: {
                course: function($http, $stateParams, config, adminEKnowledgeService) {
                    return adminEKnowledgeService.showCourse($stateParams.course_id)
                      .then(function(res) {
                          return res.data.data;
                      })
                }
            }
        });
        $stateProvider.state('app.admin.eknowledge.module_detail', {
            url: '/module/:module_id',
            template: '<admin-ek-module-detail module="$resolve.module"/>',
            resolve: {
                module: function($http, $stateParams, config, adminEKnowledgeService) {
                    return adminEKnowledgeService.getModule($stateParams.module_id)
                      .then(function(res) {
                          return res.data.data;
                      })
                }
            }
        });
    });

    app.component('adminDashboard', {
        templateUrl: 'app/admin/theme/admin-dashboard.html',
        controller: function ($state) {
            var scope = this;
            scope.detailsPayment = function (event) {
                $state.go('app.admin.payments_details', {payments_code: event.code})
            }
        }
    });
    app.component('adminNav', {
        templateUrl: 'app/admin/theme/admin-nav.html'
    });
    app.component('adminSidebar', {
        templateUrl: 'app/admin/theme/admin-sidebar.html',
        controller: function($scope, userService) {
            var scope = this;
            function loadUserInfo() {
                userService.info().then(function(res) {
                    scope.user = res.data.data;
                })
            }
            scope.$onInit = function() {
                loadUserInfo();
            };
        }
    });
    app.component('adminCourses', {
        templateUrl: 'app/admin/theme/admin-courses.html',
        controller: function($scope, adminService) {
            var scope = this;
            function loadCourses(page) {
                adminService.listCourses(page).then(function(res) {
                    scope.courses = res.data.data;
                });
            }
            scope.$onInit = function() {
                scope.courses = [];
                loadCourses(1);
            };
        }
    });
    app.component('adminCoursesCreate', {
        templateUrl: 'app/admin/theme/admin-course-form.html',
        controller: function($scope, $element, adminService) {
            var scope = this;
            scope.$onInit = function() {
                scope.course = {
                    title: null,
                    slug: null,
                    price: 0,
                    teacher_id: 0,
                    content: '',
                    image: '',
                    postmeta: []
                };
                scope.teachers = [];
                adminService.getAllTeachers().then(function(res) {
                    scope.teachers = res.data.data;
                });
            };
            scope.save = function() {
                $element.find('button').button('loading');
                console.log(scope.course);
                adminService.createCourse(scope.course).then(function(res) {
                    var course = res.data.data;
                    scope.course = course;
                    $scope.$emit('message:success', course.title + ' Create Success');
                    $element.find('button').button('reset');
                }, function() {
                    $scope.$emit('message:error', 'Something Went Wrong?');
                    $element.find('button').button('reset');
                });
            };
        }
    });
    app.component('adminCoursesEdit', {
        templateUrl: 'app/admin/theme/admin-course-form.html',
        controller: function($scope, $element, adminService, $stateParams) {
            var scope = this;
            scope.$onInit = function() {
                scope.course = {
                    title: null,
                    slug: null,
                    price: null,
                    teacher_id: 0,
                    content: '',
                    image: '',
                    postmeta: []
                };
                adminService.getCourse($stateParams.course_id).then(function(res) {
                    scope.course = res.data.data;
                });
                scope.teachers = [];
                adminService.getAllTeachers().then(function(res) {
                    scope.teachers = res.data.data;
                });
            };
            scope.save = function() {
                $element.find('button').button('loading');
                adminService.updateCourse(scope.course.id, scope.course).then(function(res) {
                    var course = res.data.data;
                    scope.course = course;
                    $scope.$emit('message:success', course.title + ' Update Success');
                    $element.find('button').button('reset');
                }, function() {
                    $scope.$emit('message:error', 'Something Went Wrong?');
                    $element.find('button').button('reset');
                });
            };
        }
    });
    app.component('adminModuleList', {
        templateUrl: 'app/admin/theme/admin-module-list.html',
        bindings: {course: '<'},
        controller: function($scope, $element, adminService) {
            var scope = this;
            scope.$onInit = function() {
                adminService.getListModulesCourse(scope.course.id).then(function(res) {
                    scope.modules = res.data.data;
                });
            };
        }
    });
    app.component('adminModuleCreate', {
        template: '<admin-module-form module="$ctrl.module" on-save="$ctrl.save($event)" on-back="$ctrl.onBack()"></admin-module-form>',
        controller: function($http, $element, $state, $stateParams, config, notifications, vlHandleErrors) {
            var scope = this,
                course_id = null;
            scope.$onInit = function() {
                course_id = $stateParams.course_id;
                scope.module = {
                    title: null,
                    slug: null,
                    parent_id: course_id,
                    image: '',
                    postmeta: []
                };
            };
            scope.save = function(event) {
                $element.find('button.button-submit').button('loading');
                $http.post(config.api_admin + 'module', event.module)
                    .then(function(rs) {
                        var module = rs.data.data;
                        notifications.showSuccess({message: module.title + ' Created Successfull'});
                        $state.go('app.admin.module_edit', {course_id: course_id, id: module.id});
                    }, function() {
                        notifications.showError({message: 'Please Check Full Fill'});
                    })
                    .then(function() {
                        $element.find('button.button-submit').button('reset');
                    });
            };
        }
    });
    app.component('adminModuleEdit', {
        template: '<admin-module-form ng-if="$ctrl.module" module="$ctrl.module" on-save="$ctrl.save($event)" on-delete="$ctrl.onBack()" on-back="$ctrl.onBack()"></admin-module-form>',
        bindings: {
            module: '<',
            id: '<'
        },
        controller: function($state, $element, $http, $stateParams, config, notifications, adminService) {
            var scope = this;

            scope.$onInit = function() {
                if(angular.isUndefined(scope.module))
                    getModule();
            };

            scope.save = function(event) {
                $element.find('button.button-submit').button('loading');
                $http.put(config.api_admin + 'module/' + scope.module.id, event.module)
                    .then(function() {
                        notifications.showSuccess({message: 'Update Module Successfull'});
                    }, function() {
                        notifications.showError({message: 'Please Check Full Fill'});
                    })
                    .then(function() {
                        $element.find('button.button-submit').button('reset');
                    });
            };
            scope.onBack = function() {
                $state.go('app.teacher.course', {course_id: $stateParams.course_id});
            };

            function getModule() {
                adminService.getModule($stateParams.id).then(function(res) {
                    scope.module = res.data.data;
                });
            }
        }
    });
    app.component('adminModuleForm', {
        templateUrl: 'app/admin/theme/admin-module-form.html',
        bindings: {
            module: '<',
            onSave: '&',
            onDelete: '&',
            onUpdate: '&',
            onBack: '&'
        },
        controller: function($window, $http, $stateParams, parseMetadataFilter, config, vlNotify, teacherCache, notifications) {
            var scope = this;

            function loadModules() {
                $http.get(config.api_admin + 'module?limit=50&course_id=' + scope.course_id)
                    .then(function(rs) {
                        scope.modules = [{id: 0, title: 'Choose Lession'}].concat(rs.data.data);
                    }, function() {
                        scope.modules = [{id: 0, title: 'Choose Lession'}];
                    });
            }

            scope.$onInit = function() {
                scope.counrses = [];
                scope.metadata = {};
                scope.course_id = $stateParams.course_id;
                if(!scope.module.parent_id)
                    scope.module.parent_id = 0;
                var postmeta = parseMetadataFilter(scope.module.postmeta);
                scope.metadata.document = postmeta.document ? JSON.parse(postmeta.document) : [];
                delete scope.module.postmeta;

                loadModules();
            };
            scope.save = function() {
                scope.module.postmeta = scope.metadata;
                scope.onSave({
                    $event: {
                        module: scope.module
                    }
                });
            };

            scope.delete = function() {
                if(!$window.confirm('Are Your Sure Delete?'))
                    return;
                vlNotify.show('#module-form');
                $http.delete(config.api_admin + 'module/' + scope.module.id)
                    .then(function() {
                        if(scope.onDelete)
                            scope.onDelete();
                        notifications.showSuccess({message: 'Delete Successfull'});
                        vlNotify.hide();
                    });
            };

            scope.back = function() {
                if(scope.onBack)
                    scope.onBack();
            };
        }
    });
    app.component('adminUsers', {
        templateUrl: 'app/admin/theme/admin-users.html',
        controller: function($scope, $window, adminService, helperFunc) {
            var scope = this;
            function loadUsers(page) {
                adminService.listUsers(page).then(function(res) {
                    scope.users = res.data.data;
                });
            }
            scope.$onInit = function() {
                scope.courses = [];
                loadUsers(1);
            };

            scope.delete = function (id, user) {
                if(! $window.confirm('Confirm Delete User ' + user.name + ' ?'))
                    return;

                adminService.deleteUser(id)
                    .then(function (res) {
                        helperFunc.notifySuccess("Delete Success");
                        loadUsers(1);
                    }, function (reason) {
                        helperFunc.notifyError("Delete Fail");
                    });
            }
        }
    });
    app.component('adminUserCreate', {
        templateUrl: 'app/admin/theme/admin-user-form.html',
        controller: function($scope, $element, adminService, notifications) {
            var scope = this;
            scope.$onInit = function() {
                scope.user = {
                    name: null,
                    email: null,
                    type: null,
                    course: []
                };
                console.log(scope.user);
                scope.types = {
                    learner: 'learner',
                    teacher_1: 'teacher_1',
                    admin: 'admin',
                    teacher_master_file: 'teacher_master_file'
                };
                scope.courses = [];
                adminService.listCourses(1).then(function(res) {
                    scope.courses = res.data.data;
                });
            };
            scope.save = function() {
                $element.find('button').button('loading');
                adminService.createUser(scope.user).then(function(res) {
                    var user = res.data.data;
                    scope.user = user;
                    notifications.showSuccess({message: user.name + ' Created Successfull'});
                    $state.go('app.admin.user_edit', {user_id: scope.user.id});
                }, function() {
                    $scope.$emit('message:error', 'Something Went Wrong?');
                    $element.find('button').button('reset');
                });
            };
        }
    });
    app.component('adminUserEdit', {
        templateUrl: 'app/admin/theme/admin-user-form.html',
        controller: function($scope, $element, adminService, $stateParams, notifications) {
            var scope = this;
            scope.$onInit = function() {
                scope.user = {
                    name: null,
                    email: null,
                    type: null,
                    course: []
                };

                adminService.getUser($stateParams.user_id).then(function(res) {
                    scope.user = res.data.data;
                    if(scope.user.type == 'learner') {
                        adminService.getCoursesByLearnerV2(scope.user.id, 'select[]=id&select[]=title').then(function(res) {
                            scope.user.course = res.data.data;
                        });
                    }
                });

                scope.types = {
                    learner: 'learner',
                    teacher_1: 'teacher_1',
                    admin: 'admin',
                    teacher_master_file: 'teacher_master_file'
                };
                scope.courses = [];
                adminService.listCoursesV2('select[]=id&select[]=title').then(function(res) {
                    scope.courses = res.data.data;
                });
            };
            scope.save = function() {
                $element.find('button').button('loading');
                adminService.updateUser(scope.user.id, scope.user).then(function(res) {
                    var user = res.data.data;
                    scope.user = user;
                    adminService.getCoursesByLearnerV2(scope.user.id, 'select[]=id&select[]=title').then(function(res) {
                        scope.user.course = res.data.data;
                    });
                    $scope.$emit('message:success', scope.user.name + ' Update Success');
                    $element.find('button').button('reset');
                }, function() {
                    $scope.$emit('message:error', 'Something Went Wrong?');
                    $element.find('button').button('reset');
                });
            };
        }
    });
    app.component('adminCheckouts', {
        templateUrl: 'app/admin/theme/admin-checkouts.html',
        controller: function($scope, adminService, $sce, $stateParams) {
            var scope = this;
            function loadCheckouts(page, search) {
                adminService.listCheckouts(page, search).then(function(res) {
                    scope.checkouts = res.data.data;
                    scope.total = res.data.total;
                    scope.links = $sce.trustAsHtml(res.data.links);
                });
            }
            scope.$onInit = function() {
                scope.checkouts = [];
                scope.links = null;
                scope.total = 0;
                scope.search = {
                    s : {}
                };

                var page = $stateParams.page || 1;
                loadCheckouts(page, $.param(scope.search));
            };

            scope.paginate = function(page) {
                loadCheckouts(page, $.param(scope.search));
            };

            scope.searchCheckout = function() {
                loadCheckouts(1, $.param(scope.search));
            };
        }
    });
    app.component('adminCheckoutView', {
        templateUrl: 'app/admin/theme/admin-checkout-form.html',
        controller: function($scope, $element, adminService, $stateParams) {
            var scope = this;
            scope.$onInit = function() {
                scope.checkout = {};
                adminService.getCheckout($stateParams.checkout_id).then(function(res) {
                    scope.checkout = res.data.data;
                });
            };
        }
    });
    app.component('adminPaymentsList', {
        templateUrl: 'app/admin/theme/admin-list-guest-payment.html',
        bindings:{
            onClick: '&'
        },
        controller: function ($httpParamSerializer,adminService) {
            var scope = this;

            var load = function (q) {
                adminService.listInfoPayment(q).then(function (res) {
                    scope.listInfoPayment = res.data.data;
                    // console.log(scope.listInfoPayment)
                })
            }

            scope.$onInit = function () {
                scope.listInfoPayment = {};
                scope.filter={};
                var q =''
                load(q);
            }

            scope.detailsPayment = function (payments_code) {
                if (scope.onClick) {
                    scope.onClick({
                        $event:{
                            code : payments_code
                        }
                    })

                }
            }

            scope.filterGuestPayment = function () {
                var q = $httpParamSerializer(scope.filter);
                // console.log(scope.filter);
                load(q);
            }
        }
    });
    app.component('adminPaymentsDetails', {
        templateUrl: 'app/admin/theme/guest-details.html',
        bindings:{
            code:'<'
        },
        controller: function (adminService,$element) {
            var scope = this;

            function load() {
                adminService.detailsPayment(scope.code).then(function (res) {
                    scope.dataDetails = res.data.data;
                    // console.log(scope.dataDetails);
                })
            }

            scope.$onInit = function () {
                $element.find('#save_btn').css({'display':'none'})
                scope.dataDetails = {};
                load();
                // console.log("thong");
            }

            scope.editInfo = function () {
                $element.find('#edit_btn').css({'display':'none'})
                $element.find('#save_btn').css({'display':'block'})
                $element.find('input[type=text]').removeAttr('disabled')
            }

            scope.saveInfo = function () {
                $element.find('#save_btn').button('loading');
                adminService.updatePayment(scope.dataDetails.code, scope.dataDetails).then(function (res) {
                    alert("The information were successful updated");
                    $element.find('#save_btn').button('reset');
                    $element.find('#edit_btn').css({'display':'block'})
                    $element.find('input[type=text]').attr("disabled", true)
                    $element.find('#save_btn').css({'display':'none'})
                })
            }

            scope.sentMail = function () {
                if(scope.dataDetails.price == 0 || scope.dataDetails.price == ' '){
                    alert('Field Price invalid');
                    return
                }else{
                    $element.find('#sent_btn').button('loading');
                    adminService.sentEmailPayment(scope.dataDetails.code).then(function (res) {
                        alert("Message sent successfully");
                    },
                    function (rs) {
                        alert('Sent mail Fail');
                    })
                    .then(function () {
                        $element.find('#sent_btn').button('reset');
                    });
                }

            }

            scope.confirm = function () {
                $element.find('#confirm_btn').button('loading');
                var a ={
                    code:scope.dataDetails.code,
                    process: 'complete'
                }
                adminService.updateProcessPayment(a).then(function (res) {
                    alert("Confirm payment success!");
                },
                function (rs) {
                    alert('Confirm Fail');
                })
                .then(function () {
                    $element.find('#confirm_btn').button('reset');
                });
            }




        }
    });
    app.component('eKnowledge', {
        templateUrl: 'app/admin/theme/admin-e-knowledge.html',
        controller: function (adminService,$element) {
            var scope = this;

            function load() {
                adminService.listEkCourses(page).then(function(res) {
                    scope.courses = res.data.data;
                });
            }

            scope.$onInit = function () {
                scope.courses = [];
                load();
            }
        }
    });
    app.component('adminEkCourse', {
        templateUrl: 'app/admin/theme/ek-course.html',
        bindings: {
            courses: '<',
        },
        controller: function(adminEKnowledgeService, $state, $scope) {
            var scope = this;
            function loadCourses() {
                adminEKnowledgeService.getCourses('')
                  .then(function(res) {
                      scope.courses = res.data.data;
                  });
            }
            scope.$onInit = function() {

            }
            scope.deleteCourse = function(course) {
                adminEKnowledgeService.deleteCourse(course.id).then(function(){
                    $scope.$emit('message:success','Delete Success');
                    loadCourses();
                });
            }
            scope.add = function() {
                $state.go('app.admin.eknowledge.course_add');
            }
            scope.edit = function(course) {
                $state.go('app.admin.eknowledge.course_edit', {
                    'course_id': course.id
                });
            }
        }
    });
    app.component('adminEkModulesByCourse', {
        templateUrl: 'app/admin/theme/ek-modules.html',
        bindings: {
            modules: '<',
            courseId: '<',
        },
        controller: function(adminEKnowledgeService, $state, $scope) {
            var scope = this;
            scope.$onInit = function() {
                if(!scope.modules) {
                    scope.modules = [];
                }
            }
            scope.viewDetail = function(module) {
                $state.go('app.admin.eknowledge.module_detail', {
                    'module_id': module.id
                });
            }
            scope.editCourse = function(course) {
                $state.go('app.admin.eknowledge.course_edit', {
                    'course_id': course.id
                });
            }
            scope.addNewLineModuleByCourse = function() {
                 scope.modules.push({});
            }
            scope.addModuleByCourse = function(ek_module) {
                adminEKnowledgeService.addModuleByCourse(scope.courseId, ek_module).then(function(res) {
                    $scope.$emit('message:success','Add Module '+ ek_module.title +' Success');
                    scope.modules[scope.modules.indexOf(ek_module)] = res.data.data;
                });
            }
            scope.editModuleByCourse = function(ek_module) {
                adminEKnowledgeService.editModuleByCourse(scope.courseId, ek_module.id, ek_module).then(function(res) {
                    $scope.$emit('message:success','Edit Module '+ ek_module.title +' Success');
                    scope.modules[scope.modules.indexOf(ek_module)] = res.data.data;
                });
            }
            scope.deleteModuleByCourse = function(ek_module) {
                adminEKnowledgeService.deleteModuleByCourse(scope.courseId, ek_module.id).then(function(res) {
                    $scope.$emit('message:success','Delete Module '+ ek_module.title +' Success');
                    scope.modules.splice(scope.modules.indexOf(ek_module), 1);
                });
            }
        }
    });
    app.component('adminEkModuleDetail', {
        templateUrl: 'app/admin/theme/ek-module-detail.html',
        bindings: {
            module: '<'
        },
        controller: function(adminEKnowledgeService,
              $state,
              commonService,
              $scope,
              $element,
              $timeout
        ) {
            var scope = this;
            function loadLessons() {
                adminEKnowledgeService.getLessonsByModule(scope.module.id).then(function(res) {
                   scope.lessons = res.data.data;
                });
            }
            scope.$onInit = function() {
                scope.modal = false;
                scope.lessons = [];
                scope.lesson = {};
                scope.section = {};
                scope.lesson_sections = [];
                scope.lesson_section_next_page_url = null;
                scope.lesson_section_prev_page_url = null;
                loadLessons();
            }
            scope.chooseLesson = function(lesson) {
              scope.lesson = lesson;
              adminEKnowledgeService.getSectionsByLesson(lesson.id, 'limit=1').then(function(res) {
                 scope.lesson_sections = res.data.data;
                 scope.lesson_section_next_page_url = res.data.next_page_url;
                 scope.lesson_section_prev_page_url = res.data.prev_page_url;
              });
            }
            function loadUrl(url) {
                commonService.getUrl(url)
                    .then(function (res) {
                        scope.lesson_sections = res.data.data;
                        scope.lesson_section_next_page_url = res.data.next_page_url;
                        scope.lesson_section_prev_page_url = res.data.prev_page_url;
                    });
            }
            scope.prevPage = function() {
                loadUrl(scope.lesson_section_prev_page_url);
            }
            scope.nextPage = function() {
                loadUrl(scope.lesson_section_next_page_url)
            }
            scope.updateModule = function() {
                adminEKnowledgeService.updateModule(scope.module.id, scope.module).then(function(res) {
                    $scope.$emit('message:success','Edit Module '+ scope.module.title +' Success');
                    scope.module = res.data.data;
                });
            }
            scope.reloadLessons = function(event) {
                hideItemModalById('admin-module-lesson-modal');
                loadLessons();
            }
            scope.reloadLesson = function(event) {
                hideItemModalById('admin-module-lesson-modal');
                scope.chooseLesson(scope.lesson);
            }
            function showItemModalById(id, delayMiliseconds) {
                delayMiliseconds = delayMiliseconds || 100;
                scope.modal = false;
                $timeout(function () {
                  scope.modal = true;
                  $element.find('#'+ id).modal('show');
                }, delayMiliseconds);
            }
            function hideItemModalById(id) {
                scope.modal = false;
                $element.find('#' + id).modal('hide');
            }
            scope.addLesson = function() {
                scope.lesson = {
                    'module_id' : scope.module.id
                };
                showItemModalById('admin-module-lesson-modal', 200);
            }
            scope.editLesson = function(lesson) {
                scope.lesson = lesson;
                showItemModalById('admin-module-lesson-modal', 200);
            }
            scope.deleteLesson = function(lesson) {
                adminEKnowledgeService.deleteLessonByModule(scope.module.id, lesson.id).then(function(res) {
                    $scope.$emit('message:success','Delete Lesson '+ lesson.title +' Success');
                    scope.lessons.splice(scope.lessons.indexOf(lesson), 1);
                });
            }
            scope.addSection = function() {
                if(!scope.lesson.id) {
                    $scope.$emit('message:error','Please choose Lesson');
                    return;
                }
                scope.section = {
                    'lession_id' : scope.lesson.id
                };
                showItemModalById('admin-module-lesson-section-modal', 200);
            }
            scope.editSection = function(section) {
                if(!scope.lesson.id) {
                    $scope.$emit('message:error','Please choose Lesson');
                    return;
                }
                scope.section = section;
                showItemModalById('admin-module-lesson-section-modal', 200);
            }
        }
    });
    app.component('adminEkCourseForm', {
        templateUrl: 'app/admin/theme/ek-course-form.html',
        bindings: {
            course: '<'
        },
        controller: function(adminEKnowledgeService, $state, commonService) {
            var scope = this;
            scope.$onInit = function() {

            }
            scope.submit = function() {
                if(scope.course.id) {
                    adminEKnowledgeService.editCourse(
                      scope.course.id,
                      scope.course
                    ).then(function(res) {
                        scope.course = res.data.data;
                    });
                }
                else {
                    adminEKnowledgeService.addCourse(
                      scope.course
                    ).then(function(res) {
                        scope.course = res.data.data;
                        $state.go('app.admin.eknowledge.course_edit', {
                            'course_id' : scope.course.id
                        })
                    });
                }
            }
        }
    });
    app.component('adminEkLessonModal', {
        templateUrl: 'app/admin/theme/ek-lesson-modal-form.html',
        bindings: {
            lesson: '<',
            onSubmit: '&'
        },
        controller: function(adminEKnowledgeService, $state, commonService, $scope) {
            var scope = this;
            scope.$onInit = function() {

            }
            scope.submit = function($event) {
                if(scope.lesson.id) {
                    adminEKnowledgeService.editLessonByModule(
                        scope.lesson.module_id,
                        scope.lesson.id,
                        scope.lesson
                    ).then(function(res) {
                        $scope.$emit('message:success','Edit Lesson '+ scope.lesson.title +' Success');
                        scope.lesson = res.data.data;
                        scope.onSubmit({
                            $event: {
                                lesson: res.data.data
                            }
                        });
                    });
                }
                else {
                    adminEKnowledgeService.addLessonByModule(
                        scope.lesson.module_id,
                        scope.lesson
                    ).then(function(res) {
                        $scope.$emit('message:success','Add Lesson '+ scope.lesson.title +' Success');
                        scope.lesson = res.data.data;
                        scope.onSubmit({
                            $event: {
                                lesson: res.data.data
                            }
                        });
                    });
                }
            }
        }
    });
    app.component('adminEkLessonSectionModal', {
        templateUrl: 'app/admin/theme/ek-lesson-section-modal-form.html',
        bindings: {
            section: '<',
            onSubmit: '&'
        },
        controller: function(adminEKnowledgeService, $state, commonService, $scope) {
            var scope = this;
            scope.$onInit = function() {

            }
            scope.submit = function($event) {
                if(scope.section.id) {
                    adminEKnowledgeService.editSectionByLesson(
                        scope.section.lession_id,
                        scope.section.id,
                        scope.section
                    ).then(function(res) {
                        $scope.$emit('message:success','Edit Section '+ scope.section.title +' Success');
                        scope.section = res.data.data;
                        scope.onSubmit({
                            $event: {
                                section: res.data.data
                            }
                        });
                    });
                }
                else {
                    adminEKnowledgeService.addSectionByLesson(
                        scope.section.lession_id,
                        scope.section
                    ).then(function(res) {
                        $scope.$emit('message:success','Add Lesson '+ scope.section.title +' Success');
                        scope.section = res.data.data;
                        scope.onSubmit({
                            $event: {
                                section: res.data.data
                            }
                        });
                    });
                }
            }
        }
    })

})(window.angular);
