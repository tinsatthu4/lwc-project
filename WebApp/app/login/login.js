(function(angular) {
    'use strict';
    function afterLoginOrRegister(token, $state, $localStorage, userService) {
        $localStorage.token = token;
        $localStorage.user = {};
        userService.info().then(function(res) {
            var user = res.data.data;
            switch (user.type) {
                case 'learner':
                    
                    $state.go('app.learner_dashboard');
                    break;
                case 'teacher_1':
                    $state.go('app.teacher_dashboard');
                    break;
                case 'admin':
                    break;
                case 'teacher_master_file':
                    break;
            }
        });
    }

    function controller($window,$scope, $element, $state, $localStorage, Auth, userService) {
        var scope = this;
        function setZopimInformation() {
            $zopim(function() {
                userService.info().then(function(res) {
                    var user = res.data.data;
                    $zopim.livechat.setName(user.name);
                    $zopim.livechat.setEmail(user.email);
                });
            });
        }
        scope.$onInit = function() {
            // angular.element('body').addClass('background-london hold-transition login-page').removeClass("skin-blue sidebar-mini");
            scope.user = {
                email: null,
                password: null
            };
            scope.block  = 'none';

            $element.bind('keypress', function(event) {
                if(event.which == 13 || event.keyCode == 13) {
                    scope.login();
                }
            });
        };
        // scope.$destroy = function() {
        //     angular.element('body').removeClass('background-london');
        // };
        scope.$postLink = function () {
            var i = $window.innerHeight;
            angular.element('.main, .col4Fix').css({ height: i });   
        }
        scope.login = function() {
            $element.find('#login-error')
                .addClass('hide');
            $element.find('button.submit-login').button('loading');
            Auth.authentication(scope.user.email, scope.user.password)
                .then(function(rs) {
                    //store Token
                    var token = rs.data.token;
                    $localStorage.token = token;
                    $localStorage.user = {};
                    userService.info().then(function(res) {
                        var user = res.data.data;
                        switch (user.type) {
                            case 'learner':
                                var i = $window.innerWidth;
                                if(i <= 767){
                                    $state.go('app.learner_mobile.my_course');
                                }else{
                                    $state.go('app.learner_dashboard');
                                }
                                break;
                            case 'teacher_1':
                                $state.go('app.teacher_dashboard');
                                break;
                            case 'admin':
                                $state.go('app.admin.admin_dashboard');
                                break;
                            case 'teacher_master_file':
                                break;
                        }
                    });

                    setZopimInformation();
                }, function(rs) {
                    if(rs.status == 400) {
                        $scope.$emit('message:error', 'User Or Password incorrect');
                    }
                })
                .then(function() {
                    $element.find('button.submit-login').button('reset');
                });
        }
    }

    angular.module("login", ['ui.router', 'configModule', 'ngStorage', 'MyService'])
        .config(function($stateProvider, $urlRouterProvider) {
            $urlRouterProvider.otherwise('login');
            $stateProvider.state('login', {
                url: '/login',
                component: 'loginComponent'
            });

            $stateProvider.state('logout', {
                url: '/logout',
                template: '<ui-view />',
                controller: function($localStorage, $state, Auth) {
                    Auth.logout().then(function() {
                        delete $localStorage.token;
                        delete $localStorage.user;
                        delete $localStorage.setting;
                        $state.go('login');
                    });
                }
            });

            $stateProvider.state('register', {
                url: '/register',
                component: 'registerComponent'
            });
        })
        .component('registerComponent', {
            templateUrl: 'app/login/register.html',
            controller: function($scope, $state, $localStorage, userService) {
                var scope = this;
                scope.$onInit = function() {
                    scope.user = {};
                    angular.element('body').addClass('register-page');
                };

                scope.$onDestroy = function() {
                    angular.element('body').removeClass('register-page');
                };

                scope.register = function() {
                    userService.register(scope.user).then(function(res) {
                        var token = res.data.token;
                        afterLoginOrRegister(token, $state, $localStorage, userService);
                        $scope.$emit('message:success', 'Register Successfull');
                    }, function(res) {
                        if(res.status !== 400) return;

                        var messages = res.data.messages;
                        angular.forEach(messages, function(message) {
                            angular.forEach(message, function(notify) {
                                $scope.$emit('message:error', notify);
                            })
                        });
                    });
                }
            }
        })
        .component('loginComponent', {
            bindings: {},
            templateUrl: 'app/login/login.html',
            controller: controller
        });

})(window.angular);