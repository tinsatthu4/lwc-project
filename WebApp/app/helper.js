(function (angular) {
    'use strict';
    angular.module('helper', ['MyService'])
        .value('uiTinymceConfig', {
            height: 500,
            theme: 'modern',
            fontsize_formats: '8pt 9pt 10pt 11pt 12pt 13pt 14pt 15pt 16pt 17pt 18pt 19pt 20pt 21pt 22pt 23pt 24pt 36pt',
            plugins: [
                'autolink lists link image hr anchor pagebreak',
                'wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern'
            ],
            toolbar1: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link',
            toolbar2: 'preview media | forecolor backcolor emoticons | fontsizeselect'
        })
        .constant('vlHandleErrors', function (rs) {
            angular.element('div.overlay').remove();
        })
        .constant('vlHtmlCommon', {
            loading: '<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>'
        })
        .constant('helperFunc', {
            showModal: function (element) {
                angular.element(element).modal('show');
            },
            hideModal: function (element) {
                angular.element(element).modal('hide');
                angular.element('.modal-backdrop').remove();
                angular.element('body')
                    .removeClass('modal-open')
                    .css('padding-right', '0px');
            },
            notifySuccess: function (message) {
                setTimeout(function () {
                    $.notify({
                        title: '<strong>Success !</strong>',
                        icon: 'icon fa fa-check',
                        message: message
                    }, {
                            type: 'success',
                            z_index: 999999
                        });
                }, 500);
            },
            notifyWarning: function (message) { },
            notifyError: function (message) {
                $.notify({
                    title: '<strong>Error !</strong>',
                    icon: 'icon fa fa-ban',
                    message: message
                }, {
                        type: 'danger',
                        z_index: 999999,
                    });
            },
            breadcrumbs: function (title, href) {
                var s = 'title';
                angular.forEach(href, function (item) {

                });

                angular.element('h1#page-title').html(s);
            }
        })
        .constant('vlCommonObject', {
            taskObjectInit: {
                title: null,
                keywords: '',
                preference: [],
                preference_text: '',
                question: '',
                type: 'assignment'
            }
        })
        .constant('commonCache', {})
        .constant('teacherCache', {
            teacherSidebarModules: {},
            teacherSidebarCourses: {}
        })
        .factory('vlNotify', function (vlHtmlCommon, $rootScope, $anchorScroll) {
            function show(idElement) {
                hide(idElement);
                angular.element(idElement).append(vlHtmlCommon.loading);
            }

            function hide(idElement) {
                if (idElement) {
                    angular.element(idElement + ' div.overlay').remove();
                    //$anchorScroll(idElement.replace('#', ''));
                }
                else
                    angular.element('div.overlay').remove();
            }

            return {
                show: show,
                hide: hide,
                errors: function (res) {
                    if (res.status !== 400) return;

                    var messages = res.data.messages;
                    angular.forEach(messages, function (message) {
                        angular.forEach(message, function (notify) {
                            $rootScope.$emit('message:error', notify);
                        })
                    });
                }
            };
        })
        .filter('trimText', function () {
            return function (input) {
                console.log(input);
                return input.trim();
            }
        })
        .filter('arrayToText', function () {
            return function (input, char) {
                if (!input) return null;
                if (!char)
                    var char = '\n';
                var s = '';
                angular.forEach(input, function (text) {
                    s = s.concat(char, text);
                    s = s.trim(char);
                });

                return s;
            };
        })
        .filter('textToArray', function () {
            return function (input) {
                if (!input) return null;
                var obj = [];
                input = input.trim();
                angular.forEach(input.split('\n'), function (value) {
                    if (value.length)
                        obj.push(value);
                });
                return obj;
            }
        })
        .filter('parseMetadata', function ($parse) {
            return function (value) {
                var newObj = {};
                if (angular.isUndefined(value) || value.length == 0) return newObj;
                angular.forEach(value, function (obj) {
                    newObj[obj.meta_key] = obj.meta_value;
                });
                return newObj;
            };
        })
        .filter('parser', function ($parse) {
            return function (value, info) {
                return $parse(info)(value);
            }
        })
        .filter('parserField', function ($parse, $sce, $filter) {
            return function (value, field) {
                if (field.value) {
                    value = $parse(field.value)(value);
                }

                if (field.filter) {
                    for (var filterName in field.filter) {
                        if (field.filter[filterName] === null)
                            value = $filter(filterName)(value);
                        else if (typeof field.filter[filterName] == "object") {
                            value = $filter(filterName).apply(null, [value].concat(field.filter[filterName]));
                        }
                    }
                }
                return value;
            }
        })
        .filter('formatDate', function () {
            return function (value, format) {
                format = format || 'DD-MM-YYYY';
                return moment(value, 'YYYY-MM-DD H:I:S').format(format);
            };
        })
        .filter('dateIsBefore', function () {
            return function (input, dateToCompare) {
                var date = dateToCompare || moment().format('YYYY-MM-DD');
                return moment(input).isBefore(date);
            }
        })
        .filter('dateIsAfter', function () {
            return function (input, dateToCompare) {
                var date = dateToCompare || moment().format('YYYY-MM-DD');
                return moment(input).isAfter(date);
            }
        })
        .filter('isBetween', function () {
            return function (fromDate, toDate) {
                var current = moment().format('YYYY-MM-DD');
                return moment().isBetween(fromDate, toDate);
            }
        })
        .filter('percent', function () {
            return function (total, value) {
                total = parseInt(total);
                value = parseInt(value);

                return (value * 100) / total;
            }
        })
        .filter('imageName', function () {
            return function (input) {
                var arr = input.split("/");
                return arr.pop();
            }
        })
        .filter('htmlToPlaintext', function () {
            return function (text) {
                return text.replace(/<\/?[^>]+(>|$)/g, "").replace(/&nbsp;/g, ' ');
            }
        })
        .filter('matchKeywords', function ($filter) {
            return function (input, keywords) {
                var contents = $filter('htmlToPlaintext')(input),
                    count = 0;
                keywords = keywords.split(',');
                angular.forEach(keywords, function (key) {
                    var patt = new RegExp(key, "g");
                    if (patt.test(contents)) {
                        count++;
                    }
                });
                return count;

            }
        })
        .filter('paymentMethod', function ($sce) {
            return function (value) {
                var s = '';
                switch (value) {
                    case 'bank_transfer':
                        s = 'Bank Transfer'
                        break;
                    case 'payment_at_office':
                        s = 'Payment At Office'
                        break;
                    case 'paypal':
                        s = 'Paypal'
                        break;
                    case 'PAYPAL':
                        s = 'Paypal'
                        break;
                }
                return $sce.trustAsHtml(s);
            }
        })
        .filter('filterProcess', function ($sce) {
            return function (value) {
                var s = '';
                switch (value) {
                    case 'pending':
                        s = '<span class="label bg-yellow">Pending</span>'
                        break;
                    case 'send_mail':
                        s = '<span class="label bg-blue">Mail sent</span>'
                        break;
                    case 'payment':
                        s = '<span class="label bg-ogran">Payment received</span>'
                        break;
                    case 'complete':
                        s = '<span class="label bg-green">Completed</span>'
                        break;
                }
                return $sce.trustAsHtml(s);
            }
        })
        .filter('unsafe', function($sce) {
            return $sce.trustAsHtml;
        })
        .directive('vlSelectProcess', function () {
            return {
                restrict: 'E',
                scope: { model: '=ngModel' },
                template: '<select ng-model="model" class="form-control input-sm" ng-options="key as value for (key,value) in options"></select>',
                link: function (scope, el) {
                    scope.options = {
                        all: 'Loại Tồn kho',
                        order_complete: 'Bán ra',
                        INPUT_FROM_RETAILER: 'Nhập KHO NCC',
                        OUTPUT_TO_STORE: 'Xuất Kho',
                        INPUT_FROM_STORE: 'Nhập Kho Từ CN'
                    };
                }
            }
        })
        .directive('ngConfirmClick', [
            function(){
                return {
                    priority: -1,
                    restrict: 'A',
                    link: function(scope, element, attrs){
                        element.bind('click', function(e){
                            var message = attrs.ngConfirmClick;
                            // confirm() requires jQuery
                            if(message && !confirm(message)){
                              e.stopImmediatePropagation();
                              e.preventDefault();
                            }
                        });
                    }
                }
            }
        ])
        .directive('vlDate', function () {
            return {
                restrict: 'E',
                scope: { model: '=ngModel' },
                template: ' <div class="input-group date"> <div class="input-group-addon"> <i class="fa fa-calendar"></i> </div><input type="text" class="form-control pull-right"> </div>',
                link: function (scope, el) {
                    var input = el.find('input');
                    input.datepicker({
                        autoclose: true,
                        format: 'dd-mm-yyyy',
                        zIndexOffset: 99999
                    });
                    if (/^\d{4}-\d{2}-\d{2}$/.test(scope.model) || /^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$/.test(scope.model)) {
                        input.datepicker('update', moment(scope.model, 'YYYY-MM-DD').format('DD-MM-YYYY'));
                    }

                    input.on('changeDate', function (e) {
                        scope.model = moment(e.date).format('YYYY-MM-DD');
                        scope.$apply();
                    });
                }
            };
        })
        .directive('calendar', function () {
            return {
                scope: { model: '=ngModel' },
                template: '<div></div>',
                link: function (scope, el, attrs) {
                    //el.
                    var date = new Date();
                    var d = date.getDate(),
                        m = date.getMonth(),
                        y = date.getFullYear();
                    var calendar = el.find('div');
                    calendar.fullCalendar({
                        header: {
                            left: 'prev,next today',
                            center: 'title',
                            right: 'month,agendaWeek,agendaDay'
                        },
                        buttonText: {
                            today: 'today',
                            month: 'month',
                            week: 'week',
                            day: 'day'
                        },
                        //Random default events
                        events: [
                            {
                                title: 'Read Document',
                                start: new Date(y, m, 1),
                                backgroundColor: "#f56954", //red
                                borderColor: "#f56954" //red
                            },
                            {
                                title: 'Do Homework',
                                start: new Date(y, m, d - 5),
                                end: new Date(y, m, d - 2),
                                backgroundColor: "#f39c12", //yellow
                                borderColor: "#f39c12" //yellow
                            },
                            {
                                title: 'Meeting Teacher',
                                start: new Date(y, m, d, 10, 30),
                                allDay: false,
                                backgroundColor: "#0073b7", //Blue
                                borderColor: "#0073b7" //Blue
                            },
                            {
                                title: 'Lunch',
                                start: new Date(y, m, d, 12, 0),
                                end: new Date(y, m, d, 14, 0),
                                allDay: false,
                                backgroundColor: "#00c0ef", //Info (aqua)
                                borderColor: "#00c0ef" //Info (aqua)
                            },
                            {
                                title: 'Birthday Party',
                                start: new Date(y, m, d + 1, 19, 0),
                                end: new Date(y, m, d + 1, 22, 30),
                                allDay: false,
                                backgroundColor: "#00a65a", //Success (green)
                                borderColor: "#00a65a" //Success (green)
                            },
                            {
                                title: 'Research Technical',
                                start: new Date(y, m, 28),
                                end: new Date(y, m, 29),
                                url: 'http://google.com/',
                                backgroundColor: "#3c8dbc", //Primary (light-blue)
                                borderColor: "#3c8dbc" //Primary (light-blue)
                            }
                        ],
                        editable: false,
                        droppable: false
                    });
                }
            }
        })
        .directive('vlPagination', function () {
            return {
                restrict: 'A',
                scope: { onClick: "=" },
                link: function (scope, el) {
                    el.on('click', 'ul.pagination a', function (e) {
                        e.preventDefault();
                        var url = this.href;
                        var page = url.match(/\?page=(\d+)/);
                        if (scope.onClick && page[1])
                            scope.onClick(parseInt(page[1]));

                        scope.$apply();
                        return false;
                    });
                }
            }
        })
        .directive('vlUiTinymce', function () {
            return {
                restrict: 'E',
                scope: {
                    model: '=ngModel'
                },
                template: '<textarea id="tiny-mce-editor" ui-tinymce ng-model="model" class="form-control"></textarea>'
            };
        })
        .directive('vlUpload', ['BaseApi', function (BaseApi) {
            return {
                restrict: 'E',
                scope: {
                    onDone: '=',
                    model: '=ngModel'
                },
                templateUrl: 'portal/theme/vl-upload.html',
                link: function (scope, el, attr) {
                    if (attr.accept)
                        el.find('input[type=file]').attr('accept', attr.accept);
                    el.find('i,img').click(function (e) {
                        el.find('input[type=file]').trigger('click');
                        return false;
                    });
                    el.find('input[type=file]').on('change', function (e) {
                        var files = e.target.files,
                            response = [];

                        BaseApi.postUpload(files[0])
                            .then(function (rs) {
                                var data = rs.data.data;
                                scope.model = data.path;

                                if (angular.isDefined(scope.onDone)) {
                                    scope.onDone(data);
                                }
                                el.find('input[type=file]').val("");
                            }, function (rs) {
                                var message = rs.data.message;
                                alert('Upload Fail');
                                el.find('input[type=file]').val("");
                            });
                    });
                }
            };
        }])
        .directive('vlUploadFile', ['$http', function ($http) {
            return {
                restrict: 'E',
                scope: {
                    onDone: '=',
                    model: '=ngModel'
                },
                template: '<input type="file" style="display: none;"/> <a href="javascript:void();" class="upload">Upload</a> <div class="form-group" ng-show="model.length"> <input type="text" disabled="disabled" class="form-control" ng-value="model"/> </div>',
                link: function (scope, el, attr) {
                    if (!attr.url) {
                        alert('Please provide url at vlUploadFile Directive');
                        return;
                    }
                    if (attr.accept)
                        el.find('input[type=file]').attr('accept', attr.accept);
                    el.find('a.upload').click(function (e) {
                        el.find('input[type=file]').trigger('click');
                        return false;
                    });
                    el.find('input[type=file]').on('change', function vlUploadFileOnUpload(e) {
                        var files = e.target.files;

                        var form = new FormData();
                        form.append('file', files[0]);
                        $http.post(attr.url, form, {
                            transformRequest: angular.identity,
                            headers: { 'Content-Type': undefined }
                        })
                            .then(function (rs) {
                                var data = rs.data.data;
                                scope.model = data.path;

                                if (angular.isDefined(scope.onDone)) {
                                    scope.onDone(data);
                                }
                                el.find('input[type=file]').val("");
                            },
                                function (rs) {
                                    var message = rs.data.message;
                                    alert('Upload Fail');
                                    el.find('input[type=file]').val("");
                                });
                    });
                }
            };
        }])
        .directive('vlTitles', function (settingService) {
            return {
                restrict: 'E',
                scope: {
                    model: '=ngModel'
                },
                template: '<select class="form-control" ng-options="key as value for (key, value) in options" ng-model="model"></select>',
                link: function (scope, el, attrs) {
                    scope.options = { 'none': attrs.title || 'Danh Xưng' };
                    settingService.getGroup('titles').then(function (rs) {
                        angular.forEach(rs.data.data, function (value, key) {
                            scope.options[key] = value;
                        });
                        if (!scope.options[scope.model])
                            scope.model = 'none';
                    });
                }
            }
        })
        .directive('vlUsers', function ($timeout, userService) {
            return {
                restrict: 'E',
                scope: {
                    model: '=ngModel'
                },
                template: '<select class="form-control select2" ng-options="opt.id as opt.name for opt in options" ng-model="model" style="width: 100%;"></select>',
                link: function (scope, el, attrs) {
                    scope.options = [{
                        id: 0,
                        name: attrs.title || 'Chọn Nhân Viên'
                    }];
                    if (!scope.model)
                        scope.model = 0;
                    userService.list().then(function (rs) {
                        scope.options = scope.options.concat(rs.data.data);
                        $timeout(function () {
                            el.find(".select2").select2();
                        }, 100);
                    });
                }
            }
        })
        .directive('vlFilesForm', function ($http) {
            return {
                restrict: 'E',
                scope: {
                    model: '=ngModel'
                },
                templateUrl: 'app/common/theme/helper-vl-files-form.html',
                link: function (scope, el, attr) {
                    scope.model = scope.model || [];
                    var uploading = 0;
                    if (!attr.url) {
                        alert('Please provide url at vlUploadFile Directive');
                        return;
                    }
                    if (attr.accept)
                        el.find('input[type=file]').attr('accept', attr.accept);

                    el.find('a.file-upload').click(function (e) {
                        el.find('input[type=file]').trigger('click');
                        return false;
                    });

                    function cb() {
                        uploading--;
                        if (uploading == 0) {
                            el.find('a.file-upload').show();
                            el.find('input[type=file]').val("");
                            el.find('#progress-file').hide();
                        }
                    }

                    el.find('input[type=file]').on('change', function vlUploadFileOnUpload(e) {
                        var files = e.target.files;
                        uploading += files.length;
                        angular.forEach(files, function (file) {
                            var form = new FormData();
                            form.append('file', file);
                            el.find('a.file-upload').hide();
                            el.find('#progress-file').show();
                            $http.post(attr.url, form, {
                                transformRequest: angular.identity,
                                headers: { 'Content-Type': undefined }
                            })
                                .then(function (rs) {
                                    var data = rs.data.data;
                                    scope.model.push(data.path);
                                },
                                    function (rs) {
                                        var message = rs.data.message;
                                        alert('Upload Fail');
                                    })
                                .then(cb);
                        });
                    });
                }
            }
        })
        .directive('vlEditorSimple', function ($timeout) {
            return {
                restrict: 'E',
                scope: { model: '=ngModel' },
                template: '<textarea class="textarea" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid rgb(221, 221, 221); padding: 10px;"></textarea>',
                link: function (scope, el) {
                    var editor = null;
                    $timeout(function () {
                        editor = el.find('textarea').wysihtml5();
                        editor.on('load', function () {
                        });
                        editor.on('change', function () {
                        });
                    }, 100);
                }
            }
        })
        .directive('viewSummernote', function ($timeout) {
            return {
                restrict: 'E',
                scope: { model: '=ngModel' },
                template: '<summernote height="::height" ng-model="::model"></summernote>',
                link: function (scope, el, attr) {
                    scope.height = attr.height || 500;
                    $timeout(function () {
                        el.find('div.summernote').summernote('disable');
                        el.find('div.note-toolbar.panel-heading').remove();
                        el.find('div.note-editable.panel-body').css('background-color', 'white');
                    }, 500);
                }
            }
        })
        .directive('vlBootstrapTagsinput', function ($timeout) {
            return {
                restrict: 'E',
                template: '<input type="text" ng-model="model"/>',
                scope: {
                    model: '=ngModel'
                },
                link: function (scope, el, attrs) {
                    var taginput = el.find('input').tagsinput({
                        trimValue: true
                    });
                    scope.$watch('model', function (newValue, oldValue) {
                        if (newValue == undefined)
                            return;
                        el.find('input').tagsinput('destroy');
                        el.find('input').tagsinput('focus');

                        if (attrs.readOnly) {
                            $timeout(function () {
                                el.find('span.tag > span').css('display', 'none');
                                el.find('input').remove();
                            }, 500);
                        }
                    }, true);
                    function updateScope(event) {
                        scope.model = el.find('input').val();
                    }

                    el.find('input').on('itemAdded', updateScope);
                    el.find('input').on('itemRemoved', updateScope);
                }
            }
        })
        .directive('orderFilterProcess', function () {
            return {
                restrict: 'E',
                scope: { model: '=ngModel' },
                template: '<select ng-model="model" class="form-control input-sm" ng-options="key as value for (key,value) in options"></select>',
                link: function (scope, el, attrs) {
                    scope.options = {
                        all: 'Filter Process',
                        pending: 'Pending',
                        payment: 'Payment',
                        sent_mail: 'Sent Mail',
                        complete: 'Complete'
                    }
                    if (!scope.model) {
                        scope.model = 'all';
                    }

                }
            }
        })
        .directive('orderFilterPaymentMethod', function () {
            return {
                restrict: 'E',
                scope: { model: '=ngModel' },
                template: '<select ng-model="model" class="form-control input-sm" ng-options="key as value for (key,value) in options"></select>',
                link: function (scope, el, attrs) {
                    scope.options = {
                        all: 'Filter Payment Method',
                        bank_transfer: 'Bank Transfer',
                        payment_at_office: 'Payment At Office',
                        paypal: 'Paypal',
                        // complete: 'Complete'
                    }
                    if (!scope.model) {
                        scope.model = 'all';
                    }

                }
            }
        })
        .directive('selectDirective', function (userService, config, $localStorage) {
            return {
                scope: {
                    model: '=ngModel',
                },
                template: '<div style="width: 100%;"></div>',
                link: function (scope, el, attrs) {
                    var selectEl = el.find('div');
                    var titleSelect = attrs.selectTitle;
                    var group = attrs.group;
                    angular.element(selectEl).select2({
                        multiple: false,
                        placeholder: titleSelect,
                        ajax: {
                            url: config.api + 'learner/my-modules/' + group + '/tasks',
                            dataType: 'json',
                            delay: 250,
                            data: function (params) {
                                return {
                                    s: params,
                                    token: $localStorage.token,
                                    // take: 10
                                };
                            },
                            results: function (data) {
                                var items = [];
                                console.log(data);
                                angular.forEach(data.data, function (item) {
                                    console.log(item)
                                    items.push({
                                        id: item.id,
                                        text: item.title
                                    });
                                });
                                return {
                                    results: items
                                }
                            },
                            cache: true
                        }
                    });
                    angular.element(selectEl).on('change', function (e) {
                        scope.model = e.val;
                        scope.$apply();
                    });
                }
            }
        })
        .directive('taskList', function ($timeout, learnerService) {
            return {
                restrict: 'E',
                scope: {
                    model: '=ngModel',
                    moduleid: '='
                },
                template: '<select class="form-control" ng-model="model" ng-options="opt.id as opt.title group by opt.type_of_grade for opt in options"></select>',
                link: function (scope, el, attrs) {
                    // scope.options = [{
                    //     id: 0,
                    //     title: 'Please select a Task'
                    // }];
                    // learnerService.allTaskByModule(scope.moduleid).then(function (res) {
                    //     scope.options = scope.options.concat(res.data.data);
                    //     $timeout(function () {
                    //         el.find(".select2").select2();
                    //     }, 100);
                    // })
                    if (!scope.model)
                        scope.model = 0;
                    scope.$watch(
                        function () {
                            return scope.moduleid;
                        },
                        function (newValue, oldValue) {
                            scope.options = [{
                                id: 0,
                                title: 'Please select a Task'
                            }];
                            if (!newValue || newValue === 0) {
                                return
                            }
                            learnerService.allTaskByModule(scope.moduleid).then(function (res) {
                                scope.options = scope.options.concat(res.data.data);
                                $timeout(function () {
                                    el.find(".select2").select2();
                                }, 100);
                            })
                        },
                        true);
                }
            }
        })
        .directive('moduleList', function ($timeout, learnerService) {
            return {
                restrict: 'E',
                scope: {
                    model: '=ngModel',
                    moduleid: '='
                },
                template: '<select class="form-control" ng-model="model" ng-options="opt.id as opt.title group by opt.type_of_grade for opt in options"></select>',
                link: function (scope, el, attrs) {
                    scope.options = [{
                        id: 0,
                        title: 'Select a Module'
                    }];
                    learnerService.myModules().then(function (res) {
                        scope.options = scope.options.concat(res.data.data);
                    });
                    if (!scope.model)
                        scope.model = 0;
                }
            }
        })
        .directive('moduleList2', function ($timeout, learnerService) {
            return {
                restrict: 'E',
                scope: {
                    model: '=ngModel',
                    // module: '='
                },
                template: '<select class="form-control" ng-model="model" ng-options="op.id as op.title for op in options"></select>',
                link: function (scope, el, attrs) {
                    scope.options = [{
                        id: 0,
                        title: 'Select a Module'
                    }];
                    learnerService.myModules().then(function (res) {
                        scope.courses = res.data.data;
                        angular.forEach(scope.courses, function (obj) {
                            if (obj.modules[0]) {
                                scope.modules = obj.modules;
                                scope.options = scope.options.concat(scope.modules);
                            }
                        })
                    });
                    if (!scope.model)
                        scope.model = 0;
                }
            }
        })
        .component('itemForm', {
            bindings: {
                item: '<',
                item2: '<',
                error: '<',
                onCancel: '&',
                onSubmit: '&'
            },
            templateUrl: function ($element, $attrs) {
                return $attrs.template;
            },
            controller: function () {
                var scope = this;

                scope.submit = function () {
                    if (scope.onSubmit)
                        scope.onSubmit({
                            $event: {
                                item: scope.item
                            }
                        })
                };
                scope.cancel = function () {
                    if (scope.onCancel) {
                        scope.onCancel({
                            $event: {
                                item: scope.item
                            }
                        })
                    }
                };
            }
        });
})(window.angular);
