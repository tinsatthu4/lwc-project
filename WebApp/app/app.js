(function(angular) {
    'use strict';
    angular.module("PortalApp", [
            'summernote', 'vcRecaptcha',
            'login', 'common', 'learnerModule', 'teacherModule', 'adminModule',
            'ng.ckeditor',
            'MyService', 'vlCommonComponent',
            'ngStorage', 'configModule', 'partial', 'helper',
            'ui.router', 'ui.router.state.events', 'templates', 'mobileLearnerModule'
        ])
        .run(function ($rootScope, $transitions, helperFunc) {
            $rootScope.$on('message:success', function(e, message) {
                helperFunc.notifySuccess(message);
            });
            $rootScope.$on('message:error', function(e, message) {
                helperFunc.notifyError(message);
            });

            $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams, options) {
                angular.element('#page-loading').removeClass('hide').addClass('show');
            });

            $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
                angular.element('#page-loading').removeClass('show').addClass('hide');
            });

            $rootScope.$on('$stateNotFound', function(event, toState, toParams, fromState, fromParams) {
                helperFunc.notifyError('Page Not Found');
            });
            $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {
                helperFunc.notifyError('Page Not Found');
            });
        })
        .config(function($stateProvider, $httpProvider, vcRecaptchaServiceProvider, config) {
            $httpProvider.interceptors.push(['$q', '$state', '$localStorage', function ($q, $state, $localStorage, $rootScope) {
                return {
                    'request': function (config) {
                        config.headers = config.headers || {};
                        if ($localStorage.token) {
                            config.headers.Authorization = 'Bearer ' + $localStorage.token;
                        }
                        return config;
                    },
                    'responseError': function (response) {
                        if (response.status === 401 && ! $state.is('app.learner_payment')) {
                            $state.go('logout');
                        }

                        if(response.status === 403 && ! $state.is('app.learner_payment')) {

                        }

                        if(response.status === 400) {
                            if(! $state.is('app.learner_payment')
                                && response.data.error
                                && response.data.error == 'token_not_provided'
                                && response.data.error == 'token_invalid'
                            )
                            {
                                $state.go('logout');
                            }
                        }

                        return $q.reject(response);
                    }
                };
            }]);

            $stateProvider.state("app", {
                url: '/',
                template: '<ui-view></ui-view>',
                controller: function($window, $timeout) {
                    var h = parseInt($window.innerHeight) - 101;
                    $timeout(function() {
                        angular.element('div.content-wrapper').css('min-height', h + 'px');
                    }, 1000);
                }
            });

            vcRecaptchaServiceProvider.setSiteKey(config.recaptcha_sitekey);
        })
        .component('app', {
            template: '<div class="wrapper"><ui-view></ui-view></div>'
        });
})(window.angular);
