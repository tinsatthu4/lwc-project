(function(angular) {
    var app = angular.module('teacherModule', []);
    app.config(function($stateProvider) {
        $stateProvider.state('app.teacher_dashboard', {
            url: 'teacher/dashboard',
            component: 'teacherDashboard'
        });
        $stateProvider.state('app.teacher', {
            url: 'teacher/',
            templateUrl: 'app/teacher/theme/teacher-layout.html',
            controller: function($state, $stateParams) {
                if(!$stateParams.course_id)
                    $state.go('app.teacher_dashboard');
                angular.element('body').addClass('skin-blue ');
            }
        });
        $stateProvider.state('app.teacher_allcourses', {
            url: 'teacher/all-course',
            component: 'teacherAllcourse'
        });
        $stateProvider.state('app.teacher_all_master_file', {
            url: 'teacher/all-master-file',
            component: 'teacherAllMasterFile'
        });
        $stateProvider.state('app.teacher_all_master_file_module', {
            url: 'teacher/all-master-file-module',
            component: 'teacherAllMasterFileModule'
        });

        $stateProvider.state('app.teacher.module_view', {
            url: 'course/:course_id/module/view/:id',
            template: '<div class="form-group"><a href="javascript:void()" ui-sref="app.teacher.module_edit({course_id:$resolve.course_id,id: $resolve.id})" class="btn btn-primary">Edit Module</a></div><common-module-view on-click-refer="clickRefer($event)" module="$resolve.module" />',
            controller: function($scope, $state) {
                $scope.clickRefer = function(event) {
                    var doc = event.doc;
                    $state.go('app.teacher.module_view', {course_id: doc.course_id, id: doc.module_id});
                };
            },
            resolve: {
                id: function($stateParams) {return $stateParams.id;},
                course_id: function($stateParams) {return $stateParams.course_id;},
                module: function($http, config, $stateParams) {
                    return $http.get(config.api_teacher + 'module/' + $stateParams.id)
                        .then(function(rs) {
                            return rs.data.data;
                        });
                }
            }
        });

        $stateProvider.state('app.teacher.module_edit', {
            url: 'course/:course_id/module/edit/:id',
            template: '<div class="form-group"><a href="javascript:void()" ui-sref="app.teacher.module_view({course_id:$resolve.course_id,id: $resolve.id})" class="btn btn-primary">View Module</a></div><module-edit id="$resolve.id"></module-edit>',
            resolve: {
                course_id: function($stateParams) {return $stateParams.course_id;},
                id: function($stateParams) {return $stateParams.id;}
            }
        });

        $stateProvider.state('app.teacher.module_create', {
            url: 'course/:course_id/module/create',
            component: 'moduleNew'
        });

        $stateProvider.state('app.teacher.task_edit', {
            url: 'course/:course_id/module/:module_id/task/:id',
            template: '<task-form on-cancle="cancle()" on-delete="cancle()" modules="$resolve.modules" id="$resolve.id" hasmodule="$resolve.hasmodule"/>',
            controller: function ($scope, $state, $stateParams) {
                $scope.cancle = function () {
                    $state.go('app.teacher.course', {course_id: $stateParams.course_id});
                };
            },
            resolve: {
                hasmodule: function () {
                    return false;
                },
                modules: function ($http, $stateParams, config) {
                    return $http.get(config.api_teacher + 'module?course_id=' + $stateParams.course_id)
                        .then(function(rs) {
                            var data = [{title: 'Choose Module',id: 0}].concat(rs.data.data);
                            return data;
                        });
                },
                id: function($stateParams) {
                    return $stateParams.id;
                }
            }
        });

        $stateProvider.state('app.teacher.overview_lessions', {
            url: 'course/:course_id/overview/lessions',
            component: 'overviewLessions'
        });

        $stateProvider.state('app.teacher.master_file_config', {
            url: 'master-file/config/course/:course_id',
            template: '<master-file-config course="$resolve.course"/>',
            resolve: {
                course: function($http, $stateParams, config, teacherCache) {
                    if(angular.isUndefined(teacherCache.teacherSidebarCourses[$stateParams.course_id]))
                    {
                        return $http.get(config.api_teacher + 'post/' + $stateParams.course_id)
                            .then(function(rs) {
                                teacherCache.teacherSidebarCourses[$stateParams.course_id] = rs.data.data;
                                return rs.data.data;
                            });
                    }
                    else {
                        return teacherCache.teacherSidebarCourses[$stateParams.course_id];
                    }
                }
            }
        });

        $stateProvider.state('app.teacher_course', {
            url: 'teacher/course/:course_id/',
            templateUrl: 'app/teacher/theme/teacher-course-layout.html',
            controller: function() {
                angular.element('body').addClass('skin-blue sidebar-collapse');
            },
            resolve: {
                course: function($http, $stateParams, config) {
                    return $http.get(config.api_teacher + 'post/' + $stateParams.course_id)
                        .then(function(rs) {
                            return rs.data.data;
                        });
                }
            }
        });

        $stateProvider.state('app.teacher_course.edit', {
            url: 'teacher/course/:course_id/edit',
            templateUrl: 'app/teacher/theme/course-form.html',
            controller: function($scope, $element, TeacherApi, $stateParams) {
                angular.element('body').addClass('skin-blue sidebar-collapse');
                TeacherApi.getCourse($stateParams.course_id).then(function(res) {
                    $scope.course = res.data.data;
                });
                $scope.save = function() {
                    $element.find('button').button('loading');
                    TeacherApi.updateCourse($scope.course.id, $scope.course).then(function(res) {
                        var course = res.data.data;
                        $scope.course = course;
                        $scope.$emit('message:success', course.title + ' Update Success');
                        $element.find('button').button('reset');
                    }, function() {
                        $scope.$emit('message:error', 'Something Went Wrong?');
                        $element.find('button').button('reset');
                    });
                };
            }
        });

        $stateProvider.state('app.teacher_course.dashboard', {
            url: 'dashboard',
            templateUrl: 'app/teacher/theme/teacher-course-dashboard.html'
        });

        $stateProvider.state('app.teacher_course.master_file_config', {
            url: 'master-file/config',
            template: '<master-file-config course="$resolve.course"/>'
        });

        $stateProvider.state('app.teacher_course.submission', {
            url: 'submission-task?page',
            template: '<submission-group-user-tasks page="$resolve.page"/>',
            resolve: {
                page: function($stateParams) {
                    if($stateParams.page && /\d+/.test($stateParams.page))
                        return $stateParams.page;
                    return 1;
                }
            }
        });
        $stateProvider.state('app.teacher_course.final_assignment_submission', {
            url: 'final-assignment-submission',
            template: '<teacher-final-assignment-submission course-id="::$resolve.course.id" />'
        });
        $stateProvider.state('app.teacher_course.submission_by_user', {
            url: 'submission-task/:id?page',
            template: '<submission-user-tasks-by-user user-id="$resolve.userId" page="$resolve.page"/>',
            resolve: {
                page: function($stateParams) {
                    return 1;
                },
                userId: function($stateParams) {
                    return $stateParams.id;
                }
            }
        });
        $stateProvider.state('app.teacher_course.submission_review', {
            url: 'submission/:id',
            template: '<submission-review-form on-cancle="cancle()" user-task-id="::id"/>',
            controller: function($state, $scope, $stateParams) {
                $scope.cancle = function() {
                    $state.go('app.teacher_course.submission', {course_id: $stateParams.course_id});
                };
                $scope.id = $stateParams.id;
            }
        });
        $stateProvider.state('app.teacher_course.create_module', {
            url: 'create-module',
            component: 'moduleNew'
        });

        $stateProvider.state('app.teacher_module', {
            url: 'teacher/course/:course_id/module/:module_id/',
            templateUrl: 'app/teacher/theme/teacher-module-layout.html',
            controller: function() {
                angular.element('body').addClass('skin-blue sidebar-collapse');
            },
            resolve: {
                course: function($http, $stateParams, config) {
                    return $http.get(config.api_teacher + 'post/' + $stateParams.course_id)
                        .then(function(rs) {
                            return rs.data.data;
                        });
                },
                module: function($http, config, $stateParams) {
                    return $http.get(config.api_teacher + 'module/' + $stateParams.module_id)
                        .then(function(rs) {
                            return rs.data.data;
                        });
                }
            }
        });
        $stateProvider.state('app.teacher_module.dashboard', {
            url: 'dashboard',
            templateUrl: 'app/teacher/theme/teacher-module-dashboard.html',
            resolve: {
                tasks: function ($stateParams, teacherService) {
                    return teacherService.listTaskByModule($stateParams.module_id).then(function(rs) {
                        return rs.data.data;
                    });
                }
            }
        });
        $stateProvider.state('app.teacher_module.edit', {
            url: 'edit',
            template: '<module-edit module="$resolve.moduleForm"></module-edit>',
            resolve: {
                moduleForm: function($http, config, $stateParams) {
                    return $http.get(config.api_teacher + 'module/' + $stateParams.module_id)
                        .then(function(rs) {
                            return rs.data.data;
                        });
                }
            }
        });
        $stateProvider.state('app.teacher_module.create_lo', {
            url: 'lo/create',
            template: '<teacher-lo-create module-id="$resolve.module.id" module="$resolve.module" />'
        });
        $stateProvider.state('app.teacher_module.create_task', {
            url: 'create-new-task',
            template: '<task-form on-cancle="cancle()" module-id="$resolve.module_id" only-create="true" module="$resolve.module" modules="$resolve.modules" task="$resolve.task" hasmodule="$resolve.hasmodule"/>',
            controller: function($scope, $state, $stateParams) {
                $scope.cancle = function() {
                    $state.go('app.teacher_module.dashboard', {course_id: $stateParams.course_id,module_id: $stateParams.module_id});
                }
            },
            resolve: {
                hasmodule: function () {
                    return false;
                },
                module: function($stateParams) {
                    return {
                        id: $stateParams.module_id
                    }
                },
                module_id: function($stateParams) {
                    return $stateParams.module_id;
                },
                modules: function ($http, $stateParams, config) {
                    return $http.get(config.api_teacher + 'module?limit=50&course_id=' + $stateParams.course_id)
                        .then(function(rs) {
                            var data = [{title: 'Choose Module',id: 0}].concat(rs.data.data);
                            return data;
                        });
                },
                task: function($stateParams) {
                    return {
                        title: null,
                        post_id: $stateParams.module_id,
                        keywords: '',
                        preference: [],
                        preference_text: '',
                        question: '',
                        type: 'assignment',
                        lo_id: 0,
                        sort: ''
                    }
                }
            }
        });
        $stateProvider.state('app.teacher_module.create_task_merit', {
            url: 'create-new-task-merit',
            template: '<task-form on-cancle="cancle()" module-id="$resolve.module_id" only-create="true" module="$resolve.module" modules="$resolve.modules" task="$resolve.task" hasmodule="$resolve.hasmodule"/>',
            controller: function($scope, $state, $stateParams) {
                $scope.cancle = function() {
                    $state.go('app.teacher_module.dashboard', {course_id: $stateParams.course_id,module_id: $stateParams.module_id});
                }
            },
            resolve: {
                hasmodule: function () {
                    return false;
                },
                module: function($stateParams) {
                    return {
                        id: $stateParams.module_id
                    }
                },
                module_id: function($stateParams) {
                    return $stateParams.module_id;
                },
                modules: function ($http, $stateParams, config) {
                    return $http.get(config.api_teacher + 'module?limit=50&course_id=' + $stateParams.course_id)
                        .then(function(rs) {
                            var data = [{title: 'Choose Module',id: 0}].concat(rs.data.data);
                            return data;
                        });
                },
                task: function($stateParams) {
                    return {
                        title: null,
                        post_id: $stateParams.module_id,
                        keywords: '',
                        preference: [],
                        preference_text: '',
                        question: '',
                        type: 'assignment',
                        type_of_grade: 'merit',
                        lo_id: 0,
                        sort: ''
                    }
                }
            }
        });
        $stateProvider.state('app.teacher_module.create_task_diction', {
            url: 'create-new-task-diction',
            template: '<task-form on-cancle="cancle()" module-id="$resolve.module_id" only-create="true" module="$resolve.module" modules="$resolve.modules" task="$resolve.task" hasmodule="$resolve.hasmodule"/>',
            controller: function($scope, $state, $stateParams) {
                $scope.cancle = function() {
                    $state.go('app.teacher_module.dashboard', {course_id: $stateParams.course_id,module_id: $stateParams.module_id});
                }
            },
            resolve: {
                hasmodule: function () {
                    return false;
                },
                module: function($stateParams) {
                    return {
                        id: $stateParams.module_id
                    }
                },
                module_id: function($stateParams) {
                    return $stateParams.module_id;
                },
                modules: function ($http, $stateParams, config) {
                    return $http.get(config.api_teacher + 'module?limit=50&course_id=' + $stateParams.course_id)
                        .then(function(rs) {
                            var data = [{title: 'Choose Module',id: 0}].concat(rs.data.data);
                            return data;
                        });
                },
                task: function($stateParams) {
                    return {
                        title: null,
                        post_id: $stateParams.module_id,
                        keywords: '',
                        preference: [],
                        preference_text: '',
                        question: '',
                        type: 'assignment',
                        type_of_grade: 'diction',
                        lo_id: 0,
                        sort: ''
                    }
                }
            }
        });
        $stateProvider.state('app.teacher_module.edit_task', {
            url: 'task/:id/edit',
            template: '<task-form on-cancle="cancle()" on-delete="cancle()" modules="$resolve.modules" id="$resolve.id" hasmodule="$resolve.hasmodule"/>',
            controller: function ($scope, $state, $stateParams) {
                $scope.cancle = function () {
                    $state.go('app.teacher_module.dashboard', {course_id: $stateParams.course_id,module_id: $stateParams.module_id});
                };
            },
            resolve: {
                hasmodule: function () {
                    return false;
                },
                moduleId: function($stateParams) {
                    return $stateParams.module_id;
                },
                modules: function ($http, $stateParams, config) {
                    return $http.get(config.api_teacher + 'module?limit=50&course_id=' + $stateParams.course_id)
                        .then(function(rs) {
                            var data = [{title: 'Choose Module',id: 0}].concat(rs.data.data);
                            return data;
                        });
                },
                id: function($stateParams) {
                    return $stateParams.id;
                }
            }
        });
        $stateProvider.state('app.teacher_profile', {
            url: 'profile',
            component: 'teacherProfile'
        })
    });
    app.component('modules', {
        templateUrl: 'app/teacher/theme/modules.html',
        controller: function($rootScope, $element, $http, $window, $compile, vlNotify, $stateParams, $sce, config, vlHandleErrors, notifications) {
            var scope = this,
                page = $stateParams.page || 1,
                limit = 10;
            scope.$onInit = function() {
                scope.modules = [];
                scope.links = '';
                scope.total = 0;
                scope.check = 0;
                scope.selectModuleId = null;
                loadList(page);
            };

            function loadList(page) {
                vlNotify.show('#modules');
                $http.get(config.api_teacher + 'module?page=' + page + '&limit=' + limit)
                    .then(handleLoad, vlHandleErrors);
            }

            function handleLoad(rs) {
                scope.modules = rs.data.data;
                scope.links = $sce.trustAsHtml(rs.data.links);
                scope.total = rs.data.total;
                vlNotify.hide('#modules');
            }
            scope.pagination = function(page) {
                loadList(page);
            };
            scope.delete = function(id, index) {
                if($window.confirm('Are you sure delete?'))
                    $http.delete(config.api_teacher + 'module/' + id)
                        .then(function() {
                            notifications.showSuccess({message: 'Delete successfull'});
                            scope.modules.splice(index, 1);
                        });
            };
            scope.edit = function(id) {
                $http.get(config.api_teacher + 'post/' + id)
                    .then(function(rs) {
                        var module = rs.data.data,
                            newSope = $rootScope.$new(true);
                        newSope.module = module;
                        $element.find('#form-' + id).html($compile('<module-form module="module"/>')(newSope));
                    });
            }
        }
    });
    app.component('moduleNew', {
        template: '<module-form module="$ctrl.module" on-save="$ctrl.save($event)" on-back="$ctrl.onBack()"></module-form>',
        controller: function($http, $element, $state, $stateParams, config, notifications, vlHandleErrors) {
            var scope = this,
                course_id = null;
            scope.$onInit = function() {
                course_id = $stateParams.course_id;
                scope.module = {
                    title: null,
                    slug: null,
                    parent_id: course_id,
                    image: '',
                    postmeta: []
                };
            };
            scope.save = function(event) {
                $element.find('button.button-submit').button('loading');
                $http.post(config.api_teacher + 'counrse/' + course_id + '/module', event.module)
                    .then(function(rs) {
                        var module = rs.data.data;
                        notifications.showSuccess({message: module.title + ' Created Successfull'});
                        $state.go('app.teacher_module.edit', {course_id: course_id, module_id: module.id});
                    }, function() {
                        notifications.showError({message: 'Please Check Full Fill'});
                    })
                    .then(function() {
                        $element.find('button.button-submit').button('reset');
                    });
            };
        }
    });
    app.component('moduleEdit', {
        template: '<module-form ng-if="$ctrl.module" module="$ctrl.module" on-save="$ctrl.save($event)" on-delete="$ctrl.onBack()" on-back="$ctrl.onBack()"></module-form>',
        bindings: {
            module: '<',
            id: '<'
        },
        controller: function($state, $element, $http, $stateParams, config, notifications) {
            var scope = this;

            scope.$onInit = function() {
                if(angular.isUndefined(scope.module))
                    getModule();
            };

            scope.save = function(event) {
                $element.find('button.button-submit').button('loading');
                $http.put(config.api_teacher + 'module/' + scope.module.id, event.module)
                    .then(function() {
                        notifications.showSuccess({message: 'Update Module Successfull'});
                    }, function() {
                        notifications.showError({message: 'Please Check Full Fill'});
                    })
                    .then(function() {
                        $element.find('button.button-submit').button('reset');
                    });
            };
            scope.onBack = function() {
                $state.go('app.teacher.course', {course_id: $stateParams.course_id});
            };

            function getModule() {
                $http.get(config.api_teacher + 'module/' + scope.id)
                    .then(function(rs) {
                        scope.module = rs.data.data;
                    });
            }
        }
    });
    app.component('moduleForm', {
        templateUrl: 'app/teacher/theme/module-form.html',
        bindings: {
            module: '<',
            onSave: '&',
            onDelete: '&',
            onUpdate: '&',
            onBack: '&'
        },
        controller: function($window, $http, $stateParams, parseMetadataFilter, config, vlNotify, teacherCache, notifications) {
            var scope = this;

            function loadModules() {
                $http.get(config.api_teacher + 'module?limit=50&course_id=' + scope.course_id)
                    .then(function(rs) {
                        scope.modules = [{id: 0, title: 'Choose Lession'}].concat(rs.data.data);
                    }, function() {
                        scope.modules = [{id: 0, title: 'Choose Lession'}];
                    });
            }

            scope.$onInit = function() {
                scope.counrses = [];
                scope.metadata = {};
                scope.course_id = $stateParams.course_id;
                if(!scope.module.parent_id)
                    scope.module.parent_id = 0;
                var postmeta = parseMetadataFilter(scope.module.postmeta);
                scope.metadata.document = postmeta.document ? JSON.parse(postmeta.document) : [];
                delete scope.module.postmeta;

                loadModules();
            };
            scope.save = function() {
                scope.module.postmeta = scope.metadata;
                teacherCache.teacherSidebarModules = {};

                scope.onSave({
                    $event: {
                        module: scope.module
                    }
                });
            };

            scope.delete = function() {
                if(!$window.confirm('Are Your Sure Delete?'))
                    return;
                vlNotify.show('#module-form');
                $http.delete(config.api_teacher + 'module/' + scope.module.id)
                    .then(function() {
                        if(scope.onDelete)
                            scope.onDelete();
                        notifications.showSuccess({message: 'Delete Successfull'});
                        teacherCache.teacherSidebarModules = {};
                        vlNotify.hide();
                    });
            };

            scope.back = function() {
                if(scope.onBack)
                    scope.onBack();
            };
        }
    });
    app.component('teacherTaskList', {
        bindings: {
            module: '<'
        },
        templateUrl: 'app/teacher/theme/teacher-task-list.html',
        controller: function ($http, $attrs, $sce, $window, TeacherApi, config, notifications) {
            var scope = this;
            scope.tasks = [];
            scope.links = '';
            scope.total = 0;

            function load(page) {
                if($attrs.merit) {
                    return $http.get(config.api_teacher + 'module/' + scope.module.id + '/task?limit=30&type_of_grade=merit&page=' + page)
                        .then(function(rs) {
                            scope.tasks = rs.data.data;
                            scope.links = $sce.trustAsHtml(rs.data.links);
                            scope.total = rs.data.total;
                        });

                }

                if($attrs.diction) {
                    return $http.get(config.api_teacher + 'module/' + scope.module.id + '/task?limit=30&type_of_grade=diction&page=' + page)
                        .then(function(rs) {
                            scope.tasks = rs.data.data;
                            scope.links = $sce.trustAsHtml(rs.data.links);
                            scope.total = rs.data.total;
                        });

                }

                return $http.get(config.api_teacher + 'module/' + scope.module.id + '/task?limit=30&page=' + page)
                    .then(function(rs) {
                        scope.tasks = rs.data.data;
                        scope.links = $sce.trustAsHtml(rs.data.links);
                        scope.total = rs.data.total;
                    });

            }

            scope.$onInit = function() {
                load(1);
            };

            scope.paginate = function(page) {
                load(page);
            };

            scope.delete = function(id, index, title) {
                if($window.confirm('Confirm Delete Task ' + title)) {
                    TeacherApi.deleteTask(scope.module.id, id).then(function() {
                        scope.tasks.splice(index, 1);
                    });
                }
            }
        }
    });
    app.component('moduleTasks', {
        bindings: {
            moduleId: '<'
        },
        templateUrl: 'app/teacher/theme/module-tasks.html',
        controller: function(TeacherApi, $window, $http, $sce, config, notifications, vlHandleErrors, vlNotify, vlHtmlCommon) {
            var scope = this;
            scope.$onInit = function() {
                scope.tasks = [];
                scope.links = '';
                scope.total = 0;
                scope.task = {};
                scope.module = {};
            };
            scope.$onChanges = function(changesObj) {
                if(changesObj.moduleId.currentValue)
                {
                    vlNotify.show('#module-tasks');
                    TeacherApi.getModule(scope.moduleId)
                        .then(function(rs) {scope.module = rs.data.data})
                        .then(function() {
                            loadList(1);
                        });
                }
            };
            scope.onSelectTask = function(id) {
                vlNotify.show('#module-tasks');
                TeacherApi.getTask(scope.module.id, id)
                    .then(function(rs) {scope.task = rs.data.data;})
                    .then(function() {
                        angular.element('#module-tasks a[href="#tab_task_info"]').tab('show');
                        vlNotify.hide();
                    });
            };
            scope.onCancleInfoTask = function(event) {
                scope.task = {};
                angular.element('#module-tasks a[href="#tab_tasks"]').tab('show');
            };
            scope.delete = function(id, index) {
                if($window.confirm('Are you sure delete?'))
                    $http.delete(config.api_teacher + 'module/' + scope.module.id + '/task/' + id)
                        .then(function() {
                            notifications.showSuccess({message: 'Delete successfull'});
                            scope.tasks.splice(index, 1);
                        });
            };

            function handleResponseList(rs) {
                scope.tasks = rs.data.data;
                scope.links = $sce.trustAsHtml(rs.data.links);
                scope.total = rs.data.total;
                vlNotify.hide();
            }

            function loadList(page) {
                $http.get(config.api_teacher + 'module/' + scope.moduleId + '/task?page=' + page)
                    .then(handleResponseList);
            }
        }
    });
    app.component('taskForm', {
        bindings: {
            onlyCreate: '<',
            module: '<',
            moduleId: '<',
            task: '<',
            id: '<',
            modules:'<',
            hasmodule: '<',
            onCancle: '&',
            onDelete: '&'
        },
        templateUrl: 'app/teacher/theme/task-form.html',
        controller: function (teacherCache, TeacherApi, $element, $stateParams, $window, $http, helperFunc, config, notifications, vlNotify, vlCommonObject, textToArrayFilter, arrayToTextFilter) {
            var scope  = this;

            scope.$onInit = function() {
                if(!scope.modules)
                    TeacherApi.getModules(1).then(function(rs) {scope.modules = rs.data.data;});
                if(!scope.task)
                    if(!scope.id) {
                        //scope.task = vlCommonObject.taskObjectInit;
                    } else {
                        loadTaskId($stateParams.module_id, $stateParams.id);
                    }

                if(scope.moduleId )
                    scope.module_id = parseInt(scope.moduleId);
                else scope.module_id = 0;

                scope.course_id = $stateParams.course_id;
                scope.los = [{id: 0, title: 'Choose Learning Outcome Scenarios'}];

                TeacherApi.listLo($stateParams.module_id)
                    .then(
                        function(res) {
                            var data = res.data.data;
                            if(data[0]) {
                                scope.los = scope.los.concat(data);
                            }
                        },
                        function(res) {}
                    )
            };
            scope.types = [{key: 'assignment', label: 'Assignment'}, {key: 'mcq', label: 'MCQ'}];
            scope.save = function() {
                $element.find('button.button-submit').button('loading');
                function handleErrors(res) {
                    if(res.status !== 400) return;
                    var message = res.data.messages;
                    angular.forEach(message, function(mess) {
                        helperFunc.notifyError(mess[0]);
                    });
                }
                if(scope.task.id)
                    $http.put(config.api_teacher + 'module/' + scope.module_id + '/task/' + scope.task.id, scope.task)
                        .then(function(rs) {
                            notifications.showSuccess({message: 'Update Task Successfull'});
                            scope.task = rs.data.data;
                            scope.task.preference_text = arrayToTextFilter(scope.task.preference);
                            teacherCache.teacherSidebarModules = {};
                            if(scope.onlyCreate) resetForCreate();
                            vlNotify.hide();
                        }, handleErrors)
                        .then(function() {
                            $element.find('button.button-submit').button('reset');
                        });
                else
                    $http.post(config.api_teacher + 'module/' + scope.module_id + '/task', scope.task)
                        .then(function(rs) {
                            notifications.showSuccess({message: 'Add Task Successfull'});
                            scope.task = rs.data.data;
                            scope.task.preference_text = arrayToTextFilter(scope.task.preference);
                            teacherCache.teacherSidebarModules = {};
                            if(scope.onlyCreate) resetForCreate();
                            vlNotify.hide();
                        }, handleErrors)
                        .then(function() {
                            $element.find('button.button-submit').button('reset');
                        });
            };
            scope.cancle = function() {
                scope.onCancle({
                    $event: {
                        task: scope.task
                    }
                });
            };
            scope.delete = function() {
                if(!$window.confirm('Are Your Sure Delete?'))
                    return;
                vlNotify.show('#task-form');
                $http.delete(config.api_teacher + 'module/' + scope.task.post_id + '/task/' + scope.task.id)
                    .then(function() {
                        if(scope.onDelete)
                            scope.onDelete();
                        notifications.showSuccess({message: 'Delete Successfull'});
                        teacherCache.teacherSidebarModules = {};
                        vlNotify.hide();
                    });
            };
            function resetForCreate() {
                scope.task = vlCommonObject.taskObjectInit;
            }

            function loadTaskId(module_id, id) {
                vlNotify.show('#task-form');
                $http.get(config.api_teacher + 'module/' + module_id + '/task/' +  id)
                    .then(function(rs) {
                        var task = rs.data.data;
                        //task.preference_text = arrayToTextFilter(task.preference);
                        if(!task.preference)
                            task.preference = [];
                        scope.task = task;
                        scope.module_id = parseInt(task.post_id);
                        vlNotify.hide();
                    });
            }
        }
    });
    app.component('mqcForm', {
        bindings: {
            task: '<'
        },
        templateUrl: 'app/teacher/theme/mcq-form.html',
        controller: function($http, $element,config, notifications, vlHandleErrors) {
            var scope = this;
            scope.$onInit = function() {
                scope.questions = [
                    {
                        title: '',
                        answer: '',
                        optionText: 'Option One\nOption Two\nOption Three\nOption Four'
                    }
                ];
                $http.get(config.api_teacher + 'task/' + scope.task.id + '/mcq')
                    .then(function(rs) {
                        if(rs.data.data.length) {
                            scope.questions = [];
                            angular.forEach(rs.data.data, function(obj) {
                                scope.questions.push({
                                    title: obj.title,
                                    answer: obj.answer,
                                    optionText: convertOptionsToText(obj.options)
                                });
                            });
                        }
                    });
            };

            scope.save = function() {
                var form = [];
                angular.forEach(scope.questions, function(ques) {
                    form.push({
                        title: ques.title,
                        options: convertOptionTextToOption(ques.optionText),
                        answer: ques.answer
                    });
                });
                if(form.length) {
                    $element.find('button.button-submit').button('loading');
                    $http.post(config.api_teacher + 'task/' + scope.task.id + '/mcq/store-array', {options: form})
                        .then(function(rs) {
                            notifications.showSuccess('Update MQC Form Successfull');
                        }, vlHandleErrors)
                        .then(function() {
                            $element.find('button.button-submit').button('reset');
                        });
                }
            };

            function convertToSlug(text) {
                return text.toString().toLowerCase()
                    .replace(/\s+/g, '-')
                    .replace(/[^\w\-]+/g, '')
                    .replace(/\-\-+/g, '-')
                    .replace(/^-+/, '')
                    .replace(/-+$/, '');
            }

            /**
             * return Key, value
             **/
            function convertOptionTextToOption(text) {
                var obj = [];
                text = text.trim('\n');
                angular.forEach(text.split('\n'), function(value) {
                    if(value.length)
                        obj.push({
                            key: convertToSlug(value),
                            title: value
                        });
                });
                return obj;
            }
            function convertOptionsToText(options) {
                var s = '';
                angular.forEach(options, function(obj) {
                    s = s.concat('\n', obj.title);
                    s = s.trim('\n');
                });

                return s;
            }
        }
    });
    app.component('taskIndex', {
        templateUrl : 'app/teacher/theme/tasks-index.html',
        controller: function ($http, $sce, $window, config) {
            var scope = this;
            scope.tasks = [];
            scope.total = 0;
            scope.links = '';
            $http.get(config.api_teacher + 'task')
                .then(function (rs) {
                    var res = rs.data;
                    scope.tasks = res.data;
                    scope.total = res.total || 0;
                    scope.links = res.links ? $sce.trustAsHtml(res.links) : '';
                });
        }
    });
    app.component('teacherDashboard', {
        templateUrl: 'app/teacher/theme/teacher-dashboard.html',
        controller: function($http, $anchorScroll, config) {
            var scope = this;
            scope.$onInit = function() {
                scope.postView = null;
            };

            scope.viewPost = function(event) {
                $http.get(config.api_teacher + 'post/' + event.id)
                    .then(function(rs) {
                        scope.postView = rs.data.data;
                        setTimeout(function() {
                            $anchorScroll('common-post-view');
                        }, 100);
                    }, function() {
                        scope.postView = null;
                    });
            };

            scope.closeViewPost = function() {
                scope.postView = null;
            }
        }
    });
    app.component('teacherCourses', {
        templateUrl: 'app/teacher/theme/teacher-courses.html',
        controller: function($http, config, vlNotify, teacherCache) {
            var scope = this;
            scope.$onInit = function() {
                loadList();
            };

            function loadList() {
                if(teacherCache.courses) {
                    scope.courses = teacherCache.courses;
                } else {
                    vlNotify.show('#teacher-courses');
                    $http.get(config.api_teacher + 'my/course?limit=40')
                        .then(function(rs) {
                            scope.courses = rs.data.data;
                            teacherCache.courses = rs.data.data;
                            vlNotify.hide('#teacher-courses');
                        });
                }
            }
        }
    });
    app.component('teacherSidebar', {
        templateUrl: 'app/teacher/theme/teacher-sidebar.html',
        controller: function($timeout, $element, $stateParams, $http, config, teacherCache) {
            var scope = this;
            scope.$onInit = function() {
                scope.course_id =  $stateParams.course_id || 0;
                loadModule();
                loadUserInfo();
                loadCourse();
            };

            function loadUserInfo() {
                if(angular.isUndefined(teacherCache.teacherSidebarUser))
                {
                    $http.get(config.api_teacher + 'user-info')
                        .then(function(rs) {
                            scope.user = rs.data.data;
                            teacherCache.teacherSidebarUser = rs.data.data;

                        });
                }
                else {
                    scope.user = teacherCache.teacherSidebarUser;
                }
            }

            function loadModule() {
                if(angular.isUndefined(teacherCache.teacherSidebarModules[scope.course_id]))
                {
                    $http.get(config.api_teacher + 'module?with-tasks=1&course_id=' + scope.course_id)
                        .then(function(rs) {
                            scope.modules = rs.data.data;
                            teacherCache.teacherSidebarModules[scope.course_id] = rs.data.data;
                            niceScrollEditor();
                        });
                }
                else {
                    scope.modules = teacherCache.teacherSidebarModules[scope.course_id];
                    niceScrollEditor();
                }
            }

            function loadCourse() {
                if(angular.isUndefined(teacherCache.teacherSidebarCourses[scope.course_id]))
                {
                    $http.get(config.api_teacher + 'post/' + scope.course_id)
                        .then(function(rs) {
                            scope.course = rs.data.data;
                            teacherCache.teacherSidebarCourses[scope.course_id] = rs.data.data;
                            angular.element('h1#page-title span').text(scope.course.title);
                        });
                }
                else {
                    scope.course = teacherCache.teacherSidebarCourses[scope.course_id];
                    angular.element('h1#page-title span').text(scope.course.title);
                }
            }

            function niceScrollEditor() {
                $timeout(function() {
                    $element.find('#sidebar-menu').css('display', 'block');
                    $element.find('#sidebar-menu').slimScroll({
                        position: 'right',
                        height: '600px',
                        railVisible: true,
                        //alwaysVisible: true,
                        color: '#00f'
                    });
                }, 100);
            }
        }
    });
    app.component('submission', {
        templateUrl : 'app/teacher/theme/submission.html',
        controller: function($http, $sce, config) {

        }
    });
    app.component('submissionUserTasks', {
        templateUrl : 'app/teacher/theme/submission-user-task.html',
        bindings: {
            onSelectReview: '&',
            page: '<'
        },
        controller: function($state, $sce, $http, $stateParams, config, vlNotify) {
            var scope = this;
            scope.$onInit = function() {
                loadList(scope.page);
                scope.course_id = $stateParams.course_id;
            };

            scope.pagination = function(page) {$state.go('.', {page: page});};

            function loadList(page) {
                vlNotify.show('#submission-user-tasks');
                $http.get(config.api_teacher + 'user-task/group?page=' + page + '&course_id=' + $stateParams.course_id)
                    .then(handleResponseList);
            }

            function handleResponseList(rs) {
                scope.userTasks = rs.data.data;
                scope.links = $sce.trustAsHtml(rs.data.links);
                scope.total = rs.data.total;
                vlNotify.hide();
            }
        }
    });
    app.component('submissionUserTasksByUser', {
        templateUrl : 'app/teacher/theme/submission-user-task-by-user.html',
        bindings: {
            userId: '<',
            onSelectReview: '&',
            page: '<'
        },
        controller: function($state, $sce, $http, $stateParams, config, vlNotify) {
            var scope = this;
            scope.$onInit = function() {
                loadList(scope.page);
                scope.course_id = $stateParams.course_id;
            };

            scope.pagination = function(page) {$state.go('.', {page: page});};

            function loadList(page) {
                vlNotify.show('#submission-user-tasks');
                $http.get(config.api_teacher + 'user-task/'+ scope.userId +'/group?page=' + page + '&course_id=' + $stateParams.course_id)
                    .then(handleResponseList);
            }

            function handleResponseList(rs) {
                scope.userTasks = rs.data.data;
                scope.user = rs.data.user;
                scope.links = $sce.trustAsHtml(rs.data.links);
                scope.total = rs.data.total;
                vlNotify.hide();
            }
        }
    });
    app.component('submissionGroupUserTasks', {
        templateUrl : 'app/teacher/theme/submission-group-user-task.html',
        bindings: {
            onSelectReview: '&',
            page: '<'
        },
        controller: function($state, $sce, $http, $stateParams, config, vlNotify, $localStorage) {
            var scope = this;
            scope.$onInit = function() {
                loadList(scope.page);
                scope.course_id = $stateParams.course_id;
            };

            scope.pagination = function(page) {$state.go('.', {page: page});};

            function loadList(page) {
                vlNotify.show('#submission-user-tasks');
                $http.get(config.api_teacher + 'group-user-task/group?page=' + page + '&course_id=' + $stateParams.course_id)
                    .then(handleResponseList);
            }

            function handleResponseList(rs) {
                scope.userTasks = rs.data.data;
                scope.links = $sce.trustAsHtml(rs.data.links);
                scope.total = rs.data.total;
                vlNotify.hide();
            }

            scope.viewDetail = function(item) {
                $state.go('app.teacher_course.submission_by_user', {
                  id: item.id,
                  course_id: scope.course_id
                });
            }

            scope.downloadExcel = function(item) {
                window.open(config.api + 'teacher/user-task/'+ item.id +'/download?course_id='+ scope.course_id +'&token=' + $localStorage.token);
            }
        }
    });
    app.component('submissionReviewForm', {
        templateUrl : 'app/teacher/theme/submission-user-form.html',
        bindings: {
            onCancle: '&',
            onSubmit: '&',
            userTaskId: '<'
        },
        controller: function($timeout, $element, $http, $sce, matchKeywordsFilter, userService, config, helperFunc, vlNotify, vlHandleErrors, notifications) {
            var scope = this;
            scope.$onInit = function() {
                scope.comments = [];
                scope.message = null;
                scope.grades = [{value: 'none', label: 'Choose Grade'},{value: 'pass', label: 'Pass'},{value: 'redo', label: 'Redo'},{value: 'merit', label: 'Merit'},{value: 'diction', label: 'Diction'}];
                scope.form = {
                    teacher_comment: '',
                    grade: 'none'
                };
                load();
                reloadComment();
            };

            scope.cancle = function() {
                if(scope.onCancle) scope.onCancle();
            };

            scope.submit = function() {
                if(scope.form.grade !== 'redo' && scope.form.teacher_comment.length <= 0) {
                    helperFunc.notifyError('Please fill Teacher Feedback');
                    return;
                }
                vlNotify.show('#submission-user-form');
                $http.put(config.api_teacher + 'user-task/' + scope.userTaskId, scope.form)
                    .then(function(rs) {
                        var data = rs.data.data;
                        scope.userTask = data;
                        scope.form.teacher_comment = data.teacher_comment;
                        scope.form.grade = data.grade;
                        notifications.showSuccess({message: 'Update Successfull'});
                        vlNotify.hide();
                    }, function(rs) {vlHandleErrors(rs); vlNotify.hide();});
                if(scope.onSubmit)
                    scope.onSubmit();
            };

            scope.send = function() {
                $element.find('button.button-send').button('loading');
                userService.postUserTaskComment(scope.userTaskId, scope.message).then(function(res) {
                    notifications.showSuccess({message: 'Send Message Success'});
                    reloadComment();
                    scope.message = null;
                }, function(res) {
                    notifications.showError({message: 'Something Went Wrong?'});
                })
                    .then(function() {
                        $element.find('button.button-send').button('reset');
                    })
            };

            function reloadComment() {
                userService.getUserTaskComment(scope.userTaskId).then(function(res) {
                    scope.comments = res.data.data;
                });
            }

            function load() {
                vlNotify.show('#submission-user-tasks');
                $http.get(config.api_teacher + 'user-task/' + scope.userTaskId)
                    .then(function(rs) {
                        var data = rs.data.data;
                        scope.userTask = angular.copy(data);
                        scope.form.teacher_comment = data.teacher_comment;
                        scope.form.grade = data.grade;
                        scope.answer = data.answer;
                        scope.totalMatch = matchKeywordsFilter(angular.copy(data.answer), data.task.keywords);
                        vlNotify.hide();
                        niceScrollEditor();
                    });
            }

            function niceScrollEditor() {
                $timeout(function() {
                    $element.find('#post-editor').css('display', 'block');
                    $element.find('#post-editor').slimScroll({
                        position: 'right',
                        height: '600px',
                        //railVisible: true,
                        alwaysVisible: true,
                        color: '#00f'
                    });
                }, 100);
            }
        }
    });
    app.component('overviewLessions', {
        templateUrl: 'app/teacher/theme/overview-lessions.html',
        controller: function($http, $stateParams, $sce, $anchorScroll, config) {
            var scope = this;

            function loadModules(page) {
                $http.get(config.api_teacher + 'module?limit=50&with-tasks-count=1&page=' + page + '&course_id=' + $stateParams.course_id)
                    .then(function(rs) {
                        scope.modules = rs.data.data;
                        scope.total = rs.data.total;
                        scope.links = $sce.trustAsHtml(rs.data.links);
                    });
            }

            scope.$onInit = function() {
                scope.modules = [];
                scope.total = 0;
                scope.links = '';
                scope.viewmodule = 0;
                scope.module = {};
                scope.course_id = $stateParams.course_id;
                loadModules(1);
            };

            scope.paginate= function(page) {
                loadModules(page);
            };
            scope.cancelViewModule = function() {
                scope.viewmodule = 0;
                scope.module = {};
            };
            scope.viewModule = function(id) {
                $http.get(config.api_teacher + 'module/' + id)
                    .then(function(rs) {
                        scope.module = rs.data.data;
                        scope.viewmodule = id;
                        setTimeout(function() {
                            $anchorScroll('module-view-' + id);
                        }, 100);
                    });
            };
        }
    });
    app.component('teacherModuleList', {
        templateUrl: 'app/teacher/theme/module-list.html',
        bindings: {course: '<'},
        controller: function($http, config) {
            var scope = this;

            scope.$onInit = function() {
                $http.get(config.api_teacher + 'module?limit=50&with-tasks-count=1&page=1&course_id=' + scope.course.id)
                    .then(function(rs) {
                        scope.modules = rs.data.data;
                    });
            };
        }
    });
    app.component('masterFileConfig', {
        templateUrl: "app/teacher/theme/teacher-master-file-config.html",
        bindings: {
            course: '<'
        },
        controller: function($scope, $http, config, vlNotify) {
            var scope = this;
            scope.$onInit = function(event) {
                scope.config = {};
                loadConfig();
            };
            scope.save = function(event) {
                vlNotify.show('#learner-courses');
                $http.post(config.api_teacher + 'master-file/config/course/' + scope.course.id, scope.config)
                    .then(function(rs) {
                        scope.config = rs.data.data;
                        $scope.$emit('message:success', 'Config Updated');
                    })
                    .then(function() {
                        vlNotify.hide('#learner-courses');
                    });
            };
            function loadConfig() {
                vlNotify.show('#learner-courses');
                $http.get(config.api_teacher + 'master-file/config/course/' + scope.course.id)
                    .then(function(rs) {
                        var data = rs.data.data;
                        if(!data[0])
                            scope.config = data;
                        vlNotify.hide('#learner-courses');
                    });
            }
        }
    });
    app.component("teacherNav", {
        templateUrl: 'app/teacher/theme/teacher-nav.html',
        controller: function(userService) {
            var scope = this;
            scope.$onInit = function() {
                userService.info().then(function(res) {
                   scope.user = res.data.data;
                });
            }

        }
    });
    app.component('teacherInfo', {
        templateUrl: 'app/teacher/theme/teacher-info.html',
        controller: function(userService) {
            var scope = this;
            scope.$onInit = function() {
                userService.info().then(function(res) {
                    scope.user = res.data.data;
                });
            }
        }
    });
    app.component('teacherListCourse', {
        templateUrl: 'app/teacher/theme/teacher-list-course.html',
        controller: function(TeacherApi) {
            var scope = this;
            scope.$onInit = function() {
                TeacherApi.getCourseList().then(function (res) {
                    scope.courses = res.data.data;
                });
            }
        }
    });
    app.component('teacherAllcourse', {
        templateUrl: 'app/teacher/theme/teacher-courses-component.html',
        controller: function($scope, $element, config, TeacherApi, helperFunc) {
            var scope = this;
            function load() {
                TeacherApi.getCourseList().then(function(res) {
                    var data = res.data.data;
                    if(data[0])
                      scope.courses = data;
                });
            }

            scope.$onInit = function() {
                scope.modal = 'none';
                scope.courses = [];
                scope.course = {};
                scope.base_uri = config.api;
                load();
                angular.element('body').addClass('skin-blue sidebar-collapse');
            };

            scope.edit = function(id) {
                scope.modal = 'none';
                TeacherApi.getCourse(id).then(function(res) {
                    scope.modal = 'update';
                    scope.course = res.data.data;
                    helperFunc.showModal('#modal-teacherAllcourse');
                });
            };

            scope.update = function(event) {
                var form = angular.copy(event.item),
                    id = event.item.id;
                delete form.id;
                $element.find('button.submit').button('loading');
                TeacherApi.updateCourse(id, form)
                    .then(function(res) {
                        var data = res.data.data;
                        $scope.$emit('message:success', data.title + ' Updated!');
                    })
                    .then(function() {
                        load();
                        $element.find('button.submit').button('reset');
                        helperFunc.hideModal('#modal-teacherAllcourse');
                        scope.modal = 'none';
                    });
            };
        }
    });
    app.component('teacherAllMasterFile', {
        templateUrl: 'app/teacher/theme/teacher-master-file-component.html',
        controller: function($scope, $http, $localStorage, $sce, $window, TeacherApi, config) {
            var scope = this;
            scope.tasks = [];
            scope.links = '';
            scope.total = 0;

            function load(page) {
                $http.get(config.api_teacher + 'master-file/list?limit=30&page=' + page)
                    .then(function(rs) {
                        scope.master_files = rs.data.data;
                        scope.links = $sce.trustAsHtml(rs.data.links);
                        scope.total = rs.data.total;
                    });

            }

            scope.$onInit = function() {
                load(1);
                angular.element('body').addClass('skin-blue sidebar-collapse');
            };

            scope.paginate = function(page) {
                load(page);
            };

            scope.download = function(user_id, course_id) {
                $window.open (
                    config.api_teacher + 'master-file/download/user/' + user_id + '/course/' + course_id + '?token=' + $localStorage.token,
                    '_blank'
                );
            }
        }
    });
    app.component('teacherAllMasterFileModule', {
        templateUrl: 'app/teacher/theme/teacher-master-file-module.html',
        controller: function($scope, $http, $localStorage, $sce, $window, TeacherApi, config) {
            var scope = this;
            scope.tasks = [];
            scope.links = '';
            scope.total = 0;

            function load(page) {
                $http.get(config.api_teacher + 'master-file/list-module?limit=30&page=' + page)
                    .then(function(rs) {
                        scope.master_files = rs.data.data;
                        scope.links = $sce.trustAsHtml(rs.data.links);
                        scope.total = rs.data.total;
                    });

            }

            scope.$onInit = function() {
                load(1);
                angular.element('body').addClass('skin-blue sidebar-collapse');
            };

            scope.paginate = function(page) {
                load(page);
            };

            scope.download = function(user_id, course_id, type) {
                $window.open (
                    config.api_teacher + 'master-file/download/user/' + user_id + '/module/' + course_id + '/type/'+ type +'?token=' + $localStorage.token,
                    '_blank'
                );
            }

            scope.downloadall = function(user_id, course_id) {
                $window.open (
                    config.api_teacher + 'master-file/download/user/' + user_id + '/module/' + course_id + '/all-file?token=' + $localStorage.token,
                    '_blank'
                );
            }
        }
    });
    app.component('teacherProfile', {
        //templateUrl: 'app/teacher/theme/profile.html',
        template: '<item-form template="app/teacher/theme/profile.html" item="$ctrl.user" on-submit="$ctrl.submit($event)"/>',
        controller: function($scope, userService) {
            var scope = this;
            scope.$onInit = function() {
                userService.info().then(function(res) {
                    scope.user = res.data.data;
                });
                angular.element('body').addClass('skin-blue sidebar-collapse');
            }

            scope.submit = function(event) {
                var user = event.item;
                userService.profile(user).then(function(res) {
                    $scope.$emit('message:success', 'Update Profile Successfull');
                }, function(res) {
                    if(res.status !== 400) return;
                    var messages = res.data.messages;
                    angular.forEach(messages, function(message) {
                        angular.forEach(message, function(mess) {
                            $scope.$emit('message:error', mess);
                        });
                    });
                });
            }
        }
    });
    app.component('teacherLoCreate', {
        bindings: {
            module: '<',
            moduleId: '<',
            onCreate: '&'
        },
        template: '<item-form item="$ctrl.item" on-submit="$ctrl.submit($event)" template="app/teacher/theme/lo-form.html"/>',
        controller: function($element, $scope, TeacherApi) {
            var scope = this;
            scope.$onInit = function() {
                scope.item = {};
            }
            scope.submit = function(e) {
                var form = e.item;
                form.module_id = scope.moduleId;
                TeacherApi.createLo(form)
                    .then(
                        function(res) {
                            var lo = res.data.data;
                            scope.item = {};
                            $scope.$emit('message:success', lo.title + ' Create Success');
                            if(scope.onCreate) {
                                scope.onCreate();
                            }
                        },
                        function(res) {
                            $scope.$emit('message:error', 'Full Fill Field Required');
                        }
                    );
            }
        }
    });
    
    function teacherFinalAssignmentSubmissionController($window, TeacherApi) {
        var scope = this;

        function handleResponse(res) {
            scope.items = [];
            scope.next_page_url = null;
            scope.prev_page_url = null;

            if(! res.data.data[0])
                return;
            scope.items = res.data.data;
            scope.next_page_url = res.data.next_page_url;
            scope.prev_page_url = res.data.prev_page_url;
        }

        function load() {
            
        }
        
        scope.$onInit = function () {
            TeacherApi.listFinalAssignmentSubmission(scope.courseId)
                .then(handleResponse);
        }
        
        scope.paginate = function(url) {

        }
        
        scope.download = function (userModuleID) {
            var url = TeacherApi.getUrlDownloadFinalAssignmentSubmission(userModuleID);

            $window.open(url, "_blank");
        }
    }
    app.component('teacherFinalAssignmentSubmission', {
        bindings: {courseId: "<"},
        templateUrl: function ($attrs) {
            return $attrs.templateUrl || 'app/teacher/theme/final-assignment-submission-list.html';
        },
        controller: teacherFinalAssignmentSubmissionController
    });

})(window.angular);
