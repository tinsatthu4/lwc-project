(function(angular) {
var app = angular.module('vlCommonComponent', ["helper"]);
    app.component('vlNewsEventsAnnouncements', {
        templateUrl: 'app/common/theme/widgets/news-event-annoument.html',
        bindings: {
          onClick: '&'
        },
        controller: function($http, vlNotify, commonCache) {
            var scope = this;

            function load() {
                if(commonCache.news && commonCache.announcements && commonCache.events) {
                    scope.news = commonCache.news;
                    scope.announcements = commonCache.announcements;
                    scope.events = commonCache.events;
                } else {
                    vlNotify.show('#vlNewsEventsAnnouncements-news');
                    vlNotify.show('#vlNewsEventsAnnouncements-events');
                    vlNotify.show('#vlNewsEventsAnnouncements-announcements');
                    $http.get('/api/news')
                        .then(function(rs) {
                            scope.news = rs.data.data;
                            commonCache.news = rs.data.data;
                            vlNotify.hide('#vlNewsEventsAnnouncements-news');
                        });
                    $http.get('/api/announcements')
                        .then(function(rs) {
                            scope.announcements = rs.data.data;
                            commonCache.announcements = rs.data.data;
                            vlNotify.hide('#vlNewsEventsAnnouncements-announcements');
                        });
                    $http.get('/api/event')
                        .then(function(rs) {
                            scope.events = rs.data.data;
                            commonCache.events = rs.data.data;
                            vlNotify.hide('#vlNewsEventsAnnouncements-events');
                        });
                }
            }

            scope.$onInit = function() {
                load();
            };

            scope.click = function(id) {
                scope.onClick({
                    $event: {
                        id: id
                    }
                });
            };
        }
    });
    app.component('commonModuleView', {
        bindings: {
            module: '<',
            onClickRefer: '&',
            onCancel: '&'
        },
        templateUrl: function($attrs) {
            return 'app/common/theme/' + ($attrs.template || 'common-module-view.html');
        },
        controller: function($element, $timeout, $parse, $sce, parseMetadataFilter) {
            var scope = this;
            scope.$onInit = function() {
                var postmeta = parseMetadataFilter(scope.module.postmeta);
                if(postmeta.document)
                    scope.docs = JSON.parse(postmeta.document);
                else
                    scope.docs = [];
                scope.content = $sce.trustAsHtml(scope.module.content);
                niceScrollBody();
            };

            scope.clickRefer = function(doc) {
                if(scope.onClickRefer)
                    scope.onClickRefer({
                        $event: {
                            doc: doc
                        }
                    });
            };
            scope.cancel = function() {
                if(scope.onCancel)
                    scope.onCancel();
            };

            function niceScrollBody() {
                $timeout(function() {
                    $element.find('#common-module-view div.post-editor').slimScroll({
                        position: 'right',
                        height: '600px',
                        railVisible: true,
                        color: '#00f'
                    });
                }, 100);
            }
        }
    });
    app.component('commonModulePreference', {
        bindings: {
            module: '<'
        },
        template: '<div class="box box-widget"><div class="box-header with-border"><h3 class="box-title">Module Reference link</h3></div><div class="box-body no-padding"><table class="table table-striped"><tbody><tr ng-repeat="doc in ::$ctrl.docs"><td> <a ng-if="doc.type==\'refer\'" ui-sref="app.teacher_module.dashboard({course_id: doc.course_id,module_id: doc.module_id})" href="{{doc.href}}" target="_blank">{{doc.name}}</a> <a ng-if="doc.type==\'upload\'" href="api/filemanager/download-no-auth?path={{doc.path}}" target="_blank">{{doc.name}}</a></td></tr></tbody></table></div></div>',
        controller: function(parseMetadataFilter) {
            var scope = this;
            scope.$onInit = function() {
                var postmeta = parseMetadataFilter(scope.module.postmeta);
                if(postmeta.document)
                    scope.docs = JSON.parse(postmeta.document);
                else
                    scope.docs = [];
            };
        }
    });
    app.component('commonCourseInfo', {
        templateUrl: 'app/common/theme/common-course-info.html',
        bindings: {
            course: '<'
        },
        controller: function ($http, $sce) {
            var scope = this;
            scope.$onInit = function() {
                scope.content = "";
                scope.content = $sce.trustAsHtml(scope.course.content);
                angular.element('h1#page-title').text(scope.course.title);
            };
        }
    });
    app.component('commonPostView', {
        templateUrl: 'app/common/theme/common-post-view.html',
        bindings: {
            post: '<',
            onClose: '&'
        },
        controller: function ($sce) {
            var scope = this;
            scope.$onInit = function() {
                scope.content = null;
                scope.content = $sce.trustAsHtml(scope.post.content);
            };

            scope.close = function() {
                if(scope.onClose) scope.onClose();
            }
        }
    });
    app.component('commonCourseView', {
        bindings: {
            course: '<'
        },
        templateUrl: function($attrs) {
            return 'app/'+ $attrs.typeuser +'/theme/' + $attrs.template
        },
        controller: function() {
            var scope = this;
            scope.$onInit = function() {

            };
        }
    });
    app.component('commonTasksByModule', {
        bindings: {
            module: '<',
            stt: '<'
        },
        templateUrl: function($attrs) {
            return 'app/'+ $attrs.typeuser +'/theme/' + $attrs.template
        },
        controller: function($attrs, learnerService) {
            var scope = this;
            scope.$onInit = function() {
                if($attrs.typeuser == 'learner') {
                    learnerService.allTaskByModuleNoAuth(scope.module.id).then(function(res) {
                        scope.tasks = res.data.data;
                    });
                }
            }
        }
    });
})(angular);