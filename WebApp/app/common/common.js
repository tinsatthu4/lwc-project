(function(angular) {
    'use strict';
    function sidebar() {
        angular.element('body').addClass('sidebar-collapse');
    }

    function userInfo($timeout, userService) {
        var scope = this;

        scope.$onInit = function() {
            $timeout(function() {
                userService.info()
                    .then(function(rs) {
                        scope.user = rs.data.data;
                    });
            }, 2000);
        }
    }
    angular.module("common", []);

    //angular.module("common", ["ui.router", "configModule", "ngStorage", "MyService", "helper", "partial"])
    //    .component("sidebar", {
    //        templateUrl: 'app/common/theme/sidebar.html',
    //        controller: sidebar
    //    })
    //    .component("userInfo", {
    //        templateUrl: 'app/common/theme/userinfo.html',
    //        controller: userInfo
    //    })
    //    .component("navMenu", {
    //        templateUrl: 'app/common/theme/nav.html',
    //        controller: function(userService) {
    //            var scope = this;
    //            scope.$onInit = function() {
    //                userService.info()
    //                    .then(function(rs) {
    //                        scope.user = rs.data.data;
    //                    });
    //            }
    //        }
    //    })
    //    .component("dashboard", {
    //        templateUrl: 'app/common/theme/dashboard.html',
    //        controller: function() {
    //
    //        }
    //    });
})(window.angular);