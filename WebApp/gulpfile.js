//var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

//elixir(function(mix) {
//    mix.sass('app.scss');
//});
var templateCache = require('gulp-angular-templatecache');
var gulp = require('gulp');
//var uglify = require('gulp-uglify');
//var htmlmin = require('gulp-htmlmin');
var htmlmin = require('gulp-html-minifier');
var uglify = require('gulp-uglifyjs');
var concat = require('gulp-concat');
var del = require('del');
var replace = require('gulp-replace');
var vendor_file, app_file, css_file;

gulp.task('clean', function() {
   del('app-dist/*.js');
});
gulp.task('templates-dist', function () {
    return gulp.src('app/**/*.html')
        .pipe(htmlmin({collapseWhitespace: true, removeComments: true}))
        .pipe(gulp.dest('templates-dist/'));
});
gulp.task('templates', function () {
    return gulp.src('app/**/*.html')
        .pipe(htmlmin({collapseWhitespace: true, removeComments: true, keepClosingSlash: true}))
        .pipe(templateCache({
            standalone: true,
            transformUrl: function(url) {
                return 'app/' + url;
            }
        }))
        .pipe(gulp.dest('app'));
});
gulp.task('build-css-vendor', function() {
    gulp.src([
        'components/AdminLTE/bootstrap/css/bootstrap.min.css',
        'components/summernote/dist/summernote.css',
        'components/AdminLTE/dist/css/AdminLTE.min.css',
        'components/AdminLTE/plugins/select2/select2.min.css',
        'components/AdminLTE/plugins/datepicker/datepicker3.css',
        'components/AdminLTE/plugins/fullcalendar/fullcalendar.min.css',
        'components/AdminLTE/dist/css/skins/skin-blue.min.css',
        'components/AdminLTE/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css',
        'components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css'
    ])
        .pipe(concat('vendor.css'))
        .pipe(gulp.dest('app-dist/'));
});

gulp.task('build-my-css', function () {
    var d = new Date();
    var file = d.getTime() + '.my.css';
    css_file = file;
    gulp.src([
        'media/css/mycss.css',
        'media/css/main.css'
    ])
        .pipe(concat(file))
        .pipe(gulp.dest('media/css'));
});

gulp.task('build-vendor', function() {
   var d = new Date();
   var file = d.getTime() + '.vendor.js';
    vendor_file = file;
   gulp.src([
       //Theme JS
       'components/AdminLTE/plugins/jQuery/jquery-2.2.3.min.js',
       'components/AdminLTE/bootstrap/js/bootstrap.min.js',
       'components/moment/min/moment.min.js',
       'components/AdminLTE/plugins/datepicker/bootstrap-datepicker.js',
       'components/AdminLTE/plugins/select2/select2.full.min.js',
       'components/AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js',
       'components/AdminLTE/plugins/fastclick/fastclick.js',
       'components/remarkable-bootstrap-notify/bootstrap-notify.min.js',
       'components/AdminLTE/plugins/fullcalendar/fullcalendar.min.js',
       'components/AdminLTE/dist/js/app.min.js',
       'components/AdminLTE/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js',
       'components/summernote/dist/summernote.min.js',
       'components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js',
       //Angular Module
       'components/angular/angular.min.js',
       'components/ngstorage/ngStorage.min.js',
       'components/angular-ui-router/release/angular-ui-router.min.js',
       'components/angular-ui-router/release/stateEvents.min.js',
       'components/angular-ui-tinymce/src/tinymce.js',
       'components/angular-recaptcha/release/angular-recaptcha.min.js',
       'components/angular-summernote/dist/angular-summernote.min.js'
   ])
       .pipe(concat('vendor.js'))
       //.pipe(concat(file))
       //.pipe(uglify({mangle: false}))
       .pipe(gulp.dest('app-dist/'));
});

gulp.task('build', function() {
   var d = new Date();
   var file = d.getTime() + '.app.js';
   app_file = file;
   gulp.src('app/**/*.js')
       .pipe(concat(file))
       .pipe(uglify({mangle: false}))
       .pipe(gulp.dest('app-dist/'));
});

gulp.task('build-dev', function() {
    var d = new Date();
    var file = d.getTime() + '.app.js';
    app_file = file;
   gulp.src('app/**/*.js')
       .pipe(concat(file))
       .pipe(uglify({mangle: false}))
       .pipe(gulp.dest('app-dist/'));
});

gulp.task('replace-js', function () {
    return gulp.src(['index.html'])
        .pipe(replace(/\d+\.app\.js/, app_file))
        .pipe(gulp.dest('./'));
});

//gulp templates && gulp
gulp.task('default', ['clean', 'build-my-css', 'build-vendor', 'build-dev', 'replace-js']);