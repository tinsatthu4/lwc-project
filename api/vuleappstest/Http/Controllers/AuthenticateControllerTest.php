<?php
namespace VuleAppsTest\Http\Controllers;
use Mockery\Mock;
use VuleAppsTest\TestCase;
use Auth;
use DB;
use App\User;
use Recaptcha;

class AuthenticateControllerTest extends TestCase {
	/**
	 * @var Mock;
	 */
	public $mock;
	/**
	 * @group AuthenticateController
	 * @group AuthenticateController@testPostRegisterSuccess
	 */
	function testPostRegisterSuccess() {
		$this->withoutMiddleware();
		DB::table('users')->truncate();

		$request = [
			'name' => 'Le Vu',
			'email' => 'example@domain.local',
			'password' => '123456',
			'password_confirmation' => '123456',
			'type' => 'master',
			'g-recaptcha-response' => 'abc'
		];
		$mock = \Mockery::mock('RecaptchaService');
		$mock->shouldReceive('check')->andReturn(true);
		$mock->shouldReceive('getResponseKey')->andReturn(true);
		$this->app->bind('recaptcha.service', function() use ($mock) {
			return $mock;
		});

		$this->json('POST', 'user/register', $request)
			->seeInDatabase('users', ['id' => 1, 'email' => 'example@domain.local', 'type' => User::TYPE_LEARNER])
			->seeJsonStructure(['token']);

		$this->assertResponseOk();
	}

	/**
	 * @group AuthenticateController
	 */
	function testPostRegisterFail() {
		$this->withoutMiddleware();

		$request = [
			'email' => 'example@domain',
			'password' => '123',
			'password_confirmation' => '456',
			'type' => 'master'
		];

		$this->json('POST', 'user/register', $request)
			->seeJsonStructure(['messages']);

		$this->assertResponseStatus(400);
	}
}