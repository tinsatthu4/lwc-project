<?php
namespace VuleAppsTest\LWcPortal\Controllers;

use League\Flysystem\Exception;
use VuleApps\LwcBackends\Models\DefaultPost;
use VuleApps\LwcBackends\Models\Term;
use VuleApps\LwcBackends\Models\TermRelation;
use VuleApps\LwcPortal\Models\Counrse;
use VuleApps\LwcPortal\Models\UserCounrse;
use VuleAppsTest\TestCase;
use Auth;
use App\User;
use DB;
use TermRepository;

class UserControllerTest extends TestCase
{
    function reset() {
        DB::table('users')->truncate();
        DB::table('posts')->truncate();
        DB::table('user_counrses')->truncate();
    }

    /**
     * @group UserController
     */
    function testIndexSuccess()
    {
        $this->reset();
        $this->withoutMiddleware();

        for($i = 1; $i<= 5; $i++)
            User::create([
                'name' => "learner {$i}",
                'email' => "learner{$i}@example.com",
                'type' => 'learner',
                'password' => bcrypt("learner{$i}"),
            ]);

        User::create([
            [
                'name' => 'Teacher 1',
                'email' => 'teacher1@example.com',
                'type' => 'teacher_1',
                'password' => bcrypt('teacher1'),
            ],
            [
                'name' => 'Teacher 2',
                'email' => 'teacher2@example.com',
                'type' => 'teacher_1',
                'password' => bcrypt('teacher2'),
            ]
        ]);

        $this->json('GET', 'administrator/user')
            ->seeJsonStructure(['data', 'links', 'total']);
        $this->assertResponseOk();
    }

    /**
     * @group UserController
     */
    function testIndexEmpty()
    {
        $this->reset();
        $this->withoutMiddleware();

        $response = $this->call('GET', 'administrator/user');
        $this->seeJsonStructure(['data', 'links', 'total']);
        $data = json_decode($response->getContent(), true);
        $this->assertEquals($data['data'], []);
        $this->assertResponseOk();
    }

    /**
     * @group UserController
     */
    public function testDeleteSuccess()
    {
        $this->reset();
        $this->withoutMiddleware();

        $user = User::create([
            'name' => "learner test",
            'email' => "learner_test@example.com",
            'type' => 'learner',
            'password' => bcrypt("learner"),
        ]);
        $this->json('DELETE', 'administrator/user/' . $user->id);
        $this->seeStatusCode(204);
        $this->assertNull(User::find($user->id));
    }

    /**
     * @group UserController
     */
    public function testDeleteModelNotFound()
    {
        $this->reset();
        $this->withoutMiddleware();

        $user = User::create([
            'name' => "learner test",
            'email' => "learner_test@example.com",
            'type' => 'learner',
            'password' => bcrypt("learner"),
        ]);
        $this->json('DELETE', 'administrator/user/' . ($user->id + 1));
        $this->seeStatusCode(404);
        $this->assertNotNull(User::find($user->id));
    }

    /**
     * @group UserController
     */
    public function testStoreSuccess()
    {
        $this->reset();
        $this->withoutMiddleware();
        
        $data = [
            'name' => "learner test",
            'email' => "learner_test@example.com",
            'type' => 'learner',
            'password' => "admin@123",
            'password_confirmation' => "admin@123"
        ];

        $response = $this->call('POST', 'administrator/user', $data);
        $this->seeJsonStructure(['data']);
        $this->seeStatusCode(201);
        $data = json_decode($response->getContent(), true);
        $this->assertNotNull(User::find($data['data']['id']));
    }

    public static function typeUserSuccess()
    {
        return [
            ['learner'],
            ['teacher_1']
        ];
    }

    /**
     * @dataProvider typeUserSuccess
     * @group UserController
     */
    public function testStoreSuccessWithCourse($type)
    {
        $this->reset();
        $this->withoutMiddleware();

        $course = Counrse::create([
            'type'  => 'counrse'
        ]);
        $data = [
            'name' => "user test",
            'email' => "user_test@example.com",
            'type' => $type,
            'password' => "admin@123",
            'password_confirmation' => "admin@123",
            'course'   => [
                ['id' => $course->id],
            ]
        ];
        $response = $this->call('POST', 'administrator/user', $data);
        $this->seeJsonStructure(['data']);
        $this->seeStatusCode(201);
        $data = json_decode($response->getContent(), true);
        $this->assertNotNull(User::find($data['data']['id']));
        if($type == 'learner') {
            $this->assertNotNull(UserCounrse::where([
                'post_id'   => $course->id,
                'user_id'   => $data['data']['id']
            ])->first());
        }
        elseif($type == 'teacher_1') {
            $course = Counrse::find($course->id);
            $this->assertEquals($course->teacher_id, $data['data']['id']);
        }
    }

    /**
     * @group UserController
     */
    public function testUpdateSuccess()
    {
        $this->reset();
        $this->withoutMiddleware();

        $user = User::create([
            'name' => "learner test",
            'email' => "learner_test@example.com",
            'type' => 'learner',
            'password' => bcrypt("learner"),
        ]);

        $data = [
            'name' => "learner test update",
            'email' => "learner_test_update@example.com",
            'type' => 'teacher_1',
            'password' => "admin@123",
            'password_confirmation' => "admin@123"
        ];

        $response = $this->call('PUT', 'administrator/user/'. $user->id, $data);
        $this->seeJsonStructure(['data']);
        $this->seeStatusCode(200);
        $result = json_decode($response->getContent(), true);
        $user = User::find($result['data']['id']);
        $this->assertEquals($data['name'], $user->name);
        $this->assertEquals($data['email'], $user->email);
        $this->assertEquals($data['type'], $user->type);
    }

    /**
     * @dataProvider typeUserSuccess
     * @group UserController
     */
    public function testUpdateSuccessWithCourse($type)
    {
        $this->reset();
        $this->withoutMiddleware();

        $course = Counrse::create([
            'type'  => 'counrse'
        ]);
        $user = User::create([
            'name' => "learner test",
            'email' => "learner_test@example.com",
            'type' => $type,
            'password' => bcrypt("learner"),
        ]);

        $data = [
            'name' => "user test",
            'email' => "user_test@example.com",
            'type' => $type,
            'password' => "admin@123",
            'password_confirmation' => "admin@123",
            'course'   => [
                ['id' => $course->id],
            ]
        ];
        $response = $this->call('PUT', 'administrator/user/' . $user->id, $data);
        $result = json_decode($response->getContent(), true);
        $this->seeJsonStructure(['data']);
        $this->seeStatusCode(200);
        $result = json_decode($response->getContent(), true);
        $user = User::find($result['data']['id']);
        $this->assertEquals($data['name'], $user->name);
        $this->assertEquals($data['email'], $user->email);
        if($type == 'learner') {
            $this->assertNotNull(UserCounrse::where([
                'post_id'   => $course->id,
                'user_id'   => $result['data']['id']
            ])->first());
        }
        elseif($type == 'teacher_1') {
            $course = Counrse::find($course->id);
            $this->assertEquals($course->teacher_id, $result['data']['id']);
        }
    }

    public static function dataValidateFail()
    {
        return [
            [[
                'name' => "user test",
            ]],
            [[]]
        ];
    }

    /**
     * @dataProvider dataValidateFail
     * @group UserController
     */
    public function testStoreValidateFail($data)
    {
        $this->reset();
        $this->withoutMiddleware();
        $response = $this->call('POST', 'administrator/user', $data);
        $this->seeJsonStructure(['messages']);
        $this->seeStatusCode(400);
    }

    /**
     * @dataProvider dataValidateFail
     * @group UserController
     */
    public function testUpdateValidateFail($data)
    {
        $this->reset();
        $this->withoutMiddleware();
        $user = User::create([
            'name' => "learner test",
            'email' => "learner_test@example.com",
            'type' => 'learner',
            'password' => bcrypt("learner"),
        ]);
        $response = $this->call('PUT', 'administrator/user/' . $user->id, $data);
        $this->seeJsonStructure(['messages']);
        $this->seeStatusCode(400);
    }

    /**
     * @group UserController
     */
    public function testUpdateModelNotFound()
    {
        $this->reset();
        $this->withoutMiddleware();
        $user = User::create([
            'name' => "learner test",
            'email' => "learner_test@example.com",
            'type' => 'learner',
            'password' => bcrypt("learner"),
        ]);
        $data = [
            'name' => "learner test update",
            'email' => "learner_test_update@example.com",
            'type' => 'teacher_1',
            'password' => "admin@123",
            'password_confirmation' => "admin@123"
        ];
        $this->call('PUT', 'administrator/user/' . ($user->id + 1) , $data);
        $this->seeStatusCode(404);
    }
}