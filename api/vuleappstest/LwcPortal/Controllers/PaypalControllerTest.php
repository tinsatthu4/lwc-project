<?php
namespace VuleAppsTest\LWcPortal\Controllers;
use Mockery\Mock;
use VuleApps\LwcPortal\Jobs\SendEmailJob;
use VuleApps\LwcPortal\Models\Checkout;
use VuleApps\LwcPortal\Models\Counrse;
use VuleAppsTest\TestCase;
use Auth;
use App\User;
use DB;
use VuleApps\LwcPortal\Models\Module;
use Paypal;
class PaypalControllerTest extends TestCase {
	/**
	 * @var Mock
	 */
	public $mock;
	function mockUser($id = 1) {
		$user = new \stdClass();
		$user->id = $id;
		Auth::shouldReceive('user')
			->andReturn($user);
	}
	function initData() {
		DB::table('posts')->truncate();
		DB::table('checkouts')->truncate();
		DB::table('user_modules')->truncate();
		DB::table('user_counrses')->truncate();
		DB::table('posts')->insert([
				'title' => 'Course 1',
				'price' => 1000.00,
				'type' => Counrse::TYPE
			]);
		DB::table('posts')->insert([
			[
				'title' => 'Module 1',
				'price' => 500.10,
				'type' => Module::TYPE,
				'parent_id' => 1
			],
			[
				'title' => 'Module 2',
				'price' => 200.20,
				'type' => Module::TYPE,
				'parent_id' => 1
			]
		]);
		DB::table('posts')->insert([
			'title' => 'Course 1',
			'price' => 1000.00,
			'type' => Counrse::TYPE
		]);
	}

	/**
	 * @group PaypalController
	 * @group PaypalController@success
	 */
	function testPostMakeSuccess() {
		$this->withoutMiddleware();
		$this->initData();
		$this->mockUser();
		$redirect = 'http:://example.com/redirect';
		Paypal::shouldReceive('send')
			->andReturn($redirect);
		$request = ['post_id' => 2];
		$this->json('POST', 'paypal/make', $request)
			->seeJsonStructure(['redirect'])
			->seeInDatabase('checkouts', ['id' => 1, 'price' => '500.10', 'post_id' => 2, 'user_id' => 1, 'checkout_type' => Checkout::TYPE_MODULE]);
		$this->assertResponseStatus(201);
		$json = $this->response->getContent();
		$res = json_decode($json);
		$this->assertEquals($redirect, $res->redirect);
	}

	/**
	 * @group PaypalController
	 * @group PaypalController@PostMakeCourseSuccess
	 */
	function testPostMakeCourseSuccess() {
		$this->withoutMiddleware();
		$this->initData();
		$this->mockUser();
		$redirect = 'http:://example.com/redirect';
		Paypal::shouldReceive('send')
			->andReturn($redirect);
		$request = ['post_id' => 1];
		$this->json('POST', 'paypal/make-course', $request)
			->seeJsonStructure(['redirect'])
			->seeInDatabase('checkouts', ['id' => 1, 'price' => '1000.00', 'post_id' => 1, 'user_id' => 1, 'checkout_type' => Checkout::TYPE_COURSE]);
		$this->assertResponseStatus(201);
		$json = $this->response->getContent();
		$res = json_decode($json);
		$this->assertEquals($redirect, $res->redirect);
	}

	/**
	 * @group PaypalController
	 * @group PaypalController@PostMakeSuccessWithArgs
	 */
	function testPostMakeSuccessWithArgs() {
		$this->withoutMiddleware();
		$this->initData();
		$this->mockUser();
		$redirect = 'http:://example.com/redirect';

		Paypal::shouldReceive('send')
			->once()
			->andReturn($redirect);
		$request = ['post_id' => 3];
		$this->json('POST', 'paypal/make', $request)
			->seeJsonStructure(['redirect'])
			->seeInDatabase('checkouts', ['id' => 1, 'price' => 200.2, 'post_id' => 3, 'user_id' => 1, 'checkout_type' => Checkout::TYPE_MODULE]);
		$this->assertResponseStatus(201);
		$json = $this->response->getContent();
		$res = json_decode($json);
		$this->assertEquals($redirect, $res->redirect);
	}

	/**
	 * @group PaypalController
	 */
	function testPostMakeFail() {
		$this->withoutMiddleware();
		$this->initData();
		$this->mockUser();
		$this->json('POST', 'paypal/make', ['post_id' => 10])
			->seeJsonStructure(['messages']);

		$this->assertResponseStatus(400);
	}

	/**
	 * @group PaypalController
	 * @group PaypalController@getRedirectSuccess
	 */
	function testGetRedirectSuccess() {
		$this->withoutMiddleware();
		$this->initData();
		DB::table('checkouts')->insert([
			'user_id' => 1, 'price' => 500.10, 'post_id' => 2, 'checkout_type' => Checkout::TYPE_MODULE
		]);
		Paypal::shouldReceive('validate')
				->withArgs(['Checkout Module Module 1', 500.10, 1, 500.10])
				->andReturn(true);

		$request = [
			'token' => 'paypal-token',
			'PayerID' => 'paypal-payperID'
		];

		$this->json('GET', 'paypal/redirect/1', $request)
			->seeInDatabase('user_modules', ['user_id' => 1, 'post_id' => 2])
			->seeInDatabase('checkouts', ['id' => 1, 'payment_token' => $request['token'], 'payper_id' => $request['PayerID']]);
		$this->assertResponseStatus(302);
	}

	/**
	 * @group PaypalControllerTest
	 * @group PaypalController@GetRedirectCourseSuccess
	 */
	function testGetRedirectCourseSuccess() {
		$this->withoutMiddleware();
		$this->initData();
		DB::table('checkouts')->insert([
			'user_id' => 1, 'price' => 1000.00, 'post_id' => 1, 'checkout_type' => Checkout::TYPE_COURSE, 'description' => json_encode([2,3])
		]);
		Paypal::shouldReceive('validate')
			->withArgs(['Course 1', 1000.00, 1, 1000.00])
			->andReturn(true);

        $this->expectsJobs(SendEmailJob::class);

		$request = [
			'token' => 'paypal-token',
			'PayerID' => 'paypal-payperID'
		];

		$this->json('GET', 'paypal/redirect-course/1', $request)
			->seeInDatabase('user_modules', ['user_id' => 1, 'post_id' => 2])
			->seeInDatabase('user_modules', ['user_id' => 1, 'post_id' => 3])
			->seeInDatabase('user_counrses', ['user_id' => 1, 'post_id' => 1])
			->seeInDatabase('checkouts', ['id' => 1, 'payment_token' => $request['token'], 'payper_id' => $request['PayerID']]);
		$this->assertResponseStatus(302);
	}

	/**
	 * @group PaypalController@PostPaypalSandbox
	 */
//	function testPostPaypalSandbox() {
//		$this->withoutMiddleware();
//		$this->initData();
//		$this->mockUser();
//		$request = ['post_id' => 2];
//		$this->json('POST', 'paypal/make', $request)
//			->seeJsonStructure(['redirect'])
//			->seeInDatabase('checkouts', ['id' => 1, 'price' => '500.10', 'post_id' => 2, 'user_id' => 1]);
//		$this->assertResponseStatus(201);
//		$json = $this->response->getContent();
//		$res = json_decode($json);
//		dd($res);
//	}
}