<?php
namespace VuleAppsTest\LWcPortal\Controllers;

use VuleApps\LwcPortal\Models\Counrse;
use VuleAppsTest\TestCase;
use Auth;
use App\User;
use DB;
use VuleApps\LwcPortal\Models\UserTask;
use VuleApps\LwcPortal\Models\Module;

class LearnerControllerTest extends TestCase {
	function mockUser($id = 1) {
		$user = new \stdClass();
		$user->id = $id;
		Auth::shouldReceive('user')
			->andReturn($user);
	}

	function initWork() {
		DB::table('works')->truncate();
		DB::table('works')->insert([
			['user_id' => 1, 'todo' => 'Todo 1', 'start_date' => '2016-10-10', 'end_date' => '2016-10-30'],
			['user_id' => 1, 'todo' => 'Todo 2', 'start_date' => '2016-10-10', 'end_date' => '2016-10-30'],
			['user_id' => 2, 'todo' => 'Todo 1', 'start_date' => '2016-10-1', 'end_date' => '2016-10-10'],
		]);
	}

	function initData() {
		DB::table('tasks')->truncate();
		DB::table('user_tasks')->truncate();
		DB::table('multi_choice_questions')->truncate();
		DB::table('tasks')->insert([
			['type' => 'mcq', 'title' => 'task 1'],
			['type' => 'mcq', 'title' => 'task 2'],
			['type' => 'mcq', 'title' => 'task 3'],
		]);
		DB::table('user_tasks')->insert([
			['user_id' => 1, 'task_id' => 1],
			['user_id' => 1, 'task_id' => 2],
		]);
		DB::table('multi_choice_questions')->insert([
			[
				'task_id' => 1,
				'options' => json_encode([]),
				'answer' => 'a1'
			],
			[
				'task_id' => 1,
				'options' => json_encode([]),
				'answer' => 'a2'
			],
			[
				'task_id' => 1,
				'options' => json_encode([]),
				'answer' => 'a3'
			],
			[
				'task_id' => 2,
				'options' => json_encode([]),
				'answer' => 'a1'
			],
			[
				'task_id' => 2,
				'options' => json_encode([]),
				'answer' => 'a2'
			],
			[
				'task_id' => 3,
				'options' => json_encode([]),
				'answer' => 'a'
			],
		]);

		DB::table('posts')->truncate();
		DB::table('posts')->insert([
			['title' => 'Course 1', 'type' => Counrse::TYPE],
			['title' => 'Course 1', 'type' => Counrse::TYPE],
		]);
		DB::table('posts')->insert([
			['title' => 'Module 1', 'type' => Module::TYPE, 'parent_id' => 1, 'time_start' => '2016-08-01'],
			['title' => 'Module 2', 'type' => Module::TYPE, 'parent_id' => 2, 'time_start' => '2016-08-01'],
			['title' => 'Module 3', 'type' => Module::TYPE, 'parent_id' => 2, 'time_start' => '2016-08-01'],
		]);
		DB::table('user_modules')->truncate();
		DB::table('user_modules')->insert([
			['user_id' => 1, 'post_id' => 3],
			['user_id' => 1, 'post_id' => 4],
			['user_id' => 2, 'post_id' => 5],
		]);
	}

	/**
	 * @group LearnerController
	 */
	function testPostMcqSubmissionSuccess() {
		$this->initData();
		$this->withoutMiddleware();
		$this->mockUser();
		$request = [
			'answer' => [
				1 => 'a1',
				2 => 'a2',
				3 => 'a3',
			]
		];

		$this->json('POST', 'learner/mcq-submission/1', $request)
			->seeJsonStructure(['data'])
			->seeInDatabase('user_tasks', ['id' => 1, 'user_id' => 1, 'task_id' => 1, 'status' => UserTask::STATUS_SUBMISSION, 'answer' => json_encode($request['answer'])]);

		$this->assertResponseOk();
	}

	/**
	 * @group LearnerController
	 */
	function testPostMcqSubmissionFailAnswer() {
		$this->initData();
		$this->withoutMiddleware();
		$this->mockUser();
		$request = [
			'answer' => [
				1 => 'a1',
				2 => 'a3',
				3 => 'a3',
			]
		];
		$this->json('POST', 'learner/mcq-submission/1', $request)
			->seeJsonStructure(['error'])
			->seeInDatabase('user_tasks', ['id' => 1, 'user_id' => 1, 'task_id' => 1, 'status' => UserTask::STATUS_DRAFT, 'answer' => '']);

		$this->assertResponseStatus(400);
	}

	/**
	 * @group LearnerController
	 */
	function testPostMcqSubmissionFailInput() {
		$this->initData();
		$this->withoutMiddleware();
		$this->mockUser();
		$request = [
			'answer' => []
		];
		$this->json('POST', 'learner/mcq-submission/1', $request)
			->seeJsonStructure(['messages'])
			->seeInDatabase('user_tasks', ['id' => 1, 'user_id' => 1, 'task_id' => 1, 'status' => UserTask::STATUS_DRAFT, 'answer' => '']);

		$this->assertResponseStatus(400);
	}

	/**
	 * @group LearnerController@testPostMcqSubmissionFailStatus
	 */
	function testPostMcqSubmissionFailStatus() {
		$this->initData();
		$this->withoutMiddleware();
		$this->mockUser();
		DB::table('user_tasks')->where('id', 1)->update(['status' => UserTask::STATUS_SUBMISSION]);

		$request = [
			'answer' => [
				1 => 'a3',
				2 => 'a2'
			]
		];

		$this->json('POST', 'learner/mcq-submission/1', $request);

		$this->assertResponseStatus(404);
	}

	/**
	 * @group LearnerController
	 */
	function testPostMcqSubmissionFailTaskId() {
		$this->initData();
		$this->withoutMiddleware();
		$this->mockUser();
		$request = [
			'answer' => []
		];
		$this->json('POST', 'learner/mcq-submission/2', $request)
			->seeJsonStructure(['messages']);

		$this->assertResponseStatus(400);
	}

	/**
	 * @group LearnerController
	 * @group LearnerController@testPostMcqSaveSuccess
	 */
	function testPostMcqSaveSuccess() {
		$this->withoutMiddleware();
		$this->mockUser();
		$this->initData();
		$request = [
			'answer' => [
				1 => 'a1',
				2 => 'a2'
			]
		];

		$this->json('POST', 'learner/mcq-save/1', $request)
			->seeJsonStructure(['data'])
			->seeInDatabase('user_tasks', ['id' => 1, 'answer' => json_encode($request['answer']), 'status' => UserTask::STATUS_DRAFT]);
		$this->assertResponseOk();
	}

	/**
	 * @group LearnerController
	 * @group LearnerController@testPostMcqSaveFailInput
	 */
	function testPostMcqSaveFailInput() {
		$this->withoutMiddleware();
		$this->mockUser();
		$this->initData();
		$request = [
			'answer' => [
				1 => ['a' => 'b'],
				2 => 'a2'
			]
		];

		$this->json('POST', 'learner/mcq-save/1', $request)
			->seeJsonStructure(['messages']);
		$this->assertResponseStatus(400);
	}

	/**
	 * @group LearnerController
	 * @group LearnerController@testPostMcqSaveFailTaskId
	 */
	function testPostMcqSaveFailTaskId() {
		$this->withoutMiddleware();
		$this->mockUser();
		$this->initData();
		$request = [
			'answer' => [
				1 => 'a3',
				2 => 'a2'
			]
		];

		$this->json('POST', 'learner/mcq-save/5', $request)
			->seeJsonStructure(['messages']);
		$this->assertResponseStatus(400);
	}

	/**
	 * @group LearnerController
	 * @group LearnerController@testPostMcqSaveFailStatus
	 */
	function testPostMcqSaveFailStatus() {
		$this->withoutMiddleware();
		$this->mockUser();
		$this->initData();
		DB::table('user_tasks')->where('id', 1)->update(['status' => UserTask::STATUS_SUBMISSION]);
		$request = [
			'answer' => [
				1 => 'a3',
				2 => 'a2'
			]
		];

		$this->json('POST', 'learner/mcq-save/1', $request);
		$this->assertResponseStatus(404);
	}

	/**
	 * @group LearnerController
	 * @group LearnerController@testGetMcq
	 */
	function testGetMcq() {
		$this->withoutMiddleware();
		$this->mockUser();
		$this->initData();

		$this->json('GET', 'learner/mcq/1');
		$this->assertResponseOk();
	}

	/**
	 * @group LearnerController
	 * @group LearnerController@work
	 * @group LearnerController@getIndexWork
	 */
	function testGetIndexWork() {
		$this->withoutMiddleware();
		$this->initWork();
		$this->mockUser();

		$this->json('GET', 'learner/work')
			->seeJsonStructure(['data', 'total', 'links']);
		$this->assertResponseOk();
	}

	/**
	 * @group LearnerController
	 * @group LearnerController@work
	 * @group LearnerController@getShowWork
	 */
	function testGetShowWork() {
		$this->withoutMiddleware();
		$this->initWork();
		$this->mockUser();

		$this->json('GET', 'learner/work/1')
			->seeJsonStructure(['data']);
		$this->assertResponseOk();
	}

	/**
	 * @group LearnerController
	 * @group LearnerController@work
	 * @group LearnerController@postStoreWorkSuccess
	 */
	function testPostStoreWorkSuccess() {
		$this->withoutMiddleware();
		$this->initWork();
		$this->mockUser();
		$request = ['todo' => 'Do Home Work', 'start_date' => '2016-10-01', 'end_date' => '2016-10-02'];

		$this->json('POST', 'learner/work', $request)
			->seeInDatabase('works', ['id' => 4, 'todo' => 'Do Home Work', 'start_date' => '2016-10-01 00:00:00', 'end_date' => '2016-10-02 00:00:00'])
			->seeJsonStructure([
				'data' => ['id', 'todo', 'start_date', 'end_date']
			]);
		$this->assertResponseStatus(201);
	}

	/**
	 * @group LearnerController
	 * @group LearnerController@work
	 * @group LearnerController@postStoreWorkFail
	 */
	function testPostStoreWorkFail() {
		$this->withoutMiddleware();
		$this->initWork();
		$this->mockUser();
		$request = ['start_date' => '10-10-2016', 'end_date' => '10-10-2016'];
		$this->json('POST', 'learner/work', $request)
			->seeJsonStructure([
				'messages' => ['todo', 'start_date', 'end_date']
			]);
		$this->assertResponseStatus(400);
	}

	/**
	 * @group LearnerController
	 * @group LearnerController@work
	 * @group LearnerController@putUpdateWorkSuccess
	 */
	function testPutUpdateWorkSuccess() {
		$this->withoutMiddleware();
		$this->initWork();
		$this->mockUser();
		$request = ['todo' => 'Do Home Work', 'start_date' => '2016-10-02', 'end_date' => '2016-10-03'];

		$this->json('PUT', 'learner/work/1', $request)
			->seeInDatabase('works', ['id' => 1, 'todo' => 'Do Home Work', 'start_date' => '2016-10-02 00:00:00', 'end_date' => '2016-10-03 00:00:00'])
			->seeJsonStructure([
				'data' => ['id', 'todo', 'start_date', 'end_date']
			]);
		$this->assertResponseStatus(200);
	}

	/**
	 * @group LearnerController
	 * @group LearnerController@work
	 * @group LearnerController@deleteDestroyWorkSuccess
	 */
	function testDeleteDestroyWorkSuccess() {
		$this->withoutMiddleware();
		$this->initWork();
		$this->mockUser();

		$this->json('DELETE', 'learner/work/1')
			->dontSeeInDatabase('works', ['id' => 1]);
		$this->assertResponseStatus(204);
	}

	/**
	 * @group LearnerController
	 * @group LearnerController@GetMyModules
	 */
	function testGetMyModules() {
		$this->withoutMiddleware();
		$this->initData();

		$user = new User();
		$user->id = 1;

		Auth::shouldReceive('user')
			->once()
			->andReturn($user);

		$this->json('GET', 'learner/my-modules')
			->seeJsonStructure(['data', 'total', 'links']);
		$this->assertResponseOk();
		$json = $this->response->getContent();
		$res = json_decode($json, true);
		$courses = $res['data'];
		$modules_1 = $courses[0]['modules'];
		$modules_2 = $courses[1]['modules'];
		$this->assertEquals(3, $modules_1[0]['id']);
		$this->assertEquals(4, $modules_2[0]['id']);
	}

	/**
	 * @group LearnerController@GetAllCourse
	 */
	function testGetAllCourse() {
		$this->withoutMiddleware();
		$this->initData();
		$this->json('GET', 'learner/all-courses')
			->seeJsonStructure(['data', 'total', 'links']);
		$this->assertResponseOk();
		$json = $this->response->getContent();
	}

	/**
	 * @group LearnerController@getModule
	 */
	function testGetModule() {
		$this->withoutMiddleware();
		$this->mockUser();
		$this->initData();
		$module = Module::find(3);
		$module->time_start = date('Y') . '-' . date('m') . '-' . date('d');
		$module->save();

		$this->json('GET', 'learner/module/3')
			->seeJsonStructure(['data' => array_keys($module->toArray())]);
		$this->assertResponseOk();
	}

	/**
	 * @group LearnerController@TestGetModuleFail
	 */
	function testGetModuleFail() {
		$this->withoutMiddleware();
		$this->mockUser();
		$this->initData();
		$module = Module::select(['id', 'time_start'])->find(3);
		$module->time_start =  '2020-' . date('m') . '-' . date('d');
		$module->save();

		$this->json('GET', 'learner/module/3');
		$this->assertResponseStatus(404);
	}

	/**
	 * @group LearnerController@testGetModuleUserDontHaveModule
	 */
	function testGetModuleUserDontHaveModule() {
		$this->withoutMiddleware();
		$this->mockUser(5);
		$this->initData();
		$module = Module::select(['id', 'time_start'])->find(3);
		$module->time_start =  '2020-' . date('m') . '-' . date('d');
		$module->save();

		$this->json('GET', 'learner/module/3');
		$this->assertResponseStatus(404);
	}
}