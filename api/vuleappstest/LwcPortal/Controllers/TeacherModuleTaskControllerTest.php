<?php
namespace VuleAppsTest\LWcPortal\Controllers;

use League\Flysystem\Exception;
use VuleApps\LwcPortal\Models\Counrse;
use VuleApps\LwcPortal\Models\Module;
use VuleApps\LwcPortal\Models\Task;
use VuleAppsTest\TestCase;
use Auth;
use App\User;
use DB;

class TeacherModuleTaskControllerTest extends TestCase 
{
    function reset() {
        DB::table('users')->truncate();
        DB::table('posts')->truncate();
        DB::table('tasks')->truncate();
    }

    /**
     * @group TeacherModuleTaskController
     */
    function testIndexSuccess()
    {
        $this->reset();
        $this->withoutMiddleware();

        $user = new \stdClass();
        $user->id = 1;
        Auth::shouldReceive('user')
            ->andReturn($user);
        $course = Counrse::create([
            'teacher_id' => 1,
            'type'       => 'counrse'
        ]);
        $module = Module::create([
            'parent_id' => $course->id,
            'type'      => 'module'
        ]);
        Task::create([
            'post_id'    => $module->id,
            'teacher_id' => $user->id
        ]);
        $response = $this->call('GET', 'teacher/module/'. $module->id . '/task');
        $data = json_decode($response->getContent(), true);

        $this->json('GET', 'teacher/module/'. $module->id . '/task')
            ->seeJsonStructure(['data', 'links', 'total']);
        $this->assertResponseOk();
    }

    /**
     * @group TeacherModuleTaskController
     */
    function testIndexEmpty()
    {
        $this->reset();
        $this->withoutMiddleware();

        $user = new \stdClass();
        $user->id = 2;
        Auth::shouldReceive('user')
            ->andReturn($user);
        $course = Counrse::create([
            'teacher_id' => 2,
            'type'       => 'counrse'
        ]);
        $module = Module::create([
            'parent_id' => $course->id,
            'type'      => 'module'
        ]);
        Task::create([
            'post_id'   => $module->id . '2',
            'teacher_id' => $user->id
        ]);

        $response = $this->call('GET', 'teacher/module/'. $module->id . '/task');
        $data = json_decode($response->getContent(), true);
        $this->assertEquals($data['data'], []);
        $this->seeJsonStructure(['data', 'links', 'total']);
        $this->assertResponseOk();
    }

    /**
     * @group TeacherModuleTaskController
     */
    function testStoreSuccess()
    {
        $this->reset();
        $this->withoutMiddleware();

        $user = new \stdClass();
        $user->id = 1;
        Auth::shouldReceive('user')
            ->andReturn($user);
        $course = Counrse::create([
            'teacher_id' => 1,
            'type'       => 'counrse'
        ]);
        $module = Module::create([
            'parent_id' => $course->id,
            'type'      => 'module'
        ]);
        $requests = [
            [
                'title'      => 'Task test 1',
                'type'       => 'assignment',
                'keywords'   => 'Task,test',
                'preference' => 'preference test',
                'question'   => 'question test'
            ],
            [
                'title'      => 'Task test 2',
                'type'       => 'mcq',
                'keywords'   => 'Task,test',
                'preference' => 'preference test',
                'question'   => 'question test'
            ]
        ];
        foreach($requests as $request) {
            $response = $this->call('POST', 'teacher/module/'. $module->id . '/task', $request);
            $data = json_decode($response->getContent(), true);
            $this->seeJsonStructure(['data']);
            $this->assertResponseStatus(201);

            $data = json_decode($response->getContent(), true);
            $task = Task::find($data['data']['id']);
            $this->assertEquals($task->title, $request['title']);
            $this->assertEquals($task->post_id, $module->id);
        }
    }

    /**
     * @group TeacherModuleTaskController
     */
    function testStoreValidateFail()
    {
        $this->reset();
        $this->withoutMiddleware();

        $user = new \stdClass();
        $user->id = 1;
        Auth::shouldReceive('getUser')
            ->andReturn($user);
        $course = Counrse::create([
            'teacher_id' => 2,
            'type'       => 'counrse'
        ]);
        $module = Module::create([
            'parent_id' => $course->id,
            'type'      => 'module'
        ]);
        $requests = [
            [
                'title'      => '',
                'type'       => '',
                'keywords'   => '',
                'preference' => '',
            ],
            []
        ];
        foreach($requests as $request) {
            $response = $this->call('POST', 'teacher/module/'. $module->id . '/task', $request);
            $this->seeJsonStructure(['messages']);
            $this->assertResponseStatus(400);
        }
    }

    /**
     * @group TeacherModuleTaskController
     */
    function testUpdateSuccess()
    {
        $this->reset();
        $this->withoutMiddleware();

        $user = new \stdClass();
        $user->id = 1;
        Auth::shouldReceive('getUser')
            ->andReturn($user);
        $course = Counrse::create([
            'teacher_id' => 1,
            'type'       => 'counrse'
        ]);
        $module = Module::create([
            'parent_id' => $course->id,
            'type'      => 'module'
        ]);
        $task = Task::create([
            'post_id'   => $module->id,
            'title'      => 'Task testing',
            'type'       => 'assignment',
            'keywords'   => 'Task,testing',
            'preference' => 'preference testing',
        ]);
        $requests = [
            [
                'title'      => 'Task test update testing 1',
                'type'       => 'assignment',
                'keywords'   => 'Task,test',
                'preference' => 'preference test',
            ],
            [
                'title'      => 'Task test update testing 2',
                'type'       => 'mcq',
                'keywords'   => 'Task,test',
                'preference' => 'preference test',
            ]
        ];
        foreach($requests as $request) {
            $response = $this->call('PUT', 'teacher/module/'. $module->id . '/task/' . $task->id, $request);
            $this->seeJsonStructure(['data']);
            $this->assertResponseStatus(200);

            $data = json_decode($response->getContent(), true);
            $task = Task::find($data['data']['id']);
            $this->assertEquals($task->title, $request['title']);
            $this->assertEquals($task->post_id, $module->id);
        }
    }

    /**
     * @group TeacherModuleTaskController
     */
    function testUpdateValidateFail()
    {
        $this->reset();
        $this->withoutMiddleware();

        $user = new \stdClass();
        $user->id = 1;
        Auth::shouldReceive('getUser')
            ->andReturn($user);
        $course = Counrse::create([
            'teacher_id' => 1,
            'type'       => 'counrse'
        ]);
        $module = Module::create([
            'parent_id' => $course->id,
            'type'      => 'module'
        ]);
        $task = Task::create([
            'post_id'   => $module->id,
            'title'      => 'Task testing',
            'type'       => 'assignment',
            'keywords'   => 'Task,testing',
            'preference' => 'preference testing',
        ]);
        $requests = [
            [
                'title'      => '',
                'type'       => '',
                'keywords'   => '',
                'preference' => '',
            ],
            []
        ];
        foreach($requests as $request) {
            $response = $this->call('PUT', 'teacher/module/'. $module->id . '/task/' . $task->id, $request);
            $this->seeJsonStructure(['messages']);
            $this->assertResponseStatus(400);
        }
    }

    /**
     * @group TeacherModuleTaskController
     */
    function testUpdateModelNotFound()
    {
        $this->reset();
        $this->withoutMiddleware();

        $user = new \stdClass();
        $user->id = 1;
        Auth::shouldReceive('getUser')
            ->andReturn($user);
        $course = Counrse::create([
            'teacher_id' => 1,
            'type'       => 'counrse'
        ]);
        $module = Module::create([
            'parent_id' => $course->id,
            'type'      => 'module'
        ]);
        $task = Task::create([
            'post_id'   => $module->id,
            'title'      => 'Task testing',
            'type'       => 'assignment',
            'keywords'   => 'Task,testing',
            'preference' => 'preference testing',
        ]);
        $request = [
            'post_id'    => $module->id,
            'title'      => 'Task testing',
            'type'       => 'assignment',
            'keywords'   => 'Task,testing',
            'preference' => 'preference testing',
        ];
        $response = $this->call('PUT', 'teacher/module/'. $module->id . '/task/' . ($task->id + 1), $request);
        $this->assertResponseStatus(404);
        $data = json_decode($response->getContent(), true);
        $this->assertEquals($data, null);
    }
}