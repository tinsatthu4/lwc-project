<?php
namespace VuleAppsTest\LWcPortal\Controllers;

use League\Flysystem\Exception;
use VuleApps\LwcBackends\Models\DefaultPost;
use VuleApps\LwcBackends\Models\Term;
use VuleApps\LwcBackends\Models\TermRelation;
use VuleApps\LwcPortal\Models\Counrse;
use VuleApps\LwcPortal\Models\UserCounrse;
use VuleApps\LwcPortal\Models\UserTask;
use VuleApps\LwcPortal\Models\UserTaskComment;
use VuleAppsTest\TestCase;
use Auth;
use App\User;
use DB;
use TermRepository;

class UserTaskCommentControllerTest extends TestCase
{
	function mockUser($id = 1) {
		$user = new \stdClass();
		$user->id = $id;
		Auth::shouldReceive('user')
			->andReturn($user);
	}

    function reset() {
        DB::table('user_tasks')->truncate();
        DB::table('user_task_comments')->truncate();
    }

    /**
	 * @group UserTaskComment
     * @group UserTaskCommentController
     */
    function testIndexLearnerSuccess()
    {
        $this->reset();
        $this->withoutMiddleware();

        UserTask::create([
            [
                'user_id' => 1,
                'task_id' => 1,
                'answer'  => 'Test answer',
                'status'  => 'publish',
                'grade'   => 10,
                'teacher_comment' => 'Test teacher comment'
            ],
            [
                'user_id' => 1,
                'task_id' => 2,
                'answer'  => 'Test answer 2',
                'status'  => 'publish',
                'grade'   => 10,
                'teacher_comment' => 'Test teacher comment 2'
            ]
        ]);

        UserTaskComment::create([
            [
                'user_task_id'  => 1,
                'parent_id'     => 0,
                'message'       => 'test message'
            ],
            [
                'user_task_id'  => 2,
                'parent_id'     => 1,
                'message'       => 'test message2'
            ]
        ]);

        $this->json('GET', 'learner/user-task-comment')
            ->seeJsonStructure(['data', 'links', 'total']);
        $this->assertResponseOk();
    }

    /**
	 * @group UserTaskComment
     * @group UserTaskCommentControllerTest
     */
    function testIndexTeacherSuccess()
    {
        $this->reset();
        $this->withoutMiddleware();

        UserTask::create([
            [
                'user_id' => 1,
                'task_id' => 1,
                'answer'  => 'Test answer',
                'status'  => 'publish',
                'grade'   => 10,
                'teacher_comment' => 'Test teacher comment'
            ],
            [
                'user_id' => 1,
                'task_id' => 2,
                'answer'  => 'Test answer 2',
                'status'  => 'publish',
                'grade'   => 10,
                'teacher_comment' => 'Test teacher comment 2'
            ]
        ]);

        UserTaskComment::create([
            [
                'user_task_id'  => 1,
                'parent_id'     => 0,
                'message'       => 'test message'
            ],
            [
                'user_task_id'  => 2,
                'parent_id'     => 1,
                'message'       => 'test message2'
            ]
        ]);

        $this->json('GET', 'api/user-task-comment')
            ->seeJsonStructure(['data', 'links', 'total']);
        $this->assertResponseOk();
    }

    /**
	 * @group UserTaskComment
     * @group UserTaskCommentControllerTest
     */
    public function testStoreLeanerSuccess()
    {
        $this->reset();
        $this->withoutMiddleware();
		$this->mockUser();
        UserTask::create([
            [
                'user_id' => 1,
                'task_id' => 1,
                'answer'  => 'Test answer',
                'status'  => 'publish',
                'grade'   => 10,
                'teacher_comment' => 'Test teacher comment'
            ],
            [
                'user_id' => 1,
                'task_id' => 2,
                'answer'  => 'Test answer 2',
                'status'  => 'publish',
                'grade'   => 10,
                'teacher_comment' => 'Test teacher comment 2'
            ]
        ]);

        UserTaskComment::create([
            [
                'user_task_id'  => 1,
                'parent_id'     => 0,
                'message'       => 'test message'
            ],
            [
                'user_task_id'  => 2,
                'parent_id'     => 1,
                'message'       => 'test message2'
            ]
        ]);


        $data = [
            'user_task_id' => 1,
            'parent_id'    => 0,
            'message'      => "Test message",
        ];

        $response = $this->call('POST', 'learner/user-task-comment', $data);
        $this->seeJsonStructure(['data']);
        $this->seeStatusCode(201);
        $data = json_decode($response->getContent(), true);
        $this->assertNotNull(UserTaskComment::find($data['data']['id']));
		$this->assertEquals(1, $data['data']['user_id']);
    }

    public static function validateStoreProvider()
    {
        return [
            [
                [
                    'user_task_id' => 11,
                    'parent_id'    => 0,
                ]
            ],
            [
                [
                    'user_task_id' => 1,
                    'parent_id'    => 0,
                ]
            ],
            [[]]
        ];
    }


    /**
     * @dataProvider validateStoreProvider
	 * @group UserTaskComment
     * @group UserTaskCommentController
     */
    public function testStoreLeanerFail($data)
    {
        $this->reset();
        $this->withoutMiddleware();

        UserTask::create([
            [
                'user_id' => 1,
                'task_id' => 1,
                'answer'  => 'Test answer',
                'status'  => 'publish',
                'grade'   => 10,
                'teacher_comment' => 'Test teacher comment'
            ],
            [
                'user_id' => 1,
                'task_id' => 2,
                'answer'  => 'Test answer 2',
                'status'  => 'publish',
                'grade'   => 10,
                'teacher_comment' => 'Test teacher comment 2'
            ]
        ]);

        UserTaskComment::create([
            [
                'user_task_id'  => 1,
                'parent_id'     => 0,
                'message'       => 'test message'
            ],
            [
                'user_task_id'  => 2,
                'parent_id'     => 1,
                'message'       => 'test message2'
            ]
        ]);

        $this->json('POST', 'learner/user-task-comment', $data);
        $this->seeJsonStructure(['messages']);
        $this->assertResponseStatus(400);
    }
}