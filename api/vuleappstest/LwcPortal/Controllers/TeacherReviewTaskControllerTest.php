<?php
namespace VuleAppsTest\LWcPortal\Controllers;

use League\Flysystem\Exception;
use VuleApps\LwcPortal\Jobs\SendEmailJob;
use VuleApps\LwcPortal\Models\Task;
use VuleApps\LwcPortal\Models\UserTask;
use VuleAppsTest\TestCase;
use Auth;
use App\User;
use DB;
use Mail;

class TeacherReviewTaskControllerTest extends TestCase {
	function reset() {
		DB::table('users')->truncate();
		DB::table('user_tasks')->truncate();
		DB::table('course_teachers')->truncate();
		DB::table('posts')->truncate();

        for($i = 1; $i<= 5; $i++)
            User::create([
                'name' => "learner {$i}",
                'email' => "learner{$i}@example.com",
                'type' => 'learner',
                'password' => bcrypt("learner{$i}"),
            ]);

        User::create([
            [
                'name' => 'Teacher 1',
                'email' => 'teacher1@example.com',
                'type' => 'teacher_1',
                'password' => bcrypt('teacher1'),
            ],
            [
                'name' => 'Teacher 2',
                'email' => 'teacher2@example.com',
                'type' => 'teacher_1',
                'password' => bcrypt('teacher2'),
            ]
        ]);

        Task::create([
            'post_id'    => 1,
            'teacher_id' => 1,
            'title' => 'test task'
        ]);
	}

	/**
	 * @group TeacherReviewTaskController
	 */
	function testPutUserTaskSuccess() {
		$this->withoutMiddleware();
		$this->reset();
		$user = new \stdClass();
		$user->id = 1;
		Auth::shouldReceive('user')
			->andReturn($user);
		UserTask::create([
			'teacher_id' => 1,
		]);
		$request = [
			'teacher_comment' => 'teacher comment',
			'grade' => 'pass'
		];
		$this->json('PUT', 'teacher/user-task/1', $request)
			->seeJsonStructure([
				'data' => ['teacher_comment', 'grade']
			]);
		$this->assertResponseOk();
		$userTask = UserTask::find(1);
		$this->assertEquals($userTask->teacher_comment, $request['teacher_comment']);
		$this->assertEquals($userTask->grade, $request['grade']);
		$this->assertEquals($userTask->teacher_id, 1);
	}

    /**
     * @group TeacherReviewTaskController
     */
    function testPutUserTaskRedoSuccess() {
        $this->withoutMiddleware();
        $this->reset();
        $user = new \stdClass();
        $user->id = 1;
        Auth::shouldReceive('user')
            ->andReturn($user);
        UserTask::create([
            'teacher_id' => 1,
            'user_id'    => 2
        ]);
        $request = [
            'teacher_comment' => 'teacher comment',
            'grade' => 'redo'
        ];
        $this->expectsJobs(SendEmailJob::class);
        $this->json('PUT', 'teacher/user-task/1', $request)
            ->seeJsonStructure([
                'data' => ['teacher_comment', 'grade']
            ]);
        $this->assertResponseOk();
        $userTask = UserTask::find(1);
        $this->assertEquals($userTask->teacher_comment, $request['teacher_comment']);
        $this->assertEquals($userTask->grade, $request['grade']);
        $this->assertEquals($userTask->teacher_id, 1);
        $this->assertEquals($userTask->status, UserTask::STATUS_DRAFT);
    }

    /**
     * @group SendMailJob
     */
    function testSendMailSuccess()
    {
//        Mail::shouldReceive('send')
//            ->once();
        $this->reset();

        (new SendEmailJob(
            'Test Subject',
            'phong.nt.26101993@gmail.com',
            'bruce.nguyen26101993@gmail.com',
            'email.redo',
            [
                'learner'  => User::find(1),
                'teacher'  => User::find(2),
                'task'     => Task::find(1)
            ]
        ))->handle();
    }

	/**
	 * @group TeacherReviewTaskController
	 * @group TeacherReviewTaskController@testPutUserTaskFail
	 */
	function testPutUserTaskFailInput() {
		$this->withoutMiddleware();
		$this->reset();
		$user = new \stdClass();
		$user->id = 1;
		Auth::shouldReceive('user')
			->andReturn($user);
		UserTask::create([
			'teacher_id' => 1,
		]);
		$request = [
			'teacher_comment' => 'teacher comment',
		];
		$this->json('PUT', 'teacher/user-task/1', $request)
			->seeJsonStructure(['messages']);

		$this->assertResponseStatus(400);
	}

	/**
	 * @group TeacherReviewTaskController
	 */
	function testPutUserTaskFailModelNotFound() {
		$this->withoutMiddleware();
		$this->reset();
		$user = new \stdClass();
		$user->id = 1;
		Auth::shouldReceive('user')
			->andReturn($user);
		UserTask::create([
			'teacher_id' => 2,
		]);
		$request = [
			'teacher_comment' => 'teacher comment',
			'grade' => 'pass'
		];
		$this->json('PUT', 'teacher/user-task/1', $request)
			->seeJsonStructure([
				'messages'=>['error']
			]);
		$this->assertResponseStatus(404);
	}

	/**
	 * @group TeacherReviewTaskController
	 * @group TeacherReviewTaskController@testGetUserTaskSuccess
	 */
	function testGetUserTaskSuccess() {
		$this->withoutMiddleware();
		$this->reset();
		$user = new \stdClass();
		$user->id = 1;
		Auth::shouldReceive('user')
			->andReturn($user);

		$this->json('GET', 'teacher/user-task/group')
			->seeJsonStructure(['data', 'links', 'total']);

		$this->assertResponseOk();
	}

	/**
	 * @group TeacherReviewTaskController@testGetMyModules
	 */
	function testGetMyModulesSuccess() {
		$this->reset();
		DB::table('posts')->insert(['title' => 'counrse 1', 'type' => 'counrse']);
		DB::table('course_teachers')->insert(['user_id' => 1, 'post_id' => 1]);

		$this->withoutMiddleware();

		$user = new \stdClass();
		$user->id = 1;
		Auth::shouldReceive('user')
			->andReturn($user);
		$this->json('GET', 'teacher/my/modules/1')
		->seeJsonStructure(['data', 'links', 'total']);
		$this->assertResponseOk();
	}

	/**
	 * @group TeacherReviewTaskController@testGetMyModules
	 */
	function testGetMyModulesFailInput() {
		$this->reset();
		DB::table('posts')->insert(['title' => 'counrse 1', 'type' => 'counrse']);
		DB::table('course_teachers')->insert(['user_id' => 2, 'post_id' => 1]);

		$this->withoutMiddleware();

		$user = new \stdClass();
		$user->id = 1;
		Auth::shouldReceive('user')
			->andReturn($user);
		$this->json('GET', 'teacher/my/modules/1')
			->seeJsonStructure(['messages']);
		$this->assertResponseStatus(400);
	}

	/**
	 * @group TeacherReviewTaskController@testGetMyCoursesSuccess
	 */
	function testGetMyCoursesSuccess() {
		$this->reset();
		DB::table('posts')->insert([
			['title' => 'counrse 1', 'type' => 'counrse'],
			['title' => 'counrse 2', 'type' => 'counrse'],
			['title' => 'counrse 3', 'type' => 'counrse'],
		]);
		DB::table('course_teachers')->insert([
			['user_id' => 1, 'post_id' => 1],
			['user_id' => 1, 'post_id' => 2],
		]);

		$this->withoutMiddleware();

		$user = new \stdClass();
		$user->id = 1;
		Auth::shouldReceive('user')
			->andReturn($user);
		$this->json('GET', 'teacher/my/course')
			->seeJsonStructure(['data', 'links', 'total'])
			->seeJsonContains(['total' => 2]);
		$this->assertResponseOk();
	}

}