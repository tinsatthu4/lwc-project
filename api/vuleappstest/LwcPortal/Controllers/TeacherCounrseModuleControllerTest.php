<?php
namespace VuleAppsTest\LWcPortal\Controllers;

use League\Flysystem\Exception;
use VuleApps\LwcPortal\Models\Counrse;
use VuleApps\LwcPortal\Models\Module;
use VuleAppsTest\TestCase;
use Auth;
use PostRepository;
use App\User;
use DB;

class TeacherCounrseModuleControllerTest extends TestCase 
{
    function reset() {
        DB::table('users')->truncate();
        DB::table('posts')->truncate();
    }

    /**
     * @group TeacherCounrseModuleController
     */
    function testIndexSuccess()
    {
        $this->reset();
        $this->withoutMiddleware();

        $user = new \stdClass();
        $user->id = 1;
        Auth::shouldReceive('getUser')
            ->andReturn($user);
        $course = Counrse::create([
            'teacher_id' => 1,
            'type'       => 'counrse'
        ]);
        Module::create([
            'parent_id' => $course->id,
            'type'      => 'module'
        ]);
        
        $this->json('GET', 'teacher/counrse/'. $course->id . '/module')
			->seeJsonStructure(['data', 'links', 'total']);
        $this->assertResponseOk();
    }

    /**
     * @group TeacherCounrseModuleController
     */
    function testIndexEmpty()
    {
        $this->reset();
        $this->withoutMiddleware();

        $user = new \stdClass();
        $user->id = 2;
        Auth::shouldReceive('getUser')
            ->andReturn($user);
        $course = Counrse::create([
            'teacher_id' => 2,
            'type'       => 'counrse'
        ]);
        Module::create([
            'parent_id' => $course->id . '2',
            'type'      => 'module'
        ]);

        $response = $this->call('GET', 'teacher/counrse/'. $course->id . '/module');
        $this->seeJsonStructure(['data', 'links', 'total']);
        $data = json_decode($response->getContent(), true);
        $this->assertEquals($data['data'], []);
        $this->seeJsonStructure(['data', 'links', 'total']);
        $this->assertResponseOk();
    }

    /**
     * @group TeacherCounrseModuleController
     */
    function testIndexValidateFail()
    {
        $this->reset();
        $this->withoutMiddleware();

        $user = new \stdClass();
        $user->id = 1;
        Auth::shouldReceive('getUser')
            ->andReturn($user);
        $course = Counrse::create([
            'teacher_id' => 2,
            'type'       => 'counrse'
        ]);
        Module::create([
            'parent_id' => $course->id,
            'type'      => 'module'
        ]);
        $this->json('GET', 'teacher/counrse/'. $course->id . '/module')
            ->seeJsonStructure(['messages']);
        $this->assertResponseStatus(400);
    }

    /**
     * @group TeacherCounrseModuleController
     */
    function testStoreSuccess()
    {
        $this->reset();
        $this->withoutMiddleware();

        $user = new \stdClass();
        $user->id = 1;
        Auth::shouldReceive('getUser')
            ->andReturn($user);
        $course = Counrse::create([
            'teacher_id' => 1,
            'type'       => 'counrse'
        ]);
        $request = [
            'title' => 'Module test'
        ];
        $response = $this->call('POST', 'teacher/counrse/'. $course->id . '/module', $request);
        $this->seeJsonStructure(['data']);
        $this->assertResponseStatus(201);

        $data = json_decode($response->getContent(), true);
        $module = Module::find($data['data']['id']);
        $this->assertEquals($module->title, 'Module test');
        $this->assertEquals($module->parent_id, $course->id);
    }

    /**
     * @group TeacherCounrseModuleController
     */
    function testStoreValidateFail()
    {
        $this->reset();
        $this->withoutMiddleware();

        $user = new \stdClass();
        $user->id = 1;
        Auth::shouldReceive('getUser')
            ->andReturn($user);
        $course = Counrse::create([
            'teacher_id' => 2,
            'type'       => 'counrse'
        ]);
        $requests = [
            ['title' => null],
            []
        ];
        foreach ($requests as $request) {
            $this->json('POST', 'teacher/counrse/'. $course->id . '/module', $request)
                ->seeJsonStructure(['messages']);
            $this->assertResponseStatus(400);
        }
    }

    /**
     * @group TeacherCounrseModuleController
     */
    function testUpdateSuccess()
    {
        $this->reset();
        $this->withoutMiddleware();

        $user = new \stdClass();
        $user->id = 1;
        Auth::shouldReceive('getUser')
            ->andReturn($user);
        $course = Counrse::create([
            'teacher_id' => 1,
            'type'       => 'counrse'
        ]);
        $module = Module::create([
            'title'     => 'module testing',
            'parent_id' => $course->id,
            'type'      => 'module'
        ]);
        $request = [
            'title' => 'Module update test'
        ];
        $response = $this->call('PUT', 'teacher/counrse/'. $course->id . '/module/' . $module->id, $request);
        $this->seeJsonStructure(['data']);
        $this->assertResponseOk();

        $data = json_decode($response->getContent(), true);
        $module = Module::find($data['data']['id']);
        $this->assertEquals($module->title, 'Module update test');
        $this->assertEquals($module->parent_id, $course->id);
    }

    /**
     * @group TeacherCounrseModuleController
     */
    function testUpdateValidateFail()
    {
        $this->reset();
        $this->withoutMiddleware();

        $user = new \stdClass();
        $user->id = 1;
        Auth::shouldReceive('getUser')
            ->andReturn($user);
        $course = Counrse::create([
            'teacher_id' => 2,
            'type'       => 'counrse'
        ]);
        $module = Module::create([
            'title'     => 'module testing',
            'parent_id' => $course->id,
            'type'      => 'module'
        ]);
        $requests = [
            ['title' => 'Module test'],
            ['title' => null],
            []
        ];
        foreach ($requests as $request) {
            $this->json('PUT', 'teacher/counrse/'. $course->id . '/module/' . $module->id, $request)
                ->seeJsonStructure(['messages']);
            $this->assertResponseStatus(400);
        }
    }

    /**
     * @group TeacherCounrseModuleController
     */
    function testUpdateModelNotFound()
    {
        $this->reset();
        $this->withoutMiddleware();

        $user = new \stdClass();
        $user->id = 1;
        Auth::shouldReceive('getUser')
            ->andReturn($user);
        $course = Counrse::create([
            'teacher_id' => 1,
            'type'       => 'counrse'
        ]);
        $module = Module::create([
            'title'     => 'module testing',
            'parent_id' => $course->id,
            'type'      => 'module'
        ]);
        $request = [
            'title' => 'Module test',
        ];
        $response = $this->call('PUT', 'teacher/counrse/'. $course->id . '/module/' . ($module->id + 1), $request);
        $this->assertResponseStatus(404);
        $data = json_decode($response->getContent(), true);
        $this->assertEquals($data, null);
    }
}