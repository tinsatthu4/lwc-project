<?php
namespace VuleAppsTest\LWcPortal\Controllers;

use League\Flysystem\Exception;
use VuleApps\LwcPortal\Models\Counrse;
use VuleAppsTest\TestCase;
use Auth;
use App\User;
use DB;

class TeacherCounrseControllerTest extends TestCase 
{
    function reset() {
        DB::table('users')->truncate();
        DB::table('posts')->truncate();
    }
    
    /**
     * @group TeacherCounrseController
     */
    function testIndexSuccess()
    {
        $this->reset();
        $this->withoutMiddleware();

        $user = new \stdClass();
        $user->id = 1;
        Auth::shouldReceive('getUser')
            ->andReturn($user);
        Counrse::create([
            'teacher_id' => 1,
            'type'       => 'counrse'
        ]);

        $this->json('GET', 'teacher/counrse')
            ->seeJsonStructure(['data', 'links', 'total']);
        $this->assertResponseOk();
    }

    /**
     * @group TeacherCounrseController
     */
    function testIndexEmpty()
    {
        $this->reset();
        $this->withoutMiddleware();

        $user = new \stdClass();
        $user->id = 2;
        Auth::shouldReceive('getUser')
            ->andReturn($user);
        Counrse::create([
            'teacher_id' => 1,
        ]);

        $response = $this->call('GET', 'teacher/counrse');
        $this->seeJsonStructure([
            'data', 'links', 'total']);
        $data = json_decode($response->getContent(), true);
        $this->assertEquals($data['data'], []);
        $this->seeJsonStructure(['data', 'links', 'total']);
        $this->assertResponseOk();
    }

    /**
     * @group TeacherCounrseController
     */
    function testStoreSuccess()
    {
        $this->reset();
        $this->withoutMiddleware();

        $user = new \stdClass();
        $user->id = 1;
        Auth::shouldReceive('getUser')
            ->andReturn($user);
        $request = [
            'title' => 'Course test'
        ];
        $response = $this->call('POST', 'teacher/counrse', $request);
        $this->seeJsonStructure(['data']);
        $this->assertResponseStatus(201);

        $data = json_decode($response->getContent(), true);
        $course = Counrse::find($data['data']['id']);
        $this->assertEquals($course->title, 'Course test');
        $this->assertEquals($course->teacher_id, 1);
    }

    /**
     * @group TeacherCounrseController
     */
    function testStoreValidateFail()
    {
        $this->reset();
        $this->withoutMiddleware();

        $user = new \stdClass();
        $user->id = 1;
        Auth::shouldReceive('getUser')
            ->andReturn($user);
        $requests = [
            ['title' => null],
            []
        ];
        foreach ($requests as $request) {
            $this->json('POST', 'teacher/counrse', $request)
                ->seeJsonStructure(['messages']);
            $this->assertResponseStatus(400);
        }
    }

    /**
     * @group TeacherCounrseController
     */
    function testUpdateSuccess()
    {
        $this->reset();
        $this->withoutMiddleware();

        $user = new \stdClass();
        $user->id = 1;
        Auth::shouldReceive('getUser')
            ->andReturn($user);
        $course = Counrse::create([
            'title'     => 'course testing',
            'parent_id' => 0,
            'teacher_id' => 1,
            'type'      => 'counrse'
        ]);
        $request = [
            'title' => 'Course update test'
        ];
        $response = $this->call('PUT', 'teacher/counrse/'. $course->id , $request);
        $this->seeJsonStructure(['data']);
        $this->assertResponseOk();

        $data = json_decode($response->getContent(), true);
        $course = Counrse::find($data['data']['id']);
        $this->assertEquals($course->title, 'Course update test');
        $this->assertEquals($course->teacher_id, 1);
        $this->assertEquals($course->type, 'counrse');
    }

    /**
     * @group TeacherCounrseController
     */
    function testUpdateValidateFail()
    {
        $this->reset();
        $this->withoutMiddleware();

        $user = new \stdClass();
        $user->id = 1;
        Auth::shouldReceive('getUser')
            ->andReturn($user);
        $course = Counrse::create([
            'teacher_id' => 1,
            'type'       => 'counrse'
        ]);
        $requests = [
            ['title' => null],
            []
        ];
        foreach ($requests as $request) {
            $this->json('PUT', 'teacher/counrse/'. $course->id, $request)
                ->seeJsonStructure(['messages']);
            $this->assertResponseStatus(400);
        }
    }

    /**
     * @group TeacherCounrseController
     */
    function testUpdateTeacherPermissionNotFound()
    {
        $this->reset();
        $this->withoutMiddleware();

        $user = new \stdClass();
        $user->id = 1;
        Auth::shouldReceive('getUser')
            ->andReturn($user);
        $course = Counrse::create([
            'teacher_id' => 2,
            'type'       => 'counrse'
        ]);
        $request = [
            'title' => 'Module test',
        ];
        $response = $this->call('PUT', 'teacher/counrse/'. $course->id, $request);
        $this->assertResponseStatus(404);
        $data = json_decode($response->getContent(), true);
        $this->assertEquals($data, null);
    }

    /**
     * @group TeacherCounrseController
     */
    function testUpdateModelNotFound()
    {
        $this->reset();
        $this->withoutMiddleware();

        $user = new \stdClass();
        $user->id = 1;
        Auth::shouldReceive('getUser')
            ->andReturn($user);
        $course = Counrse::create([
            'teacher_id' => 1,
            'type'       => 'counrse'
        ]);
        $request = [
            'title' => 'Module test',
        ];
        $response = $this->call('PUT', 'teacher/counrse/'. ($course->id + 1), $request);
        $this->assertResponseStatus(404);
        $data = json_decode($response->getContent(), true);
        $this->assertEquals($data, null);
    }
}