<?php
namespace VuleAppsTest\LWcPortal\Controllers;

use VuleAppsTest\TestCase;
use Auth;
use App\User;
use DB;

class AdministratorTeacherControllerTest extends TestCase
{
	function reset() {
		DB::table('users')->truncate();
	}

	/**
	 * @group AdministratorTeacherController
	 */
	function testGetTeacherSearch() {
		$this->reset();
		$this->withoutMiddleware();
		DB::table('users')->insert([
			[
				'name' => 'abcdef',
				'email' => 'abcdef@gmail.com',
				'password' => '12313',
				'type' => 'teacher_1',
				'token' => 100
			],
			[
				'name' => 'khkhjkhjkh',
				'email' => 'abcdef123@gmail.com',
				'password' => '12313',
				'type' => 'teacher_1',
				'token' => 100
			],
			[
				'name' => 'abcdef',
				'email' => 'abcdef2@gmail.com',
				'password' => '12313',
				'type' => 'teacher_2',
				'token' => 100
			],
			[
				'name' => 'abcdef',
				'email' => 'abcdef1@gmail.com',
				'password' => '12313',
				'type' => 'teacher_2',
				'token' => 100
			],
		]);
		$request = [
			's' => 'abcdef'
		];
		$this->json('GET', 'administrator/teacher/search', $request)
			->seeJsonStructure(['data', 'total', 'links'])
			->seeJsonContains([
				'total' => 1,
			]);
		$this->assertResponseOk();
	}
}