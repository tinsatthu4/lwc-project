<?php
namespace VuleAppsTest\LWcPortal\Controllers;

use League\Flysystem\Exception;
use VuleApps\LwcBackends\Models\DefaultPost;
use VuleApps\LwcBackends\Models\Term;
use VuleApps\LwcBackends\Models\TermRelation;
use VuleApps\LwcPortal\Models\Counrse;
use VuleAppsTest\TestCase;
use Auth;
use App\User;
use DB;
use TermRepository;

class CommonControllerTest extends TestCase
{
    function reset() {
        DB::table('posts')->truncate();
        DB::table('terms')->truncate();
        DB::table('term_relations')->truncate();
    }

    public static function additionSlug()
    {
        return [
            ['event'],
            ['news'],
            ['announcements']
        ];
    }

    /**
     * @dataProvider additionSlug
     * @group CommonController
     */
    function testGetPostsBySlugSuccess($slug)
    {
        $this->reset();
        $this->withoutMiddleware();

        $term = new Term();
        $term->id = 1;
        $term->slug = $slug;
        TermRepository::shouldReceive('getBySlug')
            ->andReturn($term);
        $post = DefaultPost::create([
            'title' => 'test',
            'type'  => 'post'
        ]);
        TermRelation::create([
            'post_id'   => $post->id,
            'term_id'   => $term->id
        ]);

        $response = $this->call('GET', 'api/'. $term->slug);
        $this->seeJsonStructure([
            'data', 'links', 'total']);
        $this->assertResponseOk();

        $data = json_decode($response->getContent(), true);
        foreach ($data['data'] as $d) {
            $this->assertNotNull(TermRelation::where([
                'post_id' => $d['id'],
                'term_id' => $term->id
            ])->first());
        }
    }

    /**
     * @dataProvider additionSlug
     * @group CommonController
     */
    function testGetPostsBySlugEmpty($slug)
    {
        $this->reset();
        $this->withoutMiddleware();

        $term = new Term();
        $term->id = 1;
        $term->slug = $slug;
        TermRepository::shouldReceive('getBySlug')
            ->andReturn($term);
        $post = DefaultPost::create([
            'title' => 'test',
            'type'  => 'post'
        ]);
        TermRelation::create([
            'post_id'   => $post->id + 1,
            'term_id'   => $term->id
        ]);

        $response = $this->call('GET', 'api/'. $term->slug);
        $this->seeJsonStructure([
            'data', 'links', 'total']);
        $data = json_decode($response->getContent(), true);
        $this->assertEquals($data['data'], []);
        $this->seeJsonStructure(['data', 'links', 'total']);
        $this->assertResponseOk();
    }

    /**
     * @dataProvider additionSlug
     * @group CommonController
     */
    function testGetPostsBySlugFail($slug)
    {
        $this->reset();
        $this->withoutMiddleware();

        $term = new Term();
        $term->id = 1;
        $term->slug = $slug;
        TermRepository::shouldReceive('getBySlug')
            ->andReturn(null);
        $post = DefaultPost::create([
            'title' => 'test',
            'type'  => 'post'
        ]);
        TermRelation::create([
            'post_id'   => $post->id,
            'term_id'   => $term->id
        ]);

        $response = $this->call('GET', 'api/'. $term->slug);
        $this->seeStatusCode(404);
    }
}