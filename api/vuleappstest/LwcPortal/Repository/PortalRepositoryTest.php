<?php
namespace VuleAppsTest\LWcPortal\Repository;
use VuleApps\LwcPortal\Models\Counrse;
use VuleApps\LwcPortal\Models\Module;
use VuleAppsTest\TestCase;
use VuleApps\LwcPortal\Models\Task;
use DB;
use VuleApps\LwcPortal\Repository\PortalRepository;

class PortalRepositoryTest extends TestCase {
	protected function initData() {
		DB::table('tasks')->truncate();
		DB::table('posts')->truncate();
		DB::table('user_counrses')->truncate();
		DB::table('posts')
			->insert([
				[
					'title' => 'Course',
					'parent_id' => 0,
					'type' => Counrse::TYPE
				],
				[
					'title' => 'lession',
					'parent_id' => 1,
					'type' => Module::TYPE
				]
			]);
		DB::table('user_counrses')->insert(['user_id' => 1, 'post_id' => 1]);
		DB::table('tasks')->insert([
			['post_id' => 2],
			['post_id' => 2],
			['post_id' => 2],
		]);
	}

	/**
	 * @group PortalRepositoryTest
	 */
	function testGroupTaskIDsByCourse() {
		$this->initData();
		$arr = app(PortalRepository::class)->groupTaskIDsByCourse(1);

		$this->assertNotEmpty($arr);
		$this->assertEquals([1,2,3], $arr);

		$arr = app(PortalRepository::class)->groupTaskIDsByCourse(2);
		$this->assertEmpty($arr);
	}
}