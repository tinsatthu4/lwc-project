<?php

namespace VuleApps\Common\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Image;

class ScaleImage extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
	protected $path;
	protected $config;
	protected $override;
	/**
	 * @param $path mean relative path to public_path()
	 * @param $config
	 * @param boolean $override
	 */
    public function __construct($path, $config, $override = true)
    {
        //
		$this->path = $path;
		$this->config = $config;
		$this->override = $override;
    }

	/**
	 * @return mean
	 */
	public function getPath()
	{
		return $this->path;
	}

	/**
	 * @param mean $path
	 */
	public function setPath($path)
	{
		$this->path = $path;
	}

	/**
	 * @return mixed
	 */
	public function getConfig()
	{
		return $this->config;
	}

	/**
	 * @param mixed $config
	 */
	public function setConfig($config)
	{
		$this->config = $config;
	}



    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
		foreach($this->config as $config) {
			try {
				$image = Image::make(public_path($this->path));
				if(!preg_match('/^(c|r)?(\d+)x(\d+)$/i', $config, $matches))
					continue;
				$PathToNewName = public_path(dirname($this->path) . "/" . preg_replace('/^(.*)\.(.*)$/', "$1-{$config}.$2", basename($this->path)));
				if(! $this->override && file_exists($PathToNewName))
					continue;
				switch($matches[1]) {
					case "c":
						$image->crop($matches['2'], $matches[3])
							->save($PathToNewName);
						break;
					default:
						if((int) $matches[3] === 0) {
							$image->resize($matches['2'], null, function($constraint) {
								$constraint->aspectRatio();
							})
								->save($PathToNewName);
						} elseif((int) $matches[2] === 0) {
							$image->resize(null, $matches['3'], function($constraint) {
								$constraint->aspectRatio();
							})
								->save($PathToNewName);
						} else {
							$image->resize($matches['2'], $matches['3'])
								->save($PathToNewName);
						}

						break;
				}
			} catch ( \Exception $e) {

			}
		}
    }
}
