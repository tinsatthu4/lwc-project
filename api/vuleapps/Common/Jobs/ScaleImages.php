<?php

namespace VuleApps\Common\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Image;

class ScaleImages extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
	protected $paths;
	protected $config;
	protected $override;
	/**
	 * @param array $paths relative path to public_path()
	 * @param $config
	 * @param boolean $override
	 */
    public function __construct($paths, $config, $override = true)
    {
        //
		$this->paths = $paths;
		$this->config = $config;
		$this->override = $override;
    }

	/**
	 * @return mean
	 */
	public function getPaths()
	{
		return $this->paths;
	}

	/**
	 * @param $paths
	 */
	public function setPaths($paths)
	{
		$this->paths = $paths;
	}

	/**
	 * @return mixed
	 */
	public function getConfig()
	{
		return $this->config;
	}

	/**
	 * @param mixed $config
	 */
	public function setConfig($config)
	{
		$this->config = $config;
	}



    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
		foreach($this->paths as $path) {
			foreach($this->config as $config) {
				try {
					$image = Image::make(public_path($path));
					if(!preg_match('/^(c|r)?(\d+)x(\d+)$/i', $config, $matches))
						continue;
					$PathToNewName = public_path(dirname($path) . "/" . preg_replace('/^(.*)\.(.*)$/', "$1-{$config}.$2", basename($path)));
					if(! $this->override && file_exists($PathToNewName))
						continue;
					switch($matches[1]) {
						case "c":
							$image->crop($matches['2'], $matches[3])
								->save($PathToNewName);
							break;
						default:
							if((int) $matches[3] === 0) {
								$image->resize($matches['2'], null, function($constraint) {
									$constraint->aspectRatio();
								})
									->save($PathToNewName);
							} elseif((int) $matches[2] === 0) {
								$image->resize(null, $matches['3'], function($constraint) {
									$constraint->aspectRatio();
								})
									->save($PathToNewName);
							} else {
								$image->resize($matches['2'], $matches['3'])
									->save($PathToNewName);
							}

							break;
					}
				} catch ( \Exception $e) {

				}
			}
		}
    }
}
