<?php
function vl_asset($path, $secure = null) {
	$newPath = dirname($path) . '/' . filemtime(public_path($path)). '.' . basename($path);
	if(file_exists(public_path($newPath)))
		return asset($newPath, $secure);

	file_put_contents(public_path($newPath), file_get_contents(public_path($path)));

	return asset($newPath, $secure);
}

function vl_admin_route($name, $parameters = [], $absolute = true) {
	return route("admin::{$name}", $parameters, $absolute);
}

function vl_admin_resoure_route($name, $parameters = [], $absolute = true) {
	$prefix = config("app.admin_prefix");
	return route("admin::{$prefix}.{$name}", $parameters, $absolute);
}

function vl_file_slug($fileNameWithExtension) {
	preg_match("/^(.*)\.([a-z0-9]+)$/i", $fileNameWithExtension, $matches);
	return str_slug($matches[1]). ".{$matches[2]}";
}

function vl_image_slug($fileNameWithExtension) {
	preg_match("/^(.*)\.(jpg|jpeg|png|gif)$/i", $fileNameWithExtension, $matches);
	return str_slug($matches[1]). ".{$matches[2]}";
}

function vl_form_box($label, $body = array()) {

}

function vl_form_thumbnail($name, $default = '') {
	return view('vuleappsCommon::components.form.thumbnail')
		->withName($name)
		->withDefault($default)
		->render();
}

function vl_form_images($name, $default = array(), $label = 'Images',$id = 'form-product-images') {
	return view('vuleappsCommon::components.form.images')
		->withLabel($label)
		->withId($id)
		->withName($name)
		->withDefault($default)
		->render();
}

function vl_form_bsText($label, $name, $value , $attrs = [], $view = 'vuleappsCommon::components.form.text') {
	return view($view)
			->withLabel($label)
			->withName($name)
			->withValue($value)
			->withAttrs($attrs)
			->render();
}

function vl_form_bsSelect($label, $name, $options = array(), $value, $attrs = [], $view = 'vuleappsCommon::components.form.select') {
	return view($view)
		->withLabel($label)
		->withName($name)
		->withOptions($options)
		->withValue($value)
		->withAttrs($attrs)
		->render();
}

function vl_form_bsTextarea($label, $name, $value , $attrs = [], $view = 'vuleappsCommon::components.form.textarea') {
	return view($view)
			->withLabel($label)
			->withName($name)
			->withValue($value)
			->withAttrs($attrs)
			->render();
}

/**
 * @param array $collection
 * @param string $name
 * @param array $value
 * @param string $style
 * @return string
 */
function vl_collections_dropdown($collection, $name, $value = array(), $style = "checkbox") {
	return view("vuleappsCommon::components.form.dropdown-checkbox")
		->withName($name)
		->withCollection($collection)
		->withValue($value)
		->render();
}

function vl_thumb($path, $size = null) {
	if($size === null)
		return asset($path);

	return asset(preg_replace('/^(.*)\.(\w+)$/i', "$1-" . $size . ".$2", $path));
}

function vl_excerpt($str, $startPos = 0, $maxLength = 100) {
    if(strlen($str) > $maxLength) {
        $excerpt   = substr($str, $startPos, $maxLength-3);
        $lastSpace = strrpos($excerpt, ' ');
        $excerpt   = substr($excerpt, 0, $lastSpace);
        $excerpt  .= '...';
    } else {
        $excerpt = $str;
    }
    return $excerpt;
}

function vl_langslug($url, $langcode = null, $attributes = array(), $https = null)
{
    $url = URL::to($url, $https);

    if (is_null($langcode)) $langcode = $url;

    return $url;
}