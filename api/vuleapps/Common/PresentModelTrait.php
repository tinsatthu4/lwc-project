<?php
namespace VuleApps\Common;

trait PresentModelTrait {
	protected $objectPresent = null;
	function present() {
		if($this->objectPresent === null) {
			$this->objectPresent = new $this->presenter($this);
			return $this->objectPresent;
		}

		return $this->objectPresent;
	}
}