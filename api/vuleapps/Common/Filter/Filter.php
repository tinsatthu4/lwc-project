<?php

namespace VuleApps\Common\Filter;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class Filter
{
    protected $builder;

    protected $attributes;

    protected $stringFields = [];
    
    protected $limit = 20;
    
    protected $orderBy = [];

    protected $betweenValue = [];

    public function __construct($builder, array $attributes = [])
    {
        $this->builder = $builder;
        $this->attributes = $attributes;
    }

    public function getModel()
    {
        if($this->builder instanceof Builder) {
            return $this->builder->getModel();
        }
        if($this->builder instanceof Relation) {
            return $this->builder->getRelated();
        }
        return $this->builder;
    }

    public function getStringField()
    {
        return $this->stringFields;
    }

    public function setStringField($array = [])
    {
        $this->stringFields = $array;
        return $this;
    }

    public function getOrderBy()
    {
        return $this->orderBy;
    }

    public function setOrderBy($value)
    {
        $this->orderBy = $value;
        return $this;
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function setLimit($limit)
    {
        $this->limit = $limit;
        return $this;
    }

    private function select()
    {
        if(empty($this->attributes['select'])) {
            return $this;
        }

        $select = $this->attributes['select'];
        $results = array_intersect($this->getModel()->getFillable(), $select);
        $this->builder = $this->builder->select($results);

        return $this;
    }

    private function search()
    {
        if(empty($this->attributes['s'])) {
            return $this;
        }

        foreach($this->attributes['s'] as $key => $param) {
            if(in_array($key, $this->stringFields)) {
                $this->builder = $this->builder
                    ->where($key, 'LIKE', '%' . $param . '%');
                continue;
            }
            $this->builder = $this->builder->where($key, $param);
        }
        return $this;
    }

    private function orderBy()
    {
        if(empty($this->orderBy['sort']) && empty($this->orderBy['order'])) {
            $this->builder = $this->builder
                ->orderBy($this->getModel()->getKeyName(), 'asc');
            return $this;
        }
        $this->builder = $this->builder
            ->orderBy($this->orderBy['sort'], $this->orderBy['order']);
        return $this;
    }
    
    private function with()
    {
        if(empty($this->attributes['with']) || $this->builder->count() == 0) {
            return $this;
        }
        foreach($this->attributes['with'] as $with) {
            $this->builder = $this->call_with($with);
        }
        return $this;
    }

    private function call_with($relation)
    {
        if(empty($relation) || !method_exists($this->getModel(), $relation)) {
            return $this->builder;
        }
        return $this->builder->with($relation);
    }

    private function between()
    {
        if (empty($this->attributes['between'])) {
            return $this;
        }
        foreach($this->attributes['between'] as $key => $between) {
            if(empty($between['from']) || empty($between['to'])) {
                continue;
            }
            if(gettype($between['from']) != gettype($between['to'])) {
                continue;
            }
            if($between['from'] > $between['to']) {
                continue;
            }
            $this->builder = $this->builder->whereBetween($key, [
                $between['from'],
                $between['to']
            ]);
        }
        return $this;
    }

    public function filter()
    {
        $this->select()->search()->between()->orderBy()->with();
        return $this->builder->paginate($this->limit);
    }
}