<?php
return [
	'status' => ['publish' => "Xuất Bản", 'draft' => "Lưu tạm"],
	'uploads_product_path' => 'uploads/san-pham',
	'paginate' => [
		'default' => 50,
		'product_admin' => 30,
		'category_admin' => 30
	],
    'user_roles' => [
        'admin'         => 'Admin',
        'member'        => 'Member',
		'learner'		=> 'Learner',
		'teacher_1'		=> 'Teacher 1',
		'teacher_2'		=> 'Teacher 2'
    ],
    'type_post' => [
        'counrse'           => 'Khóa học online',
        'counrse_offline'   => 'Khóa học offline',
        'ebook'             => 'Tài liệu',
        'forum'             => 'Forum',
        'post'              => 'Post'
    ]
];