<?php
namespace VuleApps\Common;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Route;

class RouteServiceProvider extends ServiceProvider
{
	/**
	 * Define your route model bindings, pattern filters, etc.
	 *
	 * @param  \Illuminate\Routing\Router  $router
	 * @return void
	 */
	public function boot(Router $router)
	{
		parent::boot($router);
	}

	/**
	 * Define the routes for the application.
	 *
	 * @param  \Illuminate\Routing\Router  $router
	 * @return void
	 */
	public function map(Router $router)
	{
		$this->mapWebRoutes($router);
	}

	/**
	 * Define the "web" routes for the application.
	 *
	 * These routes all receive session state, CSRF protection, etc.
	 *
	 * @param  \Illuminate\Routing\Router  $router
	 * @return void
	 */
	protected function mapWebRoutes(Router $router)
	{
		//handle ERROR
		$router->group([
			'prefix' => "api/" . config("app.admin_prefix"),
			'as' => 'admin::',
			'middleware' => 'webadmin'
		], function($router) {
			$router->controller('filemanager', \VuleApps\Common\Http\Controllers\Filemanager::class);
		});
	}
}
