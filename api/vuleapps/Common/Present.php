<?php
namespace VuleApps\Common;

use VuleApps\LwcBackends\Models\Category;
use VuleApps\LwcBackends\Models\DefaultPost;
use VuleApps\LwcBackends\Models\Product;

class Present {
	/**
	 * @var Model
	 */
	public $model;

	function __construct($_model) {
		$this->model = $_model;
	}

	function thumbPath($path, $size = null) {
		if($size === null)
			return asset($path);

		return asset(preg_replace('/^(.*)\.(\w+)$/i', "$1-" . $size . ".$2", $path));
	}

	function thumb($size = null) {
		return $this->thumbPath($this->model->image, $size);
	}

	function price() {
		return number_format($this->model->price, 0, ".", ",");
	}

	function price_retail() {
		return number_format($this->model->price,  0, ".", ",");
	}

	function status() {
		if($this->model->status == 'publish')
			return "<labe class=\"label label-success\">Xuất Bản</labe>";
		return "<labe class=\"label label-warning\">Lưu tạm</labe>";

	}

	function href() {
//		if($this->model instanceof DefaultPost)
//			return route("frontend::post.detail", [$this->model->slug, $this->model->id]);
		return '#';
	}

	function action() {
	}

	public function created_at($format = "d-m-Y H:i:s") {
		return date_format($this->model->created_at, $format);
	}

	public function __get($name) {
		if(method_exists($this, $name))
			return call_user_func([$this, $name]);

		return $this->model->{$name};
	}
}