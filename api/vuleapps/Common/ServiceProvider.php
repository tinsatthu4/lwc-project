<?php
namespace VuleApps\Common;
use Illuminate\Support\ServiceProvider as LaravelServiceProvider;
use VuleApps\Common\Repositories\Image;
use Form;

class ServiceProvider extends LaravelServiceProvider {

	public function boot() {
		$this->loadViewsFrom(__DIR__ . '/views', 'vuleappsCommon');
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		// TODO: Implement register() method.
		$this->mergeConfigFrom(__DIR__ . '/config/image.php', 'vuleapps.image');
		$this->mergeConfigFrom(__DIR__ . '/config/common.php', 'vuleapps.common');

		$this->app->singleton(Image::class,  function() {
			return new Image();
		});
	}
}