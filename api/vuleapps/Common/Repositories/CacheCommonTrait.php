<?php
namespace VuleApps\Common\Repositories;
use Cache;

trait CacheCommonTrait {
	function get($args = []) {
		$key = "{$this->key}.get.".md5(json_encode($args));
		if(Cache::has($key))
			return Cache::get($key);
		$this->buildArgs($args);

		$data = $this->model->get();
		Cache::forever($key, $data);
		return $data;
	}
}