<?php
namespace VuleApps\Common\Repositories;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class Image {
	protected $dir = "uploads/products";

	function __construct() {
		$this->dir = config('vuleapps.common.uploads_product_path');
	}

	/**
	 * Return Full path
	 * @return string
	 */
	function getDir() {
		/**
		 * Random Date
		 */
		$date = date("Y/m/d");
//		$date = "2016/". rand(1, 12) . "/" . rand(1, 30);
		return public_path("{$this->dir}/" . date("Y/m/d"));
	}

	/**
	 * @param UploadedFile $file
	 * @return array
	 */
	function save(UploadedFile $file) {
		$newFile = $file->move($this->getDir(), vl_image_slug($file->getClientOriginalName()));
		$path = trim(str_replace("\\", "/", str_replace(public_path(), "", $newFile->getPathname())), "/");

		return [
			'path' => $path,
			'url' => asset($path)
		];
	}
}