<?php
namespace VuleApps\Common\Repositories;

use Illuminate\Database\Eloquent\Model;

abstract class Repository {
	/**
	 * @var Model
	 */
	public $model;

	function __construct(Model $model) {
		$this->model = $model;
	}

	function buildArgsFromModel(&$args = array(), &$model) {
		if(!isset($args['order_by'])) {
			$model = $model->orderBy("sort", "ASC");
		} else {
			$model = $model->orderBy($args['order_by'], $args['order']);
		}

		if(isset($args['term_id']) && $args['term_id'] != 0)
			$model = $model->whereHas("termRelations", function($q) use ($args) {
				$q->where('term_id', $args['term_id']);
			});

		if(isset($args['take']))
		{
			$model = $model->take((int) $args['take']);
		}

		if(isset($args['fields']))
			$model = $model->select($args['fields']);

		if(isset($args['s']) && !empty($args['s']))
			$model = $model->whereRaw("MATCH(title) AGAINST(? IN BOOLEAN MODE)", array($args['s']));

		if(isset($args['exclude'])) {
			$model = $model->where($args['exclude']['field'], '<>', $args['exclude']['value']);
		}
	}

	/**
	 * @param array $args
	 * @param $newInstance
	 * @return $this|Model
	 */
	function buildArgs(&$args = array(), $newInstance = false) {
		$model = $this->model;

		$this->buildArgsFromModel($args, $model);

		if($newInstance) {
			return $model;
		} else {
			$this->model = $model;
		}

		return $this;
	}

	/**
	 * @param array $input
	 * @return Model
	 */
	function store($input = array()) {
		$this->model = $this->model->create($input);
		return $this->model;
	}

	function update($input) {
		$this->model->fill($input);
		$this->model->save();

		return $this->model;
	}

	function destroy() {
		$this->model->delete();
	}


	/**
	 * @param Model $model
	 */
	public function setModel($model)
	{
		$this->model = $model;
	}

	/**
	 * @return Model
	 */
	public function getModel()
	{
		return $this->model;
	}

	public function getById($id) {
		return $this->model->findOrFail($id);
	}

	function get($args = []) {
		$this->buildArgs($args);

		return $this->model->get();
	}
}