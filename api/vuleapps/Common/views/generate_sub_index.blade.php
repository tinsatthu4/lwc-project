@section('js')
    <script src="{{ asset('media/generate_sub.js') }}"></script>
@stop
@extends('master')
@section('main')
<form action="{{ action('\VuleApps\Common\Http\Controllers\GenerateSub@postIndex') }}" method="post">
<div class="row">
    <div class="col-xs-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">
                    Generate Subtitle
                </h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6 col-sm-6 form-inline">
                        <h3>Options</h3>
                        <div id="wrapper-option">
                            <div class="form-group">
                                {{--<label for="">Seconds</label>--}}
                                {{--<input type="text" name="second" value="6" class="form-control"/>--}}
                                <label for="">From</label>
                                <input type="text" name="from[]" value="1" class="form-control"/>
                                <label for="">To</label>
                                <input type="text" name="to[]" value="10" class="form-control"/>
                                <label for="">Second</label>
                                <input type="text" name="second[]" value="6" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <button id="add-more-option" class="btn btn-primary">Add more option</button>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="form-group">
                            <label for="">Nội Dung(plain text) :</label>
                            <textarea name="content" id="" cols="30" rows="10" class="form-control"></textarea>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn-primary" id="preview-generate-sub">Preview</button>
                    <button type="submit" class="btn btn-success">Download as File .srt</button>
                </div>
                <div class="form-group">
                    <div id="example-date">
                        <textarea name="preview_generate_sub" cols="30" rows="20" class="form-control"></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</form>
@stop
