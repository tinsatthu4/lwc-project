<div class="box {{ $class or 'box-default' }}">
    @if(isset($title))
    <div class="box-header with-border"><h3 class="box-title">{{ $title }}</h3></div>
    @endif
    <div class="box-body">

