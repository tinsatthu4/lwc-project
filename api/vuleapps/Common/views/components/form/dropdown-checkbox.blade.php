@foreach($collection as $item)
    <div style="margin-bottom: 10px; {{ !$item->level?null:sprintf("padding-left:%dpx", $item->level*22) }}">
        <input name="{{$name}}" type="checkbox" value="{{$item->id}}" class="minimal icheckbox" {{ !in_array($item->id, $value)?:"checked" }}/>
        <label for="">{{ $item->title }}</label>
    </div>
@endforeach
