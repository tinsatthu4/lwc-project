<div class="box box-default" id="{{ $id }}">
    <div class="box-header">
        <h3 class="box-title">{{ $label }}</h3>
    </div>
    <div class="box-body">
        <div class="form-group images-form">
            <div class="box-images">
                @foreach($default as $img)
                <div class="box-image col-md-3">
                    <img src="{{ vl_thumb($img, '150x0') }}" />
                    <input type="hidden" name="{{ $name }}" value="{{ $img }}"/>
                    <a href="javascript:void()" title="remove image" class="remove-image"><i class="fa fa-remove"></i></a>
                </div>
                @endforeach
            </div>
            <div class="clearfix"></div>
            <div class="box-upload">
                <i class="fa fa-upload"></i>
                <input type="file" class="images-input" data-el="#{{ $id }}" data-name="{{ $name }}" data-upload="{{ action('\VuleApps\Common\Http\Controllers\Filemanager@postUploadImage') }}" accept="image/*"/>
            </div>
        </div>
    </div>
</div>
