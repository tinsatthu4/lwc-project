<div class="box box-default">
    <div class="box-header">
        <h3 class="box-title">Thumbnail</h3>
    </div>
    <div class="box-body">
        <div class="form-group thumbnail-form">
            <i class="fa fa-upload"></i>
            <input type="file" class="thumbnail-input" data-default="{{$default}}" data-upload="{{ action('\VuleApps\Common\Http\Controllers\Filemanager@postUploadImage') }}" data-el="{{$name}}" accept="image/*"/>
            <input type="hidden" name="{{ $name }}"/>
            <img src="#" class="hide"/>
        </div>
    </div>
</div>
