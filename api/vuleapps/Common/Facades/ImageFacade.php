<?php
namespace VuleApps\Common\Facades;

use Illuminate\Support\Facades\Facade;
use VuleApps\Common\Repositories\Image;

class ImageFacade extends Facade {
	protected static function getFacadeAccessor() {
		return Image::class;
	}
}