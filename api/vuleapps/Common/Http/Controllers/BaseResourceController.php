<?php

namespace VuleApps\Common\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Routing\Router;
use Illuminate\Validation\ValidationException;
use VuleApps\Common\Repositories\Repository;

class BaseResourceController extends Controller
{
	/**
	 * @var Repository
	 */
	public $repository;

	public $rules;

	public $module;

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		$args = $request->all();
		return view("{$this->module}.index")
				->withModels(
					$this->repository
					->buildArgs($args)
					->getModel()
					->paginate(config("vuleapps.common.paginate.default"))
				);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		return view("{$this->module}.form")
				->withModel(null);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Router $router)
    {
        try {
			$this->validate($request, $this->rules);
			$model = $this->repository->store($request->all());
			if($request->isJson())
				return response()->json(['data' => $model]);
			return redirect()
				->route(str_replace('.store', '.index', $router->currentRouteName()))
				->withSuccess(true);
		} catch (ValidationException $e) {
			if($request->isJson())
				return response()
						->json(['message' => $e->validator->messages()], 400);
			return redirect()
				->back()
				->withErrors($e->validator->messages())
				->withInput();
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  Model  $model
     * @return \Illuminate\Http\Response
     */
    public function show(Model $model)
    {
        //
		return response()->json(['data' => $model]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Model $model
     * @return \Illuminate\Http\Response
     */
    public function edit(Model $model)
    {
		return view("{$this->module}.form")
			->withModel($model);
	}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Model  $model
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Model $model, Router $router)
    {
		try {
			$this->validate($request, $this->rules);
			$this->repository->setModel($model);
			$this->repository->update($request->all());

			if($request->isJson())
				return response()->json(['data' => $this->repository->getModel()]);

			return redirect()
				->route(str_replace('.update', '.edit', $router->currentRouteName()), $model->id)
				->withSuccess(true);

		} catch (ValidationException $e) {
			if($request->isJson())
				return response()
					->json(['message' => $e->validator->messages()], 400);

			return redirect()
				->back()
				->withErrors($e->validator->messages())
				->withInput();
		}
    }

    /**
     * Remove the specified resource from storage.
     * @param  Model  $model
     * @return \Illuminate\Http\Response
     */
    public function destroy(Model $model)
    {
		$this->repository->setModel($model);
		try {
			$this->repository->destroy();
			return response(null, 204);
		}catch(\Exception $e ) {
			return response(null, 400);
		}
	}

    /**
     * Active Post
     * @param Model $model
     * @return \Illuminate\Http\RedirectResponse
     */
    public function active(Model $model)
    {
        $this->repository->setModel($model);
        $this->repository->update([
            'status' => 'publish'
        ]);
        return redirect()->back();
    }

    /**
     * Deactive Post
     * @param Model $model
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deactive(Model $model)
    {
        $this->repository->setModel($model);
        $this->repository->update([
            'status' => 'draft'
        ]);
        return redirect()->back();
    }
}
