<?php
namespace VuleApps\Common\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Validation\Validator;

class Common extends Controller {
	function dashboard() {
		return view('vuleappsCommon::dashboard');
	}
}