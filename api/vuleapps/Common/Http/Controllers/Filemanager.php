<?php
namespace VuleApps\Common\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use VuleApps\Common\Jobs\ScaleImage;
use VuleApps\Common\Repositories\Image as ImageRepository;
class Filemanager extends Controller {
	protected $imageRepo;
	function __construct(ImageRepository $image) {
		$this->imageRepo = $image;
	}

	/**
	 * Upload single file
	 * File name input file
	 * @param Request $request
	 * @param ImageRepository $image
	 * @return \Illuminate\Http\JsonResponse
	 */
	function postUploadImage(Request $request, ImageRepository $image) {
		try {
			$this->validate($request, [
				'file' => 'required|image'
			]);
			$file = $request->file('file');
			$rs = $image->save($file);
			$this->dispatchNow(new ScaleImage($rs['path'], config('vuleapps.image.default')));
			$this->dispatchNow(new ScaleImage($rs['path'], config('vuleapps.image.theme')));

			return response()->json(['data' => $rs ]);
		} catch(ValidationException $e) {
			return response()->json(['messages' => $e->validator->messages()], 400);
		}
	}

	//save file to storage_path
	function postUploadStorage(Request $request) {
		try {
			$this->validate($request, [
				'file' => 'required'
			]);

			$file = $request->file('file');
			$date = date('Y/m/d');
			$newFile = $file->move(storage_path("uploads/{$date}"), $file->getClientOriginalName());
			return response()->json(['data' => [
				'path' => trim(str_replace("\\", "/", str_replace(storage_path(), "", $newFile->getPathname())), "/"),
				'fullpath' => $newFile->getRealPath()
			] ]);
		} catch(ValidationException $e) {
			return response()->json(['messages' => $e->validator->messages()], 400);
		}
	}

	/**
	 * Upload multiple file
	 * File name array files
	 * @param Request $request
	 * @param ImageRepository $image
	 * @return \Illuminate\Http\JsonResponse
	 */
	function postUploadImages(Request $request, ImageRepository $image) {
		try {
			$this->validate($request, [
				"files" => 'required',
				"files.*" => 'required|image'
			]);
			$rs = [];
			$files = $request->file('files');

			foreach($files as $file) {
				$img = $image->save($file);
				$rs[] = $img;
				$this->dispatch(new ScaleImage($img['path'], config('vuleapps.image.default')));
				$this->dispatchNow(new ScaleImage($img['path'], config('vuleapps.image.theme')));
			}

			return response()->json(['data' => $rs]);
		} catch (ValidationException $e) {
			return response()->json(['messages' => $e->validator->messages() ], 400);
		}
	}

    public function getDownload(Request $request)
    {
        try {
            $this->validate($request, [
                "path" => 'required'
            ]);
            $file = storage_path($request->get('path'));
            return response()->download($file);
        }
        catch(ValidationException $e) {
            return response()->json(['messages' => $e->validator->messages() ], 400);
        }
    }
}