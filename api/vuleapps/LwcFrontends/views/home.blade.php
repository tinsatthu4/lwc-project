@extends("lwcfrontend.master")
@section('main')
<div id="content-wrapper">
    @include('vuleappsLwcFrontends::widgets.slider')
    <?php $config = SettingRepository::loadGroup('webinfo');?>
    @if(isset($config['webinfo']['course']))
    <div class="bg-2">
        <!-- TT-TWO-BLOCK -->
        <div class="tt-two-blocks">
            <div class="container">
                <div class="empty-space marg-lg-b75 marg-sm-b50 marg-xs-b30"></div>
                <div class="tt-two-blocks-section background-block" style="background-image:url('{{asset('lwcfrontend/img/two-block/bg.jpg')}}');">
                </div>
                <div class="row">
                    <div class="col-md-7 col-lg-6">
                        <div class="tt-banner">
                            <h3 class="tt-banner-title c-h3"><small>@if($config['webinfo']['course']['name']){{ $config['webinfo']['course']['name'] }}@endif</small></h3>
                            <div class="tt-banner-label">{{ trans('static.home.registration') }}</div>
                            <div class="tt-banner-desc"><i class="fa fa-hand-o-right" aria-hidden="true"></i>{{ trans('static.home.description_registration') }}</div>
                        </div>
                        <div class="tt-timer clearfix" data-finaldate="@if($config['webinfo']['course']['time']){{ $config['webinfo']['course']['time'] }}@endif">
                            <div class="tt-timer-entry">
                                <div class="tt-timer-count days"></div>
                                <div class="tt-timer-text">{{ trans('static.home.days') }}</div>
                            </div>
                            <div class="tt-timer-entry">
                                <div class="tt-timer-count hours"></div>
                                <div class="tt-timer-text">{{ trans('static.home.hours') }}</div>
                            </div>
                            <div class="tt-timer-entry">
                                <div class="tt-timer-count minutes"></div>
                                <div class="tt-timer-text">{{ trans('static.home.minutes') }}</div>
                            </div>
                            <div class="tt-timer-entry">
                                <div class="tt-timer-count seconds"></div>
                                <div class="tt-timer-text">{{ trans('static.home.seconds') }}</div>
                            </div>
                        </div>
                        <div class="empty-space marg-sm-b30"></div>
                    </div>
                    <div class="col-md-4 col-md-offset-1 col-lg-4 col-lg-offset-2">
                        <!-- TT-HEDING -->
                        <div class="tt-heding type-2 color-2">
                            <h4 class="tt-heding-title c-h4 txt-uppercase"><span>{{ trans('static.home.sign_up_now') }}</span></h4>
                            <div class="tt-heding-desc">{{ trans('static.home.description_courses') }}</div>
                        </div>
                        <div class="empty-space marg-lg-b35 marg-sm-b30"></div>
                         @if($errors->has('error'))
                            <div class="alert alert-danger alert-dismissable">
                               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                               {{ $errors->first('error') }}
                            </div>
                        @endif
                        <form class="tt-sign-form" action="{{ route('course.register') }}" method="post">
                            {{ csrf_field() }}
                            <input class="c-input type-1" type="text" name="name" required="" placeholder="{{ trans('static.home.name') }}">
                            @if ($errors->has('name'))
                                <span style="color: red">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                            <input class="c-input type-1" type="text" name="email" required="" placeholder="{{ trans('static.home.email') }}">
                            @if ($errors->has('email'))
                                <span style="color: red">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                            <input type="hidden" name="post_id" value="@if(isset($config['webinfo']['course']['id'])){{ $config['webinfo']['course']['id'] }}@endif">
                            @if ($errors->has('post_id'))
                                <span style="color: red">
                                    <strong>{{ $errors->first('post_id') }}</strong>
                                </span>
                            @endif
                            <div class="c-btn type-1 color-2">
                                <input type="submit" value="{{ trans('static.home.get_free_quote') }}">
                                <i class="fa fa-arrow-right" aria-hidden="true"></i>
                            </div>
                        </form>

                    </div>
                </div>
                <div class="empty-space marg-lg-b75 marg-sm-b50 marg-xs-b30"></div>
            </div>
        </div>
    </div>
    @endif
    <div class="empty-space marg-lg-b75 marg-sm-b50 marg-xs-b30"></div>
</div>
@stop