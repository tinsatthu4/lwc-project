@extends("lwcfrontend.master")
@section('main')
@include('vuleappsFrontends::widgets.header-top')
@include('vuleappsFrontends::widgets.primary-nav')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @include('vuleappsFrontends::widgets.breadcrumbs', [
                'title' => $term->title,
                'data' => [
                    ['title' => $term->title ]
                ]
            ])
        </div>
        <div class="col-md-5 col-sm-5">
            @include('vuleappsFrontends::widgets.sidebar-default')
        </div>
        <div class="col-md-7 col-sm-7">
            <div class="col-right">
                <ul class="news-strip">
                    @foreach($models as $model)
                        @include('vuleappsFrontends::widgets.blogs')
                    @endforeach
                </ul>
                {{ $models->links() }}
            </div>
        </div>
    </div>
</div>
@stop
