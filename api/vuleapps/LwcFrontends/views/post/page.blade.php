@extends("lwcfrontend.master")
@section('main')
<div id="content-wrapper">
    @include('vuleappsLwcFrontends::widgets.breadcrumbs', [
        'title' => $model->present()->title,
        'data'  => [
            ['title' => $model->present()->title ]
        ]
    ])
    <div class="bg-1 tt-sidebar-right tt-overflow">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="tt-two-colors pright20">
                        <div class="empty-space marg-lg-b90 marg-sm-b50 marg-xs-b30"></div>
                        <!-- TT-POST -->
                        <div class="tt-post">
                            <div class="tt-post-img">
                                <img class="img-responsive" src="{{ $model->present()->thumb() }}" height="372" width="850" alt="">
                            </div>
                            <div class="tt-post-date">{{ $model->present()->created_at }}</div>
                        </div>
                        <div class="simple-text size-5 color-3">
                            <h5>{{ $model->present()->title }}</h5>
                            {!! $model->present()->content !!}
                        </div>
                        <div class="empty-space marg-lg-b45 marg-sm-b30"></div>
                    </div>
                    <div class="empty-space marg-lg-b75 marg-sm-b50 marg-xs-b30"></div>
                </div>
                <div class="col-md-3">
                      @include('vuleappsLwcFrontends::widgets.sidebar')
                </div>
                <div class="empty-space marg-lg-b75 marg-sm-b50 marg-xs-b30"></div>
            </div>
        </div>
    </div>
</div>
@stop
