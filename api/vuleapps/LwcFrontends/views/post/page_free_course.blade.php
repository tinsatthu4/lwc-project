@extends("lwcfrontend.master")
@section('main')
<div id="content-wrapper">
    @include('vuleappsLwcFrontends::widgets.breadcrumbs', [
        'title' => 'Free Courses',
        'data'  => [
            ['title' => 'Free Courses' ]
        ]
    ])
    <div class="bg-1 tt-sidebar-right tt-overflow">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="tt-two-colors pright20">
                        <div class="empty-space marg-lg-b90 marg-sm-b50 marg-xs-b30"></div>
                        <!-- TT-POST -->
                        <div class="simple-text size-5 color-3">
                            <h5>{{ trans('static.page.free_courses.free_courses') }}</h5>
                            <div class="row">
                                <form class="tt-sign-form" action="{{ route('course.register') }}" method="post">
                                    {{ csrf_field() }}
                                    <div class="col-sm-12">
                                        <div class="input-label">{{ trans('static.page.free_courses.courses') }} <span>*</span></div>
                                        <select class="c-input type-4 size-2 color-2" name="post_id">
                                            <option value="1">{{ trans('static.page.free_courses.tourism') }}</option>
                                            <option value="2">{{ trans('static.page.free_courses.business') }}</option>
                                            <option value="3">{{ trans('static.page.free_courses.accounting') }}</option>
                                            <option value="4">{{ trans('static.page.free_courses.finance') }}</option>
                                            <option value="5">{{ trans('static.page.free_courses.hospitality') }}</option>
                                            <option value="6">{{ trans('static.page.free_courses.marketing') }}</option>
                                        </select>
                                    </div>
                                    @if ($errors->has('post_id'))
                                        <span style="color: red">
                                            <strong>{{ $errors->first('post_id') }}</strong>
                                        </span>
                                    @endif
                                    <div class="col-sm-12">
                                        <div class="input-label">{{ trans('static.page.free_courses.name') }} <span>*</span></div>
                                        <input class="c-input type-4 size-2 color-2" type="text" name="name" required="" placeholder="{{ trans('static.page.free_courses.name') }}">
                                    </div>
                                    @if ($errors->has('name'))
                                        <span style="color: red">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                    <div class="col-sm-12">
                                        <div class="input-label">{{ trans('static.page.free_courses.email') }} <span>*</span></div>
                                        <input class="c-input type-4 size-2 color-2" type="text" name="email" required="" placeholder="{{ trans('static.page.free_courses.email') }}">
                                    </div>
                                    @if ($errors->has('email'))
                                        <span style="color: red">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                    <div class="empty-space marg-lg-b20"></div>
                                    <div class="c-btn type-1 color-2">
                                        <input type="submit" value="{{ trans('static.page.free_courses.get_free_quote') }}">
                                        <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="empty-space marg-lg-b45 marg-sm-b30"></div>
                    </div>
                    <div class="empty-space marg-lg-b75 marg-sm-b50 marg-xs-b30"></div>
                </div>
                <div class="col-md-3">
                      @include('vuleappsLwcFrontends::widgets.sidebar')
                </div>
            </div>
        </div>
    </div>
</div>
@stop
