@extends("lwcfrontend.master")
@section('main')
<div class="container">
    <div class="row">
        <div class="col-md-4 col-sm-4">
            <div class="col-left">
                <?php $config = SettingRepository::loadGroup('webinfo'); ?>
                @if(isset($config['webinfo']['contact_info']))
                {!! $config['webinfo']['contact_info'] !!}
                @endif
            </div>
        </div>
        <div class="col-md-8 col-sm-8">
            <div class="col-right">
                <p class="lead">
                    Liên hệ
                </p>
                <hr>
                <div id="message-contact"></div>
                @if($errors->has('error'))
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $errors->first('error') }}
                    </div>
                @endif
                <form method="post" action="">
                    {!! csrf_field() !!}
                    <div class="row">
                        <div class="col-md-6 form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                            <label>Name <span class="required">* </span></label>
                            <input type="text" value="{{ old('name') }}" name="name" class="form-control ie7-margin">
                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="col-md-6 form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                            <label>Email <span class="required">* </span></label>
                            <input type="text" value="{{ old('email') }}" name="email" class="form-control ie7-margin">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-group {{ $errors->has('address') ? ' has-error' : '' }}">
                            <label>Address <span class="required">* </span></label>
                            <input type="text" value="{{ old('address') }}" name="address" class="form-control ie7-margin">
                            @if ($errors->has('address'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('address') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="col-md-6 form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label>Phone <span class="required">* </span></label>
                            <input type="text" value="{{ old('phone') }}" name="phone" class="form-control ie7-margin">
                            @if ($errors->has('phone'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('phone') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Message <span class="required">*</span></label>
                            <textarea rows="5" name="message" class="form-control">{{ old('message') }}</textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="submit" id="submit-contact" value="Gửi" class=" button_medium">
                        </div>
                    </div>
                    <hr>
                </form>
            </div>
        </div>
    </div>
</div>


<!-- TT-TOPHEADING -->
<div class="tt-topheading background-block" style="background-image:url(img/heading/heading.jpg);">
    <div class="container">
        <h2 class="tt-topheading-title c-h3"><span>Contact us</span></h2>
        <ul class="tt-breadcrumbs">
            <li><a href="#"><i class="fa fa-home" aria-hidden="true"></i> HOME</a></li>
            <li><span>Contact us</span></li>
        </ul>
    </div>
</div>
<div class="empty-space marg-lg-b85 marg-sm-b50 marg-xs-b30"></div>

<div class="container">

    <!-- TT-HEDING -->
    <div class="tt-heding type-2">
        <h4 class="tt-heding-title c-h4 txt-uppercase"><span>Get in Touch</span></h4>
        <div class="tt-heding-desc">We Are Available</div>
    </div>
    <div class="empty-space marg-lg-b25"></div>

    <div class="simple-text size-4">
        <p>If you are interested in finding out more about how we can help your organization, please fill out the form below.<br>you can call us anytime between 4 A.M. to 2 P. M. GMT </p>
    </div>
    <div class="empty-space marg-lg-b50 marg-sm-b30"></div>

    <!-- TT-CONTACT -->
    <div class="tt-contact-wrapper">
        <div class="row">
            <div class="col-sm-4">
                <div class="tt-contact clearfix">
                    <div class="tt-contact-img"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
                    <div class="tt-contact-info">
                        MarkUp, 562, Mallin Street<br> New Youk, NY 100 254
                    </div>
                </div>
                <div class="empty-space marg-xs-b30"></div>
            </div>

            <div class="col-sm-4">
                <div class="tt-contact clearfix">
                    <div class="tt-contact-img"><i class="fa fa-envelope-o" aria-hidden="true"></i></div>
                    <div class="tt-contact-info">
                       <a href="mailto:info@education&learning.com">info@education&learning.com</a>
                       <a href="mailto:support@education.com">support@education.com</a>
                    </div>
                </div>
                <div class="empty-space marg-xs-b30"></div>
            </div>

            <div class="col-sm-4">
                <div class="tt-contact clearfix">
                    <div class="tt-contact-img"><i class="fa fa-phone" aria-hidden="true"></i></div>
                    <div class="tt-contact-info">
                       <a href="tel:18005622487">+ 1800 562 2487</a><br>
                       <a href="tel:32155468975">+ 3215 546 8975</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="empty-space marg-lg-b50 marg-sm-b30"></div>

    <div class="row">
        <div class="col-sm-6">
            <div class="tt-contact-form">
                <form onSubmit="return submitForm();" action="./" method="post" name="contactform" id="contact-form">
                    <input class="c-input type-3" type="text" required="" name="name" placeholder="First">
                    <input class="c-input type-3" type="text" required="" name="email" placeholder="Email">
                    <input class="c-input type-3" type="text" required="" name="subject" placeholder="Subject">
                    <textarea class="c-area type-1" required="" name="message" placeholder="Message"></textarea>
                    <div class="c-btn type-1 color-3">
                        <input type="submit" value="submit now">
                        <i class="fa fa-arrow-right" aria-hidden="true"></i>
                    </div>
                </form>
            </div>
            <div class="simple-text tt-request-success">
                <p></p>
            </div>
            <div class="empty-space marg-xs-b30"></div>
        </div>
        <div class="col-sm-6">
            <div class="tt-contact-map map-block" id="map-canvas"  data-lat="38.934274" data-lng="-78.198075" data-zoom="10"></div>
            <div class="addresses-block">
                <a data-lat="38.934274" data-lng="-78.198075" data-string="1. Here is some address or email or phone or something else..."></a>
            </div>
        </div>
    </div>
    <div class="empty-space marg-lg-b135 marg-md-b120 marg-sm-b100 marg-xs-b60"></div>
</div>
@stop