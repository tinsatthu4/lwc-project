@extends("lwcfrontend.master")
@section('main')
<div id="content-wrapper">
    <div class="empty-space marg-lg-b75 marg-sm-b50 marg-xs-b30"></div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="col-right">
                @if($errors->has('error'))
                    <div class="alert alert-danger alert-dismissable">
                       <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                       {{ $errors->first('error') }}
                    </div>
                @endif
                <form action="" method="POST">
                    {!! csrf_field() !!}
                    <div class="form-group {{ $errors->has('old_password') ? ' has-error' : '' }}">
                        <label for="old_password">{{ trans('static.member.change_password.old_password') }}</label>
                        <input type="password" name="old_password" placeholder="{{ trans('static.member.change_password.enter_old_password') }}" class="form-control" value="{{ old('old_password') }}">
                        @if ($errors->has('old_password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('old_password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="new_password">{{ trans('static.member.change_password.new_password') }}</label>
                        <input type="password" name="password" placeholder="{{ trans('static.member.change_password.enter_new_password') }}" class="form-control" value="{{ old('password') }}">
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        <label for="password_confirmation">{{ trans('static.member.change_password.confirm_password') }}</label>
                        <input type="password" name="password_confirmation" placeholder="{{ trans('static.member.change_password.enter_confirm_password') }}" class="form-control" value="{{ old('password_confirmation') }}">
                        @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary" value="{{ trans('static.member.change_password.save') }}">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="empty-space marg-lg-b75 marg-sm-b50 marg-xs-b30"></div>
</div>
@stop