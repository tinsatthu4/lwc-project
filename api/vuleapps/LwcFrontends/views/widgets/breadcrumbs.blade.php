<!-- TT-TOPHEADING -->
<div class="tt-topheading background-block" style="background-image:url('{{asset('lwcfrontend/img/heading/heading.jpg')}}');">
    <div class="container">
        @if(isset($title))
          <h2 class="tt-topheading-title c-h3"><span>{{ $title }}</span></h2>
        @endif
        <ul class="tt-breadcrumbs">
            <li><a href="{{ route('home') }}"><i class="fa fa-home" aria-hidden="true"></i> Home</a></li>
            @if(!empty($data))
              @foreach($data as $key => $item)
                  @if(!isset($data[$key + 1]))
                    <li><span>{{ $item['title'] }}</span></li>
                  @else
                    <li><a href="{{ $item['href'] }}" title="{{ $item['title'] }}">{{ $item['title'] }}</a></li>
                  @endif
              @endforeach
            @endif
        </ul>
    </div>
</div>
