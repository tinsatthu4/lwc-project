<!-- TT-MSLIDE -->
<?php
$data = app(\VuleApps\LwcBackends\Repositories\SettingRepository::class)->loadGroup('banners');
?>
@if(isset($data['banners']))
    <div class="swiper-container" data-autoplay="0" data-loop="1" data-speed="500" data-center="0" data-slides-per-view="1" data-add-slides="2">
        <div class="swiper-wrapper clearfix">
            @foreach($data['banners'] as $key => $banner)
                <div class="swiper-slide @if($key == 0) active @endif" data-val="{{ $key }}">
                    <div class="tt-mslide" style="background-image:url({{ vl_thumb($banner['path'], '1152x420') }});">
                        <div class="container-fluid">
                            <div class="tt-mslide-table">
                                <div class="tt-mslide-cell">
                                    <div class="row">
                                        <div class="col-md-6 col-lg-5">
                                            <h1 class="tt-mslide-title c-h1 txt-uppercase">{{ $banner['title'] }}</h1>
                                            <div class="tt-mslide-btn">
                                                <a class="c-btn type-1 color-2" href="{{ $banner['link'] }}"><span>{{ trans('static.widgets.slider.read_more') }}<i class="fa fa-arrow-right" aria-hidden="true"></i></span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="pagination type-1 pos-1 visible-xs-block"></div>
        <div class="swiper-arrow-left tt-arrow-left type-2 pos-2 hidden-xs"><i class="fa fa-angle-left" aria-hidden="true"></i></div>
        <div class="swiper-arrow-right tt-arrow-right type-2 pos-2 hidden-xs"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
    </div>
@endif