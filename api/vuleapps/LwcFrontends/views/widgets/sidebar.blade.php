<div class="pleft20">
<div class="empty-space marg-lg-b90 marg-sm-b50 marg-xs-b30"></div>
<!-- TT-HEDING -->
<div class="tt-heding type-3">
    <h5 class="tt-heding-title c-h5 txt-uppercase"><span><small>{{ trans('static.page.sidebar.all_courses') }}</small></span></h5>
</div>
<div class="empty-space marg-lg-b10"></div>
<!-- TT-SLIST -->
<ul class="tt-slist">
    <li><a href="{{ route('getPageBySlug', ['tourism']) }}">{{ trans('static.page.sidebar.tourism') }}</a></li>
    <li><a href="{{ route('getPageBySlug', ['business']) }}">{{ trans('static.page.sidebar.business') }}</a></li>
    <li><a href="{{ route('getPageBySlug', ['accounting']) }}">{{ trans('static.page.sidebar.accounting') }}</a></li>
    <li><a href="{{ route('getPageBySlug', ['finance']) }}">{{ trans('static.page.sidebar.finance') }}</a></li>
    <li><a href="{{ route('getPageBySlug', ['hospitality']) }}">{{ trans('static.page.sidebar.hospitality') }}</a></li>
    <li><a href="{{ route('getPageBySlug', ['marketing']) }}">{{ trans('static.page.sidebar.marketing') }}</a></li>
</ul>
<div class="empty-space marg-lg-b40 marg-xs-b30"></div>
@if(isset($model->meta_keywords))
<!-- TT-HEDING -->
<div class="tt-heding type-3">
    <h5 class="tt-heding-title c-h5 txt-uppercase"><span><small>{{ trans('static.page.sidebar.tag_cloud') }}</small></span></h5>
</div>
<div class="empty-space marg-lg-b10"></div>
<!-- TT-LLOUD -->
<ul class="tt-cloud-wrapper">
    @foreach(explode(',', $model->meta_keywords) as $keyword)
        <li>
            <a class="tt-cloud" href="{{ route('search', ['s' => trim($keyword)]) }}">
                {{ trim($keyword) }}
            </a>
        </li>
    @endforeach
</ul>
@endif
<div class="empty-space marg-sm-b80 marg-xs-b60"></div>
</div>