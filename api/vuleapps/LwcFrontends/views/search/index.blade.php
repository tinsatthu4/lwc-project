@extends("lwcfrontend.master")
@section('main')
<div id="content-wrapper">
    @include('vuleappsLwcFrontends::widgets.breadcrumbs', [
         'title' => trans('static.search.title') . request()->get('s'),
         'data'  => [
             ['title' => trans('static.search.title') . request()->get('s') ]
         ]
     ])
    <div class="empty-space marg-lg-b70 marg-sm-b50 marg-xs-b30"></div>
    <div class="container">
        @foreach($models as $model)
        <div class="tt-event-2 clearfix">
            <div class="tt-event-2-img-cell">
                <a class="custom-hover" href="#">
                    <img src="{{ $model->present()->thumb() }}" height="199" width="254" alt="">
                </a>
            </div>
            <div class="tt-event-2-date-cell">
                <span>{{ date('d', strtotime($model->created_at)) }}</span>
                {{ date('F', strtotime($model->created_at)) }}
            </div>
            <div class="tt-event-2-info-cell">
                <a class="tt-event-2-title c-h5" href="#">{{ $model->present()->title }}</a>
                <div class="simple-text size-4 color-3">
                    <p>{{ $model->present()->excerpt }}</p>
                </div>
            </div>
        </div>
        <div class="empty-space marg-lg-b50 marg-sm-b30"></div>
        @endforeach
        {!! $links !!}
        <div class="empty-space marg-lg-b95 marg-md-b120 marg-sm-b100 marg-xs-b60"></div>
    </div>
</div>
@stop
