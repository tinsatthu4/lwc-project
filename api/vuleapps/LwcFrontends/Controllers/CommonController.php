<?php
namespace VuleApps\LwcFrontends\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use CommonRepository;
use PostRepository;
use UserRepository;
use CheckoutRepository;
use VuleApps\LwcBackends\Models\Contact;
use Auth;
use VuleApps\LwcBackends\Models\Page;
use URL;

class CommonController extends Controller
{

    function getDashboard()
    {
        return view('vuleappsLwcFrontends::home');
    }

	function getContact()
    {
        return view('vuleappsLwcFrontends::contact');
    }

    function postContact(Request $request)
    {
        try {
            $this->validate($request, [
                'name'      => 'required',
                'email'     => 'required|email',
                'phone'     => 'required|min:11|numeric',
                'address'   => 'required',
            ]);

            $postData = $request->all();
            $contact = new Contact();
            $contact->name = $postData['name'];
            $contact->email = $postData['email'];
            $contact->phone = $postData['phone'];
            $contact->address = $postData['address'];
            $contact->message = isset($postData['message']) ? $postData['message'] : null;
            $contact->save();

            return redirect()->route('contact');
        }
        catch(ValidationException $e) {
            return redirect()->back()
                ->withErrors($e->validator->messages())
                ->withInput();
        }
    }

    function logout()
    {
        Auth::logout();
		return redirect()->route('home');
    }

    function getTerm(Request $request, $slug, Term $term) {
        switch($term->type) {
            case 'post':
                PostRepository::setModel(new Post());
                break;
        }

        $models = PostRepository::buildArgs([
            'fields' => ['id', 'title', 'slug', 'image', 'excerpt'],
            'term_id' => $term->id
        ], true)
            ->active()
            ->paginate(30);
        return view('vuleappsFrontends::post.term')
            ->withTerm($term)
            ->withModels($models);
    }

    function getPageBySlug(Request $request, Model $model)
    {
        PostRepository::setModel(new Page());
        $posts = collect();
        PostRepository::setModel($model);
        return view('vuleappsLwcFrontends::post.page')
            ->withModel($model)
            ->withPosts($posts);
    }

    function getPage(Request $request, $slug, Model $model) {

        PostRepository::setModel(new Page());
        $posts = collect();
        PostRepository::setModel($model);
        return view('vuleappsLwcFrontends::post.page')
            ->withModel($model)
            ->withPosts($posts);
    }

    function postRegisterForm(Request $request)
    {
        try {
            $this->validate($request, [
                'email'     => 'required|email',
                'name'      => 'required',
                'post_id'   => 'required',
            ]);
            UserRepository::saveRegister(0, $request->all());
            $request->session()->flash('success', true);

            return redirect()
                ->to(URL::previous());
        } catch (ValidationException $e) {
            return redirect()
                ->to(URL::previous())
                ->withErrors($e->validator->messages())
                ->withInput();
        }
    }

    function postSubcribe(Request $request)
    {
        try {
            $this->validate($request, [
                'email_subcribe'     => 'required|email',
            ]);
            $request['email'] = $request['email_subcribe'];
            unset($request['email_subcribe']);
            UserRepository::saveSubcribe($request->all());
            $request->session()->flash('success', true);

            return redirect()
                ->to(URL::previous());
        } catch (ValidationException $e) {
            return redirect()
                ->to(URL::previous())
                ->withErrorSubscribe($e->validator->messages())
                ->withInput();
        }
    }

    function getPageFreeCourses()
    {
        return view('vuleappsLwcFrontends::post.page_free_course');
    }

    function getBlog(Request $request, $slug, Model $model) {
        PostRepository::setModel($model);
        $posts = collect();
        return view('vuleappsLwcFrontends::post.post')
            ->withModel($model)
            ->withPosts($posts);
    }
}