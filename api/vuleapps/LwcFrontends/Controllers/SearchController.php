<?php

namespace VuleApps\LwcFrontends\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use VuleApps\LwcBackends\Models\DefaultPost;
use PostRepository;

class SearchController extends Controller
{
    protected static $limit = 20;

    public function index(Request $request)
    {
        PostRepository::setModel(new DefaultPost());
        $rs = PostRepository::search($request->all(), self::$limit);
        return view('vuleappsLwcFrontends::search.index')
            ->withModels($rs['items'])
            ->withLinks($rs['links']);
    }
}