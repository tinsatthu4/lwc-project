<?php

namespace VuleApps\LwcFrontends\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;

class LanguageController extends Controller
{
    public function switchLang(Request $request, $lang)
    {
        if (array_key_exists($lang, config('app.locales'))) {
            $request->session()->put('lwclocale', $lang);
        }
        return redirect()->back();
    }
}