<?php
namespace VuleApps\LwcFrontends;
use Illuminate\Support\ServiceProvider as LaravelServiceProvider;

class ServiceProvider extends LaravelServiceProvider {

	public function boot() {
		require __DIR__ . '/route_binding.php';
		$this->loadViewsFrom(__DIR__ . '/views', 'vuleappsLwcFrontends');
		view()->composer('lwcfrontend.composer.head', 'VuleApps\LwcFrontends\Composer\HeadComposer');
	}

	public function register() {
		$this->mergeConfigFrom(__DIR__ . '/config.php', 'vuleapps.lwcfrontends');
	}
}
