<?php
namespace VuleApps\LwcFrontends\Composer;

use Illuminate\View\View;
use ProductRepository;
use CategoryRepository;
use PostRepository;
use TermRepository;
use VuleApps\LwcBackends\Repositories\SettingRepository;
use Route;

class HeadComposer {
	function __construct() {

	}

	function compose(View $view) {
		if(Route::is('frontend.getTerm')) {
			$model =TermRepository::getModel();
			$view->with([
				'theme_title' => $model->present()->title,
				'theme_meta_title' =>  $model->present()->meta_title,
				'theme_meta_description' =>  $model->present()->meta_content,
				'theme_meta_keywords' =>  $model->present()->meta_keywords,
				'theme_canonical' =>  $model->present()->href,
				'theme_image' =>  null
			]);
		}

		if(Route::is('getBlog')
			|| Route::is('RegisterFormCounrseOffline')
			|| Route::is('getCounrseDetail')
		) {
			$model = PostRepository::getModel();
			$view->with([
				'theme_title' => $model->present()->title,
				'theme_meta_title' =>  $model->present()->meta_title,
				'theme_meta_description' =>  $model->present()->meta_content,
				'theme_meta_keywords' =>  $model->present()->meta_keywords,
				'theme_canonical' =>  $model->present()->href,
				'theme_image' =>  $model->present()->thumb('0x150')
			]);
		}

		if(Route::is('getBlogs')
			|| Route::is('home')
			|| Route::is('listCounrseOffline')
		) {
			$group = [
				'home' => 'seo-home-page',
				'getBlogs' => 'seo-blog-page',
				'listCounrseOffline' => 'seo-counrseoffline-page',
			];
			$data = app(SettingRepository::class)->loadGroup($group[Route::currentRouteName()]);
			$view->with([
				'theme_title' => @$data['meta_title'],
				'theme_meta_title' =>  @$data['meta_title'],
				'theme_meta_description' =>  @$data['meta_title'],
				'theme_meta_keywords' =>  @$data['meta_title'],
				'theme_canonical' =>  url()->current(),
//				'theme_image' =>  $model->present()->thumb('0x150')
			]);
		}

		$model = null;
	}
}