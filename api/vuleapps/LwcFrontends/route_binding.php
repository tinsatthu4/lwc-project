<?php

Route::bind("page_slug", function($value) {
    return \VuleApps\LwcBackends\Models\Page::where('slug', $value)->first();
});

Route::bind("page_id", function($value) {
    PostRepository::setModel(new \VuleApps\LwcBackends\Models\Page());
    return PostRepository::getById($value);
});

Route::bind("blog_id", function($value) {
    PostRepository::setModel(new \VuleApps\LwcBackends\Models\Post());
    return PostRepository::getById($value);
});