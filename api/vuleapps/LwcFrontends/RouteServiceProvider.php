<?php

namespace VuleApps\LwcFrontends;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use App;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        //

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router, Request $request)
    {
        $this->mapWebRoutes($router, $request);

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    protected function mapWebRoutes(Router $router, Request $request)
    {
		$router->group([
            'middleware' => ['web'],
            'namespace'  => 'VuleApps\LwcFrontends\Controllers'
        ], function(Router $router) {
			//Home route
			$router->get('/', [
				'as' => 'home',
				'uses' => 'CommonController@getDashboard'
			]);

            //Logout
            $router->get('/logout', [
                'as' => 'frontend.logout',
                'uses' => 'CommonController@logout'
            ]);

            $router->get('c/{slug}-{term_blog_id}', [
                'as' => 'frontend.getTerm',
                'uses' => 'CommonController@getTerm'
            ])->where([
                'slug' => '[\w-]+',
                'term_blog_id' => '[0-9]+'
            ]);
            $router->get('bai-viet/{slug}-{blog_id}', [
                'as' => 'getBlog',
                'uses' => 'CommonController@getBlog'
            ])->where([
                'slug' => '[\w-]+',
                'blog_id' => '[0-9]+'
            ]);
            //Page route
            $router->get('page/free-courses', [
                'as' => 'getPageFreeCourses',
                'uses' => 'CommonController@getPageFreeCourses'
            ]);
            $router->get('page/{page_slug}', [
                'as' => 'getPageBySlug',
                'uses' => 'CommonController@getPageBySlug'
            ])->where([
                'page_slug' => '[\w-]+',
            ]);
            $router->get('p/{slug}-{page_id}', [
                'as' => 'getPage',
                'uses' => 'CommonController@getPage'
            ])->where([
                'slug' => '[\w-]+',
                'blog_id' => '[0-9]+'
            ]);

            //Search
            $router->get('search', [
                'as' => 'search',
                'uses' => 'SearchController@index'
            ]);

            //Register form homepage
            $router->post('course/register', [
                'as' => 'course.register',
                'uses' => 'CommonController@postRegisterForm'
            ]);

            //Register form homepage
            $router->post('subcribe', [
                'as' => 'subcribe',
                'uses' => 'CommonController@postSubcribe'
            ]);

            $router->get('lang/{lang}', [
                'as'=>'lang.switch',
                'uses'=>'LanguageController@switchLang'
            ]);
        });
    }
}
