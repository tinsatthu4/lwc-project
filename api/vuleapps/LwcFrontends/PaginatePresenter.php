<?php
namespace VuleApps\Frontends;
use Illuminate\Support\HtmlString;
use Illuminate\Pagination\BootstrapThreePresenter;

class PaginatePresenter extends BootstrapThreePresenter {
	/**
	 * Render the given paginator.
	 *
	 * @return \Illuminate\Contracts\Support\Htmlable|string
	 */
	public function render()
	{
		if ($this->hasPages()) {
			return new HtmlString(sprintf(
				'<ul class="pagination category-pagination pull-right">%s %s %s</ul>',
				$this->getPreviousButton('<i class="fa fa-long-arrow-left"></i>'),
				$this->getLinks(),
				$this->getNextButton('<i class="fa fa-long-arrow-right"></i>')
			));
		}
		return new HtmlString("");
	}
}