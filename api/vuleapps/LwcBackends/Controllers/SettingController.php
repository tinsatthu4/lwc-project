<?php
namespace VuleApps\LwcBackends\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use VuleApps\Common\Http\Controllers\BaseResourceController;
use VuleApps\LwcBackends\Repositories\SettingRepository;

class SettingController extends BaseResourceController {
	public $module = "LwcBackends::setting";

	public function __construct(SettingRepository $SettingRepository) {
		$this->repository = $SettingRepository;
	}

	public function index(Request $request)
	{
		return view('LwcBackends::setting.index');
	}

	function save(Request $request) {
		try {
			$this->validate($request, [
				'meta_title' => 'required',
				'meta_description' => 'required',
				'meta_keywords' => 'required',
			]);
			$this->repository->saveGroup($request->all(), 'default');
			return redirect()->back();
		} catch (ValidationException $e) {
			return redirect()
				->back()
				->withInput()
				->withErrors($e->validator->messages());
		}
	}

	function postModule(Request $request) {
		try {
			$this->validate($request, [
				'group' => 'required',
				'input' => 'required|array',
			]);
			$this->repository->saveGroup($request->get('input'), $request->get('group'));
			$data = $this->repository->loadGroup($request->get('group'));
			return response()->json(['data' => $data]);
		} catch (ValidationException $e) {
			return response()->json(['message' => $e->validator->messages()], 400);
		}
	}

	public function getModule(Request $request) {
		try {
			$this->validate($request, [
				'module' => 'required'
			]);

			return view('LwcBackends::setting.module')
					->withModule($request->get('module'));
		} catch(ValidationException $e) {
			return redirect()->route('admin::dashboard');
		}
	}

	public function getGroup(Request $request, $group) {
		$data = $this->repository->loadGroup($group);
		return response(['data' => $data]);
	}
}