<?php
namespace VuleApps\LwcBackends\Controllers;
use VuleApps\LwcBackends\Models\Page;
use VuleApps\Common\Http\Controllers\BaseResourceController;
use VuleApps\LwcBackends\Repositories\PostRepository;
use App\Http\Requests;

class PageController extends BaseResourceController
{
	public $module = "LwcBackends::page";

	public $rules = [
		"title" => "required",
		"slug" => "required",
		"meta_title" => "required", "meta_content" => "required", "meta_keywords" => "required"
	];

	public function __construct(PostRepository $postRepository) {
		$postRepository->setModel(new Page());
		$this->repository = $postRepository;
	}
}