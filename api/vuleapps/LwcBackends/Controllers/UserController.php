<?php
namespace VuleApps\LwcBackends\Controllers;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\User;
use Illuminate\Support\MessageBag;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Mockery\CountValidator\Exception;
use VuleApps\LwcBackends\Jobs\UserBuyTokenFile;
use VuleApps\LwcBackends\Models\UserMeta;
use Auth;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->get('s', null);

        $user = new User();
        if($search != null) {
            $user = $user->where('email', 'like', "%{$search}%");
        }
        $user = $user->paginate(config("vuleapps.common.paginate.default"));

        return view('LwcBackends::user.index')
            ->withModels($user);
    }

    public function create()
    {
        return view('LwcBackends::user.form')
            ->withModel(null);
    }

    public function store(Request $request)
    {
        try {
            $this->validate($request, [
                'name'     => 'required',
                'email'    => 'required|email|max:255|unique:users',
                'password' => 'required|confirmed|min:6',
                'type'     => 'required'
            ]);

            $user = new User();
            $user->fill($request->all());
            $user->password = bcrypt($user->password);
            $user->save();
			//insert metadata
			$metadata = isset($data['metadata']) ? $data['metadata'] : [];
			$dataMeta = [];
			foreach($metadata as $key => $value) {
				$dataMeta[] = new UserMeta([
                    'meta_key'   => $key,
                    'meta_value' => is_array($value)? serialize($value) : $value,
                    'serialize'  => is_array($value) ? 1 : 0
				]);
			}
			if(!empty($dataMeta))
				$user->usermeta()->saveMany($dataMeta);


			return redirect()->route('admin::admin.user.edit', [$user->id]);
        }
        catch(ValidationException $e) {
            return redirect()->back()
                ->withErrors($e->validator->messages())
                ->withInput();
        }
    }

    public function edit(Model $model)
    {
        return view('LwcBackends::user.form')
            ->withModel($model);
    }

    public function update(Request $request, User $model)
    {
        try {
            $this->validate($request, [
                'name'     => 'required',
                'email'    => 'required|email|max:255',
                'password' => 'confirmed|min:6',
                'type'     => 'required'
            ]);

            $data = $request->all();

            $data['password'] = (!empty($request['password'])) ? bcrypt($request['password']) : $model->password;
            $model->fill($data);
            $model->save();
			$model->usermeta()->delete();

			$metadata = isset($data['metadata']) ? $data['metadata'] : [];
			$dataMeta = [];
			foreach($metadata as $key => $value) {
				$dataMeta[] = new UserMeta([
					'meta_key' => $key,
					'meta_value' => is_array($value)? serialize($value) : $value,
					'serialize' => is_array($value) ? 1 : 0
				]);
			}
			if(!empty($dataMeta))
				$model->usermeta()->saveMany($dataMeta);

            return redirect()
				->route('admin::admin.user.edit', [$model->id])
				->withSuccess(true);
        }
        catch(ValidationException $e) {
            return redirect()->back()
                ->withErrors($e->validator->messages())
                ->withInput();
        }
    }

    public function destroy(Model $model)
    {
        try {
            $model->destroy($model->id);
            return response(null, 204);
        }
        catch(Exception $e) {
            return response(null, 400);
        }
    }

	public function getLoginAs(Request $request, User $user) {
		Auth::logout();
		Auth::login($user);
		if($user->type == 'member')
			return redirect()->route('home');

		return redirect()->to(vl_admin_route('dashboard'));
	}
}
