<?php
namespace VuleApps\LwcBackends\Controllers;
use App\Http\Controllers\Controller;
use Auth;
use TermRepository;

class CommonController extends Controller
{
	function dashboard()
    {
		return view("LwcBackends::dashboard");
	}

    function learnerDashboard()
    {
        $user = Auth::getUser();
        $data = $this->dataLearner($user);
        return view("LwcBackends::dashboard_learner")
            ->withUser($user)
            ->withData($data);
    }

    function teacher01Dashboard()
    {
        $user = Auth::getUser();
        $data = $this->dataLearner($user);
        return view("LwcBackends::dashboard_teacher_01")
            ->withUser($user)
            ->withData($data);
    }

    function teacher02Dashboard()
    {
        $user = Auth::getUser();
        $data = $this->dataLearner($user);
        return view("LwcBackends::dashboard_teacher_02")
            ->withUser($user)
            ->withData($data);
    }

    function dataLearner($user)
    {
        $counrses = $user->counrses()->get();
        return [
            'counrses'   => $counrses,
            'events'     => TermRepository::getBySlug('event')
                ->post()->take(9)->get(),
            'announcements' => TermRepository::getBySlug('announcements')
                ->post()->take(4)->get(),
            'news'       => TermRepository::getBySlug('news')
                ->post()->take(4)->get()
        ];
    }

    function dataTeacher($user)
    {
        $data = [];
        return $data;
    }
}