<?php
namespace VuleApps\LwcBackends\Controllers;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Mockery\CountValidator\Exception;
use VuleApps\LwcBackends\Models\Comment;
use VuleApps\LwcBackends\Models\DefaultPost;

class CommentController extends Controller
{
    public function getCommentByPostId($type, $id, Request $request)
    {
        $search = $request->get('s', null);

        $comments = Comment::where([
            'post_id'   => $id
        ]);
        if($search != null) {
            $comments->where('comment', 'like', "%{$search}%");
        }
        $comments = $comments->paginate(config("vuleapps.common.paginate.default"));

        $post = DefaultPost::where([
            'id'    => $id,
            'type'  => $type
        ])->first();

        return view('LwcBackends::comment.index')
            ->withModels($comments)
            ->withPost($post);
    }

    public function delete(Model $model)
    {
        try {
            $model->destroy($model->id);
            return response(null, 204);
        }
        catch(Exception $e) {
            return response(null, 400);
        }
    }
}