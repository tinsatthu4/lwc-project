<?php
namespace VuleApps\LwcBackends\Controllers;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use VuleApps\Common\Http\Controllers\BaseResourceController;
use App\Http\Requests;
use VuleApps\LwcBackends\Repositories\TermRepository;

class TermController extends BaseResourceController
{
	public $module = "LwcBackends::term";

	public $rules = [
		"title" => "required",
		"slug" => "required",
		"type" => "required",
		"meta_title" => "required", "meta_content" => "required", "meta_keywords" => "required"
	];

	public function index(Request $request)
	{
		$type = $request->get('type', 'category');
		$args = $request->all();
		return view("{$this->module}.index")
			->withModels($this->repository->listByParent($args, $type));
	}

	function __construct(TermRepository $repository) {
		$this->repository = $repository;
	}

	function getTermsByPostId(Request $request, $post_id) {
		$data = $this->repository->listTermIdsByPostId($post_id);

		return response()->json(['data' => $data]);
	}

	function getByType(Request $request, $type) {
		$terms = $this->repository->listByParent($request->all(), $type);
		$terms = $terms->each(function($item) {
			$item->href_view = route('frontend.getTerm', [$item->slug, $item->id]);
		});
		return response()->json(['data' => $terms]);
	}

	function getByTerm(Request $request) {
		try {
			$this->validate($request, [
				'type' => 'required'
			]);
			$collection = $this->repository->listByParent([], $request->get('type'));
			return response()
					->json(['data' => $collection]);
		} catch(ValidationException $e) {
			return response()
				->json(['data' => []]);
		}
	}
}