<?php
namespace VuleApps\LwcBackends\Controllers;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Mockery\CountValidator\Exception;
use VuleApps\LwcBackends\Models\Subcribe;

class SubscribeController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->get('s', null);

        $subscribes = new Subcribe();
        if($search != null) {
            $subscribes = $subscribes->where('email', 'LIKE', "%{$search}%");
        }
        $subscribes = $subscribes->paginate(config("vuleapps.common.paginate.default"));

        return view('LwcBackends::subscribe.index')
            ->withModels($subscribes);
    }

    public function delete(Model $model)
    {
        try {
            $model->destroy($model->id);
            return response(null, 204);
        }
        catch(Exception $e) {
            return response(null, 400);
        }
    }
}