<?php
namespace VuleApps\LwcBackends\Controllers;
use VuleApps\LwcBackends\Models\Post;
use VuleApps\Common\Http\Controllers\BaseResourceController;
use VuleApps\LwcBackends\Repositories\PostRepository;
use App\Http\Requests;

class PostController extends BaseResourceController
{
	public $module = "LwcBackends::post";

	public $rules = [
		"title" => "required",
		"slug" => "required",
		"meta_title" => "required", "meta_content" => "required", "meta_keywords" => "required"
	];

	public function __construct(PostRepository $postRepository) {
		$postRepository->setModel(new Post());
		$this->repository = $postRepository;
	}
}