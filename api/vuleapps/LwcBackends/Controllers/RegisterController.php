<?php
namespace VuleApps\LwcBackends\Controllers;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Mockery\CountValidator\Exception;
use VuleApps\LwcBackends\Models\RegisterForm;
use VuleApps\LwcBackends\Models\Subcribe;

class RegisterController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->get('s', null);
        $email = $request->get('email', null);
        $postId = $request->get('post_id', null);

        $subscribes = new RegisterForm();
        if($search != null) {
            $subscribes = $subscribes->where('name', 'LIKE', "%{$search}%");
        }
        if($email != null) {
            $subscribes = $subscribes->where('email', 'LIKE', "%{$email}%");
        }
        if($postId != null) {
            $subscribes = $subscribes->where('post_id', $postId);
        }
        $subscribes = $subscribes->paginate(config("vuleapps.common.paginate.default"));

        return view('LwcBackends::register.index')
            ->withModels($subscribes);
    }

    public function delete(Model $model)
    {
        try {
            $model->destroy($model->id);
            return response(null, 204);
        }
        catch(Exception $e) {
            return response(null, 400);
        }
    }
}