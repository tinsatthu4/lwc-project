<?php
namespace VuleApps\LwcBackends\Controllers;
use VuleApps\Common\Http\Controllers\BaseResourceController;
use App\Http\Requests;
use PostRepository;

class DefaultPostController extends BaseResourceController
{
	function getMeta($post_id) {
		return response()->json(['data' => PostRepository::getMeta($post_id)]);
	}
}