<?php
namespace VuleApps\LwcBackends\Controllers;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller {
	function getLogin() {
		return view('LwcBackends::layout.login')
				->withErrors(null);
	}

	function getLogout() {
		Auth::logout();
		return redirect(vl_admin_route('user.login'));
	}

	function postLogin(Request $request) {
		try {
			$this->validate($request, [
				'email' => 'required|email',
				'password' => 'required'
			]);

			$credentials = [
				'email' => $request->get('email'),
				'password' => $request->get('password'),
//				'type' => 'admin'
			];

			if(Auth::attempt($credentials, 1)) {
				return redirect(vl_admin_route('dashboard'));
			}

			$message = new MessageBag(['error' => 'Email Or Password incorrect']);
			return redirect()->back()
				->withErrors($message->getMessageBag())
				->withInput();

		} catch (ValidationException $e) {
			return redirect()->back()
				->withErrors($e->validator->messages())
				->withInput();
		}
	}
}