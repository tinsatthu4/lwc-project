<?php namespace VuleApps\LwcBackends\Facades;
use Illuminate\Support\Facades\Facade;
use VuleApps\LwcBackends\Repositories\PostRepository;
class PostFacade extends Facade {
	protected static function getFacadeAccessor()
	{
		return PostRepository::class;
	}

}