<?php namespace VuleApps\LwcBackends\Facades;
use Illuminate\Support\Facades\Facade;
use VuleApps\LwcBackends\Repositories\CheckoutRepository;
class CheckoutFacade extends Facade {
	protected static function getFacadeAccessor()
	{
		return CheckoutRepository::class;
	}
}