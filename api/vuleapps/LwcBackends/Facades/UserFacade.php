<?php namespace VuleApps\LwcBackends\Facades;
use Illuminate\Support\Facades\Facade;
use VuleApps\LwcBackends\Repositories\UserRepository;
class UserFacade extends Facade {
	protected static function getFacadeAccessor()
	{
		return UserRepository::class;
	}
}