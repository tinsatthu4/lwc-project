<?php namespace VuleApps\LwcBackends\Facades;
use Illuminate\Support\Facades\Facade;
use VuleApps\LwcBackends\Repositories\SettingRepository;
class SettingFacade extends Facade {
	protected static function getFacadeAccessor()
	{
		return SettingRepository::class;
	}

}