<?php namespace VuleApps\LwcBackends\Facades;
use Illuminate\Support\Facades\Facade;
use VuleApps\LwcBackends\Repositories\TermRepository;
class TermFacade extends Facade {
	protected static function getFacadeAccessor()
	{
		return TermRepository::class;
	}
}