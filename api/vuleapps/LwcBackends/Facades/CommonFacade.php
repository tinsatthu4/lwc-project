<?php namespace VuleApps\LwcBackends\Facades;
use Illuminate\Support\Facades\Facade;
use VuleApps\LwcBackends\Repositories\CommonRepository;
class CommonFacade extends Facade {
	protected static function getFacadeAccessor()
	{
		return CommonRepository::class;
	}

}