<?php
namespace VuleApps\LwcBackends\Presenter;

use VuleApps\LwcBackends\Models\CounrseOffline;
use VuleApps\LwcBackends\Models\Term;
use VuleApps\Common\Present;

class TermPresent extends Present {
	function action() {
		if($this->model instanceof Term)
			return view("LwcBackends::term.action")
				->withModel($this->model)
				->render();
	}

	function href() {
		if($this->model->type == Term::TYPE_COUNRSE)
			return route('CounrseOnline.getCategory', [$this->model->slug, $this->model->id]);

		if($this->model->type == Term::TYPE_EBOOK)
			return '#';

		if($this->model->type == Term::TYPE_EBOOK)
			return '#';

		if($this->model->type == Term::TYPE_POST)
			return route('frontend.getTerm', [$this->model->slug, $this->model->id]);

		return '#';
	}
}