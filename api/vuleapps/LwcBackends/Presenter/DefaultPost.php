<?php
namespace VuleApps\LwcBackends\Presenter;

use VuleApps\LwcBackends\Models\Page;
use VuleApps\Common\Present;
use VuleApps\LwcBackends\Models\Post;

class DefaultPost extends Present {
	function action() {
		if($this->model instanceof Post)
			return view("LwcBackends::post.action")
				->withModel($this->model)
				->render();
        if($this->model instanceof Page)
            return view("LwcBackends::page.action")
                ->withModel($this->model)
                ->render();
	}

	function href() {
		if($this->model instanceof Page || $this->model->type == Page::TYPE)
			return route('getPage', [$this->model->slug, $this->model->id]);

		return '#defaultpost';
	}
}