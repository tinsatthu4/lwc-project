<?php
namespace VuleApps\LwcBackends\Presenter;

use App\User;
use VuleApps\Common\Present;

class UserPresent extends Present {
    function action() {
        if($this->model instanceof User)
            return view("LwcBackends::user.action")
                ->withModel($this->model)
                ->render();
    }
}