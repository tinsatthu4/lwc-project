@extends("LwcBackends::layout.master")
@section('main')
    @include('vuleappsCommon::components.box.start', ['title' => "Users ({$models->total()})"])
        {{--<div class="row">--}}
            {{--<div class="col-md-12">--}}
                {{--<form action="{{ url()->current() }}" method="GET" class="form-inline">--}}
                {{--{!!--}}
                    {{--vl_form_bsText("Search", 's', request()->get('s', null))--}}
                {{--!!}--}}
                {{--<div class="form-group"><input type="submit" value="Filter" class="btn btn-primary"/></div>--}}
                {{--</form>--}}
            {{--</div>--}}
        {{--</div>--}}
        <table class="table table-hover">
        	<thead>
        		<tr>
        			<th>Name</th>
        			<th>Email</th>
        			<th>Type</th>
        			<th>Token</th>
        			<th></th>
        		</tr>
        	</thead>
        	<tbody>
        	@foreach($models as $model)
        		<tr id="user-{{ $model->id }}">
        			<td>{{ $model->name }}</td>
        			<td>{{ $model->email }}</td>
        			<td>{{ $model->type }}</td>
        			<td>{{ $model->token }}</td>
        			<td>
        			    {!! $model->present()->action() !!}
        			</td>
        		</tr>
            @endforeach
        	</tbody>
        </table>
        <div>
            <div class="pull-right">{{ $models->appends(request()->all())->links() }}</div>
            <div class="clearfix"></div>
        </div>
    @include('vuleappsCommon::components.box.end')
@stop