@extends("LwcBackends::layout.master")
@section("main")
    @include("vuleappsCommon::components.form.notifications")
    @if($model)
    <h2>Sửa User {{ $model->name }}</h2>
    <form method="POST" action="{{ vl_admin_resoure_route("user.update", [$model->id]) }}">
    {{ method_field('PUT') }}
    @else
    <h2>Thêm User</h2>
    <form method="POST" action="{{ vl_admin_resoure_route("user.store") }}">
    @endif
    <div class="row">
        <div class="col-md-8 col-sm-7 col-xs-12">
            @include("vuleappsCommon::components.box.start")
                {!! vl_form_bsText('Họ Tên', 'name', old("name", $model?$model->name:null )) !!}
                {!! vl_form_bsText('Email', 'email', old("email", $model?$model->email:null )) !!}
                <div class="form-group">
                    {{ Form::label('Mật Khẩu', null, ['class' => 'control-label']) }}
                    {{ Form::password('password', ['class' => 'form-control']) }}
                </div>
                <div class="form-group">
                    {{ Form::label('Xác Nhận Mật Khẩu', null, ['class' => 'control-label']) }}
                    {{ Form::password('password_confirmation', ['class' => 'form-control']) }}
                </div>
            @include("vuleappsCommon::components.box.end")
        </div>
        <div class="col-md-4 col-sm-5 col-xs-12">

        @include("vuleappsCommon::components.box.start", ['class' => 'box-success'])
            <div class="form-group">
                {{ Form::label('Quyền', null, ['class' => 'control-label']) }}
                {{ Form::select("type", config('vuleapps.common.user_roles'), old('type', $model?$model->type:null), ['class' => 'form-control']) }}
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-block btn-primary btn-flat" value="{{ !$model? "Create" : "Update" }}"/>
            </div>
        @include("vuleappsCommon::components.box.end")
        </div>
    </div>

    </form>
@stop