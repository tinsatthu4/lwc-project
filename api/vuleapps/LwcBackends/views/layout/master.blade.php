<!doctype html>
<html lang="en" ng-app="app">
<head>
<meta charset="UTF-8">
@include('LwcBackends::layout.header')
</head>
<body class="hold-transition skin-blue sidebar-mini" ng-app="app">
<div class="wrapper">
    @include('LwcBackends::parts.sidebar')
    <div class="content-wrapper">
        <section class="content" style="min-height: 800px">@yield('main')</section>
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <strong>Copyright &copy; 2014-2015 <a href="#">Almsaeed Studio</a>.</strong> All rights reserved.
    </footer>
    <div class="control-sidebar-bg"></div>
</div>
@include('LwcBackends::layout.footer')
</body>
</html>