<header class="main-header">
<a href="#" class="logo">
  <!-- mini logo for sidebar mini 50x50 pixels -->
  <span class="logo-mini"><b>LWC</b></span>
  <!-- logo for regular state and mobile devices -->
  <span class="logo-lg"><b>LWC</b></span>
</a>
<nav class="navbar navbar-static-top">
  <!-- Sidebar toggle button-->
  <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
    <span class="sr-only">Toggle navigation</span>
  </a>
  <div class="navbar-custom-menu">
    <ul class="nav navbar-nav">
      <li class="dropdown user user-menu">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <span class="hidden-xs">Admin</span>
        </a>
        <ul class="dropdown-menu">
          <li class="user-footer">
            <div class="pull-left">
              <a href="#" class="btn btn-default btn-flat">Profile</a>
            </div>
            <div class="pull-right">
              <a href="{{ vl_admin_route('user.logout') }}" class="btn btn-default btn-flat">Sign out</a>
            </div>
          </li>
        </ul>
      </li>
    </ul>
  </div>
</nav>
</header>
<aside class="main-sidebar">
<section class="sidebar">
  <ul class="sidebar-menu">
    <li class="header">MAIN NAVIGATION</li>
    <?php $user_type = Auth::getUser()->type; ?>
    <li class="active treeview">
      {{--<a href="#">--}}
        {{--<i class="fa fa-cube"></i> <span>Product Module</span> <i class="fa fa-angle-left pull-right"></i>--}}
      {{--</a>--}}
      <ul class="treeview-menu">
        {{--<li class="active"><a href="{{ vl_admin_resoure_route("product.index") }}"><i class="fa fa-circle-o"></i> List Product</a></li>--}}
        {{--<li class="active"><a href="{{ vl_admin_resoure_route("product.create") }}"><i class="fa fa-circle-o"></i> Add Product</a></li>--}}
        {{--<li class="active"><a href="{{ vl_admin_resoure_route("category.index") }}"><i class="fa fa-circle-o"></i> List Category</a></li>--}}
        {{--<li class="active"><a href="{{ vl_admin_resoure_route("category.create") }}"><i class="fa fa-circle-o"></i> Add Categeory</a></li>--}}
      </ul>
    </li>
    @if(in_array($user_type, ['admin']))
    <li class="active treeview">
      <a href="#">
        <i class="fa fa-cube"></i> <span>Bài Viết</span></i>
      </a>
      <ul class="treeview-menu">
        {{--<li class="active"><a href="{{ vl_admin_resoure_route("term.index", ['type'=>'post', 'title' => 'Danh mục Bài viết']) }}"><i class="fa fa-circle-o"></i> Danh mục</a></li>--}}
        <li class="active"><a href="{{ vl_admin_resoure_route("term.index", ['type'=>'post', 'title' => 'Danh mục bài viết']) }}"><i class="fa fa-circle-o"></i> Danh mục bài viết</a></li>
        <li class="active"><a href="{{ vl_admin_resoure_route("post.index") }}"><i class="fa fa-circle-o"></i> DS bài viết</a></li>
        <li class="active"><a href="{{ vl_admin_resoure_route("post.create") }}"><i class="fa fa-circle-o"></i> Thêm bài viết</a></li>
      </ul>
    </li>
    <li class="active treeview">
      <a href="#">
        <i class="fa fa-cube"></i> <span>Trang</span></i>
      </a>
      <ul class="treeview-menu">
{{--        <li class="active"><a href="{{ vl_admin_resoure_route("page.edit", [23]) }}"><i class="fa fa-circle-o"></i> Trang Giới Thiệu</a></li>--}}
        <li class="active"><a href="{{ vl_admin_resoure_route("page.index") }}"><i class="fa fa-circle-o"></i> DS trang</a></li>
        <li class="active"><a href="{{ vl_admin_resoure_route("page.create") }}"><i class="fa fa-circle-o"></i> Thêm trang</a></li>
      </ul>
    </li>
    <li class="active treeview">
      <a href="#">
        <i class="fa fa-cube"></i> <span>User</span></i>
      </a>
      <ul class="treeview-menu">
        <li class="active"><a href="{{ vl_admin_resoure_route("user.index") }}"><i class="fa fa-circle-o"></i>Danh sách user</a></li>
        <li class="active"><a href="{{ vl_admin_resoure_route("user.create") }}"><i class="fa fa-circle-o"></i>Thêm user</a></li>
        <li class="active"><a href="{{ vl_admin_resoure_route("register.index") }}"><i class="fa fa-circle-o"></i>Danh sách user đăng ký</a></li>
        <li class="active"><a href="{{ vl_admin_resoure_route("subscribe.index") }}"><i class="fa fa-circle-o"></i>Danh sách subcribe</a></li>
      </ul>
    </li>
    <li class="active treeview">
      <a href="#">
        <i class="fa fa-cube"></i> <span>Cài đặt</span></i>
      </a>
      <ul class="treeview-menu">
        <li class="active">
            <a href="{{ vl_admin_route("setting.getModule", ['module' => 'setting-webinfo']) }}"><i class="fa fa-circle-o"></i> Thông tin web</a>
        </li>
        <li class="active">
            <a href="{{ vl_admin_route("setting.getModule", ['module' => 'setting-form-seo']) }}"><i class="fa fa-circle-o"></i> Cài đặt SEO</a>
        </li>
        <li class="active">
            <a href="{{ vl_admin_route("setting.getModule", ['module' => 'setting-banners']) }}"><i class="fa fa-circle-o"></i> Banner Trang Chủ</a>
        </li>
        <li class="active">
            <a href="{{ vl_admin_route("setting.getModule", ['module' => 'setting-menu-primary']) }}"><i class="fa fa-circle-o"></i> Menu Chính</a>
        </li>
      </ul>
    </li>
    @endif
  </ul>
</section>
</aside>
