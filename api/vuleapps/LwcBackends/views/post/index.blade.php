@extends("LwcBackends::layout.master")
@section('main')
    @include('vuleappsCommon::components.box.start', ['title' => "Bài Viết ({$models->total()})"])
        <div class="row">
            <div class="col-md-12">
                <form action="{{ url()->current() }}" method="GET" class="form-inline">
                {!!
                    vl_form_bsSelect('Nhóm KH', 'term_id', ['Tất cả'] + TermRepository::listByParent([], 'post')->pluck('title', 'id')->all(), request()->get('term_id', 0))
                !!}
                <div class="form-group"><input type="submit" value="Lọc" class="btn btn-primary"/></div>
                </form>
            </div>
        </div>

        <table class="table table-hover">
        	<thead>
        		<tr>
        			<th class="image">Hình Ảnh</th>
        			<th>Tiêu Đề</th>
        			<th>Mô tả</th>
        			<th>Trạng thái</th>
        			<th>Thời gian</th>
        			<th></th>
        		</tr>
        	</thead>
        	<tbody>
        	@foreach($models as $model)
        		<tr id="post-{{ $model->id }}">
        			<td>
                        @if($model->image)
                            <img src="{!! $model->present()->thumb('0x60') !!}" width="60px" class="image"/>
                        @endif
                    </td>
        			<td>{{ $model->present()->title }}</td>
        			<td>{!! $model->present()->excerpt !!}</td>
        			<td>{!! $model->present()->status !!}</td>
        			<td>{{ $model->present()->created_at }}</td>
        			<td>
        			    {!! $model->present()->action() !!}
        			</td>
        		</tr>
            @endforeach
        	</tbody>
        </table>
        <div>
            <div class="pull-right">{{ $models->appends(request()->all())->links() }}</div>
            <div class="clearfix"></div>
        </div>
    @include('vuleappsCommon::components.box.end')
@stop