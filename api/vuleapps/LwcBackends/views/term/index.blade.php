@extends("LwcBackends::layout.master")
@section('main')
<term-module type="{{ request()->get('type', 'category') }}" title="{{ request()->get('title', 'Categories') }}"></term-module>
@stop