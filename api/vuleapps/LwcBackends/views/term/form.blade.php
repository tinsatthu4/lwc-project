@extends("LwcBackends::layout.master")
@section('main')
    @if($model)
    <term-form title="Category Form" type="category" action="{{ vl_admin_resoure_route('term.update', $model->id) }}" id="{{ $model->id }}"></term-form>
    @else
    <term-form title="Category Form" type="category" action="{{ vl_admin_resoure_route('term.store') }}"></term-form>
    @endif
@stop