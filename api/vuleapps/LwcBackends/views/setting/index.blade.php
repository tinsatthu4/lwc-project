@extends("LwcBackends::layout.master")
@section('main')
    @include("vuleappsCommon::components.form.notifications")
    <form method="POST" action="{{ vl_admin_route("setting.save") }}">
    @include('vuleappsCommon::components.box.start', ['title' => "Settings"])
    <div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
      <li class="active"><a href="#tab_1" data-toggle="tab">General</a></li>
      <li><a href="#tab_2" data-toggle="tab">Social</a></li>
      <li class="pull-right">
        <input type="submit" value="Update" class="btn btn-primary"/>
      </li>
    </ul>
    <div class="tab-content">
      <div class="tab-pane active" id="tab_1">
        {!! vl_form_bsText('Meta Title', 'meta_title', old('meta_title', config('frontend.default.meta_title'))) !!}
        {!! vl_form_bsTextarea('Meta Description', 'meta_description', old('meta_description', config('frontend.default.meta_description'))) !!}
        {!! vl_form_bsText('Meta Keywords', 'meta_keywords', old('meta_keywords', config('frontend.default.meta_keywords'))) !!}
        {!! vl_form_bsTextarea('Footer Social', 'footer_social', old('footer_social', config('frontend.default.footer_social')), ['class' => 'form-control editor']) !!}
        {!! vl_form_bsTextarea('Google Analytics', 'google_analytic', old('google_analytic', config('frontend.default.google_analytic'))) !!}
        {!! vl_form_bsTextarea('Facebook Script', 'facebook_sdk', old('facebook_sdk', config('frontend.default.facebook_sdk'))) !!}
        {!! vl_form_bsTextarea('Head Script', 'head_script', old('head_script', config('frontend.default.head_script'))) !!}
        {!! vl_form_bsTextarea('Custom Css', 'custom_css', old('custom_css', config('frontend.default.custom_css'))) !!}
      </div>
      <div class="tab-pane" id="tab_2">
        {!! vl_form_bsText('Facebook API', 'fb_api', old('fb_api', config('frontend.default.fb_api'))) !!}
        {!! vl_form_bsText('Facebook Secret', 'fb_secret', old('fb_secret', config('frontend.default.fb_secret'))) !!}
        {!! vl_form_bsText('FB Simple Token Crawler', 'fb_token_simple', old('fb_token_simple', config('frontend.default.fb_token_simple'))) !!}
        <p class="help-block">Get Token here https://developers.facebook.com/tools/explorer</p>
      </div>
    </div>
    </div>
    @include('vuleappsCommon::components.box.end')
    </form>
@stop