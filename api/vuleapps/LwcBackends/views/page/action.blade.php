<div class="dropdown">
  <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
    Actions
    <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" role="menu">
    <li role="presentation"><a role="menuitem" tabindex="-1" href="{{ vl_admin_resoure_route("page.edit", $model->id) }}">Edit</a></li>
    <li role="presentation"><a role="menuitem" tabindex="-1" target="_blank" href="{{ $model->present()->href }}">Xem</a></li>
    <li role="presentation" class="divider"></li>
    <li role="presentation"><a role="menuitem" tabindex="-1" href="{{ vl_admin_resoure_route("page.destroy", $model->id) }}" data-el="#page-{{$model->id}}" class="delete text text-danger">Delete</a></li>
  </ul>
</div>