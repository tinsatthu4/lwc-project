@extends("LwcBackends::layout.master")
@section("main")
    @include("vuleappsCommon::components.form.notifications")
    @if($model)
    <form method="POST" action="{{ vl_admin_resoure_route("page.update", [$model->id]) }}">
    {{ method_field('PUT') }}
    @else
    <form method="POST" action="{{ vl_admin_resoure_route("page.store") }}">
    @endif
    {!! vl_form_bsText('Tiêu Đề', 'title', old("title", $model?$model->title:null )) !!}
    {!! vl_form_bsText('Slug', 'slug', old("slug", $model?$model->slug:null )) !!}
    <div class="row">
        <div class="col-md-8 col-sm-7 col-xs-12">
            @include("vuleappsCommon::components.box.start")
            {!!
                vl_form_bsTextarea("Nội Dung", "content",
                    old("content", $model?$model->content:null ),
                    ["class" => "form-control tiny-editor"]
                )
            !!}
            {!! vl_form_bsTextarea('Mô tả', 'excerpt', old("excerpt", $model?$model->excerpt:null ), ['rows' => 3]) !!}

            @include("vuleappsCommon::components.box.end")
            @include("vuleappsCommon::components.box.start", ['title' => 'SEO Information', 'class' => 'box-primary'])
            {!! vl_form_bsText('Meta Title', 'meta_title', old("meta_title", $model?$model->meta_title:null )) !!}
            {!! vl_form_bsTextarea('Meta Content', 'meta_content', old("meta_keywords", $model?$model->meta_content:null ), ['rows' => 3]) !!}
            {!! vl_form_bsTextarea('Meta Keywords', 'meta_keywords', old("meta_keywords", $model?$model->meta_keywords:null ), ['rows' => 3]) !!}
            @include("vuleappsCommon::components.box.end")
        </div>
        <div class="col-md-4 col-sm-5 col-xs-12">

        @include("vuleappsCommon::components.box.start", ['class' => 'box-success'])
            <div class="form-group">
                <div class="col-sm-2">
                    <input type="submit" class="btn btn-primary" value="{{ !$model? "Create" : "Update" }}"/>
                </div>
                <div class="col-sm-6 pull-left">
                {{ Form::select("status", config('vuleapps.common.status'), old('status', $model?$model->status:null), ['class' => 'form-control']) }}
                </div>
            </div>
        @include("vuleappsCommon::components.box.end")

        @if($model)
        <page-metadata id="{{ $model->id }}"></page-metadata>
        @else
        <page-metadata></page-metadata>
        @endif

        {!! vl_form_thumbnail("image", old('image', $model ?$model->image: null)) !!}

        </div>
    </div>

    </form>
@stop