@extends("LwcBackends::layout.master")
@section('main')
    @include('vuleappsCommon::components.box.start', ['title' => "Trang ({$models->total()})"])
        <table class="table table-hover">
        	<thead>
        		<tr>
        			{{--<th class="checkbox-all"></th>--}}
        			<th class="image">Hình Ảnh</th>
        			<th>Tiêu Đề</th>
        			<th>Mô tả</th>
        			<th>Trạng thái</th>
        			<th></th>
        		</tr>
        	</thead>
        	<tbody>
        	@foreach($models as $model)
        		<tr id="post-{{ $model->id }}">
        			<td><img src="{!! $model->present()->thumb('0x60') !!}" width="60px" class="image"/></td>
        			<td>{{ $model->present()->title }}</td>
        			<td>{!! $model->present()->excerpt !!}</td>
        			<td>{!! $model->present()->status !!}</td>
        			<td>
        			    {!! $model->present()->action() !!}
        			</td>
        		</tr>
            @endforeach
        	</tbody>
        </table>
        <div>
            <div class="pull-right">{{ $models->appends(request()->all())->links() }}</div>
            <div class="clearfix"></div>
        </div>
    @include('vuleappsCommon::components.box.end')
@stop