@extends("LwcBackends::layout.master")
@section('main')
    @include('vuleappsCommon::components.box.start', ['title' => "Subscribes"])
        <div class="row">
            <div class="col-md-12">
                <form action="{{ url()->current() }}" method="GET" class="form-inline">
                {!!
                    vl_form_bsText("Search", 's', request()->get('s', null))
                !!}
                {!!
                    vl_form_bsText("Post ID", 'post_id', request()->get('post_id', null))
                !!}
                {!!
                    vl_form_bsText("Email", 'email', request()->get('email', null))
                !!}
                <div class="form-group"><input type="submit" value="Filter" class="btn btn-primary"/></div>
                </form>
            </div>
        </div>
        <table class="table table-hover">
        	<thead>
        		<tr>
        			<th class="checkbox-all"></th>
        			<th>ID</th>
        			<th>Post ID</th>
        			<th>Name</th>
        			<th>Email</th>
        			<th>Created_at</th>
        			<th></th>
        		</tr>
        	</thead>
        	<tbody>
        	@if($models)
                @foreach($models as $model)
                    <tr id="register-{{ $model->id }}">
                        <td><input type="checkbox" name="ids[]" value="{{ $model->id }}"/></td>
                        <td>{{ $model->id }}</td>
                        <td>{{ $model->post_id }}</td>
                        <td>{{ $model->name }}</td>
                        <td>{{ $model->email }}</td>
                        <td>{{ date('H:m:s d/m/Y', strtotime($model->created_at)) }}</td>
                        <td>
                            <a href="{{ vl_admin_resoure_route('register.delete', [$model->id]) }}" data-el="#register-{{$model->id}}" class="delete label label-danger">Delete</a>
                        </td>
                    </tr>
                @endforeach
            @endif
        	</tbody>
        </table>
        <div>
            <div class="pull-right">{{ $models->appends(request()->all())->links() }}</div>
            <div class="clearfix"></div>
        </div>
    @include('vuleappsCommon::components.box.end')
@stop