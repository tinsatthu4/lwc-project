<?php
namespace VuleApps\LwcBackends\Repositories;
use VuleApps\LwcBackends\Models\TermRelation;
use VuleApps\Common\Repositories\Repository;
use Illuminate\Database\Eloquent\Collection;

class TermRepository extends Repository {
	function buildTermArgs(&$args = array()) {
		$model = $this->model;

		if(isset($args['exclude']))
			$model = $model->where("id", "<>", $args['exclude']);

		if(isset($args['fields']))
			$model = $model->select($args['fields']);

		return $model;
	}

	/**
	 * @param array $args exclude | fields
	 * @param integer $parent_id
	 * @param integer $level
	 * @return Collection
	 */
	function listByParent($args = array(), $type, $parent_id = 0, $level = 0) {
		$this->buildTermArgs($args);

		$rows = $this->model
			->type($type)
			->where('parent_id', $parent_id)
			->orderBy("id", "ASC")
			->get();
		$collection = new Collection();
		foreach($rows as &$row) {
			$row->level = $level;
			if($level)
				$row->title_option = str_repeat("--", $level) . "&nbsp;&nbsp;" . $row->title;
			else
				$row->title_option = $row->title;
			$collection->add($row);
			$collection = $collection->merge($this->listByParent($args, $type, $row->id, $level + 1));
		}

		return $collection;
	}

	function listTermIdsByPostId($post_id) {
		$relation = new TermRelation();
		return $relation->where('post_id', $post_id)->pluck("term_id")->all();
	}

	function getIdByType($id, $type = 'category') {
		return $this->model
			->type($type)
			->findOrFail($id);
	}

    function getBySlug($slug) {
        return $this->model
            ->where('slug', $slug)
            ->first();
    }
}