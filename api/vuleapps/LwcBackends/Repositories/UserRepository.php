<?php namespace VuleApps\LwcBackends\Repositories;
use App\User;
use Auth;
use DB;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use VuleApps\LwcBackends\Models\Subcribe;
use VuleApps\LwcBackends\Models\UserUpload;
use VuleApps\LwcBackends\Models\RegisterForm;

class UserRepository {
	function isCurrentUserHasRegisterCounrse($counrse_id) {
		$user = Auth::user();
		return DB::table('user_counrses')
			->where(['user_id' => $user->id, 'post_id' => $counrse_id])
			->count();
	}

	/**
	 * @param $userOrid
	 * @param $counrse_id
	 * @param $token
	 */
	function addCounrse($userOrid, $counrse_id) {
		if(!$userOrid instanceof User)
			$user = User::findOrFail($userOrid);
		else $user = $userOrid;

		DB::table('user_counrses')
			->insert([
				'user_id' => $user->id,
				'post_id' => $counrse_id
			]);
	}

	function addTokenByPackage($user_id, $package) {
		$packages = config('vuleapps.frontends.checkout_packages');
		$token = (int) $packages[$package]['token'];
		$user = User::findOrFail($user_id);
		$user->increment('token', $token);
	}

	/**
	 * Increment token for user
	 * @param $user_id
	 * @param $token
	 */
	function incrementToken($user_id, $token) {
		$user = User::findOrFail($user_id);
		$user->increment('token', (int) $token);
	}

	/**
	 * @param $userOrid
	 * @param $token
	 */
	function decrementToken($userOrid, $token) {
		if(!$userOrid instanceof User)
			$user = User::findOrFail($userOrid);
		else $user = $userOrid;

		return $user->decrement('token', (int) $token);
	}

	/**
	 * Store File upload by user
	 * save to table `user_uploads`
	 * @param $user_id
	 * @param UploadedFile $file
	 * @return UserUpload
	 */
	function storeFileUpload($user_id, UploadedFile $file) {
		$dir = storage_path("user_uploads/" .date("Y/m/d"));
		$user = User::findOrFail($user_id);
		$newFile = $file->move($dir, vl_file_slug( uniqid() . $file->getClientOriginalName()));

		$fileinfo = [
			'name' => $newFile->getFilename(),
			'path' => trim(str_replace("\\", "/", str_replace(storage_path(), "", $newFile->getPathname())), "/")
		];

		return $user->userUploads()->save(new UserUpload([
			'fileinfo' => $fileinfo
		]));
	}

	/**
	 * @param UserUpload $model
	 * @param $status
	 * @param $user_id_updated
	 */
	function changeStatusUserUploadFile(UserUpload $model, $status, $user_id_updated) {
		$model->status = $status;
		$model->user_updated = $user_id_updated;
		$model->save();
	}

	function saveRegister($user_id, $input) {
		$form = new RegisterForm();
		$input['user_id'] = $user_id;
		return $form->create($input);
	}

    function saveSubcribe($input) {
        $form = new Subcribe();
        return $form->create($input);
    }
}