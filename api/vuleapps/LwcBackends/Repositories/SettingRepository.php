<?php
namespace VuleApps\LwcBackends\Repositories;
use VuleApps\Common\Repositories\Repository;
use Cache;

class SettingRepository extends Repository {
	protected $settings = [];

	function getKeyByGroup($key, $group) {
		$setting = $this->getGroup($group);
		if(isset($setting[$key]))
			return $setting[$key];
		return null;
	}

	function getGroup($group) {
		$groups = $this->loadGroup($group);
		return $groups[$group];
	}

	/**
	 * @param string $group
	 * @return mixed
	 */
	function loadGroup($group = 'default') {
		$result = $this->model->where([
			'group' => $group
		])->get();

		return $result->pluck("value", "key")->all();
	}

	function saveGroup($input, $group = 'default') {
		$this->model->where(['group' => $group])
			->delete();
		$insert = [];
		foreach($input as $key => $val) {
			$insert[] = [
				'group' => $group,
				'key' => $key,
				'value' => is_array($val)? serialize($val) : $val,
				'serialize' => is_array($val)? 'yes' : 'no'
			];
		}
		$this->model->insert($insert);
	}
}