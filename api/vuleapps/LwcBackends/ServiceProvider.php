<?php
namespace VuleApps\LwcBackends;
use Illuminate\Support\ServiceProvider as LaravelServiceProvider;
use VuleApps\LwcBackends\Models\Checkout;
use VuleApps\LwcBackends\Models\Term;
use VuleApps\LwcBackends\Repositories\CommonRepository;
use VuleApps\LwcBackends\Repositories\SettingRepository;
use VuleApps\LwcBackends\Repositories\SettingRepositoryCache;
use VuleApps\LwcBackends\Repositories\PostRepository;
use VuleApps\LwcBackends\Repositories\PostRepositoryCache;
use VuleApps\LwcBackends\Repositories\TermRepository;
use VuleApps\LwcBackends\Repositories\CheckoutRepository;
use VuleApps\LwcBackends\Repositories\UserRepository;
use VuleApps\LwcBackends\Models\DefaultPost;
use VuleApps\LwcBackends\Models\Setting;

class ServiceProvider extends LaravelServiceProvider {

	public function boot() {
		require __DIR__ . '/route_binding.php';
		$this->loadViewsFrom(__DIR__ . '/views', 'LwcBackends');
	}

	public function register() {
		$this->app->singleton(PostRepository::class, function() {
			if(config('app.cache', false))
				return new PostRepositoryCache(new DefaultPost());
			else
				return new PostRepository(new DefaultPost());
		});

		$this->app->singleton(SettingRepository::class, function() {
			if(config('app.cache', false))
				return new SettingRepositoryCache(new Setting());
			else
				return new SettingRepository(new Setting());
		});

		$this->app->singleton(CommonRepository::class, function() {
			return new CommonRepository();
		});

		$this->app->singleton(TermRepository::class, function() {
			return new TermRepository(new Term());
		});

		$this->app->singleton(UserRepository::class, function() {
			return new UserRepository();
		});
	}
}
