<?php
namespace VuleApps\LwcBackends\Consoles;

use Illuminate\Console\Command;
use Illuminate\Database\Schema\Blueprint;
use VuleApps\LwcBackends\Models\EkModule;
use Log;
use Schema;
use DB;
use GuzzleHttp\Client;
use PHPHtmlParser\Dom;

class RunningCrawlerModuleConsole extends Command
{
    protected $signature = 'lwc:running-modules {course} {path} {--id=} {--frontend=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Running Crawler Module By Path';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $path = $this->argument('path');
            if(empty($path)) {
                $this->info('Empty Path');
                return;
            }
            $course = $this->argument('course');
            if(empty($course)) {
                $this->info('Empty Course');
                return;
            }
            $__zlcmid = $this->option('id');
            $frontend = $this->option('frontend');
            if(empty($__zlcmid)
              || empty($frontend)
            ) {
                $this->info('Empty Cookie');
                return;
            }
            $client = new Client(['headers' => [
                "Cookie" => "__zlcmid={$__zlcmid}; frontend={$frontend}"
            ]]);
            $res = $client->request(
              'GET', $path
            );
            $data = $res->getBody();
            if(empty($data)) {
                $this->info('Empty Data Return');
                return;
            }
            $dom = app(Dom::class);
            $dom->load($data);
            $modules = $dom->find('.item');
            foreach ($modules as $module) {
                $code = $module->find('.sku_logo span')->text;
                $title = $module->find('.product-name a')->text;
                $description = $module->find('.desc')->innerHtml;
                $link = $module->find('.product-name a')->getAttribute('href');

                $module = EkModule::create([
                    'course_id'   => $course,
                    'title'       => $title,
                    'code'        => $code,
                    'description' => $description,
                ]);

                $this->call('lwc:running-detail-modules', [
                    'module'      => $module->id,
                    'path'        => $link,
                    '--id'        => $__zlcmid,
                    '--frontend'  => $frontend
                ]);

                $this->info('Crawler all info module ' . $title);
            }
            $this->info('Crawler Modules Successful');
            return;
        } catch (\Exception $e) {
            Log::info($e->getMessage() . ' ' . $e->getLine() . ' ' . $e->getFile());
            return;
        }
    }
}
