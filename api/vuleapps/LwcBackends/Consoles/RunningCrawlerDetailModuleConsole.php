<?php
namespace VuleApps\LwcBackends\Consoles;

use Illuminate\Console\Command;
use Illuminate\Database\Schema\Blueprint;
use VuleApps\LwcBackends\Models\EkModule;
use Log;
use Schema;
use DB;
use GuzzleHttp\Client;
use PHPHtmlParser\Dom;

class RunningCrawlerDetailModuleConsole extends Command
{
    private $domain = 'http://learn.awards-uk.com';

    protected $signature = 'lwc:running-detail-modules {module} {path} {--id=} {--frontend=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Running Crawler Detail Module By Path';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $path = $this->argument('path');
            if(empty($path)) {
                $this->info('Empty Path');
                return;
            }
            $module_id = $this->argument('module');
            if(empty($module_id)) {
                $this->info('Empty Module');
                return;
            }
            $__zlcmid = $this->option('id');
            $frontend = $this->option('frontend');
            if(empty($__zlcmid)
              || empty($frontend)
            ) {
                $this->info('Empty Cookie');
                return;
            }
            $client = new Client(['headers' => [
                "Cookie" => "__zlcmid={$__zlcmid}; frontend={$frontend}"
            ]]);
            $res = $client->request(
              'GET', $path
            );
            $data = $res->getBody();
            if(empty($data)) {
                $this->info('Empty Data Return');
                return;
            }
            $dom = app(Dom::class);
            $dom->load($data);
            $module = $dom->find('.col-main');
            $learning_outcomes = $module->find('.learning-outcome')->innerHtml;
            $introduction = $module->find('.obs_short_description_two')->innerHtml;
            EkModule::where([
                  'id'  => $module_id
            ])->update([
                  'learning_outcomes' => $learning_outcomes,
                  'introduction'      => $introduction
            ]);

            $lessons = $dom->find('#sidebar_lesson_menu ul.open li');
            foreach ($lessons as $lesson) {
                  $link = $lesson->find('a')
                      ->getAttribute('href');
                  $title = $lesson->find('a')->text;
                  $this->call('lwc:running-lessons', [
                      'module'      => $module_id,
                      'path'        => $this->domain . $link,
                      '--id'        => $__zlcmid,
                      '--frontend'  => $frontend
                  ]);
            }
            $this->info("Crawler Detail Modules {$module_id} Successful");
            return;
        } catch (\Exception $e) {
            Log::info($e->getMessage() . ' ' . $e->getLine() . ' ' . $e->getFile());
            return;
        }
    }
}
