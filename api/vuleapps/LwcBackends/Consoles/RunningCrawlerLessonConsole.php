<?php
namespace VuleApps\LwcBackends\Consoles;

use Illuminate\Console\Command;
use Illuminate\Database\Schema\Blueprint;
use VuleApps\LwcBackends\Models\EkModule;
use VuleApps\LwcBackends\Models\EkLesson;
use VuleApps\LwcBackends\Models\EkLessonSession;
use Log;
use Schema;
use DB;
use GuzzleHttp\Client;
use PHPHtmlParser\Dom;

class RunningCrawlerLessonConsole extends Command
{
    protected $signature = 'lwc:running-lessons {module} {path} {--id=} {--frontend=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Running Crawler Lesson By Path';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $path = $this->argument('path');
            if(empty($path)) {
                $this->info('Empty Path');
                return;
            }
            $module_id = $this->argument('module');
            if(empty($module_id)) {
                $this->info('Empty Module');
                return;
            }
            $__zlcmid = $this->option('id');
            $frontend = $this->option('frontend');
            if(empty($__zlcmid)
              || empty($frontend)
            ) {
                $this->info('Empty Cookie');
                return;
            }
            $client = new Client(['headers' => [
                "Cookie" => "__zlcmid={$__zlcmid}; frontend={$frontend}"
            ]]);
            $res = $client->request(
              'GET', $path
            );
            $data = $res->getBody();
            if(empty($data)) {
                $this->info('Empty Data Return');
                return;
            }
            $dom = app(Dom::class);
            $dom->load($data);
            $lesson = EkLesson::create([
                'module_id'   => $module_id,
                'title'       => $dom->find('.lesson-title h2')->text
            ]);
            $dom_lessons = $dom->find('.lesson_section');
            $session = 1;
            foreach ($dom_lessons as $dom_lesson) {
                EkLessonSession::create([
                    'lession_id'  => $lesson->id,
                    'title'       => 'Section ' . $session,
                    'content'     => $dom_lesson->innerHtml
                ]);
                $this->info('Crawler all session ' . $session . ' of lesson ' . $lesson->title);
                $session++;
            }
            $this->info("Crawler Lesson {$lesson->title} Successful");
            return;
        } catch (\Exception $e) {
            Log::info($e->getMessage() . ' ' . $e->getLine() . ' ' . $e->getFile());
            return;
        }
    }
}
