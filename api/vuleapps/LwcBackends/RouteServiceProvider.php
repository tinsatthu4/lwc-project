<?php
namespace VuleApps\LwcBackends;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Route;

class RouteServiceProvider extends ServiceProvider
{
	/**
	 * Define your route model bindings, pattern filters, etc.
	 *
	 * @param  \Illuminate\Routing\Router  $router
	 * @return void
	 */
	public function boot(Router $router)
	{
		parent::boot($router);
	}

	/**
	 * Define the routes for the application.
	 *
	 * @param  \Illuminate\Routing\Router  $router
	 * @return void
	 */
	public function map(Router $router)
	{
		$this->mapWebRoutes($router);
	}

	/**
	 * Define the "web" routes for the application.
	 *
	 * These routes all receive session state, CSRF protection, etc.
	 *
	 * @param  \Illuminate\Routing\Router  $router
	 * @return void
	 */
	protected function mapWebRoutes(Router $router)
	{
		//login & logout
		$router->group([
			'prefix' => config("app.admin_prefix"),
			'as' => 'admin::',
			'middleware' => 'web'
		], function(Router $router) {
			$router->get('user/logout', [
				'as' => 'user.logout',
				'uses' => 'VuleApps\LwcBackends\Controllers\AuthController@getLogout'
			]);

			$router->get('user/login', [
				'as' => 'user.login',
				'uses' => 'VuleApps\LwcBackends\Controllers\AuthController@getLogin'
			]);

			$router->post('user/login', [
				'as' => 'user.login',
				'uses' => 'VuleApps\LwcBackends\Controllers\AuthController@postLogin'
			]);
		});

		$router->group([
			'prefix' => config('app.admin_prefix', 'admin'),
			'as' => 'admin::',
			'middleware' => 'webadmin',
			'namespace' => 'VuleApps\LwcBackends\Controllers'
		], function(Router $router) {
			//post
			$router->get('post/{id}/meta', 'DefaultPostController@getMeta');
			$router->resource("post", "PostController", [
				"parameters" => ["post" => "post_admin"]
			]);

			//page
			$router->resource("page", "PageController", [
				"parameters" => ["page" => "page_admin"]
			]);

			//term
			$router->get('term/by-type/{type}', [
				'as' => 'getByType',
				'uses' => 'TermController@getByType'
			])->where([
				'type' => '\w+'
			]);
			$router->get('terms/by-post-id/{post_id}', [
				'as' => 'getTermsByPostId',
				'uses' => 'TermController@getTermsByPostId'
			]);
			$router->resource('term', "TermController", [
				"parameters" => ["term" => "term_admin"]
			]);

            //Subscribe
            $router->get("subscribe", [
                'as'   => 'admin.subscribe.index',
                'uses' => 'SubscribeController@index'
            ]);
            $router->delete('subscribe/{subscribe_id}', [
                'as' => 'admin.subscribe.delete',
                'uses' => 'SubscribeController@delete'
            ]);

            //Register
            $router->get("register", [
                'as'   => 'admin.register.index',
                'uses' => 'RegisterController@index'
            ]);
            $router->delete('register/{register_id}', [
                'as' => 'admin.register.delete',
                'uses' => 'RegisterController@delete'
            ]);

            $router->resource("user", "UserController", [
                "parameters" => ["user" => "user_admin"]
            ]);

			//setting
			$router->post('setting/save', [
				'as' => 'setting.save',
				'uses' => 'SettingController@save'
			]);
			$router->post('setting/module', [
				'as' => 'setting.postModule',
				'uses' => 'SettingController@postModule'
			]);
			$router->get('setting/module', [
				'as' => 'setting.getModule',
				'uses' => 'SettingController@getModule'
			]);
			$router->get('setting/group/{group}', [
				'as' => 'setting.getGroup',
				'uses' => 'SettingController@getGroup'
			])->where('group', '[\w-]+');
			$router->resource('setting', "SettingController", [
				'except' => ['create', 'show', 'edit', 'store', 'update', 'destroy']
			]);

			//dashboard
			$router->get('/', [
				'as' => 'dashboard',
				'uses' => 'CommonController@dashboard'
			]);
		});
	}
}