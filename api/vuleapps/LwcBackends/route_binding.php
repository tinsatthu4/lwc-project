<?php
Route::bind("term_admin", function($value) {
	return \VuleApps\LwcBackends\Models\Term::findOrFail($value);
});

Route::bind("page_admin", function($value) {
	return \VuleApps\LwcBackends\Models\Page::findOrFail($value);
});

Route::bind("post_admin", function($value) {
	return \VuleApps\LwcBackends\Models\Post::findOrFail($value);
});

Route::bind("user_admin", function($value) {
    return \App\User::findOrFail($value);
});

Route::bind("subscribe_id", function($value) {
    return \VuleApps\LwcBackends\Models\Subscribe::findOrFail($value);
});

Route::bind("register_id", function($value) {
    return \VuleApps\LwcBackends\Models\RegisterForm::findOrFail($value);
});