<?php
namespace VuleApps\LwcBackends\Models;

use Illuminate\Database\Eloquent\Model;
use VuleApps\Common\PresentModelTrait;
use Illuminate\Database\Eloquent\Builder;

class Page extends DefaultPost {
	const TYPE = 'page';

	protected static function boot()
	{
		parent::boot();

		static::addGlobalScope('post_type', function(Builder $builder) {
			$builder->where('type', Page::TYPE);
		});
	}
}