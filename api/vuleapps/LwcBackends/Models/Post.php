<?php
namespace VuleApps\LwcBackends\Models;

use Illuminate\Database\Eloquent\Model;
use VuleApps\Common\PresentModelTrait;
use Illuminate\Database\Eloquent\Builder;

class Post extends DefaultPost {
	const TYPE = 'post';

	protected static function boot()
	{
		parent::boot();

		static::addGlobalScope('post_type', function(Builder $builder) {
			$builder->where('type', Post::TYPE);
		});
	}

    function term()
    {
        return $this->belongsToMany(Term::class, 'term_relations', 'term_id', 'post_id');
    }
}