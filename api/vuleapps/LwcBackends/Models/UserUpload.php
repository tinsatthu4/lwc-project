<?php
namespace VuleApps\LwcBackends\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
class UserUpload extends Model
{
	const STATUS_NEW = 'new';
	const STATUS_REVIEW = 'review';
	const STATUS_REMARK = 'remark';

	protected $fillable = [
		'fileinfo'
	];

	function setFileinfoAttribute($value) {
		$this->attributes['fileinfo'] = json_encode($value);
	}

	function getFileinfoAttribute($value) {
		return json_decode($value);
	}

	function user() {
		return $this->belongsTo(User::class, 'user_updated');

	}

	function member() {
		return $this->belongsTo(User::class, 'user_id');
	}
}