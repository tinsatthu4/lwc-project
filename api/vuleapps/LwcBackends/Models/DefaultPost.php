<?php
namespace VuleApps\LwcBackends\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Collection;
use VuleApps\Common\PresentModelTrait;
use Illuminate\Database\Eloquent\Builder;

class DefaultPost extends Model {
	protected $table = 'posts';
	protected $metadata = [];
	public $presenter = '\VuleApps\LwcBackends\Presenter\DefaultPost';
	use PresentModelTrait;

	protected $fillable = [
		"title", "slug", "type", "image", "content", "excerpt", "sort", "status",
		"meta_title", "meta_content", "meta_keywords"
	];

	public function scopeActive($query)
	{
		return $query->where('status', 'publish');
	}

	public function scopePostType($query, $type) {
		return $query->where('type', $type);
	}

	public function setSlugAttribute($value) {
		if(empty($value)) {
			$this->attributes['slug'] = str_slug($this->attributes['title']);
		}

		$this->attributes['slug'] = str_slug($value);
	}

	public function postmeta() {
		return $this->hasMany(PostMeta::class, 'post_id');
	}

	public function metavalue($key) {
		if(!$this->postmeta->isEmpty())
			$this->metadata = $this->postmeta->pluck('meta_value', 'meta_key')->all();

		if(isset($this->metadata[$key]))
			return $this->metadata[$key];

		return null;
	}

	public function terms() {
		return $this->belongsToMany(Term::class, 'term_relations', 'post_id');
	}

	public function termRelations() {
		return $this->hasMany(TermRelation::class, 'post_id');
	}

    public function comments(){
        return $this->hasMany(Comment::class, 'post_id', 'id');
    }

    public function user() {
        return $this->belongsTo(User::class, 'author', 'id');
    }

	public function modules() {
		return $this->hasMany(DefaultPost::class, 'parent_id');
	}

    public function register_form() {
        return $this->hasMany(RegisterForm::class, 'post_id', 'id');
    }
}