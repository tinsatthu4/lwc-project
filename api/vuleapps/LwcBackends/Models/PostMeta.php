<?php
namespace VuleApps\LwcBackends\Models;
use Illuminate\Database\Eloquent\Model;

class PostMeta extends Model {
	protected $table = 'postmeta';
	protected $fillable = ['post_id', 'meta_key', 'meta_value'];
	public $timestamps = false;
}