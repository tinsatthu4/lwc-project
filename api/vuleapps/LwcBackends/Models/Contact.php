<?php
namespace VuleApps\LwcBackends\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table = 'contacts';
}