<?php
namespace VuleApps\LwcBackends\Models;

use Illuminate\Database\Eloquent\Model;

class EkLesson extends Model
{
    protected $table = 'ek_lessons';

    protected $fillable = [
        "module_id",
  		  "title",
        "description",
        "status"
  	];
}
