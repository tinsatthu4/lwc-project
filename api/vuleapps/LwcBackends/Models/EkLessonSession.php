<?php
namespace VuleApps\LwcBackends\Models;

use Illuminate\Database\Eloquent\Model;

class EkLessonSession extends Model
{
    protected $table = 'ek_lesson_sessions';

    protected $fillable = [
        "lession_id",
  		  "title",
        "content",
        "status"
  	];
}
