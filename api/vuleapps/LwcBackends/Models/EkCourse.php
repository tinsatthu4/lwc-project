<?php
namespace VuleApps\LwcBackends\Models;

use Illuminate\Database\Eloquent\Model;

class EkCourse extends Model
{
    protected $table = 'ek_courses';

    protected $fillable = [
  		  "title",
        "description",
        "status"
  	];

    public function modules()
    {
        return $this->hasMany(EkModule::class, 'course_id', 'id');
    }
}
