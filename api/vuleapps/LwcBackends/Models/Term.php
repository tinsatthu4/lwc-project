<?php namespace VuleApps\LwcBackends\Models;

use Illuminate\Database\Eloquent\Model;
use VuleApps\Common\PresentModelTrait;

class Term extends Model {
	const TYPE_COUNRSE = 'counrse';
	const TYPE_COUNRSEOFFLINE = 'counrseoffline';
	const TYPE_EBOOK = 'ebook';
	const TYPE_POST = 'post';

	protected $table = 'terms';
	public $presenter = '\VuleApps\LwcBackends\Presenter\TermPresent';
	use PresentModelTrait;

	protected $fillable = [
		"title", "slug", "parent_id","type", "meta_title", "meta_content", "meta_keywords"
	];

	public function scopeType($query, $type)
	{
		return $query->where('type', $type);
	}

	public function setSlugAttribute($value) {
		if(empty($value)) {
			$this->attributes['slug'] = str_slug($this->attributes['title']);
		}

		$this->attributes['slug'] = str_slug($value);
	}

    public function post()
    {
        return $this->belongsToMany(Post::class, 'term_relations');
    }
}