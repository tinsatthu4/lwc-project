<?php
namespace VuleApps\LwcBackends\Models;
use Illuminate\Database\Eloquent\Model;

class UserMeta extends Model {
	protected $table = 'usermeta';
	protected $fillable = ['user_id', 'meta_key', 'meta_value', 'serialize'];
	public $timestamps = false;
}