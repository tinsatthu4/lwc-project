<?php namespace VuleApps\LwcBackends\Models;

use Illuminate\Database\Eloquent\Model;

class TermRelation extends  Model {
	protected $table = 'term_relations';
	public $timestamps = false;

    protected $fillable = [
       "post_id", "term_id"
    ];

}