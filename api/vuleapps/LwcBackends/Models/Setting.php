<?php
namespace VuleApps\LwcBackends\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model {
	public $timestamps = true;

	protected $fillable = [
		"group", "key"
	];

	public function getValueAttribute($value) {
		if($this->attributes['serialize'] == 'yes')
			return unserialize($value);

		return $value;
	}
}