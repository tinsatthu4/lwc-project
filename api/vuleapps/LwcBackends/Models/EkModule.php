<?php
namespace VuleApps\LwcBackends\Models;

use Illuminate\Database\Eloquent\Model;

class EkModule extends Model
{
    protected $table = 'ek_modules';

    protected $fillable = [
        "course_id",
        "code",
  		  "title",
        "description",
        "learning_outcomes",
        "introduction",
        "status"
  	];
}
