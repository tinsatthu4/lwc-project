<?php namespace VuleApps\LwcBackends\Models;
use Illuminate\Database\Eloquent\Model;
use VuleApps\LwcBackends\Models\Checkout;

class RegisterForm extends Model {
	protected $fillable = ['user_id', 'email', 'name', 'post_id'];

    function post() {
        return $this->belongsTo(DefaultPost::class, 'post_id', 'id');
    }
}