<?php namespace VuleApps\LwcBackends\Jobs;
use App\Jobs\Job;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;
use VuleApps\LwcBackends\Models\Notification;

class AddNotifications extends Job implements ShouldQueue {
	use InteractsWithQueue, SerializesModels;

	/**
	 * @var Model
	 */
	protected $user_id;
	protected $type;
	protected $message;
	public function __construct($type, $message, $user_id = 0)
	{
		$this->user_id = $user_id;
		$this->type = $type;
		$this->message = $message;
	}

	function handle() {
		try {
			Notification::create([
				'user_id' => $this->user_id,
				'type' => $this->type,
				'message' => $this->message
			]);
		} catch (\Exception $e) {
			Log::error($e->getMessage());
		}
	}
}