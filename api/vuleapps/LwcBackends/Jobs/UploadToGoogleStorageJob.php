<?php

namespace VuleApps\LwcBackends\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Storage;
use DB;
use Log;

class UploadToGoogleStorageJob extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
	protected $realPathDir, $name, $mimeType, $size, $directory;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($realPathDir, $name)
    {
		$this->realPathDir = $realPathDir;
		$this->name = $name;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
		//start merge files
		try {
			$realPathFile = "{$this->realPathDir}/{$this->name}";
			$files = scandir($this->realPathDir);
			asort($files, SORT_NUMERIC);

			if(!file_exists($realPathFile))
			{
				$handle = fopen($realPathFile, "w");
				foreach($files as $file) {
					if($file == '.' || $file == '..') continue;
					fwrite($handle, file_get_contents("{$this->realPathDir}/{$file}"));
					@unlink("{$this->realPathDir}/{$file}");
				}
				fclose($handle);
			}

			$client = app('googleClient');
			$client->addScope(\Google_Service_Storage::DEVSTORAGE_FULL_CONTROL);
			$client->setDefer(true);

			$storage = new \Google_Service_Storage($client);

			$opt = array (
				'name' => $this->name,
				'uploadType' => 'resumable'
			);
			$this->mimeType = mime_content_type($realPathFile);
			$object = new \Google_Service_Storage_StorageObject($client);

			$object->setBucket(config('services.google_storage.bucket'));
			$object->setName($this->name);
			$object->setContentType($this->mimeType);
			$request = $storage->objects->insert(config('services.google_storage.bucket'), $object, $opt);

			$chunkSizeBytes = 10 * 1024 * 1024;
			$media = new \Google_Http_MediaFileUpload($client, $request, $this->mimeType, null, true, $chunkSizeBytes);
			$media->setFileSize(filesize($realPathFile));
			$handle = fopen($realPathFile, "rb");
			while(!feof($handle)) {
				$media->nextChunk(fread($handle, $chunkSizeBytes));
			}

			$client->setDefer(false);
			fclose($handle);
			$objectInserted = $storage->objects->get($object->getBucket(), $this->name);

			//write DB
			DB::table('google_objects')
				->where('name', $this->name)
				->update([
					'status' => 1,
					'metadata' => json_encode([
						'id' => $objectInserted->getId(),
						'media_link' => $objectInserted->getMediaLink(),
						'meta_data' => $objectInserted->getMetadata(),
						'self_link' => $objectInserted->getSelfLink(),
					])
				]);
			//remove file
			@unlink($realPathFile);
			@rmdir($this->realPathDir);

			$postBody = new \Google_Service_Storage_ObjectAccessControl($client);
			$postBody->setEntity('allUsers');
			$postBody->setRole('READER');
			$storage->objectAccessControls->insert(config('services.google_storage.bucket'), $this->name, $postBody);

		} catch(\Exception $e) {
			Log::error($e->getMessage());
		}
	}
}
