<?php
namespace VuleApps\LwcBackends\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;
use Mail;
use SettingRepository;
class SendEmailJob extends Job implements ShouldQueue {
	use InteractsWithQueue, SerializesModels;

	protected $subject;
	protected $template;
	protected $data;
	protected $to;

	public function __construct($subject, $template, $to, $data = []) {
		$this->subject = $subject;
		$this->template = $template;
		$this->data = $data;
		$this->to = $to;
	}

	public function handle() {
		try {
			Mail::send('emails.custom', $this->data, function ($message) {
				$message->from(SettingRepository::getKeyByGroup('form_email', 'email'), SettingRepository::getKeyByGroup('form_name', 'email'));

				$message->to($this->to);
			});
		} catch (\Exception $e) {
			Log::error($e->getMessage());
		}
	}
}