<?php

namespace VuleApps\LwcBackends\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;
use DB;
use VuleApps\LwcBackends\Models\Activity;

class UserBuyTokenFile extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    protected $userId, $token;

    public function __construct($userId, $token)
    {
        $this->userId = $userId;
        $this->token = $token;
    }

    public function handle()
    {
        try {
            $activities = new Activity();
            $activities->user_id = $this->userId;
            $activities->type = Activity::TYPE_USER_BUY_TOKEN;
            $activities->description = 'buy '. $this->token .' Token ';
            $activities->save();
        } catch(\Exception $e) {
            Log::error($e->getMessage());
        }
    }
}
