<?php

namespace VuleApps\LwcBackends\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;
use DB;
use VuleApps\LwcBackends\Models\Activity;

class UserUploadFile extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    protected $userId, $fileUpload;

    public function __construct($userId, $fileUpload)
    {
        $this->userId = $userId;
        $this->fileUpload = $fileUpload;
    }

    public function handle()
    {
        try {
            $activities = new Activity();
            $activities->user_id = $this->userId;
            $activities->type = 'user_upload_file';
            $activities->description = 'User upload file '. $this->fileUpload;
            $activities->save();
        } catch(\Exception $e) {
            Log::error($e->getMessage());
        }
    }
}
