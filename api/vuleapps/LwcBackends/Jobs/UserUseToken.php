<?php

namespace VuleApps\LwcBackends\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;
use DB;
use VuleApps\LwcBackends\Models\Activity;

class UserUseToken extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    protected $userId, $token, $todo;

    public function __construct($userId, $token, $todo)
    {
        $this->userId = $userId;
        $this->token = $token;
        $this->todo = $todo;
    }

    public function handle()
    {
        try {
            $activities = new Activity();
            $activities->user_id = $this->userId;
            $activities->type = Activity::TYPE_USER_USE_TOKEN;
            $activities->description = 'use '. $this->token .' Token ' . $this->todo;
            $activities->save();
        } catch(\Exception $e) {
            Log::error($e->getMessage());
        }
    }
}
