<?php namespace VuleApps\LwcBackends\Jobs;
use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;
use DB;
use VuleApps\LwcBackends\Models\Activity;

class AddUserActivity extends Job implements ShouldQueue {
	use InteractsWithQueue, SerializesModels;
	protected $fillable;

	public function __construct($fillable = array())
	{
		$this->fillable = $fillable;
	}

	public function handle() {
		try {
			Activity::create($this->fillable);
		} catch(\Exception $e) {
			Log::error($e->getMessage());
		}
	}

}