<?php

namespace VuleApps\LwcBackends\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;
use Mail;

class EmailActiveAccount extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
	protected $email, $name, $link;
    
    public function __construct($email, $name, $link)
    {
		$this->email = $email;
        $this->name = $name;
		$this->link = $link;
    }

    public function handle()
    {
		//send email active account
		try {
            $data = [
                'email'  => $this->email,
                'name'   => $this->name,
                'link'   => $this->link
            ];
            Mail::send('email.email_active', $data, function ($m) use ($data) {
                $m->from(config('mail.from.address'), config('mail.from.name'));
                $m->to($data['email'], $data['name'])->subject('Kích hoạt tài khoản!');
            });
		} catch(\Exception $e) {
			Log::error($e->getMessage());
		}
	}
}
