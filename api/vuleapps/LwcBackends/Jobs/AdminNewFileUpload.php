<?php

namespace VuleApps\LwcBackends\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;
use VuleApps\LwcBackends\Models\Activity;

class AdminNewFileUpload extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    protected $userUploadId, $link;

    public function __construct($userUploadId, $link)
    {
        $this->userUploadId = $userUploadId;
        $this->link = $link;
    }

    public function handle()
    {
        try {
            $activities = new Activity();
            $activities->user_id = 0;
            $activities->type = Activity::TYPE_ADMIN_REVIEW_FILE;
			$activities->description = json_encode([
				'user_upload_id' => $this->userUploadId,
				'link' => $this->link
			]);
            $activities->save();
        } catch(\Exception $e) {
            Log::error($e->getMessage());
        }
    }
}
