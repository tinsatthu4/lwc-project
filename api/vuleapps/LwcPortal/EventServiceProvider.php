<?php namespace VuleApps\LwcPortal;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\ServiceProvider as LaravelServiceProvider;
use VuleApps\LwcPortal\Models\Activity;
use VuleApps\LwcPortal\Models\UserTask;
use Illuminate\Foundation\Bus\DispatchesJobs;
use VuleApps\LwcPortal\Jobs\WriteActivity;
use VuleApps\LwcPortal\Models\UserModule;

class EventServiceProvider extends LaravelServiceProvider {
	use DispatchesJobs;
	function boot() {
		UserModule::created(function(Model $userModule) {
			$this->dispatch(new WriteActivity (
				$userModule->user_id,
				Activity::TYPE_USERMODULE_CREATE,
				$userModule->toArray()
			));
		});

		UserTask::saving(function(Model $userTask) {
			if($userTask->status == UserTask::STATUS_DRAFT) {
				$this->dispatch(new WriteActivity (
					$userTask->user_id,
					Activity::TYPE_USERTASK_SAVE,
					$userTask->toArray()
				));
			}

			if($userTask->status == UserTask::STATUS_SUBMISSION ) {
				$this->dispatch(new WriteActivity (
					$userTask->user_id,
					Activity::TYPE_USERTASK_SUBMISSION,
					$userTask->toArray()
				));
			}
		});
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		// TODO: Implement register() method.
	}

}