<?php

namespace VuleApps\LwcPortal\Validation;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use VuleApps\LwcPortal\Models\CourseTeacher;
class TeacherHasCourse
{
	public function validation($attribute, $value, $parameters, $validator)
	{
		try {
			CourseTeacher::where([
				'user_id' => $value,
				'post_id' => $parameters[0]
			])->firstOrFail();
			return true;
		}
		catch (ModelNotFoundException $e) {
			return false;
		}
	}
}