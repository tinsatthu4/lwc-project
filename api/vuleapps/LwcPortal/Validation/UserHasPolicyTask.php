<?php

namespace VuleApps\LwcPortal\Validation;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use VuleApps\LwcPortal\Models\Counrse;
use VuleApps\LwcPortal\Models\Module;
use VuleApps\LwcPortal\Models\Task;
use VuleApps\LwcPortal\Models\UserCounrse;
use VuleApps\LwcPortal\Models\UserModule;
use VuleApps\LwcPortal\Models\UserTask;

class UserHasPolicyTask
{
    public function validation($attribute, $value, $parameters, $validator)
    {
        try {
            $task = Task::findOrFail(isset($parameters[0]) ? $parameters[0] : null);
            $module = Module::findOrFail($task->post_id);
            $cournse = Counrse::findOrFail($module->parent_id);
            UserCounrse::where([
                'user_id'   => $value,
                'post_id'   => $cournse->id
            ])->firstOrFail();
            return true;
        }
        catch (ModelNotFoundException $e) {
            return false;
        }
    }
}