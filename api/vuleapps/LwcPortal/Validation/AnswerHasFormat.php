<?php
namespace VuleApps\LwcPortal\Validation;

class AnswerHasFormat {
	public function validation($attribute, $values, $parameters, $validator)
	{
		foreach($values as $key => $value) {
			if(!is_string($value))
				return false;
		}

		return true;
	}
}