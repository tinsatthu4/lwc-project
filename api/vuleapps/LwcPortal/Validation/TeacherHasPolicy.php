<?php

namespace VuleApps\LwcPortal\Validation;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;
use VuleApps\LwcPortal\Models\Counrse;
use VuleApps\LwcPortal\Models\Module;
use VuleApps\LwcPortal\Models\Task;

class TeacherHasPolicy
{
    public function validation($attribute, $value, $parameters, $validator)
    {
        if(!isset($parameters[0]) || !isset($parameters[1])) {
            return false;
        }
        if($parameters[1] == 'module') {
            return $this->checkTeacherHasPolicyToModule($value, $parameters[0]);
        }
        if($parameters[1] == 'task') {
            return $this->checkTeacherHasPolicyToTask($value, $parameters[0]);
        }
        return false;
    }

    private function checkTeacherHasPolicyToModule($teacher_id, $course_id)
    {
        try {
            $course = Counrse::findOrFail($course_id);
            if($teacher_id != $course->teacher_id) {
                return false;
            }
            return true;
        }
        catch(ModelNotFoundException $e) {
            return false;
        }
    }

    private function checkTeacherHasPolicyToTask($teacher_id, $module_id)
    {
        try {
            $module = Module::findOrFail($module_id);
            $course = Counrse::findOrFail($module->parent_id);
            if($teacher_id != $course->teacher_id) {
                return false;
            }
            return true;
        }
        catch(ModelNotFoundException $e) {
            return false;
        }
    }
}