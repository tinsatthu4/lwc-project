<?php

namespace VuleApps\LwcPortal\Validation;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use VuleApps\LwcPortal\Models\Task;

class MatchTaskKeyword
{
    public function validation($attribute, $value, $parameters, $validator)
    {
        try {
            if(empty($value)) {
                return false;
            }
            $task = Task::select('id', 'keywords')->findOrFail(isset($parameters[0]) ? $parameters[0] : null);
            $value = strtolower($value);
			$keywords = explode(',', $task->keywords);
            foreach ($keywords as $keyword) {
                if(gettype(strpos($value, strtolower($keyword))) == 'boolean' &&
                    strpos($value, strtolower($keyword)) === false) {
                    return false;
                }
            }
            return true;
        }
        catch (ModelNotFoundException $e) {
            return false;
        }
    }
}