<!doctype html>
<html lang="en" ng-app="myPortal">
<head>
<meta charset="UTF-8">
@include('vuleappsLwcPortal::layout.header')
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <notifications-bar class="notifications"></notifications-bar>
    @include('vuleappsLwcPortal::layout.sidebar')
    {{--<div class="content-wrapper">--}}
        {{--<section class="content" style="min-height: 800px">@yield('main')</section>--}}
    {{--</div>--}}
    <div class="content-wrapper">
        <section class="content-header">
            {{--<h1>--}}
              {{--{{ 'LWC Portal Angular' }}--}}
              {{--<small>Teacher Page</small>--}}
            {{--</h1>--}}
            {{--<ol class="breadcrumb">--}}
              {{--<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>--}}
              {{--<li class="active">Widgets</li>--}}
            {{--</ol>--}}
            {{--<br/>--}}
        </section>
        <section class="content" style="min-height: 800px"><ui-view></ui-view></section>
    </div>

    {{--<footer class="main-footer">--}}
        {{--<strong>Copyright &copy; 2014-2015 <a href="#">portal</a>.</strong> All rights reserved.--}}
    {{--</footer>--}}
    <div class="control-sidebar-bg"></div>
</div>
@include('vuleappsLwcPortal::layout.footer')
</body>
</html>