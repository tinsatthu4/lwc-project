<header class="main-header">
<a href="#" class="logo">
  <!-- mini logo for sidebar mini 50x50 pixels -->
  <span class="logo-mini"><b>PORTAL</b></span>
  <!-- logo for regular state and mobile devices -->
  <span class="logo-lg"><b>PORTAL</b></span>
</a>
<nav class="navbar navbar-static-top">
  <!-- Sidebar toggle button-->
  <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
    <span class="sr-only">Toggle navigation</span>
  </a>

  {{--<div class="navbar-custom-menu">--}}
    {{--<ul class="nav navbar-nav">--}}
      {{--<li class="dropdown user user-menu">--}}
        {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown">--}}
          {{--<span class="hidden-xs">Admin</span>--}}
        {{--</a>--}}
        {{--<ul class="dropdown-menu">--}}
          {{--<li class="user-footer">--}}
            {{--<div class="pull-left">--}}
              {{--<a href="#" class="btn btn-default btn-flat">Profile</a>--}}
            {{--</div>--}}
            {{--<div class="pull-right">--}}
              {{--<a href="{{ vl_admin_route('user.logout') }}" class="btn btn-default btn-flat">Sign out</a>--}}
            {{--</div>--}}
          {{--</li>--}}
        {{--</ul>--}}
      {{--</li>--}}
    {{--</ul>--}}
  {{--</div>--}}
</nav>
</header>
<aside class="main-sidebar">
<section class="sidebar">
        <ul class="sidebar-menu">
          <li class="header">MAIN NAVIGATION</li>
          <li class="treeview active">
            <a href="#">
              <i class="fa fa-dashboard"></i> <span>Counrse</span> <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu menu-open">
              <li><a href="#" ui-sref="counrse_create">Add Counrse</a></li>
              <li><a href="#" ui-sref="counrse_list">List Counrse</a></li>
            </ul>
          </li>
          <li class="treeview active">
            <a href="#">
              <i class="fa fa-dashboard"></i> <span>Modules</span> <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu menu-open">
              <li><a href="#" ui-sref="module_create">Add Module</a></li>
              <li><a href="#" ui-sref="module_list">List Module</a></li>
            </ul>
          </li>
        </ul>

</section>
</aside>
