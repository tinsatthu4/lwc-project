MIME-Version: 1.0
Content-Type: multipart/related; boundary="----=_NextPart_01D227F5.5FD6A920"

This document is a Single File Web Page, also known as a Web Archive file.  If you are seeing this message, your browser or editor doesn't support Web Archive files.  Please download a browser that supports Web Archive, such as Windows® Internet Explorer®.

------=_NextPart_01D227F5.5FD6A920
Content-Location: file:///C:/2A24B1E5/master_file.htm
Content-Transfer-Encoding: quoted-printable
Content-Type: text/html; charset="us-ascii"

<html xmlns:v=3D"urn:schemas-microsoft-com:vml"
xmlns:o=3D"urn:schemas-microsoft-com:office:office"
xmlns:w=3D"urn:schemas-microsoft-com:office:word"
xmlns:m=3D"http://schemas.microsoft.com/office/2004/12/omml"
xmlns=3D"http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=3DContent-Type content=3D"text/html; charset=3Dus-ascii">
<meta name=3DProgId content=3DWord.Document>
<meta name=3DGenerator content=3D"Microsoft Word 14">
<meta name=3DOriginator content=3D"Microsoft Word 14">
<link rel=3DFile-List href=3D"master_file_files/filelist.xml">
<link rel=3DEdit-Time-Data href=3D"master_file_files/editdata.mso">
<!--[if !mso]>
<style>
v\:* {behavior:url(#default#VML);}
o\:* {behavior:url(#default#VML);}
w\:* {behavior:url(#default#VML);}
.shape {behavior:url(#default#VML);}
</style>
<![endif]-->
<title>ASSESSMENT ACTIVITY</title>
<!--[if gte mso 9]><xml>
 <o:DocumentProperties>
  <o:Author>WinXP</o:Author>
  <o:LastAuthor>Phong</o:LastAuthor>
  <o:Revision>7</o:Revision>
  <o:TotalTime>417</o:TotalTime>
  <o:LastPrinted>2012-07-10T09:06:00Z</o:LastPrinted>
  <o:Created>2016-10-11T08:59:00Z</o:Created>
  <o:LastSaved>2016-10-16T14:36:00Z</o:LastSaved>
  <o:Pages>7</o:Pages>
  <o:Words>1545</o:Words>
  <o:Characters>8812</o:Characters>
  <o:Company>Office XP</o:Company>
  <o:Lines>73</o:Lines>
  <o:Paragraphs>20</o:Paragraphs>
  <o:CharactersWithSpaces>10337</o:CharactersWithSpaces>
  <o:Version>14.00</o:Version>
 </o:DocumentProperties>
</xml><![endif]-->
<link rel=3DthemeData href=3D"master_file_files/themedata.thmx">
<link rel=3DcolorSchemeMapping href=3D"master_file_files/colorschememapping=
.xml">
<!--[if gte mso 9]><xml>
 <w:WordDocument>
  <w:View>Print</w:View>
  <w:SpellingState>Clean</w:SpellingState>
  <w:GrammarState>Clean</w:GrammarState>
  <w:TrackMoves>false</w:TrackMoves>
  <w:TrackFormatting/>
  <w:ValidateAgainstSchemas/>
  <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>
  <w:IgnoreMixedContent>false</w:IgnoreMixedContent>
  <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>
  <w:DoNotPromoteQF/>
  <w:LidThemeOther>EN-US</w:LidThemeOther>
  <w:LidThemeAsian>X-NONE</w:LidThemeAsian>
  <w:LidThemeComplexScript>X-NONE</w:LidThemeComplexScript>
  <w:Compatibility>
   <w:BreakWrappedTables/>
   <w:SnapToGridInCell/>
   <w:WrapTextWithPunct/>
   <w:UseAsianBreakRules/>
   <w:DontGrowAutofit/>
   <w:DontUseIndentAsNumberingTabStop/>
   <w:FELineBreak11/>
   <w:WW11IndentRules/>
   <w:DontAutofitConstrainedTables/>
   <w:AutofitLikeWW11/>
   <w:HangulWidthLikeWW11/>
   <w:UseNormalStyleForList/>
   <w:SplitPgBreakAndParaMark/>
   <w:DontVertAlignCellWithSp/>
   <w:DontBreakConstrainedForcedTables/>
   <w:DontVertAlignInTxbx/>
   <w:Word11KerningPairs/>
   <w:CachedColBalance/>
   <w:UseFELayout/>
  </w:Compatibility>
  <w:BrowserLevel>MicrosoftInternetExplorer4</w:BrowserLevel>
  <m:mathPr>
   <m:mathFont m:val=3D"Cambria Math"/>
   <m:brkBin m:val=3D"before"/>
   <m:brkBinSub m:val=3D"&#45;-"/>
   <m:smallFrac m:val=3D"off"/>
   <m:dispDef/>
   <m:lMargin m:val=3D"0"/>
   <m:rMargin m:val=3D"0"/>
   <m:defJc m:val=3D"centerGroup"/>
   <m:wrapIndent m:val=3D"1440"/>
   <m:intLim m:val=3D"subSup"/>
   <m:naryLim m:val=3D"undOvr"/>
  </m:mathPr></w:WordDocument>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <w:LatentStyles DefLockedState=3D"false" DefUnhideWhenUsed=3D"false"
  DefSemiHidden=3D"false" DefQFormat=3D"false" LatentStyleCount=3D"267">
  <w:LsdException Locked=3D"false" QFormat=3D"true" Name=3D"Normal"/>
  <w:LsdException Locked=3D"false" QFormat=3D"true" Name=3D"heading 1"/>
  <w:LsdException Locked=3D"false" QFormat=3D"true" Name=3D"heading 2"/>
  <w:LsdException Locked=3D"false" QFormat=3D"true" Name=3D"heading 3"/>
  <w:LsdException Locked=3D"false" QFormat=3D"true" Name=3D"heading 4"/>
  <w:LsdException Locked=3D"false" QFormat=3D"true" Name=3D"heading 5"/>
  <w:LsdException Locked=3D"false" QFormat=3D"true" Name=3D"heading 6"/>
  <w:LsdException Locked=3D"false" SemiHidden=3D"true" UnhideWhenUsed=3D"tr=
ue"
   QFormat=3D"true" Name=3D"heading 7"/>
  <w:LsdException Locked=3D"false" SemiHidden=3D"true" UnhideWhenUsed=3D"tr=
ue"
   QFormat=3D"true" Name=3D"heading 8"/>
  <w:LsdException Locked=3D"false" QFormat=3D"true" Name=3D"heading 9"/>
  <w:LsdException Locked=3D"false" Priority=3D"99" Name=3D"footer"/>
  <w:LsdException Locked=3D"false" SemiHidden=3D"true" UnhideWhenUsed=3D"tr=
ue"
   QFormat=3D"true" Name=3D"caption"/>
  <w:LsdException Locked=3D"false" Priority=3D"99" Name=3D"page number"/>
  <w:LsdException Locked=3D"false" QFormat=3D"true" Name=3D"Title"/>
  <w:LsdException Locked=3D"false" Priority=3D"1" Name=3D"Default Paragraph=
 Font"/>
  <w:LsdException Locked=3D"false" QFormat=3D"true" Name=3D"Subtitle"/>
  <w:LsdException Locked=3D"false" QFormat=3D"true" Name=3D"Strong"/>
  <w:LsdException Locked=3D"false" QFormat=3D"true" Name=3D"Emphasis"/>
  <w:LsdException Locked=3D"false" Priority=3D"99" Name=3D"No List"/>
  <w:LsdException Locked=3D"false" Priority=3D"99" SemiHidden=3D"true"
   Name=3D"Placeholder Text"/>
  <w:LsdException Locked=3D"false" Priority=3D"1" QFormat=3D"true" Name=3D"=
No Spacing"/>
  <w:LsdException Locked=3D"false" Priority=3D"60" Name=3D"Light Shading"/>
  <w:LsdException Locked=3D"false" Priority=3D"61" Name=3D"Light List"/>
  <w:LsdException Locked=3D"false" Priority=3D"62" Name=3D"Light Grid"/>
  <w:LsdException Locked=3D"false" Priority=3D"63" Name=3D"Medium Shading 1=
"/>
  <w:LsdException Locked=3D"false" Priority=3D"64" Name=3D"Medium Shading 2=
"/>
  <w:LsdException Locked=3D"false" Priority=3D"65" Name=3D"Medium List 1"/>
  <w:LsdException Locked=3D"false" Priority=3D"66" Name=3D"Medium List 2"/>
  <w:LsdException Locked=3D"false" Priority=3D"67" Name=3D"Medium Grid 1"/>
  <w:LsdException Locked=3D"false" Priority=3D"68" Name=3D"Medium Grid 2"/>
  <w:LsdException Locked=3D"false" Priority=3D"69" Name=3D"Medium Grid 3"/>
  <w:LsdException Locked=3D"false" Priority=3D"70" Name=3D"Dark List"/>
  <w:LsdException Locked=3D"false" Priority=3D"71" Name=3D"Colorful Shading=
"/>
  <w:LsdException Locked=3D"false" Priority=3D"72" Name=3D"Colorful List"/>
  <w:LsdException Locked=3D"false" Priority=3D"73" Name=3D"Colorful Grid"/>
  <w:LsdException Locked=3D"false" Priority=3D"60" Name=3D"Light Shading Ac=
cent 1"/>
  <w:LsdException Locked=3D"false" Priority=3D"61" Name=3D"Light List Accen=
t 1"/>
  <w:LsdException Locked=3D"false" Priority=3D"62" Name=3D"Light Grid Accen=
t 1"/>
  <w:LsdException Locked=3D"false" Priority=3D"63" Name=3D"Medium Shading 1=
 Accent 1"/>
  <w:LsdException Locked=3D"false" Priority=3D"64" Name=3D"Medium Shading 2=
 Accent 1"/>
  <w:LsdException Locked=3D"false" Priority=3D"65" Name=3D"Medium List 1 Ac=
cent 1"/>
  <w:LsdException Locked=3D"false" Priority=3D"99" SemiHidden=3D"true" Name=
=3D"Revision"/>
  <w:LsdException Locked=3D"false" Priority=3D"34" QFormat=3D"true"
   Name=3D"List Paragraph"/>
  <w:LsdException Locked=3D"false" Priority=3D"29" QFormat=3D"true" Name=3D=
"Quote"/>
  <w:LsdException Locked=3D"false" Priority=3D"30" QFormat=3D"true"
   Name=3D"Intense Quote"/>
  <w:LsdException Locked=3D"false" Priority=3D"66" Name=3D"Medium List 2 Ac=
cent 1"/>
  <w:LsdException Locked=3D"false" Priority=3D"67" Name=3D"Medium Grid 1 Ac=
cent 1"/>
  <w:LsdException Locked=3D"false" Priority=3D"68" Name=3D"Medium Grid 2 Ac=
cent 1"/>
  <w:LsdException Locked=3D"false" Priority=3D"69" Name=3D"Medium Grid 3 Ac=
cent 1"/>
  <w:LsdException Locked=3D"false" Priority=3D"70" Name=3D"Dark List Accent=
 1"/>
  <w:LsdException Locked=3D"false" Priority=3D"71" Name=3D"Colorful Shading=
 Accent 1"/>
  <w:LsdException Locked=3D"false" Priority=3D"72" Name=3D"Colorful List Ac=
cent 1"/>
  <w:LsdException Locked=3D"false" Priority=3D"73" Name=3D"Colorful Grid Ac=
cent 1"/>
  <w:LsdException Locked=3D"false" Priority=3D"60" Name=3D"Light Shading Ac=
cent 2"/>
  <w:LsdException Locked=3D"false" Priority=3D"61" Name=3D"Light List Accen=
t 2"/>
  <w:LsdException Locked=3D"false" Priority=3D"62" Name=3D"Light Grid Accen=
t 2"/>
  <w:LsdException Locked=3D"false" Priority=3D"63" Name=3D"Medium Shading 1=
 Accent 2"/>
  <w:LsdException Locked=3D"false" Priority=3D"64" Name=3D"Medium Shading 2=
 Accent 2"/>
  <w:LsdException Locked=3D"false" Priority=3D"65" Name=3D"Medium List 1 Ac=
cent 2"/>
  <w:LsdException Locked=3D"false" Priority=3D"66" Name=3D"Medium List 2 Ac=
cent 2"/>
  <w:LsdException Locked=3D"false" Priority=3D"67" Name=3D"Medium Grid 1 Ac=
cent 2"/>
  <w:LsdException Locked=3D"false" Priority=3D"68" Name=3D"Medium Grid 2 Ac=
cent 2"/>
  <w:LsdException Locked=3D"false" Priority=3D"69" Name=3D"Medium Grid 3 Ac=
cent 2"/>
  <w:LsdException Locked=3D"false" Priority=3D"70" Name=3D"Dark List Accent=
 2"/>
  <w:LsdException Locked=3D"false" Priority=3D"71" Name=3D"Colorful Shading=
 Accent 2"/>
  <w:LsdException Locked=3D"false" Priority=3D"72" Name=3D"Colorful List Ac=
cent 2"/>
  <w:LsdException Locked=3D"false" Priority=3D"73" Name=3D"Colorful Grid Ac=
cent 2"/>
  <w:LsdException Locked=3D"false" Priority=3D"60" Name=3D"Light Shading Ac=
cent 3"/>
  <w:LsdException Locked=3D"false" Priority=3D"61" Name=3D"Light List Accen=
t 3"/>
  <w:LsdException Locked=3D"false" Priority=3D"62" Name=3D"Light Grid Accen=
t 3"/>
  <w:LsdException Locked=3D"false" Priority=3D"63" Name=3D"Medium Shading 1=
 Accent 3"/>
  <w:LsdException Locked=3D"false" Priority=3D"64" Name=3D"Medium Shading 2=
 Accent 3"/>
  <w:LsdException Locked=3D"false" Priority=3D"65" Name=3D"Medium List 1 Ac=
cent 3"/>
  <w:LsdException Locked=3D"false" Priority=3D"66" Name=3D"Medium List 2 Ac=
cent 3"/>
  <w:LsdException Locked=3D"false" Priority=3D"67" Name=3D"Medium Grid 1 Ac=
cent 3"/>
  <w:LsdException Locked=3D"false" Priority=3D"68" Name=3D"Medium Grid 2 Ac=
cent 3"/>
  <w:LsdException Locked=3D"false" Priority=3D"69" Name=3D"Medium Grid 3 Ac=
cent 3"/>
  <w:LsdException Locked=3D"false" Priority=3D"70" Name=3D"Dark List Accent=
 3"/>
  <w:LsdException Locked=3D"false" Priority=3D"71" Name=3D"Colorful Shading=
 Accent 3"/>
  <w:LsdException Locked=3D"false" Priority=3D"72" Name=3D"Colorful List Ac=
cent 3"/>
  <w:LsdException Locked=3D"false" Priority=3D"73" Name=3D"Colorful Grid Ac=
cent 3"/>
  <w:LsdException Locked=3D"false" Priority=3D"60" Name=3D"Light Shading Ac=
cent 4"/>
  <w:LsdException Locked=3D"false" Priority=3D"61" Name=3D"Light List Accen=
t 4"/>
  <w:LsdException Locked=3D"false" Priority=3D"62" Name=3D"Light Grid Accen=
t 4"/>
  <w:LsdException Locked=3D"false" Priority=3D"63" Name=3D"Medium Shading 1=
 Accent 4"/>
  <w:LsdException Locked=3D"false" Priority=3D"64" Name=3D"Medium Shading 2=
 Accent 4"/>
  <w:LsdException Locked=3D"false" Priority=3D"65" Name=3D"Medium List 1 Ac=
cent 4"/>
  <w:LsdException Locked=3D"false" Priority=3D"66" Name=3D"Medium List 2 Ac=
cent 4"/>
  <w:LsdException Locked=3D"false" Priority=3D"67" Name=3D"Medium Grid 1 Ac=
cent 4"/>
  <w:LsdException Locked=3D"false" Priority=3D"68" Name=3D"Medium Grid 2 Ac=
cent 4"/>
  <w:LsdException Locked=3D"false" Priority=3D"69" Name=3D"Medium Grid 3 Ac=
cent 4"/>
  <w:LsdException Locked=3D"false" Priority=3D"70" Name=3D"Dark List Accent=
 4"/>
  <w:LsdException Locked=3D"false" Priority=3D"71" Name=3D"Colorful Shading=
 Accent 4"/>
  <w:LsdException Locked=3D"false" Priority=3D"72" Name=3D"Colorful List Ac=
cent 4"/>
  <w:LsdException Locked=3D"false" Priority=3D"73" Name=3D"Colorful Grid Ac=
cent 4"/>
  <w:LsdException Locked=3D"false" Priority=3D"60" Name=3D"Light Shading Ac=
cent 5"/>
  <w:LsdException Locked=3D"false" Priority=3D"61" Name=3D"Light List Accen=
t 5"/>
  <w:LsdException Locked=3D"false" Priority=3D"62" Name=3D"Light Grid Accen=
t 5"/>
  <w:LsdException Locked=3D"false" Priority=3D"63" Name=3D"Medium Shading 1=
 Accent 5"/>
  <w:LsdException Locked=3D"false" Priority=3D"64" Name=3D"Medium Shading 2=
 Accent 5"/>
  <w:LsdException Locked=3D"false" Priority=3D"65" Name=3D"Medium List 1 Ac=
cent 5"/>
  <w:LsdException Locked=3D"false" Priority=3D"66" Name=3D"Medium List 2 Ac=
cent 5"/>
  <w:LsdException Locked=3D"false" Priority=3D"67" Name=3D"Medium Grid 1 Ac=
cent 5"/>
  <w:LsdException Locked=3D"false" Priority=3D"68" Name=3D"Medium Grid 2 Ac=
cent 5"/>
  <w:LsdException Locked=3D"false" Priority=3D"69" Name=3D"Medium Grid 3 Ac=
cent 5"/>
  <w:LsdException Locked=3D"false" Priority=3D"70" Name=3D"Dark List Accent=
 5"/>
  <w:LsdException Locked=3D"false" Priority=3D"71" Name=3D"Colorful Shading=
 Accent 5"/>
  <w:LsdException Locked=3D"false" Priority=3D"72" Name=3D"Colorful List Ac=
cent 5"/>
  <w:LsdException Locked=3D"false" Priority=3D"73" Name=3D"Colorful Grid Ac=
cent 5"/>
  <w:LsdException Locked=3D"false" Priority=3D"60" Name=3D"Light Shading Ac=
cent 6"/>
  <w:LsdException Locked=3D"false" Priority=3D"61" Name=3D"Light List Accen=
t 6"/>
  <w:LsdException Locked=3D"false" Priority=3D"62" Name=3D"Light Grid Accen=
t 6"/>
  <w:LsdException Locked=3D"false" Priority=3D"63" Name=3D"Medium Shading 1=
 Accent 6"/>
  <w:LsdException Locked=3D"false" Priority=3D"64" Name=3D"Medium Shading 2=
 Accent 6"/>
  <w:LsdException Locked=3D"false" Priority=3D"65" Name=3D"Medium List 1 Ac=
cent 6"/>
  <w:LsdException Locked=3D"false" Priority=3D"66" Name=3D"Medium List 2 Ac=
cent 6"/>
  <w:LsdException Locked=3D"false" Priority=3D"67" Name=3D"Medium Grid 1 Ac=
cent 6"/>
  <w:LsdException Locked=3D"false" Priority=3D"68" Name=3D"Medium Grid 2 Ac=
cent 6"/>
  <w:LsdException Locked=3D"false" Priority=3D"69" Name=3D"Medium Grid 3 Ac=
cent 6"/>
  <w:LsdException Locked=3D"false" Priority=3D"70" Name=3D"Dark List Accent=
 6"/>
  <w:LsdException Locked=3D"false" Priority=3D"71" Name=3D"Colorful Shading=
 Accent 6"/>
  <w:LsdException Locked=3D"false" Priority=3D"72" Name=3D"Colorful List Ac=
cent 6"/>
  <w:LsdException Locked=3D"false" Priority=3D"73" Name=3D"Colorful Grid Ac=
cent 6"/>
  <w:LsdException Locked=3D"false" Priority=3D"19" QFormat=3D"true"
   Name=3D"Subtle Emphasis"/>
  <w:LsdException Locked=3D"false" Priority=3D"21" QFormat=3D"true"
   Name=3D"Intense Emphasis"/>
  <w:LsdException Locked=3D"false" Priority=3D"31" QFormat=3D"true"
   Name=3D"Subtle Reference"/>
  <w:LsdException Locked=3D"false" Priority=3D"32" QFormat=3D"true"
   Name=3D"Intense Reference"/>
  <w:LsdException Locked=3D"false" Priority=3D"33" QFormat=3D"true" Name=3D=
"Book Title"/>
  <w:LsdException Locked=3D"false" Priority=3D"37" SemiHidden=3D"true"
   UnhideWhenUsed=3D"true" Name=3D"Bibliography"/>
  <w:LsdException Locked=3D"false" Priority=3D"39" SemiHidden=3D"true"
   UnhideWhenUsed=3D"true" QFormat=3D"true" Name=3D"TOC Heading"/>
 </w:LatentStyles>
</xml><![endif]-->
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:Helvetica;
	panose-1:2 11 6 4 2 2 2 2 2 4;
	mso-font-charset:0;
	mso-generic-font-family:swiss;
	mso-font-pitch:variable;
	mso-font-signature:-536858881 -1073711037 9 0 511 0;}
@font-face
	{font-family:Courier;
	panose-1:2 7 4 9 2 2 5 2 4 4;
	mso-font-alt:"Courier New";
	mso-font-charset:0;
	mso-generic-font-family:modern;
	mso-font-format:other;
	mso-font-pitch:fixed;
	mso-font-signature:3 0 0 0 1 0;}
@font-face
	{font-family:Wingdings;
	panose-1:5 0 0 0 0 0 0 0 0 0;
	mso-font-charset:2;
	mso-generic-font-family:auto;
	mso-font-pitch:variable;
	mso-font-signature:0 268435456 0 0 -2147483648 0;}
@font-face
	{font-family:SimSun;
	panose-1:2 1 6 0 3 1 1 1 1 1;
	mso-font-alt:\5B8B\4F53;
	mso-font-charset:134;
	mso-generic-font-family:auto;
	mso-font-pitch:variable;
	mso-font-signature:3 680460288 22 0 262145 0;}
@font-face
	{font-family:PMingLiU;
	panose-1:2 1 6 1 0 1 1 1 1 1;
	mso-font-alt:"Microsoft JhengHei";
	mso-font-charset:136;
	mso-generic-font-family:auto;
	mso-font-format:other;
	mso-font-pitch:variable;
	mso-font-signature:0 134742016 16 0 1048576 0;}
@font-face
	{font-family:Dotum;
	panose-1:2 11 6 0 0 1 1 1 1 1;
	mso-font-alt:\B3CB\C6C0;
	mso-font-charset:129;
	mso-generic-font-family:modern;
	mso-font-format:other;
	mso-font-pitch:fixed;
	mso-font-signature:1 151388160 16 0 524288 0;}
@font-face
	{font-family:Dotum;
	panose-1:2 11 6 0 0 1 1 1 1 1;
	mso-font-alt:\B3CB\C6C0;
	mso-font-charset:129;
	mso-generic-font-family:modern;
	mso-font-format:other;
	mso-font-pitch:fixed;
	mso-font-signature:1 151388160 16 0 524288 0;}
@font-face
	{font-family:Tahoma;
	panose-1:2 11 6 4 3 5 4 4 2 4;
	mso-font-charset:0;
	mso-generic-font-family:swiss;
	mso-font-pitch:variable;
	mso-font-signature:-520081665 -1073717157 41 0 66047 0;}
@font-face
	{font-family:"Lucida Handwriting";
	mso-font-alt:Mistral;
	mso-font-charset:0;
	mso-generic-font-family:script;
	mso-font-pitch:variable;
	mso-font-signature:3 0 0 0 1 0;}
@font-face
	{font-family:Verdana;
	panose-1:2 11 6 4 3 5 4 4 2 4;
	mso-font-charset:0;
	mso-generic-font-family:swiss;
	mso-font-pitch:variable;
	mso-font-signature:-1593833729 1073750107 16 0 415 0;}
@font-face
	{font-family:Bliss-Heavy;
	panose-1:0 0 0 0 0 0 0 0 0 0;
	mso-font-alt:"Times New Roman";
	mso-font-charset:0;
	mso-generic-font-family:auto;
	mso-font-format:other;
	mso-font-pitch:auto;
	mso-font-signature:3 0 0 0 1 0;}
@font-face
	{font-family:"Rage Italic";
	mso-font-charset:0;
	mso-generic-font-family:script;
	mso-font-pitch:variable;
	mso-font-signature:3 0 0 0 1 0;}
@font-face
	{font-family:"Book Antiqua";
	panose-1:2 4 6 2 5 3 5 3 3 4;
	mso-font-charset:0;
	mso-generic-font-family:roman;
	mso-font-pitch:variable;
	mso-font-signature:647 0 0 0 159 0;}
@font-face
	{font-family:"Arial Narrow";
	panose-1:2 11 6 6 2 2 2 3 2 4;
	mso-font-charset:0;
	mso-generic-font-family:swiss;
	mso-font-pitch:variable;
	mso-font-signature:647 2048 0 0 159 0;}
@font-face
	{font-family:"\@SimSun";
	panose-1:2 1 6 0 3 1 1 1 1 1;
	mso-font-charset:134;
	mso-generic-font-family:auto;
	mso-font-pitch:variable;
	mso-font-signature:3 680460288 22 0 262145 0;}
@font-face
	{font-family:"\@PMingLiU";
	panose-1:0 0 0 0 0 0 0 0 0 0;
	mso-font-charset:136;
	mso-generic-font-family:auto;
	mso-font-format:other;
	mso-font-pitch:variable;
	mso-font-signature:0 134742016 16 0 1048576 0;}
@font-face
	{font-family:"\@Dotum";
	panose-1:0 0 0 0 0 0 0 0 0 0;
	mso-font-charset:129;
	mso-generic-font-family:modern;
	mso-font-format:other;
	mso-font-pitch:fixed;
	mso-font-signature:1 151388160 16 0 524288 0;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-parent:"";
	margin:0in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";
	mso-fareast-font-family:"Times New Roman";
	mso-ansi-language:EN-GB;}
h1
	{mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-link:"Heading 1 Char";
	mso-style-next:Normal;
	margin-top:12.0pt;
	margin-right:0in;
	margin-bottom:3.0pt;
	margin-left:0in;
	mso-pagination:widow-orphan;
	page-break-after:avoid;
	mso-outline-level:1;
	font-size:16.0pt;
	font-family:"Arial","sans-serif";
	mso-fareast-font-family:"Times New Roman";
	mso-font-kerning:16.0pt;
	mso-ansi-language:EN-GB;}
h2
	{mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-link:"Heading 2 Char";
	mso-style-next:Normal;
	margin-top:6.0pt;
	margin-right:0in;
	margin-bottom:6.0pt;
	margin-left:0in;
	text-align:center;
	mso-pagination:widow-orphan;
	page-break-after:avoid;
	mso-outline-level:2;
	font-size:8.0pt;
	mso-bidi-font-size:12.0pt;
	font-family:"Helvetica","sans-serif";
	mso-fareast-font-family:"Times New Roman";
	mso-ansi-language:EN-GB;
	mso-bidi-font-weight:normal;}
h3
	{mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-link:"Heading 3 Char";
	mso-style-next:Normal;
	margin-top:12.0pt;
	margin-right:0in;
	margin-bottom:3.0pt;
	margin-left:0in;
	mso-pagination:widow-orphan;
	page-break-after:avoid;
	mso-outline-level:3;
	font-size:13.0pt;
	font-family:"Arial","sans-serif";
	mso-fareast-font-family:"Times New Roman";
	mso-ansi-language:EN-GB;}
h4
	{mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-link:"Heading 4 Char";
	mso-style-next:Normal;
	margin:0in;
	margin-bottom:.0001pt;
	text-align:center;
	mso-pagination:widow-orphan;
	page-break-after:avoid;
	mso-outline-level:4;
	font-size:14.0pt;
	mso-bidi-font-size:10.0pt;
	font-family:"Helvetica","sans-serif";
	mso-fareast-font-family:"Times New Roman";
	mso-ansi-language:EN-GB;}
h5
	{mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-link:"Heading 5 Char";
	mso-style-next:Normal;
	margin:0in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	page-break-after:avoid;
	mso-outline-level:5;
	font-size:10.0pt;
	font-family:"Helvetica","sans-serif";
	mso-fareast-font-family:"Times New Roman";
	mso-ansi-language:EN-GB;
	mso-bidi-font-weight:normal;}
h6
	{mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-link:"Heading 6 Char";
	mso-style-next:Normal;
	margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:10.0pt;
	margin-bottom:.0001pt;
	mso-para-margin-top:0in;
	mso-para-margin-right:0in;
	mso-para-margin-bottom:0in;
	mso-para-margin-left:2.0gd;
	mso-para-margin-bottom:.0001pt;
	line-height:300%;
	mso-pagination:widow-orphan;
	page-break-after:avoid;
	mso-outline-level:6;
	font-size:18.0pt;
	font-family:"Arial","sans-serif";
	mso-fareast-font-family:PMingLiU;
	mso-bidi-font-family:"Times New Roman";
	mso-ansi-language:EN-GB;
	font-weight:normal;}
p.MsoHeading9, li.MsoHeading9, div.MsoHeading9
	{mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-link:"Heading 9 Char";
	mso-style-next:Normal;
	margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:20.0pt;
	margin-bottom:.0001pt;
	mso-para-margin-top:0in;
	mso-para-margin-right:0in;
	mso-para-margin-bottom:0in;
	mso-para-margin-left:4.0gd;
	mso-para-margin-bottom:.0001pt;
	line-height:300%;
	mso-pagination:widow-orphan;
	page-break-after:avoid;
	mso-outline-level:9;
	font-size:18.0pt;
	font-family:"Arial","sans-serif";
	mso-fareast-font-family:PMingLiU;
	mso-bidi-font-family:"Times New Roman";
	mso-ansi-language:EN-GB;}
p.MsoHeader, li.MsoHeader, div.MsoHeader
	{mso-style-unhide:no;
	mso-style-link:"Header Char";
	margin:0in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	tab-stops:center 3.0in right 6.0in;
	font-size:10.0pt;
	font-family:"Times New Roman","serif";
	mso-fareast-font-family:"Times New Roman";
	mso-ansi-language:EN-GB;}
p.MsoFooter, li.MsoFooter, div.MsoFooter
	{mso-style-priority:99;
	mso-style-unhide:no;
	mso-style-link:"Footer Char";
	margin:0in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	tab-stops:center 3.0in right 6.0in;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";
	mso-fareast-font-family:"Times New Roman";
	mso-ansi-language:EN-GB;}
p.MsoBodyText, li.MsoBodyText, div.MsoBodyText
	{mso-style-unhide:no;
	mso-style-link:"Body Text Char";
	margin:0in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	mso-bidi-font-size:12.0pt;
	font-family:"Arial","sans-serif";
	mso-fareast-font-family:"Times New Roman";
	mso-ansi-language:EN-GB;}
p.MsoBodyText2, li.MsoBodyText2, div.MsoBodyText2
	{mso-style-unhide:no;
	mso-style-link:"Body Text 2 Char";
	margin-top:0in;
	margin-right:0in;
	margin-bottom:6.0pt;
	margin-left:0in;
	line-height:200%;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";
	mso-fareast-font-family:"Times New Roman";
	mso-ansi-language:EN-GB;}
a:link, span.MsoHyperlink
	{mso-style-unhide:no;
	color:blue;
	text-decoration:underline;
	text-underline:single;}
a:visited, span.MsoHyperlinkFollowed
	{mso-style-unhide:no;
	color:purple;
	mso-themecolor:followedhyperlink;
	text-decoration:underline;
	text-underline:single;}
p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
	{mso-style-noshow:yes;
	mso-style-unhide:no;
	mso-style-link:"Balloon Text Char";
	margin:0in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:8.0pt;
	font-family:"Tahoma","sans-serif";
	mso-fareast-font-family:"Times New Roman";
	mso-ansi-language:EN-GB;}
p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph
	{mso-style-priority:34;
	mso-style-unhide:no;
	mso-style-qformat:yes;
	margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:.5in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";
	mso-fareast-font-family:"Times New Roman";
	mso-ansi-language:EN-GB;}
span.Heading1Char
	{mso-style-name:"Heading 1 Char";
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:"Heading 1";
	mso-ansi-font-size:16.0pt;
	mso-bidi-font-size:16.0pt;
	font-family:"Arial","sans-serif";
	mso-ascii-font-family:Arial;
	mso-hansi-font-family:Arial;
	mso-bidi-font-family:Arial;
	mso-font-kerning:16.0pt;
	mso-ansi-language:EN-GB;
	mso-fareast-language:EN-US;
	mso-bidi-language:AR-SA;
	font-weight:bold;}
span.Heading2Char
	{mso-style-name:"Heading 2 Char";
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:"Heading 2";
	mso-ansi-font-size:13.0pt;
	mso-bidi-font-size:13.0pt;
	font-family:"Cambria","serif";
	mso-ascii-font-family:Cambria;
	mso-ascii-theme-font:major-latin;
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:major-fareast;
	mso-hansi-font-family:Cambria;
	mso-hansi-theme-font:major-latin;
	mso-bidi-font-family:"Times New Roman";
	mso-bidi-theme-font:major-bidi;
	color:#4F81BD;
	mso-themecolor:accent1;
	mso-ansi-language:EN-GB;
	font-weight:bold;}
span.Heading3Char
	{mso-style-name:"Heading 3 Char";
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:"Heading 3";
	mso-ansi-font-size:13.0pt;
	mso-bidi-font-size:13.0pt;
	font-family:"Arial","sans-serif";
	mso-ascii-font-family:Arial;
	mso-hansi-font-family:Arial;
	mso-bidi-font-family:Arial;
	mso-ansi-language:EN-GB;
	mso-fareast-language:EN-US;
	mso-bidi-language:AR-SA;
	font-weight:bold;}
span.Heading4Char
	{mso-style-name:"Heading 4 Char";
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:"Heading 4";
	mso-ansi-font-size:12.0pt;
	mso-bidi-font-size:12.0pt;
	font-family:"Cambria","serif";
	mso-ascii-font-family:Cambria;
	mso-ascii-theme-font:major-latin;
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:major-fareast;
	mso-hansi-font-family:Cambria;
	mso-hansi-theme-font:major-latin;
	mso-bidi-font-family:"Times New Roman";
	mso-bidi-theme-font:major-bidi;
	color:#4F81BD;
	mso-themecolor:accent1;
	mso-ansi-language:EN-GB;
	font-weight:bold;
	font-style:italic;}
span.Heading5Char
	{mso-style-name:"Heading 5 Char";
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:"Heading 5";
	mso-ansi-font-size:12.0pt;
	mso-bidi-font-size:12.0pt;
	font-family:"Cambria","serif";
	mso-ascii-font-family:Cambria;
	mso-ascii-theme-font:major-latin;
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:major-fareast;
	mso-hansi-font-family:Cambria;
	mso-hansi-theme-font:major-latin;
	mso-bidi-font-family:"Times New Roman";
	mso-bidi-theme-font:major-bidi;
	color:#243F60;
	mso-themecolor:accent1;
	mso-themeshade:127;
	mso-ansi-language:EN-GB;}
span.Heading6Char
	{mso-style-name:"Heading 6 Char";
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:"Heading 6";
	mso-ansi-font-size:12.0pt;
	mso-bidi-font-size:12.0pt;
	font-family:"Cambria","serif";
	mso-ascii-font-family:Cambria;
	mso-ascii-theme-font:major-latin;
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:major-fareast;
	mso-hansi-font-family:Cambria;
	mso-hansi-theme-font:major-latin;
	mso-bidi-font-family:"Times New Roman";
	mso-bidi-theme-font:major-bidi;
	color:#243F60;
	mso-themecolor:accent1;
	mso-themeshade:127;
	mso-ansi-language:EN-GB;
	font-style:italic;}
span.Heading9Char
	{mso-style-name:"Heading 9 Char";
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:"Heading 9";
	font-family:"Cambria","serif";
	mso-ascii-font-family:Cambria;
	mso-ascii-theme-font:major-latin;
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:major-fareast;
	mso-hansi-font-family:Cambria;
	mso-hansi-theme-font:major-latin;
	mso-bidi-font-family:"Times New Roman";
	mso-bidi-theme-font:major-bidi;
	color:#404040;
	mso-themecolor:text1;
	mso-themetint:191;
	mso-ansi-language:EN-GB;
	font-style:italic;}
span.HeaderChar
	{mso-style-name:"Header Char";
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:Header;
	mso-ansi-font-size:12.0pt;
	mso-bidi-font-size:12.0pt;
	font-family:"Times New Roman","serif";
	mso-ascii-font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";
	mso-hansi-font-family:"Times New Roman";
	mso-bidi-font-family:"Times New Roman";
	mso-ansi-language:EN-GB;}
span.FooterChar
	{mso-style-name:"Footer Char";
	mso-style-priority:99;
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:Footer;
	mso-ansi-font-size:12.0pt;
	mso-bidi-font-size:12.0pt;
	font-family:"Times New Roman","serif";
	mso-ascii-font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";
	mso-hansi-font-family:"Times New Roman";
	mso-bidi-font-family:"Times New Roman";
	mso-ansi-language:EN-GB;}
span.BodyTextChar
	{mso-style-name:"Body Text Char";
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:"Body Text";
	mso-ansi-font-size:12.0pt;
	mso-bidi-font-size:12.0pt;
	font-family:"Times New Roman","serif";
	mso-ascii-font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";
	mso-hansi-font-family:"Times New Roman";
	mso-bidi-font-family:"Times New Roman";
	mso-ansi-language:EN-GB;}
span.BodyText2Char
	{mso-style-name:"Body Text 2 Char";
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:"Body Text 2";
	mso-ansi-font-size:12.0pt;
	mso-bidi-font-size:12.0pt;
	font-family:"Times New Roman","serif";
	mso-ascii-font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";
	mso-hansi-font-family:"Times New Roman";
	mso-bidi-font-family:"Times New Roman";
	mso-ansi-language:EN-GB;}
span.BalloonTextChar
	{mso-style-name:"Balloon Text Char";
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:"Balloon Text";
	mso-ansi-font-size:8.0pt;
	mso-bidi-font-size:8.0pt;
	font-family:"Tahoma","sans-serif";
	mso-ascii-font-family:Tahoma;
	mso-fareast-font-family:"Times New Roman";
	mso-hansi-font-family:Tahoma;
	mso-bidi-font-family:Tahoma;
	mso-ansi-language:EN-GB;}
p.xl25, li.xl25, div.xl25
	{mso-style-name:xl25;
	mso-style-unhide:no;
	mso-margin-top-alt:auto;
	margin-right:0in;
	mso-margin-bottom-alt:auto;
	margin-left:0in;
	text-align:right;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";
	mso-fareast-font-family:PMingLiU;
	mso-fareast-language:ZH-TW;}
p.xl26, li.xl26, div.xl26
	{mso-style-name:xl26;
	mso-style-unhide:no;
	mso-margin-top-alt:auto;
	margin-right:0in;
	mso-margin-bottom-alt:auto;
	margin-left:0in;
	text-align:center;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";
	mso-fareast-font-family:PMingLiU;
	mso-fareast-language:ZH-TW;
	text-decoration:underline;
	text-underline:single;}
p.xl29, li.xl29, div.xl29
	{mso-style-name:xl29;
	mso-style-unhide:no;
	mso-margin-top-alt:auto;
	margin-right:0in;
	mso-margin-bottom-alt:auto;
	margin-left:0in;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";
	mso-fareast-font-family:PMingLiU;
	mso-fareast-language:ZH-TW;
	text-decoration:underline;
	text-underline:single;}
p.xl42, li.xl42, div.xl42
	{mso-style-name:xl42;
	mso-style-unhide:no;
	mso-margin-top-alt:auto;
	margin-right:0in;
	mso-margin-bottom-alt:auto;
	margin-left:0in;
	mso-pagination:widow-orphan;
	border:none;
	mso-border-bottom-alt:solid windowtext .5pt;
	padding:0in;
	mso-padding-alt:0in 0in 0in 0in;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";
	mso-fareast-font-family:PMingLiU;
	mso-fareast-language:ZH-TW;
	font-weight:bold;}
span.postbody1
	{mso-style-name:postbody1;
	mso-style-unhide:no;
	mso-ansi-font-size:7.0pt;
	mso-bidi-font-size:7.0pt;}
span.SpellE
	{mso-style-name:"";
	mso-spl-e:yes;}
span.GramE
	{mso-style-name:"";
	mso-gram-e:yes;}
.MsoChpDefault
	{mso-style-type:export-only;
	mso-default-props:yes;
	font-size:10.0pt;
	mso-ansi-font-size:10.0pt;
	mso-bidi-font-size:10.0pt;
	mso-fareast-font-family:SimSun;}
 /* Page Definitions */
 @page
	{mso-footnote-separator:url("master_file_files/header.htm") fs;
	mso-footnote-continuation-separator:url("master_file_files/header.htm") fc=
s;
	mso-endnote-separator:url("master_file_files/header.htm") es;
	mso-endnote-continuation-separator:url("master_file_files/header.htm") ecs=
;}
@page WordSection1
	{size:8.5in 11.0in;
	margin:.2in .6in 17.3pt .8in;
	mso-header-margin:23.05pt;
	mso-footer-margin:.2in;
	mso-page-numbers:1;
	mso-footer:url("master_file_files/header.htm") f1;
	mso-paper-source:0;}
div.WordSection1
	{page:WordSection1;}
 /* List Definitions */
 @list l0
	{mso-list-id:208954340;
	mso-list-type:hybrid;
	mso-list-template-ids:-412209562 997082742 67698691 67698693 67698689 6769=
8691 67698693 67698689 67698691 67698693;}
@list l0:level1
	{mso-level-start-at:3;
	mso-level-number-format:bullet;
	mso-level-text:\F0A8;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;
	mso-ansi-font-size:14.0pt;
	font-family:Wingdings;
	mso-fareast-font-family:"Times New Roman";
	mso-bidi-font-family:Arial;}
@list l0:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Courier New";}
@list l0:level3
	{mso-level-number-format:bullet;
	mso-level-text:\F0A7;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Wingdings;}
@list l0:level4
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Symbol;}
@list l0:level5
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Courier New";}
@list l0:level6
	{mso-level-number-format:bullet;
	mso-level-text:\F0A7;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Wingdings;}
@list l0:level7
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Symbol;}
@list l0:level8
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Courier New";}
@list l0:level9
	{mso-level-number-format:bullet;
	mso-level-text:\F0A7;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Wingdings;}
@list l1
	{mso-list-id:602031962;
	mso-list-type:hybrid;
	mso-list-template-ids:18754490 997082742 67698691 67698693 67698689 676986=
91 67698693 67698689 67698691 67698693;}
@list l1:level1
	{mso-level-start-at:3;
	mso-level-number-format:bullet;
	mso-level-text:\F0A8;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;
	mso-ansi-font-size:14.0pt;
	font-family:Wingdings;
	mso-fareast-font-family:"Times New Roman";
	mso-bidi-font-family:Arial;}
@list l1:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Courier New";}
@list l1:level3
	{mso-level-number-format:bullet;
	mso-level-text:\F0A7;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Wingdings;}
@list l1:level4
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Symbol;}
@list l1:level5
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Courier New";}
@list l1:level6
	{mso-level-number-format:bullet;
	mso-level-text:\F0A7;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Wingdings;}
@list l1:level7
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Symbol;}
@list l1:level8
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Courier New";}
@list l1:level9
	{mso-level-number-format:bullet;
	mso-level-text:\F0A7;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Wingdings;}
@list l2
	{mso-list-id:739836857;
	mso-list-type:hybrid;
	mso-list-template-ids:-914074742 510426240 67698691 67698693 67698689 6769=
8691 67698693 67698689 67698691 67698693;}
@list l2:level1
	{mso-level-start-at:4;
	mso-level-number-format:bullet;
	mso-level-text:\F0A8;
	mso-level-tab-stop:57.75pt;
	mso-level-number-position:left;
	margin-left:57.75pt;
	text-indent:-21.75pt;
	mso-ansi-font-size:14.0pt;
	font-family:Wingdings;
	mso-fareast-font-family:"Times New Roman";
	mso-bidi-font-family:Arial;}
@list l2:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:1.25in;
	mso-level-number-position:left;
	margin-left:1.25in;
	text-indent:-.25in;
	font-family:"Courier New";}
@list l2:level3
	{mso-level-number-format:bullet;
	mso-level-text:\F0A7;
	mso-level-tab-stop:1.75in;
	mso-level-number-position:left;
	margin-left:1.75in;
	text-indent:-.25in;
	font-family:Wingdings;}
@list l2:level4
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:2.25in;
	mso-level-number-position:left;
	margin-left:2.25in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l2:level5
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:2.75in;
	mso-level-number-position:left;
	margin-left:2.75in;
	text-indent:-.25in;
	font-family:"Courier New";}
@list l2:level6
	{mso-level-number-format:bullet;
	mso-level-text:\F0A7;
	mso-level-tab-stop:3.25in;
	mso-level-number-position:left;
	margin-left:3.25in;
	text-indent:-.25in;
	font-family:Wingdings;}
@list l2:level7
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:3.75in;
	mso-level-number-position:left;
	margin-left:3.75in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l2:level8
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:4.25in;
	mso-level-number-position:left;
	margin-left:4.25in;
	text-indent:-.25in;
	font-family:"Courier New";}
@list l2:level9
	{mso-level-number-format:bullet;
	mso-level-text:\F0A7;
	mso-level-tab-stop:4.75in;
	mso-level-number-position:left;
	margin-left:4.75in;
	text-indent:-.25in;
	font-family:Wingdings;}
@list l3
	{mso-list-id:987324632;
	mso-list-type:hybrid;
	mso-list-template-ids:768217776 997082742 67698691 67698693 67698689 67698=
691 67698693 67698689 67698691 67698693;}
@list l3:level1
	{mso-level-start-at:3;
	mso-level-number-format:bullet;
	mso-level-text:\F0A8;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:1.0in;
	text-indent:-.25in;
	mso-ansi-font-size:14.0pt;
	font-family:Wingdings;
	mso-fareast-font-family:"Times New Roman";
	mso-bidi-font-family:Arial;}
@list l3:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:1.5in;
	text-indent:-.25in;
	font-family:"Courier New";}
@list l3:level3
	{mso-level-number-format:bullet;
	mso-level-text:\F0A7;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:2.0in;
	text-indent:-.25in;
	font-family:Wingdings;}
@list l3:level4
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:2.5in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l3:level5
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:3.0in;
	text-indent:-.25in;
	font-family:"Courier New";}
@list l3:level6
	{mso-level-number-format:bullet;
	mso-level-text:\F0A7;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:3.5in;
	text-indent:-.25in;
	font-family:Wingdings;}
@list l3:level7
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:4.0in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l3:level8
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:4.5in;
	text-indent:-.25in;
	font-family:"Courier New";}
@list l3:level9
	{mso-level-number-format:bullet;
	mso-level-text:\F0A7;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:5.0in;
	text-indent:-.25in;
	font-family:Wingdings;}
@list l4
	{mso-list-id:1739941616;
	mso-list-type:hybrid;
	mso-list-template-ids:1746074920 67698701 67698691 67698693 67698689 67698=
691 67698693 67698689 67698691 67698693;}
@list l4:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B2;
	mso-level-tab-stop:24.0pt;
	mso-level-number-position:left;
	margin-left:24.0pt;
	text-indent:-24.0pt;
	font-family:Wingdings;}
@list l4:level2
	{mso-level-number-format:bullet;
	mso-level-text:\F06E;
	mso-level-tab-stop:24.0pt;
	mso-level-number-position:left;
	margin-left:24.0pt;
	text-indent:-24.0pt;
	font-family:Wingdings;}
@list l4:level3
	{mso-level-number-format:bullet;
	mso-level-text:\F075;
	mso-level-tab-stop:48.0pt;
	mso-level-number-position:left;
	margin-left:48.0pt;
	text-indent:-24.0pt;
	font-family:Wingdings;}
@list l4:level4
	{mso-level-number-format:bullet;
	mso-level-text:\F06C;
	mso-level-tab-stop:1.0in;
	mso-level-number-position:left;
	margin-left:1.0in;
	text-indent:-24.0pt;
	font-family:Wingdings;}
@list l4:level5
	{mso-level-number-format:bullet;
	mso-level-text:\F06E;
	mso-level-tab-stop:96.0pt;
	mso-level-number-position:left;
	margin-left:96.0pt;
	text-indent:-24.0pt;
	font-family:Wingdings;}
@list l4:level6
	{mso-level-number-format:bullet;
	mso-level-text:\F075;
	mso-level-tab-stop:120.0pt;
	mso-level-number-position:left;
	margin-left:120.0pt;
	text-indent:-24.0pt;
	font-family:Wingdings;}
@list l4:level7
	{mso-level-number-format:bullet;
	mso-level-text:\F06C;
	mso-level-tab-stop:2.0in;
	mso-level-number-position:left;
	margin-left:2.0in;
	text-indent:-24.0pt;
	font-family:Wingdings;}
@list l4:level8
	{mso-level-number-format:bullet;
	mso-level-text:\F06E;
	mso-level-tab-stop:168.0pt;
	mso-level-number-position:left;
	margin-left:168.0pt;
	text-indent:-24.0pt;
	font-family:Wingdings;}
@list l4:level9
	{mso-level-number-format:bullet;
	mso-level-text:\F075;
	mso-level-tab-stop:192.0pt;
	mso-level-number-position:left;
	margin-left:192.0pt;
	text-indent:-24.0pt;
	font-family:Wingdings;}
@list l5
	{mso-list-id:1819764236;
	mso-list-type:hybrid;
	mso-list-template-ids:1739997230 997082742 67698691 67698693 67698689 6769=
8691 67698693 67698689 67698691 67698693;}
@list l5:level1
	{mso-level-start-at:3;
	mso-level-number-format:bullet;
	mso-level-text:\F0A8;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:1.0in;
	text-indent:-.25in;
	mso-ansi-font-size:14.0pt;
	font-family:Wingdings;
	mso-fareast-font-family:"Times New Roman";
	mso-bidi-font-family:Arial;}
@list l5:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:1.5in;
	text-indent:-.25in;
	font-family:"Courier New";}
@list l5:level3
	{mso-level-number-format:bullet;
	mso-level-text:\F0A7;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:2.0in;
	text-indent:-.25in;
	font-family:Wingdings;}
@list l5:level4
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:2.5in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l5:level5
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:3.0in;
	text-indent:-.25in;
	font-family:"Courier New";}
@list l5:level6
	{mso-level-number-format:bullet;
	mso-level-text:\F0A7;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:3.5in;
	text-indent:-.25in;
	font-family:Wingdings;}
@list l5:level7
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:4.0in;
	text-indent:-.25in;
	font-family:Symbol;}
@list l5:level8
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:4.5in;
	text-indent:-.25in;
	font-family:"Courier New";}
@list l5:level9
	{mso-level-number-format:bullet;
	mso-level-text:\F0A7;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:5.0in;
	text-indent:-.25in;
	font-family:Wingdings;}
@list l6
	{mso-list-id:2117673719;
	mso-list-type:hybrid;
	mso-list-template-ids:657902390 997082742 67698691 67698693 67698689 67698=
691 67698693 67698689 67698691 67698693;}
@list l6:level1
	{mso-level-start-at:3;
	mso-level-number-format:bullet;
	mso-level-text:\F0A8;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;
	mso-ansi-font-size:14.0pt;
	font-family:Wingdings;
	mso-fareast-font-family:"Times New Roman";
	mso-bidi-font-family:Arial;}
@list l6:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Courier New";}
@list l6:level3
	{mso-level-number-format:bullet;
	mso-level-text:\F0A7;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Wingdings;}
@list l6:level4
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Symbol;}
@list l6:level5
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Courier New";}
@list l6:level6
	{mso-level-number-format:bullet;
	mso-level-text:\F0A7;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Wingdings;}
@list l6:level7
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Symbol;}
@list l6:level8
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:"Courier New";}
@list l6:level9
	{mso-level-number-format:bullet;
	mso-level-text:\F0A7;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Wingdings;}
ol
	{margin-bottom:0in;}
ul
	{margin-bottom:0in;}
-->
</style>
<!--[if gte mso 10]>
<style>
 /* Style Definitions */
 table.MsoNormalTable
	{mso-style-name:"Table Normal";
	mso-tstyle-rowband-size:0;
	mso-tstyle-colband-size:0;
	mso-style-noshow:yes;
	mso-style-priority:99;
	mso-style-parent:"";
	mso-padding-alt:0in 5.4pt 0in 5.4pt;
	mso-para-margin:0in;
	mso-para-margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	font-family:"Times New Roman","serif";}
table.MsoTableGrid
	{mso-style-name:"Table Grid";
	mso-tstyle-rowband-size:0;
	mso-tstyle-colband-size:0;
	mso-style-unhide:no;
	border:solid windowtext 1.0pt;
	mso-border-alt:solid windowtext .5pt;
	mso-padding-alt:0in 5.4pt 0in 5.4pt;
	mso-border-insideh:.5pt solid windowtext;
	mso-border-insidev:.5pt solid windowtext;
	mso-para-margin:0in;
	mso-para-margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	font-family:"Times New Roman","serif";}
</style>
<![endif]--><!--[if gte mso 9]><xml>
 <o:shapedefaults v:ext=3D"edit" spidmax=3D"1029"/>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <o:shapelayout v:ext=3D"edit">
  <o:idmap v:ext=3D"edit" data=3D"1"/>
 </o:shapelayout></xml><![endif]-->
</head>

<body lang=3DEN-US link=3Dblue vlink=3Dpurple style=3D'tab-interval:.5in'>

<div class=3DWordSection1>

<p class=3DMsoNormal align=3Dcenter style=3D'text-align:center'><b style=3D=
'mso-bidi-font-weight:
normal'><span lang=3DEN-GB style=3D'font-size:22.0pt'>BTEC HIGHER NATIONAL =
DIPLOMA <o:p></o:p></span></b></p>

<p class=3DMsoNormal align=3Dcenter style=3D'text-align:center'><b style=3D=
'mso-bidi-font-weight:
normal'><span lang=3DEN-GB style=3D'font-size:22.0pt'>IN BUSINESS<o:p></o:p=
></span></b></p>

<p class=3DMsoNormal><span lang=3DEN-GB><o:p>&nbsp;</o:p></span></p>

<p class=3DMsoNormal align=3Dcenter style=3D'text-align:center'><b style=3D=
'mso-bidi-font-weight:
normal'><u><span lang=3DEN-GB style=3D'font-size:15.0pt'>Assignment Submiss=
ion </span></u></b><b
style=3D'mso-bidi-font-weight:normal'><u><span lang=3DEN-GB style=3D'font-s=
ize:15.0pt;
mso-fareast-font-family:PMingLiU;mso-fareast-language:ZH-TW'>Form<o:p></o:p=
></span></u></b></p>

<p class=3DMsoNormal><span lang=3DEN-GB><o:p>&nbsp;</o:p></span></p>

<p class=3DMsoNormal><span lang=3DEN-GB><o:p>&nbsp;</o:p></span></p>

<p class=3DMsoNormal><span lang=3DEN-GB><o:p>&nbsp;</o:p></span></p>

<table class=3DMsoNormalTable border=3D0 cellspacing=3D0 cellpadding=3D0 wi=
dth=3D624
 style=3D'width:6.5in;margin-left:18.9pt;border-collapse:collapse;mso-yfti-=
tbllook:
 480;mso-padding-alt:0in 5.4pt 0in 5.4pt'>
 <tr style=3D'mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td width=3D228 valign=3Dtop style=3D'width:171.0pt;padding:0in 5.4pt 0in=
 5.4pt'>
  <p class=3DMsoNormal><span lang=3DEN-GB style=3D'font-family:"Arial","san=
s-serif"'><o:p>&nbsp;</o:p></span></p>
  <p class=3DMsoNormal><span lang=3DEN-GB style=3D'font-family:"Arial","san=
s-serif"'>Student
  Name<span style=3D'mso-tab-count:1'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&=
nbsp;&nbsp;&nbsp; </span><span
  style=3D'mso-spacerun:yes'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbs=
p;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span>:<o:p></o:p></span></p>
  </td>
  <td width=3D396 valign=3Dtop style=3D'width:297.0pt;border:none;border-bo=
ttom:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=3DMsoNormal><span lang=3DEN-GB style=3D'font-family:"Lucida Hand=
writing"'>{{$config['learner']['student_name'] or null}}<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:1'>
  <td width=3D228 valign=3Dtop style=3D'width:171.0pt;padding:0in 5.4pt 0in=
 5.4pt'>
  <p class=3DMsoNormal><span lang=3DEN-GB style=3D'font-family:"Arial","san=
s-serif"'><o:p>&nbsp;</o:p></span></p>
  <p class=3DMsoNormal><span lang=3DEN-GB style=3D'font-family:"Arial","san=
s-serif"'>Intake
  / Class<span style=3D'mso-tab-count:1'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbs=
p;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span
  style=3D'mso-spacerun:yes'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </s=
pan><span
  style=3D'mso-tab-count:1'>&nbsp;&nbsp;&nbsp; </span><span
  style=3D'mso-spacerun:yes'>&nbsp;&nbsp;&nbsp;</span>:<o:p></o:p></span></=
p>
  </td>
  <td width=3D396 valign=3Dtop style=3D'width:297.0pt;border:none;border-bo=
ttom:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowt=
ext .5pt;
  mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=3DMsoNormal style=3D'margin-top:12.0pt;mso-para-margin-top:1.0gd=
'><span
  lang=3DEN-GB style=3D'font-family:"Verdana","sans-serif";mso-bidi-font-fa=
mily:
  Arial'>{{ $config['course']['intake_class'] or null }}<b style=3D'mso-bidi=
-font-weight:
  normal'><o:p></o:p></b></span></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:2'>
  <td width=3D228 valign=3Dtop style=3D'width:171.0pt;padding:0in 5.4pt 0in=
 5.4pt'>
  <p class=3DMsoNormal><span lang=3DEN-GB style=3D'font-family:"Arial","san=
s-serif"'><o:p>&nbsp;</o:p></span></p>
  <p class=3DMsoNormal><span lang=3DEN-GB style=3D'font-family:"Arial","san=
s-serif"'>Module
  / Unit<span style=3D'mso-spacerun:yes'>&nbsp; </span><span style=3D'mso-t=
ab-count:
  2'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbs=
p;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span
  style=3D'mso-spacerun:yes'>&nbsp;&nbsp;&nbsp;</span>: <o:p></o:p></span><=
/p>
  </td>
  <td width=3D396 valign=3Dbottom style=3D'width:297.0pt;border:none;border=
-bottom:
  solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;mso-borde=
r-top-alt:
  solid windowtext .5pt;mso-border-bottom-alt:solid windowtext .5pt;padding:
  0in 5.4pt 0in 5.4pt'>
  <p class=3DMsoNormal style=3D'mso-layout-grid-align:none;text-autospace:n=
one'><span
  lang=3DEN-GB style=3D'font-family:"Verdana","sans-serif";mso-bidi-font-fa=
mily:
  Arial'>{{ $config['course']['module_unit'] or null }}</span><span
  style=3D'font-size:10.0pt;font-family:Bliss-Heavy;mso-fareast-font-family=
:SimSun;
  mso-bidi-font-family:Bliss-Heavy;mso-ansi-language:EN-US'><o:p></o:p></sp=
an></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:3'>
  <td width=3D228 valign=3Dtop style=3D'width:171.0pt;padding:0in 5.4pt 0in=
 5.4pt'>
  <p class=3DMsoNormal style=3D'margin-top:12.0pt'><span lang=3DEN-GB
  style=3D'font-family:"Arial","sans-serif"'>Assignment Reference<span
  style=3D'mso-spacerun:yes'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span
  style=3D'mso-spacerun:yes'>&nbsp;&nbsp;&nbsp;&nbsp;</span>:<o:p></o:p></s=
pan></p>
  </td>
  <td width=3D396 valign=3Dtop style=3D'width:297.0pt;border:none;border-bo=
ttom:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowt=
ext .5pt;
  mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>=
</td>
 </tr>
 <tr style=3D'mso-yfti-irow:4'>
  <td width=3D228 valign=3Dtop style=3D'width:171.0pt;padding:0in 5.4pt 0in=
 5.4pt'>
  <p class=3DMsoNormal><span lang=3DEN-GB style=3D'font-family:"Arial","san=
s-serif"'><o:p>&nbsp;</o:p></span></p>
  <p class=3DMsoNormal><span lang=3DEN-GB style=3D'font-family:"Arial","san=
s-serif"'>Lecturer&#8217;s
  Name<span
  style=3D'mso-spacerun:yes'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbs=
p;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span>:<o:p></o:p></span></p>
  </td>
  <td width=3D396 valign=3Dtop style=3D'width:297.0pt;border:none;border-bo=
ttom:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowt=
ext .5pt;
  mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=3DMsoNormal style=3D'margin-top:12.0pt;mso-para-margin-top:1.0gd=
'><span
  lang=3DEN-GB style=3D'font-family:"Verdana","sans-serif";mso-bidi-font-fa=
mily:
  Arial'>{{ $config['course']['lecture_name'] or null }}</span><span lang=3D=
EN-GB
  style=3D'font-family:"Verdana","sans-serif"'><o:p></o:p></span></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:5'>
  <td width=3D228 valign=3Dtop style=3D'width:171.0pt;padding:0in 5.4pt 0in=
 5.4pt'>
  <p class=3DMsoNormal><span lang=3DEN-GB style=3D'font-family:"Arial","san=
s-serif"'><o:p>&nbsp;</o:p></span></p>
  <p class=3DMsoNormal><span lang=3DEN-GB style=3D'font-family:"Arial","san=
s-serif"'>Date
  Submitted <span style=3D'mso-tab-count:2'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&=
nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbs=
p; </span><span
  style=3D'mso-spacerun:yes'>&nbsp;&nbsp;&nbsp;</span>:<o:p></o:p></span></=
p>
  </td>
  <td width=3D396 valign=3Dtop style=3D'width:297.0pt;border:none;border-bo=
ttom:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowt=
ext .5pt;
  mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=3DMsoNormal><span lang=3DEN-GB style=3D'font-family:"Verdana","s=
ans-serif";
  mso-bidi-font-family:Arial'>{{ $config['course']['date_submitted'] or null }}</span><span lang=
=3DEN-GB
  style=3D'font-family:"Lucida Handwriting"'><o:p></o:p></span></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:6;mso-yfti-lastrow:yes'>
  <td width=3D228 valign=3Dtop style=3D'width:171.0pt;padding:0in 5.4pt 0in=
 5.4pt'>
  <p class=3DMsoNormal><span lang=3DEN-GB style=3D'font-family:"Arial","san=
s-serif"'><o:p>&nbsp;</o:p></span></p>
  <p class=3DMsoNormal><span lang=3DEN-GB style=3D'font-family:"Arial","san=
s-serif"'>Student&#8217;s
  Signature <span style=3D'mso-tab-count:1'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&=
nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span
  style=3D'mso-spacerun:yes'>&nbsp;&nbsp;&nbsp;</span>:<o:p></o:p></span></=
p>
  </td>
  <td width=3D396 valign=3Dtop style=3D'width:297.0pt;border:none;border-bo=
ttom:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowt=
ext .5pt;
  mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=3DMsoNormal><b style=3D'mso-bidi-font-weight:normal'><span lang=
=3DEN-GB
  style=3D'font-family:"Rage Italic"'><o:p>&nbsp;</o:p></span></b></p>
  </td>
 </tr>
</table>

<p class=3DMsoNormal><span lang=3DEN-GB style=3D'font-size:10.0pt'><o:p>&nb=
sp;</o:p></span></p>

<p class=3DMsoNormal><span lang=3DEN-GB><o:p>&nbsp;</o:p></span></p>

<p class=3DMsoNormal><i style=3D'mso-bidi-font-style:normal'><u><span lang=
=3DEN-GB>For
office use:<o:p></o:p></span></u></i></p>

<p class=3DMsoNormal><!--[if gte vml 1]><v:shapetype id=3D"_x0000_t202"
 coordsize=3D"21600,21600" o:spt=3D"202" path=3D"m,l,21600r21600,l21600,xe">
 <v:stroke joinstyle=3D"miter"/>
 <v:path gradientshapeok=3D"t" o:connecttype=3D"rect"/>
</v:shapetype><v:shape id=3D"Text_x0020_Box_x0020_15" o:spid=3D"_x0000_s102=
8"
 type=3D"#_x0000_t202" style=3D'position:absolute;margin-left:13.3pt;margin=
-top:6.6pt;
 width:468.35pt;height:1in;z-index:251656704;visibility:visible;
 mso-wrap-style:square;mso-width-percent:0;mso-height-percent:0;
 mso-wrap-distance-left:9pt;mso-wrap-distance-top:0;mso-wrap-distance-right=
:9pt;
 mso-wrap-distance-bottom:0;mso-position-horizontal:absolute;
 mso-position-horizontal-relative:text;mso-position-vertical:absolute;
 mso-position-vertical-relative:text;mso-width-percent:0;mso-height-percent=
:0;
 mso-width-relative:page;mso-height-relative:page;v-text-anchor:top' o:gfxd=
ata=3D"UEsDBBQABgAIAAAAIQC2gziS/gAAAOEBAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSR=
QU7DMBBF
90jcwfIWJU67QAgl6YK0S0CoHGBkTxKLZGx5TGhvj5O2G0SRWNoz/78nu9wcxkFMGNg6quQqL6RA
0s5Y6ir5vt9lD1JwBDIwOMJKHpHlpr69KfdHjyxSmriSfYz+USnWPY7AufNIadK6MEJMx9ApD/oD
OlTrorhX2lFEilmcO2RdNtjC5xDF9pCuTyYBB5bi6bQ4syoJ3g9WQ0ymaiLzg5KdCXlKLjvcW893
SUOqXwnz5DrgnHtJTxOsQfEKIT7DmDSUCaxw7Rqn8787ZsmRM9e2VmPeBN4uqYvTtW7jvijg9N/y
JsXecLq0q+WD6m8AAAD//wMAUEsDBBQABgAIAAAAIQA4/SH/1gAAAJQBAAALAAAAX3JlbHMvLnJl
bHOkkMFqwzAMhu+DvYPRfXGawxijTi+j0GvpHsDYimMaW0Yy2fr2M4PBMnrbUb/Q94l/f/hMi1qR
JVI2sOt6UJgd+ZiDgffL8ekFlFSbvV0oo4EbChzGx4f9GRdb25HMsYhqlCwG5lrLq9biZkxWOiqY
22YiTra2kYMu1l1tQD30/bPm3wwYN0x18gb45AdQl1tp5j/sFB2T0FQ7R0nTNEV3j6o9feQzro1i
OWA14Fm+Q8a1a8+Bvu/d/dMb2JY5uiPbhG/ktn4cqGU/er3pcvwCAAD//wMAUEsDBBQABgAIAAAA
IQDi1Fm1NAIAAGkEAAAOAAAAZHJzL2Uyb0RvYy54bWysVNuO0zAQfUfiHyy/06SlhW7UdLW0FCEt
F2mXD5g4TmPheIztNlm+nrHT7VYLvCDyYNme8ZmZc2ayuh46zY7SeYWm5NNJzpk0Amtl9iX/dr97
teTMBzA1aDSy5A/S8+v1yxer3hZyhi3qWjpGIMYXvS15G4ItssyLVnbgJ2ilIWODroNAR7fPagc9
oXc6m+X5m6xHV1uHQnpPt9vRyNcJv2mkCF+axsvAdMkpt5BWl9Yqrtl6BcXegW2VOKUB/5BFB8pQ
0DPUFgKwg1O/QXVKOPTYhInALsOmUUKmGqiaaf6smrsWrEy1EDnenmny/w9WfD5+dUzVJX/NmYGO
JLqXQ2DvcGDTRaSnt74grztLfmGge5I5lertLYrvnhnctGD28sY57FsJNaU3jS+zi6cjjo8gVf8J
a4oDh4AJaGhcF7kjNhihk0wPZ2liLoIuF1fzZT5fcCbIdjWdz/OkXQbF42vrfPggsWNxU3JH0id0
ON76ELOB4tElBvOoVb1TWqeD21cb7dgRqE126UsFPHPThvUUfTFbjAT8FSJP358gYgpb8O0YqqZd
9IKiU4HmQKuu5MvzYygine9NnVwCKD3uqRRtTvxGSkdyw1AN5BhJr7B+IKYdjv1O80mbFt1Pznrq
9ZL7HwdwkjP90ZBaiU8ajnSYL97OSAJ3aakuLWAEQZU8cDZuN2EcqIN1at9SpLE/DN6Qwo1K5D9l
dcqb+jlpcpq9ODCX5+T19IdY/wIAAP//AwBQSwMEFAAGAAgAAAAhAITvNdPeAAAACQEAAA8AAABk
cnMvZG93bnJldi54bWxMj0tPwzAQhO9I/AdrkbhRB0dN2jROhXjeKhF64LiJnYeI7Sh2k/DvWU5w
3JnR7Df5cTUDm/Xke2cl3G8iYNrWTvW2lXD+eLnbAfMBrcLBWS3hW3s4FtdXOWbKLfZdz2VoGZVY
n6GELoQx49zXnTboN27UlrzGTQYDnVPL1YQLlZuBiyhKuMHe0ocOR/3Y6fqrvBgJpze/q9Ln+fO1
PLun05I2uBWNlLc368MBWNBr+AvDLz6hQ0FMlbtY5dkgQSQJJUmPBTDy90kcA6tI2KYCeJHz/wuK
HwAAAP//AwBQSwECLQAUAAYACAAAACEAtoM4kv4AAADhAQAAEwAAAAAAAAAAAAAAAAAAAAAAW0Nv
bnRlbnRfVHlwZXNdLnhtbFBLAQItABQABgAIAAAAIQA4/SH/1gAAAJQBAAALAAAAAAAAAAAAAAAA
AC8BAABfcmVscy8ucmVsc1BLAQItABQABgAIAAAAIQDi1Fm1NAIAAGkEAAAOAAAAAAAAAAAAAAAA
AC4CAABkcnMvZTJvRG9jLnhtbFBLAQItABQABgAIAAAAIQCE7zXT3gAAAAkBAAAPAAAAAAAAAAAA
AAAAAI4EAABkcnMvZG93bnJldi54bWxQSwUGAAAAAAQABADzAAAAmQUAAAAA
">
 <v:stroke dashstyle=3D"dash"/>
 <v:textbox>
  <![if !mso]>
  <table cellpadding=3D0 cellspacing=3D0 width=3D"100%">
   <tr>
    <td><![endif]>
    <div>
    <p class=3DMsoNormal><span lang=3DEN-GB><o:p>&nbsp;</o:p></span></p>
    <p class=3DMsoNormal><span lang=3DEN-GB>Acknowledged by:
    ___________________________ Date: __________________</span></p>
    <p class=3DMsoNormal><span lang=3DEN-GB><span style=3D'mso-tab-count:4'=
>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&n=
bsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp=
;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&n=
bsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><s=
pan
    class=3DGramE><span lang=3DEN-GB style=3D'font-size:10.0pt'>Academic </=
span><span
    lang=3DEN-GB style=3D'font-size:10.0pt;mso-fareast-font-family:PMingLiU;
    mso-fareast-language:ZH-TW'>Dept.</span></span><span lang=3DEN-GB
    style=3D'font-size:10.0pt;mso-fareast-font-family:PMingLiU;mso-fareast-=
language:
    ZH-TW'><o:p></o:p></span></p>
    </div>
    <![if !mso]></td>
   </tr>
  </table>
  <![endif]></v:textbox>
</v:shape><![endif]--><![if !vml]><span style=3D'mso-ignore:vglayout'>

<table cellpadding=3D0 cellspacing=3D0 align=3Dleft>
 <tr>
  <td width=3D17 height=3D8></td>
 </tr>
 <tr>
  <td></td>
  <td><img width=3D630 height=3D102 src=3D"master_file_files/image001.gif"
  alt=3D"Text Box: Acknowledged by: ___________________________ Date: _____=
_____________&#13;&#10;				Academic Dept.&#13;&#10;"
  v:shapes=3D"Text_x0020_Box_x0020_15"></td>
 </tr>
</table>

</span><![endif]><i style=3D'mso-bidi-font-style:normal'><span lang=3DEN-GB=
><o:p>&nbsp;</o:p></span></i></p>

<p class=3DMsoNormal><span lang=3DEN-GB><o:p>&nbsp;</o:p></span></p>

<p class=3DMsoNormal><span lang=3DEN-GB><o:p>&nbsp;</o:p></span></p>

<p class=3DMsoNormal><span lang=3DEN-GB><o:p>&nbsp;</o:p></span></p>

<p class=3DMsoNormal><span lang=3DEN-GB><o:p>&nbsp;</o:p></span></p>

<p class=3DMsoNormal><span lang=3DEN-GB><o:p>&nbsp;</o:p></span></p>

<p class=3DMsoNormal style=3D'margin-top:0in;margin-right:-15.7pt;margin-bo=
ttom:
0in;margin-left:-27.0pt;margin-bottom:.0001pt;mso-para-margin-top:0in;
mso-para-margin-right:-1.31gd;mso-para-margin-bottom:0in;mso-para-margin-le=
ft:
-2.25gd;mso-para-margin-bottom:.0001pt'><span lang=3DEN-GB style=3D'font-si=
ze:20.0pt;
font-family:"Courier New";mso-fareast-font-family:Dotum;mso-fareast-languag=
e:
ZH-TW'><o:p>&nbsp;</o:p></span></p>

<br style=3D'mso-ignore:vglayout' clear=3DALL>

<p class=3DMsoNormal style=3D'margin-top:0in;margin-right:-15.7pt;margin-bo=
ttom:
0in;margin-left:-27.0pt;margin-bottom:.0001pt;mso-para-margin-top:0in;
mso-para-margin-right:-1.31gd;mso-para-margin-bottom:0in;mso-para-margin-le=
ft:
-2.25gd;mso-para-margin-bottom:.0001pt'><span lang=3DEN-GB style=3D'font-si=
ze:20.0pt;
font-family:"Courier New";mso-fareast-font-family:Dotum;mso-fareast-languag=
e:
ZH-TW'>8</span><span lang=3DEN-GB style=3D'font-size:20.0pt;font-family:Cou=
rier;
mso-fareast-font-family:PMingLiU;mso-fareast-language:ZH-TW'>&gt;&lt;</span=
><span
lang=3DEN-GB style=3D'font-size:18.0pt'>-----------------------------------=
-----------------</span><span
lang=3DEN-GB style=3D'font-size:18.0pt;mso-fareast-font-family:PMingLiU;mso=
-fareast-language:
ZH-TW'>-</span><span lang=3DEN-GB style=3D'font-size:18.0pt'>--------------=
</span><span
lang=3DEN-GB style=3D'font-size:18.0pt;mso-fareast-font-family:PMingLiU;mso=
-fareast-language:
ZH-TW'>---</span><span lang=3DEN-GB style=3D'font-size:18.0pt'>-------</spa=
n><b
style=3D'mso-bidi-font-weight:normal'><span lang=3DEN-GB style=3D'font-size=
:18.0pt;
mso-fareast-font-family:PMingLiU;mso-fareast-language:ZH-TW'><o:p></o:p></s=
pan></b></p>

<p class=3DMsoNormal><span lang=3DEN-GB><o:p>&nbsp;</o:p></span></p>

<p class=3DMsoNormal style=3D'tab-stops:3.15in;mso-line-break-override:rest=
rictions'><!--[if gte vml 1]><o:wrapblock><v:shape
  id=3D"Text_x0020_Box_x0020_16" o:spid=3D"_x0000_s1027" type=3D"#_x0000_t2=
02"
  style=3D'position:absolute;margin-left:13.3pt;margin-top:.2pt;width:468.3=
5pt;
  height:195pt;z-index:251657728;visibility:visible;mso-wrap-style:square;
  mso-width-percent:0;mso-height-percent:0;mso-wrap-distance-left:9pt;
  mso-wrap-distance-top:0;mso-wrap-distance-right:9pt;
  mso-wrap-distance-bottom:0;mso-position-horizontal:absolute;
  mso-position-horizontal-relative:text;mso-position-vertical:absolute;
  mso-position-vertical-relative:text;mso-width-percent:0;mso-height-percen=
t:0;
  mso-width-relative:page;mso-height-relative:page;v-text-anchor:top'
  o:gfxdata=3D"UEsDBBQABgAIAAAAIQC2gziS/gAAAOEBAAATAAAAW0NvbnRlbnRfVHlwZXNd=
LnhtbJSRQU7DMBBF
90jcwfIWJU67QAgl6YK0S0CoHGBkTxKLZGx5TGhvj5O2G0SRWNoz/78nu9wcxkFMGNg6quQqL6RA
0s5Y6ir5vt9lD1JwBDIwOMJKHpHlpr69KfdHjyxSmriSfYz+USnWPY7AufNIadK6MEJMx9ApD/oD
OlTrorhX2lFEilmcO2RdNtjC5xDF9pCuTyYBB5bi6bQ4syoJ3g9WQ0ymaiLzg5KdCXlKLjvcW893
SUOqXwnz5DrgnHtJTxOsQfEKIT7DmDSUCaxw7Rqn8787ZsmRM9e2VmPeBN4uqYvTtW7jvijg9N/y
JsXecLq0q+WD6m8AAAD//wMAUEsDBBQABgAIAAAAIQA4/SH/1gAAAJQBAAALAAAAX3JlbHMvLnJl
bHOkkMFqwzAMhu+DvYPRfXGawxijTi+j0GvpHsDYimMaW0Yy2fr2M4PBMnrbUb/Q94l/f/hMi1qR
JVI2sOt6UJgd+ZiDgffL8ekFlFSbvV0oo4EbChzGx4f9GRdb25HMsYhqlCwG5lrLq9biZkxWOiqY
22YiTra2kYMu1l1tQD30/bPm3wwYN0x18gb45AdQl1tp5j/sFB2T0FQ7R0nTNEV3j6o9feQzro1i
OWA14Fm+Q8a1a8+Bvu/d/dMb2JY5uiPbhG/ktn4cqGU/er3pcvwCAAD//wMAUEsDBBQABgAIAAAA
IQAPZvNpMAIAAFkEAAAOAAAAZHJzL2Uyb0RvYy54bWysVNuO0zAQfUfiHyy/06RV2t1GTVdLlyKk
5SLt8gGO4yQWjsfYbpPy9YydbImAJ0QeLI9nfDxzzkx2d0OnyFlYJ0EXdLlIKRGaQyV1U9Cvz8c3
t5Q4z3TFFGhR0Itw9G7/+tWuN7lYQQuqEpYgiHZ5bwraem/yJHG8FR1zCzBCo7MG2zGPpm2SyrIe
0TuVrNJ0k/RgK2OBC+fw9GF00n3Er2vB/ee6dsITVVDMzcfVxrUMa7LfsbyxzLSST2mwf8iiY1Lj
o1eoB+YZOVn5B1QnuQUHtV9w6BKoa8lFrAGrWaa/VfPUMiNiLUiOM1ea3P+D5Z/OXyyRFWpHiWYd
SvQsBk/ewkCWm0BPb1yOUU8G4/yA5yE0lOrMI/Bvjmg4tEw34t5a6FvBKkxvGW4ms6sjjgsgZf8R
KnyHnTxEoKG2XQBENgiio0yXqzQhF46H6212m2ZrSjj6VtnNZp1G8RKWv1w31vn3AjoSNgW1qH2E
Z+dH50M6LH8JiemDktVRKhUN25QHZcmZYZ8c4xcrwCrnYUqTvqDb9Wo9MjD3uTlEGr+/QXTSY8Mr
2RX09hrE8sDbO13FdvRMqnGPKSs9ERm4G1n0QzlMkk36lFBdkFkLY3/jPOKmBfuDkh57u6Du+4lZ
QYn6oFGd7TLLwjBEI1vfrNCwc0859zDNEaqgnpJxe/DjAJ2MlU2LL439oOEeFa1l5DpIP2Y1pY/9
GyWYZi0MyNyOUb/+CPufAAAA//8DAFBLAwQUAAYACAAAACEAHTb/sd0AAAAHAQAADwAAAGRycy9k
b3ducmV2LnhtbEyOwU7DMBBE70j8g7VIXBB1aCLThDgVQgLBDUpVrm68TSLsdbDdNPw95gTH0Yze
vHo9W8Mm9GFwJOFmkQFDap0eqJOwfX+8XgELUZFWxhFK+MYA6+b8rFaVdid6w2kTO5YgFColoY9x
rDgPbY9WhYUbkVJ3cN6qmKLvuPbqlODW8GWWCW7VQOmhVyM+9Nh+bo5Wwqp4nj7CS/66a8XBlPHq
dnr68lJeXsz3d8AizvFvDL/6SR2a5LR3R9KBGQlLIdJSQgEstaXIc2B7CXmZFcCbmv/3b34AAAD/
/wMAUEsBAi0AFAAGAAgAAAAhALaDOJL+AAAA4QEAABMAAAAAAAAAAAAAAAAAAAAAAFtDb250ZW50
X1R5cGVzXS54bWxQSwECLQAUAAYACAAAACEAOP0h/9YAAACUAQAACwAAAAAAAAAAAAAAAAAvAQAA
X3JlbHMvLnJlbHNQSwECLQAUAAYACAAAACEAD2bzaTACAABZBAAADgAAAAAAAAAAAAAAAAAuAgAA
ZHJzL2Uyb0RvYy54bWxQSwECLQAUAAYACAAAACEAHTb/sd0AAAAHAQAADwAAAAAAAAAAAAAAAACK
BAAAZHJzL2Rvd25yZXYueG1sUEsFBgAAAAAEAAQA8wAAAJQFAAAAAA=3D=3D
"/>
 <![endif]--><![if !vml]><span style=3D'mso-ignore:vglayout;position:relati=
ve;
 z-index:251657728'><span style=3D'position:absolute;left:17px;top:-1px;
 width:630px;height:266px'>
 <table cellpadding=3D0 cellspacing=3D0>
  <tr>
   <td width=3D630 height=3D266 bgcolor=3Dwhite style=3D'border:.75pt solid=
 black;
   vertical-align:top;background:white'><![endif]><![if !mso]><span
   style=3D'position:absolute;mso-ignore:vglayout;z-index:251657728'>
   <table cellpadding=3D0 cellspacing=3D0 width=3D"100%">
    <tr>
     <td><![endif]>
     <div v:shape=3D"Text_x0020_Box_x0020_16" style=3D'padding:4.35pt 7.95p=
t 4.35pt 7.95pt'
     class=3Dshape>
     <p class=3DMsoNormal><b style=3D'mso-bidi-font-weight:normal'><u><span
     lang=3DEN-GB>Return slip to student</span></u><span lang=3DEN-GB>:<o:p=
></o:p></span></b></p>
     <p class=3DMsoNormal style=3D'line-height:10.0pt;mso-line-height-rule:=
exactly'><span
     lang=3DEN-GB style=3D'font-size:9.0pt'><o:p>&nbsp;</o:p></span></p>
     <p class=3DMsoNormal style=3D'line-height:20.0pt;mso-line-height-rule:=
exactly'><span
     lang=3DEN-GB>I </span><span lang=3DEN-GB style=3D'font-size:10.0pt'>(s=
tudent
     Name)</span><span lang=3DEN-GB>, ______________________________, Class:
     __________________ hereby confirm that I submit my assignment</span><s=
pan
     lang=3DEN-GB style=3D'mso-fareast-font-family:PMingLiU;mso-fareast-lan=
guage:
     ZH-TW'> of </span><span lang=3DEN-GB>Module / <span class=3DGramE>Unit=
<span
     style=3D'mso-fareast-font-family:PMingLiU;mso-fareast-language:ZH-TW'>=
 </span>:</span>
     ___________________________ ______________________ on the date </span>=
<span
     lang=3DEN-GB style=3D'mso-fareast-font-family:PMingLiU;mso-fareast-lan=
guage:
     ZH-TW'>o</span><span lang=3DEN-GB>f</span><span lang=3DEN-GB style=3D'=
mso-fareast-font-family:
     PMingLiU;mso-fareast-language:ZH-TW'> ______________________________ <=
/span><span
     lang=3DEN-GB>.</span><span lang=3DEN-GB style=3D'font-size:10.0pt;mso-=
fareast-font-family:
     PMingLiU;mso-fareast-language:ZH-TW'>(<span class=3DSpellE>dd</span>/m=
m/<span
     class=3DSpellE>yy</span>)</span><span lang=3DEN-GB style=3D'mso-fareas=
t-font-family:
     PMingLiU;mso-fareast-language:ZH-TW'><o:p></o:p></span></p>
     <p class=3DMsoNormal><span lang=3DEN-GB><o:p>&nbsp;</o:p></span></p>
     <p class=3DMsoNormal><span lang=3DEN-GB><o:p>&nbsp;</o:p></span></p>
     <p class=3DMsoNormal><span lang=3DEN-GB>Student&#8217;s signature</spa=
n><span
     lang=3DEN-GB style=3D'mso-fareast-font-family:PMingLiU;mso-fareast-lan=
guage:
     ZH-TW'>: </span><span lang=3DEN-GB>_______________________________</sp=
an><span
     lang=3DEN-GB style=3D'mso-fareast-font-family:PMingLiU;mso-fareast-lan=
guage:
     ZH-TW'><o:p></o:p></span></p>
     <p class=3DMsoNormal style=3D'line-height:25.0pt;mso-line-height-rule:=
exactly'><span
     lang=3DEN-GB><o:p>&nbsp;</o:p></span></p>
     <p class=3DMsoNormal><b style=3D'mso-bidi-font-weight:normal'><span
     lang=3DEN-GB>Confirmed and received by: </span></b><span lang=3DEN-GB>=
______________________
     Date: _______________</span></p>
     <p class=3DMsoNormal><span lang=3DEN-GB><span style=3D'mso-tab-count:5=
'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&=
nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbs=
p;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&=
nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbs=
p;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
     class=3DGramE><span lang=3DEN-GB style=3D'font-size:9.0pt'>Academic </=
span><span
     lang=3DEN-GB style=3D'font-size:9.0pt;mso-fareast-font-family:PMingLiU;
     mso-fareast-language:ZH-TW'>Dept.</span></span><span lang=3DEN-GB
     style=3D'font-size:9.0pt;mso-fareast-font-family:PMingLiU;mso-fareast-=
language:
     ZH-TW'><o:p></o:p></span></p>
     </div>
     <![if !mso]></td>
    </tr>
   </table>
   </span><![endif]><![if !mso & !vml]>&nbsp;<![endif]><![if !vml]></td>
  </tr>
 </table>
 </span></span><![endif]><!--[if gte vml 1]></o:wrapblock><![endif]--><b
style=3D'mso-bidi-font-weight:normal'><span lang=3DEN-GB><o:p>&nbsp;</o:p><=
/span></b></p>

<p class=3DMsoNormal style=3D'tab-stops:3.15in;mso-line-break-override:rest=
rictions'><b
style=3D'mso-bidi-font-weight:normal'><span lang=3DEN-GB><o:p>&nbsp;</o:p><=
/span></b></p>

<p class=3DMsoNormal style=3D'tab-stops:3.15in;mso-line-break-override:rest=
rictions'><b
style=3D'mso-bidi-font-weight:normal'><span lang=3DEN-GB><o:p>&nbsp;</o:p><=
/span></b></p>

<p class=3DMsoNormal style=3D'tab-stops:3.15in;mso-line-break-override:rest=
rictions'><b
style=3D'mso-bidi-font-weight:normal'><span lang=3DEN-GB><o:p>&nbsp;</o:p><=
/span></b></p>

<p class=3DMsoNormal style=3D'tab-stops:3.15in;mso-line-break-override:rest=
rictions'><b
style=3D'mso-bidi-font-weight:normal'><span lang=3DEN-GB><o:p>&nbsp;</o:p><=
/span></b></p>

<p class=3DMsoNormal style=3D'tab-stops:3.15in;mso-line-break-override:rest=
rictions'><b
style=3D'mso-bidi-font-weight:normal'><span lang=3DEN-GB><o:p>&nbsp;</o:p><=
/span></b></p>

<p class=3DMsoNormal style=3D'tab-stops:3.15in;mso-line-break-override:rest=
rictions'><b
style=3D'mso-bidi-font-weight:normal'><span lang=3DEN-GB><o:p>&nbsp;</o:p><=
/span></b></p>

<p class=3DMsoNormal style=3D'tab-stops:3.15in;mso-line-break-override:rest=
rictions'><b
style=3D'mso-bidi-font-weight:normal'><span lang=3DEN-GB><o:p>&nbsp;</o:p><=
/span></b></p>

<p class=3DMsoNormal style=3D'tab-stops:3.15in;mso-line-break-override:rest=
rictions'><b
style=3D'mso-bidi-font-weight:normal'><span lang=3DEN-GB><o:p>&nbsp;</o:p><=
/span></b></p>

<p class=3DMsoNormal style=3D'tab-stops:3.15in;mso-line-break-override:rest=
rictions'><b
style=3D'mso-bidi-font-weight:normal'><span lang=3DEN-GB><o:p>&nbsp;</o:p><=
/span></b></p>

<p class=3DMsoNormal style=3D'tab-stops:3.15in;mso-line-break-override:rest=
rictions'><b
style=3D'mso-bidi-font-weight:normal'><span lang=3DEN-GB><o:p>&nbsp;</o:p><=
/span></b></p>

<p class=3DMsoNormal style=3D'tab-stops:3.15in;mso-line-break-override:rest=
rictions'><b
style=3D'mso-bidi-font-weight:normal'><span lang=3DEN-GB><o:p>&nbsp;</o:p><=
/span></b></p>

<p class=3DMsoNormal style=3D'tab-stops:3.15in;mso-line-break-override:rest=
rictions'><b
style=3D'mso-bidi-font-weight:normal'><span lang=3DEN-GB><o:p>&nbsp;</o:p><=
/span></b></p>

<p class=3DMsoNormal style=3D'tab-stops:3.15in;mso-line-break-override:rest=
rictions'><b
style=3D'mso-bidi-font-weight:normal'><span lang=3DEN-GB><o:p>&nbsp;</o:p><=
/span></b></p>

<p class=3DMsoNormal style=3D'tab-stops:3.15in;mso-line-break-override:rest=
rictions'><b
style=3D'mso-bidi-font-weight:normal'><span lang=3DEN-GB><o:p>&nbsp;</o:p><=
/span></b></p>

<p class=3DMsoNormal style=3D'tab-stops:3.15in;mso-line-break-override:rest=
rictions'><b
style=3D'mso-bidi-font-weight:normal'><span lang=3DEN-GB><o:p>&nbsp;</o:p><=
/span></b></p>

<p class=3DMsoNormal style=3D'tab-stops:3.15in;mso-line-break-override:rest=
rictions'><b
style=3D'mso-bidi-font-weight:normal'><span lang=3DEN-GB><o:p>&nbsp;</o:p><=
/span></b></p>

<p class=3DMsoNormal style=3D'tab-stops:3.15in;mso-line-break-override:rest=
rictions'><b
style=3D'mso-bidi-font-weight:normal'><span lang=3DEN-GB><o:p>&nbsp;</o:p><=
/span></b></p>

<b style=3D'mso-bidi-font-weight:normal'><span lang=3DEN-GB style=3D'font-s=
ize:12.0pt;
font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Ro=
man";
mso-ansi-language:EN-GB;mso-fareast-language:EN-US;mso-bidi-language:AR-SA'=
><br
clear=3Dall style=3D'page-break-before:always'>
</span></b><br style=3D'mso-ignore:vglayout' clear=3DALL>

<p class=3DMsoNormal style=3D'mso-line-break-override:restrictions'><b
style=3D'mso-bidi-font-weight:normal'><span lang=3DEN-GB>BTEC Higher Nation=
al
Diploma in Business<span style=3D'mso-tab-count:1'>&nbsp;&nbsp;&nbsp;&nbsp;=
&nbsp;&nbsp;&nbsp;&nbsp; </span><o:p></o:p></span></b></p>

<p class=3DMsoNormal style=3D'mso-line-break-override:restrictions'><!--[if=
 gte vml 1]><v:shapetype
 id=3D"_x0000_t75" coordsize=3D"21600,21600" o:spt=3D"75" o:preferrelative=
=3D"t"
 path=3D"m@4@5l@4@11@9@11@9@5xe" filled=3D"f" stroked=3D"f">
 <v:stroke joinstyle=3D"miter"/>
 <v:formulas>
  <v:f eqn=3D"if lineDrawn pixelLineWidth 0"/>
  <v:f eqn=3D"sum @0 1 0"/>
  <v:f eqn=3D"sum 0 0 @1"/>
  <v:f eqn=3D"prod @2 1 2"/>
  <v:f eqn=3D"prod @3 21600 pixelWidth"/>
  <v:f eqn=3D"prod @3 21600 pixelHeight"/>
  <v:f eqn=3D"sum @0 0 1"/>
  <v:f eqn=3D"prod @6 1 2"/>
  <v:f eqn=3D"prod @7 21600 pixelWidth"/>
  <v:f eqn=3D"sum @8 21600 0"/>
  <v:f eqn=3D"prod @7 21600 pixelHeight"/>
  <v:f eqn=3D"sum @10 21600 0"/>
 </v:formulas>
 <v:path o:extrusionok=3D"f" gradientshapeok=3D"t" o:connecttype=3D"rect"/>
 <o:lock v:ext=3D"edit" aspectratio=3D"t"/>
</v:shapetype><v:shape id=3D"Picture_x0020_2" o:spid=3D"_x0000_s1026" type=
=3D"#_x0000_t75"
 alt=3D"Description: Description: Description: Description: DIC_logo_revise=
d_110112.jpg"
 style=3D'position:absolute;margin-left:107.65pt;margin-top:0;width:158.85p=
t;
 height:44.1pt;z-index:251658752;visibility:visible;mso-wrap-style:square;
 mso-width-percent:0;mso-height-percent:0;mso-wrap-distance-left:9pt;
 mso-wrap-distance-top:0;mso-wrap-distance-right:9pt;
 mso-wrap-distance-bottom:0;mso-position-horizontal:right;
 mso-position-horizontal-relative:margin;mso-position-vertical:top;
 mso-position-vertical-relative:margin;mso-width-percent:0;
 mso-height-percent:0;mso-width-relative:page;mso-height-relative:page'>
 <v:imagedata src=3D"master_file_files/image002.jpg" o:title=3D" DIC_logo_r=
evised_110112"
  cropright=3D"4551f"/>
 <w:wrap type=3D"square" anchorx=3D"margin" anchory=3D"margin"/>
</v:shape><![endif]--><![if !vml]><img width=3D212 height=3D59
src=3D"master_file_files/image003.jpg" align=3Dright hspace=3D12
alt=3D"Description: Description: Description: Description: DIC_logo_revised=
_110112.jpg"
v:shapes=3D"Picture_x0020_2"><![endif]><b style=3D'mso-bidi-font-weight:nor=
mal'><span
lang=3DEN-GB style=3D'font-family:"Arial","sans-serif";mso-fareast-font-fam=
ily:
PMingLiU;mso-fareast-language:ZH-TW'>ASSESSMENT COVER</span></b><b
style=3D'mso-bidi-font-weight:normal'><span lang=3DEN-GB style=3D'font-fami=
ly:"Arial","sans-serif"'>
SHEET<span
style=3D'mso-spacerun:yes'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nb=
sp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span style=3D'mso-tab-count:8'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nb=
sp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nb=
sp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nb=
sp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span
style=3D'mso-spacerun:yes'>&nbsp;&nbsp;</span></span></b><b style=3D'mso-bi=
di-font-weight:
normal'><span lang=3DEN-GB style=3D'mso-fareast-font-family:PMingLiU;mso-fa=
reast-language:
ZH-TW'><o:p></o:p></span></b></p>

<p class=3DMsoNormal style=3D'tab-stops:3.15in;mso-line-break-override:rest=
rictions'><span
lang=3DEN-GB style=3D'font-family:"Helvetica","sans-serif";mso-fareast-font=
-family:
PMingLiU;mso-bidi-font-family:"Times New Roman";mso-fareast-language:ZH-TW'=
><o:p>&nbsp;</o:p></span></p>

<table class=3DMsoNormalTable border=3D0 cellspacing=3D0 cellpadding=3D0 wi=
dth=3D732
 style=3D'width:549.0pt;margin-left:-12.6pt;border-collapse:collapse;mso-yf=
ti-tbllook:
 480;mso-padding-alt:0in 5.4pt 0in 5.4pt'>
 <tr style=3D'mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td width=3D120 valign=3Dtop style=3D'width:1.25in;padding:0in 5.4pt 0in =
5.4pt'>
  <p class=3DMsoHeader style=3D'margin-top:2.4pt;margin-right:0in;margin-bo=
ttom:
  2.4pt;margin-left:0in;mso-para-margin-top:.2gd;mso-para-margin-right:0in;
  mso-para-margin-bottom:.2gd;mso-para-margin-left:0in;tab-stops:.5in cente=
r 3.0in right 6.0in'><b
  style=3D'mso-bidi-font-weight:normal'><span lang=3DEN-GB style=3D'font-si=
ze:12.0pt;
  mso-fareast-font-family:PMingLiU;mso-fareast-language:ZH-TW'>Student Name=
:<o:p></o:p></span></b></p>
  </td>
  <td width=3D300 valign=3Dtop style=3D'width:225.0pt;border:none;border-bo=
ttom:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=3DMsoHeader style=3D'margin-top:2.4pt;margin-right:0in;margin-bo=
ttom:
  2.4pt;margin-left:0in;mso-para-margin-top:.2gd;mso-para-margin-right:0in;
  mso-para-margin-bottom:.2gd;mso-para-margin-left:0in;tab-stops:.5in cente=
r 3.0in right 6.0in'><span
  lang=3DEN-GB style=3D'font-size:12.0pt;mso-fareast-font-family:PMingLiU;
  mso-fareast-language:ZH-TW'>{{ $config['learner']['student_name'] or null }}<o:p></o:p></span></=
p>
  </td>
  <td width=3D144 valign=3Dtop style=3D'width:1.5in;padding:0in 5.4pt 0in 5=
.4pt'>
  <p class=3DMsoHeader style=3D'margin-top:2.4pt;margin-right:0in;margin-bo=
ttom:
  2.4pt;margin-left:0in;mso-para-margin-top:.2gd;mso-para-margin-right:0in;
  mso-para-margin-bottom:.2gd;mso-para-margin-left:0in;tab-stops:.5in cente=
r 3.0in right 6.0in'><b
  style=3D'mso-bidi-font-weight:normal'><span lang=3DEN-GB style=3D'font-si=
ze:12.0pt;
  mso-fareast-font-family:PMingLiU;mso-fareast-language:ZH-TW'>Intake / Cla=
ss:<o:p></o:p></span></b></p>
  </td>
  <td width=3D168 valign=3Dtop style=3D'width:1.75in;border:none;border-bot=
tom:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=3DMsoHeader style=3D'margin-top:2.4pt;margin-right:0in;margin-bo=
ttom:
  2.4pt;margin-left:0in;mso-para-margin-top:.2gd;mso-para-margin-right:0in;
  mso-para-margin-bottom:.2gd;mso-para-margin-left:0in;tab-stops:.5in cente=
r 3.0in right 6.0in'><b
  style=3D'mso-bidi-font-weight:normal'><span lang=3DEN-GB style=3D'font-si=
ze:12.0pt;
  font-family:"Arial","sans-serif";mso-fareast-font-family:PMingLiU;mso-far=
east-language:
  ZH-TW'>{{ $config['course']['intake_class'] or null }}<o:p></o:p></span></=
b></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:1'>
  <td width=3D120 valign=3Dtop style=3D'width:1.25in;padding:0in 5.4pt 0in =
5.4pt'>
  <p class=3DMsoHeader style=3D'margin-top:2.4pt;margin-right:0in;margin-bo=
ttom:
  2.4pt;margin-left:0in;mso-para-margin-top:.2gd;mso-para-margin-right:0in;
  mso-para-margin-bottom:.2gd;mso-para-margin-left:0in;tab-stops:.5in cente=
r 3.0in right 6.0in'><b
  style=3D'mso-bidi-font-weight:normal'><span lang=3DEN-GB style=3D'font-si=
ze:12.0pt;
  mso-fareast-font-family:PMingLiU;mso-fareast-language:ZH-TW'>Unit Title:<=
o:p></o:p></span></b></p>
  </td>
  <td width=3D300 valign=3Dtop style=3D'width:225.0pt;border:none;border-bo=
ttom:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowt=
ext .5pt;
  mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=3DMsoHeader style=3D'margin-top:2.4pt;margin-right:0in;margin-bo=
ttom:
  2.4pt;margin-left:0in;mso-para-margin-top:.2gd;mso-para-margin-right:0in;
  mso-para-margin-bottom:.2gd;mso-para-margin-left:0in;tab-stops:.5in cente=
r 3.0in right 6.0in'><b
  style=3D'mso-bidi-font-weight:normal'><span lang=3DEN-GB style=3D'font-si=
ze:12.0pt;
  font-family:"Book Antiqua","serif";mso-fareast-font-family:PMingLiU;
  mso-fareast-language:ZH-TW'>{{ $config['course']['module_unit'] or null}}<o:p></o:p></span></b></p>
  </td>
  <td width=3D144 valign=3Dtop style=3D'width:1.5in;padding:0in 5.4pt 0in 5=
.4pt'>
  <p class=3DMsoHeader style=3D'margin-top:2.4pt;margin-right:0in;margin-bo=
ttom:
  2.4pt;margin-left:0in;mso-para-margin-top:.2gd;mso-para-margin-right:0in;
  mso-para-margin-bottom:.2gd;mso-para-margin-left:0in;tab-stops:.5in cente=
r 3.0in right 6.0in'><b
  style=3D'mso-bidi-font-weight:normal'><span lang=3DEN-GB style=3D'font-si=
ze:12.0pt;
  mso-fareast-font-family:PMingLiU;mso-fareast-language:ZH-TW'>Assignment R=
ef.<o:p></o:p></span></b></p>
  </td>
  <td width=3D168 valign=3Dtop style=3D'width:1.75in;border:none;border-bot=
tom:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowt=
ext .5pt;
  mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>=
</td>
 </tr>
 <tr style=3D'mso-yfti-irow:2'>
  <td width=3D120 valign=3Dtop style=3D'width:1.25in;padding:0in 5.4pt 0in =
5.4pt'>
  <p class=3DMsoHeader style=3D'margin-top:2.4pt;margin-right:0in;margin-bo=
ttom:
  2.4pt;margin-left:0in;mso-para-margin-top:.2gd;mso-para-margin-right:0in;
  mso-para-margin-bottom:.2gd;mso-para-margin-left:0in;tab-stops:.5in cente=
r 3.0in right 6.0in'><b
  style=3D'mso-bidi-font-weight:normal'><span lang=3DEN-GB style=3D'font-si=
ze:12.0pt;
  mso-fareast-font-family:PMingLiU;mso-fareast-language:ZH-TW'>Date due:<o:=
p></o:p></span></b></p>
  </td>
  <td width=3D300 valign=3Dtop style=3D'width:225.0pt;border:none;border-bo=
ttom:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowt=
ext .5pt;
  mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=3DMsoHeader style=3D'margin-top:2.4pt;margin-right:0in;margin-bo=
ttom:
  2.4pt;margin-left:0in;mso-para-margin-top:.2gd;mso-para-margin-right:0in;
  mso-para-margin-bottom:.2gd;mso-para-margin-left:0in;tab-stops:.5in cente=
r 3.0in right 6.0in'><span
  lang=3DEN-GB style=3D'font-size:12.0pt;mso-fareast-font-family:PMingLiU;
  mso-fareast-language:ZH-TW'>{{$config['course']['date_due'] or null}}<o:p></o:p></span></p>
  </td>
  <td width=3D144 valign=3Dtop style=3D'width:1.5in;padding:0in 5.4pt 0in 5=
.4pt'>
  <p class=3DMsoHeader style=3D'margin-top:2.4pt;margin-right:0in;margin-bo=
ttom:
  2.4pt;margin-left:0in;mso-para-margin-top:.2gd;mso-para-margin-right:0in;
  mso-para-margin-bottom:.2gd;mso-para-margin-left:0in;tab-stops:.5in cente=
r 3.0in right 6.0in'><b
  style=3D'mso-bidi-font-weight:normal'><span lang=3DEN-GB style=3D'font-si=
ze:12.0pt;
  mso-fareast-font-family:PMingLiU;mso-fareast-language:ZH-TW'>Date submitt=
ed:<o:p></o:p></span></b></p>
  </td>
  <td width=3D168 valign=3Dtop style=3D'width:1.75in;border:none;border-bot=
tom:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowt=
ext .5pt;
  mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=3DMsoHeader style=3D'margin-top:2.4pt;margin-right:0in;margin-bo=
ttom:
  2.4pt;margin-left:0in;mso-para-margin-top:.2gd;mso-para-margin-right:0in;
  mso-para-margin-bottom:.2gd;mso-para-margin-left:0in;tab-stops:.5in cente=
r 3.0in right 6.0in'><span
  lang=3DEN-GB style=3D'font-size:12.0pt;mso-fareast-font-family:PMingLiU;
  mso-fareast-language:ZH-TW'>{{$config['course']['date_submitted'] or null}}<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:3'>
  <td width=3D120 valign=3Dtop style=3D'width:1.25in;padding:0in 5.4pt 0in =
5.4pt'>
  <p class=3DMsoHeader style=3D'margin-top:2.4pt;margin-right:0in;margin-bo=
ttom:
  2.4pt;margin-left:0in;mso-para-margin-top:.2gd;mso-para-margin-right:0in;
  mso-para-margin-bottom:.2gd;mso-para-margin-left:0in;tab-stops:.5in cente=
r 3.0in right 6.0in'><b
  style=3D'mso-bidi-font-weight:normal'><span lang=3DEN-GB style=3D'font-si=
ze:12.0pt;
  mso-fareast-font-family:PMingLiU;mso-fareast-language:ZH-TW'>Assessor(s)<=
o:p></o:p></span></b></p>
  </td>
  <td width=3D300 valign=3Dtop style=3D'width:225.0pt;border:none;border-bo=
ttom:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowt=
ext .5pt;
  mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=3DMsoHeader style=3D'margin-top:2.4pt;margin-right:0in;margin-bo=
ttom:
  2.4pt;margin-left:0in;mso-para-margin-top:.2gd;mso-para-margin-right:0in;
  mso-para-margin-bottom:.2gd;mso-para-margin-left:0in;tab-stops:.5in cente=
r 3.0in right 6.0in'><span
  lang=3DEN-GB style=3D'font-size:12.0pt;mso-fareast-font-family:PMingLiU;
  mso-fareast-language:ZH-TW'>{{$config['course&']['lecture_name'] or null}}<o:p></o:p></span></=
p>
  </td>
  <td width=3D144 valign=3Dtop style=3D'width:1.5in;padding:0in 5.4pt 0in 5=
.4pt'>
  <p class=3DMsoHeader style=3D'margin-top:2.4pt;margin-right:0in;margin-bo=
ttom:
  2.4pt;margin-left:0in;mso-para-margin-top:.2gd;mso-para-margin-right:0in;
  mso-para-margin-bottom:.2gd;mso-para-margin-left:0in;tab-stops:.5in cente=
r 3.0in right 6.0in'><span
  lang=3DEN-GB style=3D'font-size:12.0pt;mso-fareast-font-family:PMingLiU;
  mso-fareast-language:ZH-TW'><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=3D168 valign=3Dtop style=3D'width:1.75in;border:none;mso-border=
-top-alt:
  solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=3DMsoHeader style=3D'margin-top:2.4pt;margin-right:0in;margin-bo=
ttom:
  2.4pt;margin-left:0in;mso-para-margin-top:.2gd;mso-para-margin-right:0in;
  mso-para-margin-bottom:.2gd;mso-para-margin-left:0in;tab-stops:.5in cente=
r 3.0in right 6.0in'><span
  lang=3DEN-GB style=3D'font-size:12.0pt;mso-fareast-font-family:PMingLiU;
  mso-fareast-language:ZH-TW'><o:p>&nbsp;</o:p></span></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:4;height:9.25pt'>
  <td width=3D120 valign=3Dtop style=3D'width:1.25in;padding:0in 5.4pt 0in =
5.4pt;
  height:9.25pt'>
  <p class=3DMsoHeader style=3D'margin-top:2.4pt;margin-right:0in;margin-bo=
ttom:
  2.4pt;margin-left:0in;mso-para-margin-top:.2gd;mso-para-margin-right:0in;
  mso-para-margin-bottom:.2gd;mso-para-margin-left:0in;tab-stops:.5in cente=
r 3.0in right 6.0in'><span
  lang=3DEN-GB style=3D'font-size:12.0pt;mso-fareast-font-family:PMingLiU;
  mso-fareast-language:ZH-TW'><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=3D300 valign=3Dtop style=3D'width:225.0pt;border:none;mso-borde=
r-top-alt:
  solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:9.25pt'>
  <p class=3DMsoHeader style=3D'margin-top:2.4pt;margin-right:0in;margin-bo=
ttom:
  2.4pt;margin-left:0in;mso-para-margin-top:.2gd;mso-para-margin-right:0in;
  mso-para-margin-bottom:.2gd;mso-para-margin-left:0in;tab-stops:.5in cente=
r 3.0in right 6.0in'><span
  lang=3DEN-GB style=3D'font-size:12.0pt;mso-fareast-font-family:PMingLiU;
  mso-fareast-language:ZH-TW'><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=3D144 valign=3Dtop style=3D'width:1.5in;padding:0in 5.4pt 0in 5=
.4pt;
  height:9.25pt'>
  <p class=3DMsoHeader style=3D'margin-top:2.4pt;margin-right:0in;margin-bo=
ttom:
  2.4pt;margin-left:0in;mso-para-margin-top:.2gd;mso-para-margin-right:0in;
  mso-para-margin-bottom:.2gd;mso-para-margin-left:0in;tab-stops:.5in cente=
r 3.0in right 6.0in'><span
  lang=3DEN-GB style=3D'font-size:12.0pt;mso-fareast-font-family:PMingLiU;
  mso-fareast-language:ZH-TW'><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=3D168 valign=3Dtop style=3D'width:1.75in;padding:0in 5.4pt 0in =
5.4pt;
  height:9.25pt'>
  <p class=3DMsoHeader style=3D'margin-top:2.4pt;margin-right:0in;margin-bo=
ttom:
  2.4pt;margin-left:0in;mso-para-margin-top:.2gd;mso-para-margin-right:0in;
  mso-para-margin-bottom:.2gd;mso-para-margin-left:0in;tab-stops:.5in cente=
r 3.0in right 6.0in'><span
  lang=3DEN-GB style=3D'font-size:12.0pt;mso-fareast-font-family:PMingLiU;
  mso-fareast-language:ZH-TW'><o:p>&nbsp;</o:p></span></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:5'>
  <td width=3D732 colspan=3D4 valign=3Dtop style=3D'width:549.0pt;padding:0=
in 5.4pt 0in 5.4pt'>
  <p class=3DMsoHeader style=3D'tab-stops:.5in center 3.0in right 6.0in'><i
  style=3D'mso-bidi-font-style:normal'><span lang=3DEN-GB style=3D'mso-bidi=
-font-weight:
  bold'>I am aware that cheating and plagiarism will not be tolerated in any
  assignment</span></i><i style=3D'mso-bidi-font-style:normal'><span lang=
=3DEN-GB
  style=3D'mso-fareast-font-family:PMingLiU;mso-fareast-language:ZH-TW;
  mso-bidi-font-weight:bold'> and I hereby declare that this is all my own
  work.</span></i><span lang=3DEN-GB style=3D'font-size:12.0pt;mso-fareast-=
font-family:
  PMingLiU;mso-fareast-language:ZH-TW'><o:p></o:p></span></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:6;mso-yfti-lastrow:yes;height:17.55pt'>
  <td width=3D120 valign=3Dtop style=3D'width:1.25in;padding:0in 5.4pt 0in =
5.4pt;
  height:17.55pt'>
  <p class=3DMsoHeader style=3D'tab-stops:.5in center 3.0in right 6.0in'><s=
pan
  lang=3DEN-GB style=3D'font-size:12.0pt;mso-fareast-font-family:PMingLiU;
  mso-fareast-language:ZH-TW'><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=3D300 valign=3Dtop style=3D'width:225.0pt;padding:0in 5.4pt 0in=
 5.4pt;
  height:17.55pt'>
  <p class=3DMsoHeader style=3D'tab-stops:.5in center 3.0in right 6.0in'><s=
pan
  lang=3DEN-GB style=3D'font-size:12.0pt;mso-fareast-font-family:PMingLiU;
  mso-fareast-language:ZH-TW'><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=3D144 valign=3Dtop style=3D'width:1.5in;padding:0in 5.4pt 0in 5=
.4pt;
  height:17.55pt'>
  <p class=3DMsoHeader style=3D'margin-top:6.0pt;tab-stops:.5in center 3.0i=
n right 6.0in'><i
  style=3D'mso-bidi-font-style:normal'><span lang=3DEN-GB style=3D'font-siz=
e:11.0pt;
  mso-fareast-font-family:PMingLiU;mso-fareast-language:ZH-TW'>Student&#821=
7;s
  Signature:<o:p></o:p></span></i></p>
  </td>
  <td width=3D168 valign=3Dtop style=3D'width:1.75in;border:none;border-bot=
tom:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;
  height:17.55pt'>
  <p class=3DMsoHeader style=3D'tab-stops:.5in center 3.0in right 6.0in'><s=
pan
  lang=3DEN-GB style=3D'font-size:12.0pt;mso-fareast-font-family:PMingLiU;
  mso-fareast-language:ZH-TW'><o:p>&nbsp;</o:p></span></p>
  </td>
 </tr>
</table>

<p class=3DMsoNormal style=3D'margin-bottom:6.0pt'><b style=3D'mso-bidi-fon=
t-weight:
normal'><span lang=3DEN-GB style=3D'mso-fareast-font-family:PMingLiU;mso-fa=
reast-language:
ZH-TW'>Assessor&#8217;s comment on learning</span><span lang=3DEN-GB> outco=
mes</span></b><b
style=3D'mso-bidi-font-weight:normal'><span lang=3DEN-GB style=3D'mso-farea=
st-font-family:
PMingLiU;mso-fareast-language:ZH-TW'> of this assessment</span></b><b
style=3D'mso-bidi-font-weight:normal'><span lang=3DEN-GB style=3D'font-fami=
ly:"Arial","sans-serif";
mso-fareast-font-family:PMingLiU;mso-fareast-language:ZH-TW'><o:p></o:p></s=
pan></b></p>

<table class=3DMsoNormalTable border=3D1 cellspacing=3D0 cellpadding=3D0 wi=
dth=3D"105%"
 style=3D'width:105.18%;margin-left:-12.6pt;border-collapse:collapse;border=
:none;
 mso-border-alt:solid windowtext .5pt;mso-yfti-tbllook:1184;mso-padding-alt:
 0in 5.4pt 0in 5.4pt;mso-border-insideh:.5pt solid windowtext;mso-border-in=
sidev:
 .5pt solid windowtext'>
 <tr style=3D'mso-yfti-irow:0;mso-yfti-firstrow:yes;page-break-inside:avoid;
  height:19.05pt'>
  <td width=3D"14%" valign=3Dtop style=3D'width:14.9%;border:solid windowte=
xt 1.0pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:1=
9.05pt'>
  <p class=3DMsoNormal align=3Dcenter style=3D'margin-top:10.0pt;margin-rig=
ht:0in;
  margin-bottom:6.0pt;margin-left:0in;text-align:center'><b style=3D'mso-bi=
di-font-weight:
  normal'><span lang=3DEN-GB style=3D'font-size:8.0pt;mso-bidi-font-size:12=
.0pt;
  font-family:"Arial","sans-serif"'>Outcome<o:p></o:p></span></b></p>
  </td>
  <td width=3D"30%" valign=3Dtop style=3D'width:30.98%;border:solid windowt=
ext 1.0pt;
  border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:19.05pt'>
  <h2 style=3D'margin-top:10.0pt'><span lang=3DEN-GB style=3D'font-family:"=
Arial","sans-serif"'>Evidence
  assessment criteria<o:p></o:p></span></h2>
  </td>
  <td width=3D"43%" valign=3Dtop style=3D'width:43.46%;border:solid windowt=
ext 1.0pt;
  border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:19.05pt'>
  <p class=3DMsoNormal align=3Dcenter style=3D'margin-top:10.0pt;margin-rig=
ht:0in;
  margin-bottom:6.0pt;margin-left:0in;text-align:center'><b style=3D'mso-bi=
di-font-weight:
  normal'><span lang=3DEN-GB style=3D'font-size:8.0pt;mso-bidi-font-size:12=
.0pt;
  font-family:"Arial","sans-serif";mso-fareast-font-family:PMingLiU;mso-far=
east-language:
  ZH-TW'>Assessor&#8217;s </span></b><b style=3D'mso-bidi-font-weight:norma=
l'><span
  lang=3DEN-GB style=3D'font-size:8.0pt;mso-bidi-font-size:12.0pt;font-fami=
ly:"Arial","sans-serif"'>Feedback<o:p></o:p></span></b></p>
  </td>
  <td width=3D"10%" valign=3Dtop style=3D'width:10.66%;border:solid windowt=
ext 1.0pt;
  border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:19.05pt'>
  <p class=3DMsoNormal align=3Dcenter style=3D'margin-top:6.0pt;margin-righ=
t:0in;
  margin-bottom:6.0pt;margin-left:0in;text-align:center'><b style=3D'mso-bi=
di-font-weight:
  normal'><span lang=3DEN-GB style=3D'font-size:8.0pt;mso-bidi-font-size:12=
.0pt;
  font-family:"Arial","sans-serif"'>Assessor&#8217;s decision<o:p></o:p></s=
pan></b></p>
  </td>
 </tr>
 @foreach($course->module as $keyModule => $module)
 	<?php $userTaskIds = app(\VuleApps\LwcPortal\Repository\PortalRepository::class)->groupTaskIDsByModule($module->id); ?>
 	<?php $userTask = \VuleApps\LwcPortal\Models\UserTask::where('user_id', $user->id)->whereIn('task_id', $userTaskIds)->with('task')->get();?>
 	@foreach($userTask as $keyTask => $user_task)
 		@if($keyTask == 0)
 <tr style=3D'mso-yfti-irow:1;page-break-inside:avoid;height:33.6pt'>
  <td width=3D"14%" rowspan=3D{{ count($userTask) }} valign=3Dtop style=3D'width:14.9%;border:so=
lid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:s=
olid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:33.6pt'>
  <p class=3DMsoNormal style=3D'mso-layout-grid-align:none;text-autospace:n=
one'><span
  lang=3DEN-SG style=3D'font-size:10.0pt;mso-fareast-font-family:SimSun;mso=
-ansi-language:
  EN-SG;mso-fareast-language:ZH-CN'>LO{{ $module->id }} <o:p></o:p></span></p>
  <p class=3DMsoNormal style=3D'mso-layout-grid-align:none;text-autospace:n=
one'><span
  lang=3DEN-SG style=3D'font-size:10.0pt;mso-fareast-font-family:SimSun;mso=
-ansi-language:
  EN-SG;mso-fareast-language:ZH-CN'>{{ $module->title }}<o:p></o:p></span></p>
  <p class=3DMsoNormal style=3D'margin-top:6.0pt;margin-right:0in;margin-bo=
ttom:
  6.0pt;margin-left:0in;mso-para-margin-top:.5gd;mso-para-margin-right:0in;
  mso-para-margin-bottom:6.0pt;mso-para-margin-left:0in'><span lang=3DEN-GB
  style=3D'font-size:9.0pt;mso-bidi-font-size:12.0pt;font-family:"Arial","s=
ans-serif"'><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=3D"30%" valign=3Dtop style=3D'width:30.98%;border-top:none;bord=
er-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1=
.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid window=
text .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:3=
3.6pt'>
  <p class=3DMsoNormal style=3D'margin-top:6.0pt;margin-right:0in;margin-bo=
ttom:
  6.0pt;margin-left:0in;tab-stops:19.25pt'><span lang=3DEN-GB style=3D'font=
-size:
  10.0pt'>{{ $keyModule + 1 }}.{{ $keyTask + 1 }}<span style=3D'mso-spacerun:yes'>&nbsp;&nbsp; </span>{{ $user_task->task->title }}<o:p></o:p></spa=
n></p>
  </td>
  <td width=3D"43%" valign=3Dtop style=3D'width:43.46%;border-top:none;bord=
er-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1=
.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid window=
text .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:3=
3.6pt'>
  <p class=3DMsoNormal style=3D'margin-top:6.0pt;margin-right:0in;margin-bo=
ttom:
  6.0pt;margin-left:0in;line-height:10.0pt;mso-line-height-rule:exactly'><s=
pan
  lang=3DEN-GB style=3D'font-size:9.0pt;mso-bidi-font-size:12.0pt;font-fami=
ly:"Arial Narrow","sans-serif";
  mso-fareast-font-family:PMingLiU;mso-fareast-language:ZH-TW;mso-bidi-font=
-weight:
  bold'>{{ $user_task->teacher_comment }}<o:p></o:p></span></p>
  </td>
  <td width=3D"10%" valign=3Dtop style=3D'width:10.66%;border-top:none;bord=
er-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1=
.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid window=
text .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:3=
3.6pt'>
  <p class=3DMsoNormal align=3Dcenter style=3D'margin-top:12.0pt;mso-para-m=
argin-top:
  1.0gd;text-align:center'><span lang=3DEN-GB style=3D'font-size:10.0pt;fon=
t-family:
  "Helvetica","sans-serif";mso-bidi-font-family:"Times New Roman"'>{{ $user_task->grade == 'pass' ? 'P' : 'R' }}<o:p></o:p></span></p>
  </td>
 </tr>
        @else
 <tr style=3D'mso-yfti-irow:2;page-break-inside:avoid;height:37.3pt'>
  <td width=3D"30%" valign=3Dtop style=3D'width:30.98%;border-top:none;bord=
er-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1=
.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid window=
text .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:3=
7.3pt'>
  <p class=3DMsoNormal style=3D'mso-layout-grid-align:none;text-autospace:n=
one'><span
  lang=3DEN-SG style=3D'font-size:10.0pt;mso-fareast-font-family:SimSun;mso=
-ansi-language:
  EN-SG;mso-fareast-language:ZH-CN'>{{ $keyModule + 1 }}.{{ $keyTask + 1 }} {{ $user_task->task->title }}<o:p></o:p></span></p>
  </td>
  <td width=3D"43%" valign=3Dtop style=3D'width:43.46%;border-top:none;bord=
er-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1=
.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid window=
text .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:3=
7.3pt'>
  <p class=3DMsoNormal style=3D'line-height:10.0pt;mso-line-height-rule:exa=
ctly'><span
  lang=3DEN-GB style=3D'font-size:9.0pt;mso-bidi-font-size:12.0pt;font-fami=
ly:"Arial Narrow","sans-serif"'>{{  $user_task->teacher_comment }}<o:p>&nbsp;</o:p></span></p>
  <p class=3DMsoNormal style=3D'line-height:10.0pt;mso-line-height-rule:exa=
ctly'><span
  lang=3DEN-GB style=3D'font-size:9.0pt;mso-bidi-font-size:12.0pt;font-fami=
ly:"Arial Narrow","sans-serif"'><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=3D"10%" valign=3Dtop style=3D'width:10.66%;border-top:none;bord=
er-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1=
.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid window=
text .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:3=
7.3pt'>
  <p class=3DMsoNormal align=3Dcenter style=3D'margin-top:12.0pt;text-align=
:center'><span
  lang=3DEN-GB style=3D'font-size:10.0pt;font-family:"Helvetica","sans-seri=
f";
  mso-bidi-font-family:"Times New Roman"'>{{ $user_task->grade == 'pass' ? 'P' : 'R' }}</span></p>
  </td>
 </tr>
        @endif
    @endforeach
 @endforeach
</table>

<p class=3DMsoNormal><b><span style=3D'font-family:"Arial","sans-serif";mso=
-fareast-font-family:
PMingLiU;mso-ansi-language:EN-US;mso-fareast-language:ZH-TW'><o:p>&nbsp;</o=
:p></span></b></p>

<table class=3DMsoNormalTable border=3D1 cellspacing=3D0 cellpadding=3D0 wi=
dth=3D"105%"
 style=3D'width:105.18%;margin-left:-12.6pt;border-collapse:collapse;border=
:none;
 mso-border-alt:solid windowtext .5pt;mso-yfti-tbllook:1184;mso-padding-alt:
 0in 5.4pt 0in 5.4pt;mso-border-insideh:.5pt solid windowtext;mso-border-in=
sidev:
 .5pt solid windowtext'>
 <tr style=3D'mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes;
  page-break-inside:avoid;height:23.25pt'>
  <td width=3D"42%" valign=3Dtop style=3D'width:42.62%;border:solid windowt=
ext 1.0pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:2=
3.25pt'>
  <h5 style=3D'margin-top:6.0pt;margin-right:0in;margin-bottom:6.0pt;margin=
-left:
  0in'><span style=3D'font-family:"Arial","sans-serif";mso-fareast-font-fam=
ily:
  PMingLiU;mso-ansi-language:EN-US;mso-fareast-language:ZH-TW'>Decision for
  PASS level<o:p></o:p></span></h5>
  </td>
  <td width=3D"57%" valign=3Dtop style=3D'width:57.38%;border:solid windowt=
ext 1.0pt;
  border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:23.25pt'>
  <h5 style=3D'margin-top:6.0pt;margin-right:0in;margin-bottom:6.0pt;margin=
-left:
  0in;text-indent:10.0pt;mso-char-indent-count:1.0'><span style=3D'mso-fare=
ast-font-family:
  PMingLiU;mso-ansi-language:EN-US;mso-fareast-language:ZH-TW'>Grade:<span
  style=3D'mso-spacerun:yes'>&nbsp;&nbsp; </span></span><span style=3D'mso-=
fareast-font-family:
  PMingLiU;mso-ansi-language:EN-US;mso-fareast-language:ZH-TW;font-weight:n=
ormal'>(Please
  tick </span><span style=3D'font-size:11.0pt;font-family:Wingdings;mso-asc=
ii-font-family:
  Helvetica;mso-fareast-font-family:PMingLiU;mso-hansi-font-family:Helvetic=
a;
  mso-ansi-language:EN-US;mso-fareast-language:ZH-TW;mso-char-type:symbol;
  mso-symbol-font-family:Wingdings;font-weight:normal'><span style=3D'mso-c=
har-type:
  symbol;mso-symbol-font-family:Wingdings'>&uuml;</span></span><span
  style=3D'mso-fareast-font-family:PMingLiU;mso-ansi-language:EN-US;mso-far=
east-language:
  ZH-TW;font-weight:normal'>)</span><span style=3D'mso-fareast-font-family:=
PMingLiU;
  mso-ansi-language:EN-US;mso-fareast-language:ZH-TW'><o:p></o:p></span></h=
5>
  <h5 style=3D'margin-top:8.0pt;margin-right:0in;margin-bottom:6.0pt;margin=
-left:
  0in;mso-para-margin-top:8.0pt;mso-para-margin-right:0in;mso-para-margin-b=
ottom:
  .5gd;mso-para-margin-left:0in;text-indent:7.0pt;mso-char-indent-count:.5'=
><span
  style=3D'font-size:14.0pt;font-family:Wingdings;mso-ascii-font-family:Hel=
vetica;
  mso-fareast-font-family:PMingLiU;mso-hansi-font-family:Helvetica;mso-ansi=
-language:
  EN-US;mso-fareast-language:ZH-TW;mso-char-type:symbol;mso-symbol-font-fam=
ily:
  Wingdings;font-weight:normal'><span style=3D'mso-char-type:symbol;mso-sym=
bol-font-family:
  Wingdings'>&uml;</span></span><span style=3D'mso-fareast-font-family:PMin=
gLiU;
  mso-ansi-language:EN-US;mso-fareast-language:ZH-TW;font-weight:normal'> <=
/span><span
  style=3D'mso-fareast-font-family:PMingLiU;mso-ansi-language:EN-US;mso-far=
east-language:
  ZH-TW'>REFER</span><span style=3D'font-size:11.0pt;mso-fareast-font-famil=
y:
  PMingLiU;mso-ansi-language:EN-US;mso-fareast-language:ZH-TW'> </span><span
  style=3D'mso-fareast-font-family:PMingLiU;mso-ansi-language:EN-US;mso-far=
east-language:
  ZH-TW'><span style=3D'mso-spacerun:yes'>&nbsp;&nbsp;</span></span><span
  style=3D'mso-fareast-font-family:PMingLiU;mso-ansi-language:EN-US;mso-far=
east-language:
  ZH-TW;font-weight:normal'>-<span style=3D'mso-spacerun:yes'>&nbsp;&nbsp;
  </span>Those with &#8220;R&#8221; needed to redo.<o:p></o:p></span></h5>
  <h5 style=3D'margin-top:8.0pt;margin-right:0in;margin-bottom:6.0pt;margin=
-left:
  0in;mso-para-margin-top:8.0pt;mso-para-margin-right:0in;mso-para-margin-b=
ottom:
  .5gd;mso-para-margin-left:0in;text-indent:7.0pt;mso-char-indent-count:.5'=
><span
  style=3D'font-size:14.0pt;font-family:Wingdings;mso-ascii-font-family:Hel=
vetica;
  mso-fareast-font-family:PMingLiU;mso-hansi-font-family:Helvetica;mso-ansi=
-language:
  EN-US;mso-fareast-language:ZH-TW;mso-char-type:symbol;mso-symbol-font-fam=
ily:
  Wingdings;font-weight:normal'><span style=3D'mso-char-type:symbol;mso-sym=
bol-font-family:
  Wingdings'>&uml;</span></span><span style=3D'mso-fareast-font-family:PMin=
gLiU;
  mso-ansi-language:EN-US;mso-fareast-language:ZH-TW;font-weight:normal'> <=
/span><span
  style=3D'mso-fareast-font-family:PMingLiU;mso-ansi-language:EN-US;mso-far=
east-language:
  ZH-TW'>PASS </span><span style=3D'mso-fareast-font-family:PMingLiU;mso-an=
si-language:
  EN-US;mso-fareast-language:ZH-TW;font-weight:normal'><span
  style=3D'mso-spacerun:yes'>&nbsp;&nbsp;&nbsp;&nbsp;</span>-<span
  style=3D'mso-spacerun:yes'>&nbsp;&nbsp;&nbsp; </span>All the above assess=
ment
  criteria fulfilled.<o:p></o:p></span></h5>
  </td>
 </tr>
</table>

<p class=3DMsoNormal><b><span style=3D'font-family:"Arial","sans-serif";mso=
-fareast-font-family:
PMingLiU;mso-ansi-language:EN-US;mso-fareast-language:ZH-TW'><o:p>&nbsp;</o=
:p></span></b></p>

<table class=3DMsoNormalTable border=3D1 cellspacing=3D0 cellpadding=3D0 wi=
dth=3D"105%"
 style=3D'width:105.18%;margin-left:-12.6pt;border-collapse:collapse;border=
:none;
 mso-border-alt:solid windowtext .5pt;mso-yfti-tbllook:1184;mso-padding-alt:
 0in 5.4pt 0in 5.4pt;mso-border-insideh:.5pt solid windowtext;mso-border-in=
sidev:
 .5pt solid windowtext'>
 <tr style=3D'mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes;
  page-break-inside:avoid;height:24.8pt'>
  <td width=3D"42%" valign=3Dtop style=3D'width:42.62%;border:solid windowt=
ext 1.0pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:2=
4.8pt'>
  <h5 style=3D'margin-top:6.0pt'><span style=3D'font-family:"Arial","sans-s=
erif";
  mso-fareast-font-family:PMingLiU;mso-ansi-language:EN-US;mso-fareast-lang=
uage:
  ZH-TW'>For re-submitted or supplementary submission after referral </span=
><span
  style=3D'font-family:"Arial","sans-serif";mso-fareast-font-family:PMingLi=
U;
  mso-ansi-language:EN-US;mso-fareast-language:ZH-TW;font-weight:normal'>(If
  applicable)</span><span style=3D'font-family:"Arial","sans-serif";mso-far=
east-font-family:
  PMingLiU;mso-ansi-language:EN-US;mso-fareast-language:ZH-TW'><o:p></o:p><=
/span></h5>
  </td>
  <td width=3D"57%" valign=3Dtop style=3D'width:57.38%;border:solid windowt=
ext 1.0pt;
  border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:24.8pt'>
  <p class=3DMsoNormal style=3D'margin-top:6.0pt'><span style=3D'font-size:=
10.0pt;
  font-family:"Arial","sans-serif";mso-fareast-font-family:PMingLiU;mso-ans=
i-language:
  EN-US;mso-fareast-language:ZH-TW'>Those referred parts have been fulfille=
d.</span><span
  style=3D'font-family:"Arial","sans-serif";mso-fareast-font-family:PMingLi=
U;
  mso-ansi-language:EN-US;mso-fareast-language:ZH-TW'><span
  style=3D'mso-spacerun:yes'>&nbsp; </span></span><span style=3D'font-famil=
y:Wingdings;
  mso-ascii-font-family:"Times New Roman";mso-fareast-font-family:PMingLiU;
  mso-hansi-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-farea=
st-language:
  ZH-TW;mso-char-type:symbol;mso-symbol-font-family:Wingdings'><span
  style=3D'mso-char-type:symbol;mso-symbol-font-family:Wingdings'>&uml;</sp=
an></span><span
  style=3D'mso-fareast-font-family:PMingLiU;mso-ansi-language:EN-US;mso-far=
east-language:
  ZH-TW'> </span><b style=3D'mso-bidi-font-weight:normal'><span style=3D'fo=
nt-size:
  10.0pt;font-family:"Arial","sans-serif";mso-fareast-font-family:PMingLiU;
  mso-ansi-language:EN-US;mso-fareast-language:ZH-TW'>PASS </span></b><b
  style=3D'mso-bidi-font-weight:normal'><span style=3D'font-size:11.0pt;fon=
t-family:
  "Arial","sans-serif";mso-fareast-font-family:PMingLiU;mso-ansi-language:E=
N-US;
  mso-fareast-language:ZH-TW'><o:p></o:p></span></b></p>
  <p class=3DMsoNormal style=3D'margin-top:6.0pt;margin-right:0in;margin-bo=
ttom:
  6.0pt;margin-left:0in;mso-para-margin-top:6.0pt;mso-para-margin-right:0in;
  mso-para-margin-bottom:.5gd;mso-para-margin-left:0in'><span style=3D'font=
-size:
  10.0pt;font-family:"Arial","sans-serif";mso-fareast-font-family:PMingLiU;
  mso-ansi-language:EN-US;mso-fareast-language:ZH-TW'>Those referred parts
  still NOT fulfilled.</span><span style=3D'font-size:11.0pt;font-family:"A=
rial","sans-serif";
  mso-fareast-font-family:PMingLiU;mso-ansi-language:EN-US;mso-fareast-lang=
uage:
  ZH-TW'><span style=3D'mso-spacerun:yes'>&nbsp;&nbsp;&nbsp;&nbsp; </span><=
/span><span
  style=3D'font-family:Wingdings;mso-ascii-font-family:"Times New Roman";
  mso-fareast-font-family:PMingLiU;mso-hansi-font-family:"Times New Roman";
  mso-ansi-language:EN-US;mso-fareast-language:ZH-TW;mso-char-type:symbol;
  mso-symbol-font-family:Wingdings'><span style=3D'mso-char-type:symbol;
  mso-symbol-font-family:Wingdings'>&uml;</span></span><b style=3D'mso-bidi=
-font-weight:
  normal'><span style=3D'mso-fareast-font-family:PMingLiU;mso-ansi-language=
:EN-US;
  mso-fareast-language:ZH-TW'> </span></b><b style=3D'mso-bidi-font-weight:=
normal'><span
  style=3D'font-size:10.0pt;font-family:"Arial","sans-serif";mso-fareast-fo=
nt-family:
  PMingLiU;mso-ansi-language:EN-US;mso-fareast-language:ZH-TW'>REFER</span>=
</b><span
  style=3D'font-family:"Arial","sans-serif";mso-fareast-font-family:PMingLi=
U;
  mso-ansi-language:EN-US;mso-fareast-language:ZH-TW'><o:p></o:p></span></p>
  </td>
 </tr>
</table>

<p class=3DMsoNormal><b><span style=3D'font-family:"Arial","sans-serif";mso=
-fareast-font-family:
PMingLiU;mso-ansi-language:EN-US;mso-fareast-language:ZH-TW'><o:p>&nbsp;</o=
:p></span></b></p>

<table class=3DMsoNormalTable border=3D1 cellspacing=3D0 cellpadding=3D0 wi=
dth=3D732
 style=3D'width:549.0pt;margin-left:-12.6pt;border-collapse:collapse;border=
:none;
 mso-border-alt:solid windowtext .5pt;mso-yfti-tbllook:480;mso-padding-alt:
 0in 5.4pt 0in 5.4pt;mso-border-insideh:.5pt solid windowtext;mso-border-in=
sidev:
 .5pt solid windowtext'>
 <tr style=3D'mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes'>
  <td width=3D732 valign=3Dtop style=3D'width:549.0pt;border:solid windowte=
xt 1.0pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=3DMsoNormal><b><span lang=3DEN-GB style=3D'font-size:10.0pt;font=
-family:
  "Arial","sans-serif"'><o:p>&nbsp;</o:p></span></b></p>
  <p class=3DMsoNormal style=3D'margin-bottom:6.0pt'><b><span lang=3DEN-GB
  style=3D'font-size:10.0pt;font-family:"Arial","sans-serif"'>Decision for =
MERIT </span></b><i
  style=3D'mso-bidi-font-style:normal'><span lang=3DEN-GB style=3D'font-siz=
e:10.0pt;
  font-family:"Arial","sans-serif";mso-bidi-font-weight:bold'>(Award only w=
hen a
  PASS is achieved plus ALL the following evidences are fulfilled &amp;
  checked)</span></i><b><span lang=3DEN-GB style=3D'font-size:10.0pt;font-f=
amily:
  "Arial","sans-serif"'><o:p></o:p></span></b></p>
  <p class=3DMsoNormal style=3D'margin-bottom:6.0pt'><span lang=3DEN-GB
  style=3D'font-size:10.0pt;font-family:"Arial","sans-serif";mso-bidi-font-=
weight:
  bold'>The learner must demonstrate all the evidence of the following to a=
ward
  a Merit:<o:p></o:p></span></p>
  <p class=3DMsoNormal style=3D'margin-bottom:6.0pt'><span lang=3DEN-GB
  style=3D'font-size:10.0pt;font-family:"Arial","sans-serif";mso-bidi-font-=
weight:
  bold'>M1.<span style=3D'mso-spacerun:yes'>&nbsp; </span>Identify and apply
  strategies to find appropriate solutions:<o:p></o:p></span></p>
  <p class=3DMsoNormal style=3D'margin-top:0in;margin-right:0in;margin-bott=
om:6.0pt;
  margin-left:.55in;text-indent:-22.5pt;mso-list:l5 level1 lfo2'><![if !sup=
portLists]><span
  lang=3DEN-GB style=3D'font-size:14.0pt;mso-bidi-font-size:10.0pt;font-fam=
ily:
  Wingdings;mso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingding=
s;
  mso-bidi-font-weight:bold'><span style=3D'mso-list:Ignore'>&uml;<span
  style=3D'font:7.0pt "Times New Roman"'> </span></span></span><![endif]><s=
pan
  lang=3DEN-GB style=3D'font-size:10.0pt;font-family:"Arial","sans-serif";
  mso-bidi-font-weight:bold'>Effective judgements have been made.<o:p></o:p=
></span></p>
  <p class=3DMsoNormal style=3D'margin-top:0in;margin-right:0in;margin-bott=
om:6.0pt;
  margin-left:.55in;text-indent:-22.5pt;mso-list:l5 level1 lfo2'><![if !sup=
portLists]><span
  lang=3DEN-GB style=3D'font-size:14.0pt;mso-bidi-font-size:10.0pt;font-fam=
ily:
  Wingdings;mso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingding=
s;
  mso-bidi-font-weight:bold'><span style=3D'mso-list:Ignore'>&uml;<span
  style=3D'font:7.0pt "Times New Roman"'> </span></span></span><![endif]><s=
pan
  lang=3DEN-GB style=3D'font-size:10.0pt;font-family:"Arial","sans-serif";
  mso-bidi-font-weight:bold'>An effective approach to study and research has
  been applied.<span style=3D'mso-tab-count:3'>&nbsp;&nbsp;&nbsp;&nbsp;&nbs=
p;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&=
nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><o:p></o:p></span></=
p>
  <p class=3DMsoNormal style=3D'margin-bottom:6.0pt'><span lang=3DEN-GB
  style=3D'font-size:10.0pt;font-family:"Arial","sans-serif";mso-bidi-font-=
weight:
  bold'>M2.<span style=3D'mso-spacerun:yes'>&nbsp; </span>Elect/design and =
apply
  appropriate methods/ techniques.<o:p></o:p></span></p>
  <p class=3DMsoNormal style=3D'margin-top:0in;margin-right:0in;margin-bott=
om:6.0pt;
  margin-left:.55in;text-indent:-22.5pt;mso-list:l3 level1 lfo4'><![if !sup=
portLists]><span
  lang=3DEN-GB style=3D'font-size:14.0pt;mso-bidi-font-size:10.0pt;font-fam=
ily:
  Wingdings;mso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingding=
s;
  mso-bidi-font-weight:bold'><span style=3D'mso-list:Ignore'>&uml;<span
  style=3D'font:7.0pt "Times New Roman"'> </span></span></span><![endif]><s=
pan
  lang=3DEN-GB style=3D'font-size:10.0pt;font-family:"Arial","sans-serif";
  mso-bidi-font-weight:bold'>Relevant theories and techniques have been
  applied.<span style=3D'mso-tab-count:3'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nb=
sp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><=
o:p></o:p></span></p>
  <p class=3DMsoNormal style=3D'margin-top:0in;margin-right:0in;margin-bott=
om:6.0pt;
  margin-left:.55in;text-indent:-22.5pt;mso-list:l3 level1 lfo4'><![if !sup=
portLists]><span
  lang=3DEN-GB style=3D'font-size:14.0pt;mso-bidi-font-size:10.0pt;font-fam=
ily:
  Wingdings;mso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingding=
s;
  mso-bidi-font-weight:bold'><span style=3D'mso-list:Ignore'>&uml;<span
  style=3D'font:7.0pt "Times New Roman"'> </span></span></span><![endif]><s=
pan
  lang=3DEN-GB style=3D'font-size:10.0pt;font-family:"Arial","sans-serif";
  mso-bidi-font-weight:bold'>Appropriate learning methods/techniques have b=
een
  applied.<span style=3D'mso-tab-count:4'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nb=
sp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nb=
sp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><o:p=
></o:p></span></p>
  <p class=3DMsoNormal style=3D'margin-bottom:6.0pt'><span lang=3DEN-GB
  style=3D'font-size:10.0pt;font-family:"Arial","sans-serif";mso-bidi-font-=
weight:
  bold'>M3.<span style=3D'mso-spacerun:yes'>&nbsp; </span>Present and commu=
nicate
  appropriate findings.<o:p></o:p></span></p>
  <p class=3DMsoNormal style=3D'margin-top:0in;margin-right:0in;margin-bott=
om:6.0pt;
  margin-left:.55in;text-indent:-21.75pt;mso-list:l2 level1 lfo6;tab-stops:
  list 35.1pt 57.75pt'><![if !supportLists]><span lang=3DEN-GB style=3D'fon=
t-size:
  14.0pt;mso-bidi-font-size:10.0pt;font-family:Wingdings;mso-fareast-font-f=
amily:
  Wingdings;mso-bidi-font-family:Wingdings;mso-bidi-font-weight:bold'><span
  style=3D'mso-list:Ignore'>&uml;<span style=3D'font:7.0pt "Times New Roman=
"'> </span></span></span><![endif]><span
  lang=3DEN-GB style=3D'font-size:10.0pt;font-family:"Arial","sans-serif";
  mso-bidi-font-weight:bold'>The appropriate structure and approach has been
  used.<span style=3D'mso-tab-count:5'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nb=
sp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nb=
sp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><o:p></o:p></span></p>
  <p class=3DMsoNormal style=3D'margin-top:0in;margin-right:0in;margin-bott=
om:6.0pt;
  margin-left:.55in;text-indent:-21.75pt;mso-list:l2 level1 lfo6;tab-stops:
  list 35.1pt 57.75pt'><![if !supportLists]><span lang=3DEN-GB style=3D'fon=
t-size:
  14.0pt;mso-bidi-font-size:10.0pt;font-family:Wingdings;mso-fareast-font-f=
amily:
  Wingdings;mso-bidi-font-family:Wingdings;mso-bidi-font-weight:bold'><span
  style=3D'mso-list:Ignore'>&uml;<span style=3D'font:7.0pt "Times New Roman=
"'> </span></span></span><![endif]><span
  lang=3DEN-GB style=3D'font-size:10.0pt;font-family:"Arial","sans-serif";
  mso-bidi-font-weight:bold'>Coherent, logical development of
  principles/concepts.<span style=3D'mso-tab-count:2'>&nbsp;&nbsp;&nbsp;&nb=
sp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </spa=
n><o:p></o:p></span></p>
  <h5 style=3D'margin-top:0in;margin-right:0in;margin-bottom:6.0pt;margin-l=
eft:
  .5in;text-indent:-.25in;mso-list:l6 level1 lfo8'><![if !supportLists]><sp=
an
  style=3D'font-size:14.0pt;mso-bidi-font-size:10.0pt;font-family:Wingdings;
  mso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingdings;mso-ansi=
-language:
  EN-US;mso-fareast-language:ZH-TW;font-weight:normal'><span style=3D'mso-l=
ist:
  Ignore'>&uml;<span style=3D'font:7.0pt "Times New Roman"'> </span></span>=
</span><![endif]><span
  style=3D'font-family:"Arial","sans-serif";mso-fareast-font-family:PMingLi=
U;
  mso-ansi-language:EN-US;mso-fareast-language:ZH-TW'>MERIT</span><span
  style=3D'mso-fareast-font-family:PMingLiU;mso-ansi-language:EN-US;mso-far=
east-language:
  ZH-TW'> - </span><span style=3D'font-family:"Arial","sans-serif";mso-fare=
ast-font-family:
  PMingLiU;mso-ansi-language:EN-US;mso-fareast-language:ZH-TW'>All the above
  indicative characteristics are evident (Please tick </span><span
  style=3D'font-family:Wingdings;mso-ascii-font-family:Arial;mso-fareast-fo=
nt-family:
  PMingLiU;mso-hansi-font-family:Arial;mso-bidi-font-family:Arial;mso-ansi-=
language:
  EN-US;mso-fareast-language:ZH-TW;mso-char-type:symbol;mso-symbol-font-fam=
ily:
  Wingdings'><span style=3D'mso-char-type:symbol;mso-symbol-font-family:Win=
gdings'>&uuml;</span></span><span
  style=3D'font-family:"Arial","sans-serif";mso-fareast-font-family:PMingLi=
U;
  mso-ansi-language:EN-US;mso-fareast-language:ZH-TW'> if applicable)</span=
><span
  style=3D'mso-fareast-font-family:PMingLiU;mso-ansi-language:EN-US;mso-far=
east-language:
  ZH-TW'><span style=3D'mso-spacerun:yes'>&nbsp;&nbsp; </span><o:p></o:p></=
span></h5>
  </td>
 </tr>
</table>

<p class=3DMsoNormal style=3D'margin-left:-27.0pt;mso-para-margin-left:-2.2=
5gd'><b><span
lang=3DEN-GB style=3D'font-size:10.0pt;font-family:"Arial","sans-serif"'><o=
:p>&nbsp;</o:p></span></b></p>

<table class=3DMsoNormalTable border=3D1 cellspacing=3D0 cellpadding=3D0 wi=
dth=3D732
 style=3D'width:549.0pt;margin-left:-12.6pt;border-collapse:collapse;border=
:none;
 mso-border-alt:solid windowtext .5pt;mso-yfti-tbllook:480;mso-padding-alt:
 0in 5.4pt 0in 5.4pt;mso-border-insideh:.5pt solid windowtext;mso-border-in=
sidev:
 .5pt solid windowtext'>
 <tr style=3D'mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes;
  height:269.5pt'>
  <td width=3D732 valign=3Dtop style=3D'width:549.0pt;border:solid windowte=
xt 1.0pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:2=
69.5pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:6.0pt;mso-pagination:none;mso=
-layout-grid-align:
  none;text-autospace:none'><b><span style=3D'font-size:10.0pt;font-family:=
"Arial","sans-serif";
  mso-fareast-font-family:PMingLiU;mso-ansi-language:EN-US;mso-fareast-lang=
uage:
  ZH-TW'>Decision for DISTINCTION </span></b><i style=3D'mso-bidi-font-styl=
e:
  normal'><span lang=3DEN-GB style=3D'font-size:10.0pt;font-family:"Arial",=
"sans-serif";
  mso-bidi-font-weight:bold'>(Award only when ALL the evidences of PASS and
  MERIT are achieved and ALL of the following characteristics are demonstra=
ted)</span></i><b><span
  style=3D'font-size:10.0pt;font-family:"Arial","sans-serif";mso-fareast-fo=
nt-family:
  PMingLiU;mso-ansi-language:EN-US;mso-fareast-language:ZH-TW'><o:p></o:p><=
/span></b></p>
  <p class=3DMsoNormal style=3D'margin-bottom:6.0pt;mso-pagination:none;mso=
-layout-grid-align:
  none;text-autospace:none'><span style=3D'font-size:10.0pt;font-family:"Ar=
ial","sans-serif";
  mso-fareast-font-family:PMingLiU;mso-ansi-language:EN-US;mso-fareast-lang=
uage:
  ZH-TW'>The learner must demonstrate all the evidence of the following to
  award a Distinction:<o:p></o:p></span></p>
  <p class=3DMsoNormal style=3D'margin-bottom:6.0pt;mso-pagination:none;mso=
-layout-grid-align:
  none;text-autospace:none'><span style=3D'font-size:10.0pt;font-family:"Ar=
ial","sans-serif";
  mso-fareast-font-family:PMingLiU;mso-ansi-language:EN-US;mso-fareast-lang=
uage:
  ZH-TW'>D1.<span style=3D'mso-spacerun:yes'>&nbsp; </span>Use critical ref=
lection
  to evaluate own work and justify valid conclusions:<o:p></o:p></span></p>
  <p class=3DMsoNormal style=3D'margin-top:0in;margin-right:0in;margin-bott=
om:6.0pt;
  margin-left:.5in;text-indent:-.25in;mso-pagination:none;mso-list:l6 level=
1 lfo8;
  mso-layout-grid-align:none;text-autospace:none'><![if !supportLists]><span
  style=3D'font-size:14.0pt;mso-bidi-font-size:10.0pt;font-family:Wingdings;
  mso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingdings;mso-ansi=
-language:
  EN-US;mso-fareast-language:ZH-TW'><span style=3D'mso-list:Ignore'>&uml;<s=
pan
  style=3D'font:7.0pt "Times New Roman"'> </span></span></span><![endif]><s=
pan
  style=3D'font-size:10.0pt;font-family:"Arial","sans-serif";mso-fareast-fo=
nt-family:
  PMingLiU;mso-ansi-language:EN-US;mso-fareast-language:ZH-TW'>Conclusions =
have
  been arrived at through synthesis of ideas and have been justified.<o:p><=
/o:p></span></p>
  <p class=3DMsoNormal style=3D'margin-top:0in;margin-right:0in;margin-bott=
om:6.0pt;
  margin-left:.5in;text-indent:-.25in;mso-pagination:none;mso-list:l6 level=
1 lfo8;
  mso-layout-grid-align:none;text-autospace:none'><![if !supportLists]><span
  style=3D'font-size:14.0pt;mso-bidi-font-size:10.0pt;font-family:Wingdings;
  mso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingdings;mso-ansi=
-language:
  EN-US;mso-fareast-language:ZH-TW'><span style=3D'mso-list:Ignore'>&uml;<s=
pan
  style=3D'font:7.0pt "Times New Roman"'> </span></span></span><![endif]><s=
pan
  style=3D'font-size:10.0pt;font-family:"Arial","sans-serif";mso-fareast-fo=
nt-family:
  PMingLiU;mso-ansi-language:EN-US;mso-fareast-language:ZH-TW'>The validity=
 of
  results has been evaluated using defined criteria.<span style=3D'mso-tab-=
count:
  3'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbs=
p;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&=
nbsp; </span><o:p></o:p></span></p>
  <p class=3DMsoNormal style=3D'margin-top:0in;margin-right:0in;margin-bott=
om:6.0pt;
  margin-left:.5in;text-indent:-.25in;mso-pagination:none;mso-list:l6 level=
1 lfo8;
  mso-layout-grid-align:none;text-autospace:none'><![if !supportLists]><span
  style=3D'font-size:14.0pt;mso-bidi-font-size:10.0pt;font-family:Wingdings;
  mso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingdings;mso-ansi=
-language:
  EN-US;mso-fareast-language:ZH-TW'><span style=3D'mso-list:Ignore'>&uml;<s=
pan
  style=3D'font:7.0pt "Times New Roman"'> </span></span></span><![endif]><s=
pan
  style=3D'font-size:10.0pt;font-family:"Arial","sans-serif";mso-fareast-fo=
nt-family:
  PMingLiU;mso-ansi-language:EN-US;mso-fareast-language:ZH-TW'>Self-critici=
sm
  of approach has taken place.<span style=3D'mso-tab-count:5'>&nbsp;&nbsp;&=
nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbs=
p;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&=
nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbs=
p;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&=
nbsp;&nbsp;&nbsp; </span><o:p></o:p></span></p>
  <p class=3DMsoNormal style=3D'margin-bottom:6.0pt;mso-pagination:none;mso=
-layout-grid-align:
  none;text-autospace:none'><span style=3D'font-size:10.0pt;font-family:"Ar=
ial","sans-serif";
  mso-fareast-font-family:PMingLiU;mso-ansi-language:EN-US;mso-fareast-lang=
uage:
  ZH-TW'>D2.<span style=3D'mso-spacerun:yes'>&nbsp; </span>Take responsibil=
ity
  for managing and organizing activities:<o:p></o:p></span></p>
  <p class=3DMsoNormal style=3D'margin-top:0in;margin-right:0in;margin-bott=
om:6.0pt;
  margin-left:.5in;text-indent:-.25in;mso-pagination:none;mso-list:l0 level=
1 lfo10;
  mso-layout-grid-align:none;text-autospace:none'><![if !supportLists]><span
  style=3D'font-size:14.0pt;mso-bidi-font-size:10.0pt;font-family:Wingdings;
  mso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingdings;mso-ansi=
-language:
  EN-US;mso-fareast-language:ZH-TW'><span style=3D'mso-list:Ignore'>&uml;<s=
pan
  style=3D'font:7.0pt "Times New Roman"'> </span></span></span><![endif]><s=
pan
  style=3D'font-size:10.0pt;font-family:"Arial","sans-serif";mso-fareast-fo=
nt-family:
  PMingLiU;mso-ansi-language:EN-US;mso-fareast-language:ZH-TW'>Autonomy/ind=
ependence
  has been demonstrated.<span style=3D'mso-tab-count:4'>&nbsp;&nbsp;&nbsp;&=
nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbs=
p;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&=
nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbs=
p;&nbsp;&nbsp;&nbsp;&nbsp; </span><o:p></o:p></span></p>
  <p class=3DMsoNormal style=3D'margin-top:0in;margin-right:0in;margin-bott=
om:6.0pt;
  margin-left:.5in;text-indent:-.25in;mso-pagination:none;mso-list:l0 level=
1 lfo10;
  mso-layout-grid-align:none;text-autospace:none'><![if !supportLists]><span
  style=3D'font-size:14.0pt;mso-bidi-font-size:10.0pt;font-family:Wingdings;
  mso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingdings;mso-ansi=
-language:
  EN-US;mso-fareast-language:ZH-TW'><span style=3D'mso-list:Ignore'>&uml;<s=
pan
  style=3D'font:7.0pt "Times New Roman"'> </span></span></span><![endif]><s=
pan
  style=3D'font-size:10.0pt;font-family:"Arial","sans-serif";mso-fareast-fo=
nt-family:
  PMingLiU;mso-ansi-language:EN-US;mso-fareast-language:ZH-TW'>Activities h=
ave
  been managed.<span style=3D'mso-tab-count:6'>&nbsp;&nbsp;&nbsp;&nbsp;&nbs=
p;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&=
nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbs=
p;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&=
nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbs=
p;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><o:p></o:p></span></p>
  <p class=3DMsoNormal style=3D'margin-bottom:6.0pt;mso-pagination:none;mso=
-layout-grid-align:
  none;text-autospace:none'><span style=3D'font-size:10.0pt;font-family:"Ar=
ial","sans-serif";
  mso-fareast-font-family:PMingLiU;mso-ansi-language:EN-US;mso-fareast-lang=
uage:
  ZH-TW'>D3.<span style=3D'mso-spacerun:yes'>&nbsp; </span>Demonstrate
  convergent/ lateral/ creative thinking:<o:p></o:p></span></p>
  <p class=3DMsoNormal style=3D'margin-top:0in;margin-right:0in;margin-bott=
om:6.0pt;
  margin-left:.5in;text-indent:-.25in;mso-pagination:none;mso-list:l1 level=
1 lfo12;
  mso-layout-grid-align:none;text-autospace:none'><![if !supportLists]><span
  style=3D'font-size:14.0pt;mso-bidi-font-size:10.0pt;font-family:Wingdings;
  mso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingdings;mso-ansi=
-language:
  EN-US;mso-fareast-language:ZH-TW'><span style=3D'mso-list:Ignore'>&uml;<s=
pan
  style=3D'font:7.0pt "Times New Roman"'> </span></span></span><![endif]><s=
pan
  style=3D'font-size:10.0pt;font-family:"Arial","sans-serif";mso-fareast-fo=
nt-family:
  PMingLiU;mso-ansi-language:EN-US;mso-fareast-language:ZH-TW'>Ideas have b=
een
  generated and decisions taken.<span style=3D'mso-tab-count:4'>&nbsp;&nbsp=
;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&n=
bsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp=
;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&n=
bsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><o:p></o:p></span></p>
  <p class=3DMsoNormal style=3D'margin-top:0in;margin-right:0in;margin-bott=
om:6.0pt;
  margin-left:.5in;text-indent:-.25in;mso-pagination:none;mso-list:l1 level=
1 lfo12;
  mso-layout-grid-align:none;text-autospace:none'><![if !supportLists]><span
  style=3D'font-size:14.0pt;mso-bidi-font-size:10.0pt;font-family:Wingdings;
  mso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingdings;mso-ansi=
-language:
  EN-US;mso-fareast-language:ZH-TW'><span style=3D'mso-list:Ignore'>&uml;<s=
pan
  style=3D'font:7.0pt "Times New Roman"'> </span></span></span><![endif]><s=
pan
  style=3D'font-size:10.0pt;font-family:"Arial","sans-serif";mso-fareast-fo=
nt-family:
  PMingLiU;mso-ansi-language:EN-US;mso-fareast-language:ZH-TW'>Self-evaluat=
ion
  has taken place.<span style=3D'mso-tab-count:4'>&nbsp;&nbsp;&nbsp;&nbsp;&=
nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbs=
p;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&=
nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><o:p></o:p></span></p>
  <p class=3DMsoNormal style=3D'margin-top:0in;margin-right:0in;margin-bott=
om:6.0pt;
  margin-left:.5in;text-indent:-.25in;mso-pagination:none;mso-list:l1 level=
1 lfo12;
  mso-layout-grid-align:none;text-autospace:none'><![if !supportLists]><span
  style=3D'font-size:14.0pt;mso-bidi-font-size:10.0pt;font-family:Wingdings;
  mso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingdings;mso-ansi=
-language:
  EN-US;mso-fareast-language:ZH-TW'><span style=3D'mso-list:Ignore'>&uml;<s=
pan
  style=3D'font:7.0pt "Times New Roman"'> </span></span></span><![endif]><s=
pan
  style=3D'font-size:10.0pt;font-family:"Arial","sans-serif";mso-fareast-fo=
nt-family:
  PMingLiU;mso-ansi-language:EN-US;mso-fareast-language:ZH-TW'>Problems have
  been solved.<span style=3D'mso-tab-count:7'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp=
;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&n=
bsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp=
;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&n=
bsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp=
;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&n=
bsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><o:p></o:=
p></span></p>
  <p class=3DMsoNormal style=3D'margin-top:0in;margin-right:0in;margin-bott=
om:3.0pt;
  margin-left:.5in;mso-pagination:none;mso-layout-grid-align:none;text-auto=
space:
  none'><span style=3D'font-size:10.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:PMingLiU;mso-ansi-language:EN-US;mso-fareast-lang=
uage:
  ZH-TW'><o:p>&nbsp;</o:p></span></p>
  <p class=3DMsoNormal style=3D'margin-top:0in;margin-right:0in;margin-bott=
om:3.0pt;
  margin-left:.5in;text-indent:-.25in;mso-pagination:none;mso-list:l1 level=
1 lfo12;
  mso-layout-grid-align:none;text-autospace:none'><![if !supportLists]><span
  style=3D'font-size:14.0pt;mso-bidi-font-size:10.0pt;font-family:Wingdings;
  mso-fareast-font-family:Wingdings;mso-bidi-font-family:Wingdings;mso-ansi=
-language:
  EN-US;mso-fareast-language:ZH-TW'><span style=3D'mso-list:Ignore'>&uml;<s=
pan
  style=3D'font:7.0pt "Times New Roman"'> </span></span></span><![endif]><b
  style=3D'mso-bidi-font-weight:normal'><span style=3D'font-size:10.0pt;fon=
t-family:
  "Arial","sans-serif";mso-fareast-font-family:PMingLiU;mso-ansi-language:E=
N-US;
  mso-fareast-language:ZH-TW'>DISTINCTION</span></b><span style=3D'font-siz=
e:
  10.0pt;font-family:"Arial","sans-serif";mso-fareast-font-family:PMingLiU;
  mso-ansi-language:EN-US;mso-fareast-language:ZH-TW'> <b style=3D'mso-bidi=
-font-weight:
  normal'>- All the Merit and Distinction characteristics are evident. (Ple=
ase
  tick </b></span><b style=3D'mso-bidi-font-weight:normal'><span
  style=3D'font-size:10.0pt;font-family:Wingdings;mso-ascii-font-family:Ari=
al;
  mso-fareast-font-family:PMingLiU;mso-hansi-font-family:Arial;mso-bidi-fon=
t-family:
  Arial;mso-ansi-language:EN-US;mso-fareast-language:ZH-TW;mso-char-type:sy=
mbol;
  mso-symbol-font-family:Wingdings'><span style=3D'mso-char-type:symbol;
  mso-symbol-font-family:Wingdings'>&uuml;</span></span></b><b
  style=3D'mso-bidi-font-weight:normal'><span style=3D'font-size:10.0pt;fon=
t-family:
  "Arial","sans-serif";mso-fareast-font-family:PMingLiU;mso-ansi-language:E=
N-US;
  mso-fareast-language:ZH-TW'> if applicable)</span></b><span style=3D'font=
-size:
  10.0pt;mso-fareast-font-family:PMingLiU;mso-ansi-language:EN-US;mso-farea=
st-language:
  ZH-TW'> </span><span style=3D'font-size:10.0pt;font-family:"Arial","sans-=
serif";
  mso-fareast-font-family:PMingLiU;mso-ansi-language:EN-US;mso-fareast-lang=
uage:
  ZH-TW'><o:p></o:p></span></p>
  </td>
 </tr>
</table>

<p class=3DMsoNormal style=3D'margin-left:-27.0pt;mso-para-margin-left:-2.2=
5gd'><b><span
lang=3DEN-GB style=3D'font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></=
span></b></p>

<p class=3DMsoNormal style=3D'margin-left:-27.0pt;mso-para-margin-left:-2.2=
5gd'><b><span
lang=3DEN-GB style=3D'font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></=
span></b></p>

<table class=3DMsoNormalTable border=3D1 cellspacing=3D0 cellpadding=3D0 wi=
dth=3D"105%"
 style=3D'width:105.18%;margin-left:-12.6pt;border-collapse:collapse;border=
:none;
 mso-border-alt:solid windowtext .5pt;mso-yfti-tbllook:1184;mso-padding-alt:
 0in 5.4pt 0in 5.4pt;mso-border-insideh:.5pt solid windowtext;mso-border-in=
sidev:
 .5pt solid windowtext'>
 <tr style=3D'mso-yfti-irow:0;mso-yfti-firstrow:yes;page-break-inside:avoid;
  height:23.65pt'>
  <td width=3D"38%" colspan=3D2 valign=3Dtop style=3D'width:38.42%;border:s=
olid windowtext 1.0pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:2=
3.65pt'>
  <h5 style=3D'margin-top:12.0pt'><span lang=3DEN-GB style=3D'font-size:11.=
0pt;
  font-family:"Tahoma","sans-serif";mso-fareast-font-family:PMingLiU;
  mso-fareast-language:ZH-TW'>Final Grade decided by the Assessor:<o:p></o:=
p></span></h5>
  </td>
  <td width=3D"61%" colspan=3D3 valign=3Dtop style=3D'width:61.58%;border:s=
olid windowtext 1.0pt;
  border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:23.65pt'>
  <p class=3DMsoNormal align=3Dcenter style=3D'margin-top:10.0pt;margin-rig=
ht:0in;
  margin-bottom:6.0pt;margin-left:0in;mso-para-margin-top:10.0pt;mso-para-m=
argin-right:
  0in;mso-para-margin-bottom:.5gd;mso-para-margin-left:0in;text-align:cente=
r'><span
  style=3D'font-size:16.0pt;font-family:Wingdings;mso-ascii-font-family:Ari=
al;
  mso-fareast-font-family:PMingLiU;mso-hansi-font-family:Arial;mso-bidi-fon=
t-family:
  Arial;mso-ansi-language:EN-US;mso-fareast-language:ZH-TW;mso-char-type:sy=
mbol;
  mso-symbol-font-family:Wingdings'><span style=3D'mso-char-type:symbol;
  mso-symbol-font-family:Wingdings'>&thorn;</span></span><b style=3D'mso-bi=
di-font-weight:
  normal'><span style=3D'font-family:"Arial","sans-serif";mso-fareast-font-=
family:
  PMingLiU;mso-ansi-language:EN-US;mso-fareast-language:ZH-TW'> </span></b>=
<b
  style=3D'mso-bidi-font-weight:normal'><span style=3D'font-size:11.0pt;fon=
t-family:
  "Tahoma","sans-serif";mso-fareast-font-family:PMingLiU;mso-ansi-language:
  EN-US;mso-fareast-language:ZH-TW'>Refer</span></b><b style=3D'mso-bidi-fo=
nt-weight:
  normal'><span lang=3DEN-GB style=3D'font-family:"Arial","sans-serif";mso-=
fareast-font-family:
  PMingLiU;mso-fareast-language:ZH-TW'><span
  style=3D'mso-spacerun:yes'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><=
/b><b
  style=3D'mso-bidi-font-weight:normal'><span lang=3DEN-GB style=3D'font-si=
ze:16.0pt;
  font-family:"Arial","sans-serif";mso-fareast-font-family:PMingLiU;mso-far=
east-language:
  ZH-TW'><span style=3D'mso-spacerun:yes'>&nbsp;</span></span></b><span
  style=3D'font-size:16.0pt;font-family:Wingdings;mso-ascii-font-family:Ari=
al;
  mso-fareast-font-family:PMingLiU;mso-hansi-font-family:Arial;mso-bidi-fon=
t-family:
  Arial;mso-ansi-language:EN-US;mso-fareast-language:ZH-TW;mso-char-type:sy=
mbol;
  mso-symbol-font-family:Wingdings'><span style=3D'mso-char-type:symbol;
  mso-symbol-font-family:Wingdings'>&uml;</span></span><b style=3D'mso-bidi=
-font-weight:
  normal'><span style=3D'font-family:"Arial","sans-serif";mso-fareast-font-=
family:
  PMingLiU;mso-ansi-language:EN-US;mso-fareast-language:ZH-TW'> </span></b>=
<b
  style=3D'mso-bidi-font-weight:normal'><span style=3D'font-size:11.0pt;fon=
t-family:
  "Tahoma","sans-serif";mso-fareast-font-family:PMingLiU;mso-ansi-language:
  EN-US;mso-fareast-language:ZH-TW'>Pass</span></b><b style=3D'mso-bidi-fon=
t-weight:
  normal'><span style=3D'font-size:11.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:PMingLiU;mso-ansi-language:EN-US;mso-fareast-lang=
uage:
  ZH-TW'><span style=3D'mso-spacerun:yes'>&nbsp; </span></span></b><span
  style=3D'font-size:11.0pt;font-family:"Arial","sans-serif";mso-fareast-fo=
nt-family:
  PMingLiU;mso-ansi-language:EN-US;mso-fareast-language:ZH-TW'><span
  style=3D'mso-spacerun:yes'>&nbsp;&nbsp;&nbsp;&nbsp;</span></span><span
  style=3D'font-size:16.0pt;font-family:Wingdings;mso-ascii-font-family:Ari=
al;
  mso-fareast-font-family:PMingLiU;mso-hansi-font-family:Arial;mso-bidi-fon=
t-family:
  Arial;mso-ansi-language:EN-US;mso-fareast-language:ZH-TW;mso-char-type:sy=
mbol;
  mso-symbol-font-family:Wingdings'><span style=3D'mso-char-type:symbol;
  mso-symbol-font-family:Wingdings'>&uml;</span></span><b style=3D'mso-bidi=
-font-weight:
  normal'><span style=3D'font-family:"Arial","sans-serif";mso-fareast-font-=
family:
  PMingLiU;mso-ansi-language:EN-US;mso-fareast-language:ZH-TW'> </span></b>=
<b
  style=3D'mso-bidi-font-weight:normal'><span style=3D'font-size:11.0pt;fon=
t-family:
  "Tahoma","sans-serif";mso-fareast-font-family:PMingLiU;mso-ansi-language:
  EN-US;mso-fareast-language:ZH-TW'>Merit</span></b><span style=3D'font-fam=
ily:
  "Arial","sans-serif";mso-fareast-font-family:PMingLiU;mso-ansi-language:E=
N-US;
  mso-fareast-language:ZH-TW'><span
  style=3D'mso-spacerun:yes'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><=
span
  style=3D'font-size:16.0pt;font-family:Wingdings;mso-ascii-font-family:Ari=
al;
  mso-fareast-font-family:PMingLiU;mso-hansi-font-family:Arial;mso-bidi-fon=
t-family:
  Arial;mso-ansi-language:EN-US;mso-fareast-language:ZH-TW;mso-char-type:sy=
mbol;
  mso-symbol-font-family:Wingdings'><span style=3D'mso-char-type:symbol;
  mso-symbol-font-family:Wingdings'>&uml;</span></span><b style=3D'mso-bidi=
-font-weight:
  normal'><span style=3D'font-family:"Arial","sans-serif";mso-fareast-font-=
family:
  PMingLiU;mso-ansi-language:EN-US;mso-fareast-language:ZH-TW'> </span></b>=
<b
  style=3D'mso-bidi-font-weight:normal'><span style=3D'font-size:11.0pt;fon=
t-family:
  "Tahoma","sans-serif";mso-fareast-font-family:PMingLiU;mso-ansi-language:
  EN-US;mso-fareast-language:ZH-TW'>Distinction</span></b><b style=3D'mso-b=
idi-font-weight:
  normal'><span lang=3DEN-GB style=3D'font-size:10.0pt;font-family:"Arial",=
"sans-serif"'><o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:1;page-break-inside:avoid;height:97.65pt'>
  <td width=3D"100%" colspan=3D5 valign=3Dtop style=3D'width:100.0%;border:=
solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:s=
olid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:97.65pt'>
  <p class=3DMsoNormal style=3D'margin-top:6.0pt'><b style=3D'mso-bidi-font=
-weight:
  normal'><span lang=3DEN-GB style=3D'font-size:10.0pt;font-family:"Arial",=
"sans-serif";
  mso-fareast-font-family:PMingLiU;mso-fareast-language:ZH-TW'>Assessor&#82=
17;s
  overall or </span></b><b style=3D'mso-bidi-font-weight:normal'><span
  lang=3DEN-GB style=3D'font-size:10.0pt;font-family:"Arial","sans-serif"'>=
additional
  comments</span></b><b style=3D'mso-bidi-font-weight:normal'><span lang=3D=
EN-GB
  style=3D'font-size:10.0pt;font-family:"Arial","sans-serif";mso-fareast-fo=
nt-family:
  PMingLiU;mso-fareast-language:ZH-TW'>:<o:p></o:p></span></b></p>
  <p class=3DMsoNormal><b style=3D'mso-bidi-font-weight:normal'><span lang=
=3DEN-GB
  style=3D'font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></b></=
p>
  <p class=3DMsoNormal><b style=3D'mso-bidi-font-weight:normal'><span lang=
=3DEN-GB
  style=3D'font-size:10.0pt;font-family:"Arial","sans-serif"'><o:p>&nbsp;</=
o:p></span></b></p>
  <p class=3DMsoNormal><b style=3D'mso-bidi-font-weight:normal'><span lang=
=3DEN-GB
  style=3D'font-size:10.0pt;font-family:"Arial","sans-serif"'><o:p>&nbsp;</=
o:p></span></b></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:2;page-break-inside:avoid;height:30.1pt'>
  <td width=3D"22%" valign=3Dtop style=3D'width:22.54%;border:solid windowt=
ext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:s=
olid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:30.1pt'>
  <p class=3DMsoNormal style=3D'margin-top:6.0pt'><b style=3D'mso-bidi-font=
-weight:
  normal'><span style=3D'font-size:10.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:PMingLiU;mso-ansi-language:EN-US;mso-fareast-lang=
uage:
  ZH-TW'>Assessed and graded by: <o:p></o:p></span></b></p>
  </td>
  <td width=3D"40%" colspan=3D2 valign=3Dtop style=3D'width:40.38%;border-t=
op:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid =
windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid window=
text .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:3=
0.1pt'>
  <p class=3DMsoNormal style=3D'margin-top:6.0pt'><b style=3D'mso-bidi-font=
-weight:
  normal'><span lang=3DEN-GB style=3D'font-size:10.0pt;font-family:"Arial",=
"sans-serif";
  mso-fareast-font-family:PMingLiU;mso-fareast-language:ZH-TW'><o:p>&nbsp;<=
/o:p></span></b></p>
  </td>
  <td width=3D"7%" valign=3Dtop style=3D'width:7.56%;border-top:none;border=
-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1=
.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid window=
text .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:3=
0.1pt'>
  <p class=3DMsoNormal style=3D'margin-top:6.0pt'><b style=3D'mso-bidi-font=
-weight:
  normal'><span style=3D'font-size:10.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:PMingLiU;mso-ansi-language:EN-US;mso-fareast-lang=
uage:
  ZH-TW'>Date:<o:p></o:p></span></b></p>
  </td>
  <td width=3D"29%" valign=3Dtop style=3D'width:29.52%;border-top:none;bord=
er-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1=
.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid window=
text .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:3=
0.1pt'>
  <p class=3DMsoNormal style=3D'margin-top:6.0pt'><b style=3D'mso-bidi-font=
-weight:
  normal'><span lang=3DEN-GB style=3D'font-size:10.0pt;font-family:"Arial",=
"sans-serif";
  mso-fareast-font-family:PMingLiU;mso-fareast-language:ZH-TW'><o:p>&nbsp;<=
/o:p></span></b></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:3;page-break-inside:avoid;height:50.9pt'>
  <td width=3D"100%" colspan=3D5 valign=3Dtop style=3D'width:100.0%;border:=
solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:s=
olid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:50.9pt'>
  <p class=3DMsoNormal style=3D'margin-top:6.0pt;margin-right:0in;margin-bo=
ttom:
  6.0pt;margin-left:0in'><b style=3D'mso-bidi-font-weight:normal'><span
  lang=3DEN-GB style=3D'font-size:10.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:PMingLiU;mso-fareast-language:ZH-TW'>Student&#821=
7;s
  acknowledgement of grade or result and <span class=3DSpellE>feed back</sp=
an>
  (if any):</span></b><span lang=3DEN-GB style=3D'font-size:9.0pt;font-fami=
ly:"Arial","sans-serif";
  mso-fareast-font-family:PMingLiU;mso-fareast-language:ZH-TW'><span
  style=3D'mso-spacerun:yes'>&nbsp; </span></span><b style=3D'mso-bidi-font=
-weight:
  normal'><span lang=3DEN-GB style=3D'font-size:10.0pt;font-family:"Arial",=
"sans-serif";
  mso-fareast-font-family:PMingLiU;mso-fareast-language:ZH-TW'><o:p></o:p><=
/span></b></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:4;page-break-inside:avoid;height:19.35pt'>
  <td width=3D"22%" valign=3Dtop style=3D'width:22.54%;border:solid windowt=
ext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:s=
olid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:19.35pt'>
  <p class=3DMsoNormal style=3D'margin-top:6.0pt;margin-right:0in;margin-bo=
ttom:
  6.0pt;margin-left:0in'><b style=3D'mso-bidi-font-weight:normal'><span
  lang=3DEN-GB style=3D'font-size:10.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:PMingLiU;mso-fareast-language:ZH-TW'>Student&#821=
7;s
  signature:</span></b><b style=3D'mso-bidi-font-weight:normal'><span lang=
=3DEN-GB
  style=3D'font-size:10.0pt;font-family:"Arial","sans-serif"'> </span></b><b
  style=3D'mso-bidi-font-weight:normal'><span lang=3DEN-GB style=3D'font-si=
ze:10.0pt;
  font-family:"Arial","sans-serif";mso-fareast-font-family:PMingLiU;mso-far=
east-language:
  ZH-TW'><o:p></o:p></span></b></p>
  </td>
  <td width=3D"40%" colspan=3D2 valign=3Dtop style=3D'width:40.38%;border-t=
op:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid =
windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid window=
text .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:1=
9.35pt'>
  <p class=3DMsoNormal align=3Dcenter style=3D'margin-top:6.0pt;margin-righ=
t:0in;
  margin-bottom:6.0pt;margin-left:0in;text-align:center'><b style=3D'mso-bi=
di-font-weight:
  normal'><span lang=3DEN-GB style=3D'font-size:10.0pt;font-family:"Arial",=
"sans-serif"'><o:p>&nbsp;</o:p></span></b></p>
  </td>
  <td width=3D"7%" valign=3Dtop style=3D'width:7.56%;border-top:none;border=
-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1=
.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid window=
text .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:1=
9.35pt'>
  <p class=3DMsoNormal align=3Dcenter style=3D'margin-top:6.0pt;margin-righ=
t:0in;
  margin-bottom:6.0pt;margin-left:0in;text-align:center'><b style=3D'mso-bi=
di-font-weight:
  normal'><span lang=3DEN-GB style=3D'font-size:10.0pt;font-family:"Arial",=
"sans-serif"'>Date</span></b><b
  style=3D'mso-bidi-font-weight:normal'><span lang=3DEN-GB style=3D'font-si=
ze:10.0pt;
  font-family:"Arial","sans-serif";mso-fareast-font-family:PMingLiU;mso-far=
east-language:
  ZH-TW'>:<o:p></o:p></span></b></p>
  </td>
  <td width=3D"29%" valign=3Dtop style=3D'width:29.52%;border-top:none;bord=
er-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1=
.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid window=
text .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:1=
9.35pt'>
  <p class=3DMsoNormal align=3Dcenter style=3D'margin-top:6.0pt;margin-righ=
t:0in;
  margin-bottom:6.0pt;margin-left:0in;text-align:center'><b style=3D'mso-bi=
di-font-weight:
  normal'><span lang=3DEN-GB style=3D'font-size:11.0pt;mso-bidi-font-size:1=
2.0pt;
  font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></b></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:5;mso-yfti-lastrow:yes;page-break-inside:avoid;
  height:33.15pt'>
  <td width=3D"100%" colspan=3D5 valign=3Dtop style=3D'width:100.0%;border:=
solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:s=
olid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt;height:33.15pt'>
  <p class=3DMsoNormal style=3D'margin-top:12.0pt;margin-right:0in;margin-b=
ottom:
  6.0pt;margin-left:0in'><b style=3D'mso-bidi-font-weight:normal'><span
  lang=3DEN-GB style=3D'font-size:10.0pt;font-family:"Arial","sans-serif"'>=
Internal
  Verification </span></b><b style=3D'mso-bidi-font-weight:normal'><span
  lang=3DEN-GB style=3D'font-size:10.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:PMingLiU;mso-fareast-language:ZH-TW'>(if any):<sp=
an
  style=3D'mso-spacerun:yes'>&nbsp;&nbsp; </span></span></b><i style=3D'mso=
-bidi-font-style:
  normal'><span lang=3DEN-GB style=3D'font-size:10.0pt;font-family:"Arial",=
"sans-serif";
  mso-fareast-font-family:PMingLiU;mso-fareast-language:ZH-TW'>See the sepa=
rate
  IV report for verifying assessment decision.</span></i><b style=3D'mso-bi=
di-font-weight:
  normal'><span lang=3DEN-GB style=3D'font-size:10.0pt;font-family:"Arial",=
"sans-serif";
  mso-fareast-font-family:PMingLiU;mso-fareast-language:ZH-TW'><o:p></o:p><=
/span></b></p>
  </td>
 </tr>
 <![if !supportMisalignedColumns]>
 <tr height=3D0>
  <td width=3D177 style=3D'border:none'></td>
  <td width=3D125 style=3D'border:none'></td>
  <td width=3D192 style=3D'border:none'></td>
  <td width=3D59 style=3D'border:none'></td>
  <td width=3D232 style=3D'border:none'></td>
 </tr>
 <![endif]>
</table>

<p class=3DMsoNormal><b><span lang=3DEN-GB style=3D'font-family:"Arial","sa=
ns-serif"'><o:p>&nbsp;</o:p></span></b></p>

<p class=3DMsoNormal><b><span lang=3DEN-GB style=3D'font-family:"Arial","sa=
ns-serif"'>GENERAL
GRADE DESCRIPTION</span></b><b><span lang=3DEN-GB style=3D'font-family:"Ari=
al","sans-serif";
mso-fareast-font-family:PMingLiU;mso-fareast-language:ZH-TW'><o:p></o:p></s=
pan></b></p>

<p class=3DMsoNormal style=3D'text-align:justify;text-justify:inter-ideogra=
ph;
line-height:12.0pt'><b><span lang=3DEN-GB style=3D'font-size:9.0pt;font-fam=
ily:
"Arial","sans-serif"'>PASS (P)<o:p></o:p></span></b></p>

<p class=3DMsoNormal style=3D'text-align:justify;text-justify:inter-ideogra=
ph;
line-height:12.0pt'><span lang=3DEN-GB style=3D'font-size:9.0pt;font-family=
:"Arial","sans-serif";
mso-bidi-font-weight:bold'>A &#8220;Pass&#8221; grade is achieved by meetin=
g ALL
outcomes and associated criteria as defined in the Assessment Criteria for =
pass
for this unit.<o:p></o:p></span></p>

<p class=3DMsoNormal style=3D'text-align:justify;text-justify:inter-ideogra=
ph;
line-height:12.0pt'><b><span lang=3DEN-GB style=3D'font-size:9.0pt;font-fam=
ily:
"Arial","sans-serif";mso-bidi-font-style:italic'>REFER</span></b><b><span
lang=3DEN-GB style=3D'font-size:9.0pt;font-family:"Arial","sans-serif";mso-=
fareast-font-family:
PMingLiU;mso-fareast-language:ZH-TW;mso-bidi-font-style:italic'> (R)<o:p></=
o:p></span></b></p>

<p class=3DMsoNormal style=3D'text-align:justify;text-justify:inter-ideogra=
ph;
line-height:12.0pt'><span lang=3DEN-GB style=3D'font-size:9.0pt;font-family=
:"Arial","sans-serif";
mso-bidi-font-weight:bold;mso-bidi-font-style:italic'>Students failing to m=
eet
the requirements of a PASS will automatically be referred.<span
style=3D'mso-spacerun:yes'>&nbsp; </span><o:p></o:p></span></p>

<p class=3DMsoNormal style=3D'text-align:justify;text-justify:inter-ideogra=
ph;
line-height:12.0pt'><b><span lang=3DEN-GB style=3D'font-size:9.0pt;font-fam=
ily:
"Arial","sans-serif"'>MERIT (M)<o:p></o:p></span></b></p>

<p class=3DMsoNormal style=3D'text-align:justify;text-justify:inter-ideogra=
ph;
line-height:12.0pt'><span lang=3DEN-GB style=3D'font-size:9.0pt;font-family=
:"Arial","sans-serif";
mso-bidi-font-weight:bold'>A &#8220;Merit&#8221; grade is achieved by meeti=
ng
PASS requirement and fulfilling all merit grade characteristics as defined =
in
the Grade Descriptor.<o:p></o:p></span></p>

<p class=3DMsoNormal style=3D'text-align:justify;text-justify:inter-ideogra=
ph;
line-height:12.0pt'><b><span lang=3DEN-GB style=3D'font-size:9.0pt;font-fam=
ily:
"Arial","sans-serif"'>DISTINCTION (D)<o:p></o:p></span></b></p>

<p class=3DMsoNormal style=3D'text-align:justify;text-justify:inter-ideogra=
ph;
line-height:12.0pt'><span lang=3DEN-GB style=3D'font-size:9.0pt;font-family=
:"Arial","sans-serif";
mso-bidi-font-weight:bold'>A &#8220;Distinction&#8221; is achieved by meeti=
ng
PASS and &#8220;MERIT&#8221; requirement and fulfilling all distinction
characteristics as defined in the Grade Descriptor.<o:p></o:p></span></p>

<p class=3DMsoNormal style=3D'text-align:justify;text-justify:inter-ideogra=
ph;
line-height:12.0pt'><b><span lang=3DEN-GB style=3D'font-size:9.0pt;font-fam=
ily:
"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></b></p>

<h3 style=3D'margin:0in;margin-bottom:.0001pt;text-align:justify;text-justi=
fy:
inter-ideograph;line-height:12.0pt'><span lang=3DEN-GB style=3D'font-size:9=
.0pt'>NOTES
TO STUDENTS &#8211; please pay particular attention to the <u>deadline</u> =
and
the <u>instructions </u>given with the assignment<o:p></o:p></span></h3>

<p class=3DMsoNormal style=3D'margin-top:0in;margin-right:-15.9pt;margin-bo=
ttom:
0in;margin-left:24.1pt;margin-bottom:.0001pt;text-align:justify;text-justif=
y:
inter-ideograph;text-indent:-24.1pt;line-height:12.0pt;mso-list:l4 level1 l=
fo14;
tab-stops:list 24.0pt'><![if !supportLists]><span lang=3DEN-GB style=3D'fon=
t-size:
9.0pt;font-family:Wingdings;mso-fareast-font-family:Wingdings;mso-bidi-font=
-family:
Wingdings;mso-fareast-language:ZH-TW;mso-bidi-font-weight:bold'><span
style=3D'mso-list:Ignore'>&sup2;<span style=3D'font:7.0pt "Times New Roman"=
'>&nbsp;&nbsp;
</span></span></span><![endif]><span lang=3DEN-GB style=3D'font-size:9.0pt;
font-family:"Arial","sans-serif";mso-bidi-font-weight:bold'>You must attach=
 a
completed </span><span lang=3DEN-GB style=3D'font-size:9.0pt;font-family:"A=
rial","sans-serif";
mso-fareast-font-family:PMingLiU;mso-fareast-language:ZH-TW;mso-bidi-font-w=
eight:
bold'>Assignment Submission Form which </span><span lang=3DEN-GB
style=3D'font-size:9.0pt;font-family:"Arial","sans-serif";mso-bidi-font-wei=
ght:
bold'>is</span><span lang=3DEN-GB style=3D'font-size:9.0pt;font-family:"Ari=
al","sans-serif";
mso-fareast-font-family:PMingLiU;mso-fareast-language:ZH-TW;mso-bidi-font-w=
eight:
bold'> attached herewith together with the assignment brief when you submit
your assignment.<o:p></o:p></span></p>

<p class=3DMsoNormal style=3D'margin-top:0in;margin-right:-15.9pt;margin-bo=
ttom:
0in;margin-left:24.1pt;margin-bottom:.0001pt;text-align:justify;text-justif=
y:
inter-ideograph;text-indent:-24.1pt;line-height:12.0pt;mso-list:l4 level1 l=
fo14;
tab-stops:list 24.0pt'><![if !supportLists]><span lang=3DEN-GB style=3D'fon=
t-size:
9.0pt;font-family:Wingdings;mso-fareast-font-family:Wingdings;mso-bidi-font=
-family:
Wingdings'><span style=3D'mso-list:Ignore'>&sup2;<span style=3D'font:7.0pt =
"Times New Roman"'>&nbsp;&nbsp;
</span></span></span><![endif]><span lang=3DEN-GB style=3D'font-size:9.0pt;
font-family:"Arial","sans-serif";mso-fareast-font-family:PMingLiU;mso-farea=
st-language:
ZH-TW;mso-bidi-font-weight:bold'>Yo</span><span lang=3DEN-GB style=3D'font-=
size:
9.0pt;font-family:"Arial","sans-serif";mso-bidi-font-weight:bold'>ur assign=
ment
should be word processed with the main content <u>not more than </u></span>=
<b><i
style=3D'mso-bidi-font-style:normal'><u><span lang=3DEN-GB style=3D'font-si=
ze:9.0pt;
font-family:"Arial","sans-serif";mso-fareast-font-family:PMingLiU;mso-farea=
st-language:
ZH-TW'>3,500 </span></u></i></b><b><i style=3D'mso-bidi-font-style:normal'>=
<u><span
lang=3DEN-GB style=3D'font-size:9.0pt;font-family:"Arial","sans-serif"'>wor=
ds</span></u></i></b><span
lang=3DEN-GB style=3D'font-size:9.0pt;font-family:"Arial","sans-serif";mso-=
bidi-font-weight:
bold'> in length</span><span lang=3DEN-GB style=3D'font-size:9.0pt;font-fam=
ily:
"Arial","sans-serif";mso-fareast-font-family:PMingLiU;mso-fareast-language:
ZH-TW;mso-bidi-font-weight:bold'>. </span><span lang=3DEN-GB style=3D'font-=
size:
9.0pt;font-family:"Arial","sans-serif";mso-bidi-font-weight:bold'>You should
state the <b>Word Count</b></span><b><span lang=3DEN-GB style=3D'font-size:=
9.0pt;
font-family:"Arial","sans-serif";mso-fareast-font-family:PMingLiU;mso-farea=
st-language:
ZH-TW'> </span></b><span lang=3DEN-GB style=3D'font-size:9.0pt;font-family:=
"Arial","sans-serif";
mso-fareast-font-family:PMingLiU;mso-fareast-language:ZH-TW;mso-bidi-font-w=
eight:
bold'>at the end of the report<b> </b>with the <i style=3D'mso-bidi-font-st=
yle:
normal'>attachment of<b> List of R</b></i></span><b><i style=3D'mso-bidi-fo=
nt-style:
normal'><span lang=3DEN-GB style=3D'font-size:9.0pt;font-family:"Arial","sa=
ns-serif"'>eference
</span></i></b><span lang=3DEN-GB style=3D'font-size:9.0pt;font-family:"Ari=
al","sans-serif";
mso-bidi-font-weight:bold'>by using <b><u>Harvard Referencing System</u>.</=
b></span><span
lang=3DEN-GB style=3D'font-size:9.0pt;font-family:"Arial","sans-serif"'><o:=
p></o:p></span></p>

<p class=3DMsoNormal style=3D'margin-top:0in;margin-right:-15.9pt;margin-bo=
ttom:
0in;margin-left:24.1pt;margin-bottom:.0001pt;text-align:justify;text-justif=
y:
inter-ideograph;text-indent:-24.1pt;line-height:12.0pt;mso-list:l4 level1 l=
fo14;
tab-stops:list 24.0pt'><![if !supportLists]><span lang=3DEN-GB style=3D'fon=
t-size:
9.0pt;font-family:Wingdings;mso-fareast-font-family:Wingdings;mso-bidi-font=
-family:
Wingdings'><span style=3D'mso-list:Ignore'>&sup2;<span style=3D'font:7.0pt =
"Times New Roman"'>&nbsp;&nbsp;
</span></span></span><![endif]><span lang=3DEN-GB style=3D'font-size:9.0pt;
font-family:"Arial","sans-serif"'>Remember to keep an electronic or paper c=
opy
for yourself as you may be asked to reproduce in case of whatever goes wrong
subsequently.<o:p></o:p></span></p>

<h1 style=3D'margin-top:0in;margin-right:-15.9pt;margin-bottom:0in;margin-l=
eft:
24.1pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograp=
h;
text-indent:-24.1pt;line-height:12.0pt;mso-list:l4 level1 lfo14;tab-stops:l=
ist 24.0pt'><![if !supportLists]><span
lang=3DEN-GB style=3D'font-size:9.0pt;font-family:Wingdings;mso-fareast-fon=
t-family:
Wingdings;mso-bidi-font-family:Wingdings;font-weight:normal;mso-bidi-font-w=
eight:
bold'><span style=3D'mso-list:Ignore'>&sup2;<span style=3D'font:7.0pt "Time=
s New Roman"'>&nbsp;&nbsp;
</span></span></span><![endif]><span lang=3DEN-GB style=3D'font-size:9.0pt'=
>Submission
of the assignment after the stipulated due date will be accepted subject to=
 late
submission processing fee of $150 per assignment.</span><span lang=3DEN-GB
style=3D'font-size:9.0pt;font-weight:normal;mso-bidi-font-weight:bold'> (<s=
pan
class=3DGramE>see</span> Assessment Activities Guideline for detail)<o:p></=
o:p></span></h1>

<p class=3DMsoNormal style=3D'margin-top:0in;margin-right:-15.9pt;margin-bo=
ttom:
0in;margin-left:24.1pt;margin-bottom:.0001pt;text-align:justify;text-justif=
y:
inter-ideograph;text-indent:-24.1pt;line-height:12.0pt;mso-list:l4 level1 l=
fo14;
tab-stops:list 24.0pt'><![if !supportLists]><span lang=3DEN-GB style=3D'fon=
t-size:
9.0pt;font-family:Wingdings;mso-fareast-font-family:Wingdings;mso-bidi-font=
-family:
Wingdings'><span style=3D'mso-list:Ignore'>&sup2;<span style=3D'font:7.0pt =
"Times New Roman"'>&nbsp;&nbsp;
</span></span></span><![endif]><span lang=3DEN-GB style=3D'font-size:9.0pt;
font-family:"Arial","sans-serif"'>Ensure that you give yourself enough time=
 to
complete the assignment by the due date.<o:p></o:p></span></p>

<p class=3DMsoNormal style=3D'margin-top:0in;margin-right:-15.9pt;margin-bo=
ttom:
0in;margin-left:24.1pt;margin-bottom:.0001pt;text-align:justify;text-justif=
y:
inter-ideograph;text-indent:-24.1pt;line-height:12.0pt;mso-list:l4 level1 l=
fo14;
tab-stops:list 24.0pt'><![if !supportLists]><span lang=3DEN-GB style=3D'fon=
t-size:
9.0pt;font-family:Wingdings;mso-fareast-font-family:Wingdings;mso-bidi-font=
-family:
Wingdings'><span style=3D'mso-list:Ignore'>&sup2;<span style=3D'font:7.0pt =
"Times New Roman"'>&nbsp;&nbsp;
</span></span></span><![endif]><span lang=3DEN-GB style=3D'font-size:9.0pt;
font-family:"Arial","sans-serif"'>Don&#8217;t leave things such as printing=
 to
the last minute &#8211; excuses of this nature will not be accepted for fai=
lure
to hand-in the work on time.<span style=3D'mso-spacerun:yes'>&nbsp; </span>=
<o:p></o:p></span></p>

<p class=3DMsoNormal style=3D'margin-top:0in;margin-right:-15.9pt;margin-bo=
ttom:
0in;margin-left:24.1pt;margin-bottom:.0001pt;text-align:justify;text-justif=
y:
inter-ideograph;text-indent:-24.1pt;line-height:12.0pt;mso-list:l4 level1 l=
fo14;
tab-stops:list 24.0pt'><![if !supportLists]><span lang=3DEN-GB style=3D'fon=
t-size:
9.0pt;font-family:Wingdings;mso-fareast-font-family:Wingdings;mso-bidi-font=
-family:
Wingdings'><span style=3D'mso-list:Ignore'>&sup2;<span style=3D'font:7.0pt =
"Times New Roman"'>&nbsp;&nbsp;
</span></span></span><![endif]><span lang=3DEN-GB style=3D'font-size:9.0pt;
font-family:"Arial","sans-serif"'>You must take responsibility for managing
your own time effectively.<o:p></o:p></span></p>

<p class=3DMsoNormal style=3D'margin-top:0in;margin-right:-15.9pt;margin-bo=
ttom:
0in;margin-left:24.1pt;margin-bottom:.0001pt;text-align:justify;text-justif=
y:
inter-ideograph;text-indent:-24.1pt;line-height:12.0pt;mso-list:l4 level1 l=
fo14;
tab-stops:list 24.0pt'><![if !supportLists]><span lang=3DEN-GB style=3D'fon=
t-size:
9.0pt;font-family:Wingdings;mso-fareast-font-family:Wingdings;mso-bidi-font=
-family:
Wingdings'><span style=3D'mso-list:Ignore'>&sup2;<span style=3D'font:7.0pt =
"Times New Roman"'>&nbsp;&nbsp;
</span></span></span><![endif]><span lang=3DEN-GB style=3D'font-size:9.0pt;
font-family:"Arial","sans-serif"'>If you are unable to hand in your assignm=
ent
on time and have valid reasons such as illness, you may apply (in writing) =
for
an extension.<span style=3D'mso-spacerun:yes'>&nbsp; </span>(see your Stude=
nt
Handbook for details)<o:p></o:p></span></p>

<p class=3DMsoNormal style=3D'margin-top:0in;margin-right:-15.9pt;margin-bo=
ttom:
0in;margin-left:24.1pt;margin-bottom:.0001pt;text-align:justify;text-justif=
y:
inter-ideograph;text-indent:-24.1pt;line-height:12.0pt;mso-list:l4 level1 l=
fo14;
tab-stops:list 24.0pt'><![if !supportLists]><span lang=3DEN-GB style=3D'fon=
t-size:
9.0pt;font-family:Wingdings;mso-fareast-font-family:Wingdings;mso-bidi-font=
-family:
Wingdings'><span style=3D'mso-list:Ignore'>&sup2;<span style=3D'font:7.0pt =
"Times New Roman"'>&nbsp;&nbsp;
</span></span></span><![endif]><b style=3D'mso-bidi-font-weight:normal'><sp=
an
lang=3DEN-GB style=3D'font-size:9.0pt;font-family:"Arial","sans-serif"'>Stu=
dents
whose assignments are graded with Refer (for BTEC), or Fail (for PGDs), may
apply to redo their assignments by submitting the Letter of Undertaking and=
 pay
an admin fee of $150 per assignment before submitting their redo assignment=
 for
marking.<o:p></o:p></span></b></p>

<p class=3DMsoNormal style=3D'margin-top:0in;margin-right:-15.9pt;margin-bo=
ttom:
0in;margin-left:24.1pt;margin-bottom:.0001pt;text-align:justify;text-justif=
y:
inter-ideograph;text-indent:-24.1pt;line-height:12.0pt;mso-list:l4 level1 l=
fo14;
tab-stops:list 24.0pt'><![if !supportLists]><span lang=3DEN-GB style=3D'fon=
t-size:
9.0pt;font-family:Wingdings;mso-fareast-font-family:Wingdings;mso-bidi-font=
-family:
Wingdings'><span style=3D'mso-list:Ignore'>&sup2;<span style=3D'font:7.0pt =
"Times New Roman"'>&nbsp;&nbsp;
</span></span></span><![endif]><span lang=3DEN-GB style=3D'font-size:9.0pt;
font-family:"Arial","sans-serif"'>Take great care that if you use other peo=
ples
work or ideas in your assignment, you properly reference them, using the <u=
><span
style=3D'mso-bidi-font-weight:bold'>Harvard Referencing System</span></u>, =
in
your text and any bibliography.<o:p></o:p></span></p>

<p class=3DMsoNormal style=3D'margin-top:0in;margin-right:-15.9pt;margin-bo=
ttom:
0in;margin-left:24.1pt;margin-bottom:.0001pt;text-align:justify;text-justif=
y:
inter-ideograph;text-indent:-24.1pt;line-height:12.0pt;mso-list:l4 level1 l=
fo14;
tab-stops:list 24.0pt'><![if !supportLists]><span lang=3DEN-GB style=3D'fon=
t-size:
9.0pt;font-family:Wingdings;mso-fareast-font-family:Wingdings;mso-bidi-font=
-family:
Wingdings'><span style=3D'mso-list:Ignore'>&sup2;<span style=3D'font:7.0pt =
"Times New Roman"'>&nbsp;&nbsp;
</span></span></span><![endif]><u><span lang=3DEN-GB style=3D'font-size:9.0=
pt;
font-family:"Arial","sans-serif";mso-bidi-font-weight:bold'>If you are caug=
ht
plagiarising, you could have your grade reduced to a Refer grade (for BTEC)=
 or
Fail grade (for PGDs), or at worst, you could be excluded from the course.<=
/span></u><span
lang=3DEN-GB style=3D'font-size:9.0pt;font-family:"Arial","sans-serif"'><o:=
p></o:p></span></p>

<p class=3DMsoNormal style=3D'margin-right:-15.9pt;text-align:justify;text-=
justify:
inter-ideograph;line-height:12.0pt'><u><span lang=3DEN-GB style=3D'font-siz=
e:9.0pt;
font-family:"Arial","sans-serif";mso-bidi-font-weight:bold'><o:p><span
 style=3D'text-decoration:none'>&nbsp;</span></o:p></span></u></p>

<p class=3DMsoNormal style=3D'margin-right:-15.9pt;text-align:justify;text-=
justify:
inter-ideograph;line-height:12.0pt'><span lang=3DEN-GB style=3D'font-size:9=
.0pt;
font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></p>

<p class=3DMsoNormal><b style=3D'mso-bidi-font-weight:normal'><span lang=3D=
EN-GB
style=3D'font-size:14.0pt;font-family:"Arial","sans-serif"'>{{$course->title }}</span></b><b style=3D'mso-bidi-font-weight:norma=
l'><span
lang=3DEN-GB style=3D'font-size:14.0pt;font-family:"Arial","sans-serif";mso=
-fareast-font-family:
PMingLiU;mso-fareast-language:ZH-TW'><o:p></o:p></span></b></p>

<p class=3DMsoNormal><b style=3D'mso-bidi-font-weight:normal'><span lang=3D=
EN-GB
style=3D'font-size:11.0pt;font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:=
p></span></b></p>

<p class=3DMsoNormal><b style=3D'mso-bidi-font-weight:normal'><span lang=3D=
EN-GB
style=3D'font-size:11.0pt;font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:=
p></span></b></p>

<p class=3DMsoNormal><b style=3D'mso-bidi-font-weight:normal'><span lang=3D=
EN-GB
style=3D'font-size:11.0pt;font-family:"Arial","sans-serif"'>LEARNING OUTCOM=
ES: <o:p></o:p></span></b></p>

@foreach($course->module as $key => $module)

<p class=3DMsoNormal style=3D'mso-layout-grid-align:none;text-autospace:non=
e'><span
lang=3DEN-SG style=3D'font-size:11.0pt;font-family:"Arial","sans-serif";mso=
-fareast-font-family:
SimSun;mso-ansi-language:EN-SG;mso-fareast-language:ZH-CN'>LO{{ $key + 1 }} <span
style=3D'mso-tab-count:1'>&nbsp;&nbsp;&nbsp; </span>{{ $module->title}}<o:p></o:p></span></p>
@endforeach

<p class=3DMsoNormal style=3D'line-height:18.0pt;mso-line-height-rule:exact=
ly'><span
lang=3DEN-GB style=3D'font-size:11.0pt;font-family:"Arial","sans-serif"'><o=
:p>&nbsp;</o:p></span></p>

<p class=3DMsoNormal><b style=3D'mso-bidi-font-weight:normal'><span lang=3D=
EN-GB
style=3D'font-size:11.0pt;font-family:"Arial","sans-serif";mso-fareast-font=
-family:
PMingLiU;mso-fareast-language:ZH-TW'><o:p>&nbsp;</o:p></span></b></p>

<p class=3DMsoNormal><b style=3D'mso-bidi-font-weight:normal'><span lang=3D=
EN-GB
style=3D'font-size:11.0pt;font-family:"Arial","sans-serif"'>ASSESSMENT CRIT=
ERIA:<o:p></o:p></span></b></p>

@foreach($course->module as $keyModule => $module)
	<?php $userTaskIds = app(\VuleApps\LwcPortal\Repository\PortalRepository::class)->groupTaskIDsByModule($module->id); ?>
	<?php $userTask = \VuleApps\LwcPortal\Models\UserTask::where('user_id', $user->id)->whereIn('task_id', $userTaskIds)->with('task')->get();?>
	@foreach($userTask as $keyTask => $user_task)
        <p class=3DMsoNormal style=3D'margin-left:19.5pt;text-indent:-19.5pt;mso-li=
        st:l3 level2 lfo16'><![if !supportLists]><span
        lang=3DEN-GB style=3D'font-size:11.0pt;font-family:"Arial","sans-serif";mso=
        -fareast-font-family:
        Arial'><span style=3D'mso-list:Ignore'>{{ $keyModule + 1 }}.{{ $keyTask + 1 }}<span style=3D'font:7.0pt "Times =
        New Roman"'>&nbsp;
        </span></span></span><![endif]><span lang=3DEN-GB style=3D'font-size:11.0pt;
        font-family:"Arial","sans-serif"'>{{ $user_task->task->title }}<o:p></o:p></span></p>
    @endforeach
    <p class=3DMsoNormal style=3D'margin-left:19.5pt;mso-layout-grid-align:none;
            text-autospace:none'><span lang=3DEN-SG style=3D'font-size:11.0pt;font-fami=
            ly:"Arial","sans-serif";
            mso-fareast-font-family:SimSun;mso-ansi-language:EN-SG;mso-fareast-language:
            ZH-CN'><o:p>&nbsp;</o:p></span></p>

@endforeach

<p class=3DMsoNormal style=3D'margin-right:-15.9pt;text-align:justify;text-=
justify:
inter-ideograph;mso-line-height-alt:12.0pt'><b><span lang=3DEN-SG
style=3D'font-size:14.0pt;font-family:"Arial","sans-serif";mso-fareast-font=
-family:
PMingLiU;mso-ansi-language:EN-SG;mso-fareast-language:ZH-TW'><o:p>&nbsp;</o=
:p></span></b></p>

<p class=3DMsoNormal style=3D'margin-right:-15.9pt;text-align:justify;text-=
justify:
inter-ideograph;mso-line-height-alt:12.0pt'><b><span lang=3DEN-GB
style=3D'font-size:14.0pt;font-family:"Arial","sans-serif";mso-fareast-font=
-family:
PMingLiU;mso-fareast-language:ZH-TW'><o:p>&nbsp;</o:p></span></b></p>

<p class=3DMsoNormal align=3Dright style=3D'margin-right:14.0pt;text-align:=
right;
word-break:break-all'><b><span style=3D'font-size:11.0pt;font-family:"Arial=
","sans-serif";
mso-fareast-font-family:PMingLiU;mso-ansi-language:EN-US;mso-fareast-langua=
ge:
ZH-TW'><o:p>&nbsp;</o:p></span></b></p>

<b style=3D'mso-bidi-font-weight:normal'><span lang=3DEN-GB style=3D'font-s=
ize:12.0pt;
font-family:"Arial","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-ansi-language:EN-GB;mso-fareast-language:EN-US;mso-bidi-language:AR-SA'=
><br
clear=3Dall style=3D'mso-special-character:line-break;page-break-before:alw=
ays'>
</span></b>

<p class=3DMsoNormal style=3D'text-align:justify;text-justify:inter-ideogra=
ph;
tab-stops:.75in'><b style=3D'mso-bidi-font-weight:normal'><span lang=3DEN-GB
style=3D'font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></b></p>

<p class=3DMsoNormal><b style=3D'mso-bidi-font-weight:normal'><u><span
style=3D'font-size:22.0pt;mso-ansi-language:EN-US'>Task Assignment<o:p></o:=
p></span></u></b></p>

<p class=3DMsoNormal><b style=3D'mso-bidi-font-weight:normal'><u><span
style=3D'font-size:13.0pt;mso-ansi-language:EN-US'>Q1 [LO1 &#8211; Criteria=
 1.1]<o:p></o:p></span></u></b></p>

<p class=3DMsoListParagraph style=3D'margin-left:0in'><span style=3D'font-s=
ize:13.0pt;
mso-ansi-language:EN-US'>Differentiate between the tall and the flat
organization structure. What are the general working cultural characteristi=
cs
associated to the two organizations mentioned? </span><span lang=3DEN-SG
style=3D'font-size:13.0pt;mso-ansi-language:EN-SG'>(</span><span lang=3DEN-=
GB
style=3D'font-size:13.0pt'>Assessment Criteria: 1.1)<o:p></o:p></span></p>

<p class=3DMsoListParagraph style=3D'margin-left:0in'><span style=3D'font-s=
ize:13.0pt;
mso-ansi-language:EN-US'><o:p>&nbsp;</o:p></span></p>

<p class=3DMsoNormal><b style=3D'mso-bidi-font-weight:normal'><u><span
style=3D'font-size:13.0pt;mso-ansi-language:EN-US'>Q2 [LO1 - Criteria 1.2]<=
o:p></o:p></span></u></b></p>

<p class=3DMsoListParagraph style=3D'margin-left:0in'><span style=3D'font-s=
ize:13.0pt;
mso-ansi-language:EN-US'>Based on the strategic organizational focus of
external responses and internal structural control, identify and compare tw=
o corresponding
organizational cultures. Assess the business performances of them in terms =
of
their business focus and response to organizational performances that neede=
d as
business outcomes.</span><span style=3D'font-size:13.0pt'> </span><span
lang=3DEN-SG style=3D'font-size:13.0pt;mso-ansi-language:EN-SG'>(</span><sp=
an
lang=3DEN-GB style=3D'font-size:13.0pt'>Assessment Criteria: 1.2; Grading C=
riteria
M1)<o:p></o:p></span></p>

<p class=3DMsoListParagraph style=3D'margin-left:0in'><span style=3D'font-s=
ize:13.0pt;
mso-ansi-language:EN-US'><o:p>&nbsp;</o:p></span></p>

<p class=3DMsoNormal><b style=3D'mso-bidi-font-weight:normal'><u><span
style=3D'font-size:13.0pt;mso-ansi-language:EN-US'>Q3 [LO1- Criteria 1.3]<o=
:p></o:p></span></u></b></p>

<p class=3DMsoNormal><span style=3D'font-size:13.0pt;mso-ansi-language:EN-U=
S'>Discuss
how the two behavioral factors which affect performance of employees at wor=
k,
which are EITHER related to an intrinsic person-and-job (P-J) fit and the
extrinsic person-and-organization (P-O) fits </span><span lang=3DEN-SG
style=3D'font-size:13.0pt;mso-ansi-language:EN-SG'>(</span><span lang=3DEN-=
GB
style=3D'font-size:13.0pt'>Assessment Criteria: 1.3; Grading Criteria M1, D=
1)</span><span
style=3D'font-size:13.0pt;mso-ansi-language:EN-US'><o:p></o:p></span></p>

<p class=3DMsoNormal><span style=3D'font-size:13.0pt;mso-ansi-language:EN-U=
S'><o:p>&nbsp;</o:p></span></p>

<p class=3DMsoNormal><b style=3D'mso-bidi-font-weight:normal'><u><span
style=3D'font-size:13.0pt;mso-ansi-language:EN-US'>Q4 [LO2 - Criteria 2.1]<=
o:p></o:p></span></u></b></p>

<p class=3DMsoNormal><span style=3D'font-size:13.0pt;mso-ansi-language:EN-U=
S'>Base
on the leadership styles in communication with employees, identify and desc=
ribe
the major categories leadership styles at work? Compare theses leadership
styles and their effectiveness, </span><span lang=3DEN-SG style=3D'font-siz=
e:13.0pt;
mso-ansi-language:EN-SG'>(</span><span lang=3DEN-GB style=3D'font-size:13.0=
pt'>Assessment
Criteria: 2.1; Grading Criteria <span class=3DGramE>M2;</span>)</span><span
style=3D'font-size:13.0pt;mso-ansi-language:EN-US'><o:p></o:p></span></p>

<p class=3DMsoNormal><span style=3D'font-size:13.0pt;mso-ansi-language:EN-U=
S'><o:p>&nbsp;</o:p></span></p>

<p class=3DMsoNormal><b style=3D'mso-bidi-font-weight:normal'><u><span
style=3D'font-size:13.0pt;mso-ansi-language:EN-US'>Q5 [LO2 - Criteria 2.2]<=
o:p></o:p></span></u></b></p>

<p class=3DMsoNormal><span style=3D'font-size:13.0pt;mso-ansi-language:EN-U=
S'>Based
on theories learnt, identify the principles of management and organizational
theories relate to its business/operational context, explain how they under=
pin
the management practices at workplaces.</span><span lang=3DEN-SG
style=3D'font-size:13.0pt;mso-ansi-language:EN-SG'> <span style=3D'mso-tab-=
count:
1'>&nbsp;&nbsp;&nbsp;&nbsp; </span><span style=3D'mso-tab-count:1'>&nbsp;&n=
bsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>(</span><=
span
lang=3DEN-GB style=3D'font-size:13.0pt'>Assessment Criteria: 2.2)</span><sp=
an
lang=3DEN-SG style=3D'font-size:13.0pt;mso-ansi-language:EN-SG'><o:p></o:p>=
</span></p>

<p class=3DMsoNormal><span style=3D'font-size:13.0pt;mso-ansi-language:EN-U=
S'><o:p>&nbsp;</o:p></span></p>

<p class=3DMsoNormal><b style=3D'mso-bidi-font-weight:normal'><u><span
style=3D'font-size:13.0pt;mso-ansi-language:EN-US'>Q6 [LO2 - Criteria 2.3]<=
o:p></o:p></span></u></b></p>

<p class=3DMsoNormal><span style=3D'font-size:13.0pt;mso-ansi-language:EN-U=
S'>Evaluate
the human relations approach and the classical approach to management theor=
ies
used in their related organization contexts.</span><span lang=3DEN-SG
style=3D'font-size:13.0pt;mso-ansi-language:EN-SG'> <span style=3D'mso-tab-=
count:
1'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>(</span><span lang=3DEN-GB
style=3D'font-size:13.0pt'>Assessment Criteria: 2.3; Grading Criteria M3; <=
span
style=3D'color:red'>D2</span>)</span><span lang=3DEN-SG style=3D'font-size:=
13.0pt;
mso-ansi-language:EN-SG'><o:p></o:p></span></p>

<p class=3DMsoNormal><span lang=3DEN-SG style=3D'font-size:13.0pt;mso-farea=
st-font-family:
SimSun;mso-ansi-language:EN-SG;mso-fareast-language:ZH-CN'><o:p>&nbsp;</o:p=
></span></p>

<p class=3DMsoNormal><b style=3D'mso-bidi-font-weight:normal'><u><span
style=3D'font-size:13.0pt;mso-ansi-language:EN-US'>Q7 [LO3 - Criteria 3.1]<=
o:p></o:p></span></u></b></p>

<p class=3DMsoNormal style=3D'mso-layout-grid-align:none;text-autospace:non=
e'><span
lang=3DEN-SG style=3D'font-size:13.0pt;mso-fareast-font-family:SimSun;mso-a=
nsi-language:
EN-SG;mso-fareast-language:ZH-CN'>Discuss the impact that transformational =
and
transactional leadership styles may have on motivation in organizations dur=
ing
change or transitional time of organization.</span><span lang=3DEN-SG
style=3D'font-size:13.0pt;mso-ansi-language:EN-SG'> (</span><span lang=3DEN=
-GB
style=3D'font-size:13.0pt'>Assessment Criteria: 3.1; Grading Criteria M1, <=
span
style=3D'color:red'>D3</span>)</span><span lang=3DEN-SG style=3D'font-size:=
13.0pt;
mso-fareast-font-family:SimSun;mso-ansi-language:EN-SG;mso-fareast-language:
ZH-CN'><o:p></o:p></span></p>

<p class=3DMsoNormal><span style=3D'font-size:13.0pt;mso-ansi-language:EN-U=
S'><o:p>&nbsp;</o:p></span></p>

<p class=3DMsoNormal><b style=3D'mso-bidi-font-weight:normal'><u><span
style=3D'font-size:13.0pt;mso-ansi-language:EN-US'>Q8 [LO3 - Criteria 3.2]<=
o:p></o:p></span></u></b></p>

<p class=3DMsoNormal><span style=3D'font-size:13.0pt;mso-ansi-language:EN-U=
S'>Compare
two motivational theories of intrinsic and extrinsic in nature that apply to
workplace or organization.</span><span lang=3DEN-SG style=3D'font-size:13.0=
pt;
mso-ansi-language:EN-SG'> Explain how would they be effective in <span
class=3DGramE>context .</span> (</span><span lang=3DEN-GB style=3D'font-siz=
e:13.0pt'>Assessment
Criteria: <span class=3DGramE>3.2;</span>)</span><span style=3D'font-size:1=
3.0pt;
mso-ansi-language:EN-US'><o:p></o:p></span></p>

<p class=3DMsoNormal><span lang=3DEN-SG style=3D'font-size:13.0pt;mso-farea=
st-font-family:
SimSun;mso-ansi-language:EN-SG;mso-fareast-language:ZH-CN'><o:p>&nbsp;</o:p=
></span></p>

<p class=3DMsoNormal><b style=3D'mso-bidi-font-weight:normal'><u><span
style=3D'font-size:13.0pt;mso-ansi-language:EN-US'>Q9 [LO3 - Criteria 3.3]<=
o:p></o:p></span></u></b></p>

<p class=3DMsoNormal><span style=3D'font-size:13.0pt;mso-ansi-language:EN-U=
S'>Evaluate
the influences or impacts of the motivation theories for managers how do
managers use them at work to achieve their team objectives? <span
style=3D'mso-tab-count:1'>&nbsp;&nbsp; </span></span><span lang=3DEN-SG
style=3D'font-size:13.0pt;mso-ansi-language:EN-SG'>(</span><span lang=3DEN-=
GB
style=3D'font-size:13.0pt'>Criteria: 3.3; Grading Criteria M2; <span
style=3D'color:red'>D1</span>)</span><span style=3D'font-size:13.0pt;mso-an=
si-language:
EN-US'><o:p></o:p></span></p>

<p class=3DMsoNormal><span style=3D'font-size:13.0pt;mso-ansi-language:EN-U=
S'><o:p>&nbsp;</o:p></span></p>

<b style=3D'mso-bidi-font-weight:normal'><u><span style=3D'font-size:13.0pt;
font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Ro=
man";
mso-ansi-language:EN-US;mso-fareast-language:EN-US;mso-bidi-language:AR-SA'=
><br
clear=3Dall style=3D'page-break-before:always'>
</span></u></b>

<p class=3DMsoNormal><b style=3D'mso-bidi-font-weight:normal'><u><span
style=3D'font-size:13.0pt;mso-ansi-language:EN-US'>Q10 [LO4 - Criteria 4.1]=
</span></u></b><span
style=3D'font-size:13.0pt;mso-ansi-language:EN-US'><o:p></o:p></span></p>

<p class=3DMsoNormal><span style=3D'font-size:13.0pt;mso-ansi-language:EN-U=
S'>Explain
the natures of group, inclusive of identifying the working factors that
influence group working behavior within organizations.</span><span lang=3DE=
N-SG
style=3D'font-size:13.0pt;mso-ansi-language:EN-SG'> (</span><span lang=3DEN=
-GB
style=3D'font-size:13.0pt'>Assessment Criteria: 1.3)</span><span lang=3DEN-=
SG
style=3D'font-size:13.0pt;mso-fareast-font-family:SimSun;mso-ansi-language:=
EN-SG;
mso-fareast-language:ZH-CN'><o:p></o:p></span></p>

<p class=3DMsoNormal><span lang=3DEN-SG style=3D'font-size:13.0pt;mso-ansi-=
language:
EN-SG'><o:p>&nbsp;</o:p></span></p>

<p class=3DMsoNormal><b style=3D'mso-bidi-font-weight:normal'><u><span
style=3D'font-size:13.0pt;mso-ansi-language:EN-US'>Q11 [LO4 - Criteria 4.2]=
<o:p></o:p></span></u></b></p>

<p class=3DMsoNormal><span style=3D'font-size:13.0pt;mso-ansi-language:EN-U=
S'>Discuss
the key factors that promote or inhibit the development effective teamwork.=
</span><span
lang=3DEN-SG style=3D'font-size:13.0pt;mso-ansi-language:EN-SG'> (</span><s=
pan
lang=3DEN-GB style=3D'font-size:13.0pt'>Assessment Criteria: 1.3; Grading C=
riteria
M1, <span style=3D'color:red'>D2</span>)</span><span lang=3DEN-SG style=3D'=
font-size:
13.0pt;mso-fareast-font-family:SimSun;mso-ansi-language:EN-SG;mso-fareast-l=
anguage:
ZH-CN'><o:p></o:p></span></p>

<p class=3DMsoNormal><span lang=3DEN-SG style=3D'font-size:13.0pt;mso-ansi-=
language:
EN-SG'><o:p>&nbsp;</o:p></span></p>

<p class=3DMsoNormal><b style=3D'mso-bidi-font-weight:normal'><u><span
style=3D'font-size:13.0pt;mso-ansi-language:EN-US'>Q12 [LO4 - Criteria 4.3]=
<o:p></o:p></span></u></b></p>

<p class=3DMsoNormal><span style=3D'font-size:13.0pt;mso-ansi-language:EN-U=
S'>Evaluate
the impact of technology on team functioning within an organization and bri=
efly
discuss the real benefits that brought by technology to an organization whi=
ch
adopts it.</span><span style=3D'font-size:13.0pt;mso-ansi-language:EN-SG'> =
<span
lang=3DEN-SG><o:p></o:p></span></span></p>

<p class=3DMsoNormal><span lang=3DEN-SG style=3D'font-size:13.0pt;mso-ansi-=
language:
EN-SG'>(</span><span lang=3DEN-GB style=3D'font-size:13.0pt'>Assessment Cri=
teria:
1.3; Grading Criteria M2; <span style=3D'color:red'>D3</span>)</span><span
style=3D'font-size:13.0pt;mso-ansi-language:EN-US'><o:p></o:p></span></p>

<p class=3DMsoNormal align=3Dcenter style=3D'margin-bottom:10.0pt;text-alig=
n:center'><span
style=3D'font-size:11.0pt;font-family:"Arial","sans-serif";mso-fareast-font=
-family:
PMingLiU;mso-ansi-language:EN-US;mso-fareast-language:ZH-TW;mso-bidi-font-w=
eight:
bold'><o:p>&nbsp;</o:p></span></p>

<p class=3DMsoNormal align=3Dcenter style=3D'margin-bottom:10.0pt;text-alig=
n:center'><b><span
lang=3DEN-GB style=3D'font-size:11.0pt;font-family:"Arial","sans-serif";mso=
-fareast-font-family:
PMingLiU;mso-fareast-language:ZH-TW'>- END -<o:p></o:p></span></b></p>

<p class=3DMsoNormal style=3D'margin-top:0in;margin-right:0in;margin-bottom=
:.25in;
margin-left:1.0in;text-align:justify;text-justify:inter-ideograph;text-inde=
nt:
-1.0in'><b><span lang=3DEN-GB style=3D'font-size:11.0pt;font-family:"Arial"=
,"sans-serif";
mso-fareast-font-family:PMingLiU;mso-fareast-language:ZH-TW'><o:p>&nbsp;</o=
:p></span></b></p>

</div>

</body>

</html>

------=_NextPart_01D227F5.5FD6A920
Content-Location: file:///C:/2A24B1E5/master_file_files/themedata.thmx
Content-Transfer-Encoding: base64
Content-Type: application/vnd.ms-officetheme

UEsDBBQABgAIAAAAIQDp3g+//wAAABwCAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbKyRy07DMBBF
90j8g+UtSpyyQAgl6YLHjseifMDImSQWydiyp1X790zSVEKoIBZsLNkz954743K9Hwe1w5icp0qv
8kIrJOsbR12l3zdP2a1WiYEaGDxhpQ+Y9Lq+vCg3h4BJiZpSpXvmcGdMsj2OkHIfkKTS+jgCyzV2
JoD9gA7NdVHcGOuJkTjjyUPX5QO2sB1YPe7l+Zgk4pC0uj82TqxKQwiDs8CS1Oyo+UbJFkIuyrkn
9S6kK4mhzVnCVPkZsOheZTXRNajeIPILjBLDsAyJX89nIBkt5r87nons29ZZbLzdjrKOfDZezE7B
/xRg9T/oE9PMf1t/AgAA//8DAFBLAwQUAAYACAAAACEApdan58AAAAA2AQAACwAAAF9yZWxzLy5y
ZWxzhI/PasMwDIfvhb2D0X1R0sMYJXYvpZBDL6N9AOEof2giG9sb69tPxwYKuwiEpO/3qT3+rov5
4ZTnIBaaqgbD4kM/y2jhdj2/f4LJhaSnJQhbeHCGo3vbtV+8UNGjPM0xG6VItjCVEg+I2U+8Uq5C
ZNHJENJKRds0YiR/p5FxX9cfmJ4Z4DZM0/UWUtc3YK6PqMn/s8MwzJ5PwX+vLOVFBG43lExp5GKh
qC/jU72QqGWq1B7Qtbj51v0BAAD//wMAUEsDBBQABgAIAAAAIQBreZYWgwAAAIoAAAAcAAAAdGhl
bWUvdGhlbWUvdGhlbWVNYW5hZ2VyLnhtbAzMTQrDIBBA4X2hd5DZN2O7KEVissuuu/YAQ5waQceg
0p/b1+XjgzfO3xTVm0sNWSycBw2KZc0uiLfwfCynG6jaSBzFLGzhxxXm6XgYybSNE99JyHNRfSPV
kIWttd0g1rUr1SHvLN1euSRqPYtHV+jT9yniResrJgoCOP0BAAD//wMAUEsDBBQABgAIAAAAIQAw
3UMpqAYAAKQbAAAWAAAAdGhlbWUvdGhlbWUvdGhlbWUxLnhtbOxZT2/bNhS/D9h3IHRvYyd2Ggd1
itixmy1NG8Ruhx5piZbYUKJA0kl9G9rjgAHDumGHFdhth2FbgRbYpfs02TpsHdCvsEdSksVYXpI2
2IqtPiQS+eP7/x4fqavX7scMHRIhKU/aXv1yzUMk8XlAk7Dt3R72L615SCqcBJjxhLS9KZHetY33
37uK11VEYoJgfSLXcduLlErXl5akD8NYXuYpSWBuzEWMFbyKcCkQ+AjoxmxpuVZbXYoxTTyU4BjI
3hqPqU/QUJP0NnLiPQaviZJ6wGdioEkTZ4XBBgd1jZBT2WUCHWLW9oBPwI+G5L7yEMNSwUTbq5mf
t7RxdQmvZ4uYWrC2tK5vftm6bEFwsGx4inBUMK33G60rWwV9A2BqHtfr9bq9ekHPALDvg6ZWljLN
Rn+t3slplkD2cZ52t9asNVx8if7KnMytTqfTbGWyWKIGZB8bc/i12mpjc9nBG5DFN+fwjc5mt7vq
4A3I4lfn8P0rrdWGizegiNHkYA6tHdrvZ9QLyJiz7Ur4GsDXahl8hoJoKKJLsxjzRC2KtRjf46IP
AA1kWNEEqWlKxtiHKO7ieCQo1gzwOsGlGTvky7khzQtJX9BUtb0PUwwZMaP36vn3r54/RccPnh0/
+On44cPjBz9aQs6qbZyE5VUvv/3sz8cfoz+efvPy0RfVeFnG//rDJ7/8/Hk1ENJnJs6LL5/89uzJ
i68+/f27RxXwTYFHZfiQxkSim+QI7fMYFDNWcSUnI3G+FcMI0/KKzSSUOMGaSwX9nooc9M0pZpl3
HDk6xLXgHQHlowp4fXLPEXgQiYmiFZx3otgB7nLOOlxUWmFH8yqZeThJwmrmYlLG7WN8WMW7ixPH
v71JCnUzD0tH8W5EHDH3GE4UDklCFNJz/ICQCu3uUurYdZf6gks+VuguRR1MK00ypCMnmmaLtmkM
fplW6Qz+dmyzewd1OKvSeoscukjICswqhB8S5pjxOp4oHFeRHOKYlQ1+A6uoSsjBVPhlXE8q8HRI
GEe9gEhZteaWAH1LTt/BULEq3b7LprGLFIoeVNG8gTkvI7f4QTfCcVqFHdAkKmM/kAcQohjtcVUF
3+Vuhuh38ANOFrr7DiWOu0+vBrdp6Ig0CxA9MxEVvrxOuBO/gykbY2JKDRR1p1bHNPm7ws0oVG7L
4eIKN5TKF18/rpD7bS3Zm7B7VeXM9olCvQh3sjx3uQjo21+dt/Ak2SOQEPNb1Lvi/K44e//54rwo
ny++JM+qMBRo3YvYRtu03fHCrntMGRuoKSM3pGm8Jew9QR8G9Tpz4iTFKSyN4FFnMjBwcKHAZg0S
XH1EVTSIcApNe93TREKZkQ4lSrmEw6IZrqSt8dD4K3vUbOpDiK0cEqtdHtjhFT2cnzUKMkaq0Bxo
c0YrmsBZma1cyYiCbq/DrK6FOjO3uhHNFEWHW6GyNrE5lIPJC9VgsLAmNDUIWiGw8iqc+TVrOOxg
RgJtd+uj3C3GCxfpIhnhgGQ+0nrP+6hunJTHypwiWg8bDPrgeIrVStxamuwbcDuLk8rsGgvY5d57
Ey/lETzzElA7mY4sKScnS9BR22s1l5se8nHa9sZwTobHOAWvS91HYhbCZZOvhA37U5PZZPnMm61c
MTcJ6nD1Ye0+p7BTB1Ih1RaWkQ0NM5WFAEs0Jyv/chPMelEKVFSjs0mxsgbB8K9JAXZ0XUvGY+Kr
srNLI9p29jUrpXyiiBhEwREasYnYx+B+HaqgT0AlXHeYiqBf4G5OW9tMucU5S7ryjZjB2XHM0ghn
5VanaJ7JFm4KUiGDeSuJB7pVym6UO78qJuUvSJVyGP/PVNH7Cdw+rATaAz5cDQuMdKa0PS5UxKEK
pRH1+wIaB1M7IFrgfhemIajggtr8F+RQ/7c5Z2mYtIZDpNqnIRIU9iMVCUL2oCyZ6DuFWD3buyxJ
lhEyEVUSV6ZW7BE5JGyoa+Cq3ts9FEGom2qSlQGDOxl/7nuWQaNQNznlfHMqWbH32hz4pzsfm8yg
lFuHTUOT278QsWgPZruqXW+W53tvWRE9MWuzGnlWALPSVtDK0v41RTjnVmsr1pzGy81cOPDivMYw
WDREKdwhIf0H9j8qfGa/dugNdcj3obYi+HihiUHYQFRfso0H0gXSDo6gcbKDNpg0KWvarHXSVss3
6wvudAu+J4ytJTuLv89p7KI5c9k5uXiRxs4s7Njaji00NXj2ZIrC0Dg/yBjHmM9k5S9ZfHQPHL0F
3wwmTEkTTPCdSmDooQcmDyD5LUezdOMvAAAA//8DAFBLAwQUAAYACAAAACEADdGQn7YAAAAbAQAA
JwAAAHRoZW1lL3RoZW1lL19yZWxzL3RoZW1lTWFuYWdlci54bWwucmVsc4SPTQrCMBSE94J3CG9v
07oQkSbdiNCt1AOE5DUNNj8kUeztDa4sCC6HYb6ZabuXnckTYzLeMWiqGgg66ZVxmsFtuOyOQFIW
TonZO2SwYIKObzftFWeRSyhNJiRSKC4xmHIOJ0qTnNCKVPmArjijj1bkIqOmQci70Ej3dX2g8ZsB
fMUkvWIQe9UAGZZQmv+z/TgaiWcvHxZd/lFBc9mFBSiixszgI5uqTATKW7q6xN8AAAD//wMAUEsB
Ai0AFAAGAAgAAAAhAOneD7//AAAAHAIAABMAAAAAAAAAAAAAAAAAAAAAAFtDb250ZW50X1R5cGVz
XS54bWxQSwECLQAUAAYACAAAACEApdan58AAAAA2AQAACwAAAAAAAAAAAAAAAAAwAQAAX3JlbHMv
LnJlbHNQSwECLQAUAAYACAAAACEAa3mWFoMAAACKAAAAHAAAAAAAAAAAAAAAAAAZAgAAdGhlbWUv
dGhlbWUvdGhlbWVNYW5hZ2VyLnhtbFBLAQItABQABgAIAAAAIQAw3UMpqAYAAKQbAAAWAAAAAAAA
AAAAAAAAANYCAAB0aGVtZS90aGVtZS90aGVtZTEueG1sUEsBAi0AFAAGAAgAAAAhAA3RkJ+2AAAA
GwEAACcAAAAAAAAAAAAAAAAAsgkAAHRoZW1lL3RoZW1lL19yZWxzL3RoZW1lTWFuYWdlci54bWwu
cmVsc1BLBQYAAAAABQAFAF0BAACtCgAAAAA=

------=_NextPart_01D227F5.5FD6A920
Content-Location: file:///C:/2A24B1E5/master_file_files/colorschememapping.xml
Content-Transfer-Encoding: quoted-printable
Content-Type: text/xml

<?xml version=3D"1.0" encoding=3D"UTF-8" standalone=3D"yes"?>
<a:clrMap xmlns:a=3D"http://schemas.openxmlformats.org/drawingml/2006/main"=
 bg1=3D"lt1" tx1=3D"dk1" bg2=3D"lt2" tx2=3D"dk2" accent1=3D"accent1" accent=
2=3D"accent2" accent3=3D"accent3" accent4=3D"accent4" accent5=3D"accent5" a=
ccent6=3D"accent6" hlink=3D"hlink" folHlink=3D"folHlink"/>
------=_NextPart_01D227F5.5FD6A920
Content-Location: file:///C:/2A24B1E5/master_file_files/image001.gif
Content-Transfer-Encoding: base64
Content-Type: image/gif

R0lGODlhdgJmAHcAMSH+GlNvZnR3YXJlOiBNaWNyb3NvZnQgT2ZmaWNlACH5BAEAAAAALAIAAgBy
AmIAhgAAAAAAABQUFBISEg0IBAAAAwgKDQ0LCgQAAAQIDQ0KCAMAAAAECgoEAAMGCw0LCwgDAAsG
AwAABAoLDQQABAQEAAADCAsLDQgDAwQICwoLCgsKCAoIBAoEAwoEBAMAAwQICgQECgMABAQDCAQA
AwsGBAAECAgECAADBAQDBAQGCwgICAMEBAgGCgsGCAoGAwgIDQMDCAgKCggGCwADAyoqKigoKEtL
S2BgYH9/f5+fn4SEhI+Pj6Kior+/v6+vr9/f38/Pz+/v7////wECAwECAwECAwECAwECAwECAwEC
AwECAwECAwECAwECAwECAwECAwECAwECAwECAwECAwECAwECAwECAwECAwECAwECAwECAwECAwEC
AwECAwECAwECAwECAwECAwECAwECAwECAwECAwECAwECAwECAwECAwECAwECAwECAwECAwECAwEC
AwECAwECAwECAwECAwECAwECAwECAwECAwECAwECAwECAwECAwECAwECAwECAwf/gAEAg4OCgoUA
h4SGhIiNjIiKkY2JlJCVkpiWm4qXk4+ci5WgmaWjmqSbqZ+ipquoraqxrLSnrJ6wjrO5vKadtr2h
tbi+u8PCwa67xMDFx8q3zdKvzNCw1bLRzqLU0taQOEBDQzo55uLk5jno5efj7evv6uzz8u7p9/D0
+fX48f77/umzJ7DfQIAEAypMyBChw4MQDUrkR7FgxYUPJ1rciDHixYYeOYLU2JHkyI8ZUYYsqdJk
SpEvWcJceXKmS5oxa/YQ4GOcz59AgwodSrSo0aNIkypdyrSp06dQo0qdSrWq1atYs2rdyrXrVSGD
0HkdS7as2bNo06pdy7at27dm/30IAAC3rt27ePPq3cu3r1+scun+HUy4sOHDiBMrbiv3htjFkCNL
nky5smWzQWrwvMy5s+fPoEMb3iy6tOnTqFOrRjqo5+rXsGPLnv1XyA7HtHPr3s27N9XAvoMLH048
N/DiyJMrXy458GPm0KNLn642M2nq2LNr3w4VwHXu4MOL5/59vPnz6IODBfA8vfv38E8fj0+/vv3K
8+/r38+/b+P2/QUo4IBlBdEagQgmqGBW5S3o4IMQDuWdaxFWaKGDtuF24YYcBphfhyCG+N6HIpZo
InfOnajiitRlNgCFRRFQgAFGHYBAAlrZiKNRCiywI1M9/hhUkCxCZmMASCbJQP+Rqh14VAMBOFDU
AxAEIGRVR14ZFAFIankUl1YO1WOYTC4WgQQTjBNBAGiWedqLRx1AQQUWXFAUkVnheaePTuk5JJ9u
JnZmmuNwWadQVHoZ6GHrAQhUBA7ISCNRflpVqVCXIpXpOJsu2tegPzUwo1A6egqZDwMINiUGBtgo
pU9UIrkkkVAGwICNGVS55BCxBlBnrBJogEAAMxq6AZ+9vjpErV0OASaSaCbrE7NkYrpArsQaAOwE
NkrAQZSmwgWqT2viuKasR7I5gbRcKhtuXqiqShQBuzbQJq8Q4CgpkR3QeGSxM1IpJZW/5ovvjh5M
EOQDrA5B66FQJlBqBL82/LD/nRFTusCo9q5r8BD9vlvXuIVGqcAHNO6LrMWAirwXqhoS1cCOXNJ8
qE89gnAjznyebAAB9xJJ8QVULnlACA77+CySDvjMqY9OKyDCBEubjPLTiia9Y6kE1HnACHa67BbJ
Q5Q7bbZEVu2u2HcFYQOcGicpq5o3P90s1lqP66rDKBMAAgkGlEBjkF2HrWbQPgrsbJ2F/zRup3ju
nSgBWbNdFtmi0liuyvrWbTlfcA8FaahtAk0o1hGMqrXWNY9T6rIMJNyACSfYSfi9haqeNrRpmv6T
pHiLCajTZaOQwumfp0Uyl0v6zrmzuCefl5OYTv1Tj1LauKsCKtDaJpELQ9Dm/9AlS9m61gTbSYAD
3aYZpJzIp+8s+wh83/KfOBbtk/bSrzXumuMbVeoMQCT5ra9/dcmQo8akuwUgyWvD8tWxsnWuJBVg
BQ4MWJV8ZTj4DcGD/2pVBHeVriTpTG7ZG6HrIpikrFVwbYIrHwK9UsIkKStdLHAgttCUruaBa4Zr
iZdnHtACnFlvK18zHBCXqBwhdoZ8ZVsbViJQOSZakTfxclRkevVDraxpV3uSmxjHSMYymvGMaEyj
GtfIxja68Y1wjOMZVWeWMcnxjnjMox73SMfPuK1BVwykIKsyoUEa8pBTASQiF8nIoDSqkZCM5E9I
JMlKGpKSg2wAGIF0taS8jv9ULGSaJQkEMy1a8QAYiJ4nEdBHqjTgVcxTCuVGaR4DAQBGhnSBsKoo
pk5a5ZU+Ad5REkXL84TOkERcVt3ABLG54QtJoxqT+SzwgiitSUpOY6YSl3VDBGDTgdj8AAat1KtN
FnM7ADjmIBUAA75NSmrcupECYnABeMLOWQFj1QFIIINhNW0BTROBsGYEz0+OA5jjEBjDWsXPYdVp
UAY9p3YUuEhqvWp0QOESDwHnTjtaCZ4gnZrPMCoUhH7Qmx5NgNN0FFGJYseJhzzADMh1qAhsUnvw
Ax/K7MkpkfpUYSizqcyUNUCeuvODgGupS6cDU0PGcIU4ipoKhAq/9PlMcZT/C6nCRLrTq3EPKAi9
Jr7Mp1I+FU6pS41OFpFZpTb1inG882idxkSDBaBpTAyIFbYSsCYTVAmu6tpfKKuF16Tx7qC26lRa
i+M2dS4WLsR7bHioJ1nI+rKy3HEsZtVyJFVuVjqP/KxoWYTJ0ZqWQ6U9rWoj9J/VunZD1sHla2fr
IEXS9rYBoixud9sfivL2t/xJLXCHix7hEve44UkRcpeLntgy97nmKSR0pwse21L3uskJLXa3yxzj
cve7u/EueMcrm9aS97y9saVs0cte2Vi3vfA1jXTjS9/U+La++C2NePPL31PNpb8A9oxyA0zgyTi3
wAiOjG4TzODRrLfBEN6L/3YjTOGX/bfCGNbLfjPMYbKYt8MgZsuBQ0xitby3xCjGyoJTzGKs3LfF
ML7KhmNM46ME5gfqCMI4cGwOHQ+BxznwMZCFnOMdF/nHRx6ykXu85CA3mchMRnKUlSxlJ1cZylam
spaTzOUpdznLX8aymJ9M5iuXecteTjOY1TxmM7sZzWuOc5vhPOcwn9nOb8Yzne/MZj7L2c917nOe
Bb3nQf/Z0IE+dKEXjWcezAUbk/iFpJHBiEk/IxaWTgaml+GNTWuD0tc4BaQ1kelRV5rT2zj1p7tx
iVZ3OtKoBrWrM/3pUr+a1LFmtah3nepQ09rXvDYGsG3d61kL29jElnWwLwYN61UvIhAAOx==

------=_NextPart_01D227F5.5FD6A920
Content-Location: file:///C:/2A24B1E5/master_file_files/image002.jpg
Content-Transfer-Encoding: base64
Content-Type: image/jpeg

iVBORw0KGgoAAAANSUhEUgAAAhgAAAC8CAYAAAA6ltfBAAAAAXNSR0IArs4c6QAAAARnQU1BAACx
jwv8YQUAAAAJcEhZcwAAIdUAACHVAQSctJ0AAFDZSURBVHhe7Z0JeBvF+f9jjkBbbmhpKVcp/5am
BQqBAiFEsWVDIIclOaYtLU1/QNMC5UxCTltSKIUWKFcIueNLCaTlLOVuwxECSRzfOclNbse2JN9O
Yv+/rzxSVtJoNavDR/x+nud9nGjnfWd2dnfmu7uzM/0YhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEY
hmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEY
hmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEY
hmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEY
hmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEY
hmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEY
hmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEY
hmF6Nin2JVX97UuWnvTAwtdPmzTz7dMfWLj0tAnz3jj5vufeOaFfR0eKSMcwDMMw+nSg07jJvvDC
1Kl5PzRqGeMKviXCJAXzpPwzZflGM9M9M04SIQxhGmM/MfXRxdKYekblFCEMkZ2dfazZPv8iWcxI
NsxeeIpwj5tr7n/2bNO0+SPScwvsabkFS4ZOyytNzS3cme4orE13uBoyHEVNNzpdDemOorq03MJd
Q6fll2P762Zn0V9SJ8+1/eK+J85FGBYdScRutx+Xn59/ZnNz84Xt7e2Xwq7ENftz/P1JeXn5uc89
99wpdA2L5AzTZ1iyZMmxc+fOPWP37t0X4Hr4GV0bsCtgA7766qvzZs+efSqun2NEcqY7WLp064m3
PLZ4e7qz6KBRS5uyYKQIkxTSHEXjZPnqm+ugacrcu0QIQ6ROmnVjBvzlcSNb2rS8F0UIQwzInnh+
+vRFjbKYkSzD+YpVuMdE+sTZp6bZC++CaFiKumrE346YzelqNjtcX5jtBfdf96cnvyOySDhVVVX9
VQwd7bHCpVdCDSb24+KmpqYxBw8efLG1tfXztra2PYcOHWpEo9kGOwQ7LP4exO+tMC/SbsHfd5H+
sdra2pEbN278dk8UHcXFxceHHjOZoexJ7RRkecosGeeTLB89QxmOX7p06XFkdH6Q9cVOk/Z53bp1
F9bX19+G8/1ZnO8f4+8uWOPhw4dDr41D+K0V2+pxTWzHNfQh/v33xsZG26pVq77XE68NlOk42fEP
NTr+wiUpqF6jdD4KF32EwMBdq6QDiWKpk+aNEmGSgjmnIAudmDRvPRs6dcF0EcIQNzzy0p2yeNEs
LSf/TRHCEKk5eQMN7Z/TdXjwuJkDhbshBk+aeTr8c9MdrgPS2PGa0+WF/cM0/u/fFVkmBDQqJ6Mx
OYBGwxvN0IjEdNy7E2owIAouR4P4V5R/PfbjEBqbuECsZmqAm5ub/4TGIKHHIx6wb0u1xyuSoexL
sBtqDZhBli9f/g3Uz3ZZvqFWXV39gnBLCJQ3jvFeWV6RDHXhRafqQedYC9uPY/o1Osz1iLMCned/
sC/zkMbu8XjuwHl0Q2lp6fdx5368yLJXQ6LiwIEDA7C/udjHctRHIq4NEh5fIN4DO3fuPE9k1e2g
XC+HHnuZodwf4gbiBOGWUFA9KQ0NDWtk+YYa0i0Rbvr0aIExPf+KjBgERrqjMF+EMESqvWCaPJ6+
pU3LW9kvO9uwsjRNnm0xKDBaY+jAU1CHI+G/NSxeEszsdO1Lzc2/PVF3WUJgNHc2D/qg4XhCuPV4
qJHAft2KhmUZLtiDYhcSDuJ7YHNqamp+KrLuNrCfX4pi6YJ07ehU7hduCeWtt976JurjgMhKF3Ru
c4RbQhACo16ETwrYt2bU3WbYYpxfd27ZsuUCkX2vge6OIZhGofwf4VRoFbuWcFBXDTgeLgjJmG7a
Egn29Q1RLF1QHx0Qlo8Kt4SC8CQwtnbmpA9E71vCTZ+eLDCuufOxs9GpNsvy1jOzo+i/IoQhUqfM
nyWLF83M9sIdw+57zrCqzHAuul8WL6I5XXvPvfbabwj3qNAYj7Tcwkd9r1dk8ZJlTtdh7Ntsk33h
iaIoMXO0CQxqPNFA3Ip9WksdqSh60kFWLaifhbt27TpfFKXLQRmUBAaBxr8RHfw1wjVhHO0CIxQ6
7hAb/2tpaclK1p1vokBxj0EHdwuOT0lXXhvIj143/hOi5keiKF2OqsAgUDWH3G73jcI1YSB03xIY
1z709DfMjsJdsrz1LMNRtHHgWOOPCdEZ/1sWL7q5vJfeNul0EUYZCJOn5PHklpaTVyFco2Ky248z
O4tmyuJ0mU1f/BodQ1GkmDiaBEZdXd3PsS+fiOJ2C2hM6xobG++nd62iWF2GEYFBIP3anTt3xjSA
OhJ9TWBowX5X4Nhn9sQxHOjcfwwh9J+uFBahoH4aUIacHTt2xNVmxYIRgUGgrDtxftIg+4SBsH1L
YPQzmY5Ly80vluWta86iepPd8Jckx5gdRauk8aJb++AHZ14k4iiTbi/6lyRWRDPb89UOar9+KemO
RXZZjK62DOeiuTh1Yx5UdTQIDOrMcQc5GY1Coyhqt0KNOOr0fXQ2CW2gooFsDQkMAse0CH8S1iH2
ZYFBYN8PoxP9F47F2aJY3QqJHZTnbhTLLYrY7aAsy91u98WiiF2CUYFBwOedRN4oIGQfExggddrC
N2R56xk6NXT4/zDU4dMrjnSna4csXjTzjROZmj9YhFLGnFOwQhYvkqEulAadpdvzb0l3uNpkMbrB
2lGW/xNFM0xvFxglJSXfRgP6tihijwKdzE6UzfB5GyuxCAz4HKbBqiJE3PR1geEHdbCjpqbmOlG0
bgHFOAn1QYMbu+2pRSTQ5tSgbMNFUZNOLAKDaGpqyhEh4gbh+p7AMOfkPyvLW88gMDqGPPj8DSKE
EqaHZ59Frzpk8VTM7HD9RoRS4oIxY06En6GBl2m5+eOEe0Rongyzo2ijzL+7LMPhOjBowrxzRBEN
0ZsFxvbt2y9CmapE8XokaNvrUb+3iiInlVgEBoHy1aPh+7kIExcsMI6AevACiyhel4Lsvxvr+dBV
oHytsLvxz5ifwKoSq8BA+ZogwFNFmLhAuL74BGP+Q7K8o5lp4tzfihBKpE6b92ManCiLpWT2wkki
lBJX3/vXM+HjlsaSmqsjzV4QtTFA2onhvt1vEH3PiiIaorcKDLfb/UM04FtE0eICjQhCHaZPw/bC
duDi3tXa2lpLDaBIEhcUB8T8lEkV5BNzh4JzoGLfvn0xTaCnhQVGMDgmjS0tLZmiiF0ChPc5OAYV
oghxQdcGrB538r5rA0ZP5WrwW4tIEheIcxCx6cYuqSIjVoFBoC63ok2I+5UXQvU9gZHhLLTK8o5m
qdPyJosQSgyZ8GIqPfmQxVKy6YsMTbY16E/TSdC0SmPJDOIHYuty4S5lwD32k8y5hdul/t1tzkWe
QRNeMPwUozcKjMbGRmpAvxLFMgx8aR6Lz9BQTqmrqzPTjJ1PP/30N+h9td/oaxSXy3UWfWKHdGOR
/l/wqxUhDIOGtI2+bhG7kBSQR1x3rDi+C+KdaKi3CAyUsRHH9IDGamBuHOsmbKM768Miadwgntfj
8QwSxUwqNGgX+RWLrA2D/SYxvBLmqKmpGVZcXHz+7Nmzvxl6beB6OQOC9HLc3d+BOnchz30ihGGQ
5yHU+71iF5ICjm3MAoPA/r2FfY9r7hiE6XsC4/qHXro8Y7rxjj91yrxZIoQS5mn5t9FTAlksFUud
uuDfIpQSQybMvMFQfk5XY9rkuboqNd1eZEPa9jBfY7Yp3Vk4Y+jkuX9Mn5Zvo8nOzDkL7slwFM3F
tpjOE7+Zc/PvE0VVprcJDBTjmyjHp50lMgYaib0wx969e2n8kOE7JtwZnk4TLqG+Yro7RN50J5i0
jiZegUGdKvbtdyJcTPQWgYF0f6GO028zZsw4iaaFf+qpp86iT41Xr159FX0RguM1CXXyT9jX8YgO
+CfkLlgPZHM88nmzM0dj4JjVQVT83ev1XoL/Gr42qB2B/6+Q/wrUk+ExH3BpQec7QoRLOChXXAKD
9kk8aYkZhOl7AsP0wDOnoXM1PI+D2V7wtgihxNApc+J7tZBbsLJfP/XPv1IfmWVU0Oy84IILdOeV
SLcXvizxUzOnq95sL7yD1mMR4cIYdMffTk53Fo2HqT950ZrTtRRhDDUOvUlgoAgpKMOznaVRB20D
3A4+uXv37m+LUHFB8x2gMb0DcfeLLJRBXW+FX1KmfUfcuN+5o6OpjWfSsN4iMFBXduGmBO7aT9yz
Z881iP8MfGtEGEPgLv01/DHceavS0tIyFWUz1LnjWB3CPs2prq6OaQxXKPTVBc5xmofma5GFMihL
dV1d3Q9EqISC8sQlMAhUbTNE57UipGEQou8JDJrPAh2T4fKhsyyDu/LjVHNu0TOyOKqGu/Nt5147
Wvn76bTcPEOCJi03HwImcud8gclEg0Z3h/opGU1mljPfJEJFBULmVrOjyPh4FWdRfbSnMKH0JoHR
3Nw8DBd5myiOEmi0NiVrNP+2bdu+h/jviKyUQX3/MxlrHqBuEjKoD/u0Gh3OySKsIY5WgaGFnmRB
YE5HjAYRThn4/VqESShut/tq6gBFNkrgOO1Eh3eTCJFQUJ4zIKgMf8GCa2NpMuaQSYTAIFBnX8V6
owL3vicw6KmAOSff0OecPnO69hhZ7TV18nxDc1KEmbOoznzvC8qTAqXbi54Pi6FjaVMXvipcpQx5
4MWfIN2hUD81K/ybCKNKSkxPS5yuDtOk2ekihhK9RWDQAERc3GtFUZRA+s9wQX9PhEgKtDBRU1PT
s2hHlR+fI+2hZAz8Q9yECAwCHeGL+GP4brsvCAw/uNu+Avu6XoRUAuXbig70VBEiIdCYCJTjC5GF
EkhfRl9hiRBJgcoFkZFj5NogvF5vTItp6pEogUHgGL6MP4ZvEODTFwVGv364W35Flr+ewaflhgdn
KTfeZkfh57I46uZqu/quScpTzZrthW/J48gtddr8p4WrlHR7Po2/kPpGsebB9z5leIrctJwFN0Aw
GH6KYZo4+yERQoneIjDQSD0kiqEEyvoJOoDThHtSoacRaLAfQxmV79aQfk2iZzRE9gkTGIh1CMLp
lyK0Mn1JYBD79+//HvI0NCYHnW5c7/JDwXH6rQitBI5PebLHg/hBdiko38Oob2WRgfJ9TU+JRIiE
kEiBgX1pb25u/rMIrQxc+6bAME2aY2hKbTL6IiR10ly1RWwGDjwed+SbZHGUDXfnGc6XrxcRo2LO
LSyXxolg5pyFuos/pdsLxsv8opqzaA2tWSLCKDMg294f/tVh8aKY2V5g6HPV3iAwNm7ceArKuEsU
IypIuy5R4y1UIZGBjmOOKEJUqJHyeDxxDagMBSETJjAI1GMtGlJDMy72NYFB1NbWno993i5CRwXn
ydZly5bF9AoqFBobgn1WnguGriN0+F2+Xg7K+Fc650UxooKOeLxwTQjY74QJDAK7Ut/Y2Hi1CK8E
3ProE4yc/Htk+esaOvy0nDyrCKELTU4Fn7qwGEaMHv9PnvcrEVKXC8bYT8xwLt4rjSMxmil0yMRZ
I4W7lHTn4n/IfBXsNRHCMOZcYzORkg2dMu+fwl2J3iAwcFH+n2rjhIa+BRd+whfxUqGqquok1OVK
UZSooKyr6LM/4R43qKKECgwC+7PcyJOWvigwCJyjyuOD6FxuaWlRajujgXyHUzwRWhckO4h8bxGu
XQqy74/z4oPOkkQHaTck8glfogUGgTKuMfKUFC599gnGCFn+0Wzo1AUPixC6mCa+dGG6w9Uii2HE
MhyuCSKkLlff8/fvIr8GWYwI1p42ee5lwl1Kmr0oT+IX1cz2AkOf82pJnbrgHVlMPTPn5n8g3JXo
6QID2dKXI593liA6uIB1X3UlG7fb/Qs05EodDRqoQ+vXrzd0F6QH8lUSGFQ+mPLdJOr/afxRGo/R
VwUGQqY0NzfTui5KtLW1GboRiEBKa2urcscJcVGIP0rHMRlUV1f/GG2Nt7M0+tD5WVNTc7NwjRtV
gYFsD8KUX+fgfFOuU0rXNwXGuJk/w1284fkd0nLzlBrzNLvrF0gf7/wR6DwLldYKGQyxkG5kCXVn
Uf1142bqfjpodhYtkvpGM3vB8yKEYVKnzTc0joQsw7HoY+GuRE8XGKWlpRfigj8kiqALOjbPpk2b
kvIJqCooRgrKsbCzRNFpbGycLlzjBvWkJDBwvJejg6NPJpXA/hxUnfK6rwoMYu/evZcirtIMlzgG
B7Zu3RrXGKHPP//8O9jfOhFSFxwTmlH0x8K124Ag+rsoUlSampoSdn6oCgxcj5tQT7PEf6OC403j
Me4Q2eiC5H1TYFw+xn5axvRFhufCGDplvu6XF36GTpmXRa84ZDGMmNlR9KYIqYtp/IxhRubAQNzt
NOZBuEtJi3UOjC4WGNjvT4W7Ej1dYKATukNkHxU0DC8Jt24FHccVaHeU7oJQp18Kt7hBnkoCA3ku
Q2d4No77JvFTVNBB7QFR5yjoywIDYVNQp0qvAej82LFjxxDhGhO4wx8uwkUFHXvMr2oTybZt236A
OlJa9Rjn0YaxY8cm5JNVAwLjK3rVibzLxU9RwbH01NbW6j4BJ5C0bwoMkIJO1vAcD/D5gnw7Q0Qm
PbfwAZm/UTPnFhSLkLoMnTr/Tpl/RHMWfQ433f1ggdE9AgOiYZ7IXhdqsNEBpgm37oZe65SKoumC
hqxt6dKlca8BQhgRGPhDo/uHwKep89fooKwfQzzpTkbXlwUGgU6fFu9Soq6uTukVcySwr38VoaKC
cimNX0s2NOYIYuc9USxdcIxaEjXxlhGBQelRxkvxX+Ul7lHWUpzPp/gyiwCS9VmBQYt4Gf+M1F64
jSagEiEikjZl/pNSf4Nmdrq2X2AaEzW/1KkLHTL/SJY6Zf7LwjUiLDC6XmBQY4SyrRDZ64J0dYWF
hboXeFeCBmq6KFpUqqurEzIOw6jAIJ+2trZHOn+NDuLTlKiP+TKLQF8XGDt27PgZYis9vYJ4ni/c
YgLH7l0RShccj2bU9feFW7eDct8vihYVj8eTkCXdjQoMAmLgTtSd8ngMHM/ZeoO2kaTvCgx0oEWy
Muia0+Ux2WefJUJEBGkLw3xjMLOjyDNwrD1qfmZHAa3rIY0hs7RpeY8L14iwwOh6gYEsT0TZlBZR
QtnoaVqPAQ3jSFG0qOzates24RYXsQgM/KWpnf/t26AA2tuDuBse5stQQl8XGHv27PkWOlClRfFQ
T4bGS2mB+zHY142dkfRBPuto0ivh2u2gE79aFC0q6LQfEG5xEYvAwH+PQd0pj6fCOUVzx0RcZRxJ
+q7AGDp14V9lZYhih2gZdhEiIkOnzP9E4mvcnK6WQQ88EzU/s73wA6l/BDM9Mutu4RoRFhhdLzBw
N3gGLnClLzLQqOcLtx4BOpqfosE5KIqnS11d3YPCLS5iERjE1q1bv4t63tG5NTpIuwsNsfSOuK8L
DJoPBdeT0rwUOGepMwscByPQazXsq9JaODge7wq3HsGbb755Nuq/VRRPl9raWqOzIEuJRWAQFRUV
p6OslWJzVFDXtRBFlwj3ILC5Tz/BuEtWBl2juTCcBRkiRCRSzI7CdVJ/49Z+3QPPRBsYlYJ0yvnR
HBimSbMi3pH5YYHR9QJj48aN5+LiVp3/ols/Tw3ljTfeOAf1qvRFARqdXOEWF6iqmAQGAZGTptro
E9i392iadOEeoK8LDKK1tfVjkY0uqMP9JEiEmyFee+21MyFQlMYIeL1el3DrEcybN+9k1L/SU569
e/fOFG5xgbqOSWAQuD6vRHmVPq8lkNeqXbt2fVO4B8Cmviswrn/gmZuRp8FPSV00z8NYEULK2Zdd
9q10R6HhGSnl5kKHnXerCC3l4vvuOyF9ukt5Ui/6PPe6cc/+TLhHhAVG1wuMNWvWXIALW1VgRH3N
1ZU89thjZ6O+lOoVjcmjwi0uUFUxCwwCHaNDtb4J3KlNE64BWGD4xhgoLYBHd7u0Mq9wMwTq+Szk
4xGhdKmpqVko3HoEdI6g/pWevuzevTsh50g8AoNobm5WHrxL4FoKa/fxcx9+gjH5pQGG5o4QZs7N
1x30lXb/s2ej04t7ki2/pU6ZpzvZ1lW/f/S8dKdLeblzs6OoQWURtW4RGDkLaHpyGr+ibBmOxTnC
XYmeLDBotVI0REqDrHraE4wvv/zyXJRJ6QlGU1OToWMWCdRVXAIDv9F4DOXZFpFfCxreoC93WGD4
BIbqp6o177zzTkwC4+233z4d+SjNgeH1ehcJtx4BDcbGviuVvSc8wSBo8CYE9WKRLCrYv8OIlSXc
feDnviswrr590plm5yIjs1/6zGwvLBAhpAwe/+Jl6PBjXIU03Mz2It21Nsw5C65GfgYWCXNtybYv
0Z0Dg+gOgdEV9GSBIcqm9BklGttC4dYj2LJly+VoZJTGYNTW1iZkIBvyi0tgELjbPQ8C4evOlNFB
2q204Jdw7/MCA6HpE2XV47AvlnWKiFdeeeUbuDaU1ufBvr4n3HoE9PrQwLWRkDYnXoFB0AJsiLNB
JI0K9nG/2+0OrOWDn/quwIBEOwYd89eycuhZ6tT5uo/kzfaCYTK/OOxfIrSUIY+8aKOxIRI/qZkd
rk/gJm1stbDA6HqBQe/4UbZtIntdULYVwq1HUFdXlyWKFpVdu3b9WrjFRSIEBoFG8GbEMjIe403/
Vwp9XWDQOQuxu0VkowvKuk64xQTyUZoMSnSKPeYrkqampus7SxYddPjd9hWJDGy/FueO0kRhBK6F
T/xrquC/fVhgALOz6DNZOfTM7Cja2M9kinjypscyeFTHkN8qEVoKOvQ/y/wimTm3QOnOlwVG1wsM
QDMjfiSy1wXpPOjkumR5dhVQV0+JokVl9+7dVwm3uEiUwAB0F/6XztTRQb7tqH/f6pd9XWCUlZV9
R/V6QroPhVtMYF9fFqF0wfFoxTl2gXDrdiCMJomiRcXj8SRkcTbUdUIEBkGfzqJOlccqtba2/h1u
Kfhn3xYY6Y7CfFk5oljtReljTxUhwki3F02X+MRsEBi79B4rDp08x9CkXhAYTuGqCwuMbhEYNLhK
qaNGh3FYb36GroTe16Ku1oui6YKGqqW4uDhsxHksJFBgUKwTcF4s7fSIDtK3oP4H9XWBsXnz5nSR
RVRQ1heFW0ygo54gQkWltrb2d8KtW6GvZrDfn4pi6YJj1FxXV3ehcI2LRAoMJDsO8f7Z6REdXA9t
iEvz4vTxJxiOIqesHLrmdLXcMG5OxOlc03Ly5kv9YjQaJ3LJ7fdGHJRpzi10yfwi2dBHXrpTuOrC
AqN7BEZ1dbVFZB8VlK9HzIWxb9++wWgcle5wUGaapj4hIMuECQyC7nrROCqN9ieQ9itAYzj6rMCA
IFZezAuC7I/CLSZI0GE/lAZB4y6avmyJesyTDY75AJRZdUG4dRDrCXm1g1gJExgELaqImJuFW1Rw
Texuamq6oE8LDNOUeb/3fQoqKYuOtQ8d/9L1IkQYZnvhexKfOMzVNmTCs/9PhA8jbeqCZXI/mbk6
UqfOUVq/ggVG9wgMGi2PBqlBFEEXXMTNKgtyJRMUg17rvN5Zouh4vd6EzIFBJFpgEGhwRyGu0oA8
AnfVb+I4KM1xcLQJDISlzy+VOh2q040bN14hXGOCFuXCuaY0QRrya8WxGShcuwt69TZXFCkq6JAT
8gUJgXpKqMAgaCwJ6lV5LR/U//8gQJUGUB+VAuOGiTPT0OkaXlbd7Fj8SxEijNSpC9bIfGI2p+vw
4IeeM4nwYWD7FqmfxDKmLzp81R3TlJYwZoHRPQKDwIWp/DgSaQvwp9vu1NAwZKDRUbqrpE5m69at
PxeucYN4CRcYgDoF5fEkKIPyu+mjTWDU1dX9XoSPCq65rUuXLo26rlI0WltbZ4qQUcG18U6sE3sl
AlpzB/Wu1CHTeYTzI124xk0yBAaBOlVey4eg/RL/1OWoFBg0DTe98pCVRdecrkkiRBAmk+k4s6No
n9QnDkubtlC6MuCAAdn9UZZmmY/MULaGX9xnV1ogiwVG9wkMt9ttpNM+7PV6rcK1S9m1a9dZyF9p
fQgCd/qf40/CxBDyTobAoLjfQFnJJ6EcTQID5+gZyFvp6xECwuAZ4RoXEDU/x74oPWFCusO41nUn
RkwW+/btoyXQi0VRooK0VbFOQiYjWQIDLsfiWP6n0ztxHJUCg+bCQL7Ks2D6zWwvmCVCBHHutTee
kTF9kXKHr2qmqfN8I9ZDuWHqgvMynIuUn8CY7UVbBo4de7xw14UFRvcJDHoPizIaaZyqcYFeJty7
BGR7Iu5m3uosQXSosUcZdWelNQpiJkVgEM3NzRfjGCiPx1DhaBEYCEeDeo0sitW2ffv2hHw5hHC0
4rDSqqoEro0GerQv3LsEZHssro28zhKogWvjz8I9IaCOkiIwCJT1bMTfLkIkhKNSYFBni3w3hZZD
waSL6Qwa/9wPaa0PSXqZKc8iCkEjnWwrfWr+YAP50RMM5dUMWWB0n8AgPB7PCDTMh0RRooJ92oY7
i58K96RCcx+gbvJRPuXXA2joSxLxiFwLsk+awCDQMWUbOQbROFoEBjrPiYinvKw3yvixf96QRIBr
4xojx4UEOMp8jXBPKvRFFa7DZ1E+I9fGZpwbSk+WVUmmwCAgwM3YRaXBqyoclQKDMOcWGF75NC0n
b61wD4LGSih1+E5X89BJc5U/kTXbC5eILIIYmlvwG1n6yOZSnqOfBUb3Cgx6d4yGSvkJAYGGaj8u
/GiL8cVFbW3tqWhYlBovP0h/MBnlQtykCgzyge/zviAJoLcLDIQ5Dh2SE7GUxQWlRRkTfuwh/haI
LFRx47rPxt+EvaILpaysjJauN1Qu1E87rvOIY/piJdkCg8C+5lD5Rai4OGoFBjr7PFlZdM25qPri
YcPC3peZpxfdJk0fYhkO14G0aQsekG2TGQTGlyKLINJyFj4iSx/JzLn5ymtAsMDoXoFBoFO+iESD
KI4SSN+Kcj8mW90wXtCgXIW6U17K2Q/KM4/u7ESYhIG2LakCg6B6RJ2u6IwUH71YYKRAWF6G46+8
bosfnC9JGWiJ/fk2jouhx/TwOYjOfEZdXV3CJ6hraWn5CcrzhchKGfi8mYz66QqBQWNGkI/y6yo9
jlqBkTHdlSMri645Xa0Dsu/5rggRwOwonCBNH2pOV+X141+w4q98e7jtRPiwBjJ12rwZkrQRLTU3
/3bhGhUWGN0vMAg0iLehYVS+Y/SDsleh0bMmovGiwZwox9NoDJU/UfODul5LaxqIUAmlKwQGUV1d
fQn2Q2klTz16mcBI2bdv33fp/IPQfRP+SteMFvg0eDwepa/WYqGhoeEm5KE8xbsfnMebsV+/S8Rr
G5wbJyMWPdUxfH6gHDu169okEpyvSRcYxM6dO2keGKU1YvQ4ml+R/MZAR++39iEPzwz7pjvDUfSC
JG2YpU6b/+6QiTOvyHAukm4PNYig1oFj7WF3pKk5ea/L0ksN+zhkwswbhGtUWGD4Oqb5aMQuj8XQ
uQ9AiLgfx1IMlONxX4FiAPtajg7izyjT9/Bf5fJQ44uG83IYvU9WmuchFDQ8NfBP2rgQlKtLBAbR
1NREQi+u8RjdKTBwHrzp9Xr/oDW32z0Wdjc6uQdqa2sn4Tz5G37Pw7n7EeJux/4aFrZ+cOzbEe8e
UdSkgbKOi7WcbW1tG+A/Hvt+vpEnbHA9Fn6XoFN8HPW6rzOaMVBmbzIHn6JcXSIwCBxn+uqtTYSM
iaP3CYaz6HrjAsPVMXTKnEwRIoA5t1Cpwzc7Cl+6xPrg99Kdi5RWXaX5K678w6M/FNkI7MekTlu4
UpZeak5X+zVj7MrT0LLAiA9ccLRomdIXO9EoLi4+Hg1+YWfk2ECD34TOfhkalL+jYbsdHclg3H39
GI3rBXv27LkQ9tO6ujozfr8bF/scNL5rsA8xd6jIrwFxzGIXkgLK12UCg/wRZ7YvYIx0p8DoanD+
LEjGa7FQkNWxONf+0ZlrbMC/BW3CCpz7z0KI/x+uiSH49yX+awPH7Sc4l4fi+vgDhMWLSFuGc095
MrZQ4NuMGNliF5IC4neZwCBwHiqv5SPjqBUY1HGnO4taZeXRM7PdFaLOO1IgPJQ6fNPEWdNMdjvN
meGRbZdYe1pOYdDThwF2e3+zvXCHJK3cnIu8A0zZJwn3qLDAiI9ECgyCvsBAvCW+4AkCDethlLMN
fw7SP8XPcYOY9YlatEkP5NNlAoPYvHnzqainks6oxukrAgPX11vJGAMUCWR5PI7LjM7cEwPOLd+1
ISzmJzmhIFYLBP5vRdGTRlcLDPFl2ScirGGOWoExcOwTpyLvA6FliWr2IloxLsCAbHv/tNx8pQ4/
bVoenWApZnvRWtl2mQ0ZP/O2zpw6uXzMA6elO12NsrQyQ55bhasSLDDiAw1JQgUGIS7i2YidMDGQ
aFC0PWhAh4giJxXk1aUCg6ipqbkUnVldZ2Rj9BGBsQR3/d8SRewyxFdXf8U5kTAxkGhw3rjp83NR
5KTS1QKDwHV/PvZxtwhtiKNWYNBsmGZ7oXJH7zezo+ifIoQPn1BxLor+RMLpar/hwed964GkOwrf
kaaRGMTLOF9GApO94GLVMRyd5vqfcFWCBUZ8oKFLuMAgEPNYlJ/mIUjYN+iJAo1LKb2bFkVNOt0h
MIjm5uY7YunIjmaBgfpAUQ4+hn/2F8XrcpA3rd75B5yHjb5C9SBQpq/q6uquFEVNOt0hMAjEy8S5
YHg8xlErMIjUaXkfycqjZ2anaxVcA43Wpbc9eFHG9EUKk2e52gZPeuFH5JPhcL0kTxNuqZPnPOfL
SJA6dUGakUm20nML5gtXJVhgxAcusqQIDD+4INPQaClP1ZxMqLPF3eNM/D1ZFK9LQH7dIjAQi2ay
NDRTI3G0Cgych9shLOmVWMLqOB7a2tp+gWu8ShSvW6FrAyzC34grYieD7hIYCJmCtuDJzujqHNUC
I91RYHiJ9bTcgl00jkKE6Dd00qxrVAaLQhS4r73Tfgb5pOYsnCRLI7MMZ/BkWxAq/ydLF9GchVOE
qxIsMOIDDUpSBQZBn3/iYn4BeXXb0ww0nlW4ox8mitSldJfAIOjzRNr3zhzUONoEBurfg/Pv7263
OymfIccDineSeGWitDJxMsD5sRkdZ1In94pEdwkMYseOHbSWz6ciCyWOaoGRlps/VVYeXXO6mgaM
fsgnFIgbHnohS0VgpDuLtvbL7pybIDW38NfSNBJLm5a3HC6BEzU9t8AuSxfZFv1auCrBAiM+ukJg
+GloaLgS+/Q68ox5ZLtR0IDsQOP0QHe8b/fTnQKDQAd2JepBuQM7WgSGOPaP0eedoig9FvoaBHVU
hHPF8HwZsYL62dfS0jIF9XOqKEaX050Cg4DovBh1rjxJ4NH9iiQ3T7mjD5hzUfvAMZMD75vNjkX3
SdOFmetT4dLv2vueNkGUHJanCzGna6v2iYk5J3+BNJ3MaMn38c8b+uaaBUZ8dKXAIJBXCq002dTU
NAsNXI2vEAkG+3QIsVc0NzffhX93m7DwgzJ0q8Ag0ED/CeVQGo/R2wQG9otohe1Fh/kJPa1AR2BO
5KqfXQF2JYWEBsr/LM7fPZ17l1hQR7Ryaznq53762khk3W10t8AgEDuL6kVkpcvR/QTDnvcLQ+MZ
fObqME2aHVi/Hx3+E/J0wZaWk1cgXPoNfnDmRej8lRY9MztdjdrJtpDf+7J0UnMWtV4/cbahu43u
EBiDJ882pU6ee5sRS5u8wNA6B9Qo42ReiJO/MJnW0NDwLK6bhE8BrAI9vofQyG5ra8tDg7oJF3k8
3+x7qYNGA23HHdnlCN/lj3sjgXp2hta7zKjs2JWklJu+XkAeSuXYsWPHncItISDv/jiX58nyimYQ
iXk4Rxag030Jx/e5/fv3/23fvn0Oj8fzEGzMnj17RuAu9CqazTMRs8H2FOjzWez/SOz7bHTC63F+
xzxBFHwbUXcrUZePIebV+Cnp836ogmP4sPZ4RzKI3qeFS1JAPU+Q5RtqaLOCPmKISG8UGL+4y36u
akevtdQp8+4QIfqhEy+SpQk1s7PoL8Kl3wWmzNOUvjyBZUxfhM67UyQMHDjweHNuYbksndxcB84c
NMrQALzuEBip0xa+JY2pa0eeCDHh0CRd6CguwkVsQ0c7Ff+ej4b1bTSKX0AEVeG3DWTYXo671E/R
ILyK/z+Phud+/DUh/RloOHuMqGCYREGz1dJkWuiMR+FamIS/cyDI38K1sRz/r/RfG/RvXBvLIMZe
R5oX6+rqHqInORAY3+6KycQYDb1RYFw27slvIf/9oeWJZmm5eYH5/c2OQqVVWW+YMOtPwgXYjzHn
FmyTpQs1+iTVNHX+YPLKQHkhMPbI0skMeayDm6FOonsExnwWGF0HnQ9k1ED6/23oHGGYoxTt9aA1
prvpjQKjn8l0XNq0hZWyMumZOTfft/z5wLFjj89wFG2UpQk2eq0yN2jEfbq9cLk8bbiZ7UW+ybYG
3WE/J925qEGWRmYo5/u+zAzAAoNhGIbpUfRKgQF1as4p+I+sTHpmdhR9RM6/+I39lHS7wmygTteh
Qff85ee+HAVmZ9EiaVqJpeYsGE8+g8e/eBlitcvSyCwtN3+2LzMDsMBgGIZhehS9VGDQ+/9ZsjLp
WYbTtcFkX3qc6ZE5NIajRZZGa2aHq8E0fkbQMu8ZjqLHZGllljYtzzfZVlrO/JuRnzSNzDIciyb7
MjMACwyGYRimR9FbBQY674myMumavdA9aMLfTr567F+ugthQeKLg2h267Do68rvo1Yk8fbChjP8i
nwyH64+y7VIjIWIvvNWXmQFYYDAMwzA9it4qMDLsrl/JyqRrTtehG8Y9/4PUaQtuUREJZnthGbIK
GiyEGDepCgyzvWAZXGhg6OOy7RGsnT7D7cxNHRYYDMMwTI+itwqMIZNmX+37FFRSrkhGX3Zc//Cz
g2+Y8OKf1ESC6w2RXYBr73360nRnkdInsmk5+ZsvHnbfCRlOl/K4DVgTvcIR2SnDAoNhGIbpUfRW
gWF6ePZZ6QYGTvrNNGnOr83OQodsW6iZHUXPiOwCXHP/s2dDYNTL0kusllZtTctZ+Jlkm9TMjsJ9
A7LvOUlkpwwLDIZhGKZH0WsFhsl+XLpzUbWsXHo2dMq8ianTFsyTbQu1jOmLHhLZBfjewBHfRCf5
tSx9mDmLDl97z9MXQwhtkG6XWxnNtyGyU+ZoFxjvPDfshIInL/uW3xYuNJ0oNknRpn9r9kAaRxPx
u/ildtNx2tih6WePHXi8druK2RFTuIeS8tUnN55XX267zVtundJQlZXTXGX7/aYPhv8/u903x0UY
tK+yPPRsSXZ22GyOs2cH7wf+r1svWqhsWl+yjo7I5+lz910cdLxeefrab4hNATo6+qVo05B1RKiD
UELrZKE9+vnQVGYZ7Cmz3t+0drTdU2Yb5y613PjGvEHSCe2o/rTxVUx7TtKETrI0evbccxeHTelN
9Raajs7PqiXZ/ZcsyT6W6lAkTTYpb88cfHptceYNDZXWu1B3UxvXZDnqSjLH1Vdk3br7C8tPltgH
xL30O/ap/56Vw3/WWmW7ra4085Fm5OEts0xqqhz9u52fj7pCdh7pQect1Ze2/uh6F5t1oXNK6/e0
Yt50XLR+Kmak7miflr+S9n33auswtCH3NFTaplE91ZVaJjRVjf7Ntk9GXDnDPsDQTerSGNoY4apP
bxUYIMWck29gdsxOg7iYgb/vhv4eZk5Xh+mRl0aLvLSkpOXmlUh9QoymMx8ybubN5tyCGtl2mQ2d
Mv8/Ih9DmB1Fi2XxolrXC4xPhLshGquyHm2qtLn9hv+XRWpc6QJsqrKt9qfFxVe7fdmoq8TmMGpL
LJORPhAbDejGsRAVYnO/DR/edD9EQGB7NKO0Gz4ZPka4B2hcN+ocT7nl5YYKa2tDha1Da42VtkMQ
HR97KizXIWnQfrWsyXpPlk8kw/66ty0fGbaWTV2J9S/a/UQ592/+9OYfic261K3OzNL6wjy7Voyy
is1hIK8XNGndTWtGrwpt2Juqss9vWZtVE0iD+N6KrL+LzRGx9+t3DI7Rh9r47gqrb0B1KB3o6N2V
o3+FOt6EOg6tc7IDqC9n9bLgmXM3Lx02GGU7Un4Fq6+0Bq7d1nWZl2J/PLJ0kWzPF6PyhHsAxKgM
TYcy098DNcWZX3vLrKvcpdaCxirraOokhFvCIGHWstZqxXn7XmOFtT60DjV12Y7zdwf+PQOC4zKj
wsdTZvkR9vUZikGxQuOTUd71FbY9EOUzvZWWnwhXXYrfSj8fx3dfoP5wjnnWWn8nNuuCel0S8IN5
ym3/FZt08ZRYbjZ67PetHPmocI/I9s+Gnw5Rd7+3wra6scrWpnMsOhrKrbX1lbbXcOyGFxcfacsi
kAKfz2Tl0rHqjo6O6EKtFwuMfugc35CVS8/Q2b9pzi0sk20LMhIHk2ZfLbIKwpybr9yxpuUuHIe/
TaG/RzKzPX+GyMYQ8M0PjaVi5pyCWSKEYYZOnW98LhJ7gW8uEqPsWZ45EhdPoOGBwDi4fdmwH4rN
QexcdsvPcQEe8qcla660TRebw3CXWD7SpnWXWV8Xm3xs+GDY+EgXs8wo7fqlN98l3H24V4++uKkq
a7MsfYg1eyttj2jv5Onil6SLaKibjh0rRg0R7n5SPKWWD8LS4+5HbNcFDebbob41qywR67R2tWVu
UPrKrHWhAgN3qBeiTg4Gpauw4rhZh4skUkhg1JdbP9f61ZVZ3xGbA1AnR3fZECNB54LMUMcl+1dY
AmJr4/+GmVA2adpIhrvJpcK9X2ul9XIj5wzZrs9HvCLcAyDGVllaqVVaVzVXjb5YuMZLSvMaWzo6
9FJpXjrWWGGDgLa+TMdXxIrIgY3DTkHaZ3EeNspiRbRKaxN8/lFWkKF7J/3F66YLUYfNWl9cX0pr
zEAk/1vrV1dq+UJs0qV2deZIo8d+7xcjIwprEskNa2y3Y3+/lvlGsfaGKtty92rLQBFORkp9hbVE
4hvZyq0tR73AGDp53guyckWx3bjbj76eiNPVcvkvJ0ovEAgM5XxNk+a8Sq9KZNtklpaz8BGRjSHS
p7uek8WLZqiLf4oQhknLzVOe1dRvQ6fMe1W4G6Ls1YzvoOMMaoT2rcj8vdgcxP7iUbnadD6rtK6T
vYJY9sagk9GR1GnT4m7wXrHZR7wCo2rJgP74PUwkoCH2WejvsMP1a22PCfeECAx6TIzOIqyBghDb
EO3Od8Nnwy9CupZQX2+Z5U1slt6pxi4wKK1tLz3dEMnCUBUYDWXWYYgVFj/SscQd3073V50ddK8U
GDCk/2p3ybBvC/eYoFdpNaXWx1F3bbI8DNgB7zpbFkJKzxE3xBDKWyXxUzYI32K9c6W3C4yN7ww7
ob4qax5E2GGZn6rh+m3Eft8tawMBCwwZ5pz8h2XlSojZiw4MuuNv0vezZkfhBKlPvOZ00dwZstcy
UUnPLTA+LwgM+1KOWz1DjzMJk8l0HIST8voqfov1CQ2Bi+BL7UnuLrWGPVKmu1a6k9OmI8MF377+
o2GXiWQBvBUWE7ZrnozYDh4oGTVAbPYRKjDQuR3GXcEm6pxl1rQma8P6/938S+He78Aqyy8gJIIa
CMT7CB2sxVuelYl/FwV1hJW2ivryzEuFe7+mtaOX4LctfoNQ8GpjoV4Oabfjrn3LDuQp3H24q0ZS
Yy7rMA5Xr8g0i2RS3CXWKRI/sk00FkAkCyIugeFLb/vv1qVy4aMqMFBPb2rTYP/r6X3+rmUjbkSd
3YffNh3Zbm2DMPtLVVXn/mz53y3X4DgG6hS2vb489NG9tUazfYu3wrbYlzGQCowq29ey88VvO78c
6ZuYTwtiBAkM/L8W5VzXWJm1Efu3W3ZMUfdPCnfD0PiB+sqs2YgT9poCdV6Pa+t1T5nl4cZKi61l
bdaIuuLM32PbM7C1MFkn2IpjkybCB3BXWn6IOpPdkVOMSuzfUwdWZd5J4ztqii1/wHnybEOFb58l
r0+sG5sqM88ToYPoCQKDyozjuzX0eGtt36rMicI9QMdS03HYt5e1ZQhYpa0BebzrrbDmIL/bffW0
ynI3tr0In/WwsGOB9DX7y448pdMgExhuWTn91lRlrYLAiL5qb28WGKbJc0bLypUIM9sL1tOaJSKr
IEgEkBiQ+cVliJmak6f3KCsiZkfRL6Uxo5qr4bpxMy4QYZRJt+dfZeTJjN9Sp82bIEIYpr7M8ljw
RWDdQg2i2Oxj70rcbVcGvx7xm7fUEvY6wFNhfVSbBg3bNnt28ICrUIHhKbe6X3vBfCbdDUSyDs1d
274vR9zepPFHrKbVb6SfIzYTKW40EGhEG3HxLt5eMfx08bsP2kfqnP22d0XmAn8sMvjts98z4CRt
mtB34GhAsrFvAR+tNVZafWv0yKB9QZnQYEn8qmxt+1dZvyeSBhG3wICho3OKpEGoCgx3mXWjNk1t
ceZLYpOP5a/ceIa33PoeylCNYzpKW2f0b219vv7SNRc2raFH/0fi7fx8xDRtGu25GCow8O/2dR/c
dFPoeaK10GNGwC/kCYZ1JqWjQag04LF6tWVg6PHB8dyo8N5dihBewZ1Tpe0w8pi767MREZ8UUH4Q
ohnNa7JWanwPoV6d9EREJPNR9n7Gt5qqbLKbgK9qSq3Dl6JjFUmD8N3NV9p+ifLsDPXFb58tXz46
bBBmTxAY+PfBsn9n/Ex2zP0mP/ZZDm3+PkPbBuE7e/Xbt0Rss5csGdD/wMpR6Uh75IasKutrutER
SUIJExg1xZnzZeXUmvDVp1cLjCnz0Mm5DHdyKmbOzY84oMecU3h1cgRGUVPo1OSqDHlk1qUQC4ek
caOYObcwR4RRJi23YL4slr65OtKmzL9ZhDDMzuXD03HyB+5g0Eh17CoecYnY7AMdzXj/9lDDhV6s
7QTooq4ryQx6/YDGYZHYHCBUYLghMGhEvdgclRoa9KXxJ/OW2e7GpqBGZdunwwaECiYZEBjztbFI
YIwefa7uCPfaEssTWh+twb9u19IRZ4mkQVSXZg6NLEzQEK22Bi0G6CcRAgPWSnUnkgdQfoJRbluu
TUN3/h3rfx20n/uqsk9a/8nIH4j/RuT1F284L1RgfL1s5BSxOQyZwFj7/rAbxWZl4BcmMMSmAHu/
HPlrbV4417ybPs/4jtiszM6VmechTn1QfpW2Nk+F5Q5ZByiDOjecL050grXuCutv8FOYH67bSUF5
wJDvp6HHJhI7P/GVM+yRfn2F5WGRJEBPERilb970U7FZidoS3/kTVG78v7F6VaZvAU0V6GlcfaXV
0bjWVrLnC93xMOECY1XmfLEtPnqzwPDNSTHd1SorW/zminhXd8PDT50HgdEs94vdMpxFu659+hVD
n2H5GThi7DdRJsNL2PvMuch7o4HZQ1HnNBNqmzSWnjldjbFMIuZnqct0Fi6yWu2FUF2SeY/Y3PnF
QKnlE+12rcH38PqlNwYEyevPmE7Db0ENqrvCNlZsDhCvwKh4e/jp8Nvl9+80a6u33DaNHoOKZMrE
JDBWZ76v9Qk1NJ5/EEm1pHjLrAtl6f12oNgiXTcnQQKDbMeBcmvQOaMqMDyl1oe0aciaKrMqWzaO
DnoFpkKPFhgrMm/T5kUCY/enxsdh1NO4i6C8bO21xZkR91GPbatukj7Zoi8hUI97tPnQPu5bcYuh
G6s9X478Afz2auM0VNl2flk47BSRxEcvFRgpaBuKtHnjuB9uXqNW7lAUvi7qmQJj6NQFYwZNmHdO
Msw8Kf9MUcTIoEPBXb/huTCULLcI7ZgcGpththfG1pnrmNlRtArho7/XigDKFMNno35zHTA7Fv+S
6lSEC2NAtr1/hqPoXqRXnWgs1FboxVfBU2YNEhB1JZmBgXE7lg//vrYxqa/wjUzf7v8/GS6cwCDa
+vJMc0hD0FpdPurHYnMAyRiMJnqX37jG9kAkW/fxzYExFIS3KusP8A1+f9z5Tv/VnR9aop/rGowK
DNomPiEM+KBuGrT/RwMWNj8JBMBpuNsMEnSIEzTQ9sCqI/WvJVaBAYH4H/wNqieU9d0OzZMdVYFx
4MthpzRW2cq16chwLKvdZbbAGBkV4hUYsHbs70zZueK33asybxHuARBDJjDoGjpuyQzTSaibdNRt
0Ksg+GwoDnktEQ16LYE6DY5TZVu7NcGfvu7+fPhobb3QIGdcwzF1nO4y3+ucQCxY+/4VI0eIzT56
xiBP6+E6iDfZMffbbs2gbLohgf9+bd6IsTTSq6MEECYw0L59QZ/EysrqsyrrfR0dHdHb8ngEBqwd
d6VJMXS274si6oIyrA4pU0LM7FgUNo9BgIEDj0dnXiXzi8fMOXlBn0caJd2Rf7ssrhEz5xauo1Vg
03IW/inD6cpOn77o1rSpC+815+TPMjsKt8p8VC0tNy9sIJNRaFBT0IWAjhMXnq8RbKyw/lG7rb7M
+rEbd9hBv6FjoicdvvSVWaF3bBu081/4CRUY0YzShn6mShMv1SM/GvAVlr4qa/PeFb75L5QwKjBo
XAru3ANfgdArD7ozpfe5R36zHfJWBQ9udZeG1Ge5dU/dasuL2t+85ZbKsWP7hdVZTAID9VZbYv0d
6mNekC/MXX7kc1pVgUHsWz3y4qY1ks+DaaBuufWFPWX6nzn6SYDAiGpqX5FYm/DbflgtTDoXgqcs
M+qcCqF8+arpXHQcQWMvaldlGn51Gg1vmW9AaCAPiJiGkndi++qF/LD/QV834Zz6q9jso2cIjOim
/YrkwKpR18A/0E6QCMONldLcHTEiG+Spb13xFUkyzZyb/4Eooi60YqnMPy5zujpME18aKrKQgg74
Q6lvHJaWk/8PET4mBt/9+OmIszc0bo8wp6tx8KSZF4mixsyu5SOvxwl+5OKrzGqn95X0jhgXyXuB
CwBG4zG++u/In+JiDYy0pwZp3zLLD2meCXd58FcptSWWBSKbIBIhMAgqIzrRCfReW+LTWF9lezgZ
YzAa11joSxVtXgeXLUo/x1PuG/kf+N1TZnlCuPgGlqJR+zRoe4V1DgSeNSSWd6Okg4hHYCDeKYgb
9OQBjWwLffFDfkYEBtGx7pfIJ/ydPVnzmqwV1SvDn1qF0nMEhr5hP7+sLc4+Vbgrs+Kf6VdBiAXi
UNl3fjZipNicMGpWZ76hLS9EzZrQ80IVulGAYA8a4AoR4BKbffRGgbF/xahs7Zgt3Bx01KwYaWgM
h0FYYEQibdrCp2X+cRkExqDxz0kncfKDO/oFUt84zDRl/oMifMyY7YVPyGJ3uzld9Emp0kAxPZa+
bjoNJ/g+7cleV2F7eOkS03fxb4//N1zgbfu/GP7/aAS7p8xW5v+d7ECp9cE3515zNu4Gg8Zf7Fs1
UvrUSiYw6P+RjAafygSGH6QZic60WhuPzPd0o9K2qGPXCJrCOyJGBQaEwV+06SEcvqJR4Oiwgwbb
oVxbO8RnmjUrskKFGX0RcO2Opb7PXQNPPnyNX0nWIF9GGuIRGLRNDHJzB8ewbakvs37HqMAgNn+Y
firqYRHShj9BomNRJR+s6idRAoN+i2S7vohdYDRW2Q42Vlnn7Vg++gzhaoiaksxBVAZ/PByXjs9e
ScsQmxMG4gbdBHjLLCvxc0ztAgl2HJOgr1FqVlteE5t99BSBQf/Xs70rjgiMvStG3Bby1Vn7+vcj
D0TOzu53LNItRrtTrmZZq7OzL9KKUKnACC1jkFX0EYGROnXefTL/eMzsdDWcPzj4U8FQIDCUFkwz
YoiZKcLHzLDH5n8bnXnPOp7OovrUqXm6gs0AKbjQ39VeCO5S67/ry223aX/zlltX+J8G0OeO2m3o
YD/e+fnIEXSh+H/Dv1v3rrRJn7CECgzc+TeWv3fjH6reu/G2SPbeKybdGRXrykb/AJ3CUn/MYLO+
FWkOCMKowKgrD6mv1Rbf5Grr3r3pQnTagbEYtI+eCptvHIC73PqU1gfiZzW916cJu1C3QZ8JouO+
n3y0xCswCG+l9S78HvrJ5Bvv3HfxCUYFBkEdEjqXuxEjaB4Rn1XaGj3l4V+s+EnEIM+v/nfz47Jz
xW+fvZY+WLgHgF+wwKC5KHyznR75DZ3Dxp3Lbor6FEaPj/IGXxb6BKOmxPJrsTlhVK/KXBxcdtsm
o+NF/NC4HHpFqo1Hg5LFZh89Y5Cn9dCG/94yQXbM/bb81bQrhXu/+hJrRtATDIg9WgNGbA6DBAba
P+3nwdGsxWI+RzvuK0xg7F85aqmsnH6rfH/YryAwogvD3i4whkyYOUrmH4+Zcwu+Ntntuuosw1F0
p8w3VqOl56+776krRPi4SLMX/hYiIymf78ZiZocr5rkvZHirrBO1FwMalV2esuBpsHGB5IrkuOBH
XoYLPtBRoVFr8JRaXNr0UPYVkQZRhQoMo1+RRGLr0jEn4q7zUXRuQQMdYTS9b6D8oRgRGNnZA/qj
foIaYXT+U8XmFDGoMrCtvsy6hBYAQx0FdWyeyqzx5EBPPtwllv9qt0HghY04T4TAoA6kscKWr41D
T3looicIjGXa31UEhp+6sqwrkFel1p8Mx3h34+pR2vlJAiRCYCTiKxLs90t1JZaQLwxsB6tX60+W
Fg3fl04hHTHyCvtiJV7cJZlBY6hwnrdVfDg8plenFe/deAlEepAA3b8y+KuXniEwjH1FsnXpTWFl
9pRbHxebwzAqMJoqs3bfNyxoYb0wgdEjviJJpikLjInzrkD6mOZ/iGRp0/K+RGhddTZ08jwzLWYm
84/JnEWtgyc/F9cUv35oQCHizZLm09U2fdFbtPKtKFpCqP7SMpDGXvgvBlxctNCSZlyGrX0vRIVI
7pv6GI3lGv/2Tp/gme48pZEb02QJDD/ecutoGrynLQ/yqwn95M6PEYGx5b+3XIBYRzpG7EeN5k4d
4iBbu2/o0D0QZ3fg39r6bNROqIU726CBeohRHDrxTiIEBtG5wJMt9NhRXQW9YjIiMAh3xW2no3N6
RxuDjCaGEkmC6CkCA+fJzAOlo7+PO9yakN+r9J56RYOe7iCvoDlhKI9IE6nFCo2hQj7BT2DKrUED
M1Xxltue18ahJwU7P88Mel3XGwUG3eigToJe/eC63LPtE/mnv3TtNVRl/Qv5rA81+H6lbRvJ6lZb
QhfUZIERiavH2L9LAwhlMWI1c27eEhE+IqmPLv4h8m2X+cdkTtfXpjELE/ZJmMluPzHdufh1aV5d
ZBmOos/Sn1hieMBZNIrfGvFNXBDhs/kJa0Rjq/2skfBURJ5oChdix87lI28VScNIlMCgwYuR1k1w
V1j+rC0TrH3T/24OWxGVMCIwqleGLBIHsaFtqGgtlvrwNUoCY1nI0OnSSqUBwV1blnm7djsaw5rQ
+kiUwCDQyV2JbbqLYUUSGNRx7l5t+ckS3OWJnwKso30P+TSzelXmx2JzED1JYNDv9ZW2B4J/9wmC
mNYx8hP61RAZ6udfRudq6dicfWrT2tG/kz0RpNchOOZBd9u4XpvqSm2pIokSNOMnncvaOE1rsnyv
8UQSH71RYBDeCusf6esRbf6NVVn/2VWsPz4rFDfF0ZQF1l5fZQudrIsFRiRobgZ0zobXxNA1e2HU
rzlMDzxzGvKNvmiaopnthVGfmhjl2oee/ka6sygP8RMnhBTN7HC9PfzxtxN2lx9CCjqw17QXhNbq
Voc/TnRXZF0VcqEFrLEyq6XyvRulaxkQ8QoMurP0llv+gDvmLegESmWj/KveGTYADUigs0V+7eX/
yZCOCTAiMBqqgj/rRRk24eegpw01JZagO0Gt0X57ymxBXxMc+MLyk2bNQmBIc+irj0YE3iETiRQY
hLfceg/SBN2JaU0mMHZ9PuISb5nVhX1uRucgm0gM4mVUoTbO7i9GrRabguhpAmPjxmEnhK65g/10
b4jxdQNBK5PSJ9PamCJuIa16KpLpQq+f6Bz3PVWssL1IM3uKTQHcZbabsF9HjjvlUWk70LjGZhFJ
9EjB9WpD+pABwNZD1LGLNAF6q8DYsXz0NyAyKrT5k0FEvac6KVnzOttN8AkayN5YaS1f/sq1oW0F
Cww9kH5lqH88ZrYXhA1aC2WA3d7fbC/aIvOPxcyOAumERXFjtx+Dzv5uCI1aWb4JN2dRY3pu4dSB
s2fHNHBLFRpYqL0g/IaLuX3thzeFzSnhG2ldrl3cSutjLdH7PDRUYKCzq9/9ReYttatHDZGbbcha
8ZSgodx6JRrooM9BW9bYluJOJPAko6N47PG4wGdq06CzPfzVf+WNkhGBgUY+qJGsLbaGrWbb+cop
ZDCl3yptX4feFS5caDoRd1eBR/RUN55yS9AXOIkWGPRJIurxlaCYGgsVGNjvJ1EuTcdibSWR0dFx
5DjXr8u8FGmCZoMM/QrBT7wCA9YO8fKw/HzptI2fDgubYRQxpAKD2L9yhAnbg8qEOnyloyP2yexo
LEdwvXUajtPmmtWZf6S5J+ipkEjug55U7Flp/ZmnzDIjzLfK+jI9cRRJffge6Vfa/hGUzme+Rfte
2frZyGtDF9GjPLZ9MuJKXMPBCwP6rdI2I/Q1HSETGAdKLJM/zDf/KNTeL7iBvtQI7FuowHCXWEtl
fh8uNv+o4MnLAvOphAoMEj+0HpHsmPtt+2fhwtBTlkXzYYQ9uaPrHTHHb/ho+PdD95n+T1O+15f7
ZmUNXQG5pb5S+qQoTGB4S63/poGlsrL6DNv6xCBPwuwoXCyLEatlOF0q338fY84t+EzmH4ulTlnw
NxE3KZjsiy802wsX+ASAJP+4zVnUmm4v/Ff69MU/EVkmFWrQcKGF39FW2XaEdmR+aksskkYNF1OZ
RfeJVajAIKP/R7KmKlvH2v/e7Ltj9paM/jYavy1aX+HvwV3eazWrLAtxR7YubHuVrdweYT9UBcaS
Jf2OdZdZgl5/HFg1KmzBt87R+NYV2nR+Q4cbWDbeD3Uw7hJL0CBLT7n1BbHZR6IFBkHrpaDeNgTF
FRYqMFCeB0PfPVN8+kyvtthCi8W9iVihjbfs8bGPBAgMn9FvkWy30meqQWOFUrA/IfVsO+yR3Mkb
oWFNFo3BCVueX1gz8liF4/YvCHMXyvd2faV1M+UrSUuP9TfKVvDsqMru37wmK3SwaqcPbhJwvnzd
UG79sJnyofltqB7od1n6qqyXt24dI329LBMYuN7oSUCYeSusW8eYLgjECRUYZDK/lnWjO2ghO+EW
LjCE0W+RbO8XI6Ur4KLjz8Z+hwm+TrO2QkiX4Rx4FddvQVNl1j/pZgnxgkUnGUQOzXwqwoYSJjDI
QsuoNRybVuXPVEf89eWdNGCxJxk6Q2WBkTH95cdlMWKy6YsPp0576XIRWhcaqyGNEYMNnTz/XhE2
qaROnfVDCKPH0h2FG9IdcY4hQbkRZ1tqbsEzGfaCnyF8Ql/x6EGrN6IDCfo6gsxbbglb8trP/hIL
3fEFNYR0sdSutuquqSMTGHpGabXzYDStv/V6/B78SFffmuvKI38VoCowPlmMTlHbgaMD3b9q1HCx
OQgIj7AnQo1rbG1l72fQcQ2jZlVm0BOX2lLL52KTj2QIDMJTYbkO9RsyzXm4wKBxA81rRwe9/ohm
iPuBf7n2UBIlMPRMcSbPoMHIpf9Oo+nxg+aFaaywrV23bNTJIkkspNDriqaw6arVzSfuKm1L6jdZ
Iy66Rk/G6EkT0oU/kVAyeuJhfYqeAIqQYcgERiSDaI4qMGSGc7ij8t3oAkPPtBNthdKwNutG1FPE
MWdRDftfU2b5U+iTJw1SgaFrqhNt2Ts6jrlx6uxBNHNlT7LrJ6p18sT19z9xvixGzDbGrjTYcvDD
L/xI6h+DXfenJw2vfhgP2dnZx6ZOm/PTDMeie1OnLcxLm5a/0uwo2uV7wuF0teLfh/CXvs6B+RY2
a4KY2Gt2FKxOyy1wQQA+bJ6+6IqBY5P7KkQP3F0HfWqKC+lwdfHINLE5DBIluHsJarCh/ptfnXmd
bt3HKzCIprWjr0PDL31FozX4VtdVWmzCTYqqwPh62fBb6G5NE7uZvioRm4OAkPhOY5Ut6EsW1Kd0
wCNBrxu0aXEXuW/cb7WPiZMjMAj6TBXpg+5mZWMwOjqy+6MTehadUNjMqSHWjgb2rdBl8rX0VIFB
eCtsY4PT4HiUy5e6NwItMoe7+oUoQ/B5oW/tSF9eV2q1yl5ZyGiusqZ5y230BE3+mi7EEP8wBMyK
+jJrxGvdz9EgMAjv+hFnecuszyKukRuVwzj3P6krtwaNj5KQPIHBMAEGDOg/cOwTp6ZNyfu+adKM
iwdNmPlj06S5F5vsc84dPGnm6QOwXaTsEbhXW4dB2eehE6XXDAvRic16ctyRTk6Gt9Q21p+ezFOe
9Xi0hnD9RzffrM0nmlHadUuHh001X71u1MlNVaPH1a3OXNlQYXHXl1vQoFoO11dYGrxllip0nE80
bhj9fZE8IjuWj7wT6RcGrNz6gmwNFXdZ5k3u0sw8fzpPue15vbEmnnLLBH9a8tu3IvLsltUVtku8
pdZAbPx7fvFbR5Z8p09dA9tg9B3/EohasdmHt/jXZ6EOFvjToLx5+0pHSr+c0UJPJ+A3XRu/uiRz
nNgcSopnrfXaupLMQqTbXo+GEQZBYWnzllv2uSFMaPrz0K+OQsl/4eoz6yut8/35Uf1s+Xh4xCdf
TVUjzneXWvL96VVs22cjwp5ioiF/SpumtiR4rAtRtWRAf5wDT2vTeSosMyAaldZZiUbtatsFEH+P
oOP8ALG/hshpRP2hk++sx/oySy3qtxzb5yDdTbFMnOX7NLMya+iB4syXPKWZ5YhdB4P4tHbgWB9q
KLe4PaWWigMllln0xUmk14ehFL9lOgvlm6utm0iGvJ8eO/BI2fevGvWwLF24WfPK/2MOLG6Ic/8K
+k2eVm47Px/1K+GuC81kW1NmuRvn1pueEssW1E09jsEhOhZoS9pQT7UQSqUQRy80VdquVxR5KTh+
j8rKFclwLOZBYOheMwzDdAN00deVjjmtqWri+R2b7ed71t57pmqDycTOO+8MO6Ghavx3m7dOvLB9
+4RzRAfcZa/1jgJSNqIOO9Y/fFbHLvv5zVvtFzaum3BO9bo7TqZB1CJN3FCsAxt/c0rjhinf9+Wx
8ZFza4vHnkpPXUUSBiyxD+jvrrrzjKavpp5H9UTndHuCjwXDMAzDMAzDMAzDMAzDMAzDMAzDMAzD
MAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzD
MAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzD
MAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzD
MAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzD
MAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDJIF+/f4/x2+Llwr/hoUAAAAASUVORK5CYIJ=

------=_NextPart_01D227F5.5FD6A920
Content-Location: file:///C:/2A24B1E5/master_file_files/image003.jpg
Content-Transfer-Encoding: base64
Content-Type: image/jpeg

/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0a
HBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIy
MjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCAA7ANQDASIA
AhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQA
AAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3
ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWm
p6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEA
AwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSEx
BhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElK
U1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3
uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD3+qx1
GxBIN5bgg4IMq/41Zr5UsNN07UvGOqQai4jhD3Dr+/WHc4Y4G9uBXRQoKrdt2sY1arhay3PqH+0r
D/n9tv8Av6v+NH9pWH/P7bf9/V/xr53svC3g2WTFxru2IKGEglQF8SyAjGOCUVSPdh60h8LeEkS1
MuqlUkt5WllW4jbZIDhBt647n1FbfVYfzP7jP28uy+8+iv7Rsf8An8t/+/q/40f2jY/8/lv/AN/V
/wAa8Cl8MeG4UCxakrKk0se83KHeqoxQoB1BIGScdeOtA8O+FwYvN1QpGY3IkWZHMg8tSH2gfL85
I2Hk4rzXNp2PfjgKTim5P/wH/gnvv9o2X/P5b/8Af1f8anjkSVA8bq6noynINfN+taFotjplzNZX
iTTw3flLmdWEieqAcnvknHbGa9k+GXHw+0vHo/8A6G1EZtuxnicFGlSVWMr622sdazqilnYKoGSS
cAVVXVNPd9iX1qznoomUn+dZfjf/AJEbXP8Aryl/9BNee/DzwJ4a8QfDuC51LTozcyNKGulYq64Y
gHOewrqhTi4c8n1seVKbUuVI9ekljhjaSV1RF6sxwB+NRteWywLO1xCsLfdkLjafoa8F0XVL+7+F
PjLTri5ku7OxKLbSyHPBfoD6cA47ZqbxAVf4L+DbIj5bi6UED0y/+Na/VbSs31t+FzP2+l0uh7wk
0UkXmpIjRkZ3qwIx9ajivLWdWaG5hkVBlijghfrjpXhUGs3uieEtc8CRMTqQ1EWNoM8mKU8ke2Af
++6ueA7JNGvPiBoyHK29syA+u0OM0PC2Td/+G7gq92lY9l/tXTv+f+1/7/L/AI1I95axuiPcwq7g
FFZwC2emPWvnnwPpEF54d8x/h+2unzmX7Wt2IvT5cZ7evvW/8RtCXWviL4f0eI/ZWl07ZCQf9Wy7
yoz6AgCm8NFVOS/f8PmHtny81j2mW4gg2+dNHHvOF3sBk+2aS4ure0j8y5nihTpukcKP1r5+1/xL
da7pvh2w1ZDHrWlaqLe7Rxy3TD/jjB9/rXWePPCmtT+NU19tHHiLSFiCCw84qYsDBwvfnnjPXkVP
1azSk7Xv/XzH7a6bij1RL60kgaeO6heFesiyAqPx6VJHLHNGJInV0boynIP4148dT8M3/wAJ/FEO
gaY2mSRqGu7SQHKuSBnqePlx+HSsPWNWvbP4SeD9Mtrh7W21BpEuZUOPlD/dz6fMSfpQsK27ba2/
C4Oul9x7tHqVhLP5Ed7bPN/zzWVS35ZqWa5gtlDTzxxAnAMjhc/nXmPjH4c+F9J8C3l3YWgtbuxh
82K7SQ72Yepzzn+vFcf4l1C78QfDLwhNqjO8r3zQtKfvOoyu7647+1EMPGdnF6XsEqrjdNHv73EM
UPnSSxpF13swC8+9DXEKQ+c0qLFjPmFgFx65rwHXtSvdH8Ha54E1pi09k0cmnzN/y2g8wcfgOR+I
7V0fibwtreveEvCV1psK39pZ2cLT6a0vlib5VOeozwCOuRnij6sla8tGL2zd7I9Wh1GyuEd4LuCZ
UGWMcgbA/CodP1e11JmWAvlc/eXGcdf5j864H4e6j4Y/4SC70628NPoGu+Tia3fJDoME4P4g9Bke
tejW9lbWpYwQpGW4O0dvT6VjUhySsbQlzK5PRRRWZQV8eax/yHNQ/wCvqX/0M19h18h6vaTHW9QO
0f8AH1L3/wBs134CrCm5c7sYV8PWrpKlFyt2MvqQOpPQetdBpXgfxNrW02Oi3TI3SWRPLT82xXsf
hPSrSw+Ea6naWtvb6mbGWU3axKZNw3c7uueKxLzxF4nJh26q3kxpEmQ4VnMixkEgc5zuwfY1pWzP
ldoIvDZRKqvela3Q5a9+HHirS4VMmlNMoUZa2YSY/Ac/pXNyxPBIY5onikB5WRSp/I17Z471XVrX
W1trTUJLaEWSuPLfYcvKELH1IHSrPg6X/hKI9QTV1h1KzUp9na4jVmxghhyM4BH614soKUmfU0cf
UpUFOaTS7bnhGB6V9FfDL/kn2l/R/wD0Nq+fdZktrbXdQgiASOK5kRUAOFAYgCvoH4YsG+HmlMOh
VyOP9tquFGpB3lFpGWa4uhWoRVOabvsn5G34ihs7jw5qMOoTm3s3t3WaZeqIRyfwrjbP4U2semLY
L4m1xtLb5vssc6ojA8nOB3rrvFNncah4V1WztY/MuJ7WSONMgbmI4GTXO3nhq7vZdflngmZ2soV0
7E5XbMsbAlQD8p3bea66c2o2TsfNTim9Vc0X8GaH/wAIrdeFLIfZLaVAZREwMvJzuOc5J29T6VTu
vhvpt1ouh6W95diHR5PMiIK5kOc/Nx/LFU5PD+qjVL67htWj1K80iKOK+DDEVyquH3c8E5XBAIqu
3h/VX0XUltI9Qt7qSyEIgISJXk3A7twkOXGD83GQapOS+0S7fynRTeBtIuPGsXipxL9ujUAJuHll
gNoYjGcge/YVFaeBLC01rXdTjurkyaxE8cyErtQN1K8dfrTX8LRS+I0jktpG0dNPKBDO20zGTJJG
clsEnJrButJ8R3OgWdjc2MslzHpzKlwrK8ony2AWLgJgBDuGSfXjlJyenMNpL7Itt8MbLRrSa3s/
F+u2kMKmaSK3uFG0H+Laq98Htzit8+DrG+13RPELX13LPp9sscRYj96MH5nyM5O7PasttAvl1LUr
ttPle8vdGSOO5RxlLgRuGDHdwSSuD0+lD6PqP74alpt3qBexgjszFcAfZ5BHhwSWG1t/O8Z/Sm5y
bu5EqKXQueIvhto/iPX7fWpZLi2u4ipcwFQJdpBG7IPPGPpU2s+CZtV1abULfxPrWntMFV4bacCP
gY4GOKoJ4Y1K6N//AGuJLqYaVbxQyichWuVV97KARhs7fmwKpavpviW9sY0+wyfb4bK38i5iZS7T
AAybnLDbhs9BznrQnLRc2w2lq+UvaV4H8OLoGueHbGe6aSaQRX9y5zKXwHHJGOjZ4Hc02fRfCK+G
I/B97NNew2XG5I2kkhYkkEsikK3zd+3Wug0OxubTV9fnniKR3V4ssJyDuURIpPtyDVDQV1Dw5Zvp
c+k3N0FuJHS7tijCYO5bc+5gQ3ODn04NJzk29fMfKrbGV/wqm1ngis7/AMRa3eabEQUs5bgbOOgO
BnFSa3ovhjxHBpOjrczWkdjfNb20VvHgebGuWQ5B4AHXv60y40XV3W6j+x3La098ZYNUE4EaReZl
f4sgBPlKbefxzUsOg6smqxTLb7Aus3lyJCykKjxMqPjP94jjrVc8r3ctibLZRNLxj4E0rxpFbi+a
WGaAnZPBgNtPVTkHI70zUfBH2u102G01/VtObT7cW8bWswUOoAGWGOTxWLpug65HbEtPd2eqR2kq
SXDKnlSyFcBnfeSw3fMDtBHt0qvbRvJrOl2ujwyWd5/Z90ZnN2JlMhQBJG2s2fmPDHBOaS5krKWw
3Z6tbnSeG/AeneHtTm1U3V5qGqTrte7vJNz47gfkPyrqq4vwro97aahDPcre28kduY50dE8uZzjl
mDsXYEEhsDqfpXaVlUbctXc0gklorBRRRWZYV84/ELRH0TxjersIgumNxC2OCGPI/A5r3fUPFGh6
WxW91S2iYDJXfkj6gZrz7xv4n8IeK9N+wwzXFxexZkt5oID8hA5yWx8pHWs6qTWp6mVTqU6vMotx
ejPO9N8Xa5pVmbK3vmayKlDazKJIyp6jB7HJ6V1dj8UomW2j1fw7ZzpblDE1v8hTb93CnI47c1w4
trZTjMkh/wBkH+QH9abdWPlXLqhCxDBUzOAcEZ5rnUpI+hqYbD1X70dfuPQtY+NGmC8afTvDpmux
H5Xn3bBcLnOMDJIz7iuJuvib4kkg+zWE1vpVqM7YrCEJjv8AeOT39awX0+Iys0l0ME5xGhP6nFTJ
YQAZW2mkH952wP0A/nXuwrYKnFdX958bPLMwqSkkuWN+rtoZ0UV1qeoJDGHnu7mUKuTlndj/AFJr
628PaSmheHrDS0ORawrGSO5A5P4nNfO2g2us2V/Hf6LpoFzCCUkS3Mu3Iwf73avevDmvrc6HZtq9
9YpqbJ++jSZRg/TPBxjIrHFYr21kk0l3Jjl0sMuZzjJ+TvY0NdmuLbQb+e0dUnit3dGYZAIBNeXz
+M/Ff2W0MT7kS3illmSIfPuiLYJPTBA6d69U1I202j3YmZnt3hZXMRySpGDj3rjr3RvD7aUbJ9Pu
9qWkcMk8DozGMNhQzDjJx1xmuKSb2OvDVqVNfvI3On028um8J2t7L++ujZrK275dzbc8+leYW/jD
xKYIZm1QpvtfNXzIk2vJJnAHHO3GAP1r0mfV9Psw+nyQzRxJbrykeVAbgAY71zw8P+D3hs7ERXLN
bGNlkG7eyoTjecfdyT2FJp9B4etRg5c8b38jdtdVvLrwJ/asimG8Ni8vI5VgpwcfhnFcfZ+LNbWL
S47u9ZftdiLk3DRxgdh0x6nrXYz6hpS2dxpLx3EUOySFgEP3ehKn8T+VZUnhfw/penxMoupFiCwo
4lG/BHAGep9hTaYqVWir8y3Zo+F9SvL/AMC2uo3cxku3gd2k2gcgtjgcdq4XTvH2pW+jPd6jqMs8
/lROsMaRqSpXJYHYRz74xj3rtNJutN07Rl0e1iujbwRFQXZWYg89QeeD17HjrWQdH8Mf2eNDWyul
DQmJpkKiR0T5sM3qduPXgZxSaZVOrQUpc0dG/wADrNAv21DTBM8ySuHKsQwJUj+FsAYI9MVwU/iH
xSmt6xp5vYkjSPzVl8sFrePj5lAHOBk4PWuzsrzSdNku4bK1kjbess5VPvMyE5yepwmOPasyHSNA
gmvr+K2uh9u3JNDjywyopcjbxwf1z6U2mzOlVpQlJtXT20NLwhqd3rGifbbpw6PK6wOE2+ZGpwH/
AOBYzXL614k1seJZ9JgvBbx2kiPJIsYLMDyo7jBHXvXS6dNpehwXVrYwSxxRyKwiMgKjf2XJwoGD
xxj8aoSWvh24udR1poJZ5rpo4ZA+AVKjAKKe/P4mizsEKtKNSUmtOhf8H6zda7YXF1PJFJGsxija
NCoO3rweetYuu+JNVsb+9dbiG1tIHXYGKyFtp5JwCcMONvUHn2rV8Pf2boWjvbWEN0YF8y4YyMrM
SSCeRx/EOO3I7UeJ7HStRtWgvY5hEpWV/JKp5hD4AJPJ5A6daLOwRqUlWbt7pJ4SvdRv7W5l1OSN
2eTeiiRSUU/w7QPlA6ckk98Vyl5rmoab4r1a0s7q3s7WFiY4xaLtbESudxUbuAWOSa7ZtZ06yuWh
8ryyXVNyIMEnHXHQDPU++Kxriw0CbWb8S2sss95IFmcOpGCoUqCDkAgjIHOOelDTtZBTq01OUpR3
2JPBerXepicS6ib23hARHaDa5bqdzD5eARx17111cpo9vpOl6xNJZrdxtfbHMTSAxKXGflXseAD+
nFdXTV7GVaUZTvDYKKKKZkcp4g8D2viG5Ek00dugznyLWMSNnrlyCce1c5q/wmhSyUaFdOs/IlFy
+A6kdAVXgg4PSvTqKrmfUqE5w+FtdTwxfhR4mdtryxBfVro4/wDQTWdrvw317QoftC2UV9CBl3t3
Zyn1XAP4ivoSio5Y9F/XzO2OZYnmvKV122/Kx8taJ4e8T+JLpotJsSkStta4Mflxr9XPP4DJr2bw
f8LtP8Pul9qcp1PUxyJJRmOI/wCwp7+5/Su+AAGAABS10SxDceWKSXkefJOc3Obb9XexFPEZbWSG
OQxM6FVdQMpkdR9K850n4SwaZq8d3Jd295AD+8hmtjhx/wB9HnPNel0VgmaRqTinGLsnuRwQQ20S
xQRJFGvREUKB+Ap2xAu0Ku30xxTqKCBCqkYIGPTFMSCGOR5EiRXkOXZVALfX1qSigBCARggEUFFP
VR1z0paKAECKOigfhRsXOdoz9KWigAppRWKkqCVOQSOlOooAbsU5yo568daNiHqq9c9KdRQAgAAw
ABQVDDDAEe4paKAE2L/dH5UgRAchVB69KdRQA0xoWDFFJByDjp/nJp1FFABRRRQB/9k=

------=_NextPart_01D227F5.5FD6A920
Content-Location: file:///C:/2A24B1E5/master_file_files/header.htm
Content-Transfer-Encoding: quoted-printable
Content-Type: text/html; charset="us-ascii"

<html xmlns:v=3D"urn:schemas-microsoft-com:vml"
xmlns:o=3D"urn:schemas-microsoft-com:office:office"
xmlns:w=3D"urn:schemas-microsoft-com:office:word"
xmlns:m=3D"http://schemas.microsoft.com/office/2004/12/omml"
xmlns=3D"http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=3DContent-Type content=3D"text/html; charset=3Dus-ascii">
<meta name=3DProgId content=3DWord.Document>
<meta name=3DGenerator content=3D"Microsoft Word 14">
<meta name=3DOriginator content=3D"Microsoft Word 14">
<link id=3DMain-File rel=3DMain-File href=3D"../master_file.htm">
<![if IE]>
<base href=3D"file:///C:\2A24B1E5\master_file_files\header.htm"
id=3D"webarch_temp_base_tag">
<![endif]>
</head>

<body lang=3DEN-US link=3Dblue vlink=3Dpurple>

<div style=3D'mso-element:footnote-separator' id=3Dfs>

<p class=3DMsoNormal><span lang=3DEN-GB><span style=3D'mso-special-characte=
r:footnote-separator'><![if !supportFootnotes]>

<hr align=3Dleft size=3D1 width=3D"33%">

<![endif]></span></span></p>

</div>

<div style=3D'mso-element:footnote-continuation-separator' id=3Dfcs>

<p class=3DMsoNormal><span lang=3DEN-GB><span style=3D'mso-special-characte=
r:footnote-continuation-separator'><![if !supportFootnotes]>

<hr align=3Dleft size=3D1>

<![endif]></span></span></p>

</div>

<div style=3D'mso-element:endnote-separator' id=3Des>

<p class=3DMsoNormal><span lang=3DEN-GB><span style=3D'mso-special-characte=
r:footnote-separator'><![if !supportFootnotes]>

<hr align=3Dleft size=3D1 width=3D"33%">

<![endif]></span></span></p>

</div>

<div style=3D'mso-element:endnote-continuation-separator' id=3Decs>

<p class=3DMsoNormal><span lang=3DEN-GB><span style=3D'mso-special-characte=
r:footnote-continuation-separator'><![if !supportFootnotes]>

<hr align=3Dleft size=3D1>

<![endif]></span></span></p>

</div>

<div style=3D'mso-element:footer' id=3Df1>

<p class=3DMsoFooter align=3Dcenter style=3D'text-align:center;tab-stops:19=
8.75pt center 3.0in right 6.0in'><span
class=3DMsoPageNumber><span lang=3DEN-GB>Page </span></span><!--[if support=
Fields]><span
class=3DMsoPageNumber><span lang=3DEN-GB><span style=3D'mso-element:field-b=
egin'></span><span
style=3D'mso-spacerun:yes'>&nbsp;</span>PAGE <span style=3D'mso-element:fie=
ld-separator'></span></span></span><![endif]--><span
class=3DMsoPageNumber><span lang=3DEN-GB><span style=3D'mso-no-proof:yes'>3=
</span></span></span><!--[if supportFields]><span
class=3DMsoPageNumber><span lang=3DEN-GB><span style=3D'mso-element:field-e=
nd'></span></span></span><![endif]--><span
class=3DMsoPageNumber><span lang=3DEN-GB> of <span style=3D'mso-field-code:=
" NUMPAGES "'><span
style=3D'mso-no-proof:yes'>7</span></span></span></span></p>

</div>

</body>

</html>

------=_NextPart_01D227F5.5FD6A920
Content-Location: file:///C:/2A24B1E5/master_file_files/filelist.xml
Content-Transfer-Encoding: quoted-printable
Content-Type: text/xml; charset="utf-8"

<xml xmlns:o=3D"urn:schemas-microsoft-com:office:office">
 <o:MainFile HRef=3D"../master_file.htm"/>
 <o:File HRef=3D"themedata.thmx"/>
 <o:File HRef=3D"colorschememapping.xml"/>
 <o:File HRef=3D"image001.gif"/>
 <o:File HRef=3D"image002.jpg"/>
 <o:File HRef=3D"image003.jpg"/>
 <o:File HRef=3D"header.htm"/>
 <o:File HRef=3D"filelist.xml"/>
</xml>
------=_NextPart_01D227F5.5FD6A920--
