MIME-Version: 1.0
Content-Type: multipart/related; boundary="----=_NextPart_01D29220.9F7B4370"

This document is a Single File Web Page, also known as a Web Archive file.  If you are seeing this message, your browser or editor doesn't support Web Archive files.  Please download a browser that supports Web Archive, such as Windows® Internet Explorer®.

------=_NextPart_01D29220.9F7B4370
Content-Location: file:///C:/D13228D2/page2.htm
Content-Transfer-Encoding: quoted-printable
Content-Type: text/html; charset="us-ascii"

<html xmlns:v=3D"urn:schemas-microsoft-com:vml"
xmlns:o=3D"urn:schemas-microsoft-com:office:office"
xmlns:w=3D"urn:schemas-microsoft-com:office:word"
xmlns:m=3D"http://schemas.microsoft.com/office/2004/12/omml"
xmlns=3D"http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=3DContent-Type content=3D"text/html; charset=3Dus-ascii">
<meta name=3DProgId content=3DWord.Document>
<meta name=3DGenerator content=3D"Microsoft Word 14">
<meta name=3DOriginator content=3D"Microsoft Word 14">
<link rel=3DFile-List href=3D"page2_files/filelist.xml">
<link rel=3DEdit-Time-Data href=3D"page2_files/editdata.mso">
<!--[if !mso]>
<style>
v\:* {behavior:url(#default#VML);}
o\:* {behavior:url(#default#VML);}
w\:* {behavior:url(#default#VML);}
.shape {behavior:url(#default#VML);}
</style>
<![endif]--><!--[if gte mso 9]><xml>
 <o:DocumentProperties>
  <o:Author>Phong</o:Author>
  <o:LastAuthor>Phong</o:LastAuthor>
  <o:Revision>3</o:Revision>
  <o:TotalTime>3</o:TotalTime>
  <o:Created>2017-02-28T16:26:00Z</o:Created>
  <o:LastSaved>2017-02-28T17:13:00Z</o:LastSaved>
  <o:Pages>3</o:Pages>
  <o:Words>164</o:Words>
  <o:Characters>939</o:Characters>
  <o:Lines>7</o:Lines>
  <o:Paragraphs>2</o:Paragraphs>
  <o:CharactersWithSpaces>1101</o:CharactersWithSpaces>
  <o:Version>14.00</o:Version>
 </o:DocumentProperties>
</xml><![endif]-->
<link rel=3DthemeData href=3D"page2_files/themedata.thmx">
<link rel=3DcolorSchemeMapping href=3D"page2_files/colorschememapping.xml">
<!--[if gte mso 9]><xml>
 <w:WordDocument>
  <w:TrackMoves>false</w:TrackMoves>
  <w:TrackFormatting/>
  <w:PunctuationKerning/>
  <w:ValidateAgainstSchemas/>
  <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>
  <w:IgnoreMixedContent>false</w:IgnoreMixedContent>
  <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>
  <w:DoNotPromoteQF/>
  <w:LidThemeOther>EN-US</w:LidThemeOther>
  <w:LidThemeAsian>X-NONE</w:LidThemeAsian>
  <w:LidThemeComplexScript>X-NONE</w:LidThemeComplexScript>
  <w:Compatibility>
   <w:BreakWrappedTables/>
   <w:SnapToGridInCell/>
   <w:WrapTextWithPunct/>
   <w:UseAsianBreakRules/>
   <w:DontGrowAutofit/>
   <w:SplitPgBreakAndParaMark/>
  </w:Compatibility>
  <w:DoNotOptimizeForBrowser/>
  <m:mathPr>
   <m:mathFont m:val=3D"Cambria Math"/>
   <m:brkBin m:val=3D"before"/>
   <m:brkBinSub m:val=3D"&#45;-"/>
   <m:smallFrac m:val=3D"off"/>
   <m:dispDef/>
   <m:lMargin m:val=3D"0"/>
   <m:rMargin m:val=3D"0"/>
   <m:defJc m:val=3D"centerGroup"/>
   <m:wrapIndent m:val=3D"1440"/>
   <m:intLim m:val=3D"subSup"/>
   <m:naryLim m:val=3D"undOvr"/>
  </m:mathPr></w:WordDocument>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <w:LatentStyles DefLockedState=3D"false" DefUnhideWhenUsed=3D"true"
  DefSemiHidden=3D"true" DefQFormat=3D"false" DefPriority=3D"99"
  LatentStyleCount=3D"267">
  <w:LsdException Locked=3D"false" Priority=3D"0" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" QFormat=3D"true" Name=3D"Normal"/>
  <w:LsdException Locked=3D"false" Priority=3D"9" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" QFormat=3D"true" Name=3D"heading 1"/>
  <w:LsdException Locked=3D"false" Priority=3D"9" QFormat=3D"true" Name=3D"=
heading 2"/>
  <w:LsdException Locked=3D"false" Priority=3D"9" QFormat=3D"true" Name=3D"=
heading 3"/>
  <w:LsdException Locked=3D"false" Priority=3D"9" QFormat=3D"true" Name=3D"=
heading 4"/>
  <w:LsdException Locked=3D"false" Priority=3D"9" QFormat=3D"true" Name=3D"=
heading 5"/>
  <w:LsdException Locked=3D"false" Priority=3D"9" QFormat=3D"true" Name=3D"=
heading 6"/>
  <w:LsdException Locked=3D"false" Priority=3D"9" QFormat=3D"true" Name=3D"=
heading 7"/>
  <w:LsdException Locked=3D"false" Priority=3D"9" QFormat=3D"true" Name=3D"=
heading 8"/>
  <w:LsdException Locked=3D"false" Priority=3D"9" QFormat=3D"true" Name=3D"=
heading 9"/>
  <w:LsdException Locked=3D"false" Priority=3D"39" Name=3D"toc 1"/>
  <w:LsdException Locked=3D"false" Priority=3D"39" Name=3D"toc 2"/>
  <w:LsdException Locked=3D"false" Priority=3D"39" Name=3D"toc 3"/>
  <w:LsdException Locked=3D"false" Priority=3D"39" Name=3D"toc 4"/>
  <w:LsdException Locked=3D"false" Priority=3D"39" Name=3D"toc 5"/>
  <w:LsdException Locked=3D"false" Priority=3D"39" Name=3D"toc 6"/>
  <w:LsdException Locked=3D"false" Priority=3D"39" Name=3D"toc 7"/>
  <w:LsdException Locked=3D"false" Priority=3D"39" Name=3D"toc 8"/>
  <w:LsdException Locked=3D"false" Priority=3D"39" Name=3D"toc 9"/>
  <w:LsdException Locked=3D"false" Priority=3D"35" QFormat=3D"true" Name=3D=
"caption"/>
  <w:LsdException Locked=3D"false" Priority=3D"10" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" QFormat=3D"true" Name=3D"Title"/>
  <w:LsdException Locked=3D"false" Priority=3D"1" Name=3D"Default Paragraph=
 Font"/>
  <w:LsdException Locked=3D"false" Priority=3D"11" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" QFormat=3D"true" Name=3D"Subtitle"/>
  <w:LsdException Locked=3D"false" Priority=3D"22" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" QFormat=3D"true" Name=3D"Strong"/>
  <w:LsdException Locked=3D"false" Priority=3D"20" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" QFormat=3D"true" Name=3D"Emphasis"/>
  <w:LsdException Locked=3D"false" Priority=3D"59" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Table Grid"/>
  <w:LsdException Locked=3D"false" UnhideWhenUsed=3D"false" Name=3D"Placeho=
lder Text"/>
  <w:LsdException Locked=3D"false" Priority=3D"1" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" QFormat=3D"true" Name=3D"No Spacing"/>
  <w:LsdException Locked=3D"false" Priority=3D"60" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Light Shading"/>
  <w:LsdException Locked=3D"false" Priority=3D"61" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Light List"/>
  <w:LsdException Locked=3D"false" Priority=3D"62" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Light Grid"/>
  <w:LsdException Locked=3D"false" Priority=3D"63" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Medium Shading 1"/>
  <w:LsdException Locked=3D"false" Priority=3D"64" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Medium Shading 2"/>
  <w:LsdException Locked=3D"false" Priority=3D"65" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Medium List 1"/>
  <w:LsdException Locked=3D"false" Priority=3D"66" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Medium List 2"/>
  <w:LsdException Locked=3D"false" Priority=3D"67" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Medium Grid 1"/>
  <w:LsdException Locked=3D"false" Priority=3D"68" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Medium Grid 2"/>
  <w:LsdException Locked=3D"false" Priority=3D"69" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Medium Grid 3"/>
  <w:LsdException Locked=3D"false" Priority=3D"70" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Dark List"/>
  <w:LsdException Locked=3D"false" Priority=3D"71" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Colorful Shading"/>
  <w:LsdException Locked=3D"false" Priority=3D"72" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Colorful List"/>
  <w:LsdException Locked=3D"false" Priority=3D"73" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Colorful Grid"/>
  <w:LsdException Locked=3D"false" Priority=3D"60" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Light Shading Accent 1"/>
  <w:LsdException Locked=3D"false" Priority=3D"61" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Light List Accent 1"/>
  <w:LsdException Locked=3D"false" Priority=3D"62" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Light Grid Accent 1"/>
  <w:LsdException Locked=3D"false" Priority=3D"63" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Medium Shading 1 Accent 1"/>
  <w:LsdException Locked=3D"false" Priority=3D"64" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Medium Shading 2 Accent 1"/>
  <w:LsdException Locked=3D"false" Priority=3D"65" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Medium List 1 Accent 1"/>
  <w:LsdException Locked=3D"false" UnhideWhenUsed=3D"false" Name=3D"Revisio=
n"/>
  <w:LsdException Locked=3D"false" Priority=3D"34" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" QFormat=3D"true" Name=3D"List Paragraph"/>
  <w:LsdException Locked=3D"false" Priority=3D"29" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" QFormat=3D"true" Name=3D"Quote"/>
  <w:LsdException Locked=3D"false" Priority=3D"30" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" QFormat=3D"true" Name=3D"Intense Quote"/>
  <w:LsdException Locked=3D"false" Priority=3D"66" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Medium List 2 Accent 1"/>
  <w:LsdException Locked=3D"false" Priority=3D"67" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Medium Grid 1 Accent 1"/>
  <w:LsdException Locked=3D"false" Priority=3D"68" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Medium Grid 2 Accent 1"/>
  <w:LsdException Locked=3D"false" Priority=3D"69" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Medium Grid 3 Accent 1"/>
  <w:LsdException Locked=3D"false" Priority=3D"70" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Dark List Accent 1"/>
  <w:LsdException Locked=3D"false" Priority=3D"71" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Colorful Shading Accent 1"/>
  <w:LsdException Locked=3D"false" Priority=3D"72" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Colorful List Accent 1"/>
  <w:LsdException Locked=3D"false" Priority=3D"73" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Colorful Grid Accent 1"/>
  <w:LsdException Locked=3D"false" Priority=3D"60" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Light Shading Accent 2"/>
  <w:LsdException Locked=3D"false" Priority=3D"61" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Light List Accent 2"/>
  <w:LsdException Locked=3D"false" Priority=3D"62" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Light Grid Accent 2"/>
  <w:LsdException Locked=3D"false" Priority=3D"63" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Medium Shading 1 Accent 2"/>
  <w:LsdException Locked=3D"false" Priority=3D"64" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Medium Shading 2 Accent 2"/>
  <w:LsdException Locked=3D"false" Priority=3D"65" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Medium List 1 Accent 2"/>
  <w:LsdException Locked=3D"false" Priority=3D"66" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Medium List 2 Accent 2"/>
  <w:LsdException Locked=3D"false" Priority=3D"67" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Medium Grid 1 Accent 2"/>
  <w:LsdException Locked=3D"false" Priority=3D"68" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Medium Grid 2 Accent 2"/>
  <w:LsdException Locked=3D"false" Priority=3D"69" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Medium Grid 3 Accent 2"/>
  <w:LsdException Locked=3D"false" Priority=3D"70" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Dark List Accent 2"/>
  <w:LsdException Locked=3D"false" Priority=3D"71" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Colorful Shading Accent 2"/>
  <w:LsdException Locked=3D"false" Priority=3D"72" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Colorful List Accent 2"/>
  <w:LsdException Locked=3D"false" Priority=3D"73" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Colorful Grid Accent 2"/>
  <w:LsdException Locked=3D"false" Priority=3D"60" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Light Shading Accent 3"/>
  <w:LsdException Locked=3D"false" Priority=3D"61" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Light List Accent 3"/>
  <w:LsdException Locked=3D"false" Priority=3D"62" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Light Grid Accent 3"/>
  <w:LsdException Locked=3D"false" Priority=3D"63" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Medium Shading 1 Accent 3"/>
  <w:LsdException Locked=3D"false" Priority=3D"64" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Medium Shading 2 Accent 3"/>
  <w:LsdException Locked=3D"false" Priority=3D"65" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Medium List 1 Accent 3"/>
  <w:LsdException Locked=3D"false" Priority=3D"66" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Medium List 2 Accent 3"/>
  <w:LsdException Locked=3D"false" Priority=3D"67" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Medium Grid 1 Accent 3"/>
  <w:LsdException Locked=3D"false" Priority=3D"68" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Medium Grid 2 Accent 3"/>
  <w:LsdException Locked=3D"false" Priority=3D"69" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Medium Grid 3 Accent 3"/>
  <w:LsdException Locked=3D"false" Priority=3D"70" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Dark List Accent 3"/>
  <w:LsdException Locked=3D"false" Priority=3D"71" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Colorful Shading Accent 3"/>
  <w:LsdException Locked=3D"false" Priority=3D"72" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Colorful List Accent 3"/>
  <w:LsdException Locked=3D"false" Priority=3D"73" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Colorful Grid Accent 3"/>
  <w:LsdException Locked=3D"false" Priority=3D"60" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Light Shading Accent 4"/>
  <w:LsdException Locked=3D"false" Priority=3D"61" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Light List Accent 4"/>
  <w:LsdException Locked=3D"false" Priority=3D"62" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Light Grid Accent 4"/>
  <w:LsdException Locked=3D"false" Priority=3D"63" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Medium Shading 1 Accent 4"/>
  <w:LsdException Locked=3D"false" Priority=3D"64" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Medium Shading 2 Accent 4"/>
  <w:LsdException Locked=3D"false" Priority=3D"65" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Medium List 1 Accent 4"/>
  <w:LsdException Locked=3D"false" Priority=3D"66" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Medium List 2 Accent 4"/>
  <w:LsdException Locked=3D"false" Priority=3D"67" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Medium Grid 1 Accent 4"/>
  <w:LsdException Locked=3D"false" Priority=3D"68" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Medium Grid 2 Accent 4"/>
  <w:LsdException Locked=3D"false" Priority=3D"69" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Medium Grid 3 Accent 4"/>
  <w:LsdException Locked=3D"false" Priority=3D"70" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Dark List Accent 4"/>
  <w:LsdException Locked=3D"false" Priority=3D"71" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Colorful Shading Accent 4"/>
  <w:LsdException Locked=3D"false" Priority=3D"72" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Colorful List Accent 4"/>
  <w:LsdException Locked=3D"false" Priority=3D"73" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Colorful Grid Accent 4"/>
  <w:LsdException Locked=3D"false" Priority=3D"60" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Light Shading Accent 5"/>
  <w:LsdException Locked=3D"false" Priority=3D"61" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Light List Accent 5"/>
  <w:LsdException Locked=3D"false" Priority=3D"62" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Light Grid Accent 5"/>
  <w:LsdException Locked=3D"false" Priority=3D"63" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Medium Shading 1 Accent 5"/>
  <w:LsdException Locked=3D"false" Priority=3D"64" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Medium Shading 2 Accent 5"/>
  <w:LsdException Locked=3D"false" Priority=3D"65" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Medium List 1 Accent 5"/>
  <w:LsdException Locked=3D"false" Priority=3D"66" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Medium List 2 Accent 5"/>
  <w:LsdException Locked=3D"false" Priority=3D"67" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Medium Grid 1 Accent 5"/>
  <w:LsdException Locked=3D"false" Priority=3D"68" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Medium Grid 2 Accent 5"/>
  <w:LsdException Locked=3D"false" Priority=3D"69" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Medium Grid 3 Accent 5"/>
  <w:LsdException Locked=3D"false" Priority=3D"70" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Dark List Accent 5"/>
  <w:LsdException Locked=3D"false" Priority=3D"71" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Colorful Shading Accent 5"/>
  <w:LsdException Locked=3D"false" Priority=3D"72" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Colorful List Accent 5"/>
  <w:LsdException Locked=3D"false" Priority=3D"73" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Colorful Grid Accent 5"/>
  <w:LsdException Locked=3D"false" Priority=3D"60" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Light Shading Accent 6"/>
  <w:LsdException Locked=3D"false" Priority=3D"61" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Light List Accent 6"/>
  <w:LsdException Locked=3D"false" Priority=3D"62" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Light Grid Accent 6"/>
  <w:LsdException Locked=3D"false" Priority=3D"63" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Medium Shading 1 Accent 6"/>
  <w:LsdException Locked=3D"false" Priority=3D"64" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Medium Shading 2 Accent 6"/>
  <w:LsdException Locked=3D"false" Priority=3D"65" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Medium List 1 Accent 6"/>
  <w:LsdException Locked=3D"false" Priority=3D"66" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Medium List 2 Accent 6"/>
  <w:LsdException Locked=3D"false" Priority=3D"67" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Medium Grid 1 Accent 6"/>
  <w:LsdException Locked=3D"false" Priority=3D"68" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Medium Grid 2 Accent 6"/>
  <w:LsdException Locked=3D"false" Priority=3D"69" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Medium Grid 3 Accent 6"/>
  <w:LsdException Locked=3D"false" Priority=3D"70" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Dark List Accent 6"/>
  <w:LsdException Locked=3D"false" Priority=3D"71" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Colorful Shading Accent 6"/>
  <w:LsdException Locked=3D"false" Priority=3D"72" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Colorful List Accent 6"/>
  <w:LsdException Locked=3D"false" Priority=3D"73" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" Name=3D"Colorful Grid Accent 6"/>
  <w:LsdException Locked=3D"false" Priority=3D"19" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" QFormat=3D"true" Name=3D"Subtle Emphasis"/>
  <w:LsdException Locked=3D"false" Priority=3D"21" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" QFormat=3D"true" Name=3D"Intense Emphasis"/>
  <w:LsdException Locked=3D"false" Priority=3D"31" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" QFormat=3D"true" Name=3D"Subtle Reference"/>
  <w:LsdException Locked=3D"false" Priority=3D"32" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" QFormat=3D"true" Name=3D"Intense Reference"/>
  <w:LsdException Locked=3D"false" Priority=3D"33" SemiHidden=3D"false"
   UnhideWhenUsed=3D"false" QFormat=3D"true" Name=3D"Book Title"/>
  <w:LsdException Locked=3D"false" Priority=3D"37" Name=3D"Bibliography"/>
  <w:LsdException Locked=3D"false" Priority=3D"39" QFormat=3D"true" Name=3D=
"TOC Heading"/>
 </w:LatentStyles>
</xml><![endif]-->
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:Tahoma;
	panose-1:2 11 6 4 3 5 4 4 2 4;
	mso-font-charset:0;
	mso-generic-font-family:swiss;
	mso-font-pitch:variable;
	mso-font-signature:-520081665 -1073717157 41 0 66047 0;}
@font-face
	{font-family:Georgia;
	panose-1:2 4 5 2 5 4 5 2 3 3;
	mso-font-charset:0;
	mso-generic-font-family:roman;
	mso-font-pitch:variable;
	mso-font-signature:647 0 0 0 159 0;}
@font-face
	{font-family:Harabara;
	mso-font-alt:"Times New Roman";
	mso-font-charset:0;
	mso-generic-font-family:auto;
	mso-font-pitch:auto;
	mso-font-signature:0 0 0 0 0 0;}
@font-face
	{font-family:"Alte Haas Grotesk";
	mso-font-alt:"Times New Roman";
	mso-font-charset:0;
	mso-generic-font-family:auto;
	mso-font-pitch:auto;
	mso-font-signature:0 0 0 0 0 0;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-parent:"";
	margin-top:0in;
	margin-right:0in;
	margin-bottom:10.0pt;
	margin-left:0in;
	line-height:115%;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	font-family:"Alte Haas Grotesk";
	mso-fareast-font-family:"Alte Haas Grotesk";
	mso-bidi-font-family:"Alte Haas Grotesk";
	color:black;}
h1
	{mso-style-priority:9;
	mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-link:"Heading 1 Char";
	mso-style-next:Normal;
	margin-top:24.0pt;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:0in;
	margin-bottom:.0001pt;
	line-height:115%;
	mso-pagination:widow-orphan lines-together;
	page-break-after:avoid;
	mso-outline-level:1;
	font-size:20.0pt;
	font-family:Harabara;
	mso-fareast-font-family:Harabara;
	mso-bidi-font-family:Harabara;
	color:black;
	mso-font-kerning:0pt;
	mso-bidi-font-weight:normal;}
h2
	{mso-style-noshow:yes;
	mso-style-priority:9;
	mso-style-qformat:yes;
	mso-style-link:"Heading 2 Char";
	mso-style-next:Normal;
	margin-top:.25in;
	margin-right:0in;
	margin-bottom:4.0pt;
	margin-left:0in;
	mso-add-space:auto;
	line-height:115%;
	mso-pagination:widow-orphan lines-together;
	page-break-after:avoid;
	mso-outline-level:2;
	font-size:18.0pt;
	font-family:"Alte Haas Grotesk";
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:minor-fareast;
	mso-bidi-font-family:"Times New Roman";
	color:black;
	mso-bidi-font-weight:normal;}
h2.CxSpFirst
	{mso-style-noshow:yes;
	mso-style-priority:9;
	mso-style-qformat:yes;
	mso-style-link:"Heading 2 Char";
	mso-style-next:Normal;
	mso-style-type:export-only;
	margin-top:.25in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:0in;
	margin-bottom:.0001pt;
	mso-add-space:auto;
	line-height:115%;
	mso-pagination:widow-orphan lines-together;
	page-break-after:avoid;
	mso-outline-level:2;
	font-size:18.0pt;
	font-family:"Alte Haas Grotesk";
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:minor-fareast;
	mso-bidi-font-family:"Times New Roman";
	color:black;
	mso-bidi-font-weight:normal;}
h2.CxSpMiddle
	{mso-style-noshow:yes;
	mso-style-priority:9;
	mso-style-qformat:yes;
	mso-style-link:"Heading 2 Char";
	mso-style-next:Normal;
	mso-style-type:export-only;
	margin:0in;
	margin-bottom:.0001pt;
	mso-add-space:auto;
	line-height:115%;
	mso-pagination:widow-orphan lines-together;
	page-break-after:avoid;
	mso-outline-level:2;
	font-size:18.0pt;
	font-family:"Alte Haas Grotesk";
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:minor-fareast;
	mso-bidi-font-family:"Times New Roman";
	color:black;
	mso-bidi-font-weight:normal;}
h2.CxSpLast
	{mso-style-noshow:yes;
	mso-style-priority:9;
	mso-style-qformat:yes;
	mso-style-link:"Heading 2 Char";
	mso-style-next:Normal;
	mso-style-type:export-only;
	margin-top:0in;
	margin-right:0in;
	margin-bottom:4.0pt;
	margin-left:0in;
	mso-add-space:auto;
	line-height:115%;
	mso-pagination:widow-orphan lines-together;
	page-break-after:avoid;
	mso-outline-level:2;
	font-size:18.0pt;
	font-family:"Alte Haas Grotesk";
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:minor-fareast;
	mso-bidi-font-family:"Times New Roman";
	color:black;
	mso-bidi-font-weight:normal;}
h3
	{mso-style-noshow:yes;
	mso-style-priority:9;
	mso-style-qformat:yes;
	mso-style-link:"Heading 3 Char";
	mso-style-next:Normal;
	margin-top:14.0pt;
	margin-right:0in;
	margin-bottom:4.0pt;
	margin-left:0in;
	mso-add-space:auto;
	line-height:115%;
	mso-pagination:widow-orphan lines-together;
	page-break-after:avoid;
	mso-outline-level:3;
	font-size:14.0pt;
	font-family:"Alte Haas Grotesk";
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:minor-fareast;
	mso-bidi-font-family:"Times New Roman";
	color:black;
	mso-bidi-font-weight:normal;}
h3.CxSpFirst
	{mso-style-noshow:yes;
	mso-style-priority:9;
	mso-style-qformat:yes;
	mso-style-link:"Heading 3 Char";
	mso-style-next:Normal;
	mso-style-type:export-only;
	margin-top:14.0pt;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:0in;
	margin-bottom:.0001pt;
	mso-add-space:auto;
	line-height:115%;
	mso-pagination:widow-orphan lines-together;
	page-break-after:avoid;
	mso-outline-level:3;
	font-size:14.0pt;
	font-family:"Alte Haas Grotesk";
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:minor-fareast;
	mso-bidi-font-family:"Times New Roman";
	color:black;
	mso-bidi-font-weight:normal;}
h3.CxSpMiddle
	{mso-style-noshow:yes;
	mso-style-priority:9;
	mso-style-qformat:yes;
	mso-style-link:"Heading 3 Char";
	mso-style-next:Normal;
	mso-style-type:export-only;
	margin:0in;
	margin-bottom:.0001pt;
	mso-add-space:auto;
	line-height:115%;
	mso-pagination:widow-orphan lines-together;
	page-break-after:avoid;
	mso-outline-level:3;
	font-size:14.0pt;
	font-family:"Alte Haas Grotesk";
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:minor-fareast;
	mso-bidi-font-family:"Times New Roman";
	color:black;
	mso-bidi-font-weight:normal;}
h3.CxSpLast
	{mso-style-noshow:yes;
	mso-style-priority:9;
	mso-style-qformat:yes;
	mso-style-link:"Heading 3 Char";
	mso-style-next:Normal;
	mso-style-type:export-only;
	margin-top:0in;
	margin-right:0in;
	margin-bottom:4.0pt;
	margin-left:0in;
	mso-add-space:auto;
	line-height:115%;
	mso-pagination:widow-orphan lines-together;
	page-break-after:avoid;
	mso-outline-level:3;
	font-size:14.0pt;
	font-family:"Alte Haas Grotesk";
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:minor-fareast;
	mso-bidi-font-family:"Times New Roman";
	color:black;
	mso-bidi-font-weight:normal;}
h4
	{mso-style-noshow:yes;
	mso-style-priority:9;
	mso-style-qformat:yes;
	mso-style-link:"Heading 4 Char";
	mso-style-next:Normal;
	margin-top:12.0pt;
	margin-right:0in;
	margin-bottom:2.0pt;
	margin-left:0in;
	mso-add-space:auto;
	line-height:115%;
	mso-pagination:widow-orphan lines-together;
	page-break-after:avoid;
	mso-outline-level:4;
	font-size:12.0pt;
	font-family:"Alte Haas Grotesk";
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:minor-fareast;
	mso-bidi-font-family:"Times New Roman";
	color:black;
	mso-bidi-font-weight:normal;}
h4.CxSpFirst
	{mso-style-noshow:yes;
	mso-style-priority:9;
	mso-style-qformat:yes;
	mso-style-link:"Heading 4 Char";
	mso-style-next:Normal;
	mso-style-type:export-only;
	margin-top:12.0pt;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:0in;
	margin-bottom:.0001pt;
	mso-add-space:auto;
	line-height:115%;
	mso-pagination:widow-orphan lines-together;
	page-break-after:avoid;
	mso-outline-level:4;
	font-size:12.0pt;
	font-family:"Alte Haas Grotesk";
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:minor-fareast;
	mso-bidi-font-family:"Times New Roman";
	color:black;
	mso-bidi-font-weight:normal;}
h4.CxSpMiddle
	{mso-style-noshow:yes;
	mso-style-priority:9;
	mso-style-qformat:yes;
	mso-style-link:"Heading 4 Char";
	mso-style-next:Normal;
	mso-style-type:export-only;
	margin:0in;
	margin-bottom:.0001pt;
	mso-add-space:auto;
	line-height:115%;
	mso-pagination:widow-orphan lines-together;
	page-break-after:avoid;
	mso-outline-level:4;
	font-size:12.0pt;
	font-family:"Alte Haas Grotesk";
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:minor-fareast;
	mso-bidi-font-family:"Times New Roman";
	color:black;
	mso-bidi-font-weight:normal;}
h4.CxSpLast
	{mso-style-noshow:yes;
	mso-style-priority:9;
	mso-style-qformat:yes;
	mso-style-link:"Heading 4 Char";
	mso-style-next:Normal;
	mso-style-type:export-only;
	margin-top:0in;
	margin-right:0in;
	margin-bottom:2.0pt;
	margin-left:0in;
	mso-add-space:auto;
	line-height:115%;
	mso-pagination:widow-orphan lines-together;
	page-break-after:avoid;
	mso-outline-level:4;
	font-size:12.0pt;
	font-family:"Alte Haas Grotesk";
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:minor-fareast;
	mso-bidi-font-family:"Times New Roman";
	color:black;
	mso-bidi-font-weight:normal;}
h5
	{mso-style-noshow:yes;
	mso-style-priority:9;
	mso-style-qformat:yes;
	mso-style-link:"Heading 5 Char";
	mso-style-next:Normal;
	margin-top:11.0pt;
	margin-right:0in;
	margin-bottom:2.0pt;
	margin-left:0in;
	mso-add-space:auto;
	line-height:115%;
	mso-pagination:widow-orphan lines-together;
	page-break-after:avoid;
	mso-outline-level:5;
	font-size:11.0pt;
	font-family:"Alte Haas Grotesk";
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:minor-fareast;
	mso-bidi-font-family:"Times New Roman";
	color:black;
	mso-bidi-font-weight:normal;}
h5.CxSpFirst
	{mso-style-noshow:yes;
	mso-style-priority:9;
	mso-style-qformat:yes;
	mso-style-link:"Heading 5 Char";
	mso-style-next:Normal;
	mso-style-type:export-only;
	margin-top:11.0pt;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:0in;
	margin-bottom:.0001pt;
	mso-add-space:auto;
	line-height:115%;
	mso-pagination:widow-orphan lines-together;
	page-break-after:avoid;
	mso-outline-level:5;
	font-size:11.0pt;
	font-family:"Alte Haas Grotesk";
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:minor-fareast;
	mso-bidi-font-family:"Times New Roman";
	color:black;
	mso-bidi-font-weight:normal;}
h5.CxSpMiddle
	{mso-style-noshow:yes;
	mso-style-priority:9;
	mso-style-qformat:yes;
	mso-style-link:"Heading 5 Char";
	mso-style-next:Normal;
	mso-style-type:export-only;
	margin:0in;
	margin-bottom:.0001pt;
	mso-add-space:auto;
	line-height:115%;
	mso-pagination:widow-orphan lines-together;
	page-break-after:avoid;
	mso-outline-level:5;
	font-size:11.0pt;
	font-family:"Alte Haas Grotesk";
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:minor-fareast;
	mso-bidi-font-family:"Times New Roman";
	color:black;
	mso-bidi-font-weight:normal;}
h5.CxSpLast
	{mso-style-noshow:yes;
	mso-style-priority:9;
	mso-style-qformat:yes;
	mso-style-link:"Heading 5 Char";
	mso-style-next:Normal;
	mso-style-type:export-only;
	margin-top:0in;
	margin-right:0in;
	margin-bottom:2.0pt;
	margin-left:0in;
	mso-add-space:auto;
	line-height:115%;
	mso-pagination:widow-orphan lines-together;
	page-break-after:avoid;
	mso-outline-level:5;
	font-size:11.0pt;
	font-family:"Alte Haas Grotesk";
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:minor-fareast;
	mso-bidi-font-family:"Times New Roman";
	color:black;
	mso-bidi-font-weight:normal;}
h6
	{mso-style-noshow:yes;
	mso-style-priority:9;
	mso-style-qformat:yes;
	mso-style-link:"Heading 6 Char";
	mso-style-next:Normal;
	margin-top:10.0pt;
	margin-right:0in;
	margin-bottom:2.0pt;
	margin-left:0in;
	mso-add-space:auto;
	line-height:115%;
	mso-pagination:widow-orphan lines-together;
	page-break-after:avoid;
	mso-outline-level:6;
	font-size:10.0pt;
	font-family:"Alte Haas Grotesk";
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:minor-fareast;
	mso-bidi-font-family:"Times New Roman";
	color:black;
	mso-bidi-font-weight:normal;}
h6.CxSpFirst
	{mso-style-noshow:yes;
	mso-style-priority:9;
	mso-style-qformat:yes;
	mso-style-link:"Heading 6 Char";
	mso-style-next:Normal;
	mso-style-type:export-only;
	margin-top:10.0pt;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:0in;
	margin-bottom:.0001pt;
	mso-add-space:auto;
	line-height:115%;
	mso-pagination:widow-orphan lines-together;
	page-break-after:avoid;
	mso-outline-level:6;
	font-size:10.0pt;
	font-family:"Alte Haas Grotesk";
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:minor-fareast;
	mso-bidi-font-family:"Times New Roman";
	color:black;
	mso-bidi-font-weight:normal;}
h6.CxSpMiddle
	{mso-style-noshow:yes;
	mso-style-priority:9;
	mso-style-qformat:yes;
	mso-style-link:"Heading 6 Char";
	mso-style-next:Normal;
	mso-style-type:export-only;
	margin:0in;
	margin-bottom:.0001pt;
	mso-add-space:auto;
	line-height:115%;
	mso-pagination:widow-orphan lines-together;
	page-break-after:avoid;
	mso-outline-level:6;
	font-size:10.0pt;
	font-family:"Alte Haas Grotesk";
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:minor-fareast;
	mso-bidi-font-family:"Times New Roman";
	color:black;
	mso-bidi-font-weight:normal;}
h6.CxSpLast
	{mso-style-noshow:yes;
	mso-style-priority:9;
	mso-style-qformat:yes;
	mso-style-link:"Heading 6 Char";
	mso-style-next:Normal;
	mso-style-type:export-only;
	margin-top:0in;
	margin-right:0in;
	margin-bottom:2.0pt;
	margin-left:0in;
	mso-add-space:auto;
	line-height:115%;
	mso-pagination:widow-orphan lines-together;
	page-break-after:avoid;
	mso-outline-level:6;
	font-size:10.0pt;
	font-family:"Alte Haas Grotesk";
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:minor-fareast;
	mso-bidi-font-family:"Times New Roman";
	color:black;
	mso-bidi-font-weight:normal;}
p.MsoTitle, li.MsoTitle, div.MsoTitle
	{mso-style-priority:10;
	mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-link:"Title Char";
	mso-style-next:Normal;
	margin-top:24.0pt;
	margin-right:0in;
	margin-bottom:6.0pt;
	margin-left:0in;
	mso-add-space:auto;
	line-height:115%;
	mso-pagination:widow-orphan lines-together;
	page-break-after:avoid;
	font-size:36.0pt;
	font-family:"Alte Haas Grotesk";
	mso-fareast-font-family:"Alte Haas Grotesk";
	mso-bidi-font-family:"Alte Haas Grotesk";
	color:black;
	font-weight:bold;
	mso-bidi-font-weight:normal;}
p.MsoTitleCxSpFirst, li.MsoTitleCxSpFirst, div.MsoTitleCxSpFirst
	{mso-style-priority:10;
	mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-link:"Title Char";
	mso-style-next:Normal;
	mso-style-type:export-only;
	margin-top:24.0pt;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:0in;
	margin-bottom:.0001pt;
	mso-add-space:auto;
	line-height:115%;
	mso-pagination:widow-orphan lines-together;
	page-break-after:avoid;
	font-size:36.0pt;
	font-family:"Alte Haas Grotesk";
	mso-fareast-font-family:"Alte Haas Grotesk";
	mso-bidi-font-family:"Alte Haas Grotesk";
	color:black;
	font-weight:bold;
	mso-bidi-font-weight:normal;}
p.MsoTitleCxSpMiddle, li.MsoTitleCxSpMiddle, div.MsoTitleCxSpMiddle
	{mso-style-priority:10;
	mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-link:"Title Char";
	mso-style-next:Normal;
	mso-style-type:export-only;
	margin:0in;
	margin-bottom:.0001pt;
	mso-add-space:auto;
	line-height:115%;
	mso-pagination:widow-orphan lines-together;
	page-break-after:avoid;
	font-size:36.0pt;
	font-family:"Alte Haas Grotesk";
	mso-fareast-font-family:"Alte Haas Grotesk";
	mso-bidi-font-family:"Alte Haas Grotesk";
	color:black;
	font-weight:bold;
	mso-bidi-font-weight:normal;}
p.MsoTitleCxSpLast, li.MsoTitleCxSpLast, div.MsoTitleCxSpLast
	{mso-style-priority:10;
	mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-link:"Title Char";
	mso-style-next:Normal;
	mso-style-type:export-only;
	margin-top:0in;
	margin-right:0in;
	margin-bottom:6.0pt;
	margin-left:0in;
	mso-add-space:auto;
	line-height:115%;
	mso-pagination:widow-orphan lines-together;
	page-break-after:avoid;
	font-size:36.0pt;
	font-family:"Alte Haas Grotesk";
	mso-fareast-font-family:"Alte Haas Grotesk";
	mso-bidi-font-family:"Alte Haas Grotesk";
	color:black;
	font-weight:bold;
	mso-bidi-font-weight:normal;}
p.MsoSubtitle, li.MsoSubtitle, div.MsoSubtitle
	{mso-style-priority:11;
	mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-link:"Subtitle Char";
	mso-style-next:Normal;
	margin-top:.25in;
	margin-right:0in;
	margin-bottom:4.0pt;
	margin-left:0in;
	mso-add-space:auto;
	line-height:115%;
	mso-pagination:widow-orphan lines-together;
	page-break-after:avoid;
	font-size:24.0pt;
	font-family:"Georgia","serif";
	mso-fareast-font-family:Georgia;
	mso-bidi-font-family:Georgia;
	color:#666666;
	font-style:italic;
	mso-bidi-font-style:normal;}
p.MsoSubtitleCxSpFirst, li.MsoSubtitleCxSpFirst, div.MsoSubtitleCxSpFirst
	{mso-style-priority:11;
	mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-link:"Subtitle Char";
	mso-style-next:Normal;
	mso-style-type:export-only;
	margin-top:.25in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:0in;
	margin-bottom:.0001pt;
	mso-add-space:auto;
	line-height:115%;
	mso-pagination:widow-orphan lines-together;
	page-break-after:avoid;
	font-size:24.0pt;
	font-family:"Georgia","serif";
	mso-fareast-font-family:Georgia;
	mso-bidi-font-family:Georgia;
	color:#666666;
	font-style:italic;
	mso-bidi-font-style:normal;}
p.MsoSubtitleCxSpMiddle, li.MsoSubtitleCxSpMiddle, div.MsoSubtitleCxSpMiddle
	{mso-style-priority:11;
	mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-link:"Subtitle Char";
	mso-style-next:Normal;
	mso-style-type:export-only;
	margin:0in;
	margin-bottom:.0001pt;
	mso-add-space:auto;
	line-height:115%;
	mso-pagination:widow-orphan lines-together;
	page-break-after:avoid;
	font-size:24.0pt;
	font-family:"Georgia","serif";
	mso-fareast-font-family:Georgia;
	mso-bidi-font-family:Georgia;
	color:#666666;
	font-style:italic;
	mso-bidi-font-style:normal;}
p.MsoSubtitleCxSpLast, li.MsoSubtitleCxSpLast, div.MsoSubtitleCxSpLast
	{mso-style-priority:11;
	mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-link:"Subtitle Char";
	mso-style-next:Normal;
	mso-style-type:export-only;
	margin-top:0in;
	margin-right:0in;
	margin-bottom:4.0pt;
	margin-left:0in;
	mso-add-space:auto;
	line-height:115%;
	mso-pagination:widow-orphan lines-together;
	page-break-after:avoid;
	font-size:24.0pt;
	font-family:"Georgia","serif";
	mso-fareast-font-family:Georgia;
	mso-bidi-font-family:Georgia;
	color:#666666;
	font-style:italic;
	mso-bidi-font-style:normal;}
p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
	{mso-style-noshow:yes;
	mso-style-priority:99;
	mso-style-link:"Balloon Text Char";
	margin:0in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:8.0pt;
	font-family:"Tahoma","sans-serif";
	mso-fareast-font-family:"Alte Haas Grotesk";
	color:black;}
span.Heading1Char
	{mso-style-name:"Heading 1 Char";
	mso-style-priority:9;
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:"Heading 1";
	mso-ansi-font-size:14.0pt;
	mso-bidi-font-size:14.0pt;
	font-family:"Cambria","serif";
	mso-ascii-font-family:Cambria;
	mso-ascii-theme-font:major-latin;
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:major-fareast;
	mso-hansi-font-family:Cambria;
	mso-hansi-theme-font:major-latin;
	mso-bidi-font-family:"Times New Roman";
	mso-bidi-theme-font:major-bidi;
	color:#365F91;
	mso-themecolor:accent1;
	mso-themeshade:191;
	font-weight:bold;}
span.Heading2Char
	{mso-style-name:"Heading 2 Char";
	mso-style-noshow:yes;
	mso-style-priority:9;
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:"Heading 2";
	mso-ansi-font-size:13.0pt;
	mso-bidi-font-size:13.0pt;
	font-family:"Cambria","serif";
	mso-ascii-font-family:Cambria;
	mso-ascii-theme-font:major-latin;
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:major-fareast;
	mso-hansi-font-family:Cambria;
	mso-hansi-theme-font:major-latin;
	mso-bidi-font-family:"Times New Roman";
	mso-bidi-theme-font:major-bidi;
	color:#4F81BD;
	mso-themecolor:accent1;
	font-weight:bold;}
span.Heading3Char
	{mso-style-name:"Heading 3 Char";
	mso-style-noshow:yes;
	mso-style-priority:9;
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:"Heading 3";
	font-family:"Cambria","serif";
	mso-ascii-font-family:Cambria;
	mso-ascii-theme-font:major-latin;
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:major-fareast;
	mso-hansi-font-family:Cambria;
	mso-hansi-theme-font:major-latin;
	mso-bidi-font-family:"Times New Roman";
	mso-bidi-theme-font:major-bidi;
	color:#4F81BD;
	mso-themecolor:accent1;
	font-weight:bold;}
span.Heading4Char
	{mso-style-name:"Heading 4 Char";
	mso-style-noshow:yes;
	mso-style-priority:9;
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:"Heading 4";
	font-family:"Cambria","serif";
	mso-ascii-font-family:Cambria;
	mso-ascii-theme-font:major-latin;
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:major-fareast;
	mso-hansi-font-family:Cambria;
	mso-hansi-theme-font:major-latin;
	mso-bidi-font-family:"Times New Roman";
	mso-bidi-theme-font:major-bidi;
	color:#4F81BD;
	mso-themecolor:accent1;
	font-weight:bold;
	font-style:italic;}
span.Heading5Char
	{mso-style-name:"Heading 5 Char";
	mso-style-noshow:yes;
	mso-style-priority:9;
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:"Heading 5";
	font-family:"Cambria","serif";
	mso-ascii-font-family:Cambria;
	mso-ascii-theme-font:major-latin;
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:major-fareast;
	mso-hansi-font-family:Cambria;
	mso-hansi-theme-font:major-latin;
	mso-bidi-font-family:"Times New Roman";
	mso-bidi-theme-font:major-bidi;
	color:#243F60;
	mso-themecolor:accent1;
	mso-themeshade:127;}
span.Heading6Char
	{mso-style-name:"Heading 6 Char";
	mso-style-noshow:yes;
	mso-style-priority:9;
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:"Heading 6";
	font-family:"Cambria","serif";
	mso-ascii-font-family:Cambria;
	mso-ascii-theme-font:major-latin;
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:major-fareast;
	mso-hansi-font-family:Cambria;
	mso-hansi-theme-font:major-latin;
	mso-bidi-font-family:"Times New Roman";
	mso-bidi-theme-font:major-bidi;
	color:#243F60;
	mso-themecolor:accent1;
	mso-themeshade:127;
	font-style:italic;}
span.TitleChar
	{mso-style-name:"Title Char";
	mso-style-priority:10;
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:Title;
	mso-ansi-font-size:26.0pt;
	mso-bidi-font-size:26.0pt;
	font-family:"Cambria","serif";
	mso-ascii-font-family:Cambria;
	mso-ascii-theme-font:major-latin;
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:major-fareast;
	mso-hansi-font-family:Cambria;
	mso-hansi-theme-font:major-latin;
	mso-bidi-font-family:"Times New Roman";
	mso-bidi-theme-font:major-bidi;
	color:#17365D;
	mso-themecolor:text2;
	mso-themeshade:191;
	letter-spacing:.25pt;
	mso-font-kerning:14.0pt;}
span.SubtitleChar
	{mso-style-name:"Subtitle Char";
	mso-style-priority:11;
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:Subtitle;
	mso-ansi-font-size:12.0pt;
	mso-bidi-font-size:12.0pt;
	font-family:"Cambria","serif";
	mso-ascii-font-family:Cambria;
	mso-ascii-theme-font:major-latin;
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:major-fareast;
	mso-hansi-font-family:Cambria;
	mso-hansi-theme-font:major-latin;
	mso-bidi-font-family:"Times New Roman";
	mso-bidi-theme-font:major-bidi;
	color:#4F81BD;
	mso-themecolor:accent1;
	letter-spacing:.75pt;
	font-style:italic;}
span.BalloonTextChar
	{mso-style-name:"Balloon Text Char";
	mso-style-noshow:yes;
	mso-style-priority:99;
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:"Balloon Text";
	mso-ansi-font-size:8.0pt;
	mso-bidi-font-size:8.0pt;
	font-family:"Tahoma","sans-serif";
	mso-ascii-font-family:Tahoma;
	mso-hansi-font-family:Tahoma;
	mso-bidi-font-family:Tahoma;}
.MsoChpDefault
	{mso-style-type:export-only;
	mso-default-props:yes;
	font-size:10.0pt;
	mso-ansi-font-size:10.0pt;
	mso-bidi-font-size:10.0pt;
	font-family:"Alte Haas Grotesk";
	mso-ascii-font-family:"Alte Haas Grotesk";
	mso-fareast-font-family:"Alte Haas Grotesk";
	mso-hansi-font-family:"Alte Haas Grotesk";
	mso-bidi-font-family:"Alte Haas Grotesk";
	color:black;}
@page WordSection1
	{size:595.3pt 841.9pt;
	margin:1.0in 1.0in 1.0in 1.0in;
	mso-header-margin:0in;
	mso-footer-margin:.5in;
	mso-page-numbers:1;
	mso-paper-source:0;}
div.WordSection1
	{page:WordSection1;}
-->
</style>
<!--[if gte mso 10]>
<style>
 /* Style Definitions */
 table.MsoNormalTable
	{mso-style-name:"Table Normal";
	mso-tstyle-rowband-size:0;
	mso-tstyle-colband-size:0;
	mso-style-noshow:yes;
	mso-style-priority:99;
	mso-style-parent:"";
	mso-padding-alt:0in 5.4pt 0in 5.4pt;
	mso-para-margin:0in;
	mso-para-margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	font-family:"Alte Haas Grotesk";
	color:black;}
</style>
<![endif]--><!--[if gte mso 9]><xml>
 <o:shapedefaults v:ext=3D"edit" spidmax=3D"1027"/>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <o:shapelayout v:ext=3D"edit">
  <o:idmap v:ext=3D"edit" data=3D"1"/>
 </o:shapelayout></xml><![endif]-->
</head>

<body bgcolor=3Dwhite lang=3DEN-US style=3D'tab-interval:.5in'>

<div class=3DWordSection1>

<p class=3DMsoNormal><!--[if gte vml 1]><v:shapetype id=3D"_x0000_t75" coor=
dsize=3D"21600,21600"
 o:spt=3D"75" o:preferrelative=3D"t" path=3D"m@4@5l@4@11@9@11@9@5xe" filled=
=3D"f"
 stroked=3D"f">
 <v:stroke joinstyle=3D"miter"/>
 <v:formulas>
  <v:f eqn=3D"if lineDrawn pixelLineWidth 0"/>
  <v:f eqn=3D"sum @0 1 0"/>
  <v:f eqn=3D"sum 0 0 @1"/>
  <v:f eqn=3D"prod @2 1 2"/>
  <v:f eqn=3D"prod @3 21600 pixelWidth"/>
  <v:f eqn=3D"prod @3 21600 pixelHeight"/>
  <v:f eqn=3D"sum @0 0 1"/>
  <v:f eqn=3D"prod @6 1 2"/>
  <v:f eqn=3D"prod @7 21600 pixelWidth"/>
  <v:f eqn=3D"sum @8 21600 0"/>
  <v:f eqn=3D"prod @7 21600 pixelHeight"/>
  <v:f eqn=3D"sum @10 21600 0"/>
 </v:formulas>
 <v:path o:extrusionok=3D"f" gradientshapeok=3D"t" o:connecttype=3D"rect"/>
 <o:lock v:ext=3D"edit" aspectratio=3D"t"/>
</v:shapetype><v:shape id=3D"Picture_x0020_2" o:spid=3D"_x0000_s1026" type=
=3D"#_x0000_t75"
 style=3D'position:absolute;margin-left:337.5pt;margin-top:-3.75pt;width:13=
2.7pt;
 height:53.1pt;z-index:251658240;visibility:visible;mso-wrap-style:square;
 mso-width-percent:0;mso-height-percent:0;mso-wrap-distance-left:9pt;
 mso-wrap-distance-top:0;mso-wrap-distance-right:9pt;
 mso-wrap-distance-bottom:0;mso-position-horizontal:absolute;
 mso-position-horizontal-relative:text;mso-position-vertical:absolute;
 mso-position-vertical-relative:text;mso-width-percent:0;mso-height-percent=
:0;
 mso-width-relative:page;mso-height-relative:page'>
 <v:imagedata src=3D"page2_files/image001.png" o:title=3D""/>
</v:shape><![endif]--><![if !vml]><span style=3D'mso-ignore:vglayout;positi=
on:
relative;z-index:251658240'><span style=3D'position:absolute;left:450px;
top:-5px;width:177px;height:71px'><img width=3D177 height=3D71
src=3D"page2_files/image002.gif" v:shapes=3D"Picture_x0020_2"></span></span=
><![endif]><o:p>&nbsp;</o:p></p>

<p class=3DMsoNormal><o:p>&nbsp;</o:p></p>

<br style=3D'mso-ignore:vglayout' clear=3DALL>

<p class=3DMsoNormal><span style=3D'font-size:12.0pt;line-height:115%;font-=
family:
"Arial","sans-serif";mso-fareast-font-family:Arial'>Assignment Front Sheet<=
/span></p>

<table class=3DMsoNormalTable border=3D1 cellspacing=3D0 cellpadding=3D0 wi=
dth=3D650
 style=3D'margin-left:-11.4pt;border-collapse:collapse;mso-table-layout-alt=
:fixed;
 border:none;mso-border-alt:solid black .5pt;mso-yfti-tbllook:1024;mso-padd=
ing-alt:
 0in 5.75pt 0in 5.75pt;mso-border-insideh:.5pt solid black;mso-border-insid=
ev:
 .5pt solid black'>
 <tr style=3D'mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td width=3D357 colspan=3D2 valign=3Dtop style=3D'width:267.6pt;border:so=
lid black 1.0pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><span style=3D'font-size:11.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:Arial'>Learner Name: {{student name</span></p>
  </td>
  <td width=3D293 colspan=3D3 valign=3Dtop style=3D'width:219.75pt;border:s=
olid black 1.0pt;
  border-left:none;mso-border-left-alt:solid black .5pt;mso-border-alt:soli=
d black .5pt;
  padding:0in 5.75pt 0in 5.75pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><span style=3D'font-family:"Arial","sans-serif";mso-fareast-font-=
family:
  Arial'>Learner Registration ID: {{Athe ID if any ( lwc)</span></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:1;height:65.0pt'>
  <td width=3D650 colspan=3D5 valign=3Dtop style=3D'width:487.35pt;border:s=
olid black 1.0pt;
  border-top:none;mso-border-top-alt:solid black .5pt;mso-border-alt:solid =
black .5pt;
  padding:0in 5.75pt 0in 5.75pt;height:65.0pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><span style=3D'font-size:11.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:Arial'>Qualification: {{<span style=3D'color:red'=
>course
  name</span></span></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><span style=3D'font-size:11.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:Arial'>Unit Title:<span style=3D'color:red'> {{co=
de
  course </span></span></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><span style=3D'font-size:11.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:Arial'>Assignment Title: {{<span style=3D'color:r=
ed'>module
  name</span></span></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><span style=3D'font-size:11.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:Arial'>Assignment issued: {{<span style=3D'color:=
red'>start
  date</span></span></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><span style=3D'font-size:11.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:Arial'>Assignment submitted: {{<span
  style=3D'color:red'>date last task completion</span></span></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:2'>
  <td width=3D103 valign=3Dtop style=3D'width:76.95pt;border:solid black 1.=
0pt;
  border-top:none;mso-border-top-alt:solid black .5pt;mso-border-alt:solid =
black .5pt;
  padding:0in 5.75pt 0in 5.75pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><span style=3D'font-size:11.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:Arial'>Learning Outcomes</span></p>
  </td>
  <td width=3D254 valign=3Dtop style=3D'width:190.65pt;border-top:none;bord=
er-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><span style=3D'font-size:11.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:Arial'>Assessment Criteria</span></p>
  </td>
  <td width=3D44 valign=3Dtop style=3D'width:33.0pt;border-top:none;border-=
left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;mso-border=
-top-alt:
  solid black .5pt;mso-border-left-alt:solid black .5pt;mso-border-alt:soli=
d black .5pt;
  padding:0in 5.75pt 0in 5.75pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><span style=3D'font-size:11.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:Arial'>A</span></p>
  </td>
  <td width=3D57 valign=3Dtop style=3D'width:42.75pt;border-top:none;border=
-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><span style=3D'font-size:11.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:Arial'>NYA</span></p>
  </td>
  <td width=3D192 valign=3Dtop style=3D'width:2.0in;border-top:none;border-=
left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;mso-border=
-top-alt:
  solid black .5pt;mso-border-left-alt:solid black .5pt;mso-border-alt:soli=
d black .5pt;
  padding:0in 5.75pt 0in 5.75pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><span style=3D'font-size:11.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:Arial'>Evidence to show achievement of the LO</sp=
an></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:3;height:17.0pt'>
  <td width=3D103 rowspan=3D10 valign=3Dtop style=3D'width:76.95pt;border:s=
olid black 1.0pt;
  border-top:none;mso-border-top-alt:solid black .5pt;mso-border-alt:solid =
black .5pt;
  padding:0in 5.75pt 0in 5.75pt;height:17.0pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><span style=3D'font-size:11.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:Arial'>LO1 {{1}}</span></p>
  </td>
  <td width=3D254 valign=3Dtop style=3D'width:190.65pt;border-top:none;bord=
er-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><span style=3D'font-size:11.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:Arial'>1.1 {{1}}</span></p>
  </td>
  <td width=3D44 rowspan=3D10 valign=3Dtop style=3D'width:33.0pt;border-top=
:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black=
 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'>{{1}}</p>
  </td>
  <td width=3D57 rowspan=3D10 valign=3Dtop style=3D'width:42.75pt;border-to=
p:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black=
 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'>{{1}}</p>
  </td>
  <td width=3D192 rowspan=3D10 valign=3Dtop style=3D'width:2.0in;border-top=
:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black=
 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'>{{1}}</p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:4;height:17.0pt'>
  <td width=3D254 valign=3Dtop style=3D'width:190.65pt;border-top:none;bord=
er-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><span style=3D'font-size:11.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:Arial;color:red'>{{1}} </span></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:5;height:17.0pt'>
  <td width=3D254 valign=3Dtop style=3D'width:190.65pt;border-top:none;bord=
er-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><span style=3D'font-size:11.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:Arial'>1.2 {{2}}</span></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:6;height:17.0pt'>
  <td width=3D254 valign=3Dtop style=3D'width:190.65pt;border-top:none;bord=
er-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'>{{2}}</p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:7;height:17.0pt'>
  <td width=3D254 valign=3Dtop style=3D'width:190.65pt;border-top:none;bord=
er-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><span style=3D'font-size:11.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:Arial'>1.3</span></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:8;height:17.0pt'>
  <td width=3D254 valign=3Dtop style=3D'width:190.65pt;border-top:none;bord=
er-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:9;height:17.0pt'>
  <td width=3D254 valign=3Dtop style=3D'width:190.65pt;border-top:none;bord=
er-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><span style=3D'font-size:11.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:Arial'>1.4</span></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:10;height:17.0pt'>
  <td width=3D254 valign=3Dtop style=3D'width:190.65pt;border-top:none;bord=
er-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:11;height:17.0pt'>
  <td width=3D254 valign=3Dtop style=3D'width:190.65pt;border-top:none;bord=
er-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><span style=3D'font-size:11.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:Arial'>1.5</span></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:12;height:17.0pt'>
  <td width=3D254 valign=3Dtop style=3D'width:190.65pt;border-top:none;bord=
er-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:13;height:17.0pt'>
  <td width=3D103 rowspan=3D10 valign=3Dtop style=3D'width:76.95pt;border:s=
olid black 1.0pt;
  border-top:none;mso-border-top-alt:solid black .5pt;mso-border-alt:solid =
black .5pt;
  padding:0in 5.75pt 0in 5.75pt;height:17.0pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><span style=3D'font-size:11.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:Arial'>LO2</span></p>
  </td>
  <td width=3D254 valign=3Dtop style=3D'width:190.65pt;border-top:none;bord=
er-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><span style=3D'font-size:11.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:Arial'>2.1</span></p>
  </td>
  <td width=3D44 rowspan=3D10 valign=3Dtop style=3D'width:33.0pt;border-top=
:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black=
 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  </td>
  <td width=3D57 rowspan=3D10 valign=3Dtop style=3D'width:42.75pt;border-to=
p:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black=
 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  </td>
  <td width=3D192 rowspan=3D10 valign=3Dtop style=3D'width:2.0in;border-top=
:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black=
 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:14;height:17.0pt'>
  <td width=3D254 valign=3Dtop style=3D'width:190.65pt;border-top:none;bord=
er-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:15;height:17.0pt'>
  <td width=3D254 valign=3Dtop style=3D'width:190.65pt;border-top:none;bord=
er-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><span style=3D'font-size:11.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:Arial'>2.2</span></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:16;height:17.0pt'>
  <td width=3D254 valign=3Dtop style=3D'width:190.65pt;border-top:none;bord=
er-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:17;height:17.0pt'>
  <td width=3D254 valign=3Dtop style=3D'width:190.65pt;border-top:none;bord=
er-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><span style=3D'font-size:11.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:Arial'>2.3</span></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:18;height:17.0pt'>
  <td width=3D254 valign=3Dtop style=3D'width:190.65pt;border-top:none;bord=
er-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:19;height:17.0pt'>
  <td width=3D254 valign=3Dtop style=3D'width:190.65pt;border-top:none;bord=
er-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><span style=3D'font-size:11.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:Arial'>2.4</span></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:20;height:17.0pt'>
  <td width=3D254 valign=3Dtop style=3D'width:190.65pt;border-top:none;bord=
er-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:21;height:17.0pt'>
  <td width=3D254 valign=3Dtop style=3D'width:190.65pt;border-top:none;bord=
er-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><span style=3D'font-size:11.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:Arial'>2.5</span></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:22;height:17.0pt'>
  <td width=3D254 valign=3Dtop style=3D'width:190.65pt;border-top:none;bord=
er-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:23;height:17.0pt'>
  <td width=3D103 rowspan=3D10 valign=3Dtop style=3D'width:76.95pt;border:s=
olid black 1.0pt;
  border-top:none;mso-border-top-alt:solid black .5pt;mso-border-alt:solid =
black .5pt;
  padding:0in 5.75pt 0in 5.75pt;height:17.0pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><span style=3D'font-size:11.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:Arial'>LO3</span></p>
  </td>
  <td width=3D254 valign=3Dtop style=3D'width:190.65pt;border-top:none;bord=
er-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><span style=3D'font-size:11.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:Arial'>3.1</span></p>
  </td>
  <td width=3D44 rowspan=3D10 valign=3Dtop style=3D'width:33.0pt;border-top=
:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black=
 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  </td>
  <td width=3D57 rowspan=3D10 valign=3Dtop style=3D'width:42.75pt;border-to=
p:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black=
 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  </td>
  <td width=3D192 rowspan=3D10 valign=3Dtop style=3D'width:2.0in;border-top=
:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black=
 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:24;height:17.0pt'>
  <td width=3D254 valign=3Dtop style=3D'width:190.65pt;border-top:none;bord=
er-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:25;height:17.0pt'>
  <td width=3D254 valign=3Dtop style=3D'width:190.65pt;border-top:none;bord=
er-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><span style=3D'font-size:11.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:Arial'>3.2</span></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:26;height:17.0pt'>
  <td width=3D254 valign=3Dtop style=3D'width:190.65pt;border-top:none;bord=
er-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:27;height:17.0pt'>
  <td width=3D254 valign=3Dtop style=3D'width:190.65pt;border-top:none;bord=
er-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><span style=3D'font-size:11.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:Arial'>3.3</span></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:28;height:17.0pt'>
  <td width=3D254 valign=3Dtop style=3D'width:190.65pt;border-top:none;bord=
er-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:29;height:17.0pt'>
  <td width=3D254 valign=3Dtop style=3D'width:190.65pt;border-top:none;bord=
er-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><span style=3D'font-size:11.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:Arial'>3.4</span></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:30;height:17.0pt'>
  <td width=3D254 valign=3Dtop style=3D'width:190.65pt;border-top:none;bord=
er-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:31;height:17.0pt'>
  <td width=3D254 valign=3Dtop style=3D'width:190.65pt;border-top:none;bord=
er-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><span style=3D'font-size:11.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:Arial'>3.5</span></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:32;height:17.0pt'>
  <td width=3D254 valign=3Dtop style=3D'width:190.65pt;border-top:none;bord=
er-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:33;height:17.0pt'>
  <td width=3D103 rowspan=3D10 valign=3Dtop style=3D'width:76.95pt;border:s=
olid black 1.0pt;
  border-top:none;mso-border-top-alt:solid black .5pt;mso-border-alt:solid =
black .5pt;
  padding:0in 5.75pt 0in 5.75pt;height:17.0pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><span style=3D'font-size:11.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:Arial'>LO4</span></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  </td>
  <td width=3D254 valign=3Dtop style=3D'width:190.65pt;border-top:none;bord=
er-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><span style=3D'font-size:11.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:Arial'>4.1</span></p>
  </td>
  <td width=3D44 rowspan=3D10 valign=3Dtop style=3D'width:33.0pt;border-top=
:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black=
 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  </td>
  <td width=3D57 rowspan=3D10 valign=3Dtop style=3D'width:42.75pt;border-to=
p:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black=
 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  </td>
  <td width=3D192 rowspan=3D10 valign=3Dtop style=3D'width:2.0in;border-top=
:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black=
 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:34;height:17.0pt'>
  <td width=3D254 valign=3Dtop style=3D'width:190.65pt;border-top:none;bord=
er-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:35;height:17.0pt'>
  <td width=3D254 valign=3Dtop style=3D'width:190.65pt;border-top:none;bord=
er-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><span style=3D'font-size:11.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:Arial'>4.2</span></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:36;height:17.0pt'>
  <td width=3D254 valign=3Dtop style=3D'width:190.65pt;border-top:none;bord=
er-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:37;height:17.0pt'>
  <td width=3D254 valign=3Dtop style=3D'width:190.65pt;border-top:none;bord=
er-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><span style=3D'font-size:11.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:Arial'>4.3</span></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:38;height:17.0pt'>
  <td width=3D254 valign=3Dtop style=3D'width:190.65pt;border-top:none;bord=
er-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:39;height:17.0pt'>
  <td width=3D254 valign=3Dtop style=3D'width:190.65pt;border-top:none;bord=
er-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><span style=3D'font-size:11.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:Arial'>4.4</span></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:40;height:17.0pt'>
  <td width=3D254 valign=3Dtop style=3D'width:190.65pt;border-top:none;bord=
er-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:41;height:17.0pt'>
  <td width=3D254 valign=3Dtop style=3D'width:190.65pt;border-top:none;bord=
er-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><span style=3D'font-size:11.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:Arial'>4.5</span></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:42;height:17.0pt'>
  <td width=3D254 valign=3Dtop style=3D'width:190.65pt;border-top:none;bord=
er-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:43;height:17.0pt'>
  <td width=3D103 rowspan=3D10 valign=3Dtop style=3D'width:76.95pt;border:s=
olid black 1.0pt;
  border-top:none;mso-border-top-alt:solid black .5pt;mso-border-alt:solid =
black .5pt;
  padding:0in 5.75pt 0in 5.75pt;height:17.0pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><span style=3D'font-size:11.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:Arial'>LO5</span></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  </td>
  <td width=3D254 valign=3Dtop style=3D'width:190.65pt;border-top:none;bord=
er-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><span style=3D'font-size:11.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:Arial'>5.1</span></p>
  </td>
  <td width=3D44 rowspan=3D10 valign=3Dtop style=3D'width:33.0pt;border-top=
:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black=
 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  </td>
  <td width=3D57 rowspan=3D10 valign=3Dtop style=3D'width:42.75pt;border-to=
p:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black=
 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  </td>
  <td width=3D192 rowspan=3D10 valign=3Dtop style=3D'width:2.0in;border-top=
:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black=
 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:44;height:17.0pt'>
  <td width=3D254 valign=3Dtop style=3D'width:190.65pt;border-top:none;bord=
er-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:45;height:17.0pt'>
  <td width=3D254 valign=3Dtop style=3D'width:190.65pt;border-top:none;bord=
er-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><span style=3D'font-size:11.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:Arial'>5.2</span></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:46;height:17.0pt'>
  <td width=3D254 valign=3Dtop style=3D'width:190.65pt;border-top:none;bord=
er-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:47;height:17.0pt'>
  <td width=3D254 valign=3Dtop style=3D'width:190.65pt;border-top:none;bord=
er-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><span style=3D'font-size:11.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:Arial'>5.3</span></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:48;height:17.0pt'>
  <td width=3D254 valign=3Dtop style=3D'width:190.65pt;border-top:none;bord=
er-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:49;height:17.0pt'>
  <td width=3D254 valign=3Dtop style=3D'width:190.65pt;border-top:none;bord=
er-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><span style=3D'font-size:11.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:Arial'>5.4</span></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:50;height:17.0pt'>
  <td width=3D254 valign=3Dtop style=3D'width:190.65pt;border-top:none;bord=
er-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:51;height:17.0pt'>
  <td width=3D254 valign=3Dtop style=3D'width:190.65pt;border-top:none;bord=
er-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><span style=3D'font-size:11.0pt;font-family:"Arial","sans-serif";
  mso-fareast-font-family:Arial'>5.5</span></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:52;height:17.0pt'>
  <td width=3D254 valign=3Dtop style=3D'width:190.65pt;border-top:none;bord=
er-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:17.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:53;mso-yfti-lastrow:yes'>
  <td width=3D650 colspan=3D5 valign=3Dtop style=3D'width:487.35pt;border:s=
olid black 1.0pt;
  border-top:none;mso-border-top-alt:solid black .5pt;mso-border-alt:solid =
black .5pt;
  padding:0in 5.75pt 0in 5.75pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><span style=3D'font-family:"Arial","sans-serif";mso-fareast-font-=
family:
  Arial'>Actions needed to achieve LOs and meet the standards<span
  style=3D'mso-spacerun:yes'>&nbsp; </span></span></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
</table>

<p class=3DMsoNormal><o:p>&nbsp;</o:p></p>

<table class=3DMsoNormalTable border=3D1 cellspacing=3D0 cellpadding=3D0 wi=
dth=3D601
 style=3D'margin-left:-5.75pt;border-collapse:collapse;mso-table-layout-alt=
:fixed;
 border:none;mso-border-alt:solid black .5pt;mso-yfti-tbllook:1024;mso-padd=
ing-alt:
 0in 5.75pt 0in 5.75pt;mso-border-insideh:.5pt solid black;mso-border-insid=
ev:
 .5pt solid black'>
 <tr style=3D'mso-yfti-irow:0;mso-yfti-firstrow:yes;height:21.0pt'>
  <td width=3D147 valign=3Dtop style=3D'width:110.05pt;border:solid black 1=
.0pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt;height:21.0=
pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><span style=3D'font-family:"Arial","sans-serif";mso-fareast-font-=
family:
  Arial'>Assessor:</span></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  </td>
  <td width=3D454 valign=3Dtop style=3D'width:340.75pt;border:solid black 1=
.0pt;
  border-left:none;mso-border-left-alt:solid black .5pt;mso-border-alt:soli=
d black .5pt;
  padding:0in 5.75pt 0in 5.75pt;height:21.0pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><span style=3D'color:red'>{{Lecture name</span></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:1'>
  <td width=3D147 valign=3Dtop style=3D'width:110.05pt;border:solid black 1=
.0pt;
  border-top:none;mso-border-top-alt:solid black .5pt;mso-border-alt:solid =
black .5pt;
  padding:0in 5.75pt 0in 5.75pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><span style=3D'font-family:"Arial","sans-serif";mso-fareast-font-=
family:
  Arial'>Signature:</span></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  </td>
  <td width=3D454 valign=3Dtop style=3D'width:340.75pt;border-top:none;bord=
er-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style=3D'mso-yfti-irow:2;mso-yfti-lastrow:yes'>
  <td width=3D147 valign=3Dtop style=3D'width:110.05pt;border:solid black 1=
.0pt;
  border-top:none;mso-border-top-alt:solid black .5pt;mso-border-alt:solid =
black .5pt;
  padding:0in 5.75pt 0in 5.75pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><span style=3D'font-family:"Arial","sans-serif";mso-fareast-font-=
family:
  Arial'>Date: </span></p>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><o:p>&nbsp;</o:p></p>
  </td>
  <td width=3D454 valign=3Dtop style=3D'width:340.75pt;border-top:none;bord=
er-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  mso-border-top-alt:solid black .5pt;mso-border-left-alt:solid black .5pt;
  mso-border-alt:solid black .5pt;padding:0in 5.75pt 0in 5.75pt'>
  <p class=3DMsoNormal style=3D'margin-bottom:0in;margin-bottom:.0001pt;lin=
e-height:
  normal'><span style=3D'color:red'>{{date Teacher mark</span></p>
  </td>
 </tr>
</table>

<p class=3DMsoNormal><o:p>&nbsp;</o:p></p>

<p class=3DMsoNormal><o:p>&nbsp;</o:p></p>

<p class=3DMsoNormal><span style=3D'font-size:11.0pt;line-height:115%;font-=
family:
"Arial","sans-serif";mso-fareast-font-family:Arial'>A =3D Achieved </span><=
/p>

<p class=3DMsoNormal><span style=3D'font-size:11.0pt;line-height:115%;font-=
family:
"Arial","sans-serif";mso-fareast-font-family:Arial'>NYA =3D Not Yet Achieve=
d (
when choose preferal in 1<sup>st</sup> page)</span></p>

<p class=3DMsoNormal><o:p>&nbsp;</o:p></p>

</div>

</body>

</html>

------=_NextPart_01D29220.9F7B4370
Content-Location: file:///C:/D13228D2/page2_files/themedata.thmx
Content-Transfer-Encoding: base64
Content-Type: application/vnd.ms-officetheme

UEsDBBQABgAIAAAAIQDp3g+//wAAABwCAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbKyRy07DMBBF
90j8g+UtSpyyQAgl6YLHjseifMDImSQWydiyp1X790zSVEKoIBZsLNkz954743K9Hwe1w5icp0qv
8kIrJOsbR12l3zdP2a1WiYEaGDxhpQ+Y9Lq+vCg3h4BJiZpSpXvmcGdMsj2OkHIfkKTS+jgCyzV2
JoD9gA7NdVHcGOuJkTjjyUPX5QO2sB1YPe7l+Zgk4pC0uj82TqxKQwiDs8CS1Oyo+UbJFkIuyrkn
9S6kK4mhzVnCVPkZsOheZTXRNajeIPILjBLDsAyJX89nIBkt5r87nons29ZZbLzdjrKOfDZezE7B
/xRg9T/oE9PMf1t/AgAA//8DAFBLAwQUAAYACAAAACEApdan58AAAAA2AQAACwAAAF9yZWxzLy5y
ZWxzhI/PasMwDIfvhb2D0X1R0sMYJXYvpZBDL6N9AOEof2giG9sb69tPxwYKuwiEpO/3qT3+rov5
4ZTnIBaaqgbD4kM/y2jhdj2/f4LJhaSnJQhbeHCGo3vbtV+8UNGjPM0xG6VItjCVEg+I2U+8Uq5C
ZNHJENJKRds0YiR/p5FxX9cfmJ4Z4DZM0/UWUtc3YK6PqMn/s8MwzJ5PwX+vLOVFBG43lExp5GKh
qC/jU72QqGWq1B7Qtbj51v0BAAD//wMAUEsDBBQABgAIAAAAIQBreZYWgwAAAIoAAAAcAAAAdGhl
bWUvdGhlbWUvdGhlbWVNYW5hZ2VyLnhtbAzMTQrDIBBA4X2hd5DZN2O7KEVissuuu/YAQ5waQceg
0p/b1+XjgzfO3xTVm0sNWSycBw2KZc0uiLfwfCynG6jaSBzFLGzhxxXm6XgYybSNE99JyHNRfSPV
kIWttd0g1rUr1SHvLN1euSRqPYtHV+jT9yniResrJgoCOP0BAAD//wMAUEsDBBQABgAIAAAAIQAw
3UMpqAYAAKQbAAAWAAAAdGhlbWUvdGhlbWUvdGhlbWUxLnhtbOxZT2/bNhS/D9h3IHRvYyd2Ggd1
itixmy1NG8Ruhx5piZbYUKJA0kl9G9rjgAHDumGHFdhth2FbgRbYpfs02TpsHdCvsEdSksVYXpI2
2IqtPiQS+eP7/x4fqavX7scMHRIhKU/aXv1yzUMk8XlAk7Dt3R72L615SCqcBJjxhLS9KZHetY33
37uK11VEYoJgfSLXcduLlErXl5akD8NYXuYpSWBuzEWMFbyKcCkQ+AjoxmxpuVZbXYoxTTyU4BjI
3hqPqU/QUJP0NnLiPQaviZJ6wGdioEkTZ4XBBgd1jZBT2WUCHWLW9oBPwI+G5L7yEMNSwUTbq5mf
t7RxdQmvZ4uYWrC2tK5vftm6bEFwsGx4inBUMK33G60rWwV9A2BqHtfr9bq9ekHPALDvg6ZWljLN
Rn+t3slplkD2cZ52t9asNVx8if7KnMytTqfTbGWyWKIGZB8bc/i12mpjc9nBG5DFN+fwjc5mt7vq
4A3I4lfn8P0rrdWGizegiNHkYA6tHdrvZ9QLyJiz7Ur4GsDXahl8hoJoKKJLsxjzRC2KtRjf46IP
AA1kWNEEqWlKxtiHKO7ieCQo1gzwOsGlGTvky7khzQtJX9BUtb0PUwwZMaP36vn3r54/RccPnh0/
+On44cPjBz9aQs6qbZyE5VUvv/3sz8cfoz+efvPy0RfVeFnG//rDJ7/8/Hk1ENJnJs6LL5/89uzJ
i68+/f27RxXwTYFHZfiQxkSim+QI7fMYFDNWcSUnI3G+FcMI0/KKzSSUOMGaSwX9nooc9M0pZpl3
HDk6xLXgHQHlowp4fXLPEXgQiYmiFZx3otgB7nLOOlxUWmFH8yqZeThJwmrmYlLG7WN8WMW7ixPH
v71JCnUzD0tH8W5EHDH3GE4UDklCFNJz/ICQCu3uUurYdZf6gks+VuguRR1MK00ypCMnmmaLtmkM
fplW6Qz+dmyzewd1OKvSeoscukjICswqhB8S5pjxOp4oHFeRHOKYlQ1+A6uoSsjBVPhlXE8q8HRI
GEe9gEhZteaWAH1LTt/BULEq3b7LprGLFIoeVNG8gTkvI7f4QTfCcVqFHdAkKmM/kAcQohjtcVUF
3+Vuhuh38ANOFrr7DiWOu0+vBrdp6Ig0CxA9MxEVvrxOuBO/gykbY2JKDRR1p1bHNPm7ws0oVG7L
4eIKN5TKF18/rpD7bS3Zm7B7VeXM9olCvQh3sjx3uQjo21+dt/Ak2SOQEPNb1Lvi/K44e//54rwo
ny++JM+qMBRo3YvYRtu03fHCrntMGRuoKSM3pGm8Jew9QR8G9Tpz4iTFKSyN4FFnMjBwcKHAZg0S
XH1EVTSIcApNe93TREKZkQ4lSrmEw6IZrqSt8dD4K3vUbOpDiK0cEqtdHtjhFT2cnzUKMkaq0Bxo
c0YrmsBZma1cyYiCbq/DrK6FOjO3uhHNFEWHW6GyNrE5lIPJC9VgsLAmNDUIWiGw8iqc+TVrOOxg
RgJtd+uj3C3GCxfpIhnhgGQ+0nrP+6hunJTHypwiWg8bDPrgeIrVStxamuwbcDuLk8rsGgvY5d57
Ey/lETzzElA7mY4sKScnS9BR22s1l5se8nHa9sZwTobHOAWvS91HYhbCZZOvhA37U5PZZPnMm61c
MTcJ6nD1Ye0+p7BTB1Ih1RaWkQ0NM5WFAEs0Jyv/chPMelEKVFSjs0mxsgbB8K9JAXZ0XUvGY+Kr
srNLI9p29jUrpXyiiBhEwREasYnYx+B+HaqgT0AlXHeYiqBf4G5OW9tMucU5S7ryjZjB2XHM0ghn
5VanaJ7JFm4KUiGDeSuJB7pVym6UO78qJuUvSJVyGP/PVNH7Cdw+rATaAz5cDQuMdKa0PS5UxKEK
pRH1+wIaB1M7IFrgfhemIajggtr8F+RQ/7c5Z2mYtIZDpNqnIRIU9iMVCUL2oCyZ6DuFWD3buyxJ
lhEyEVUSV6ZW7BE5JGyoa+Cq3ts9FEGom2qSlQGDOxl/7nuWQaNQNznlfHMqWbH32hz4pzsfm8yg
lFuHTUOT278QsWgPZruqXW+W53tvWRE9MWuzGnlWALPSVtDK0v41RTjnVmsr1pzGy81cOPDivMYw
WDREKdwhIf0H9j8qfGa/dugNdcj3obYi+HihiUHYQFRfso0H0gXSDo6gcbKDNpg0KWvarHXSVss3
6wvudAu+J4ytJTuLv89p7KI5c9k5uXiRxs4s7Njaji00NXj2ZIrC0Dg/yBjHmM9k5S9ZfHQPHL0F
3wwmTEkTTPCdSmDooQcmDyD5LUezdOMvAAAA//8DAFBLAwQUAAYACAAAACEADdGQn7YAAAAbAQAA
JwAAAHRoZW1lL3RoZW1lL19yZWxzL3RoZW1lTWFuYWdlci54bWwucmVsc4SPTQrCMBSE94J3CG9v
07oQkSbdiNCt1AOE5DUNNj8kUeztDa4sCC6HYb6ZabuXnckTYzLeMWiqGgg66ZVxmsFtuOyOQFIW
TonZO2SwYIKObzftFWeRSyhNJiRSKC4xmHIOJ0qTnNCKVPmArjijj1bkIqOmQci70Ej3dX2g8ZsB
fMUkvWIQe9UAGZZQmv+z/TgaiWcvHxZd/lFBc9mFBSiixszgI5uqTATKW7q6xN8AAAD//wMAUEsB
Ai0AFAAGAAgAAAAhAOneD7//AAAAHAIAABMAAAAAAAAAAAAAAAAAAAAAAFtDb250ZW50X1R5cGVz
XS54bWxQSwECLQAUAAYACAAAACEApdan58AAAAA2AQAACwAAAAAAAAAAAAAAAAAwAQAAX3JlbHMv
LnJlbHNQSwECLQAUAAYACAAAACEAa3mWFoMAAACKAAAAHAAAAAAAAAAAAAAAAAAZAgAAdGhlbWUv
dGhlbWUvdGhlbWVNYW5hZ2VyLnhtbFBLAQItABQABgAIAAAAIQAw3UMpqAYAAKQbAAAWAAAAAAAA
AAAAAAAAANYCAAB0aGVtZS90aGVtZS90aGVtZTEueG1sUEsBAi0AFAAGAAgAAAAhAA3RkJ+2AAAA
GwEAACcAAAAAAAAAAAAAAAAAsgkAAHRoZW1lL3RoZW1lL19yZWxzL3RoZW1lTWFuYWdlci54bWwu
cmVsc1BLBQYAAAAABQAFAF0BAACtCgAAAAA=

------=_NextPart_01D29220.9F7B4370
Content-Location: file:///C:/D13228D2/page2_files/colorschememapping.xml
Content-Transfer-Encoding: quoted-printable
Content-Type: text/xml

<?xml version=3D"1.0" encoding=3D"UTF-8" standalone=3D"yes"?>
<a:clrMap xmlns:a=3D"http://schemas.openxmlformats.org/drawingml/2006/main"=
 bg1=3D"lt1" tx1=3D"dk1" bg2=3D"lt2" tx2=3D"dk2" accent1=3D"accent1" accent=
2=3D"accent2" accent3=3D"accent3" accent4=3D"accent4" accent5=3D"accent5" a=
ccent6=3D"accent6" hlink=3D"hlink" folHlink=3D"folHlink"/>
------=_NextPart_01D29220.9F7B4370
Content-Location: file:///C:/D13228D2/page2_files/image001.png
Content-Transfer-Encoding: base64
Content-Type: image/png

iVBORw0KGgoAAAANSUhEUgAAAZUAAACiCAYAAACJU1CaAAAAAXNSR0IArs4c6QAAAARnQU1BAACx
jwv8YQUAAAAJcEhZcwAAIdUAACHVAQSctJ0AAD3HSURBVHhe7Z0JfJTF+ceJWltbe9vDHmpb+69S
q1WqooIxuwlSgWR3k3hULa22tNqiIqIgyW5W61W11qOiIuRctFirtVVrsUULHkDuXa6IpnKF3JvN
BQGS/+/ZzPvm3d15j91sQoLP9/N5PoGd55mZd95555mZd2beCQzDMAzDMAzDMAzDMAzDMAzDMAzD
MAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzD
MAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzD
MAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzD
MAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMMxHm9zcI8+8
/v4Tp3t8Uxz3PTfTce9zrqz7nnXMuPPZ9PPmP37a6VfN/5TQZBiGGbsMDAwcccldPne6p+z3ZmJz
F8+HScqg5fCweUpvl6URLWmLlrqEiSn2/MIZsjiiJaOgbNEEj+cIYWbI1MVPfSvdK49HFYSnenzH
CRPL2G4v+rrdXTLP7i5+zV5Q1pru9R1MLygbkEg/wnrtBaVVaXlFD2V4yy7AjUvKfRinpDQ3N3+t
o6NjRnt7+7z9+/ff29/f/zjkCchDbW1t+T09PVd3dXWdGQgEjhY2DGNKeXn5J1GvzkXduSYYDHpQ
nx4W9eqxffv23YXf5kKm7d69+0vChIlm9erVR8343TMNkoYsRuye0lqYWGqMzUAjukGWRox4Sp4V
JqbY84uflsYRJWjIWyfnzDtGmBliyyt0yOKIEK9v/9m/ufuLwsQUu8f3bVx/KWz3xsRlTfpxDVUZ
3lInugUJO5ddu3Z9Eg3y/XhgHpDJwYMHC6jTIdQPNSl9fX2T4CweOnDgwGbk7yDyZgquIYjG4CXY
/bShoWHURnvI36+1ZRklv0fWPidUE6a7u/trkrhV6ezsnCdUEwLlfYMsXq2g/jzQ0tLy+9bW1rvR
2LqRp3lU1vjd3tvb+53CwsJPiOjGLDt37vwi8nr93r17V+GaOgdrjjHQwyXur8b13gW774moRhzU
5e9ryz9akKcfC9Vhgeu6Xha/Inv27Pm2UI3l0DmV0udlaUQLGs+1wsQUm7vk77I4YgS9/ilz77LU
00hzF10njSNSmnM9K817xBgd4brnwqF0SeJIRPoR19+mLFqWUK9p+/btX0Cj2yeekxgQ1kD1Q6gf
EpCNI/GguCDrUZn7B3OWGHBGLbimu9ATtdwBSBRk9XWRrBRczxNCNWEwIjtDRCcFTqVCqCYEGrC3
RVQJg3LoQaNbC0ezFPFdWl9fP2xnmiwaGxu/A8f5FPLYJbKbELA/iGt7Ddd4AaId0RkElOUskawU
quM0ihfqCQOn8oaIUgo6pFOFaiyHzKnkF/9RloZEtkHd0o2yuYvXS+wl4utPW/C4pd4FdO+UxzEk
cHwBqBqWS2qq5yg4lCUy++FKRsGKOvs9Pv2egw5j3amgYp+Lh+RdkZ2kQQ8eGpM5K1euPFIklXTM
nArCD6DRtwn1hBgPTiUaXHcnHGox8vZ9kcyoU1dX9xnU7fshvSJbSYGcC+rWStTbr4ukko6ZUyGQ
h7/gz7Cc27h0Kun5JbfI0pBIMGfeg5amqpC/7RL7WPH6BtJue/pCYWYI4lwujUMjtvyiVUJdh4EU
e4HvIZlt0sTre8+2aOlXRIKWGKtOBb3ZT6BBuwcPqW7ekgEevlfQuH1ZJJtUzJwKgfLdit7yscIk
bsajU1FA+eyDc3loONefCBhNTMV9f19kY0TAfW1Go+wQSSYVK04FZduPe5crTBJiXDoVW17h5bI0
osVeUHaAXmgLM31SUzESKOuRxSGTae6yy4SlIXZ32Wsye62Q4xHqUpCvy9ILSvtltsmUDO+Kl3Pj
6H2PRaeChvAreOgNK3QyQVr1eAZPF8knDStOhUAZ3y9M4mY8OxUFlH8tyur/RJIjBpJK6e3tnWtU
35MJrukAHEAe/pmU9lLBilMhcJ07Q6FQ3IuHFMalU5k6/7ELZWnECEYVU+Y/PkmY6ZLqefK4DOhK
45CJp/QmYWpEit1TUiu114q7+E6hH8OUhb7P4xoslW8yBCOiK0XSpow1p9LQ0HASGplNIvlRA9fZ
2tfXN1lkIylYdSrQ66PVRsIsLg4Hp0Kg/Hc3NTWdKZJNOh6P5wiU8z0iuVEF6T6KP0mbZrXqVAjU
6SKYJDQNNi6dypSFf/g2Glu9JbQRYs8rninMdLHnlf0gHqdiLyg17SFOmjPnY3Z38R6ZvVbgeOYI
kxjsXl+BzGbExFu21dKiATCWnEpXV9fx+/fv3yySHnXw8LdCzhLZGTaIy5JTIeBIa2jKT5ha5nBx
KgTKaxfkuyLpZJKCenynSOaQgPQfJMcm8jMs4nEqKE8aLV0iTONiXDqVibmeY9O9KywtqbUtXv4r
YabLRfnLL5bZ6oq3rEyY6jJpzm2fTS/wmU+peYsvFiYR0LJlu6dst9RmJMVTZmlvz1hxKsjHMUhr
jUjWEtBvgvhQ+W9C4zpry5YtUyAXtLa2Xoxh/6/RS1uK8P/hwbK8Ygz623t6er4psjUskKxlp0LA
sXiEqWUOtVPBNR7UyLBW5hEo/+rm5uZPi+STAq7h55Q/kYQp0N2He7G2t7f3LtSln+zYsSPtvffe
Ox9O/yL8/3L8fgfy+V/SEyamUNmgPt4osjQs4nEqBJLe1t7eHveKu3HpVAiMFiylm5a33CtMdEl3
F18ts9UTjED+I0x1OfdX95xEK8Vk9qpgdGTzlE4UJhHYvGU/ltoYibesHiOfJ2zuktvS84sX4t9P
YbTzoVRXR+wFZc+JLBgyVpwKRih/EEmagjzVoOG/HJX6k8JcF9r42NHR8WM0Em/Qgy2iMIR08WfY
GyaRXFxOBde1Fw3gD4S5JQ61UwkGg9PR2J6ydevWU7dt23ZaZWXl2fgtA43YtbC9H2X5Fq4rrhVW
sHtMJD9sqDxxHyztO0E+aVXa/bTMWJgbQvs0oP97shNRGIJ87EW9nSLMEyZep0KgHB4V5pYZz05l
nSydaIEDWCpMdLlo8bJbZbZ6YneX+mFmeC2ptz4+GU5Faq+Kt2zfpCs80hdiKK/HpTYSQVkcSC8o
XZw6O3az2OR5D2LEE55GszRdCEfXmuox33Q2FpwKen5peOAOiCR1gU4PensL6urqPi5MLQPzI/FA
/xRxtA7GZgzSWShMEwZpxeVUCDTC75SXl39MRGHKoXYqaOBMX7D/73//Ox7lSS/ILa24QrntT0bj
i6g+hvJcNxirMdB7Hc7wZGEaF2SHa/u3iMoQpFOH6xvWBtxEnAryR2WaKqKwxLh1Kmj8npOlEy1w
AC8LE12QtwdktnpC70om5noMe6S0Y11mqxWk23Ty9Lmyhi4F+a6W2ciE8i/s9EhBeT0is40V38CF
+cvPFna6HGqnQiMJPGjVIjldkI+mUChkaQm4EXggT0V674lodUF6nWgs4t73oyURp0Igj7eIKEwZ
D05FgU4zQLneDdkvzHXBPXp7uHuI0Fm5TkSnC+4RbYv/43CP8kFURyMeOs7FdDSMMv2dMEuIRJwK
gXLfGM/y7XHrVNLcRVb3blQLE11siwt9Ejt98ZZ1nzH7IcO5Rlzv9VJbjeiNeCbNnPNJOIFumU2s
lAbtC4tNd3mn3lL4Vcq3PI5IsblLrhVmuhxqp4IH7KciKV2QhxZUcNPVf1ZBQ3wCGq1tInpd0EgY
LhM3I1GnArtONByWXliPJ6eigB7zVbhGw/cR1Dh3dXUlfNwINZ6oNztEdLrgHj+QrBfoiO4Img4b
jFkfXFoI1/ZVYRY3iToVAvm7W0Rjyvh1KvlFVjdA7qGVWMJMSrq7dJXEzkj6U2986CRhLiXdU3q3
xC5C9DY+2j3LaHWb1CZaMGp6RZiZAn1L12nLLzStQIfSqVC8iN8vkpKCB7APD2BCq1eMQGN7GqIP
DqYiB3nrqa+vN6wfRiDvCTkVoq+v73UrPfXx6FQI2M0jxyGikYIy+CdUE1oOa2WUgvv7QrLrNsWH
DstKkYQutAhAmMTNcJwKinwvnMWPRFSGjFunYnUDZHqBr3fKwsc/L8yk2D0lAbmtjtAL9tuLzhHm
UnC9JVJbjdg8xdIerc1bOlWmLxO713evMDPFVlD2oCyOaIFTMe1pH0qnQtNZIhldaEpBqCcdNJjX
iGR0QQORJ9TjZjhOhUDDY7ricbw6FZgeibL952AscqheouE6QZhYhkYeiLtCRCMF92YPiOv0CavQ
uXLI+4ciKSkI3xXPuzMtw3EqBNIut7J8fdw6lam3PWF1A+TBtBsfMjqrKyW9oLRZamsgNk+J4VEK
6e4S01EB4rhDqEeAUU6mTF8mdPy9MDMl7fali2RxRIs9v+gFYaLLoXQqaLCeEMlIQcPQRKfHCvWk
gySo8XlnMDU56C1vTHRu38ypINxwmSvKvg1/DBvV8epUCIxAzzCqewTK/3qhbhlcM634Mlz4gXh/
I9RHBJTbT0RSuuD6pwn1uDBzKmb1isD13y6i02X8OpX5j3wLDsPS8SVGZ3XR6ijEs19mZyS4HqNK
m0LXK7PTSkZBmfTdhc3ry5Xpy8QkHxGkLV5uacoww1tmurjhUDkVaqgxCjHszcUz/5soeAB/LJKT
gvD9O3bsSGhDHmwNnQoaniehYzgFBKf3Iv7oTgGNZ6cCUnB9r4qopKDx+5vQtQwa61uFuRTU6Ub8
MV2OPhzoxT/SMTwZAiPRPwr1uDBzKj09Pa+hXD8Q/5WCvHWjbhge6DlunQptDoQz6JOlFS2pC5+6
QpjFcO6ND51k1Tlpxe4t012JkerxHGV3lzbK7FTx+gbSFhdJexxpntJLpTYSGQmnkl7gM31Pc6ic
Sl1d3XeMGlQEHWhpaTlVqI8YHtxjPIDbRbJSaCmyUI8LXIOhUwmFQqloNP8u/iuFygjp/0REGcM4
dyoTUPZXiaikoHx2xTtSRJ4NyxThSdsHYwSc20KRpBR0mjYI1biw4FSeg86PUXXMRsK02Vj32R63
ToVAvDuj05EJ0r5ZmMSQ7imdLLMxE8T5tIgihkm5cz4Lp9Ers1OF3sssWibd+MhORR9UWJdIQgoe
/LrhLim1Ch7uJSJZKehRJvRex8yp4OGfCjkBZdwufpKC8D2NjY3S1ULj3am8//77J6CcdOsfNYzr
1q2zvFIKJkegvAw7CR0dHbOE+ojS2tp6mlHDjrCeRJYyW3Eq+ENH0xQO/qIPnPYNItoYxrVTsRWU
vCNLK0bcJQ8Kkxguun15ltTGRNLyCnUb3tTbltDoR2qniL2gbO/pV82XHp3OTkUf9FANe3Go7M8I
1REHDeeVIlkpCLe8Mk+LFadCeiiLX4ufdMF98OFPzDTYeHcqVLfg1M2cwHlC3ZQtW7Ych7LaK0xj
oEZ+pF7QR0Mv4pGXPSLpGJCX/qamprinVq04FdKjE4qR/i7xsxSEh9Bp+lY44ijGtVOxu8usbYD0
lK0QJjGk5RfNkdmYCa6nHObSOWv7wqfPM3UqnpKm6XOlGx/ZqRiAhuQxkYQUNGYFQnXE2b1799ki
WSl4SKuEalxYdSr4Jy0Y+M/gr3KoMURjkhmOWMN4dyoE6oLhmW9II0eomoJ7eSo11sI0BtTnZqgl
tEw5EXBthuUHpxLXLnfCqlMh4BhyjMqDQN17DX9iymScO5USSxsg7d6yNcIkBru7OF9mYyY2d/F2
+g6LiCaCaV5ftsxGKygP+uKjtJKyU9EHI5EykYQUVPTrhOqIEwgEThDJSkFe3ktkgxyeZUtOhaAe
K8ra8JO2CP8wGAx+QZiEORycCsqXvlKoC3rSuieAR/P++++fI8ykoN5tEaqjAsqPFlro8sEHH8R0
FMyIx6ngvzQNZrpvBvm8RpiojGunEkcj+QF9412YRWDLW275jK0I8fo6Tr/qfulZPLaC0hulNhqx
uUt0v/jITkUfPBjPiiSkoEGOqeQjxaOPPvo1kawU9Dbr6YW+ULdMPE6FQOM6XwTpgvvxJFTVTszh
4FTQ0JeK6KSgXHTn/aN57733zhNmUtBQUvs1aqDuGNbz+vp6p1C1TDxOhcA1fw11sUkES0G9am1p
aYn4EOK4dirWN0CWBU+/ar7UASDsL1G61sTrO3jWTX84XkQTAUY/90ltImWZUI9hpJyKLX/51IyC
krvMZcVsYaLLIZz+KhZJSBnNkQp6iyeKZKWgDOpyc3PjXjQQr1PBT3TCgOH3+BF+AD33dGFyWDgV
xLFCRCcFdWGuUDVl27ZtPxJmUnA9H6mRigJ+m436aDYN9jz+qB2W8e1UrO489/r6Js9bIv2ssN1T
slZqYya0JDhv2Rkimgjs7rJSqU2k6H4DY6ScSjI5VE4FD9tDIgkp6L3G/W2RRNmxY8e5IlkpeLhM
z52TEa9TIeAkToed4VHxePi3Kt8cOUxGKi+L6KTAiZqeYafQ2Nh4MsrPaMVVE9RG7Z0Kys+wk9DQ
0HCRULVMIk4FP9N7u38MashB2fRDR/2u/bh2KmnzHzsxw7tCml6k+AYuvPlh6SdH071l78ttzCXD
+4z04Dqbu/h1mX6k+H4u1GNgp6IPKu88kYQUNDS6izKSDfJytUhWChqGV4VqXOAZjdupELh2t1DR
BSO98ErIw8SpGJ5SjTQMT73QUl9f/znU2W5hGgM5HDTk0tWayUZsgKSNlrp0dHTEXX6JOBWCVnnh
+umUBl2Q393gS6Q/rp3KRI/naBqFyNKLlqm3PjFDmKnQ1IS9wNcl07ciOqf5ptjdZmeJ+QamLnxC
96gFdir60CGRIgkpaGi2juI+ladEslLQsD4iVOMiUadC5zKh3KuEmhTEvQ/25493p4K6dSzK33Cf
DupKPJ94phfTdcJUCuIz/TR5MkA6Z+A+GW3w7U3k20CJOhUCjuU3Qk0XmpqGasq4dioERho7ZOnF
iKf0F8JEZdIcz3FwSgek+hbE5il1i6hUUlM9R+Fam2T6Q+LrnzJ/iXTjI8FORZ+qqqqv46HSPZ8J
YftDoZBu2SYL2kuAh2inSFYKGlbTd1MycA0JORUCPdhzYW94PDzuTXVLS8tk8V8pY92pBIPBs3Gd
Rg1v35o1awwPko0GefqzMJeCcv+TUB1RkI/FIkkpqHcJ3ZvhOBV6ltFhWy1UpaBeHUTeLhn3TsWW
X2xpA6TNXRQz135hXvGpCIv7iBZFEGdMJTt5+pWfgdMwHj15fb1T5j4cHirKYKeiD6JOQcXdPJiK
HIT/XqiPGEjD8AFFo7Z/586dCTWcsE3YqRBo0E2/zYEGwnA57lh3KiijO0RUUpD+FvyJ6x0IGsPr
B63lIM2meD5WlQg0AsGzs1UkKQVll9AIeDhOhYD9KSgDw08gI+8fYFRTKf4rZcw7Fbu72NoGSK/v
KWGicuGCP6XJdK2KPa/wryIqlYzFJXTQpVRfERrJnDh7tu4R0uxUjEHc94lkpCC8FY3iiO1+RhJ0
/Pr6wdTkoNHenOg03HCdivjQlKHjRRqGK3rGslOprq7+FJy62aGiusco6UFf7ESx6NZpAvnWPfIp
GeDezhZJ6dLV1TVdqMfFcJ0KgXq/QKgnzDhwKtY2QGL0ELP3Ij2OxlsmdnfpWyIqlYz8sgtkulpB
WehufCTYqRiDRvOHePgND71Do7IMf0ZktQ56YqYfcsLDl/AqtOE6FQJ5pO/3m35+V4+x7FTgsG8U
0eiCUUeWUI8L3DezEwrawIlCPamgzL+M+A2nVOm5SvQTxslwKpQ2ysjS9/v1GPNOJS2vcIEsvWix
eUoo/QguWvS06SZFI4FD2zZhQuQ+BHtB2WUyXa1A5zWhLoWdijGIno4+f3MwJTnkdPCQJHRKsBHo
JZ6JazObAuhtaGiQnotkhWQ4FQKO9U/CJG7GqlOBszwZ5Wu4Egl1o+W1116T7kszA/m+VESjC9J/
A/foGGGSFBAtnfdluDeFQP7uESZxkwynQuAZoE6d4fJ1I8b+SMVCIy6kYWKuJ8LDwyncJdFTxZZf
vFn2u0baps99OGIVht3ru1miFyHI81KhLoWdijnBYHA6KrbhFA6Ce+jdhzAZNoiPjt03nHYhcO2F
wiQhkEZSnEp7ezstkzX8PoYeY9GpoDGigw4NV7cR0EnoeyMEzGkBhuGnqgV0hInpVxCtQNOkKC/D
M+0IXFcnGnTphmsrJMupEMhLgTCLmzHvVDK8z5pONw1Kac+U6yI/K4w8LZfrhqXf5i5+WPK7KnZv
2YHJ8x6MOFMJNvfLdLUCZxWzakwLOxVzkARtyjL8rCyBfNC3tek032FNhfX19U2y4lCg00lz88Is
IZLlVAg0kDMQn+HXDGWMNaeC/Hwf11EjzHWh+43RzLDKH2WWibQMOywE0nqNpqyEWUIgmk8gveVW
0oOe7jecrJBMpwJ1Wr5uej9kjHmnMnXx8m9mmLwYD4vXd/DcX90RcVw0RgwvS3VJvGX7Um996pfS
MEWQ7oV5SyM+CGXLL/JJdbXiLtHd+EiwU7FGKBSik2UNp6IIemDhFJ6Fc5GeqmAELR2G7Q1W0iHw
4N4mTBMGaSXNqUCdVssZHm0jYyw4Fao/uGc/gu6jKBPdjYlacK2PCvOEQTQpuOd09IgpqOcfkhPC
P+PutNB+FNhbej8Bva3DXXmWTKdCdHR0TMZ9ifu93Zh3KidPn/txWqIrSzNaUhcvmyLMwtgLfFUy
PRLkt+2CG/44RRYWId5n7CK6MHAq/5bqacS2aGmGUJdyODgVVLZWPDQXo9eYbkWgf7KIOi7w8Bsu
A9WC/HYgrQfFbmTDRoBWGGEkdBVsDHdta4Hu6kRfompJplMh4Hy/hLwZfh8jmpF2KrgP9yCN+Yqg
rizACG8xnMi9+H8R7Fcjz40oC9MevAL0d9IUmcjCsEAevkLxiagNQRYPIr//QaOdabYpEeopkB/i
HhbCTvf7LVpID/rniygSJtlOhUAZPSDMLTPmnQpuE30PfrsszWixu4svE0Zh0j2le2R6JHZPSd15
199/IhyW4eZIW17RVSK6MGn5hZtkehrpv+CmPxh+4/lwcCrxgrjuFVHHBUyPQE/R9Et1WvCQHoBN
DR6yJ+GUbsLDdAXkUjR016JxuAN//wGHYrhbOxrk/8Po01oTBflLqlMhcD25iNdyA41GdUSdSrLB
pe3H/ZQem5QoqBsXIN64XkijHjSh/ryAvHhQBj/HvcqF/AT/nw9ZhvuwOZ77QLqwScozjvST7lSQ
vU9BDPfVRDMOnAq9xyh5W5ZmtNg8pTcJkwl0ajEcRo9Mj8SeX/xm6vWPHYt/d0aHacXuLl0goqRz
X45M965olukpYi8o22u08ZFgpxIfqNTHwP5fIqpRB+k3oxE+XWRn2CC+pDsV+q4LGifDDY9axpNT
QXn1o8Eceg6TCMr6MkSf8NLs4YJyvBd/krI0fiScCgEnakMZGS7x1zI+nEpekdXj69XPCp8z995v
oIHXHYVgVPNM+Gwwb9mHsnBF0vKK1DjPv+a+T5uNbNILfIYbHwl2KvEjNvwZNsYjAR6mRjz48Zwx
ZQriTLpTIWgkhbibRTSGjBenIhozWok0InuSCJT3T5FO0uq6FZBeP8rwvkQ+8qbHSDkVAnl9QkRj
yrhwKmnuIotfgPT9WZhMmLrg0bPwm+4RLRj9hJ0FHM+7snBFbHmF6qm4qQuXnmy2aMBeEN74aAg7
lcTAc3jM/v37y+iBFNGOKDSV0dHR8T2RfNJA9kfEqRB4+H9mpXzGg1NBnenp6+v7FZIbMYeiQC/j
kV5QJD2i4Pbso2lZ/DOp1zWSTgXmn0X5mK6OJMaFU0n3lFj6oqE9v1jdAQ8HMF2mo4jdUxw+jsHu
LnleFq4InMS/wxECjFJSZTqRUmr6VUV2KomDqI7Aw3MjHkzDT+wOB2qU0cj48M/PimSTCqIfMacC
c5oGe2UwJn3GulPBNVSh4T1bJDcqoF5NRLoVIgsjAuL/H00niSSTykg6FQLxz0TdNZ0GGxdOhV7A
y9KMFuRhG733IJvU25/+mUxHFTTspGfzlDwiDReCtDdCLdyjgMO4QqYTJfRZV0PYqQwfWm6MB/RV
K5U8HhBfHR6euD/lGg8j6VQINFrfRnkb9rrHolNBufQj35u6urp+if9+TCQ1qiALH8f9X4S/cS3k
MAPx9aLMHgkGg3GdrBwPI+1UEAUtxTb8xDMxPpxKfvF59I0SWbpasbtL2yfN8XySbDK8K26V6Sgy
zfNMeAlf2uLChbJwVby+plTxHfK0/CLTEVNaXnE+6RrBTiU5INqU9vZ2O+L+Ox5aw+PgjYA9NWZ+
PDBz8N+k7KI2AnkdUadCdHd3/1ZEJ+VQOhVcP5X3AUg3OgYf4u+riO93bW1tU6nNEUkcUuDYvkp5
Qt52i2wnBOw7EM+SpqamhI6tiYeRdioE6g2dX2ZYJuPCqaTe+tQ3LG2ALPD1nXn5gq+RjclBlAem
3PR4eFfuRYuXz5aED4nXtz/1ek94U5I9v+QBqY5G0txFhhsfiZFyKqkLl82EA37GTNLzixcKE11o
P0Zra2suelaXJkPQYCRt9ZQMOJeT0EOfi4eYlgs34a/uih5q06DTBSnHA39vY2Pj+aP14S8Ceb1I
VkaKKF/YGw64zKOQjksWP8mePXsM91KZ0dzcbJfFKxPkIxtObhbuz7SNGzdOra+v/yEanhPWrl1L
nz4e8fclwwGdq2M6Ojpm7N+//3HUl82QXnKKgzUpFtS7PujugKxAI34Vdc5EVCMO7snXZOWvCNVz
oToscD/PlMWvSF1dnX79HStO5fiZMz+Z7i3rlqUbIXA8tkVLw41Xurf0WanOoPScM9fzGdJLXVyc
LglXhZxZ2i1PfId0bXnLV8h0tGLLX276sI6UU0lbvNzSuyfZic6HE3AQR2/ZsuVb1PNtaWnJ2blz
58/xcF+Lxvoq9ECnI+z0VatWjcj7EubwBT4jpby8/Dg4xLPI0aA+XU31qqGh4WeoZy5qtNGgfsMj
ZjYYCWPFqUzweI6w5RcZLv0NCxxA6uLC8LcI7AWlb0p1IHZv2a4JqanhG3/BTY9932wUZPOWhodz
dk/Jf2ThGuk/74Y/nka6RrBTYRjmI8mYcSoYIlveAFlQFv6uvM1TUicLJ7HnFdaEYwWpHt9xNG0m
01PEtrgwl3Tt7uItsnBFrGx8JNipMAzzkWQMOZUJNre1DZB2r69g0qQ5H0v3lrXKwkns7tKXRbQT
Tp778Meha7xL3l08F6pH4N/t0WERgniij9+XwU6FYZiPJGPJqVj9AqS9oGTpxJx5X6BRgyxcyDIR
LZGC/2+MCo8QOJW7zpn78GfSvb6DsnBF7AXhMjCFnQrDMB9JxpJTQRrzo9OUic1d+mrqwsdOxr8N
jmgp8Ypow9g9Ja/L9BSx5RUuT11UeAq9s5GFK2L3lP1DRGkIOxWGYT6SjCWnknHHihxZutGC0ULV
lFuWGH/Yy+P7hYg2TLq7tFiqJ4Smy9LzC+2ysAjxlj0hojSEnQrDMB9JxpJTuWjhsnPNRgokdnfx
7rRFTxk22nbPivAKMYW0xYX3yvQUsblLKu3uwitlYVpJyys03fhIsFNJDnX/nf6lUGXmxA/XZtLe
pDG932E0We1JPar+nayTWiocp9a9Mj28dP4jwBG7ymce11w+85Tm9Znf2/72NNofkmidSPlwzYzP
71yb+b3mctcpu1bPpG+4HDb1a+XKiUfveHf6N/bW5UxsemfGd+tXZ30OP4/O9Y0lp3LO3Ie/YcWp
0H6WtMXLjHbJ96fe8tQPRbRhLsovvEGip5XdF93+9G2S3yMk7fanrxFRGnKonUqGd4W6UMGIQCD3
6M5a58pQlfOfJPh3zB4cevg6a12vhMOrnSs9Yqm2lrZK50+VONors+gYm3AF3vbGJS7ld12pcf6z
+pXpZ5K+QrffeXZHtfON7lrXvq5a10C337W/s8ZZFQo4cxCsPhw9tdlXSeOMksZqR8TH3dqrHHcp
YcFqxx/Ez1JaKi45Fdf/algfeW3a4Iw50bhlg2OaEl+n3/ViYGWuupijY5Pj/8hOTa8i+0ciKAZt
PKFa50svPTkpfHqEgscz4YhQrePXXX7n+yiTAyibfvzt6qhxPr93c07EwZjBSudTalw60lHj+usA
6gDpd9a47pDpaCVY5XzxwXmTjwknAFAu8yN1HK8GK7OeR31YEqx1/mo3OgVCNWF6q3O+FaxyPIAy
3Ip6QfWhX1z33q4a58aOase97dWzviXUDQmiLncFsm9DPa+Efa8aV61zH65/U0eN446Gaqfh54Xb
Njh+q1xve7XjMfGzlNZy5+8UXZSJbod0YGBCCu7FE4qunrRWZBVRHRBmEQzg9y6/Y2aw0vECrqkZ
dYTqx4Aoq15cmx/P1H29fkd4T56MXW/NPEVbV/WkszYnvFpWylhyKuL7KCFZ2hHi9fXTOxBpGIm3
bH/qLY99VUQbxpZXZDa11mf2PXsSKxsfCZt3hVNmLxO7p+QGYWbKRYuXGR5Nowju1UvCxIyUjirn
f0XlG0CFivmUKxqSXCW8qwYPod95hghSaa90/E3Raa9yqqc+178x82bVVkdQ4QdqXpmufn0TjXJa
FxpKmS4e/H40Cg8px4n3+p0FMr1oaakJOyOFlFC1c5smvDdUqd/49W6MTKO1IvPXIkilpSLz50p4
d8DV9fafhxrevs3ZP6JrVMP9ror+uunSrwu2V2X9TNHrCrh6Xik9Rx2FDCDf3RuzH0AYNYRqfIog
3mAw4FRH6GhANsr0oqRj+9s54bz21Lr+LgmPEHQqOu9b8D3aJR8GjXyhTG9InK1d/uyZQj0uBjs8
2b9DfeuRx60VZ0+n33lP/epU3SN4ENelPX5Xg9x+SNCRaQ7WZF8hzGJor3D8SdFFXS0XP0tpr3C+
oujiOXpe/BwDORXo1Cq6etJW6Xhf5lR6/FnfRPjrEGndiJLeTn+2t7x8UszZaw3vZJ6rrau64s/R
//bNWHIqdFCkpQ2QEHtBqVGeg8dPmhnRw7MvfPo8ow2Q4V31i5etl4UNia8/7danDL/4qECfKJbH
IRWPMDOFVqlJ7GPEtrjQJ0xMCdY47lcqS6jasR4/RQyT0Utcqq1QoVrXIhEUhnrl+H2HEt4dyP6N
CIrbqawuTP0EepHvRYZnR+iT9GzMvoX0E3EqSONzaITCIyBV/M6Id3AKK1fmHonwiMa5o8rxiAhW
icepkIT8rjwRHIGRU8G9mYJ8K73PcLn1xJSNs6dDjFjGhlOBoIPQXJMZ1+cFAitTj0UH5l/RcdE1
U32Q1Ymw+J1v4npijk1BWS5E+MFofb24UAcP9mzMuRWmMVNGY82p4Hk8BflQnz9Fhsoq8ndFegKu
F+vrI53w4edUgNUNkEZid5fUi+hUUm9bchLCdL+9YkVoCXPqzU9a+n52Wl7R9y1N5UGQX8sOAPdg
pSyOGPGUGk7paAlVuRxDlcXVVfVCKs2/hqEKjIaqXluh0Ki+KYLDdFS6vgs75YHt3/lWpjqVpXUq
GB304uFeEi2o3Esq/pV+KukHNzjO0VTqfppeqV99yVfxgOfi9z2Dvzv/Q1NypI+eexZGV8tIaLpM
SQt2B5HeCvq9E9KsmXLqDuSco+gpEqpxvSqCI2h713mGZhpBEfVTCQrxOhWUV0/D+hkxJzMYOpVa
xx+UMMTXiPt2QeCl9BM6al33CmdzMOR33k4NFOm3Vzt/p5QN5EPFNlTj2K38jnL6kzJVp3Uq4ZFc
1H0alOw/emafqDZEWqeChnhnz0bXYyLudcrvJBgNLxEmpoTrnN/1nNYe0ov7+Hh7RZa9rcJ1Ik13
0VQP0ilF2F6tbke1a76IKkxPIPsKXOdQDx51NVTt+hvym7v7v5kTd7+bObEHoxjE/7JWjxxLp2TE
MhpOBWlXysqf7qlyf4lg7YzPQz+iE4bRfAvydW97udO2c53j/3asmXEG7ttslNU/Ea46VlxvQzCQ
c7KIKkyMU/G7ymT56N102TRhEstYcypoDK1+AVJf3KXqN1cUBvegGH9W2FS8vsZJc+ZYOq479UbP
52gaThpPlNCXKU+c7TE9OZdOZ7YX+BplcUSLzV10ozAzpbki82uadxcYiThSRdCE1mrnafg9YkgN
ne7AP8MvScO01Thma8JbqksyPiWCIpxKa6WjWftAyGhcN2u6Uqnxt6+laob6zfjWmqwfoMFYqjd1
tOPdrKuVtGC7r+rPNun35ttrHL9U9DT6PaHKnJgpsPD0S5QuGp7dq1dHvleK26mE9bLXlj8ZOQVh
5FQwYixWwoKVztU0HSaC4EAcN3f4XVeL/8bQWu54SbFtKc96Q/wcgdapNK/PWil+NiRqpLJa/DzY
SPrDDf5gWI2TPi9hiU6/83LYaJyA832MkHSPRurc5DwN96SadNFwPjig+dIiLfRA2beocdW6urs3
uhwiOAY0xpdBv3tI39naWTvrKyI4zGg4lZZyx30iyBA0+k8oNiQYmayS1WOFoD97Gp713SivXa1V
2TGzLlFOpb921YzwobxxMeZGKuhhy9KOR9Dzjzn+OfxZ4YLS7TL9OEQ9+sUczxF2d2m9JA6pZBSs
mC0MdbG5S38ls5WJ1Xc/RG7uhCNDVc6tSsUM1rgWiyB6gKTTVyG/K1uoTEAv9HH1d4wixM9h4nUq
7e9SD9TVp9gEq50vD5RHTmXqYdmpVGX9UdHTCq4pfPyPgseTehS9vI3Wg9M4+ObzFx8v1MIk4lTC
un5XhPM3cipoCDT3wnmwd2POdSLIlNF2KkTThlnXKmHUiImfDaF3ZRg1hh2EkPbGwKyI3rQMlNvn
aAozun4hf9rp0X5yGiJIF3RcrkZ+NSOW7N+JoDBjxal88NYlJ6L+qM8KZN0uC88KjU5CW1yniP9G
cEicysTc3KPp2yPDEaNvNmOkcpMs7XjEXlD2kIguAltB6QaZvlWxuYstbXxUsDxVNShBm7csTZjG
kHFn6bT0AguLGEi8vr2T5y2N6zhuDKvVXiUczN/FzxNCNQ4aMotK5uxQ/k3vWSicHobOaleF8nt3
rfPOsKFA61Q6qp3t3Ruzz5VJ+ap05UThFDyof1VswnEGXG/QUlIRrotVp9JW4Vil6HXWOIemA2pd
Ecuw0VD9kBrvQT00SLRIQOg2vDPjIqEWJh6nEqp2/k/5N6SzccPQahwjp9K4Lver1HPW2B6EE88f
GDD/Bnq8TqWt0vF639acc6LvU9DvPJs6IcJEz6mkNK/N/HSP3/WqEkbTOSLMkJbKzIlKmZOEql2m
n3DQg6bRcM/UzhKcVcy0pQzRwNNLbyXvdfRuTQSPilOBbkl02ZO0B3LVVa3ozM1T9FFmB4IVmbqr
Cq0S7VRaqjKdsnx0b748/PkRKfE4FQhN5+wZrqDHrfaEo7F7fdnQkaUdh6yImFNVsOUX/k2ub01s
ecWmX3zUQgdfyuLRFW/Zfhpl2fKKfpHheWYanMPF6e7iX9rdxS8gXPf0gGjBCGkNkjccEUQTqs3+
tVJB8fDtDqycePTmFzM/jQrWqVSwtvJMetk5qFPtrKcHjfYKYGRBSzOpIafRyI9FlGHifVFP0JQX
fot4j9MTyN7UUe0w/AiSFafiwUgbjcRORQ/X4etWbZw92uWknX7n3YoeGqe34YDU9xK4zohRglWn
0h3IHmgqd9L7IaVcKa//UhyDkVMhkPdcOL/9qs6g/fKB+tmG06fxOhU9MXpRj/LpoHJC56EKeVI7
IOEwlKUwMaS9xqVeP+I+ULd6+jdEUNxsevXi45GPoVFvrZO+hW+J9mrHNUP5cB2guETQqDgVPWnX
vKjvqnE+q/yOOlwVjmiYjPqL+mSJzV1yj0g+hqm3LznH6gtuPbG7l0uHuGl5RUtk+lbFnl8iXbGj
B51mDLue6HhGXnyW36co7Hhn1umoLOGeOCpoP60oaa3MukSpRGgw3i9/KfU4/HvwpajfebCtYtbp
e9ZnpsOpCB3X3g1R00KJOBWC0sfvEY4FDXIjetDq+55orDiV+nWXfBVhaqPcsDYzDQ21uswUjiu8
D4k2F+J61KkvNOTz0ci9rPw/FLX0Oh6nQmUdqsm6VdWnMDH1ZuZUiH2bcn6hnSIMxxFw/ie0RX80
NxpORU9QHttC5VdYWuDSWD5rkWKHRrhZO0KIl11vzjxLKXeSni2uC0SQKc3o9av3zJ898OGaGZNE
0CF1KtrVX3gm1yi/45ktDkcUBXVWaJk1vYfUE+3S4sPSqfzouju/CadieKijmaQtfvpCEV0E6PHn
y/StitWNj1rSPWVlsrhGUIJWV6hpoUYQlalRqTTt1a5rg9WOhzQVKbzJK1jtXKvqVDkWtFU48pT/
o5JvUiq8gtap0Dw1GuO90YJGfW/5yxk2YaKCBvYkNMLa+XVqfLu7NmZHjIYUrDiV5mqnTaOz/6+P
nv3Frtqhd0JoQMKbRts3OM/C/8U0jLOvveqykzCq+b2qF/X+IF6nQiuuUB7vauJr7a7K+boVp0K0
V2RlI1/qaCcsfpcfjb504168TgX35WD0fSLpqHK2xuFU9mOE+VKrP+ubQt0UbX3Cvxui61M8tGxw
qKv8yDE0VzhUx2DGvk2X0gIV1bZ+9fTJImhUnArK+kB02ZNghLxZ41TUZxH/1h6gqxKsdl2Mutdm
JHvezVou1GOcCur8Plk+OmtzbhYmsYw1p3L6/Ps/BR1r7w5kAod04YIl3xXRRWBzF8Y3HRUldk9x
uojKMml3FH3fXlC6TxbfSIi9oEy3bM3oqHaq7xrE3hR1n0Pz+qxZpIMh9+3Kb6hwq+B4/qH8H5Ux
pmJrnUpbpbPlSc+k4x5dePYXtfLAzZOOe3JO7EYsIrxkEg2TEodIJxi9FJKw4lRaKjNvUHRwvR/i
p5SGt2bZlAcJDqG74S3nl3Fd9yp69PDSQ9+4bpba4OPB2qNdARavU6HfuwK5P8Rv4alDEqTzQnvV
UDxGToVoqc4+F3rbVX0IRnivy3r38TqVxnWZL0bfJ5K7F57yRaiqU6uR01+uepSLOuqD03yHyk2o
WqK5PPM3ij3KrHfVk+q7trjZumbGtxHP0PuZWv1VX9G0VTtmqvcMTmXHOzPUNmU0nErj+qyHZeV/
z8IfhJfSE+hU/EXR7/I7I5b5K7SVO2aqOjrStC5LHeVEv1N581nbWbJ8aE9ViGGsOZXU1NSjMKL4
n8zOouz7wYzr1ILXYveUTJfoW5V+m2fZRBFVXNgLfPdL4hsJeT/9tpUJP4RoIIZWytS4munlH/2b
Hm7aMEg6TRtmnkXTY/Q7GsEeNCTtio0ydaRF61SsrP6SEd5cGXCpDzIJGohnRLCKFadCR8goOh01
jvDelFcenv5xXNNu5ff2SucvcG2blf93+p03kd4H/72EGvHBKcKA68C/VqSrLysTcSoEGiyvYgeh
xQCqkzZzKsTuCteJNELRxDHQvTE7SwSrxOtUEl39hVHnXM3/+4PVWRcLVUuEalwXauyl12KVhx8+
+eOow01KXLRKUQSZgrr8mJqPGlcbOkPqqqrRcCpWVn+FKoc6ePSMBlbNPEEEqZBToboXIcJGEe3x
MdFOZcRXfyVLjJwKYfMMZwNkaTPtzBdRRZDhKTkt4fc1Xl+vfeGj1EuLm0meJz9p9/rekcabPOlW
PomcKC0bnBnRFW5QhpYJP/nkpI9pX3Sr4ncd3LY6djNfok6l7pXIvSg0N4wGV10Vhnw2z5kUObqx
4lTgSNQpg+6AU31w4UTUKTBcbx3SGlzpVePcR5vtSGfdX+1fxHWqixJ2rM1UV+sl6lRovhsNc41i
GyE6TuWVqLJpqXF+g8pDsWupGFyZp2W0nApdD/4OHYETcK2P570ILYlFWamOAPdh/UC5+d4wqlc9
gdyYRjVU6yxR4uqudXYGNSvt9Ni70fVd6IcUO4yU/yKCwowVp0KbZ6GrrkjEc1kS/Xy1led+trvW
MUkrXQFnvmJD9bKxMut8oX74OhXoPBdtY1XsntJNIpoYpixa9qV0b1lCU1H2grJG5Zv3iXDx7aXH
0we+ZHEPX3y9F9/x50tFUgnz1vMZX0aDGnvOUo0j4qVcVEMyKH5XIx2xIlRU4nUqNKWEofxP0TBv
CkYdfNeqOYMMFX/vNZlDc/uEmVOhuNH4qhvh8BBeKYImNLw7NAWmlc5a1xpNnunBVxvMtnKnehxN
ok6FgFOYjPKLePEeliinsnOd44voyT8C+Uf05svW8qznFbvd72S+KH5WGS2nQr/RJkzNbwOd1eZ7
Q7TAkdP5Zqp9lz/7SSPHUleHkaY/+0GUcXNzpfNs8XOYdoysEYe6MKMnkF1N765EcAxNG5y0YqxK
TZuWbddkR3TWxopTIdAZUheP0DtLjOy8Rk68f3vO11FW6pJ22Fdqn8nD1qnYvaUPyOysCBr/10Q0
MZzo8XwCDbvuJ4iNBM6qWkSTMNMX/flLdCS9LP7Exbc7zVOqf2RCfKSENBWbJPyS8o2hY1eIYJXj
Uq0OCSq09Kh9rVOBXUdHlXNGjNQ6Z5Svnnnc9rcnH9MTGDoUrwcPNxr78Omzogc8dHSH37VHu1+C
MHMqe9YPzbHTg7Nz9SXqmn8aGeEa1CkwReAgfitUwsARvagJ/5P4eVhOhegdPChSiXdQNE4lGHCc
g2vepYb5XQ8rZ3b1+i/7jjbvbZVZMUeixOtUgpWO/8ruVWuFczptChUmUqcinLe6wIKmEpWTkK1A
e5JwPdFnWa2hlYbaESz9G870Yui+o+ihnPe0VUYeeIo69bAmHiq7XS0bsq4Vx8GHaVydeyw6PVcg
LHLFoT+7MLojFOFUcG1bX59xjlbKX5qmbiyMcCqVjv9E61a9evFJpBftVPCsPCcr/4ZyJy1oUfMT
qsyZiDxrD1/tR55fbsXoY0DT8aCyaqvOmoVwtVOEunww6HdFTE9GOxWMeq+R5SOIOidMYhmTTsVT
YnZMvZFIV0EIUuB0tkhsTCXejY96pHpWH4Xrm4M4d0anEZcMjriW0QhIRJ0Uumoij31Ahd0Z3Suu
fg0jGjp2XKMXqpIfkKh1KnpClZiWFNOxI5212ep0xaA4O4OVtHRyaI8ICXqIMffZzKk0rJulvrRE
eO/T950fMdLBw6KZAgvL3sCq9IgplfaKrKG9K7XO/4qfh+1UGqozPoXGoE6JIywap0INLTWGEeG1
ru1UNmjAh6ZqSKIaCiJep6InJkuK1RVxHYMNmGoXqnbEnOxsBDoUF8Au8rrCDaarCWEb8LdcTPmp
0z9h8btaokcWtIcHIxR1EYoiiKOzu9YZwN9a2AVjwgPZa/s3Z0bUEULrVMJ6uLdaad2QpXZstU5F
qluRGe6YRDsVPZEdKNkZcP0EjjVi71J4+nawvtAZbOuQFq3sjCgrPGu/F1GoRDkVXekxWlK8cmDg
yEvuXPE2GvqtoyVp+UXhE2b1CL9Qd5dKbc2EduSLaKSkLV7+jMzOTNJuX3aXiCIpnL/gvk/TajQ4
uX/DOVjby0JLrb1lW+3usnvS8p6O69RXq9ARFdrKg4azUARpwQMwtJyRZOfbM6Qr4+JxKqTfH8j5
Ahpl+TsGIdQrp/cI4QQ0mDkVPOC3KeGhaucW8bPKnnVZdu0DhWunFTURvdS2CsdVSniw2tmkTDUM
16kQ7Rsy06CjrlaKnv7q3ehKl05PasXvei66Z02MtlOhhg//V/dRQLZvXhvbQBvRszn7fNzriNVt
RoIyrgwFcqSLaWgk0rPRtVJmJxW/68X2qqGRjJZopxItzeszdZ1KtLRsGL5TIUJ+hwtOVnvagoE4
D3b5nQ8MSKbJrDoVw30qAqqEoylWkNlZESvI7MxkxJg878EvXLjgibQ0d9F1cIp32wtKl9g9pcvt
BSVL4Uj+YMtbflta3nLX+bc88Z3c3JWWX3omwgf/vvh7XbWOns4ax16SpndnSb8rEap1LFR0uvyO
4JbBL+fF8MEbP75R0dMTpLe3WrNPhfZaUOMIiT4duB8N9lsdNZEfo1LYvi7zSjwQe0l6N2aHNr84
tDqLCNW4livhiCdm9RhNEdB8s6JDH8MSQSpNNc6z4BhEGq6eN0unhkeKeNhnK3Y9G7PbIp1K7iT0
lJWwvdvfzvqBCIqhK+B8XI1nU0579Iv68OqoQOx5ZCgr3IfsR7XpakH+/qrE21rpeF38HEFvIPsF
RUdPMJKN2KeChnqpGh7I/pf4OUxbhevCnoCrVwnfG4g8PdgKQepk+LPvxv3agzgirxmC36j3XQ/n
c7MyHagHLfYIVmVfCl3a8T/kvIeENv36g1XOKwcG9N9LdFQ6HlGuSSYt5ZnqB/LoNGSZjiKt5Znh
zyiQU+nxZ1fIdLSCvG3R27tDpw+gHjwCaYFu9LVRWR1A2Nu0d0V7GKkW2tuj1FUj2bvpMv19Kgyj
hSp3sHbh54MfLvz8AMlqj3RxwkDAczTpkLTX3yjt0RH1q2d/QtHTE0qnPOpFLOVj0MHl/LzLf/mt
3YFLf7X9jelnG72ERNjRa3xTPq9I9MO3emXqsUpY+UuRX1RUWPvi+Z9WdKKn/Qj6up4STqLkh5Yl
q3aDnw5QH1rqEcpsZNCpxVpd2aiDlljvfuuSC7sDl/22a+MVC9rKnVe+9+Y0ww2G2mtf+3TktJ+C
VkdPoq/ttZKMTylhsngjbBG/+DluyFnuWDN9cldt9jVUH7o2XnYLGsirN75iP5M+nSvULEHl7/9H
+qnBStdPOgOXz+/yX7GARuibXss4zejeKNDXOLXXFS0vasrBrExf0tTDd9GBkOloZdVK8307qx9L
PXbXmulTMJq4lsqqM3DZzcHqrMsC/5x2sp5DUqCTJGTpRotsUQ7DMAzDMAzDMAzDMAzDMAzDMAzD
MAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzD
MAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzD
MAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAzDMAwjmDDh/wGl
+c/XHOhEsgAAAABJRU5ErkJggk==

------=_NextPart_01D29220.9F7B4370
Content-Location: file:///C:/D13228D2/page2_files/image002.gif
Content-Transfer-Encoding: base64
Content-Type: image/gif

R0lGODlhsQBHAHcAMSH+GlNvZnR3YXJlOiBNaWNyb3NvZnQgT2ZmaWNlACH5BAEAAAAALAEAFgCv
ACAAhwAAAAAAAAAAMwAAZgAAmQAAzAAA/wAzAAAzMwAzZgAzmQAzzAAz/wBmAABmMwBmZgBmmQBm
zABm/wCZAACZMwCZZgCZmQCZzACZ/wDMAADMMwDMZgDMmQDMzADM/wD/AAD/MwD/ZgD/mQD/zAD/
/zMAADMAMzMAZjMAmTMAzDMA/zMzADMzMzMzZjMzmTMzzDMz/zNmADNmMzNmZjNmmTNmzDNm/zOZ
ADOZMzOZZjOZmTOZzDOZ/zPMADPMMzPMZjPMmTPMzDPM/zP/ADP/MzP/ZjP/mTP/zDP//2YAAGYA
M2YAZmYAmWYAzGYA/2YzAGYzM2YzZmYzmWYzzGYz/2ZmAGZmM2ZmZmZmmWZmzGZm/2aZAGaZM2aZ
ZmaZmWaZzGaZ/2bMAGbMM2bMZmbMmWbMzGbM/2b/AGb/M2b/Zmb/mWb/zGb//5kAAJkAM5kAZpkA
mZkAzJkA/5kzAJkzM5kzZpkzmZkzzJkz/5lmAJlmM5lmZplmmZlmzJlm/5mZAJmZM5mZZpmZmZmZ
zJmZ/5nMAJnMM5nMZpnMmZnMzJnM/5n/AJn/M5n/Zpn/mZn/zJn//8wAAMwAM8wAZswAmcwAzMwA
/8wzAMwzM8wzZswzmcwzzMwz/8xmAMxmM8xmZsxmmcxmzMxm/8yZAMyZM8yZZsyZmcyZzMyZ/8zM
AMzMM8zMZszMmczMzMzM/8z/AMz/M8z/Zsz/mcz/zMz///8AAP8AM/8AZv8Amf8AzP8A//8zAP8z
M/8zZv8zmf8zzP8z//9mAP9mM/9mZv9mmf9mzP9m//+ZAP+ZM/+ZZv+Zmf+ZzP+Z///MAP/MM//M
Zv/Mmf/MzP/M////AP//M///Zv//mf//zP///wECAwECAwECAwECAwECAwECAwECAwECAwECAwEC
AwECAwECAwECAwECAwECAwECAwECAwECAwECAwECAwECAwECAwECAwECAwECAwECAwECAwECAwEC
AwECAwECAwECAwECAwECAwECAwECAwECAwECAwECAwj/AAEIHEiwoMGDCBMqXMiwocOHECNKJEjj
S0UaCC1a9FJQI8aBGyt2vOjxILaTEE+qxOYQpUmXBV1iazVR4EqWC1UmhElDhw6OB2ns0GGRoE+L
H0EKHamDhpeeRJMO1Nnw5k2FVw1SjWkVJU6sXb2C/UpwpcCSBz1+oYh24EWCakm+LQtzbFiyXM3m
fXkXb96+fqduHdhKpxefPRFebGq0p1O2c302fTrZMVDBgf/KzMq3rk3PnwEjtAoAJ2fNfnXKTXjx
MgC5a8/Ofa1W6dPYoXMOpgs69O7duXn3Lq1XeGDSe1/7dE2xsg6BlhO7tfhcuVPp0KknH11cs1bk
mDt//88MnDf37nrbjkQbF+7c9pC3ix87vjvx4ydp/h2v/zz+lf0VhtJhTVV3UIEGAkDdbI4ZSFmC
1hUV3k7lCVffVvYFN96GuqXWFWYsraaYRpDdppSEtInkHokTnsdQhjrpleF9LspXo4V6CYgTDT0x
Z9RPPELn3EDLBWndU1IpiJ2GfFXloVcy9jYjehVamBxV6c1mkIhxxcaliAUlWSWNLz75FZZSlted
jmWClp9vALBJ4JIFIVjdYte55VNjezqZGZP+cXjfaS3aSCZ9+8GpmnoGoXXbdbixmJ2KbdplqaCD
qjkccGMOWp9xLkHlI5FA3ulcdUWaeh2EN3b2J4yDff/4qaGeUkheXbIyCpmEXU7KK5iIJqRjfyDO
NGux/xFb67GJfqcsaXOy2px0q0IlUIFSPdiSjK4ie6uZ//XlH17gWYlsiiimlaeSIRXV2q7bygpq
WLZe6Kq89+LbbLHQ9ajQYkJiZC0ARyUVXZLjVimureHmOyNq5WLqG066rvgRiSySdKKWsmmln7EK
0aRvTMqSjNXHJbf5ZlUpfwxAtP82RR2QKT4nc50Du7UDxzX17PPPQI/Is3uPkvhoimFKKpvSQTft
9NMOibqQdsoJ+ZxjKEYHIdVQd+311+winLRIv/a6NMc8P7NQNaQIVI0qbsOdkNoOVTPQ2wI9IzdB
dh//JDfdAsndd+ADtV0Q3ocT9PfLiEmrZ4PvaV2QttOm+4wzeQvkDN3PdK5259Vg/kzonAMeeuYA
bH635p4D4DnmzoTe9+adGzQ67QRdXvtAgKc+UOyj/+355Xm3DmxQRasIW5LAtqv47GojPrjvz98t
uOrUk6553Ln/vTcAeMde/UBwq4J46n1/P/j54GO+OvVu8UhZv5PzuGrADzKXKqnzF6T66bxTG/ac
4T7XlQ6AACRe5/4Ht9ERZIC9E2Dv2neQAhowdYDbneswuD2DqIJu7uuSxqbzpdqMTYTp4h74PigQ
UrxNentjIQBYaLfowU18NkSf5hCnt7jpznWx+x4H3v03PfOtkHwZDCLqjui6D4ovWpArEKmwZSAq
0sk6VuxTQRaYtwJasItgBCIGYYc61f0wgKwr3gM1+L4tTpBzvPNiBkvnwA2SEIUGgw+6hrY8pv1u
dut7IPeMOMMa4pBwTERgB3vYRL41cnyJ+yMiC2k9vhWQkF/UWoMgdDD+8chxyomKFg0iPjW6rho/
JF3pKKlDDp6ud28b3RkHiEq8CXB6aWwd+iS4w859EJWi0x4GeblL4oGNISn0IPkKp8LzxVCFjMSl
QaRnOB4qzm9uuyb4ePdMOzJxhioE5zYDAgA7

------=_NextPart_01D29220.9F7B4370
Content-Location: file:///C:/D13228D2/page2_files/filelist.xml
Content-Transfer-Encoding: quoted-printable
Content-Type: text/xml; charset="utf-8"

<xml xmlns:o=3D"urn:schemas-microsoft-com:office:office">
 <o:MainFile HRef=3D"../page2.htm"/>
 <o:File HRef=3D"themedata.thmx"/>
 <o:File HRef=3D"colorschememapping.xml"/>
 <o:File HRef=3D"image001.png"/>
 <o:File HRef=3D"image002.gif"/>
 <o:File HRef=3D"filelist.xml"/>
</xml>
------=_NextPart_01D29220.9F7B4370--
