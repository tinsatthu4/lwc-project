<!doctype html>
<html lang="en" ng-app="myPortal">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>{{ $title or "Teacher Portal" }}</title>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="{{ asset('components/AdminLTE/bootstrap/css/bootstrap.min.css') }}">
<!-- Font Awesome -->
<link rel="stylesheet" href="{{ asset('components/font-awesome/css/font-awesome.min.css') }}">
<!-- Ionicons -->
<link rel="stylesheet" href="{{ asset('components/Ionicons/css/ionicons.min.css') }}">
<!-- Theme style -->
<link rel="stylesheet" href="{{ asset('components/AdminLTE/dist/css/AdminLTE.min.css') }}">
<link rel="stylesheet" href="{{ asset('components/AdminLTE/dist/css/skins/_all-skins.min.css') }}">
<link rel="stylesheet" href="{{ asset('components/bootstrap-notify/css/bootstrap-notify.css') }}">
<link rel="stylesheet" href="{{ asset('components/ng-notifications-bar/dist/ngNotificationsBar.min.css') }}">
<link rel="stylesheet" href="{{ asset('components/angular-loading-bar/src/loading-bar.css') }}">
<link rel="stylesheet" href="{{ asset('components/AdminLTE/plugins/datatables/dataTables.bootstrap.css') }}">
</head>
<body class="skin-blue sidebar-mini" ng-app="myPortal">
  <div class="wrapper">
    @include('LwcPortal::user.layout.header-top')
    <notifications-bar class="notifications"></notifications-bar>
    <aside class="main-sidebar">
      <section class="sidebar">
        <ul class="sidebar-menu">
          <li class="header">MAIN NAVIGATION</li>
          <li>
            <a href="javascript:void()" ui-sref="admin">
              <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            </a>
          </li>
          <li class="treeview">
              <a href="javascript:void()">
                <i class="fa fa-inbox"></i> <span>Course</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="javascript:void()" ui-sref="course_create">Add Course</a></li>
                <li><a href="javascript:void()" ui-sref="course_list">List Courses</a></li>
              </ul>
            </li>
          <li class="treeview">
            <a href="javascript:void()">
              <i class="fa fa-inbox"></i> <span>Modules</span> <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <li><a href="javascript:void()" ui-sref="module_create">Add Module</a></li>
              <li><a href="javascript:void()" ui-sref="module_list">List Modules</a></li>
            </ul>
          </li>
          <li class="treeview ">
            <a href="javascript:void()">
              <i class="fa fa-list"></i> <span>Tasks</span> <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu menu-open">
              <li><a href="javascript:void()" ui-sref="task_create">Add Task</a></li>
              <li><a href="javascript:void()" ui-sref="task_list">List Tasks</a></li>
            </ul>
          </li>
          <li class="treeview ">
              <a href="javascript:void()">
                <i class="fa fa-list"></i> <span>Users</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu menu-open">
                <li><a href="javascript:void()" ui-sref="user_create">Add User</a></li>
                <li><a href="javascript:void()" ui-sref="user_list">List Users</a></li>
              </ul>
            </li>
        </ul>
      </section>
    </aside>
    <div class="content-wrapper">
      <section class="content-header"><h1>{{ 'Admin Portal' }}</h1></section>
      <section class="content">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <ui-view></ui-view>
           </div>
        </div>
      </section>
    </div>
  </div>
</body>
<div class='notifications top-right'></div>
<script src="{!! asset('components/AdminLTE/plugins/jQuery/jQuery-2.2.0.min.js') !!}"></script>
<script src="{!! asset('components/jquery-ui/jquery-ui.min.js') !!}"></script>
<script src="{!! asset('components/tinymce/tinymce.min.js') !!}"></script>
<script src="{!! asset('components/AdminLTE/bootstrap/js/bootstrap.min.js') !!}"></script>
<script src="{!! asset('components/AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js') !!}"></script>
<script src="{!! asset("components/angular/angular.min.js") !!}"></script>
<script src="{!! asset("components/angular-ui-router/release/angular-ui-router.min.js") !!}"></script>
<script src="{!! asset("components/angular-ui-tinymce/dist/tinymce.min.js") !!}"></script>
<script src="{!! asset("components/ng-notifications-bar/dist/ngNotificationsBar.min.js") !!}"></script>
<script src="{!! asset('components/angular-loading-bar/build/loading-bar.min.js') !!}"></script>
<script src="{!! vl_asset("portal/src/common.js") !!}"></script>
<script src="{!! asset("portal/src/admin-config.js") !!}"></script>
<script src="{!! vl_asset("portal/src/admin.js") !!}"></script>
<script src="{!! asset("components/AdminLTE/dist/js/app.min.js") !!}"></script>
@yield('js')
</html>