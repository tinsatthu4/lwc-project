<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Portal LWC</title>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="{{ asset('components/AdminLTE/bootstrap/css/bootstrap.min.css') }}">
<!-- Font Awesome -->
<link rel="stylesheet" href="{{ asset('components/font-awesome/css/font-awesome.min.css') }}">
<!-- Ionicons -->
<link rel="stylesheet" href="{{ asset('components/Ionicons/css/ionicons.min.css') }}">
<!-- Theme style -->
<link rel="stylesheet" href="{{ asset('components/AdminLTE/dist/css/AdminLTE.min.css') }}">
<!-- AdminLTE Skins. Choose a skin from the css/skins
   folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="{{ asset('components/AdminLTE/dist/css/skins/_all-skins.min.css') }}">
<link rel="stylesheet" href="{{ asset('components/bootstrap-notify/css/bootstrap-notify.css') }}">
<link rel="stylesheet" href="{{ asset('components/AdminLTE/plugins/iCheck/square/blue.css') }}">
<link rel="stylesheet" href="{{ asset('components/chosen/chosen.css') }}">
<link rel="stylesheet" href="{{ asset('components/jquery-ui/themes/ui-lightness/jquery-ui.min.css') }}">
<link rel="stylesheet" href="{{ asset('components/jquery-ui/themes/ui-lightness/jquery-ui.min.css') }}">
<link rel="stylesheet" href="{{ asset('bk-elearning/style.css') }}">
<link rel="stylesheet" href="{{ asset('lwcportal/css/custom.css') }}">
@yield('css')