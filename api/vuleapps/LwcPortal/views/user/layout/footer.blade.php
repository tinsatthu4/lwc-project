<div class='notifications top-right'></div>
@yield("js_init")
<script src="{!! asset('components/AdminLTE/plugins/jQuery/jQuery-2.2.0.min.js') !!}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{!! asset('components/jquery-ui/jquery-ui.min.js') !!}"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{!! asset('components/AdminLTE/bootstrap/js/bootstrap.min.js') !!}"></script>
<script src="{!! asset('components/bootstrap-notify/js/bootstrap-notify.js') !!}"></script>
<script src="{!! asset('components/chosen/chosen.jquery.js') !!}"></script>
{{--<script src="http://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.1/chosen.jquery.min.js"></script>--}}
<script src="{!! asset('components/tinymce/tinymce.min.js') !!}"></script>
<script src="{!! asset('components/AdminLTE/plugins/iCheck/icheck.min.js') !!}"></script>
<script src="{!! asset('components/AdminLTE/plugins/input-mask/jquery.inputmask.js') !!}"></script>
<script src="{!! asset('components/AdminLTE/plugins/input-mask/jquery.inputmask.date.extensions.js') !!}"></script>
<script src="{!! asset('components/AdminLTE/plugins/input-mask/jquery.inputmask.extensions.js') !!}"></script>
<script>
    var BASE_ASSET = '{!! asset('/') !!}';
</script>
<script src="{!! asset("components/angular/angular.min.js") !!}"></script>
<script src="{!! asset("components/angular-ui-tinymce/dist/tinymce.min.js") !!}"></script>
<script src="{!! asset('components/angular-loading-bar/build/loading-bar.min.js') !!}"></script>
<script src="{!! asset('bk-elearning/angular/config.js') !!}"></script>
<script src="{!! asset("bk-elearning/angular/common.js") !!}"></script>
<script src="{!! vl_asset('bk-elearning/angular/module.js') !!}"></script>
<script src="{!! vl_asset('bk-elearning/app.js') !!}"></script>
<script>
    $(document).ready(function() {
        $('#myCarousel').carousel({
            interval: 6000
        })
    });
</script>
@yield('js')