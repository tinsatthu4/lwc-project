<!doctype html>
<html lang="en" ng-app="myPortal">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>{{ $title or "Teacher Portal" }}</title>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="{{ asset('components/AdminLTE/bootstrap/css/bootstrap.min.css') }}">
<!-- Font Awesome -->
<link rel="stylesheet" href="{{ asset('components/font-awesome/css/font-awesome.min.css') }}">
<!-- Ionicons -->
<link rel="stylesheet" href="{{ asset('components/Ionicons/css/ionicons.min.css') }}">
<!-- Theme style -->
<link rel="stylesheet" href="{{ asset('components/AdminLTE/dist/css/AdminLTE.min.css') }}">
<link rel="stylesheet" href="{{ asset('components/AdminLTE/dist/css/skins/_all-skins.min.css') }}">
<link rel="stylesheet" href="{{ asset('components/bootstrap-notify/css/bootstrap-notify.css') }}">
<link rel="stylesheet" href="{{ asset('components/ng-notifications-bar/dist/ngNotificationsBar.min.css') }}">
{{--<link rel="stylesheet" href="{{ asset('components/angular-loading-bar/src/loading-bar.css') }}">--}}
{{--<link rel="stylesheet" href="{{ asset('components/AdminLTE/plugins/datatables/dataTables.bootstrap.css') }}">--}}
</head>
<body class="skin-blue sidebar-mini" ng-app="myPortal">
  <div class="wrapper">
    @include('LwcPortal::user.layout.header-top')
    <notifications-bar class="notifications"></notifications-bar>
    <div ui-view></div>
    <div id="page-loading" class="hide" style=" position: fixed; z-index: 99999; top: 20px; right: 20px;"><div class="alert alert-success alert-dismissible"> <i class="fa fa-spinner fa-spin fa-1x fa-fw"></i><span class="">Loading...</span></div></div></div>
  </div>
</body>
<div class='notifications top-right'></div>
<script src="{!! asset('components/AdminLTE/plugins/jQuery/jQuery-2.2.0.min.js') !!}"></script>
<script src="{!! asset('components/jquery-ui/jquery-ui.min.js') !!}"></script>
<script src="{!! asset('components/tinymce/tinymce.min.js') !!}"></script>
<script src="{!! asset('components/AdminLTE/bootstrap/js/bootstrap.min.js') !!}"></script>
<script src="{!! asset('components/AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js') !!}"></script>
<script src="{!! asset("components/angular/angular.min.js") !!}"></script>
<script src="{!! asset("components/angular-ui-router/release/angular-ui-router.min.js") !!}"></script>
<script src="{!! asset("components/angular-ui-router/release/stateEvents.min.js") !!}"></script>
<script src="{!! asset("components/angular-ui-tinymce/dist/tinymce.min.js") !!}"></script>
<script src="{!! asset("components/ng-notifications-bar/dist/ngNotificationsBar.min.js") !!}"></script>
<script src="{!! asset('components/angular-loading-bar/build/loading-bar.min.js') !!}"></script>
<script src="{!! vl_asset("portal/src/templates.js") !!}"></script>
<script src="{!! vl_asset("portal/src/common.js") !!}"></script>
<script src="{!! vl_asset("portal/src/common-component.js") !!}"></script>
<script src="{!! asset("portal/src/teacher-config.js") !!}"></script>
<script src="{!! vl_asset("portal/src/teacher.js") !!}"></script>
<script src="{!! asset("components/AdminLTE/dist/js/app.min.js") !!}"></script>
@yield('js')
</html>