<!doctype html>
<html lang="en" ng-app="myPortal">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>{{ $title or "Portal" }}</title>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="{{ asset('components/AdminLTE/bootstrap/css/bootstrap.min.css') }}">
<!-- Font Awesome -->
<link rel="stylesheet" href="{{ asset('components/font-awesome/css/font-awesome.min.css') }}">
<!-- Ionicons -->
<link rel="stylesheet" href="{{ asset('components/Ionicons/css/ionicons.min.css') }}">
<!-- Theme style -->
<link rel="stylesheet" href="{{ asset('components/AdminLTE/dist/css/AdminLTE.min.css') }}">
<link rel="stylesheet" href="{{ asset('components/AdminLTE/dist/css/skins/_all-skins.min.css') }}">
<link rel="stylesheet" href="{{ asset('components/bootstrap-notify/css/bootstrap-notify.css') }}">
<link rel="stylesheet" href="{{ asset('components/ng-notifications-bar/dist/ngNotificationsBar.min.css') }}">
</head>
<body class="skin-blue sidebar-mini" ng-app="myPortal">
  <div class="wrapper">
    @include('LwcPortal::user.layout.header-top')
    <notifications-bar class="notifications"></notifications-bar>
    <aside class="main-sidebar">
      <section class="sidebar">
        <sidebar-users counrse-id="{{ $counrse_id }}"></sidebar-users>
      </section>
    </aside>
    <div class="content-wrapper">
      <section class="content-header"><h1>{{ 'Welcome Portal' }}</h1></section>
      <section class="content" style="min-height: 800px">
        <ui-view></ui-view>
      </section>
    </div>
  </div>
</body>
<div class='notifications top-right'></div>
<script src="{!! asset('components/AdminLTE/plugins/jQuery/jQuery-2.2.0.min.js') !!}"></script>
<script src="{!! asset('components/jquery-ui/jquery-ui.min.js') !!}"></script>
<script src="{!! asset('components/AdminLTE/bootstrap/js/bootstrap.min.js') !!}"></script>
<script src="{!! asset('components/AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js') !!}"></script>
<script src="{!! asset('components/tinymce/tinymce.min.js') !!}"></script>
<script src="{!! asset("components/angular/angular.min.js") !!}"></script>
<script src="{!! asset("components/angular-ui-router/release/angular-ui-router.min.js") !!}"></script>
<script src="{!! asset("components/angular-ui-tinymce/dist/tinymce.min.js") !!}"></script>
<script src="{!! asset("components/ng-notifications-bar/dist/ngNotificationsBar.min.js") !!}"></script>
<script src="{!! asset('components/angular-loading-bar/build/loading-bar.min.js') !!}"></script>
<script src="{!! vl_asset("portal/src/common.js") !!}"></script>
<script src="{!! asset("portal/src/learner-config.js") !!}"></script>
<script src="{!! vl_asset("portal/src/learner.js") !!}"></script>
@yield('js')
</html>