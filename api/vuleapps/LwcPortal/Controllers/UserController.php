<?php
namespace VuleApps\LwcPortal\Controllers;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Illuminate\Http\Request;
use TermRepository;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use VuleApps\LwcPortal\Models\Counrse;
use VuleApps\LwcPortal\Models\UserCounrse;
use Artisan;
use VuleApps\LwcPortal\Repository\PortalRepository;

class UserController extends Controller
{
	/**
	 * @api {GET} user-info User Info
	 * @apiDescription Namespace <strong>VuleApps\LwcPortal\Controllers\UserController@getUserInfo</strong>
	 * @apiGroup User
	 * @apiPermission User is logged
	 */
	function getUserInfo() {
		return response()->json(['data' => Auth::user()]);
	}

    function dashboard()
    {
        return view("LwcPortal::user.dashboard");
    }

    function learnerDashboard()
    {
        $user = Auth::getUser();
        $data = $this->dataLearner($user);
        return view("LwcPortal::user.dashboard_learner")
            ->withUser($user)
            ->withData($data);
    }

    function teacher01Dashboard()
    {
        $user = Auth::getUser();
        return view("LwcPortal::user.dashboard_teacher_01");
    }

	function teacherMasterFileDashboard() {
		return view('LwcPortal::user.dashboard_master_file');
	}

    function adminDashboard()
    {
        $user = Auth::getUser();
        return view("LwcPortal::user.dashboard_admin")
            ->withUser($user);
    }

    function dataLearner($user)
    {
        $data = [];
        $counrses = $user->counrses()->get();
        $data['counrses'] = $counrses;
        $events = TermRepository::getBySlug('event');
        if($events != null) {
            $data['events'] = $events->post()->take(4)->get();
        }
        $announcements = TermRepository::getBySlug('announcements');
        if($announcements != null) {
            $data['announcements'] = $announcements->post()->take(4)->get();
        }
        $news = TermRepository::getBySlug('news');
        if($announcements != null) {
            $data['news'] = $news->post()->take(9)->get();
        }
        return $data;
    }

    function dataTeacher($user)
    {
        $data = [];
        return $data;
    }

	function getCounrsePage(Request $request, $slug, $counrse_id) {
		return view("LwcPortal::user.counrse_page")
				->withCounrseId($counrse_id);
	}

    public function index(Request $request)
    {
        $search = $request->get('s', null);
        $user = new User();
        if($search != null) {
            $user = $user->where('email', 'like', "%{$search}%");
        }
        $user = $user->paginate($request->get('limit', 30));

        return response()->json([
            'data' => $user->items(),
            'links' => (string) $user->render(),
            'total' => $user->total()
        ]);
    }

    public function show($id)
    {
        try {
            return response()->json(['data' => User::findOrFail($id)]);
        } catch(ModelNotFoundException $e) {
            return response(null, 404);
        }
    }

    public function store(Request $request)
    {
        try {
            $this->validate($request, [
                'name'     => 'required',
                'email'    => 'required|email|max:255|unique:users',
                'password' => 'required|confirmed|min:6',
                'type'     => 'required'
            ]);

            $user = new User();
            $user->fill($request->all());
            $user->password = bcrypt($user->password);
            $user->save();

            $data = $request->all();
            if($request->has('course') && is_array($request->course)) {
                foreach ($request->course as $item) {
                    if(isset($item['id']))
                        app(PortalRepository::class)->registerCourseForUser($model->id, $item['id']);
                }
            }

            elseif($user->type == 'teacher_1') {
                $courses = isset($data['course']) ? $data['course'] : [];
                foreach($courses as $course) {
                    Counrse::where('id', $course['id'])
                        ->update(['teacher_id' => $user->id]);
                }
            }

            return response()
                ->json([
                    'data' => $user
                ], 201);
        }
        catch(ValidationException $e) {
            return response()->json([
                'messages' => $e->validator->messages()
            ], 400);
        }
    }

    public function update(Request $request, $user_id)
    {
        try {
            $model = User::findOrFail($user_id);
            $this->validate($request, [
                'name'     => 'required',
                'email'    => 'required|email|max:255',
                'password' => 'confirmed|min:6',
                'type'     => 'required'
            ]);

            $data = $request->all();

            $data['password'] = (!empty($request['password'])) ? bcrypt($request['password']) : $model->password;
            $model->fill($data);
            $model->save();

            if($model->type == 'learner' && isset($data['course'])) {
                if($request->has('course') && is_array($request->course)) {
                    foreach ($request->course as $item) {
                        if(isset($item['id']))
                            app(PortalRepository::class)->registerCourseForUser($model->id, $item['id']);
                    }
                }
//                UserCounrse::where('user_id', $model->id)->delete();
//                $courses = isset($data['course']) ? $data['course'] : [];
//                foreach($courses as $course) {
//                    if(Counrse::find($course['id']) != null) {
//                        UserCounrse::create([
//                            'post_id'   => $course['id'],
//                            'user_id'   => $model->id
//                        ]);
//                    }
//                }
            }
            elseif($model->type == 'teacher_1' && isset($data['course'])) {
                Counrse::where('teacher_id', $model->id)
                    ->update(['teacher_id' => 0]);
                $courses = isset($data['course']) ? $data['course'] : [];
                foreach($courses as $course) {
                    Counrse::where('id', $course['id'])
                        ->update(['teacher_id' => $model->id]);
                }
            }
            return response()
                ->json([
                    'data' => $model
                ]);
        }
        catch (ValidationException $e) {
            return response()->json([
                'messages' => $e->validator->messages()
            ], 400);
        }
        catch (ModelNotFoundException $e) {
            return response(null, 404);
        }
    }

    public function destroy($id)
    {
        try {
            $model = User::findOrFail($id);
            $model->delete();
            return response(null, 204);
        } catch (ModelNotFoundException $e) {
            return response(null, 404);
        }
    }

    public function getCourseByUser(Request $request)
    {
        try {
            $user = User::findOrFail($request->get('user_id'));

            if($user->type == 'learner') {
                $course = $user->counrses()->get();
                return response()->json([
                    'data' => $course->toArray(),
                ]);
            }
            elseif($user->type == 'teacher_1') {
                $course = Counrse::where('teacher_id', $user->id)
                    ->get();
                return response()->json([
                    'data' => $course->toArray(),
                ]);
            }
            return response()->json([
                'data' => [],
            ]);
        } catch (ModelNotFoundException $e) {
            return response(null, 404);
        }
    }

	public function putProfile(Request $request) {
		try {
			$model = Auth::user();
			$this->validate($request, [
				'name' => 'required',
				'password' => 'confirmed|min:6',
			]);
			$input = $request->except('email', 'is_admin', 'type');
			$input['password'] = (!empty($input['password'])) ? bcrypt($input['password']) : $model->password;
			$model->fill($input);
			$model->save();
			return response()
				->json([
					'data' => $model
				]);
		} catch (ValidationException $e) {
			return response()
				->json([
					'messages' => $e->validator->messages()
				], 400);
		}
	}

    public function getAllTeacher() {
        $model = User::select('id', 'name')
            ->where('type', 'teacher_1')
            ->get();
        return response()
            ->json([
                'data' => $model
            ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCoursesByLearner(Request $request, $id) {

        $model = Counrse::whereHas('user', function($query) use ($id) {
            $query->where('user_id', $id);
        });

        if($request->has('select'))
            $model = $model->select($request->select);

        $model = $model->paginate($request->input('limit', 50));

        return response()->json([
            'data' => $model->items(),
            'links' => (string) $model->render(),
            'total' => $model->total()
        ]);
    }
}