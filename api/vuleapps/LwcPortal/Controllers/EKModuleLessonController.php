<?php
namespace VuleApps\LwcPortal\Controllers;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Request;
use VuleApps\LwcBackends\Models\EkLesson;

class EKModuleLessonController extends Controller
{
    public function index(Request $request, $module)
    {
        $lessions = EkLesson::where('module_id', $module)
            ->paginate($request->get('limit', 20));
        return response()->json([
            'data'  => $lessions->items(),
            'links' => (string) $lessions->render(),
            'total' => $lessions->total()
        ], 200);
    }

    public function show($module, $lesson)
    {
        try {
            $model = EkLesson::where([
                'module_id' => $module,
                'id'        => $lesson
              ])
              ->firstOrFail();
            return response()->json([
                'data' => $model
            ]);
        } catch(ModelNotFoundException $e) {
            return response(null, 404);
        }
    }

    public function store(Request $request, $module) {
        try {
            $this->validate($request, [
                'title'      => 'required',
            ]);
            $input = $request->input();
            $input['module_id'] = $module;
            $model = EkLesson::create($input);
            $model->save();
            return response()->json([
                'data' => $model
            ], 201);
        } catch (ValidationException $e) {
            return response()->json([
                'messages' => $e->validator->messages()
            ], 400);
        }
    }

    public function update(Request $request, $module, $lesson)
    {
        try {
            $this->validate($request, [
                'title'      => 'required',
            ]);
            $model = EkLesson::where([
              'module_id'  => $module,
              'id'         => $lesson
            ])
            ->firstOrFail();
            $model->fill($request->input());
            $model->save();
            return response()->json([
                'data' => $model
            ], 200);
        } catch (ValidationException $e) {
            return response()->json([
                'messages' => $e->validator->messages()
            ], 400);
        } catch (ModelNotFoundException $e) {
            return response(null, 404);
        }
    }

    public function destroy(Request $request, $module, $lesson) {
    		try {
            $model = EkLesson::where([
              'module_id'  => $module,
              'id'         => $lesson
            ])
            ->firstOrFail();
      			$model->delete();
      			return response(null, 204);
    		} catch (ModelNotFoundException $e) {
    			  return response(null, 404);
    		}
  	}
}
