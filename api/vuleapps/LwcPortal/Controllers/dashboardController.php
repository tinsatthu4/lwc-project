<?php
namespace VuleApps\LwcPortal\Controllers;

use App\Http\Controllers\Controller;
use Auth;

class dashboardController extends Controller{
	function index() {
		$user = Auth::getUser();
		if(in_array($user->type, ['learner'])) {
			return redirect(route('portal.learner.dashboard'));
		}
		if(in_array($user->type, ['teacher_1'])) {
			return redirect(route('portal.teacher01.dashboard'));
		}
		if(in_array($user->type, ['admin'])) {
			return redirect(route('portal.admin.dashboard'));
		}
		return redirect(route('portal.dashboard'));
	}
}