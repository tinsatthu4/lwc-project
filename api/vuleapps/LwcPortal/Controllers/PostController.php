<?php
namespace VuleApps\LwcPortal\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;
use VuleApps\LwcBackends\Models\DefaultPost;
use VuleApps\LwcBackends\Models\Post;
use VuleApps\LwcBackends\Models\PostMeta;

class PostController extends Controller
{
	/**
	 * @api {GET} post List Post
	 * @apiDescription Namespace : <strong>VuleApps/LwcPortal/Controllers/PostController@index</strong>
	 * @apiGroup Post
	 * @apiParam {String} [type=null]
	 * @apiParam {Number} [parent_id=null]
	 * @apiParam {Number} [with-count=null] If want to count another Table
	 * @apiParam {Number} [limit=20]
	 * @apiSuccess (200) {Object[]} data Post Object
	 * @apiSuccess (200) {String} links Pagination HTML String
	 * @apiSuccess (200) {Number} Total Total Record
	 */
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		try {
			$model = new DefaultPost();
			if($request->get('type', null) != null) {
				$model = $model->where('type', $request->get('type', null));
			}
			if($request->get('parent_id', null) != null) {
				$model = $model->where('parent_id', $request->get('parent_id', null));
			}

			if($request->get('with-count')) {
				$model = $model->withCount($request->get('with-count'));
			}

			$model = $model
				->with('postmeta')
				->paginate($request->get('limit', 20));

			return response()->json([
				'data' => $model->items(),
				'links' => (string) $model->render(),
				'total' => $model->total()
			]);
		} catch(\Exception $e) {
			return response()->json([
				'data' => [],
				'links' => '',
				'total' => 0,
				'messages' => $e->getMessage()
			]);
		}

    }

	/**
	 * @api {POST} post Create a Post
	 * @apiDescription Namespace : <strong>VuleApps/LwcPortal/Controllers/PostController@store</strong>
	 * @apiGroup Post
	 * @apiParam {String} type Detect type of Post.List Type support : `post,counrse,module`
	 * @apiParam {String} title
	 * @apiParam {String} [image]
	 * @apiParam {String} [slug]
	 * @apiParam {LongText} [content]
	 * @apiParam {Text} [excerpt]
	 * @apiParam {Number} [sort]
	 * @apiParam {String} [meta_title]
	 * @apiParam {String} [meta_content]
	 * @apiParam {String} [meta_keywords]
	 * @apiParam {Object[]} [postmeta=array]
	 * @apiSuccess (201) {Object} data Post Object
	 * @apiError (400) {Object} messages Validator Messages of Laravel
	 */
    public function store(Request $request)
    {
        //
		try {
			$this->validate($request, [
				'title' => 'required',
//				'slug' => 'required',
				'type' => 'required',
				'postmeta' => 'array'
			]);
			$input = $request->input();

			if(isset($input['postmeta']))
			{
				$metadata = $input['postmeta'];
				unset($input['postmeta']);
			} else {
				$metadata = [];
			}
			$model = DefaultPost::create($input);
			$postMetadata = [];

			foreach($metadata as $key => $value) {
				$postMetadata[] = new PostMeta([
					'meta_key' => $key,
					'meta_value' => is_array($value)? json_encode($value) : $value
				]);
			}
			if(!empty($postMetadata))
				$model->postmeta()->saveMany($postMetadata);
			$model = DefaultPost::with('postmeta')
				->find($model->id);
			return response()
					->json([
						'data' => $model
					], 201);
		} catch (ValidationException $e) {
			return response()->json([
				'messages' => $e->validator->messages()
			], 400);
		}
    }

	/**
	 * @api {GET} post/:id Get a Post
	 * @apiDescription Namespace : <strong>VuleApps/LwcPortal/Controllers/PostController@show</strong>
	 * @apiGroup Post
	 * @apiSuccess (200) {Object} data Post Object
	 * @apiError (404) {NUll} NULL Not Found Post
	 */
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		try {
			return response()->json(['data' => DefaultPost::with('postmeta')->findOrFail($id)]);
		} catch(ModelNotFoundException $e) {
			return response(null, 404);
		}
    }

	/**
	 * @api {PUT} post/:id Update a Post
	 * @apiDescription Namespace : <strong>VuleApps/LwcPortal/Controllers/PostController@update</strong>
	 * @apiGroup Post
	 * @apiParam {String} title
	 * @apiParam {String} [image]
	 * @apiParam {String} [slug]
	 * @apiParam {LongText} [content]
	 * @apiParam {Text} [excerpt]
	 * @apiParam {Number} [sort]
	 * @apiParam {String} [meta_title]
	 * @apiParam {String} [meta_content]
	 * @apiParam {String} [meta_keywords]
	 * @apiParam {Object[]} [postmeta=array]
	 * @apiSuccess (200) {Object} data Post Object
	 * @apiError (400) {Object} messages Validator Messages of Laravel
	 * @apiError (404) {NUll} NULL Not Found Post
	 */
	/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		try {
			$model = DefaultPost::findOrFail($id);
			$this->validate($request, [
				'title' => 'required',
				'postmeta' => 'array'
			]);
			$input = $request->input();
			if(isset($input['postmeta']))
			{
				$metadata = $input['postmeta'];
				unset($input['postmeta']);
			} else {
				$metadata = [];
			}

			unset($input['type']);


			$model->fill($input)
				->save();

			$model->postmeta()->delete();
			$postMetadata = [];
			foreach($metadata as $key => $value) {
				$postMetadata[] = new PostMeta([
					'meta_key' => $key,
					'meta_value' => is_array($value)? json_encode($value) : $value
				]);
			}
			if(!empty($postMetadata))
				$model->postmeta()->saveMany($postMetadata);

			return response()
				->json([
					'data' => DefaultPost::with('postmeta')->find($model->id)
				]);
		} catch (ValidationException $e) {
			return response()->json([
				'messages' => $e->validator->messages()
			], 400);
		} catch (ModelNotFoundException $e) {
			return response(null, 404);
		}
    }

	/**
	 * @api {DELETE} post/:id Delete a Post
	 * @apiDescription Namespace : <strong>VuleApps/LwcPortal/Controllers/PostController@destroy</strong>
	 * @apiGroup Post
	 * @apiSuccess (204) {Null} Null Null Body
	 * @apiError (404) {NUll} NULL Not Found Post
	 */
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		try {
			$model = DefaultPost::findOrFail($id);
			$model->delete();
			return response(null, 204);
		} catch (ModelNotFoundException $e) {
			return response(null, 404);
		}
    }
}
