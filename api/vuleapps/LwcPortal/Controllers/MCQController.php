<?php
namespace VuleApps\LwcPortal\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use VuleApps\LwcPortal\Models\MultiChoiceQuestion;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use VuleApps\LwcPortal\Models\Task;

class MCQController extends Controller
{
	/**
	 * @api {GET} task/:task_id/mcq List Multi Choice Question
	 * @apiDescription Namespace : <strong>VuleApps/LwcPortal/Controllers/MCQController@index</strong>
	 * @apiGroup Task Multi Choice Question
	 * @apiParam {Number} [limit=20]
	 * @apiSuccess (200) {Object[]} data MultiChoiceQuestion Object
	 * @apiSuccess (200) {String} links Pagination HTML String
	 * @apiSuccess (200) {Number} Total Total Record
	 */
    public function index(Request $request, $task_id)
    {
		$model = MultiChoiceQuestion::where('task_id', $task_id)
					->paginate($request->input('limit', 100));
		return response()->json([
			'data' => $model->items(),
			'links' => (string) $model->render(),
			'total' => $model->total()
		]);
	}

	/**
	 * @api {POST} task/:task_id/mcq Create MCQ
	 * @apiDescription Namespace : <strong>VuleApps/LwcPortal/Controllers/MCQController@store</strong>
	 * @apiGroup Task Multi Choice Question
	 * @apiParam {String} title
	 * @apiParam {Object[]} options
	 * @apiParam {String} answer
	 * @apiSuccess (201) {Object} data MultiChoiceQuestion Object
	 * @apiError (400) {Object} messages Validator Message Laravel
	 */
    public function store(Request $request, $task_id)
    {
		try {
			$this->validate($request, [
				'title'   => 'required',
				'options' => 'required',
				'answer'  => 'required'
			]);
			$input = $request->input();
			$input['task_id'] = $task_id;

			$model = MultiChoiceQuestion::create($input);
			return response()
				->json([
					'data' => $model
				], 201);
		} catch (ValidationException $e) {
			return response()->json([
				'messages' => $e->validator->messages()
			], 400);
		}
    }

	/**
	 * @api {GET} task/:task_id/mcq/:id GET a MCQ
	 * @apiDescription Namespace : <strong>VuleApps/LwcPortal/Controllers/MCQController@show</strong>
	 * @apiGroup Task Multi Choice Question
	 * @apiSuccess (200) {Object} data MultiChoiceQuestion Object
	 * @apiError (404) {NULL} NULL Not Found MCQ
	 */
    public function show($module_id, $id)
    {
		try {
			return response()->json(['data' => MultiChoiceQuestion::where('task_id', $module_id)->findOrFail($id)]);
		} catch(ModelNotFoundException $e) {
			return response(null, 404);
		}
    }

	/**
	 * @api {PUT} task/:task_id/mcq/:id Update a MCQ
	 * @apiDescription Namespace : <strong>VuleApps/LwcPortal/Controllers/MCQController@update</strong>
	 * @apiGroup Task Multi Choice Question
	 * @apiParam {String} title
	 * @apiParam {Object[]} options
	 * @apiParam {String} answer
	 * @apiSuccess (200) {Object} data MultiChoiceQuestion Object
	 * @apiError (400) {Object} messages Validator Message Laravel
	 * @apiError (404) {NULL} NULL Not Found MCQ
	 */
    public function update(Request $request, $task_id, $id)
    {
		try {
			$model = MultiChoiceQuestion::where('task_id', $task_id)->findOrFail($id);
			$this->validate($request, [
                'title'   => 'required',
                'options' => 'required',
                'answer'  => 'required'
			]);
			$input = $request->input();

			$model->fill($input)
				->save();
			return response()
				->json([
					'data' => $model
				]);
		} catch (ValidationException $e) {
			return response()->json([
				'messages' => $e->validator->messages()
			], 400);
		} catch (ModelNotFoundException $e) {
			return response(null, 404);
		}
    }

	/**
	 * @api {DELETE} task/:task_id/mcq/:id Delete a MCQ
	 * @apiDescription Namespace : <strong>VuleApps/LwcPortal/Controllers/MCQController@detroy</strong>
	 * @apiGroup Task Multi Choice Question
	 * @apiSuccess (204) {NULL} NULL
	 * @apiError (404) {NULL} NULL Not Found MCQ
	 */
    public function destroy($task_id, $id)
    {
		try {
			$model = MultiChoiceQuestion::where('task_id', $task_id)->findOrFail($id);
			$model->delete();
			return response(null, 204);
		} catch (ModelNotFoundException $e) {
			return response(null, 404);
		}
    }

	/**
	 * @api {POST} task/:task_id/mcq/store-array Create Multiple MCQ
	 * @apiDescription Namespace : <strong>VuleApps/LwcPortal/Controllers/MCQController@postStoreArray</strong>
	 * @apiGroup Task Multi Choice Question
	 * @apiParam {Object[]} options
	 * @apiSuccess (200) {Object[]} data MCQ Object
	 * @apiError (400) {Object} messages Validator Message Laravel
	 * @apiError (404) {NULL} NULL Not Found Task
	 */
	public function postStoreArray(Request $request, $task_id) {
		try {
			$task = Task::findOrFail($task_id);
			$this->validate($request, [
				'options' => 'array'
			]);
			$options = $request->get('options');
			$models = [];
			//clear all data
			MultiChoiceQuestion::where('task_id', $task->id)
				->delete();
			//update new data
			foreach($options as $option) {
				$input = [
					'title' => $option['title'],
					'answer' => $option['answer'],
					'task_id' => $task->id,
					'options' => $option['options']
				];
				$models[] = MultiChoiceQuestion::create($input);
			}

			return response()->json([
				'data' => $models
			]);
		} catch(ValidationException $e) {
			return response()->json([
				'messages' => $e->validator->messages()
			], 400);
		}
		catch (ModelNotFoundException $e) {
			return response(null, 404);
		}
	}
}
