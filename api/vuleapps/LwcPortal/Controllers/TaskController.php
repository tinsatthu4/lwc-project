<?php
namespace VuleApps\LwcPortal\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use VuleApps\LwcPortal\Models\Task;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class TaskController extends Controller
{
	/**
	 * @api {GET} task List Tasks
	 * @apiDescription Namespace : <strong>VuleApps/LwcPortal/Controllers/TaskController@index</strong>
	 * @apiGroup Task
	 * @apiParam {Number} [limit=20]
	 * @apiSuccess (200) {Object[]} data Task Object
	 * @apiSuccess (200) {String} links Pagination HTML String
	 * @apiSuccess (200) {Number} Total Total Record
	 */
    public function index(Request $request)
    {
		$model = Task::paginate($request->input('limit', 20));
		return response()->json([
			'data' => $model->items(),
			'links' => (string) $model->render(),
			'total' => $model->total()
		]);
	}

	/**
	 * @api {GET} task/:id Get a Task
	 * @apiDescription Namespace : <strong>VuleApps/LwcPortal/Controllers/TaskController@show</strong>
	 * @apiGroup Task
	 * @apiSuccess (200) {Object} data Task Object
	 * @apiError (404) {NUll} NULL Not Found Task
	 */
    public function show($id)
    {
		try {
			return response()->json(['data' => Task::findOrFail($id)]);
		} catch(ModelNotFoundException $e) {
			return response(null, 404);
		}
    }

    /**
     * @api {DELETE} task/:id Delete a Task
     * @apiDescription Namespace : <strong>VuleApps/LwcPortal/Controllers/TaskController@show</strong>
     * @apiGroup Task
     * @apiSuccess (204) {NUll} NULL delete success
     * @apiError (404) {NUll} NULL Not Found Task
     */
    public function destroy($id)
    {
        try {
            $model = Task::findOrFail($id);
            $model->delete();
            return response(null, 204);
        } catch (ModelNotFoundException $e) {
            return response(null, 404);
        }
    }

    /**
     * @api {POST} /task/:task_id/task Create task by module
     * @apiGroup Teacher API
     * @apiDescription Namespace: <strong>VuleApps\LwcPortal\Controllers\TaskController@store</strong>
     * @apiPermission Teacher is Logged
     * @apiPermission Teacher has permission to access the course of module
     * @apiParam {String} post_id Module id
     * @apiParam {String} title Title task
     * @apiParam {String} type Only Has 2 value : assignment,mcq
     * @apiParam {String} keywords Task keywords
     * @apiParam {String} preference Task preference
     * @apiParam {String} teacher id
     * @apiSuccess (201) {Object[]} data Module with Module Object
     */
    public function store(Request $request)
    {
        try {
            $this->validate($request, [
                'post_id'    => 'required',
                'title'      => 'required',
                'type'       => 'required|in:assignment,mcq',
                'keywords'   => 'required',
                'preference' => 'required',
                'teacher_id' => "required"
            ]);
            $input = $request->input();
            $model = Task::create($input);
            return response()
                ->json([
                    'data' => $model
                ], 201);
        } catch (ValidationException $e) {
            return response()->json([
                'messages' => $e->validator->messages()
            ], 400);
        }
    }

    /**
     * @api {PUT} /task/:task_id Update task
     * @apiGroup Task
     * @apiDescription Namespace: <strong>VuleApps\LwcPortal\Controllers\TaskController@update</strong>
     * @apiParam {String} post_id Module id
     * @apiParam {String} title Title task
     * @apiParam {String} type Only Has 2 value : assignment,mcq
     * @apiParam {String} keywords Task keywords
     * @apiParam {String} preference Task preference
     * @apiParam {String} teacher id
     * @apiSuccess (200) {Object[]} data Module with Module Object
     */
    public function update(Request $request, $task_id)
    {
        try {
            $model = Task::findOrFail($task_id);
            $this->validate($request, [
                'post_id'    => 'required',
                'title'      => 'required',
                'type'       => 'required|in:assignment,mcq',
                'keywords'   => 'required',
                'preference' => 'required',
                'teacher_id' => "required"
            ]);
            $input = $request->input();
            $model->fill($input)
                ->save();
            return response()
                ->json([
                    'data' => $model
                ]);
        } catch (ValidationException $e) {
            return response()->json([
                'messages' => $e->validator->messages()
            ], 400);
        } catch (ModelNotFoundException $e) {
            return response(null, 404);
        }
    }
}
