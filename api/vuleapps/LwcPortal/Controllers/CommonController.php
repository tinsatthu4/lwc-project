<?php
namespace VuleApps\LwcPortal\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Auth;
use Mockery\CountValidator\Exception;
use TermRepository;
use VuleApps\LwcPortal\Models\Counrse;
use VuleApps\LwcPortal\Models\UserCounrse;
use VuleApps\LwcPortal\Repository\PortalRepository;

class CommonController extends Controller
{
	function index()
    {
        $model = Pos::paginate($request->input('limit', 20));
        return response()->json([
            'data' => $model->items(),
            'links' => (string) $model->render(),
            'total' => $model->total()
        ]);
	}

    /**
     * @api {GET} api/:term_slug Get post by slug (event, announments, news)
     * @apiGroup Post
     * @apiDescription Namespace: <strong>VuleApps\LwcPortal\Controllers\CommonController@getPostsBySlug</strong>
     * @apiParam {number} [limit=30] Number Record
     * @apiSuccess (200) {Object[]} data Post with Post Object
     * @apiSuccess (200) {String} links HTML String pagination
     * @apiSuccess (200) {Number} total Total record
     */
    function getPostsBySlug($slug, Request $request)
    {
        $term = TermRepository::getBySlug($slug);
        if($term == null) {
            return response(null, 404);
        }
        $posts = $term->post()->paginate($request->get('page', 30));
        return response()
            ->json([
                'data' => $posts->items(),
                'links' => (string) $posts->render(),
                'total' => $posts->total()
            ], 200);
    }
}