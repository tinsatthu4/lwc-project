<?php namespace VuleApps\LwcPortal\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use VuleApps\LwcPortal\Exceptions\PaypalException;
use VuleApps\LwcPortal\Models\GuestPayment;
use Mail;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use Paypal;

class GuestPaymentController extends Controller
{
    function postGenerateLinkPayment(Request $request)
    {
        try {
            $this->validate($request, [
                'name'   => 'required',
                'email'  => 'required',
                'phone'  => 'required',
                'course' => 'required',
                'price'  => 'required|numeric',
            ]);
            $input        = $request->only('name', 'email', 'phone', 'course', 'price');
            $base64       = base64_encode(implode(',', $input));
            $guestPayment = GuestPayment::where('base64', $base64)->first();
            
            if ($guestPayment) {
                return response()->json(["data" => $guestPayment]);
            }

            $uid = uniqid();
            $input['code']   = $uid;
            $input['base64'] = $base64;
            $input['link']   = url("/#!/guest-payment/{$uid}");
            $guest = GuestPayment::create($input);
            return response()->json(['data' => $guest]);
        } 
        catch (ValidationException $e) {
            return response()->json(['messages' => $e->validator->messages()], 400);
        }
    }

    function postSavePaymentMethod(Request $request, $code)
    {
        try {
            $this->validate($request, [
                'payment_method' => 'required'
            ]);

            $guestPayment = GuestPayment::where('code', $code)->firstOrFail();
            $guestPayment->payment_method = $request->input('payment_method');
            $guestPayment->save();

            //Send Mail
            Mail::send('email.guest_payment', $guestPayment->toArray(), function ($m) {
                $m->from('noti@info-awards.com', 'Info Awards');

//                $m->to('t.kahyan@gmail.com', "t.kahyan")
//                    ->cc("huy6289@gmail.com")
                $m->to('huy6289@gmail.com', "huy6289")
                    ->subject('New Payment Method From Student');
            });

            $guestPayment->process = GuestPayment::PROCESS_PAYMENT;
            $guestPayment->save();

            return response(null, 204);
        } 
        catch (ValidationException $e) {
            return response()->json(['messages' => $e->validator->messages()], 400);
        } 
        catch (ModelNotFoundException $e) {
            return response(null, 404);
        }
    }

    function postPaypalGenerateLink(Request $request, $code)
    {
        try {
            $guestPayment   = GuestPayment::where('code', $code)->firstOrFail();
            $returnUrl      = route('guest.paypal.success', $guestPayment->code);
            $cancelUrl      = route('guest.paypal.cancel', $guestPayment->code);
            $ItemName       = $guestPayment->course;
            $ItemPrice      = $guestPayment->price;
            $ItemQty        = 1;
            $ItemTotalPrice = $ItemQty * $ItemPrice;
            $redirect       = Paypal::send($returnUrl, $cancelUrl, $ItemName, $ItemPrice, $ItemQty, $ItemTotalPrice);
            return response()->json(['redirect' => $redirect]);
        } 
        catch (ModelNotFoundException $e) {
            return response(null, 404);
        }
    }

    function show(Request $request, $code)
    {
        try {
            $guestPayment = GuestPayment::where('code', $code)->firstOrFail();
            return response()->json(['data' => $guestPayment]);
        } 
        catch (ModelNotFoundException $e) {
            return response(null, 404);
        }
    }

    function getPaypalSuccess(Request $request, $code)
    {
        try {
            $this->validate($request, [
                'token' => 'required',
                'PayerID' => 'required'
            ]);

            $guestPayment = GuestPayment::where('code', $code)->firstOrFail();

            $ItemName       = $guestPayment->course;
            $ItemPrice      = $guestPayment->price;
            $ItemQty        = 1;
            $ItemTotalPrice = $ItemQty * $ItemPrice;
            Paypal::validate($ItemName, $ItemPrice, $ItemQty, $ItemTotalPrice);
            $guestPayment->payment_method = GuestPayment::PAYMENT_PAYPAL;
            $guestPayment->desc           = json_encode(["PayerID" => $request->input('PayerID')]);
            $guestPayment->save();

            Mail::send('email.guest_payment', $guestPayment->toArray(), function ($m) {
                $m->from('noti@info-awards.com', 'Info Awards');

                $m->to('huy6289@gmail.com', "huy6289")
                    ->subject('Paypal Payment Method New Student');
            });

            $guestPayment->process = GuestPayment::PROCESS_PAYMENT;
            $guestPayment->save();

            return redirect()->to("/#!/guest-payment/{$code}?paypal=1");
        } 
        catch (ModelNotFoundException $e) {
            return response("Invalid", 404);
        } 
        catch (PaypalException $e) {
            return redirect()->to("/#!/guest-payment/{$code}?message={$e->getMessage()}");
        }
    }

    function getPaypalCancel(Request $request, $code)
    {
        try {
            $guestPayment = GuestPayment::where('code', $code)->firstOrFail();
            return redirect()->to("/#!/guest-payment/{$code}");
        } 
        catch (ModelNotFoundException $e) {
            return response("Invalid", 404);
        }
    }

    function postCreateInfoGuestPayment(Request $request)
    {
        try {
            $this->validate($request, [
                'name'   => 'required',
                'email'  => 'required',
                'course' => 'required',
            ]);

            $uid      = uniqid();
            $input    = $request->only('name', 'email', 'course');
            $base64   = base64_encode(implode(',', $input));
            
            $hasGuest = GuestPayment::where('base64', $base64)->first();
            if ($hasGuest) {
                return response()->json(["data" => $hasGuest, "message" => "The course is registered by this information! (name, email, course)"], 200);
            }

            $guestPayment = GuestPayment::create([
                'code'                    => $uid,
                'payment_method'          => 'unknown',
                'base64'                  => $base64,
                'link'                    => url("/#!/guest-payment/{$uid}"),
                'phone'                   => $request->input('phone', null),
                'email'                   => $request->input('email', null),
                'address'                 => $request->input('address', null),
                'course'                  => $request->input('course', null),
                'name'                    => $request->input('name', null),
                'surname_name'            => $request->input('surname_name', null),
                'birthday'                => $request->input('birthday', null),
                'country'                 => $request->input('country', null),
                'nationality_of'          => $request->input('nationality_of', null),
                'gender'                  => $request->input('gender', null),
                'highest_education_level' => $request->input('highest_education_level', null),
                'path_studen_photo'       => $request->input('path_studen_photo', null),
                'path_transcript'         => $request->input('path_transcript', null)
            ]);

            return response()->json([
                'data' => $guestPayment
            ], 200);
        } 
        catch (ModelNotFoundException $e) {
            return response("Invalid", 404);
        } 
        catch (ValidationException $e) {
            return response()->json(['messages' => $e->validator->messages()], 400);
        }
    }

    function postUpdateInfoGuestPayment(Request $request, $code)
    {
        try {
            $this->validate($request, [
                'name'   => 'required',
                'email'  => 'required',
                'phone'  => 'required',
                'course' => 'required',
                'price'  => 'required|numeric',
            ]);

            $input  = $request->only('name', 'email', 'course');
            $base64 = base64_encode(implode(',', $input));

            $guestPayment = GuestPayment::where('code', $code)->firstOrFail();
            if ($guestPayment) {
                $guestPayment->link                    = url("/#!/guest-payment/{$guestPayment->code}");
                $guestPayment->base64                  = $base64;
                $guestPayment->course                  = $request->input('course', null);
                $guestPayment->price                   = $request->input('price', null);
                $guestPayment->payment_method          = 'unknown';
                $guestPayment->name                    = $request->input('name', null);
                $guestPayment->phone                   = $request->input('phone', null);
                $guestPayment->email                   = $request->input('email', null);
                $guestPayment->address                 = $request->input('address', null);
                $guestPayment->surname_name            = $request->input('surname_name', null);
                $guestPayment->birthday                = $request->input('birthday', null);
                $guestPayment->country                 = $request->input('country', null);
                $guestPayment->nationality_of          = $request->input('nationality_of', null);
                $guestPayment->gender                  = $request->input('gender', null);
                $guestPayment->highest_education_level = $request->input('highest_education_level', null);
                $guestPayment->path_studen_photo       = $request->input('path_studen_photo', null);
                $guestPayment->path_transcript         = $request->input('path_transcript', null);
                $guestPayment->note         = $request->input('note', null);
                $guestPayment->save();

                return response()->json([
                                            'data' => $guestPayment
                                        ], 200);
            } else {
                return response('', 404);
            }
        } 
        catch (ModelNotFoundException $e) {
            return response("Invalid", 404);
        } 
        catch (ValidationException $e) {
            return response()->json(['messages' => $e->validator->messages()], 400);
        }
    }

    function getInfoGuestPayment(Request $request, $code = null)
    {
        $guestPayment = new GuestPayment();

        if (isset($code)) {
            $guestPayment = $guestPayment->where('code', $code)->first();
            return response()->json([
                                        'data' => $guestPayment
                                    ], 200);
        } else {
            $sort  = $request->input('sort', 'id');
            $order = $request->input('order', 'desc');
            $take  = $request->input('take', '20');

            if($request->has('code')) {
                $guestPayment = $guestPayment->hasCode($request->input('code', null));
            }

            if($request->has('payment_method')) {
                $guestPayment = $guestPayment->hasPaymentMethod($request->input('payment_method', null));
            }

            if($request->has('phone')) {
                $guestPayment = $guestPayment->hasPhone($request->input('phone', null));
            }

            if($request->has('email')) {
                $guestPayment = $guestPayment->hasEmail($request->input('email', null));
            }

            if($request->has('course')) {
                $guestPayment = $guestPayment->hasCourse($request->input('course', null));
            }

            if($request->has('price_from')) {
                $guestPayment = $guestPayment->hasPriceFrom($request->input('price_from', null));
            }

            if($request->has('price_to')) {
                $guestPayment = $guestPayment->hasPriceTo($request->input('price_to', null));
            }

            if($request->has('process')) {
                $guestPayment = $guestPayment->hasProcess($request->input('process', null));
            }

            $guestPayment = $guestPayment->orderBy($sort, $order)
                                         ->simplePaginate($take)
                                         ->appends($request->except('page'));

            return response()->json( $guestPayment, 200);
        }     
    }

    function postMailInfoGuestPayment(Request $request, $code)
    {
        try {
            if ($code) {
                $guestPayment = GuestPayment::where('code', $code)->firstOrFail();

                //Send Mail
                Mail::send('email.guest_payment_link', $guestPayment->toArray(), function ($m) use ($guestPayment) {
                    $m->from('noti@info-awards.com', 'LAA course information');

                    $m->to($guestPayment->email, $guestPayment->name . ' ' . $guestPayment->surname_name)
                      ->subject($guestPayment->course);
                });

                $guestPayment->process = GuestPayment::PROCESS_SEND_MAIL;
                $guestPayment->save();

                return response('', 200);
            }

        } 
        catch (ModelNotFoundException $e) {
            return response("Invalid", 404);
        } 
        catch (ValidationException $e) {
            return response()->json(['messages' => $e->validator->messages()], 400);
        }
    }

    function postFile(Request $request)
    {
        try {
            $folder = 'payment';
            $size   = null;
            $mimes  = null;

            if ($request->has('folder')) {
                $folder = $request->input('folder', null);
            }

            if ($request->has('size')) {
                $size = "|max:" . str_replace(' ', '', $request->input('size', null));
            }

            if ($request->has('mimes')) {
                $mimes = "|mimes:" . str_replace(' ', '', $request->input('mimes', null));
            }

            $this->validate($request, [
                'file' => 'required|file' . $mimes . $size
            ]);

            if ($request->hasFile('file')) {
                $file      = $request->file('file');
                $imageName = uniqid() . '.' . strtolower($file->getClientOriginalExtension());
                $file->move(storage_path($folder), $imageName);
            }

            return response()->json([
                'data' => [
                    'path' => $folder . '/' . $imageName
                ]
            ], 200);
        } 
        catch (ModelNotFoundException $e) {
            return response("Invalid", 404);
        } 
        catch (ValidationException $e) {
            return response()->json(['messages' => $e->validator->messages()], 400);
        }
    }

    public function updateProcessGuestPayment(Request $request)
    {
        try {
            $this->validate($request, [
                'code'    => 'required',
                'process' => 'required'
            ]);

            $process = GuestPayment::PROCESS_PENDING;

            $guestPayment = GuestPayment::hasCode($request->code)->firstOrFail();

            switch ($request->process) {
                case GuestPayment::PROCESS_PENDING:
                    $process = GuestPayment::PROCESS_PENDING;
                    break;
                case GuestPayment::PROCESS_PAYMENT:
                    $process = GuestPayment::PROCESS_PAYMENT;
                    break;
                case GuestPayment::PROCESS_SEND_MAIL:
                    $process = GuestPayment::PROCESS_SEND_MAIL;
                    break;
                case GuestPayment::PROCESS_COMPLETE:
                    $process = GuestPayment::PROCESS_COMPLETE;
                    break;
                default:
                    break;
            }
            $guestPayment = $guestPayment->fill([
                'process' => $process
            ]);
            $guestPayment->save();

            return response()->json([
                'data' => $guestPayment
            ], 200);
        } 
        catch (ModelNotFoundException $e) {
            return response("Invalid", 404);
        } 
        catch (ValidationException $e) {
            return response()->json(['messages' => $e->validator->messages()], 400);
        }
    }
}