<?php
namespace VuleApps\LwcPortal\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Auth;
use App\User;

class AdministratorTeacherController extends Controller {

	/**
	 * @api {GET} teacher/search Search Teachers
	 * @apiDescription Namespace: <strong>VuleApps\LwcPortal\Controllers\AdministratorTeacherController@getTeacherSearch</strong>
	 * @apiParam {string} s Keyword for search
	 * @apiParam {number} [limit] Number record (default: 30 )
	 * @apiPermission administrator must logged
	 * @apiGroup Administrator
	 * @apiSuccess (200) {Object[]} data List of users
	 * @apiSuccess (200) {Number} total Total records
	 * @apiSuccess (200) {String} links HTML String Pagination
 	 */
	function getTeacherSearch(Request $request) {
		try {
			$this->validate($request, [
				's' => 'required',
				'limit' => 'number'
			]);
			$model = User::isTeacher()
				->where('name', 'like', "%{$request->input('s')}%")
				->paginate($request->get('limit', 30));
			return response()
				->json([
					'data' => $model->items(),
					'total' => $model->total(),
					'links' => (string) $model->render()
				]);

		} catch(ValidationException $e) {
			return response()
						->json(['data' => [], 'total' => 0, 'links' => '']);
		}
	}

//	function postCourseAddTeacher(Request $request, $course_id) {
//		try {
//			$this->validate($request, [
//				'counrse_id' => 'exists:posts,id,type,counrse',
//				'teacher_id' => 'exists:users,id,type,teacher_1'
//			]);
//		} catch (ValidationException $e) {
//			return response()->json(['messages' => $e->validator->messages()], 400);
//		}
//	}
}