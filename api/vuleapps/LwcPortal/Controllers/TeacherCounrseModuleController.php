<?php
namespace VuleApps\LwcPortal\Controllers;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Request;
use VuleApps\LwcPortal\Models\Module;

class TeacherCounrseModuleController extends Controller
{
    private function getTeacherID()
    {
        return Auth::getUser()->id;
    }

    /**
     * @api {GET} /counrse/:counrse_id/module Get modules by course
     * @apiGroup Teacher API
     * @apiDescription Namespace: <strong>VuleApps\LwcPortal\Controllers\TeacherCounrseModuleController@index</strong>
     * </br>
     * UnitTest: <strong>vuleappstest\LwcPortal\Controllers\TeacherCounrseModuleControllerTest@testIndexSuccess</strong>
     * @apiPermission Teacher is Logged
     * @apiPermission Teacher has permission to access the course
     * @apiParam {number} [limit=30] Number Records
     * @apiSuccess (200) {Object[]} data Module with Module Object
     * @apiSuccess (200) {String} links HTML String pagination
     * @apiSuccess (200) {Number} total Total record
     */
    public function index(Request $request, $counrse_id)
    {
        try {
            $request->merge(['teacher_id' => $this->getTeacherID()]);
            $this->validate($request, [
                'teacher_id' => "required|teacher_has_policy:{$counrse_id},module"
            ]);
            $moduleOfCournse = Module::where('parent_id', $counrse_id)
                ->paginate($request->get('limit', 30));

            return response()->json([
                'data' => $moduleOfCournse->items(),
                'links' => (string) $moduleOfCournse->render(),
                'total' => $moduleOfCournse->total()
            ], 200);
        }
        catch(ValidationException $e) {
            return response()->json([
                'messages' => $e->validator->messages()
            ], 400);
        }
    }

    /**
     * @api {POST} /counrse/:counrse_id/module Create module by course
     * @apiGroup Teacher API
     * @apiDescription Namespace: <strong>VuleApps\LwcPortal\Controllers\TeacherCounrseModuleController@store</strong>
     * </br>
     * UnitTest: <strong>vuleappstest\LwcPortal\Controllers\TeacherCounrseModuleControllerTest@testStoreSuccess</strong>
     * @apiPermission Teacher is Logged
     * @apiPermission Teacher has permission to access the course
     * @apiParam {String} title Title module
     * @apiSuccess (201) {Object[]} data Module with Module Object
     */
    public function store(Request $request, $counrse_id) {
        try {
			$teacher_id = $this->getTeacherID();
            $request->merge(['teacher_id' => $teacher_id]);
            $this->validate($request, [
                'title'      => 'required',
                'teacher_id' => "required|teacher_has_course:{$counrse_id}"
            ]);
            $input = $request->input();

            $input['parent_id'] = $counrse_id;
            $input['type'] = Module::TYPE;
            $input['teacher_id'] = $teacher_id;
            $model = Module::create($input);
			$model->save();
            return response()->json([
                'data' => $model
            ], 201);
        } catch (ValidationException $e) {
            return response()->json([
                'messages' => $e->validator->messages()
            ], 400);
        }
    }

    /**
     * @api {PUT} /counrse/:counrse_id/module/:module_id Update module by course
     * @apiGroup Teacher API
     * @apiDescription Namespace: <strong>VuleApps\LwcPortal\Controllers\TeacherCounrseModuleController@update</strong>
     * </br>
     * UnitTest: <strong>vuleappstest\LwcPortal\Controllers\TeacherCounrseModuleControllerTest@testUpdateSuccess</strong>
     * @apiPermission Teacher is Logged
     * @apiPermission Teacher has permission to access the course
     * @apiPermission Module is exist.
     * @apiParam {String} title Title module
     * @apiSuccess (200) {Object[]} data Module with Module Object
     */
    public function update(Request $request, $counrse_id, $module_id)
    {
        try {
            $request->merge(['teacher_id' => $this->getTeacherID()]);
            $model = Module::findOrFail($module_id);
            if($model->parent_id != $counrse_id) {
                return response(null, 404);
            }
            $this->validate($request, [
                'title'      => 'required',
                'teacher_id' => "required|teacher_has_policy:{$counrse_id},module"
            ]);
            $input = $request->input();
            unset($input['teacher_id']);
            unset($input['type']);

            $model->fill($input)->save();
            return response()->json([
                'data' => $model
            ], 200);
        } catch (ValidationException $e) {
            return response()->json([
                'messages' => $e->validator->messages()
            ], 400);
        } catch (ModelNotFoundException $e) {
            return response(null, 404);
        }
    }
}