<?php
namespace VuleApps\LwcPortal\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Auth;
use Mockery\CountValidator\Exception;
use TermRepository;
use VuleApps\LwcBackends\Models\PostMeta;
use VuleApps\LwcBackends\Models\UserMeta;
use VuleApps\LwcPortal\Models\Counrse;
use VuleApps\LwcPortal\Models\Module;
use VuleApps\LwcPortal\Models\UserCounrse;
use VuleApps\LwcPortal\Models\UserModule;
use VuleApps\LwcPortal\Repository\PortalRepository;
use VuleApps\LwcPortal\Models\UserTask;
use Zipper;
use PhpOffice\PhpWord\IOFactory;
use VuleApps\LwcPortal\Traits\SetupMF;

class MasterFileController extends Controller
{
    use SetupMF;

    /**
     * @api {GET} /master-file/list Master Files List
     * @apiDescription Namespace : <strong>VuleApps/LwcPortal/Controllers/MasterFileController@index</strong>
     * @apiGroup Master File
     * @apiParam {Number} [limit=20]
     * @apiSuccess (200) {Object[]} data UserCournse Object. UserCournse with('cournse') with('user')
     * @apiSuccess (200) {String} links Pagination HTML String
     * @apiSuccess (200) {Number} Total Total Record
	 * @apiSuccessExample {json} Success-Response:
	 * {"data":[{"id":1,"user_id":1,"post_id":1,"processed":0,"course_count":27,"user":{"id":1,"name":"learner 1"},"course":{"id":1,"title":"Course 1"}},{"id":2,"user_id":1,"post_id":2,"processed":0,"course_count":27,"user":{"id":1,"name":"learner 1"},"course":{"id":2,"title":"Course 2"}},{"id":3,"user_id":1,"post_id":3,"processed":0,"course_count":27,"user":{"id":1,"name":"learner 1"},"course":{"id":3,"title":"Course 3"}},{"id":4,"user_id":2,"post_id":1,"processed":0,"course_count":27,"user":{"id":2,"name":"learner 2"},"course":{"id":1,"title":"Course 1"}},{"id":5,"user_id":2,"post_id":2,"processed":0,"course_count":27,"user":{"id":2,"name":"learner 2"},"course":{"id":2,"title":"Course 2"}},{"id":6,"user_id":2,"post_id":3,"processed":0,"course_count":27,"user":{"id":2,"name":"learner 2"},"course":{"id":3,"title":"Course 3"}},{"id":7,"user_id":3,"post_id":4,"processed":15,"course_count":27,"user":{"id":3,"name":"learner 3"},"course":{"id":4,"title":"Course 4"}},{"id":8,"user_id":3,"post_id":5,"processed":15,"course_count":27,"user":{"id":3,"name":"learner 3"},"course":{"id":5,"title":"Course 5"}}],"links":"","total":8}
     */
    public function index(Request $request)
    {
        $model = UserCounrse::has('course')
			->has('user')
			->with([
				'user' => function($user) { $user->select('id', 'name'); },
				'course' => function($course) { $course->select('id', 'title'); }
			])
            ->paginate($request->get('limit', 20));
		$totalTaskIds = [];
		$model->each(function($obj) use (&$totalTaskIds) {
			if(!isset($totalTaskIds[$obj->course->id])) {
				$totalTaskIds[$obj->course->id] = app(PortalRepository::class)->groupTaskIDsByCourse($obj->course->id);
			}

			$obj->processed = UserTask::where([
				'user_id' => $obj->user->id,
				'status' => UserTask::STATUS_SUBMISSION
			])
				->whereIn('task_id', $totalTaskIds[$obj->course->id])
				->count();

			$obj->course_count = count($totalTaskIds[$obj->course->id]);

		});
        return response()->json([
            'data' => $model->items(),
            'links' => (string) $model->render(),
            'total' => $model->total()
        ]);
    }

    public function indexModule(Request $request)
    {
        $model = UserModule::has('user')
            ->with([
                'user'   => function($user) { $user->select('id', 'name'); },
                'module' => function($module) {
                    $module->select('id', 'title', 'parent_id');
                },
            ])
            ->paginate($request->get('limit', 20));

        $totalTaskIds = [];
        foreach($model as &$m) {
            if(isset($m->module->parent_id)) {
                $course = Counrse::where('id', $m->module->parent_id);
                if($course->count() > 0) {
                    $m->course = Counrse::where('id', $m->module->parent_id)
                        ->first();
                }
            }
            if(isset($m->module->id)) {
                if(!isset($totalTaskIds[$m->module->id])) {
                    $totalTaskIds[$m->module->id] = app(PortalRepository::class)->groupTaskIDsByModule($m->module->id);
                }

                $m->processed = UserTask::where([
                    'user_id' => $m->user->id,
                    'status' => UserTask::STATUS_SUBMISSION
                ])
                    ->whereIn('task_id', $totalTaskIds[$m->module->id])
                    ->count();

                $m->course_count = count($totalTaskIds[$m->module->id]);
            }
        }

        return response()->json([
            'data' => $model->items(),
            'links' => (string) $model->render(),
            'total' => $model->total()
        ]);
    }

    /**
     * @api {GET} /master-file/download/user/:user_id/course/:course_id Download Master File
     * @apiDescription Namespace : <strong>VuleApps/LwcPortal/Controllers/MasterFileController@getMasterFile</strong>
     * @apiGroup Master File
     * @apiParam {Number} [limit=20]
     * @apiSuccess (200) {NULL} success file .mht
     */
    public function getMasterFile($user_id, $course_id)
    {
        try {
            $user = User::findOrFail($user_id);
            $userCourse = UserCounrse::where([
                'user_id'   => $user_id,
                'post_id'   => $course_id
            ])->firstOrFail();
            $course = Counrse::findOrFail($course_id);
            $userConfig = UserMeta::where([
                'user_id'   => $user_id,
                'meta_key' => 'master_file'
            ])->first();
            $courseConfig = PostMeta::where([
                'post_id'   => $course_id,
                'meta_key' => 'master_file'
            ])->first();
            $html = view('LwcPortal::user.master_file')
                ->with([
                    'user'     => $user,
                    'course'   => $course,
                    'config'   => [
                        'learner'   => ($userConfig) ? unserialize($userConfig->meta_value) : [],
                        'course'    => ($courseConfig) ? unserialize($courseConfig->meta_value) : []
                    ]
                ])->render();
            return response($html)->withHeaders([
                'Content-type' => 'message/rfc822',
                'Content-disposition' => 'attachment; filename="master_file_user_'. $user_id .'_course_'. $course_id .'.mht";'
            ]);
        }
        catch(ModelNotFoundException $e) {
            return response(null, 404);
        }
    }

    /**
     * @api {GET} learner/master-file/config Get config Master File For Learner
     * @apiDescription Namespace : <strong>VuleApps/LwcPortal/Controllers/MasterFileController@getConfigLearner</strong>
     * @apiGroup Master File
     * @apiSuccess (200) {Array} data config
     */
    public function getConfigLearner()
    {
        try {
            $learner = Auth::user();
            $config = $learner->metavalue('master_file');
            return response()->json([
                'data' => $config,
            ]);
        }
        catch (ModelNotFoundException $e) {
            return response(null, 404);
        }
    }

    /**
     * @api {POST} learner/master-file/config Get config Master File For Learner
     * @apiDescription Namespace : <strong>VuleApps/LwcPortal/Controllers/MasterFileController@getConfigLearner</strong>
     * @apiGroup Master File
     * @apiParam {Array} [meta_value]
     * @apiSuccess (200) {Array} data config
     */
    public function postConfigLearner(Request $request)
    {
        try {
            $learner = Auth::user();
            $config = UserMeta::where([
                'user_id'   => $learner->id,
                'meta_key'  => 'master_file'
            ]);
            if($config->count() == 0) {
                $data = [
                    'user_id'       => $learner->id,
                    'meta_key'      => 'master_file',
                    'meta_value'    => serialize($request->all()),
                    'serialize'     => 1
                ];
                UserMeta::create($data);
            }
            else {
                $data = [
                    'meta_value'    => serialize($request->all()),
                ];
                $config->update($data);
            }
            return response()->json([
                'data' => $learner->metavalue('master_file'),
            ]);
        }
        catch (ModelNotFoundException $e) {
            return response(null, 404);
        }
    }

    /**
     * @api {GET} teacher/master-file/config Get config Master File For Teacher
     * @apiDescription Namespace : <strong>VuleApps/LwcPortal/Controllers/MasterFileController@getConfigTeacher</strong>
     * @apiGroup Master File
     * @apiParam {Number} [course_id]
     * @apiSuccess (200) {Array} data config
     */
    public function getConfigTeacher($id)
    {
        try {
            $course = Counrse::findOrFail($id);
            $config = $course->metavalue('master_file');
            return response()->json([
                'data' => unserialize($config),
            ]);
        }
        catch (ModelNotFoundException $e) {
            return response(null, 404);
        }
    }

    /**
     * @api {POST} teacher/master-file/config Get config Master File For Teacher
     * @apiDescription Namespace : <strong>VuleApps/LwcPortal/Controllers/MasterFileController@postConfigTeacher</strong>
     * @apiGroup Master File
     * @apiParam {Array} [meta_value]
     * @apiSuccess (200) {Array} data config
     */
    public function postConfigTeacher(Request $request, $id)
    {
        try {
            $course = Counrse::findOrFail($id);
            $config = PostMeta::where([
                'post_id'   => $id,
                'meta_key'  => 'master_file'
            ]);
            if($config->count() == 0) {
                $data = [
                    'post_id'       => $id,
                    'meta_key'      => 'master_file',
                    'meta_value'    => serialize($request->all()),
                ];
                PostMeta::create($data);
            }
            else {
                $data = [
                    'meta_value'    => serialize($request->all()),
                ];
                $config->update($data);
            }
            return response()->json([
                'data' => unserialize($course->metavalue('master_file')),
            ]);
        }
        catch (ModelNotFoundException $e) {
            return response(null, 404);
        }
    }

    public function getMasterFileByType($user_id, $module_id, $type, Request $request)
    {
        try {
            $user = User::findOrFail($user_id);
            $userCourse = UserModule::where([
                'user_id'   => $user_id,
                'post_id'   => $module_id
            ])->firstOrFail();
            $module = Module::findOrFail($module_id);

            $html = '';

            $task_ids = $module->tasks()->pluck('id');
            $task_ids = ($task_ids) ? $task_ids->toArray() : [];
            $teacher = User::find($module->teacher_id);
            $last_task_info = UserTask::whereIn('task_id', $task_ids)
                ->where('user_id', $user_id)
                ->orderBy('updated_at', 'desc')
                ->first();

            if($type == 1) {
                $html = view('LwcPortal::user.module_master_file_1')
                    ->with([
                        'user'     => $user,
                        'module'   => $module,
                        'teacher'  => $teacher,
                        'last_task' => $last_task_info
                    ])->render();
            }

            if($type == 2) {
                $html = view('LwcPortal::user.module_master_file_2')
                    ->with([
                        'user'     => $user,
                        'module'   => $module,
                        'los'      => $module->los()->get(),
                        'teacher'  => $teacher,
                        'last_task' => $last_task_info
                    ])->render();
            }

            if($type == 3) {
                $html = view('LwcPortal::user.module_master_file_3')
                    ->with([
                        'user'     => $user,
                        'module'   => $module,
                    ])->render();
            }

            if($type == 4) {
                $html = view('LwcPortal::user.module_master_file_4')
                    ->with([
                        'user'     => $user,
                        'module'   => $module,
                        'tasks'    => UserTask::whereIn('task_id', $task_ids)
                            ->where('user_id', $user_id)
                            ->with('task')
                            ->get()
                    ])->render();
            }

            return response($html)->withHeaders([
                'Content-type' => 'message/rfc822',
                'Content-disposition' => 'attachment; filename="master_file_user_'. $user_id .'_course_'. $module_id .'_'. $type .'.mht";'
            ]);
        }
        catch(ModelNotFoundException $e) {
            return response(null, 404);
        }
    }

    public function getAllMasterFile($user_id, $module_id, Request $request)
    {
        try {
            $user = User::findOrFail($user_id);
            $userCourse = UserModule::where([
                'user_id'   => $user_id,
                'post_id'   => $module_id
            ])->firstOrFail();
            $module = Module::findOrFail($module_id);

            $html = '';

            $task_ids = $module->tasks()->pluck('id');
            $task_ids = ($task_ids) ? $task_ids->toArray() : [];
            $teacher = User::find($module->teacher_id);
            $last_task_info = UserTask::whereIn('task_id', $task_ids)
                ->where('user_id', $user_id)
                ->orderBy('updated_at', 'desc')
                ->first();

            $folder = time() . $user_id . $module_id;

//            $html1 = view('LwcPortal::user.module_master_file_1')
//                ->with([
//                    'user'     => $user,
//                    'module'   => $module,
//                    'teacher'  => $teacher,
//                    'last_task' => $last_task_info
//                ])->render();

            $this->master_file_one($folder, [
                'user'     => $user,
                'module'   => $module,
                'teacher'  => $teacher,
                'last_task' => $last_task_info,
                'course'    => $module->course->title
            ]);

//            $this->createFile($folder. '/masterfile01.docx', $html1);

//            $html2 = view('LwcPortal::user.module_master_file_2')
//                ->with([
//                    'user'     => $user,
//                    'module'   => $module,
//                    'los'      => $module->los()->get(),
//                    'teacher'  => $teacher,
//                    'last_task' => $last_task_info
//                ])->render();
//
//            $this->createFile($folder. '/masterfile02.docx', $html2);

            $this->master_file_two($folder, [
                'user'     => $user,
                'module'   => $module,
                'los'      => $module->los()->get(),
                'teacher'  => $teacher,
                'last_task' => $last_task_info
            ]);
//
//            $html3 = view('LwcPortal::user.module_master_file_3')
//                ->with([
//                    'user'     => $user,
//                    'module'   => $module,
//                ])->render();
//
//            $this->createFile($folder. '/masterfile03.docx', $html3);

            $this->master_file_three($folder);
//
//            $html4 = view('LwcPortal::user.module_master_file_4')
//                ->with([
//                    'user'     => $user,
//                    'module'   => $module,
//                    'tasks'    => UserTask::whereIn('task_id', $task_ids)
//                        ->where('user_id', $user_id)
//                        ->with('task')
//                        ->get()
//                ])->render();
//
            $this->master_file_four($folder, [
                    'user'     => $user,
                    'module'   => $module,
                    'tasks'    => UserTask::whereIn('task_id', $task_ids)
                        ->where('user_id', $user_id)
                        ->with('task')
                        ->get()
                ]);

            $files = storage_path('tmp/master_files/' . $folder);

            Zipper::make(storage_path("tmp/zip/{$folder}_master_files.zip"))->add($files)->close();

            if($this->folderExist(storage_path("tmp/zip/{$folder}_master_files.zip")) === true) {
                return response()->download(storage_path("tmp/zip/{$folder}_master_files.zip"));
            }
            return response(null, 404);
        }
        catch(ModelNotFoundException $e) {
            return response(null, 404);
        }
    }

    private function folderExist($path)
    {
        if(!file_exists($path)) {
            return $this->folderExist($path);
        }
        return true;
    }

    private function createFile($file_name, $content)
    {
        $path = storage_path('tmp/master_files/'.$file_name);

        $dir = dirname($path);
        if (!is_dir($dir)) {
            @mkdir($dir, 0777, true);
        }
        if (!($handle = fopen($path, 'w'))) {
            return false;
        }
        file_put_contents($path, $content);
        fclose($handle);
    }
}