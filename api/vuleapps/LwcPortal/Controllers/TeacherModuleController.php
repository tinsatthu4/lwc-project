<?php
namespace VuleApps\LwcPortal\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;
use VuleApps\LwcBackends\Models\PostMeta;
use VuleApps\LwcPortal\Models\Module;
use Auth;
use PostRepository;
class TeacherModuleController extends Controller
{
	protected function getId() {
		return Auth::user()->id;
	}
	/**
	 * @api {GET} module List Module
	 * @apiDescription Namespace : <strong>VuleApps/LwcPortal/Controllers/TeacherModuleController@index</strong>
	 * @apiGroup Teacher API
	 * @apiParam {Number} [limit=20]
	 * @apiParam {Number} [course_id=0] Filter by Course_id
	 * @apiParam {Number} [sort=id]
	 * @apiParam {Number} [order=asc]
	 * @apiParam {Number} [with-tasks=0] Get all tasks
	 * @apiParam {Number} [with-tasks-count=0] Count All Tasks. has field `tasks_count`
	 * @apiSuccess (200) {Object[]} data Module Object and Tasks inside. Module with('tasks')
	 * @apiSuccess (200) {String} links Pagination HTML String
	 * @apiSuccess (200) {Number} Total Total Record
	 */
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

		$builder = Module::hasTeacher($this->getId())
					->orderBy($request->input('sort', 'id'), $request->input('order', 'asc'));

		if($request->input('with-tasks', 0))
			$builder = $builder->with('tasks');

		if($request->input('with-tasks-count', 0))
			$builder = $builder->withCount('tasks');

		if($request->input('course_id', 0))
			$builder = $builder->hasCourse($request->input('course_id'));
		$model = $builder->paginate($request->get('limit', 20));
		return response()->json([
			'data' => $model->items(),
			'links' => (string) $model->render(),
			'total' => $model->total()
		]);
    }

	/**
	 * @api {POST} module Create a Module
	 * @apiDescription Namespace : <strong>VuleApps/LwcPortal/Controllers/TeacherModuleController@store</strong>
	 * @apiGroup Teacher API
	 * @apiParam {String} title
	 * @apiParam {Number} parent_id Course ID
  	 * @apiParam {String} [image]
	 * @apiParam {String} [slug]
	 * @apiParam {LongText} [content]
	 * @apiParam {Text} [excerpt]
	 * @apiParam {Number} [sort]
	 * @apiParam {String} [meta_title]
	 * @apiParam {String} [meta_content]
	 * @apiParam {String} [meta_keywords]
	 * @apiParam {Object[]} [postmeta=array]
	 * @apiSuccess (201) {Object} data Module Object
	 * @apiError (400) {Object} messages Validator Messages of Laravel
	 */
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
		try {
			$this->validate($request, [
				'title' => 'required',
				'parent_id' => 'required'
			]);
			$input = $request->input();
			$input['type'] = Module::TYPE;
			$input['teacher_id'] = $this->getId();

			if(isset($input['postmeta']))
			{
				$metadata = $input['postmeta'];
				unset($input['postmeta']);
			} else {
				$metadata = [];
			}
			$model = Module::create($input);
			$postMetadata = [];

			foreach($metadata as $key => $value) {
				$postMetadata[] = new PostMeta([
					'meta_key' => $key,
					'meta_value' => is_array($value)? json_encode($value) : $value
				]);
			}
			if(!empty($postMetadata))
				$model->postmeta()->saveMany($postMetadata);

			return response()
					->json([
						'data' => Module::with('postmeta')->find($model->id)
					], 201);
		} catch (ValidationException $e) {
			return response()->json([
				'messages' => $e->validator->messages()
			], 400);
		}
    }

	/**
	 * @api {GET} module/:id Get a Module
	 * @apiDescription Namespace : <strong>VuleApps/LwcPortal/Controllers/TeacherModuleController@show</strong>
	 * @apiGroup Teacher API
	 * @apiSuccess (200) {Object} data Module Object
	 * @apiError (404) {NUll} NULL Not Found Module
	 */
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		try {
			return response()->json(['data' => Module::hasTeacher($this->getId())
													->with('postmeta')
													->findOrFail($id)]);
		} catch(ModelNotFoundException $e) {
			return response(null, 404);
		}
    }

	/**
	 * @api {PUT} module/:id Update a Module
	 * @apiDescription Namespace : <strong>VuleApps/LwcPortal/Controllers/TeacherModuleController@update</strong>
	 * @apiGroup Teacher API
	 * @apiParam {String} title
	 * @apiParam {Number} parent_id Course ID
	 * @apiParam {String} [image]
	 * @apiParam {String} [slug]
	 * @apiParam {LongText} [content]
	 * @apiParam {Text} [excerpt]
	 * @apiParam {Number} [sort]
	 * @apiParam {String} [meta_title]
	 * @apiParam {String} [meta_content]
	 * @apiParam {String} [meta_keywords]
	 * @apiParam {Object[]} [postmeta=array]
	 * @apiSuccess (200) {Object} data Module Object
	 * @apiError (400) {Object} messages Validator Messages of Laravel
	 * @apiError (404) {NUll} NULL Not Found Module
	 */
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		try {
			$model = Module::hasTeacher($this->getId())
						->findOrFail($id);
			$this->validate($request, [
				'title' => 'required',
				'parent_id' => 'required'
			]);
			$input = $request->input();

			if(isset($input['postmeta']))
			{
				$metadata = $input['postmeta'];
				unset($input['postmeta']);
			} else {
				$metadata = [];
			}
			$model->fill($input)
				->save();
			$model->postmeta()->delete();
			$postMetadata = [];
			foreach($metadata as $key => $value) {
				$postMetadata[] = new PostMeta([
					'meta_key' => $key,
					'meta_value' => is_array($value)? json_encode($value) : $value
				]);
			}
			if(!empty($postMetadata))
				$model->postmeta()->saveMany($postMetadata);

			return response()
				->json([
					'data' => Module::with('postmeta')->find($model->id)
				]);
		} catch (ValidationException $e) {
			return response()->json([
				'messages' => $e->validator->messages()
			], 400);
		} catch (ModelNotFoundException $e) {
			return response(null, 404);
		}
    }

	/**
	 * @api {DELETE} post/:id Delete a Module
	 * @apiDescription Namespace : <strong>VuleApps/LwcPortal/Controllers/TeacherModuleController@destroy</strong>
	 * @apiGroup Teacher API
	 * @apiSuccess (204) {Null} Null Null Body
	 * @apiError (404) {NUll} NULL Not Found Module
	 */
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		try {
			$model = Module::hasTeacher($this->getId())
						->findOrFail($id);
			$model->delete();
			return response(null, 204);
		} catch (ModelNotFoundException $e) {
			return response(null, 404);
		}
    }
}
