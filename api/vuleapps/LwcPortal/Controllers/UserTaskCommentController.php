<?php
namespace VuleApps\LwcPortal\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Validation\ValidationException;
use VuleApps\LwcPortal\Models\UserTaskComment;
use DB;

class UserTaskCommentController extends Controller
{
    public function index(Request $request)
    {
        $model = UserTaskComment::with('user')
			->where([
				'parent_id' => $request->input('parent_id', 0)
			])
			->orderBy($request->input('sort', 'created_at'), $request->input('order', 'asc'))
			->paginate($request->input('limit', 50));

        return response()->json([
            'data'  => $model,
            'links' => (string) $model->render(),
            'total' => $model->total()
        ]);
    }

    public function indexChild(Request $request)
    {
        $model = UserTaskComment::where([
            'parent_id' => 0
        ])
            ->orderBy($request->input('sort', 'created_at'), $request->input('order', 'asc'))
            ->with('child')
            ->paginate($request->input('limit', 50));

        return response()->json([
            'data'  => $model,
            'links' => (string) $model->render(),
            'total' => $model->total()
        ]);
    }

	public function store(Request $request)
    {
        try {
            $this->validate($request, [
                'user_task_id'     => 'required|exists:user_tasks,id',
                'parent_id'        => 'required',
                'message'          => 'required'
            ]);
			$request->merge(['user_id' => Auth::user()->id]);
            $model = UserTaskComment::create($request->input());
            return response()->json([
                    'data' => $model
                ], 201);
        }
        catch(ValidationException $e) {
            return response()->json([
                'messages' => $e->validator->messages()
            ], 400);
        }
    }

	public function getByUserTaskID(Request $request, $user_task_id) {
		$model = UserTaskComment::with(['user' => function($q) {
				return $q->select(['id', 'name', 'type']);
			}])
			->where([
				'user_task_id' => $user_task_id
			])
			->orderBy('id', 'ASC')
			->paginate($request->input('limit', 50));

		return response()->json([
			'data'  => $model->items(),
			'links' => (string) $model->render(),
			'total' => $model->total()
		]);
	}
}
