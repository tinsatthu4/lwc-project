<?php
namespace VuleApps\LwcPortal\Controllers;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use Illuminate\Validation\ValidationException;
use VuleApps\Common\Present;

class AuthController extends Controller {
	function getLogin() {
		return view('LwcPortal::user.layout.login')
				->withErrors(null);
	}

	function getLogout() {
		Auth::logout();
		return redirect(route('portal.login'));
	}

	function postLogin(Request $request) {
		try {
			$this->validate($request, [
				'email' => 'required|email',
				'password' => 'required'
			]);
			$credentials = [
				'email' => $request->get('email'),
				'password' => $request->get('password'),
			];
			if(Auth::attempt($credentials, 1)) {
				$user = Auth::getUser();
				if(in_array($user->type, ['learner'])) {
					return redirect(route('portal.learner.dashboard'));
				}
				if(in_array($user->type, ['teacher_1'])) {
					return redirect()
							->to(route('portal.teacher01.dashboard') . '#/');
				}
				if(in_array($user->type, ['admin'])) {
					return redirect(route('portal.admin.dashboard'));
				}
				if(in_array($user->type, [User::TYPE_MASTER])) {
					return redirect(route('masterFile.dashboard'));
				}
				return redirect(route('portal.dashboard'));
			}

			$message = new MessageBag(['error' => 'Email Or Password incorrect']);
			return redirect()->back()
				->withErrors($message->getMessageBag())
				->withInput();

		} catch (ValidationException $e) {
			return redirect()->back()
				->withErrors($e->validator->messages())
				->withInput();
		}
	}
}