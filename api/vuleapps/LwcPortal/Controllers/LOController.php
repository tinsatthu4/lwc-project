<?php

namespace VuleApps\LwcPortal\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;
use VuleApps\LwcPortal\Models\LO;

class LOController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
		$model = LO::hasModule($request->input('module_id', 0))
					->get();
		return response()->json(['data' => $model]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		try {
			$this->validate($request, [
				'title' => 'required',
				'scenarios' => 'required',
				'module_id' => 'required|numeric'
			]);
			$lo = LO::create($request->input());

			return response()->json(['data' =>$lo], 201);

		} catch(ValidationException $e) {
			return response()->json([
				'message' => $e->validator->messages()
			], 400);
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		try {
			$lo = LO::findOrFail($id);
			return response()->json(['data' =>$lo]);
		}
		catch(ModelNotFoundException $e) {
			return response(null, 404);
		}
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
		try {
			$this->validate($request, [
				'title' => 'required',
				'scenarios' => 'required',
				'module_id' => 'required|numeric'
			]);
			$lo = LO::findOrFail($id);
			$lo->fill($request->input());
			$lo->save();
			return response()->json(['data' =>$lo]);

		}
		catch(ValidationException $e) {
			return response()->json([
				'message' => $e->validator->messages()
			], 400);
		}
		catch(ModelNotFoundException $e) {
			return response(null, 404);
		}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		try {
			$lo = LO::findOrFail($id);
			$lo->delete();
			return response(null, 204);
		} catch(ModelNotFoundException $e) {
			return response(null, 404);
		}
    }
}
