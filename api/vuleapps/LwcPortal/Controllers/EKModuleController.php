<?php
namespace VuleApps\LwcPortal\Controllers;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Request;
use VuleApps\LwcBackends\Models\EkModule;

class EKModuleController extends Controller
{
    public function show($id)
    {
        try {
            return response()->json(['data' => EkModule::findOrFail($id)]);
        } catch(ModelNotFoundException $e) {
            return response(null, 404);
        }
    }

    public function update(Request $request, $module)
    {
        try {
            $model = EkModule::findOrFail($module);
            $this->validate($request, [
                'title'      => 'required',
            ]);
            $input = $request->input();
            $model->fill($input)->save();
            return response()->json([
                'data' => $model
            ], 200);
        } catch (ValidationException $e) {
            return response()->json([
                'messages' => $e->validator->messages()
            ], 400);
        } catch (ModelNotFoundException $e) {
            return response(null, 404);
        }
    }
}
