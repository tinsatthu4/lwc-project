<?php
namespace VuleApps\LwcPortal\Controllers;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Request;
use VuleApps\LwcPortal\Models\Counrse;

class TeacherCounrseController extends Controller
{
    private function getTeacherID()
    {
        return Auth::getUser()->id;
    }

    /**
     * @api {GET} /counrse Get courses
     * @apiGroup Teacher API
     * @apiDescription Namespace: <strong>VuleApps\LwcPortal\Controllers\TeacherCounrseController@index</strong>
     * </br>
     * UnitTest: <strong>vuleappstest\LwcPortal\Controllers\TeacherCounrseControllerTest@testIndexSuccess</strong>
     * @apiPermission Teacher is Logged
     * @apiParam {number} [limit=20] Number Record
     * @apiParam {String} [s=null] Search by title
     * @apiSuccess (200) {Object[]} data Counrse with Counrse Object
     * @apiSuccess (200) {String} links HTML String pagination
     * @apiSuccess (200) {Number} total Total record
     */
    public function index(Request $request)
    {
        $teacherId = $this->getTeacherID();
        $counresOfTeacher = Counrse::where('teacher_id', $teacherId);
        $search = $request->get('s', null);
        if($search != null) {
            $counresOfTeacher = $counresOfTeacher->where('title', 'LIKE', '%'. $search .'%');
        }
        $counresOfTeacher = $counresOfTeacher->paginate($request->get('limit', 20));

        return response()->json([
            'data' => $counresOfTeacher->items(),
            'links' => (string) $counresOfTeacher->render(),
            'total' => $counresOfTeacher->total()
        ], 200);
    }


    /**
     * @api {POST} /counrse Create course by teacher
     * @apiGroup Teacher API
     * @apiDescription Namespace: <strong>VuleApps\LwcPortal\Controllers\TeacherCounrseController@store</strong>
     * </br>
     * UnitTest: <strong>vuleappstest\LwcPortal\Controllers\TeacherCounrseControllerTest@testStoreSuccess</strong>
     * @apiPermission Teacher is Logged
     * @apiParam {String} title Title Course
     * @apiSuccess (201) {Object[]} data Counrse with Counrse Object
     */
    public function store(Request $request) {
        try {
            $teacher_id = $this->getTeacherID();
            $request->merge(['teacher_id' => $teacher_id]);
            $this->validate($request, [
                'title'      => 'required',
                'teacher_id' => "required"
            ]);
            $input = $request->input();

            $input['type'] = Counrse::TYPE;
            $input['teacher_id'] = $teacher_id;
            $model = Counrse::create($input);
            $model->save();
            return response()->json([
                'data' => $model
            ], 201);
        } catch (ValidationException $e) {
            return response()->json([
                'messages' => $e->validator->messages()
            ], 400);
        }
    }

    /**
     * @api {PUT} /counrse/:counrse_id Update course by teacher
     * @apiGroup Teacher API
     * @apiDescription Namespace: <strong>VuleApps\LwcPortal\Controllers\TeacherCounrseController@update</strong>
     * </br>
     * UnitTest: <strong>vuleappstest\LwcPortal\Controllers\TeacherCounrseControllerTest@testUpdateSuccess</strong>
     * @apiPermission Teacher is Logged
     * @apiPermission Teacher has permission to access the course
     * @apiPermission Course is exist.
     * @apiParam {String} title Title Course
     * @apiSuccess (200) {Object[]} data Course with Course Object
     */
    public function update(Request $request, $counrse_id)
    {
        try {
            $request->merge(['teacher_id' => $this->getTeacherID()]);
            $model = Counrse::findOrFail($counrse_id);
            if($model->teacher_id != $this->getTeacherID()) {
                return response(null, 404);
            }
            $this->validate($request, [
                'title'      => 'required',
                'teacher_id' => "required"
            ]);
            $input = $request->input();
            unset($input['teacher_id']);
            unset($input['type']);

            $model->fill($input)->save();
            return response()->json([
                'data' => $model
            ], 200);
        } catch (ValidationException $e) {
            return response()->json([
                'messages' => $e->validator->messages()
            ], 400);
        } catch (ModelNotFoundException $e) {
            return response(null, 404);
        }
    }
}