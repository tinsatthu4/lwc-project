<?php
namespace VuleApps\LwcPortal\Controllers;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Request;
use VuleApps\LwcBackends\Models\EkLessonSession;

class EKLessonSessionController extends Controller
{
    public function index(Request $request, $lesson)
    {
        $sessions = EkLessonSession::where('lession_id', $lesson)
            ->simplePaginate($request->get('limit', 1));
        return response()->json($sessions, 200);
    }

    public function show($lesson, $session)
    {
        try {
            $model = EkLessonSession::where([
                'lession_id' => $lesson,
                'id'         => $session
              ])
              ->firstOrFail();
            return response()->json([
                'data' => $model
            ]);
        } catch(ModelNotFoundException $e) {
            return response(null, 404);
        }
    }

    public function store(Request $request, $lesson) {
        try {
            $this->validate($request, [
                'title'      => 'required',
            ]);
            $input = $request->input();
            $input['lession_id'] = $lesson;
            $model = EkLessonSession::create($input);
            $model->save();
            return response()->json([
                'data' => $model
            ], 201);
        } catch (ValidationException $e) {
            return response()->json([
                'messages' => $e->validator->messages()
            ], 400);
        }
    }

    public function update(Request $request, $lesson, $session)
    {
        try {
            $this->validate($request, [
                'title'      => 'required',
            ]);
            $model = EkLessonSession::where([
              'lession_id'  => $lesson,
              'id'          => $session
            ])
            ->firstOrFail();
            $model->fill($request->input());
            $input['lession_id'] = $lesson;
            $model->save();
            return response()->json([
                'data' => $model
            ], 200);
        } catch (ValidationException $e) {
            return response()->json([
                'messages' => $e->validator->messages()
            ], 400);
        } catch (ModelNotFoundException $e) {
            return response(null, 404);
        }
    }

    public function destroy(Request $request, $lesson, $session) {
    		try {
            $model = EkLessonSession::where([
              'lession_id'  => $lesson,
              'id'          => $session
            ])
            ->firstOrFail();
      			$model->delete();
      			return response(null, 204);
    		} catch (ModelNotFoundException $e) {
    			  return response(null, 404);
    		}
  	}
}
