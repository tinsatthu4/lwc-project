<?php
namespace VuleApps\LwcPortal\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use VuleApps\Common\Filter\Filter;
use VuleApps\LwcPortal\Models\Checkout;

class CheckoutController extends Controller
{
    public function index(Request $request)
    {
//		$model = Checkout::with('user')
//                    ->with('course')
//					->paginate($request->get('limit', 20));

        $request->merge(['with' => [
            'user', 'course'
        ]]);

        $model = (new Filter(new Checkout(), $request->input()))
            ->setStringField([
                'name'
            ])
            ->filter();

		return response()->json([
			'data' => $model->items(),
			'links' => (string) $model->render(),
			'total' => $model->total()
		]);
    }

    public function show($id)
    {
        try {
            return response()->json(['data' => Checkout::with('user')
                ->with('course')
                ->findOrFail($id)]);
        } catch(ModelNotFoundException $e) {
            return response(null, 404);
        }
    }
}
