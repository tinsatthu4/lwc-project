<?php namespace VuleApps\LwcPortal\Controllers;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Auth;
use VuleApps\LwcPortal\Exceptions\PaypalException;
use VuleApps\LwcPortal\Jobs\SendEmailJob;
use VuleApps\LwcPortal\Models\Module;
use VuleApps\LwcPortal\Models\Counrse;
use Illuminate\Validation\ValidationException;
use VuleApps\LwcPortal\Models\Checkout;
use Paypal;
use VuleApps\LwcPortal\Models\UserCounrse;
use VuleApps\LwcPortal\Models\UserModule;

class PaypalController extends Controller {

	function postMake(Request $request) {
		try {
			$user_id = Auth::user()->id;
			$this->validate($request, [
				'post_id' => 'required|exists:posts,id,type,' . Module::TYPE. '|none_exists:user_modules,post_id,user_id,' . $user_id,
			]);
			$module = Module::select('id', 'price', 'title')->find($request->input('post_id'));
			$checkout = [
				'user_id' => $user_id,
				'post_id' => $module->id,
				'price' => $module->price,
				'checkout_type' => Checkout::TYPE_MODULE
			];
			$checkout = Checkout::create($checkout);
			$returnUrl = route('paypal.redirect', [$checkout->id]);
			$cancelUrl = route('paypal.cancel', [$checkout->id]);
			$ItemName = "Checkout Module {$module->title}";
			$ItemPrice = $checkout->price;
			$ItemQty = 1;
			$ItemTotalPrice = $ItemQty*$ItemPrice;
			$redirect = Paypal::send($returnUrl, $cancelUrl, $ItemName, $ItemPrice, $ItemQty, $ItemTotalPrice);
			return response()->json(['redirect' => $redirect], 201);
		} catch (ValidationException $e) {
			return response()->json(['messages' => $e->validator->messages()], 400);
		}
	}

	function getRedirect(Request $request, $checkout_id) {
		try {
			$request->merge(['checkout_id' => $checkout_id]);
			$this->validate($request, [
				'checkout_id' => 'required|exists:checkouts,id',
				'token' => 'required',
				'PayerID' => 'required'
			]);
			$checkout = Checkout::isModule()->findOrFail($checkout_id);
			$module = Module::find($checkout->post_id);

			$ItemName = "Checkout Module {$module->title}";
			$ItemPrice = $checkout->price;
			$ItemQty = 1;
			$ItemTotalPrice = $ItemQty*$ItemPrice;
			Paypal::validate($ItemName, $ItemPrice, $ItemQty, $ItemTotalPrice);
			$checkout->payment_token = $request->input('token');
			$checkout->payper_id = $request->input('PayerID');
			$checkout->save();
			UserModule::create([
				'user_id' => $checkout->user_id,
				'post_id' => $checkout->post_id
			]);

			return redirect()->to('/#/learner-module/my-modules');
		}
		catch(ModelNotFoundException $e) {
			return response()->json(['messages' => 'checkout invalid'], 400);
		}
		catch (ValidationException $e) {
			return response()->json(['messages' => $e->validator->messages()], 400);
		}
		catch (PaypalException $e) {
			return response()->json(['messages' => $e->getMessage()], 400);
		}
	}

	function getCancel(Request $request, $checkout_id) {
		return redirect()->to('/#/learner-module/my-modules?payment=0');
	}

	function postMakeCourse(Request $request) {
		try {
			$user_id = Auth::user()->id;
			$this->validate($request, [
				'post_id' => 'required|exists:posts,id,type,' . Counrse::TYPE . '|none_exists:user_counrses,post_id,user_id,' . $user_id,
                'promotion_code' => 'in:CODE20,CODE40,code20,code40'
			]);

			$course = Counrse::find($request->input('post_id'));
			$modules = Module::select('id')->hasCourse($request->input('post_id'));

			$checkout = [
				'user_id' => $user_id,
				'post_id' => $course->id,
				'price' => $this->getPaymentPrice($course->price, $request->input('promotion_code', null)),
				'checkout_type' => Checkout::TYPE_COURSE,
				'description' => json_encode($modules->pluck('id')->all())
			];

			$checkout = Checkout::create($checkout);
			$returnUrl = route('paypal.redirect-course', [$checkout->id]);
			$cancelUrl = route('paypal.cancel', [$checkout->id]);
			$ItemName = $request->has('promotion_code') ? "{$course->title},code:{$request->input('promotion_code')}" :  "{$course->title}";
			$ItemPrice = $checkout->price;
			$ItemQty = 1;
			$ItemTotalPrice = $ItemQty*$ItemPrice;
			$redirect = Paypal::send($returnUrl, $cancelUrl, $ItemName, $ItemPrice, $ItemQty, $ItemTotalPrice);
			return response()->json(['redirect' => $redirect], 201);
		} catch (ValidationException $e) {
			return response()->json(['messages' => $e->validator->messages()], 400);
		}
	}

    private function getPaymentPrice($price, $promotion = null)
    {
        if($promotion == null) {
            return $price;
        }

        if(strtolower($promotion) == 'code20') {
            return $price - ($price * (20/100));
        }

        if(strtolower($promotion) == 'code40') {
            return $price - ($price * (40/100));
        }

        return $price;
    }

	function getRedirectCourse(Request $request, $checkout_id) {
		try {
			$request->merge(['checkout_id' => $checkout_id]);
			$this->validate($request, [
				'checkout_id' => 'required|exists:checkouts,id',
				'token' => 'required',
				'PayerID' => 'required'
			]);
			$checkout = Checkout::isCourse()->findOrFail($checkout_id);
			$course = Counrse::find($checkout->post_id);
			$moduleIds = json_decode($checkout->description, true);
			$ItemName = $course->title;
			$ItemPrice = $checkout->price;
			$ItemQty = 1;
			$ItemTotalPrice = $ItemQty*$ItemPrice;
			Paypal::validate($ItemName, $ItemPrice, $ItemQty, $ItemTotalPrice);
			$checkout->payment_token = $request->input('token');
			$checkout->payper_id = $request->input('PayerID');
			$checkout->save();
			UserCounrse::create([
				'user_id' => $checkout->user_id,
				'post_id' => $checkout->post_id
			]);

            $user = User::find($checkout->user_id);
            $this->dispatch(new SendEmailJob(
                'Payment is successful',
                config('from.address'),
                isset($user->email) ? $user->email : null,
                'email.payment_success',
                [
                    'course'    => $course,
                    'user'      => $user
                ]
            ));

			foreach($moduleIds as $id) {
				UserModule::create([
					'user_id' => $checkout->user_id,
					'post_id' => $id
				]);
			}

			return redirect()->to('/#/learner-module/my-modules?payment=1');
		}
		catch(ModelNotFoundException $e) {
			return response()->json(['messages' => 'checkout invalid'], 400);
		}
		catch (ValidationException $e) {
			return response()->json(['messages' => $e->validator->messages()], 400);
		}
		catch (PaypalException $e) {
			return response()->json(['messages' => $e->getMessage()], 400);
		}
	}
}