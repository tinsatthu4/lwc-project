<?php
namespace VuleApps\LwcPortal\Controllers;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use VuleApps\LwcPortal\Jobs\SendEmailJob;
use VuleApps\LwcPortal\Models\Task;
use VuleApps\LwcPortal\Models\Module;
use Auth;
use VuleApps\LwcBackends\Models\Post;
use VuleApps\LwcPortal\Models\Counrse;
use VuleApps\LwcPortal\Models\UserTask;
use DB;
use Log;
use VuleApps\LwcPortal\Repository\PortalRepository;
use Excel;
use VuleApps\LwcPortal\Models\UserModule;
use VuleApps\LwcPortal\Traits\SetupMF;
use Zipper;

class TeacherReviewTaskController extends Controller {

	use SetupMF;

	protected function getId() {
		return Auth::user()->id;
	}

	/**
	 * @api {PUT} /user-task/:user_task_id Update User Task
	 * @apiDescription Namespace: <strong>VuleApps\LwcPortal\Controllers\TeacherReviewTaskController@putUserTask</strong>
	 * <br/>
	 * UnitTest: <strong>vuleappstest\LwcPortal\Controllers\TeacherReviewTaskControllerTest@testPutUserTaskSuccess</strong>
	 * @apiGroup Teacher API
	 * @apiPermission Teacher is Logged
	 * @apiParam {String} teacher_comment Comment of teacher
	 * @apiParam {String} grade Only Has 4 value : pass,fail,merit,diction
	 * @apiSuccess 200 {Object} data UserTask Object
	 * @apiError 400 {Object} messages Validator Messages
	 * @apiError 404 {NULL} Null UserTask Not Found
	 */
	function putUserTask(Request $request, $user_task_id) {
		try {
			$request->merge(['user_task_id' => $user_task_id]);
			$this->validate($request, [
				'user_task_id' => 'exists:user_tasks,id',
//				'teacher_comment' => 'required',
				'grade' => 'required|in:pass,redo,merit,diction'
			]);
			$userTask = UserTask::hasTeacher($this->getId())
							->FindOrFail($user_task_id);
			$userTask->fill($request->only(['teacher_comment', 'grade']))
				->save();

            $grade = $request->input('grade');
            if($grade == 'redo') {
                UserTask::where('id',$user_task_id)->update([
                    'status' => UserTask::STATUS_DRAFT
                ]);
                $learner = User::find($userTask->user_id);
                $teacher = User::find($this->getId());
                $task = Task::find($userTask->task_id);
                $this->dispatch(new SendEmailJob(
                    'Redo Task',
                    $teacher->email,
                    $learner->email,
                    'email.redo',
                    [
                        'learner'  => $learner,
                        'teacher'  => $teacher,
                        'task'     => $task
                    ]
                ));
            }

			return response()->json(['data' => $userTask]);
		} catch(ValidationException $e) {
			return response()->json([
				'messages' => $e->validator->messages()
			], 400);
		} catch(ModelNotFoundException $e) {
			return response()->json([
				'messages' => [
					'error' => ['User Task invalid']
				]
			], 404);
		}
	}

	/**
	 * @api {GET} /user-task/group Group User Task by User And Status of Task
	 * @apiGroup Teacher API
	 * @apiDescription Namespace: <strong>VuleApps\LwcPortal\Controllers\TeacherReviewTaskController@getUserTask</strong>
	 * @apiPermission Teacher is Logged
	 * @apiParam {Number} course_id Filter by Course
	 * @apiParam {number} [limit=10] Number Record
	 * @apiParam {String} [sort=id] Field sort
	 * @apiParam {String} [order=DESC] DESC,ASC
	 * @apiSuccess (200) {Object[]} data UserTask with User Object
	 * @apiSuccess (200) {String} links HTML String pagination
	 * @apiSuccess (200) {Number} total Total record
	 * @apiSuccessExample {json} Success-Response:
	 * {"data":[{"id":5,"task_id":13,"user_id":1,"teacher_id":6,"answer":"<p>key a,key b,key c,key e<\/p>","status":"submission","grade":"none","teacher_comment":"","created_at":"2016-10-12 06:24:54","updated_at":"2016-10-12 06:25:09","count_task":27,"process":2,"user":{"id":1,"name":"learner 1"},"task":{"id":13,"title":"Task 7.4","type":"assignment"}},{"id":4,"task_id":1,"user_id":1,"teacher_id":6,"answer":"<p>key a,key b,key c,key e<\/p>","status":"submission","grade":"none","teacher_comment":"","created_at":"2016-10-12 06:24:47","updated_at":"2016-10-12 06:24:50","count_task":27,"process":2,"user":{"id":1,"name":"learner 1"},"task":{"id":1,"title":"Task 6.1","type":"assignment"}}],"links":"<ul class=\"pagination\"><li class=\"disabled\"><span>&laquo;<\/span><\/li> <li class=\"active\"><span>1<\/span><\/li><li><a href=\"http:\/\/portal.lwc.local\/teacher\/user-task\/group?page=2\">2<\/a><\/li><li><a href=\"http:\/\/portal.lwc.local\/teacher\/user-task\/group?page=3\">3<\/a><\/li> <li><a href=\"http:\/\/portal.lwc.local\/teacher\/user-task\/group?page=2\" rel=\"next\">&raquo;<\/a><\/li><\/ul>","total":5}
	 */
	function getUserTask(Request $request) {
		$teacher_id = $this->getId();
		try {
			$this->validate($request, [
				'limit' => 'integer',
				'order' => 'in:desc,asc',
				'course_id' => 'required|exists:posts,id,teacher_id,' . $teacher_id
			]);
			$builder = UserTask::hasTeacher($teacher_id)
				->hasStatus(UserTask::STATUS_SUBMISSION)
				->orderBy($request->input('sort', 'id'), $request->input('order', 'DESC'))
				->with([
					'user' => function($query) {
						$query->select('id', 'name');
					},
					'task' => function($query) {
						$query->select('id', 'title', 'type');
					}
				]);

			$course_id = $request->input('course_id');
			$module_ids = Module::where('parent_id', $course_id)
				->select('id')
				->get()
				->pluck('id')->all();

			$builder = $builder->whereHas('task', function($query) use ($module_ids) {
				$query->whereIn('post_id', $module_ids);
			});

			$model = $builder->paginate($request->input('limit', 10));

			$process[] = [];
			$taskIDs = app(PortalRepository::class)->groupTaskIDsByCourse($course_id);
			$model->each(function($item) use (&$process, $taskIDs) {
				$item->count_task = count($taskIDs);
				if(isset($process[$item->user_id]))
					$item->process = $process[$item->user_id];
				else {
					$process[$item->user_id] = UserTask::where([
													'user_id' => $item->user_id,
													'status' => UserTask::STATUS_SUBMISSION
												])
												->whereIn('task_id', $taskIDs)
												->count();
					$item->process = $process[$item->user_id];
				}
			});
			return response()->json([
				'data' => $model->items(),
				'links' => (string) $model->render(),
				'total' => $model->total()
			], 200);
		} catch (ValidationException $e) {
			return response()->json([
				'data' => [],
				'links' => "",
				'total' => 0
			], 200);
		}
	}

	function getUserTaskByUserId(Request $request, $id) {
		$teacher_id = $this->getId();
		try {
			$this->validate($request, [
				'limit' => 'integer',
				'order' => 'in:desc,asc',
				// 'course_id' => 'required|exists:posts,id,teacher_id,' . $teacher_id
			]);
			$builder = UserTask::hasTeacher($teacher_id)
				->where('user_id', $id)
				->hasStatus(UserTask::STATUS_SUBMISSION)
				->orderBy($request->input('sort', 'id'), $request->input('order', 'DESC'))
				->with([
					'user' => function($query) {
						$query->select('id', 'name');
					},
					'task' => function($query) {
						$query->select('id', 'title', 'type', 'question', 'lo_id')
								->with('lo');
					}
				]);

			$course_id = $request->input('course_id');
			$module_ids = Module::where('parent_id', $course_id)
				->select('id')
				->get()
				->pluck('id')->all();

			$builder = $builder->whereHas('task', function($query) use ($module_ids) {
				$query->whereIn('post_id', $module_ids);
			});

			$model = $builder->paginate($request->input('limit', 10));
			//
			// $process[] = [];
			// $taskIDs = app(PortalRepository::class)->groupTaskIDsByCourse($course_id);
			// $model->each(function($item) use (&$process, $taskIDs) {
			// 	$item->count_task = count($taskIDs);
			// 	if(isset($process[$item->user_id]))
			// 		$item->process = $process[$item->user_id];
			// 	else {
			// 		$process[$item->user_id] = UserTask::where([
			// 										'user_id' => $item->user_id,
			// 										'status' => UserTask::STATUS_SUBMISSION
			// 									])
			// 									->whereIn('task_id', $taskIDs)
			// 									->count();
			// 		$item->process = $process[$item->user_id];
			// 	}
			// });
			return response()->json([
				'data' => $model->items(),
				'user' => User::find($id),
				'links' => (string) $model->render(),
				'total' => $model->total()
			], 200);
		} catch (ValidationException $e) {
			return response()->json([
				'data' => [],
				'links' => "",
				'total' => 0
			], 200);
		}
	}

	function getGroupUserTask(Request $request) {
		$teacher_id = $this->getId();
		try {
			$this->validate($request, [
				'limit' => 'integer',
				'order' => 'in:desc,asc',
				// 'course_id' => 'required|exists:posts,id,teacher_id,' . $teacher_id
			]);
			
			$builder = UserTask::select('user_id')
				->hasTeacher($teacher_id)
				->hasStatus(UserTask::STATUS_SUBMISSION)
				->orderBy($request->input('sort', 'id'), $request->input('order', 'DESC'))
				->with([
					'user' => function($query) {
						$query->select('id', 'name');
					}
				]);

			$course_id = $request->input('course_id');
			$module_ids = Module::where('parent_id', $course_id)
				->select('id')
				->get()
				->pluck('id')->all();

			$builder = $builder->whereHas('task', function($query) use ($module_ids) {
				$query->whereIn('post_id', $module_ids);
			});

			$model = $builder
				->groupBy('user_id')
				->paginate($request->input('limit', 10));

			$process[] = [];
			$taskIDs = app(PortalRepository::class)->groupTaskIDsByCourse($course_id);
			$model->each(function($item) use (&$process, $taskIDs) {
				$item->count_task = count($taskIDs);
				if(isset($process[$item->user_id]))
					$item->process = $process[$item->user_id];
				else {
					$process[$item->user_id] = UserTask::where([
													'user_id' => $item->user_id,
													'status' => UserTask::STATUS_SUBMISSION
												])
												->whereIn('task_id', $taskIDs)
												->count();
					$item->process = $process[$item->user_id];
				}
			});
			return response()->json([
				'data' => $model->items(),
				'links' => (string) $model->render(),
				'total' => $model->total()
			], 200);
		} catch (ValidationException $e) {
			return response()->json([
				'data' => [],
				'links' => "",
				'total' => 0
			], 200);
		}
	}

	/**
	 * @api {GET} /user-task/:user_task_id Get User Task
	 * @apiGroup Teacher API
	 * @apiDescription Namespace: <strong>VuleApps\LwcPortal\Controllers\TeacherReviewTaskController@getUserTaskById</strong>
	 * @apiPermission Teacher is Logged
	 * @apiSuccess (200) {Object} data User Task Object
	 * @apiError (404) {Null} Null Record Not Found
	 */
	function getUserTaskById(Request $request, $id) {
		try {
			$teacher_id = $this->getId();
			$userTask = UserTask::hasTeacher($teacher_id)
				->with('task')
				->findOrFail($id);

			return response(['data' => $userTask]);
		} catch(ModelNotFoundException $e) {
			return response(null, 404);
		}
	}

	/**
	 * @api {GET} /my/modules/:course_id My Modules
	 * @apiGroup Teacher API
	 * @apiDescription Namespace: <strong>VuleApps\LwcPortal\Controllers\TeacherReviewTaskController@getMyModules</strong>
	 * @apiPermission Teacher is Logged
	 * @apiParam {number} [limit=10] Number Record
	 * @apiParam {String} [sort=id] Field sort
	 * @apiParam {String} [order=DESC] DESC,ASC
	 * @apiSuccess (200) {Object[]} data Course Object
	 * @apiSuccess (200) {String} links HTML String pagination
	 * @apiSuccess (200) {Number} total Total record
	 * @apiError 400 {Object} messages Validator Messages
	 */
	function getMyModules(Request $request, $course_id) {
		$teacher_id = $this->getId();
		$request->merge(['course_id' => $course_id]);
		try {
			$this->validate($request, [
				'course_id' => 'exists:posts,id,type,counrse|exists:course_teachers,post_id,user_id,' . $teacher_id
			]);
			$model = Module::hasTeacher($teacher_id)
				->orderBy($request->input('sort', 'id'), $request->input('order', 'DESC'))
				->paginate($request->input('limit', 10));
			return response()->json([
				'data' => $model->items(),
				'links' => (string) $model->render(),
				'total' => $model->total()
			], 200);
		} catch(ValidationException $e) {
			return response()->json(['messages' => $e->validator->messages()], 400);
		}
	}

	/**
	 * @api {GET} /my/course My Courses
	 * @apiGroup Teacher API
	 * @apiDescription Namespace: <strong>VuleApps\LwcPortal\Controllers\TeacherReviewTaskController@getMyCourses</strong>
	 * @apiPermission Teacher is Logged
	 * @apiParam {number} [limit=10] Number Record. Min: 10 Max: 50
	 * @apiParam {String} [sort=id] Field sort
	 * @apiParam {String} [order=DESC] DESC,ASC
	 * @apiSuccess (200) {Object[]} data Course Object
	 * @apiSuccess (200) {String} links HTML String pagination
	 * @apiSuccess (200) {Number} total Total record
	 * @apiError 400 {Object} messages Validator Messages
	 */
	function getMyCourses(Request $request) {
		$teacher_id = $this->getId();
		try {
			$this->validate($request, [
				'limit' => 'integer|min:10|max:50',
			]);
			$model = Counrse::whereHas('teacher', function($query) use ($teacher_id) {
					$query->where('user_id', $teacher_id);
				})
				->orderBy($request->input('sort', 'id'), $request->input('order', 'DESC'))
				->paginate($request->input('limit', 10));
			return response()->json([
				'data' => $model->items(),
				'links' => (string) $model->render(),
				'total' => $model->total()
			], 200);
		} catch(ValidationException $e) {
			return response()->json(['messages' => $e->validator->messages()], 400);
		}
	}

	function downloadUserTaskByUserId(Request $request, $id) {
		$teacher_id = $this->getId();
		$user = User::findOrFail($id);
		try {
			$this->validate($request, [
				'limit' => 'integer',
				'order' => 'in:desc,asc',
				// 'course_id' => 'required|exists:posts,id,teacher_id,' . $teacher_id
			]);
			$builder = UserTask::hasTeacher($teacher_id)
				->where('user_id', $id)
				->hasStatus(UserTask::STATUS_SUBMISSION)
				->orderBy($request->input('sort', 'id'), $request->input('order', 'DESC'))
				->with([
					// 'user' => function($query) {
					// 	$query->select('id', 'name');
					// },
					'task' => function($query) {
						$query->select('id', 'title', 'type', 'question', 'lo_id')
								->with('lo');
					}
				]);

			$course_id = $request->input('course_id');
			$course = Counrse::findOrFail($course_id);
			$module_ids = Module::where('parent_id', $course_id)
				->select('id')
				->get()
				->pluck('id')->all();

			$builder = $builder->whereHas('task', function($query) use ($module_ids) {
				$query->whereIn('post_id', $module_ids);
			});

			$model = $builder->get();
			$results = $model->toArray();

			$exports[] = [
					'Lo Title',
					'Lo Scenarios',
					'Task Title',
					'Question',
					'Teacher Comment',
					'Grade'
			];
			foreach ($results as $key => $value) {
					$exports[] = [
							!empty($value['task']['lo']['title']) ? $value['task']['lo']['title'] : null,
							!empty($value['task']['lo']['scenarios']) ? $value['task']['lo']['scenarios'] : null,
							!empty($value['task']['title']) ? $value['task']['title'] : null,
							!empty($value['task']['question']) ? $value['task']['question'] : null,
							!empty($value['teacher_comment']) ? $value['teacher_comment'] : null,
							!empty($value['grade']) ? $value['grade'] : null,
					];
			}
			Excel::create($course->title . ' - Tasks of ' . $user->name, function($excel) use ($exports, $course_id) {
					$excel->sheet('Tasks', function($sheet) use ($exports) {
							$sheet->fromArray($exports, null, 'A1', false, false);
					});
			})->download('xls');

		} catch (ValidationException $e) {
			dd($e->getMessage());
			Log::info($e->getMessage());
			return false;
		} catch (\Exception $e) {
			Log::info($e->getMessage());
			dd($e->getMessage());
			return false;
		}
	}

    function getFinalAssignmentSubmissionByCounrse(Request $request, $counrseID) {
        $userModule = UserModule::where("is_final_file", 1)
            ->orderBy("id", "ASC")
            ->whereHas("module", function ($q) use ($counrseID) {
                return $q->where("parent_id", $counrseID);
            })
            ->with("module", "user")
            ->paginate(50);

        return $userModule;
    }

    function getDownloadFinalAssignmentSubmission(Request $request, $userModuleId) {
        try {
            $teacher_id = $this->getId();
            $userModule =  UserModule::where("is_final_file", 1)
                ->findOrFail($userModuleId);
            $unid = uniqid();
            mkdir(storage_path("final-assignment-submission/{$unid}"));

            $excelTaskSubmission = "Tasks Submission Files " . $unid;

            $builder = UserTask::hasTeacher($teacher_id)
                ->where('user_id', $userModule->user_id)
                ->hasStatus(UserTask::STATUS_SUBMISSION)
				->orderBy($request->input('sort', 'id'), $request->input('order', 'DESC'))
                ->with([
                    'task' => function($query) {
                        $query->select('id', 'title', 'type', 'question', 'lo_id', 'post_id')
							->with([
								'lo'
							]);
					},
					'user',
					'teacher'
                ])
                ->whereHas('task', function($query) use ($userModule) {
                    $query->where('post_id', $userModule->post_id);
                });
            $model = $builder->get();
			$results = $model->toArray();

            $exports[] = [
                'Lo Title',
                'Lo Scenarios',
                'Task Title',
                'Question',
                'Teacher Comment',
                'Grade'
            ];
            foreach ($results as $key => $value) {
                $exports[] = [
                    !empty($value['task']['lo']['title']) ? $value['task']['lo']['title'] : null,
                    !empty($value['task']['lo']['scenarios']) ? $value['task']['lo']['scenarios'] : null,
                    !empty($value['task']['title']) ? $value['task']['title'] : null,
                    !empty($value['task']['question']) ? $value['task']['question'] : null,
                    !empty($value['teacher_comment']) ? $value['teacher_comment'] : null,
                    !empty($value['grade']) ? $value['grade'] : null,
                ];
            }

            Excel::create($excelTaskSubmission, function($excel) use ($exports) {
                $excel->sheet('Tasks', function($sheet) use ($exports) {
                    $sheet->fromArray($exports, null, 'A1', false, false);
                });
			})->store('xls', storage_path("final-assignment-submission/{$unid}/"));

			$module = Module::find($userModule->post_id);
			$this->master_file(storage_path("final-assignment-submission/{$unid}"), [
				'tasks'		=> $results,
				'learner'	=> User::find($userModule->user_id),
				'teacher'	=> User::find($teacher_id),
				'module'	=> Module::find($userModule->post_id),
				'course'	=> Counrse::find($module->parent_id)
			]);

            Zipper::make(storage_path("final-assignment-submission/{$unid}.zip"))->add([
                storage_path("final-assignment-submission/{$unid}/{$excelTaskSubmission}.xls"),
				storage_path($userModule->final_file),
				storage_path("final-assignment-submission/{$unid}/master-file.docx"),
			])->close();

            return response()->download(storage_path("final-assignment-submission/{$unid}.zip"));
        } catch (ModelNotFoundException $e) {
            return response(null ,404);
        }
	}
	
	private function getDataForMasterFile() {
		return [];
	}

}
