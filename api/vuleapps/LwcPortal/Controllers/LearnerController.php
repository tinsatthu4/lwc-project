<?php
namespace VuleApps\LwcPortal\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Log;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use VuleApps\LwcPortal\Models\Counrse;
use VuleApps\LwcPortal\Models\MultiChoiceQuestion;
use VuleApps\LwcPortal\Models\Task;
use VuleApps\LwcPortal\Models\UserCounrse;
use VuleApps\LwcPortal\Models\UserModule;
use VuleApps\LwcPortal\Models\UserTask;
use VuleApps\LwcPortal\Models\Work;
use VuleApps\LwcPortal\Models\Activity;
use VuleApps\LwcPortal\Models\Module;
use VuleApps\LwcPortal\Models\TmpUserPaymentsMethod;

class LearnerController extends Controller {

	protected function getUserID()
	{
		return Auth::user()->id;
	}

	function index() {
		return view("LwcPortal::user.dashboard_learner");
	}

	/**
	 * @api {GET} user-task/:task_id Get User Task
	 * @apiGroup Learner
	 * @apiDescription Namespace VuleApps\LwcPortal\Controllers\LearnerController@getUserTask
	 * @apiParam {Number} [with-task=0] With Task Object
	 * @apiSuccess (200) {NULL} Null User Task Object
	 * @apiError (400) {Object} messages Validator Message Laravel
	 */
	/**
	 * @param Request $request
	 * @param $task_id
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function getUserTask(Request $request, $task_id)
	{
		try {
			$userID = $this->getUserID();
			$task_id = intval($task_id);
			$request->merge(['user_id' => $userID, 'task_id' => $task_id]);
			$this->validate($request, [
				'task_id' => 'required|exists:tasks,id',
				'user_id'	=> 'has_policy_task:' . $task_id
			]);

			$builder = UserTask::where([
				'task_id' => $task_id,
				'user_id' => $userID
			]);

			if($request->input('with-task', 0)) {
				$builder = $builder->with('task');
			}

			$userTask = $builder->firstOrFail();

			return response()->json([
				'data' => $userTask
			], 200);
		}
		catch (ValidationException $e) {
			return response()->json([
				'messages' => $e->validator->messages()
			], 400);
		}
		catch(ModelNotFoundException $e) {
			$task = Task::find($request->get('task_id'));
			$input = [
				'task_id' => $task_id,
				'user_id' => $this->getUserID(),
				'teacher_id' => $task->teacher_id
			];
			$model = UserTask::create($input);

			if($request->input('with-task', 0)) {
				$model = UserTask::with('task')->find($model->id);
			} else $model = UserTask::find($model->id);

			return response()->json([
				'data' => $model
			], 200);
		}
	}

	/**
	 * @api {GET} courses All Course Learner
	 * @apiGroup Learner
	 * @apiDescription Namespace : <strong>VuleApps\LwcPortal\Controllers\LearnerController@getCourses</strong>
	 * @apiParam {String} [page=1]
	 * @apiParam {String} [limit=50]
	 */
	/**
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function getCourses(Request $request) {
		$learner_id = $this->getUserID();
		$model = Counrse::whereHas('user', function($query) use ($learner_id) {
			$query->where('user_id', $learner_id);
		})->paginate($request->input('limit', 50));

		return response()->json([
			'data' => $model->items(),
			'links' => (string) $model->render(),
			'total' => $model->total()
		]);
	}

	public function getAllCourse(Request $request) {
		try {
			$userId = Auth::user()->id;
			$this->validate($request, [
				'limit' => 'digits_between:10,100'
			]);
			$allCourse = Counrse::with(['modules', 'userCourse' => function($q) use ($userId) {
					$q->where('user_id', $userId);

				}])
				->take(50)
				->get();

			return response()->json([
				'data' => $allCourse,
				'total' => 0,
				'links' => null,
			]);
		} catch (ValidationException $e) {
			return response()->json(['data' => [], 'total' => 0, 'links' => null]);
		}
	}

	/**
	 * @api {POST} user-task/:task_id/submission Submission Assignment Task
	 * @apiGroup Learner
	 * @apiDescription Namespace <strong>VuleApps\LwcPortal\Controllers\LearnerController@submission(Request $request, $task_id)</strong>
	 * @apiSuccess (200) {Object} data
	 * @apiError (400) {Object} messages Validator Messages
	 * @apiError (404){Null} Null Not Found
	 */
	public function submission(Request $request, $task_id)
	{
		try {
			$userID = $this->getUserID();
			$model = UserTask::where([
				'task_id' => $task_id,
				'user_id' => $userID
			])->firstOrFail();

			$request->merge(['user_id' => $userID, 'task_id' => $task_id]);
			$this->validate($request, [
				// 'answer'	=> 'required|match_task_keyword:'. $model->task_id
				// 'answer'	=> 'required'
			]);

			$input = $request->input();
			$input['status'] = 'submission';
			$model->fill($input)->save();

			return response()->json([
				'data' => $model
			]);
		} catch (ValidationException $e) {
			return response()->json([
				'messages' => $e->validator->messages()
			], 400);
		} catch (ModelNotFoundException $e) {
			return response(null, 404);
		}
	}

	/**
	 * @api {POST} user-task/:task_id/save Save Assignment Task
	 * @apiDescription VuleApps\LwcPortal\Controllers\LearnerController@save
	 * @apiGroup Learner
	 * @apiSuccess (200) {Object} data
	 * @apiError (400) {Object} messages Validator Messages
	 * @apiError (404){Null} Null Not Found
	 */
	public function save(Request $request, $task_id)
	{
		try {
			$userID = $this->getUserID();
			$model = UserTask::where([
				'task_id' => $task_id,
				'user_id' => $userID
			])->firstOrFail();

			$input = $request->input();
			$input['status'] = 'draft';
			$model->fill($input)->save();

			return response()->json([
				'data' => $model
			]);
		} catch (ModelNotFoundException $e) {
			return response(null, 404);
		}
	}

	/**
	 * @api {GET} mcq/:task_id Get Mcq
	 * @apiGroup Learner
	 * @apiDescription Namespace <strong>VuleApps\LwcPortal\Controllers\LearnerController@getMcq</strong><br/>
	 * UnitTest <strong>VuleAppsTest\LWcPortal\Controllers\LearnerControllerTest@testGetMcq</strong>
	 * @apiSuccess (200) {Object[]} data Collection MCQ
	 * @apiError (400) {Object[]} messages Validator Messages
	 */
	public function getMcq(Request $request, $task_id) {
		try {
			$request->merge([
				'task_id' => $task_id,
				'user_id' => $this->getUserID()
			]);

			$this->validate($request, [
				'task_id' => 'required|exists:tasks,id,type,' . Task::TYPE_MCQ,
				'user_id' => 'exists:user_tasks,user_id,task_id,' . $task_id,
			]);

			$mcqs = MultiChoiceQuestion::where(['task_id' => $task_id])
				->select(["id", "title", "options", "metadata", "task_id"])
				->get();
			return response()->json(['data' => $mcqs]);
		} catch (ValidationException $e) {
			return response()->json(['messages' => $e->validator->messages()], 400);
		}
	}

	/**
	 * @api {POST} mcq-save/:task_id Save Mcq Task
	 * @apiDescription Namespace <strong>VuleApps\LwcPortal\Controllers\LearnerController@postMcqSave</strong>
	 * TestCase <strong>VuleAppsTest\LWcPortal\Controllers\LearnerControllerTest@testPostMcqSaveSuccess</strong>
	 * @apiGroup Learner
	 * @apiParam {Object[]} answer Answer of User
	 * @apiSuccess (200) {Object} data User Task Object
	 * @apiError (400) {Object} messages Validator Message
	 * @apiError (404) {Null} Null Not found user Task
	 */
	/**
	 * @param Request $request
	 * @param $task_id
	 */
	public function postMcqSave(Request $request, $task_id) {
		try {
			$learner_id = $this->getUserID();
			$request->merge([
				'task_id' => $task_id,
				'user_id' => $learner_id
			]);
			$this->validate($request, [
				'task_id' => 'required|exists:tasks,id',
				'user_id' => 'exists:user_tasks,user_id,task_id,' . $task_id,
				'answer' => 'required|array|answer_has_format'
			]);
			$userTask = UserTask::where(['user_id' => $learner_id, 'task_id' => $task_id, 'status' => UserTask::STATUS_DRAFT])
				->firstOrFail();
			$input['answer'] = json_encode($request->input('answer'));
			$userTask->fill($input);
			$userTask->save();
			return response()->json(['data' => $userTask]);
		} catch(ValidationException $e) {
			return response()->json(['messages' => $e->validator->messages()], 400);
		} catch(ModelNotFoundException $e) {
			return response(null, 404);
		}
	}

	/**
	 * @api {POST} mcq-check/:task_id Check Mcq Answers
	 * @apiDescription Namespace <strong>VuleApps\LwcPortal\Controllers\LearnerController@postMcqCheck</strong>
	 * @apiGroup Learner
	 * @apiParam {Object[]} answer Answer of User
	 * @apiSuccess (200) {Object} data User Task Object
	 * @apiSuccess (200) {Number} data.match Total match
	 * @apiSuccess (200) {Number} data.total Total Option
	 * @apiError (400) {Object} messages Validator Message
	 */
	public function postMcqCheck(Request $request, $task_id) {
		try {
			$learner_id = $this->getUserID();
			$request->merge([
				'task_id' => $task_id,
				'user_id' => $learner_id
			]);
			$this->validate($request, [
				'task_id' => 'required|exists:tasks,id',
				'user_id' => 'exists:user_tasks,user_id,task_id,' . $task_id,
				'answer' => 'required|array|answer_has_format'
			]);
			$mcqs = MultiChoiceQuestion::where(['task_id' => $task_id])
				->select(["id", "answer"])
				->get();
			$mcqOptions = $mcqs->pluck('answer', 'id')->all();
			$answers = $request->input('answer');
			$rs = $this->checkMatchMcqAndAnswer($mcqOptions, $answers);
			return response()->json(['data' => $rs]);
		} catch(ValidationException $e) {
			return response()->json(['messages' => $e->validator->messages()], 400);
		} catch(ModelNotFoundException $e) {
			return response(null, 404);
		}
	}

	/**
	 * @api {POST} mcq-submission/:task_id Submission Mcq Task
	 * @apiDescription Namespace <strong>VuleApps\LwcPortal\Controllers\LearnerController@postMcqSubmission</strong>
	 * TestCase <strong>VuleAppsTest\LWcPortal\Controllers\LearnerControllerTest@testPostMcqSubmissionSuccess</strong>
	 * TestCase <strong>VuleAppsTest\LWcPortal\Controllers\LearnerControllerTest@testPostMcqSubmissionFailAnswer</strong>
	 * TestCase <strong>VuleAppsTest\LWcPortal\Controllers\LearnerControllerTest@testPostMcqSubmissionFailInput</strong>
	 * @apiGroup Learner
	 * @apiParam {Object[]} answer Answer of User
	 * @apiSuccess (200) {Object} data User Task Object
	 * @apiError (400) {Object} messages Validator Message
	 * @apiError (400) {Object} error Answer Fail
	 * @apiError (404) {Null} Null Not found user Task
	 */
	public function postMcqSubmission(Request $request, $task_id) {
		try {
			$learner_id = $this->getUserID();
			$request->merge([
				'task_id' => $task_id,
				'user_id' => $learner_id
			]);
			$this->validate($request, [
				'task_id' => 'required|exists:tasks,id',
				'user_id' => 'exists:user_tasks,user_id,task_id,' . $task_id,
				'answer' => 'required|array|answer_has_format'
			]);
			$userTask = UserTask::where(['user_id' => $learner_id, 'task_id' => $task_id, 'status' => UserTask::STATUS_DRAFT])
				->firstOrFail();
			$mcqs = MultiChoiceQuestion::where(['task_id' => $task_id])
				->select(["id", "answer"])
				->get();
			$mcqOptions = $mcqs->pluck('answer', 'id')->all();
			$answers = $request->input('answer');
			$rs = $this->checkMatchMcqAndAnswer($mcqOptions, $answers);
			if($rs['check'] == false)
				return response()->json(['error' => true], 400);

			$input['answer'] = json_encode($request->input('answer'));
			$input['status'] = UserTask::STATUS_SUBMISSION;
			$userTask->fill($input);
			$userTask->save();

			return response()->json(['data' => $userTask]);
		} catch(ValidationException $e) {
			return response()->json(['messages' => $e->validator->messages()], 400);
		} catch(ModelNotFoundException $e) {
			return response(null, 404);
		}
	}

	/**
	 * @param $mcqOptions
	 * @param $answers
	 * $mcqOptions = [
	 *  $mcq_id => $mcq_answer
	 * ]
	 * $answer = [
	 * 	$mcq_id => 'answer'
	 * ]
	 * @return Array [match, total, check]
	 * match : Total answer match
	 * total : Total question
	 * check : Boolean True Or False
	 */
	protected function checkMatchMcqAndAnswer($mcqOptions, $answers) {
		$total = count($mcqOptions);
		$match = 0;
		foreach($answers as $mcq_id => $answer) {
			if(strcmp($answer, $mcqOptions[$mcq_id]) === 0) {
				$match++;
			}
		}
		return [
			'match' => $match,
			'total' => $total,
			'check' => ($total == $match ? true : false)
		];
	}

	/**
	 * @api {GET} learner/work List Work
	 * @apiGroup Learner Work
	 * @apiSuccessExample {json} Response:
	 * {"data":[{"end_date":"2016-10-30 00:00:00","id":1,"start_date":"2016-10-10 00:00:00","todo":"Todo 1","user_id":1},{"end_date":"2016-10-30 00:00:00","id":2,"start_date":"2016-10-10 00:00:00","todo":"Todo 2","user_id":1}],"links":"","total":2}
	 */
	public function getIndexWork(Request $request) {
		try {
			$this->validate($request, [
				'limit' => 'digits_between:1,100',
				'sort' => 'in:asc,desc,ASC,DESC'
			]);
			$user_id = Auth::user()->id;
			$models = Work::user($user_id)
				->take($request->input('limit', 50))
				->orderBy($request->input('order', 'id'), $request->input('sort', 'asc'))
				->paginate();
			return response()->json([
				'data' => $models->items(),
				'total' => $models->total(),
				'links' => (string) $models->links()
			]);
		} catch (ValidationException $e) {
			return response()->json(['data' => [], 'total' => 0, 'links' => null]);
		}
	}

	/**
	 * @api {GET} learner/work/:id Get Work
	 * @apiGroup Learner Work
	 * @apiError (404) {Null} Null Not Found
	 * @apiSuccessExample {json} Response:
	 * {"data":{"id":1,"end_date":"2016-10-30 00:00:00","start_date":"2016-10-10 00:00:00","todo":"Todo 1","user_id":1}}
	 */
	public function getShowWork(Request $request, $id) {
		try {
			$user_id = Auth::user()->id;
			$work = Work::user($user_id)->findOrFail($id);
			return response()->json(['data' => $work]);
		} catch (ModelNotFoundException $e) {
			return response(null, 404);
		}
	}

	/**
	 * @api {POST} learner/work Create Work
	 * @apiGroup Learner Work
	 * @apiDescription VuleApps\LwcPortal\Controllers\LearnerController@postStoreWork
	 * @apiParamExample {json} Request:
	 * {"todo":"Do Home Work","start_date":"2016-10-01","end_date":"2016-10-02"}
	 * @apiSuccess (201) {Object} data
	 * @apiError (400) {Object} messages
	 * @apiErrorExample {json} Response:
	 * {"messages":{"todo":["The todo field is required."],"start_date":["The start date does not match the format Y-m-d."],"end_date":["The end date does not match the format Y-m-d."]}}
	 * @apiSuccessExample {json} Response:
	 * {"data":{"todo":"Do Home Work","start_date":"2016-10-01 00:00:00","end_date":"2016-10-02 00:00:00","user_id":1,"id":4}}
	 */
	/**
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function postStoreWork(Request $request) {
		try {
			$this->validate($request, [
				'todo' => 'required',
				'start_date' => 'date|date_format:Y-m-d',
				'end_date' => 'date|date_format:Y-m-d'
			]);
			$user_id = Auth::user()->id;
			$request->merge(['user_id' => $user_id]);
			$work = Work::create($request->input());
			return response()->json(['data' => Work::find($work->id)], 201);
		} catch (ValidationException $e) {
			return response()->json(['messages' => $e->validator->messages()], 400);
		}
	}

	/**
	 * @api {PUT} learner/work/:id Update Work
	 * @apiGroup Learner Work
	 * @apiDescription VuleApps\LwcPortal\Controllers\LearnerController@putUpdateWork
	 * @apiParamExample {json} Request:
	 * {"todo":"Do Home Work","start_date":"2016-10-01","end_date":"2016-10-02"}
	 * @apiSuccess (200) {Object} data
	 * @apiError (400) {Object} messages
	 * @apiErrorExample {json} Response:
	 * {"messages":{"todo":["The todo field is required."],"start_date":["The start date does not match the format Y-m-d."],"end_date":["The end date does not match the format Y-m-d."]}}
	 * @apiSuccessExample {json} Response:
	 * {"data":{"todo":"Do Home Work","start_date":"2016-10-01 00:00:00","end_date":"2016-10-02 00:00:00","user_id":1,"id":1}}
	 */
	public function putUpdateWork(Request $request, $id) {
		try {
			$this->validate($request, [
				'todo' => 'required',
				'start_date' => 'date|date_format:Y-m-d',
				'end_date' => 'date|date_format:Y-m-d'
			]);

			$user_id = Auth::user()->id;
			$work = Work::user($user_id)->findOrFail($id);
			$work->fill($request->only(['todo', 'start_date', 'end_date']));
			$work->save();
			return response()->json(['data' => $work]);
		} catch (ValidationException $e) {
			return response()->json(['messages' => $e->validator->messages()], 400);
		} catch (ModelNotFoundException $e) {
			return response(null, 404);
		}
	}

	/**
	 * @api {DELETE} learner/work/:id Delete Work
	 * @apiGroup Learner Work
	 * @apiDescription VuleApps\LwcPortal\Controllers\LearnerController@deleteDestroyWork
	 * @apiSuccess (204) {NULL} Null
	 * @apiError (204) {Null} Null Work Not Found
	 */
	public function deleteDestroyWork(Request $request, $id) {
		try {
			$user_id = Auth::user()->id;
			$work = Work::user($user_id)->findOrFail($id);
			$work->delete();
			return response(null, 204);
		} catch (ModelNotFoundException $e) {
			return response(null, 404);
		}
	}

	public function getModule(Request $request, $id) {
		try {
			$user_id = $this->getUserID();
			$module = Module::validTimeStart(date('Y-m-d'))
				->whereHas('userModules', function($q) use ($user_id) {
					$q->where('user_id', $user_id);
				})
                ->with([
                    "userModule" => function($q) use ($user_id) {
                        return $q->where('user_id', $user_id);
                    }
                ])
                ->findOrFail($id);
			return response()->json(['data' => $module]);
		} catch (ModelNotFoundException $e) {
			return response(null, 404);
		}
	}

    public function getMyModules(Request $request) {
        try {
            $this->validate($request, [
                'limit' => 'digits_between:10,100'
            ]);
            $user = Auth::user();
            $allCourse = Counrse::select(['id', 'title'])->paginate($request->input('limit', 50));
            $allCourse->each(function($course) use ($user) {
                $course->modules = $user->modules()
                    ->select(['posts.id', 'posts.title', 'posts.status', 'posts.time_end', 'posts.time_start'])
                    ->hasCourse($course->id)->get();
                $course->modules->each(function($module) use ($user) {
                    $tasks = $module->tasks()->select('id')->get();
                    $module->count_task = $tasks->count();
                    $module->count_task_submission = UserTask::hasStatus(UserTask::STATUS_SUBMISSION)
                        ->whereIn('task_id', $tasks->pluck("id")->all())
                        ->hasUser($user->id)->count();
                });
            });

            return response()->json([
                'data' => $allCourse->items(),
                'total' => $allCourse->total(),
                'links' => (string) $allCourse->links(),
            ]);
        } catch (ValidationException $e) {
            return response()->json(['data' => [], 'total' => 0, 'links' => null]);
        }
    }

	public function getMyActivites(Request $request) {
		try {
			$user_id = Auth::user()->id;
			$this->validate($request, [
				'limit' => 'digits_between:5,50'
			]);
			$activities = Activity::hasUser($user_id)
				->orderBy('id', 'DESC')
				->paginate($request->input('limit', 50));

			return response()->json([
				'data' => $activities->items(),
				'total' => $activities->total(),
				'links' => (string) $activities->links()
			]);
		} catch (ValidationException $e) {
			return response()->json([
				'messages' => $e->validator->messages(),
				'data' => [],
				'total' => 0,
				'links' => null
			]);
		}
	}

	public function getAllTasks(Request $request, $module_id) {
		$user_id = Auth::user()->id;
		$tasks = Task::hasModule($module_id)
                    ->hasTypeOfGrade($request->input('type_of_grade', 'none'))
                    ->orderBy($request->input('sort', 'sort'), $request->input('orderby', 'ASC'))
					->with(['user_task' => function($q) use ($user_id) {
						$q->where("user_id", $user_id);
					}])
					->get();

		return response()->json([
			'data' => $tasks
		]);
	}

	public function getLearnerMetadata(Request $request) {
		$user_id = Auth::user()->id;

		$count_modules = UserModule::hasUser($user_id)->count();

		$count_courses = UserCounrse::hasUser($user_id)->count();
		return response()->json(['data' => [
			'count_modules' => $count_modules,
			'count_courses' => $count_courses
		]]);
	}

	public function postSavePaymentMethod(Request $request) {
		try {
			$this->validate($request, [
                'course_id'      => 'required',
                'payment_method' => 'required'
            ]);

			$tmpUserPaymentsMethod = TmpUserPaymentsMethod::create([
										'course_id'      => $request->course_id,
										'user_id'        => Auth::user()->id, 
										'payment_method' => $request->payment_method
									 ]);

			return response(null, 204);
		} catch (ValidationException $e) {
            return response()->json([
                'messages' => $e->validator->messages()
            ], 400);
        } catch (ModelNotFoundException $e) {
            return response(null, 404);
        }
	}

	public function postModuleFinaleFile(Request $request, $moduleId) {
        try {
            $this->validate($request, [
                "file" => "required"
            ]);

            $user = Auth::getUser();

            $userModule = UserModule::where([
                "user_id" => $user->id,
                "post_id" => $moduleId
            ])->firstOrFail();

            $file = $request->file('file');
            $uid = uniqid() . "-" . $file->getClientOriginalName();
            $file->move(storage_path("final_files/" . $user->id), $uid);

            $userModule->final_file = "final_files/{$user->id}/" . $uid;
            $userModule->is_final_file = 1;
            $userModule->save();

            return response()->json(["data" => $userModule]);
        } catch (ValidationException $e) {
            return response()->json(["messages" => $e->validator->messages()], 400);
        } catch (ModelNotFoundException $e) {
            return response(null, 404);
        }
    }
}