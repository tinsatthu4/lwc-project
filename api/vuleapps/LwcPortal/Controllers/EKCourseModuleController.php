<?php
namespace VuleApps\LwcPortal\Controllers;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Request;
use VuleApps\LwcBackends\Models\EkModule;

class EKCourseModuleController extends Controller
{
    public function index(Request $request, $course)
    {
        $modules = EkModule::where('course_id', $course)
            ->paginate($request->get('limit', 20));
        return response()->json([
            'data'  => $modules->items(),
            'links' => (string) $modules->render(),
            'total' => $modules->total()
        ], 200);
    }

    public function show($course, $module)
    {
        try {
            $model = EkModule::where([
                'course_id'  => $course,
                'id'         => $module
              ])
              ->firstOrFail();
            return response()->json([
                'data' => $model
            ]);
        } catch(ModelNotFoundException $e) {
            return response(null, 404);
        }
    }

    public function store(Request $request, $course) {
        try {
            $this->validate($request, [
                'title'      => 'required',
            ]);
            $input = $request->input();
            $input['course_id'] = $course;
            $model = EkModule::create($input);
            $model->save();
            return response()->json([
                'data' => $model
            ], 201);
        } catch (ValidationException $e) {
            return response()->json([
                'messages' => $e->validator->messages()
            ], 400);
        }
    }

    public function update(Request $request, $counrse, $module)
    {
        try {
            $this->validate($request, [
                'title'      => 'required',
            ]);
            $model = EkModule::where([
              'course_id'  => $course,
              'id'         => $module
            ])
            ->firstOrFail();
            $model->fill($request->input());
            $model->save();
            return response()->json([
                'data' => $model
            ], 200);
        } catch (ValidationException $e) {
            return response()->json([
                'messages' => $e->validator->messages()
            ], 400);
        } catch (ModelNotFoundException $e) {
            return response(null, 404);
        }
    }

    public function destroy(Request $request, $course, $module) {
    		try {
            $model = EkModule::where([
              'course_id'  => $course,
              'id'         => $module
            ])
            ->firstOrFail();
      			$model->delete();
      			return response(null, 204);
    		} catch (ModelNotFoundException $e) {
    			  return response(null, 404);
    		}
  	}
}
