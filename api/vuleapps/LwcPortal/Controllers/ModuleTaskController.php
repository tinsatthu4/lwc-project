<?php
namespace VuleApps\LwcPortal\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use VuleApps\LwcPortal\Models\Task;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ModuleTaskController extends Controller
{
	/**
	 * @api {GET} module/:module_id/task List Tasks of Module
	 * @apiDescription Namespace : <strong>VuleApps/LwcPortal/Controllers/ModuleTaskController@index</strong>
	 * @apiGroup Module Task
	 * @apiParam {Number} [limit=20]
	 * @apiParam {String} [sort=id] Field will be sort
	 * @apiParam {String} [orderby=desc] ASC or DESC
	 * @apiSuccess (200) {Object[]} data Tasks Object
	 * @apiSuccess (200) {String} links Pagination HTML String
	 * @apiSuccess (200) {Number} Total Total Record
	 */
    public function index(Request $request, $module_id)
    {
		$model = Task::where('post_id', $module_id)
					->orderBy($request->input('sort', 'id'), $request->input('orderby', 'DESC'))
					->paginate($request->input('limit', 20));
		return response()->json([
			'data' => $model->items(),
			'links' => (string) $model->render(),
			'total' => $model->total()
		]);
	}

	/**
	 * @api {POST} module/:module_id/task Create a Task of specifies Module
	 * @apiDescription Namespace : <strong>VuleApps/LwcPortal/Controllers/ModuleTaskController@store</strong>
	 * @apiGroup Module Task
	 * @apiParam {String} title
	 * @apiParam {String} type Only in `assignment,mcq`
	 * @apiParam {String} keywords Keywords of tasks.Seperate with comma. Example : key1,key2,key3
	 * @apiParam {String} preference
	 * @apiSuccess (201) {Object} data Task Object
	 * @apiError (400) {Object} messages Validator Messages of Laravel
	 */
    public function store(Request $request, $module_id)
    {
		try {
			$this->validate($request, [
				'title' => 'required',
				'type' => 'required|in:assignment,mcq',
				'keywords' => 'required',
				'preference' => 'required'
			]);
			$input = $request->input();
			$input['post_id'] = $module_id;

			$model = Task::create($input);
			return response()
				->json([
					'data' => $model
				], 201);
		} catch (ValidationException $e) {
			return response()->json([
				'messages' => $e->validator->messages()
			], 400);
		}
    }

	/**
	 * @api {GET} module/:module_id/task/:id Get a Module
	 * @apiDescription Namespace : <strong>VuleApps/LwcPortal/Controllers/ModuleTaskController@show</strong>
	 * @apiGroup Module Task
	 * @apiSuccess (200) {Object} data Task Object
	 * @apiError (404) {NUll} NULL Not Found Module
	 */
    public function show($module_id, $id)
    {
		try {
			return response()->json(['data' => Task::where('post_id', $module_id)->findOrFail($id)]);
		} catch(ModelNotFoundException $e) {
			return response(null, 404);
		}
    }

	/**
	 * @api {PUT} module/:module_id/task/:id Update a Module
	 * @apiDescription Namespace : <strong>VuleApps/LwcPortal/Controllers/ModuleTaskController@update</strong>
	 * @apiGroup Module Task
	 * @apiParam {String} title
	 * @apiParam {String} type Only in `assignment,mcq`
	 * @apiParam {String} keywords Keywords of tasks.Seperate with comma. Example : key1,key2,key3
	 * @apiParam {String} preference
	 * @apiParam {String} type Only in `assignment,mcq`
	 * @apiSuccess (200) {Object} data Task Object
	 * @apiError (400) {Object} messages Validator Messages of Laravel
	 * @apiError (404) {NUll} NULL Not Found Task
	 */
    public function update(Request $request, $module_id, $id)
    {
		try {
			$model = Task::where('post_id', $module_id)->findOrFail($id);
			$this->validate($request, [
				'title' => 'required',
				'type' => 'required|in:assignment,mcq',
				'keywords' => 'required',
				'preference' => 'required'
			]);
			$input = $request->input();

			$model->fill($input)
				->save();
			return response()
				->json([
					'data' => $model
				]);
		} catch (ValidationException $e) {
			return response()->json([
				'messages' => $e->validator->messages()
			], 400);
		} catch (ModelNotFoundException $e) {
			return response(null, 404);
		}
    }

	/**
	 * @api {DELETE} module/:module_id/task/:id Delete a Task
	 * @apiDescription Namespace : <strong>VuleApps/LwcPortal/Controllers/ModuleTaskController@destroy</strong>
	 * @apiGroup Module Task
	 * @apiSuccess (204) {Null} Null Null Body
	 * @apiError (404) {NUll} NULL Not Found Task
	 */
	public function destroy($module_id, $id)
    {
		try {
			$model = Task::where('post_id', $module_id)->findOrFail($id);
			$model->delete();
			return response(null, 204);
		} catch (ModelNotFoundException $e) {
			return response(null, 404);
		}
    }
}
