<?php
namespace VuleApps\LwcPortal\Controllers;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Request;
use VuleApps\LwcBackends\Models\EkCourse;

class EKCourseController extends Controller
{
    public function index(Request $request)
    {
        $counres = new EKCourse();
        $search = $request->get('s', null);
        if($search != null) {
            $counres = $counres->where('title', 'LIKE', '%'. $search .'%');
        }
        if($request->has('select')) {
            $counres = $counres->select($request->select);
        }
        $counres = $counres->with('modules');
        $counres = $counres->paginate($request->get('limit', 20));

        return response()->json([
            'data'  => $counres->items(),
            'links' => (string) $counres->render(),
            'total' => $counres->total()
        ], 200);
    }

    public function show($id)
    {
        try {
            return response()->json(['data' => EKCourse::findOrFail($id)]);
        } catch(ModelNotFoundException $e) {
            return response(null, 404);
        }
    }

    public function store(Request $request) {
        try {
            $this->validate($request, [
                'title'      => 'required',
            ]);
            $input = $request->input();
            $model = EKCourse::create($input);
            $model->save();
            return response()->json([
                'data' => $model
            ], 201);
        } catch (ValidationException $e) {
            return response()->json([
                'messages' => $e->validator->messages()
            ], 400);
        }
    }

    public function update(Request $request, $course)
    {
        try {
            $model = EKCourse::findOrFail($course);
            $this->validate($request, [
                'title'      => 'required',
            ]);
            $input = $request->input();
            $model->fill($input)->save();
            return response()->json([
                'data' => $model
            ], 200);
        } catch (ValidationException $e) {
            return response()->json([
                'messages' => $e->validator->messages()
            ], 400);
        } catch (ModelNotFoundException $e) {
            return response(null, 404);
        }
    }

    public function destroy(Request $request, $course) {
    		try {
      			$model = EKCourse::findOrFail($course);
      			$model->delete();
      			return response(null, 204);
    		} catch (ModelNotFoundException $e) {
    			  return response(null, 404);
    		}
  	}
}
