<?php
namespace VuleApps\LwcPortal\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Collection;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use VuleApps\LwcPortal\Models\Module;
use VuleApps\LwcPortal\Models\Task;
use VuleApps\LwcPortal\Models\UserCounrse;
use VuleApps\LwcPortal\Models\UserTask;
use VuleApps\LwcPortal\Models\UserModule;
class UserTaskController extends Controller
{
	public function getUserID()
	{
		return Auth::getUser()->id;
	}

	public function getModulesAndTasks(Request $request, $counrse_id) {
		try {
			//check user has counrse
			$user_id = $this->getUserID();
			$modules = Module::whereHas('users', function($q) use ($user_id) {
				$q->where('users.id', $user_id);
			})
						->take(50)->get();
			$task = new Collection();
			foreach($modules as $module) {
				$task = $task->merge(Task::where('post_id', $module->id)->take(50)->get());
			}

			return response()->json([
				'data' => [
					'modules' => $modules,
					'tasks' => $task
				]
			]);
		} catch(ModelNotFoundException $e) {
			return response()
				->json(['messages' => $e->getMessage()], 404);
		}
	}

//    public function getUserTask(Request $request, $task_id)
//    {
//		try {
//			$userID = $this->getUserID();
//			$task_id = intval($task_id);
//			$request->merge(['user_id' => $userID, 'task_id' => $task_id]);
//			$this->validate($request, [
//				'task_id' => 'required|exists:tasks,id',
//				'user_id'	=> 'has_policy_task:' . $task_id
//			]);
//			$userTask = UserTask::where([
//				'task_id' => $task_id,
//				'user_id' => $userID
//			])->firstOrFail();
//			return response()->json([
//				'data' => $userTask
//			], 200);
//		}
//		catch (ValidationException $e) {
//			return response()->json([
//				'messages' => $e->validator->messages()
//			], 400);
//		}
//		catch(ModelNotFoundException $e) {
//			$task = Task::find($request->get('task_id'));
//			$input = [
//				'task_id' => $task_id,
//				'user_id' => $this->getUserID(),
//				'teacher_id' => $task->teacher_id
//			];
//			$model = UserTask::create($input);
//			$model = UserTask::find($model->id);
//			return response()->json([
//				'data' => $model
//			], 200);
//		}
//	}

	public function storeUserTask(Request $request)
	{
		try {
			$userID = $this->getUserID();
			$request->merge(['user_id' => $userID]);
			$this->validate($request, [
				'task_id'	=> 'required|exists:tasks,id',
				'user_id'	=> 'has_policy_task:' . $request->get('task_id', null)
			]);
			$task = Task::find($request->get('task_id'));
			$input = $request->only(['task_id']);
			$input['teacher_id'] = $task->teacher_id;
			$model = UserTask::create($input);

			return response()->json([
				'data' => $model
			], 201);
		}
		catch (ValidationException $e) {
			return response()->json([
				'messages' => $e->validator->messages()
			], 400);
		}
	}

	public function submission(Request $request, $task_id)
	{
		try {
			$userID = $this->getUserID();
			$model = UserTask::where([
				'task_id' => $task_id,
				'user_id' => $userID
			])->firstOrFail();

			$request->merge(['user_id' => $userID, 'task_id' => $task_id]);
			$this->validate($request, [
				'answer'	=> 'required|match_task_keyword:'. $model->task_id
			]);

			$input = $request->input();
			$input['status'] = 'submission';
			$model->fill($input)->save();

			return response()->json([
				'data' => $model
			]);
		} catch (ValidationException $e) {
			return response()->json([
				'messages' => $e->validator->messages()
			], 400);
		} catch (ModelNotFoundException $e) {
			return response(null, 404);
		}
	}

	public function save(Request $request, $task_id)
	{
		try {
			$userID = $this->getUserID();
			$model = UserTask::where([
				'task_id' => $task_id,
				'user_id' => $userID
			])->firstOrFail();

			$input = $request->input();
			$input['status'] = 'draft';
			$model->fill($input)->save();

			return response()->json([
				'data' => $model
			]);
		} catch (ModelNotFoundException $e) {
			return response(null, 404);
		}
	}
}
