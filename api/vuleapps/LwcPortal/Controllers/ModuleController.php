<?php
namespace VuleApps\LwcPortal\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;
use VuleApps\LwcBackends\Models\PostMeta;
use VuleApps\LwcPortal\Models\Module;

class ModuleController extends Controller
{
	/**
	 * @api {GET} module List Module
	 * @apiDescription Namespace : <strong>VuleApps/LwcPortal/Controllers/ModuleController@index</strong>
	 * @apiGroup Module
	 * @apiParam {Number} [limit=20]
	 * @apiSuccess (200) {Object[]} data Module Object. Module withCount('tasks')
	 * @apiSuccess (200) {String} links Pagination HTML String
	 * @apiSuccess (200) {Number} Total Total Record
	 */
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		$model = Module::withCount('tasks')
					->paginate($request->get('limit', 20));
		return response()->json([
			'data' => $model->items(),
			'links' => (string) $model->render(),
			'total' => $model->total()
		]);
    }

	/**
	 * @api {POST} module Create a Module
	 * @apiDescription Namespace : <strong>VuleApps/LwcPortal/Controllers/ModuleController@store</strong>
	 * @apiGroup Module
	 * @apiParam {String} title
	 * @apiParam {Number} parent_id Course ID
  	 * @apiParam {String} [image]
	 * @apiParam {String} [slug]
	 * @apiParam {LongText} [content]
	 * @apiParam {Text} [excerpt]
	 * @apiParam {Number} [sort]
	 * @apiParam {String} [meta_title]
	 * @apiParam {String} [meta_content]
	 * @apiParam {String} [meta_keywords]
	 * @apiParam {Object[]} [postmeta=array]
	 * @apiSuccess (201) {Object} data Module Object
	 * @apiError (400) {Object} messages Validator Messages of Laravel
	 */
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
		try {
			$this->validate($request, [
				'title' => 'required',
				'parent_id' => 'required'
			]);
			$input = $request->input();
			$input['type'] = Module::TYPE;

            if(isset($input['postmeta']))
            {
                $metadata = $input['postmeta'];
                unset($input['postmeta']);
            } else {
                $metadata = [];
            }
            $model = Module::create($input);
            $postMetadata = [];

            foreach($metadata as $key => $value) {
                $postMetadata[] = new PostMeta([
                    'meta_key' => $key,
                    'meta_value' => is_array($value)? json_encode($value) : $value
                ]);
            }
            if(!empty($postMetadata))
                $model->postmeta()->saveMany($postMetadata);

            return response()
                ->json([
                    'data' => Module::with('postmeta')->find($model->id)
                ], 201);
		} catch (ValidationException $e) {
			return response()->json([
				'messages' => $e->validator->messages()
			], 400);
		}
    }

	/**
	 * @api {GET} module/:id Get a Module
	 * @apiDescription Namespace : <strong>VuleApps/LwcPortal/Controllers/ModuleController@show</strong>
	 * @apiGroup Module
	 * @apiSuccess (200) {Object} data Module Object
	 * @apiError (404) {NUll} NULL Not Found Module
	 */
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		try {
			return response()->json(['data' => Module::with('postmeta')->findOrFail($id)]);
		} catch(ModelNotFoundException $e) {
			return response(null, 404);
		}
    }

	/**
	 * @api {PUT} module/:id Update a Module
	 * @apiDescription Namespace : <strong>VuleApps/LwcPortal/Controllers/ModuleController@update</strong>
	 * @apiGroup Module
	 * @apiParam {String} title
	 * @apiParam {Number} parent_id Course ID
	 * @apiParam {String} [image]
	 * @apiParam {String} [slug]
	 * @apiParam {LongText} [content]
	 * @apiParam {Text} [excerpt]
	 * @apiParam {Number} [sort]
	 * @apiParam {String} [meta_title]
	 * @apiParam {String} [meta_content]
	 * @apiParam {String} [meta_keywords]
	 * @apiParam {Object[]} [postmeta=array]
	 * @apiSuccess (200) {Object} data Module Object
	 * @apiError (400) {Object} messages Validator Messages of Laravel
	 * @apiError (404) {NUll} NULL Not Found Module
	 */
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		try {
			$model = Module::findOrFail($id);
			$this->validate($request, [
				'title' => 'required',
				'parent_id' => 'required',
				'time_start' => 'required'
			]);
			$input = $request->input();
			unset($input['type']);

            if(isset($input['postmeta']))
            {
                $metadata = $input['postmeta'];
                unset($input['postmeta']);
            } else {
                $metadata = [];
            }
            $model->fill($input)
                ->save();
            $model->postmeta()->delete();
            $postMetadata = [];
            foreach($metadata as $key => $value) {
                $postMetadata[] = new PostMeta([
                    'meta_key' => $key,
                    'meta_value' => is_array($value)? json_encode($value) : $value
                ]);
            }
            if(!empty($postMetadata))
                $model->postmeta()->saveMany($postMetadata);

            return response()
                ->json([
                    'data' => Module::with('postmeta')->find($model->id)
                ]);
		} catch (ValidationException $e) {
			return response()->json([
				'messages' => $e->validator->messages()
			], 400);
		} catch (ModelNotFoundException $e) {
			return response(null, 404);
		}
    }

	/**
	 * @api {DELETE} post/:id Delete a Module
	 * @apiDescription Namespace : <strong>VuleApps/LwcPortal/Controllers/ModuleController@destroy</strong>
	 * @apiGroup Module
	 * @apiSuccess (204) {Null} Null Null Body
	 * @apiError (404) {NUll} NULL Not Found Module
	 */
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		try {
			$model = Module::findOrFail($id);
			$model->delete();
			return response(null, 204);
		} catch (ModelNotFoundException $e) {
			return response(null, 404);
		}
    }
}
