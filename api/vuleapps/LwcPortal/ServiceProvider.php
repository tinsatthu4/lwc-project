<?php
namespace VuleApps\LwcPortal;
use Illuminate\Support\ServiceProvider as LaravelServiceProvider;
use Validator;
use VuleApps\LwcBackends\Models\DefaultPost;
use VuleApps\LwcBackends\Models\Setting;
use VuleApps\LwcBackends\Models\Term;
use VuleApps\LwcBackends\Repositories\CommonRepository;
use VuleApps\LwcBackends\Repositories\SettingRepository;
use VuleApps\LwcBackends\Repositories\SettingRepositoryCache;
use VuleApps\LwcBackends\Repositories\PostRepository;
use VuleApps\LwcBackends\Repositories\PostRepositoryCache;
use VuleApps\LwcBackends\Repositories\TermRepository;
use VuleApps\LwcBackends\Repositories\UserRepository;
use VuleApps\LwcPortal\Repository\PortalRepository;
use VuleApps\LwcPortal\Repository\MyPayPal;
use DB;

class ServiceProvider extends LaravelServiceProvider {

	public function boot() {
		$this->loadViewsFrom(__DIR__ . '/views', 'LwcPortal');
		Validator::extend('has_policy_task', 'VuleApps\LwcPortal\Validation\UserHasPolicyTask@validation');
		Validator::extend('match_task_keyword', 'VuleApps\LwcPortal\Validation\MatchTaskKeyword@validation');
        Validator::extend('teacher_has_policy', 'VuleApps\LwcPortal\Validation\TeacherHasPolicy@validation');
        Validator::extend('teacher_has_course', 'VuleApps\LwcPortal\Validation\TeacherHasCourse@validation');
        Validator::extend('answer_has_format', 'VuleApps\LwcPortal\Validation\AnswerHasFormat@validation');
		Validator::extend('none_exists', function($attribute, $value, $parameters, $validator) {
			$table = array_shift($parameters);
			$field = array_shift($parameters);
			$more_field = array_shift($parameters);
			$more_value = array_shift($parameters);
			$where = [
				$field => $value,
				$more_field => $more_value
			];

			if(DB::table($table)->where($where)->count())
				return false;

			return true;
		});
		$this->loadViewsFrom(__DIR__ . '/views', 'vuleappsLwcPortal');
	}

	public function register() {
		$this->app->singleton(PostRepository::class, function() {
			if(config('app.cache', false))
				return new PostRepositoryCache(new DefaultPost());
			else
				return new PostRepository(new DefaultPost());
		});

		$this->app->singleton(SettingRepository::class, function() {
			if(config('app.cache', false))
				return new SettingRepositoryCache(new Setting());
			else
				return new SettingRepository(new Setting());
		});

		$this->app->singleton(CommonRepository::class, function() {
			return new CommonRepository();
		});

		$this->app->singleton(TermRepository::class, function() {
			return new TermRepository(new Term());
		});

		$this->app->singleton(UserRepository::class, function() {
			return new UserRepository();
		});

		$this->app->singleton(PortalRepository::class, function() {
			return new PortalRepository();
		});

		$this->app->singleton(MyPayPal::class, function() {
			return new MyPayPal();
		});
	}
}
