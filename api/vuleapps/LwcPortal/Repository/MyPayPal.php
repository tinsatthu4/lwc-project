<?php
namespace VuleApps\LwcPortal\Repository;

use VuleApps\LwcPortal\Exceptions\PaypalException;
use Config;

class MyPayPal
{
	function __construct() {

	}

	function post($methodName_, $nvpStr_, $PayPalApiUsername, $PayPalApiPassword, $PayPalApiSignature, $PayPalMode) {
		// Set up your API credentials, PayPal end point, and API version.
		$API_UserName = urlencode($PayPalApiUsername);
		$API_Password = urlencode($PayPalApiPassword);
		$API_Signature = urlencode($PayPalApiSignature);

		$paypalmode = ($PayPalMode=='sandbox') ? '.sandbox' : '';

		$API_Endpoint = "https://api-3t".$paypalmode.".paypal.com/nvp";
		$version = urlencode('124.0');

		// Set the curl parameters.
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $API_Endpoint);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);

		// Turn off the server and peer verification (TrustManager Concept).
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);

		// Set the API operation, version, and API signature in the request.
		$nvpreq = "METHOD=$methodName_&VERSION=$version&PWD=$API_Password&USER=$API_UserName&SIGNATURE=$API_Signature$nvpStr_";

		// Set the request as a POST FIELD for curl.
		curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);

		// Get response from the server.
		$httpResponse = curl_exec($ch);

		if(!$httpResponse) {
			exit("$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')');
		}

		// Extract the response details.
		$httpResponseAr = explode("&", $httpResponse);

		$httpParsedResponseAr = array();
		foreach ($httpResponseAr as $i => $value) {
			$tmpAr = explode("=", $value);
			if(sizeof($tmpAr) > 1) {
				$httpParsedResponseAr[$tmpAr[0]] = urldecode($tmpAr[1]);
			}
		}

		if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) {
			throw new PaypalException("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
		}

		return $httpParsedResponseAr;
	}

	/**
	 * @param $returnUrl
	 * @param $cancleUrl
	 * @param $ItemName
	 * @param $ItemPrice
	 * @param $ItemQty
	 * @param $ItemTotalPrice
	 * @return string
	 * @throws \Exception
	 */
	function send($returnUrl, $cancleUrl, $ItemName, $ItemPrice, $ItemQty, $ItemTotalPrice) {
		$TotalTaxAmount 	= 0.00;
		$HandalingCost 		= 0.00;
		$InsuranceCost 		= 0.00;
		$ShippinDiscount 	= 0.00;
		$ShippinCost 		= 0.00;
		$ItemPrice = (float) str_replace(',', '', $ItemPrice);
		$ItemTotalPrice = (float) str_replace(',', '', $ItemTotalPrice);
		$GrandTotal = ($ItemTotalPrice + $TotalTaxAmount + $HandalingCost + $InsuranceCost + $ShippinCost + $ShippinDiscount);
		$padata = 	'&METHOD=SetExpressCheckout'.
			'&RETURNURL='.urlencode($returnUrl).
			'&CANCELURL='.urlencode($cancleUrl).
			'&PAYMENTREQUEST_0_PAYMENTACTION='.urlencode("SALE").
			'&L_PAYMENTREQUEST_0_NAME0='.urlencode($ItemName).
//			'&L_PAYMENTREQUEST_0_NUMBER0='.urlencode($ItemNumber).
//			'&L_PAYMENTREQUEST_0_DESC0='.urlencode($ItemDesc).
			'&L_PAYMENTREQUEST_0_AMT0='.$ItemPrice.
			'&L_PAYMENTREQUEST_0_QTY0='.$ItemQty.
			'&NOSHIPPING=1'. //set 1 to hide buyer's shipping address, in-case products that does not require shipping
			'&PAYMENTREQUEST_0_ITEMAMT='.$ItemTotalPrice.
			'&PAYMENTREQUEST_0_TAXAMT='.$TotalTaxAmount.
			'&PAYMENTREQUEST_0_SHIPPINGAMT='.$ShippinCost.
			'&PAYMENTREQUEST_0_HANDLINGAMT='.$HandalingCost.
			'&PAYMENTREQUEST_0_SHIPDISCAMT='.$ShippinDiscount.
			'&PAYMENTREQUEST_0_INSURANCEAMT='.$InsuranceCost.
			'&PAYMENTREQUEST_0_AMT='.$GrandTotal.
			'&PAYMENTREQUEST_0_CURRENCYCODE='.urlencode(Config::get('paypal.currency_code')).
			'&LOCALECODE=GB'. //PayPal pages to match the language on your website.
			'&LOGOIMG='. Config::get('paypal.logoimg') . //site logo
			'&CARTBORDERCOLOR=FFFFFF'. //border color of cart
			'&ALLOWNOTE=1';

		$response = $this->post('SetExpressCheckout', $padata, Config::get('paypal.api_user'), Config::get('paypal.api_pass'), Config::get('paypal.api_signature'), Config::get('paypal.mode'));
		$paypalmode = (Config::get('paypal.mode')=='sandbox') ? '.sandbox' : '';
		if("SUCCESS" == strtoupper($response["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($response["ACK"]))
		{
			return 'https://www'.$paypalmode.'.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token='.$response["TOKEN"];
		}else
			throw new PaypalException($response["L_LONGMESSAGE0"]);
	}

	/**
	 * @param $ItemName
	 * @param $ItemPrice
	 * @param $ItemQty
	 * @param $ItemTotalPrice
	 * @param $token
	 * @param $payer_id
	 * @return bool
	 * @throws PaypalException
	 */
	function validate($ItemName, $ItemPrice, $ItemQty, $ItemTotalPrice) {
		$token = request()->input('token');
		$payer_id = request()->input('PayerID');
		$TotalTaxAmount 	= 0.00;
		$HandalingCost 		= 0.00;
		$InsuranceCost 		= 0.00;
		$ShippinDiscount 	= 0.00;
		$ShippinCost 		= 0.00;
		$GrandTotal = ($ItemTotalPrice + $TotalTaxAmount + $HandalingCost + $InsuranceCost + $ShippinCost + $ShippinDiscount);

		$padata = 	'&TOKEN='.urlencode($token).
			'&PAYERID='.urlencode($payer_id).
			'&PAYMENTREQUEST_0_PAYMENTACTION='.urlencode("SALE").

			//set item info here, otherwise we won't see product details later
			'&L_PAYMENTREQUEST_0_NAME0='.urlencode($ItemName).
			'&L_PAYMENTREQUEST_0_AMT0='.urlencode($ItemPrice).
			'&L_PAYMENTREQUEST_0_QTY0='. urlencode($ItemQty).
			'&PAYMENTREQUEST_0_ITEMAMT='.urlencode($ItemTotalPrice).
			'&PAYMENTREQUEST_0_TAXAMT='.urlencode($TotalTaxAmount).
			'&PAYMENTREQUEST_0_SHIPPINGAMT='.urlencode($ShippinCost).
			'&PAYMENTREQUEST_0_HANDLINGAMT='.urlencode($HandalingCost).
			'&PAYMENTREQUEST_0_SHIPDISCAMT='.urlencode($ShippinDiscount).
			'&PAYMENTREQUEST_0_INSURANCEAMT='.urlencode($InsuranceCost).
			'&PAYMENTREQUEST_0_AMT='.urlencode($GrandTotal).
			'&PAYMENTREQUEST_0_CURRENCYCODE='.urlencode(Config::get('paypal.currency_code'));
		$response = $this->post('DoExpressCheckoutPayment', $padata, Config::get('paypal.api_user'), Config::get('paypal.api_pass'), Config::get('paypal.api_signature'), Config::get('paypal.mode'));

		if("SUCCESS" == strtoupper($response["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($response["ACK"]))
		{
			return true;
		} else {
			throw new PaypalException($response["L_LONGMESSAGE0"]);
		}
	}
}