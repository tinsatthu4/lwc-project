<?php namespace VuleApps\LwcPortal\Repository;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use VuleApps\LwcPortal\Models\Counrse;
use App\User;
use DB;
use VuleApps\LwcPortal\Models\Module;
use VuleApps\LwcPortal\Models\Task;
use VuleApps\LwcPortal\Models\UserCounrse;
use VuleApps\LwcPortal\Models\UserMeta;
use VuleApps\LwcPortal\Models\VoucherCodeReference;
use VuleApps\LwcPortal\Models\UserModule;

class PortalRepository {
	/**
	 * @api {RETURN FUNC} groupTaskIDsByCourse groupTaskIDsByCourse
	 * @apiGroup Portal Repository
	 * @apiDescription Uses : <strong>PortalRepository::groupTaskIDsByCourse($course_id)</strong> <br/>
	 * Namespace <strong>VuleApps\LwcPortal\Repository\PortalRepository@groupTaskIDsByCourse</strong>
	 * TestCase <strong>VuleAppsTest\LWcPortal\Repository\PortalRepositoryTest@testGroupTaskIDsByCourse</strong>
	 * @apiParam {Number} course_id
	 * @apiSuccessExample {json} Example-Return
	 * [1,2,3,4]
	 */
	/**
	 * @param $course_id
	 */
	function groupTaskIDsByCourse($course_id) {
		$moduleIds = Module::where('parent_id', $course_id)
							->select('id')
							->get()
							->pluck('id')
							->all();

		if(empty($moduleIds))
			return [];

		$taskIds = Task::whereIn('post_id', $moduleIds)
						->select('id')
						->get()
						->pluck('id')
						->all();
		return $taskIds;
	}

	/**
	 * @api {RETURN FUNC} groupTaskIDsByModule groupTaskIDsByModule
	 * @apiGroup Portal Repository
	 * @apiDescription Uses : <strong>PortalRepository::groupTaskIDsByModule($module_id)</strong> <br/>
	 * Namespace <strong>VuleApps\LwcPortal\Repository\PortalRepository@groupTaskIDsByModule</strong>
	 * TestCase <strong>VuleAppsTest\LWcPortal\Repository\PortalRepositoryTest@testGroupTaskIDsByModule</strong>
	 * @apiParam {Number} course_id
	 * @apiSuccessExample {json} Example-Return
	 * [1,2,3,4]
	 */
	/**
	 * @param $course_id
	 */
	function groupTaskIDsByModule($modul_id) {
		$taskIds = Task::where('post_id', $modul_id)
					   ->select('id')
					   ->get()
					   ->pluck('id')
					   ->all();
		return $taskIds;
	}

	/**
	 * 
	 */
	function generateReferenceCode($user_id) {
		return UserMeta::create([
								'user_id'    => $user_id,
								'meta_key'   => 'code',
								'meta_value' => uniqid()
							]);
	}

	/**
	 * 
	 */
	function checkAndAddVoucherAfterPayment($user_id_use_code, $code) {
		$user_give_code = UserMeta::where([
									['meta_key', 'code'],
									['meta_value', $code]
								 ])
								 ->first();

		if( $user_give_code ) {
			$user_use_voucher = VoucherCodeReference::where([
												['user_id_give_code', $user_give_code->user_id],
												['user_id_use_code', $user_id_use_code]
											])
											->count();

			if( !$user_use_voucher > 0 ) {
				return VoucherCodeReference::create([
										'user_id_give_code' => $user_give_code->user_id,
										'user_id_use_code'  => $user_id_use_code
									]);
			}
		}
		
	}

	/**
	 * 
	 */
	function checkUserReferenceCode($user_id, $reference_code) {
		$checkUserRef = UserMeta::where([
									['meta_key', 'code'],
									['meta_value', $reference_code]
								])
								->count();

		if($checkUserRef>0) {
			return UserMeta::create([
								'user_id'    => $user_id,
								'meta_key'   => 'reference_by_code',
								'meta_value' => $reference_code
							]);
		}
	}

	function registerCourseForUser($user_id, $course_id) {
        if(! UserCounrse::where([
            'user_id' => $user_id,
            'post_id' => $course_id
        ])->count())
            UserCounrse::create([
                'user_id' => $user_id,
                'post_id' => $course_id
            ]);

        $modules = Module::select('id')->hasCourse($course_id);
        $moduleIds = $modules->pluck('id')->all();
        foreach($moduleIds as $id) {

            if(! UserModule::where([
                'user_id' => $user_id,
                'post_id' => $id
            ])->count())
                UserModule::create([
                    'user_id' => $user_id,
                    'post_id' => $id
                ]);
        }
    }
}