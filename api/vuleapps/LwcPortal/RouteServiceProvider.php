<?php
namespace VuleApps\LwcPortal;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Route;
use VuleApps\LwcPortal\Controllers\LOController;

class RouteServiceProvider extends ServiceProvider
{
	/**
	 * Define your route model bindings, pattern filters, etc.
	 *
	 * @param  \Illuminate\Routing\Router  $router
	 * @return void
	 */
	public function boot(Router $router)
	{
		parent::boot($router);
	}

	/**
	 * Define the routes for the application.
	 *
	 * @param  \Illuminate\Routing\Router  $router
	 * @return void
	 */
	public function map(Router $router)
	{
		$this->mapWebRoutes($router);
	}

	/**
	 * Define the "web" routes for the application.
	 *
	 * These routes all receive session state, CSRF protection, etc.
	 *
	 * @param  \Illuminate\Routing\Router  $router
	 * @return void
	 */
	protected function mapWebRoutes(Router $router)
	{
		//register
        $router->get('api/filemanager/download-no-auth', 'VuleApps\Common\Http\Controllers\Filemanager@getDownload');

        /**
		 * @api {POST} user/register Post Register
		 * @apiDescription <strong>App\Http\Controllers\AuthenticateController@postRegister</strong>
		 * @apiGroup User
		 * @apiParam {String} name
		 * @apiParam {String} email
		 * @apiParam {String} password
		 * @apiParam {String} password_confirmation
		 * @apiSuccess (200) {String} token
		 * @apiSuccessExample {json} Response-Example:
		 * {"token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsImlzcyI6Imh0dHA6XC9cL2xvY2FsaG9zdFwvdXNlclwvcmVnaXN0ZXIiLCJpYXQiOjE0NzkxNzg2ODUsImV4cCI6MTQ3OTE4NTg4NSwibmJmIjoxNDc5MTc4Njg1LCJqdGkiOiJmNDQxYzAzNGM3ODc0ZWE3ZmVjODhiNTQxYzEwMmZkMSJ9.KLwhrU2eVZGPwgEgIt92uWylXPpuKriiO5npBhmKCgI"}
		 * @apiError (400) {Object} messages
		 * @apiErrorExample {json} Response-Example:
		 * {"messages":{"name":["The name field is required."],"email":["The email must be a valid email address."],"password":["The password must be at least 6 characters.","The password confirmation does not match."]}
		 */
		$router->post('api/user/register', 'App\Http\Controllers\AuthenticateController@postRegister');
		$router->group([
			'prefix' => 'api',
			'middleware' => 'jwt.auth',
		],  function(Router $router) {
			/**
			 * @api {GET} user-task-comment/by-user-task-id/:user_task_id Get Comment By User Task ID
			 * @apiGroup API
			 */
			$router->get('user-task-comment/by-user-task-id/{user_task_id}', 'VuleApps\LwcPortal\Controllers\UserTaskCommentController@getByUserTaskID');
			$router->post('user-task-comment', 'VuleApps\LwcPortal\Controllers\UserTaskCommentController@store');
		});
		$router->group([
			'prefix' => 'api',
			'middleware' => 'teacher_1',
		], function(Router $router) {
            $router->resource('post', 'VuleApps\LwcPortal\Controllers\PostController', ['except' => [
                'create', 'edit'
            ]]);
			$router->resource('module', 'VuleApps\LwcPortal\Controllers\ModuleController', ['except' => [
				'create', 'edit'
			]]);
			$router->resource('module.task', 'VuleApps\LwcPortal\Controllers\ModuleTaskController', ['except' => [
				'create', 'edit'
			]]);
            $router->post('task/{task}/mcq/store-array', 'VuleApps\LwcPortal\Controllers\MCQController@postStoreArray');
			$router->resource('task.mcq', 'VuleApps\LwcPortal\Controllers\MCQController', ['except' => [
                'create', 'edit'
            ]]);
			$router->resource('task', 'VuleApps\LwcPortal\Controllers\TaskController', ['only' => ['index', 'show']]);

            /**
             * @api {GET} api/user-task-comment Get User Task Comment
             * @apiGroup API
             * @apiParam {Number} [parent_id=0]
             * @apiParam {String} [sort=id]
             * @apiParam {String} [order=asc]
             * @apiParam {Number} [limit=50]
             * @apiSuccessExample {json} Response:
             * {"data":{"total":1,"per_page":50,"current_page":1,"last_page":1,"next_page_url":null,"prev_page_url":null,"from":1,"to":1,"data":[{"id":1,"user_task_id":0,"parent_id":0,"message":"","created_at":"2016-12-15 14:20:38","updated_at":"2016-12-15 14:20:38"}]},"links":"","total":1}
             */

            /**
             * @api {POST} api/user-task-comment Get User Task Comment
             * @apiGroup API
             * @apiParam {Number} user_task_id user task id exist in user_tasks table
             * @apiParam {Number} parent_id Parent comment id
             * @apiParam {Text}   message Content of comment
             * @apiParam {Number} [limit=50]
             * @apiSuccessExample {json} Response:
             * {"data":{"user_task_id":1,"parent_id":0,"message":"Test message","updated_at":"2016-12-15 14:21:41","created_at":"2016-12-15 14:21:41","id":2}}
             */
//			$router->resource('user-task-comment', 'VuleApps\LwcPortal\Controllers\UserTaskCommentController', ['only' => ['index', 'store']]);
			$router->controller('filemanager', \VuleApps\Common\Http\Controllers\Filemanager::class);
		});

		//Router For Learner
		$router->group([
					'prefix' => 'api/learner',
					'middleware' => 'lernerApi',
		], function(Router $router) {
					$router->controller('filemanager', \VuleApps\Common\Http\Controllers\Filemanager::class);
		});

		$router->group([
            'prefix' => 'api/learner',
            'middleware' => 'jwt.auth',
		], function(Router $router) {
          $router->post('save/tmp-payment-method', 'VuleApps\LwcPortal\Controllers\LearnerController@postSavePaymentMethod');
		});

		$router->group([
			'prefix' => 'api/learner',
			'middleware' => 'lernerApi',
			'namespace' => 'VuleApps\LwcPortal\Controllers'
		], function(Router $router) {
			$router->resource('post', 'PostController', ['only' => ['index', 'show']]);
			$router->group([
				'prefix' => 'ek',
			], function(Router $router) {

                $router->resource('course', 'EKCourseController', ['only' => [
                    'index', 'show'
                ]]);

                $router->resource('course.module', 'EKCourseModuleController', ['only' => [
                    'index', 'show'
                ]]);

                $router->resource('module', 'EKModuleController', ['only' => [
                    'show'
                ]]);

                $router->resource('module.lesson', 'EKModuleLessonController', ['only' => [
                    'index', 'show'
                ]]);

                $router->resource('lesson.session', 'EKLessonSessionController', ['only' => [
                    'index', 'show'
                ]]);
			});

			$router->group(['prefix' => 'user-module'], function (Router $router) {
                $router->post('file/{moduleID}', 'LearnerController@postModuleFinaleFile');
            });
			/**
			 * @api {GET} learner/module/:module_id Get Module
			 * @apiGroup Learner
			 * @apiDescription Filter By Time Start
			 */
			$router->get('module/{id}', 'LearnerController@getModule');
			$router->resource('module', 'ModuleController', ['only' => ['index']]);
			$router->resource('module.task', 'ModuleTaskController', ['only' => ['index', 'show']]);
			$router->resource('task.mcq', 'MCQController', ['only' => ['index', 'show']]);
			$router->resource('task', 'TaskController', ['only' => 'show']);
			$router->get('user-task/modules-and-tasks/{counrse_id}', 'UserTaskController@getModulesAndTasks');

			$router->post('user-task/{task_id}/submission', [
				'as' => 'api.user_tasks.submission',
				'uses' => 'LearnerController@submission'
			]);
			$router->post('user-task/{task_id}/save', [
				'as' => 'api.user_tasks.save',
				'uses' => 'LearnerController@save'
			]);
			$router->get('/', [
				'as' => 'portal.learner.dashboard',
				'uses' => 'LearnerController@index'
			]);
			$router->get('cournse/{slug}-{id}', [
				'as' => 'UserController.getCounrsePage',
				'uses' => 'UserController@getCounrsePage'
			])->where(
				[
					'slug' => '[\w-]+',
					'id' => '[\d]+'
				]
			);
			$router->get('user-info', 'UserController@getUserInfo');
            $router->get('master-file/config', [
                'as' => 'learner.master_file.config',
                'uses' => 'MasterFileController@getConfigLearner'
            ]);
            $router->post('master-file/config', [
                'as' => 'learner.master_file.config',
                'uses' => 'MasterFileController@postConfigLearner'
            ]);
			//Work
			$router->group([
				'prefix' => 'work'
			], function(Router $router) {
				$router->put('{work}', 'LearnerController@putUpdateWork');
				$router->delete('{work}', 'LearnerController@deleteDestroyWork');
				$router->post('/', 'LearnerController@postStoreWork');
				$router->get('{work}', 'LearnerController@getShowWork');
				$router->get('/', 'LearnerController@getIndexWork');
			});

			/**
			 * @api {GET} learner/my-activities Activites
			 * @apiParam {Number} [limit=50]
			 * @apiGroup Learner
			 * @apiSuccessExample {json} Response:
			 * {"data":[{"id":1,"teacher_id":0,"parent_id":0,"title":"Course 1","slug":"","excerpt":null,"content":null,"image":null,"sort":0,"type":"counrse","author":0,"status":"publish","meta_title":null,"meta_content":null,"meta_keywords":null,"created_at":null,"updated_at":null,"deleted_at":null,"price":0,"modules":[{"id":3,"teacher_id":0,"parent_id":1,"title":"Module 1","slug":"","excerpt":null,"content":null,"image":null,"sort":0,"type":"module","author":0,"status":"publish","meta_title":null,"meta_content":null,"meta_keywords":null,"created_at":null,"updated_at":null,"deleted_at":null,"price":0}]},{"id":2,"teacher_id":0,"parent_id":0,"title":"Course 1","slug":"","excerpt":null,"content":null,"image":null,"sort":0,"type":"counrse","author":0,"status":"publish","meta_title":null,"meta_content":null,"meta_keywords":null,"created_at":null,"updated_at":null,"deleted_at":null,"price":0,"modules":[{"id":4,"teacher_id":0,"parent_id":2,"title":"Module 2","slug":"","excerpt":null,"content":null,"image":null,"sort":0,"type":"module","author":0,"status":"publish","meta_title":null,"meta_content":null,"meta_keywords":null,"created_at":null,"updated_at":null,"deleted_at":null,"price":0},{"id":5,"teacher_id":0,"parent_id":2,"title":"Module 3","slug":"","excerpt":null,"content":null,"image":null,"sort":0,"type":"module","author":0,"status":"publish","meta_title":null,"meta_content":null,"meta_keywords":null,"created_at":null,"updated_at":null,"deleted_at":null,"price":0}]}],"total":2,"links":""}
			 */
			$router->get('my-activities', 'LearnerController@getMyActivites');

			/**
			 * @api {GET} learner/my-metadata Meta Data
			 * @apiGroup Learner
			 */
			$router->get('my-metadata', 'LearnerController@getLearnerMetadata');
			/**
			 * @api {GET} learner/all-courses Group All modules by Course
			 * @apiDescription LearnerController@getAllCourse
			 * @apiGroup Learner
			 */
			$router->get('all-courses', 'LearnerController@getAllCourse');
			$router->group(['prefix' => 'my-modules'], function(Router $router) {
				/**
				 * @api {GET} learner/my-modules/:module_id/tasks Get All Tasks
				 * @apiGroup Learner
				 */
				$router->get('/{module_id}/tasks', [
					'middleware' => 'lwc.user_has_module',
					'uses' => 'LearnerController@getAllTasks'
				]);

				/**
				 * @api {GET} learner/my-modules Group My Modules by Course
				 * @apiGroup Learner
				 * @apiParam {Number} [limit=50]
				 * @apiSuccessExample {json} Response:
				 * {"data":[{"id":1,"teacher_id":0,"parent_id":0,"title":"Course 1","slug":"","excerpt":null,"content":null,"image":null,"sort":0,"type":"counrse","author":0,"status":"publish","meta_title":null,"meta_content":null,"meta_keywords":null,"created_at":null,"updated_at":null,"deleted_at":null,"price":0,"modules":[{"id":3,"teacher_id":0,"parent_id":1,"title":"Module 1","slug":"","excerpt":null,"content":null,"image":null,"sort":0,"type":"module","author":0,"status":"publish","meta_title":null,"meta_content":null,"meta_keywords":null,"created_at":null,"updated_at":null,"deleted_at":null,"price":0,"pivot":{"user_id":1,"post_id":3}}]},{"id":2,"teacher_id":0,"parent_id":0,"title":"Course 1","slug":"","excerpt":null,"content":null,"image":null,"sort":0,"type":"counrse","author":0,"status":"publish","meta_title":null,"meta_content":null,"meta_keywords":null,"created_at":null,"updated_at":null,"deleted_at":null,"price":0,"modules":[{"id":4,"teacher_id":0,"parent_id":2,"title":"Module 2","slug":"","excerpt":null,"content":null,"image":null,"sort":0,"type":"module","author":0,"status":"publish","meta_title":null,"meta_content":null,"meta_keywords":null,"created_at":null,"updated_at":null,"deleted_at":null,"price":0,"pivot":{"user_id":1,"post_id":4}}]}],"total":2,"links":""}
				 */
				$router->get('/', 'LearnerController@getMyModules');
			});

      /**
       * @api {GET} learner/user-task-comment Get User Task Comment
       * @apiGroup Learner
       * @apiParam {Number} [parent_id=0]
       * @apiParam {String} [sort=id]
       * @apiParam {String} [order=asc]
       * @apiParam {Number} [limit=50]
       * @apiSuccessExample {json} Response:
       * {"data":{"total":1,"per_page":50,"current_page":1,"last_page":1,"next_page_url":null,"prev_page_url":null,"from":1,"to":1,"data":[{"id":1,"user_task_id":0,"parent_id":0,"message":"","created_at":"2016-12-15 14:20:38","updated_at":"2016-12-15 14:20:38"}]},"links":"","total":1}
       */

      /**
       * @api {POST} learner/user-task-comment Get User Task Comment
       * @apiGroup Learner
       * @apiParam {Number} user_task_id user task id exist in user_tasks table
       * @apiParam {Number} parent_id Parent comment id
       * @apiParam {Text}   message Content of comment
       * @apiParam {Number} [limit=50]
       * @apiSuccessExample {json} Response:
       * {"data":{"user_task_id":1,"parent_id":0,"message":"Test message","updated_at":"2016-12-15 14:21:41","created_at":"2016-12-15 14:21:41","id":2}}
       */
      $router->get('user-task-comment/child', 'UserTaskCommentController@indexChild');
      $router->resource('user-task-comment', 'UserTaskCommentController', ['only' => ['index', 'store']]);
      $router->resource('counrse', 'CounrseController',
          ['only' => ['index', 'show']]
      );
      $router->resource('module.task', 'ModuleTaskController', ['only' => [
          'index'
      ]]);
			$router->controller('/', 'LearnerController');
		});

		//Rotue For Teacher
		$router->group([
			'prefix' => 'api/teacher',
			'middleware' => 'teacherApi',
		], function(Router $router) {
		    $router->group(["prefix" => 'user-module'], function (Router $router) {
                $router->get("final-assignment-submission/counrse/{counrseID}", "VuleApps\LwcPortal\Controllers\TeacherReviewTaskController@getFinalAssignmentSubmissionByCounrse");
                $router->get("final-assignment-submission/{userModuleID}/download", "VuleApps\LwcPortal\Controllers\TeacherReviewTaskController@getDownloadFinalAssignmentSubmission");
            });

			//dashboard
			$router->get('/', [
				'as' => 'portal.teacher01.dashboard',
				'uses' => 'VuleApps\LwcPortal\Controllers\UserController@teacher01Dashboard'
			]);

            $router->resource('counrse', 'VuleApps\LwcPortal\Controllers\TeacherCounrseController',
				['only' => ['index', 'store', 'update']]
			);
            $router->resource('counrse.module', 'VuleApps\LwcPortal\Controllers\TeacherCounrseModuleController');

			$router->resource('post', 'VuleApps\LwcPortal\Controllers\PostController');

			$router->resource('module', 'VuleApps\LwcPortal\Controllers\TeacherModuleController');
			$router->resource('module.task', 'VuleApps\LwcPortal\Controllers\TeacherModuleTaskController');
			$router->resource('lo', LOController::class, [
				'only' => ['index', 'show', 'store', 'destroy', 'update']
			]);
			$router->resource('task', 'VuleApps\LwcPortal\Controllers\TaskController', ['only' => ['index', 'show']]);
			$router->post('task/{task}/mcq/store-array', 'VuleApps\LwcPortal\Controllers\MCQController@postStoreArray');
			$router->resource('task.mcq', 'VuleApps\LwcPortal\Controllers\MCQController');

			$router->controller('filemanager', \VuleApps\Common\Http\Controllers\Filemanager::class);

			$router->get('user-task/group', 'VuleApps\LwcPortal\Controllers\TeacherReviewTaskController@getUserTask');
			$router->get('user-task/{id}/group', 'VuleApps\LwcPortal\Controllers\TeacherReviewTaskController@getUserTaskByUserId');
			$router->get('user-task/{id}/download', 'VuleApps\LwcPortal\Controllers\TeacherReviewTaskController@downloadUserTaskByUserId');
			$router->get('group-user-task/group', 'VuleApps\LwcPortal\Controllers\TeacherReviewTaskController@getGroupUserTask');

			$router->put('user-task/{user_task_id}', 'VuleApps\LwcPortal\Controllers\TeacherReviewTaskController@putUserTask');
			$router->get('user-task/{user_task_id}', 'VuleApps\LwcPortal\Controllers\TeacherReviewTaskController@getUserTaskById');
			$router->get('my/modules/{course_id}', 'VuleApps\LwcPortal\Controllers\TeacherReviewTaskController@getMyModules');
			$router->get('my/course', 'VuleApps\LwcPortal\Controllers\TeacherReviewTaskController@getMyCourses');

			$router->get('user-info', 'VuleApps\LwcPortal\Controllers\UserController@getUserInfo');

            $router->get('master-file/config/course/{id}', [
                'as' => 'teacher.master_file.config',
                'uses' => 'VuleApps\LwcPortal\Controllers\MasterFileController@getConfigTeacher'
            ]);
            $router->post('master-file/config/course/{id}', [
                'as' => 'teacher.master_file.config',
                'uses' => 'VuleApps\LwcPortal\Controllers\MasterFileController@postConfigTeacher'
            ]);

            $router->get('master-file/list', [
                'as' => 'masterFile.index',
                'uses' => 'VuleApps\LwcPortal\Controllers\MasterFileController@index'
            ]);
            $router->get('master-file/list-module', [
                'as' => 'masterFile.indexModule',
                'uses' => 'VuleApps\LwcPortal\Controllers\MasterFileController@indexModule'
            ]);
            $router->get('master-file/download/user/{user_id}/course/{course_id}', 'VuleApps\LwcPortal\Controllers\MasterFileController@getMasterFile');
            $router->get('master-file/download/user/{user_id}/module/{module_id}/type/{type}', 'VuleApps\LwcPortal\Controllers\MasterFileController@getMasterFileByType');
            $router->get('master-file/download/user/{user_id}/module/{module_id}/all-file', 'VuleApps\LwcPortal\Controllers\MasterFileController@getAllMasterFile');
		});

		$router->group([
			'prefix' => 'api/administrator',
			'middleware' => 'portal'
		], function(Router $router) {
            $router->get('/', [
                'as' => 'portal.admin.dashboard',
                'uses' => 'VuleApps\LwcPortal\Controllers\UserController@adminDashboard'
            ]);
            $router->get('user/get-course-by-id', [
                'as' => 'portal.admin.getCourseByUser',
                'uses' => 'VuleApps\LwcPortal\Controllers\UserController@getCourseByUser'
            ]);
			$router->get('user/get-teachers', [
				'as' => 'portal.admin.getAllTeacher',
				'uses' => 'VuleApps\LwcPortal\Controllers\UserController@getAllTeacher'
			]);
            $router->get('learner/{id}/get-course-by-learner', [
                'as' => 'portal.admin.getCoursesByLearner',
                'uses' => 'VuleApps\LwcPortal\Controllers\UserController@getCoursesByLearner'
            ]);
			$router->get('teacher/search', 'VuleApps\LwcPortal\Controllers\AdministratorTeacherController@getTeacherSearch');
			$router->resource('post', 'VuleApps\LwcPortal\Controllers\PostController');
            $router->resource('counrse', 'VuleApps\LwcPortal\Controllers\CounrseController');
			$router->resource('module', 'VuleApps\LwcPortal\Controllers\ModuleController');
            $router->resource('task', 'VuleApps\LwcPortal\Controllers\TaskController');
            $router->resource('user', 'VuleApps\LwcPortal\Controllers\UserController');
			$router->resource('module.task', 'VuleApps\LwcPortal\Controllers\ModuleTaskController', ['except' => [
				'create', 'edit'
			]]);
			$router->post('task/{task}/mcq/store-array', 'VuleApps\LwcPortal\Controllers\MCQController@postStoreArray');
			$router->resource('task.mcq', 'VuleApps\LwcPortal\Controllers\MCQController', ['except' => [
				'create', 'edit'
			]]);
            $router->resource('checkout', 'VuleApps\LwcPortal\Controllers\CheckoutController', ['only' => [
                'index', 'show'
            ]]);
						$router->group([
							'prefix' => 'ek',
						], function(Router $router) {
									$router->resource('course', 'VuleApps\LwcPortal\Controllers\EKCourseController');
									$router->resource('module', 'VuleApps\LwcPortal\Controllers\EKModuleController');
									$router->resource('course.module', 'VuleApps\LwcPortal\Controllers\EKCourseModuleController');
									$router->resource('module.lesson', 'VuleApps\LwcPortal\Controllers\EKModuleLessonController');
									$router->resource('lesson.session', 'VuleApps\LwcPortal\Controllers\EKLessonSessionController');
						});
			$router->controller('filemanager', \VuleApps\Common\Http\Controllers\Filemanager::class);
		});

		//for Teacher Master File
		$router->group([
			'prefix' => 'api/master-file',
			'middleware' => 'teacher_master_file'
		], function(Router $router) {

			$router->get('user-info', 'VuleApps\LwcPortal\Controllers\UserController@getUserInfo');
			$router->get('/', [
				'as' => 'masterFile.dashboard',
				'uses' => 'VuleApps\LwcPortal\Controllers\UserController@teacherMasterFileDashboard'
			]);
            $router->get('/list', [
                'as' => 'masterFile.index',
                'uses' => 'VuleApps\LwcPortal\Controllers\MasterFileController@index'
            ]);
            $router->get('/download/user/{user_id}/course/{course_id}', 'VuleApps\LwcPortal\Controllers\MasterFileController@getMasterFile');
            $router->get('/download/user/{user_id}/module/{module_id}/all-file', 'VuleApps\LwcPortal\Controllers\MasterFileController@getAllMasterFile');
            $router->get('/download/user/{user_id}/module/{module_id}/type/{type}', 'VuleApps\LwcPortal\Controllers\MasterFileController@getMasterFileByType');
		});

		$router->group([
            'prefix' => 'api/common',
            'middleware' => 'jwt.auth'
        ], function(Router $router) {
			/**
			 * @api {GET} filemanager/download Filemanager Download
			 * @apiGroup Common
			 * @apiParams {String} path Path to file download
			 */
			$router->get('filemanager/download', 'VuleApps\Common\Http\Controllers\Filemanager@getDownload');

			/**
			 * @api {PUT} common/profile Update Profile
			 * @apiGroup Common
			 * @apiDescription Use <strong>VuleApps\LwcPortal\Controllers\UserController@putProfile</strong>
			 */
			$router->put('profile', 'VuleApps\LwcPortal\Controllers\UserController@putProfile');
		});

		//Paypal Checkout
		$router->group(['prefix' => 'api/paypal'], function(Router $router) {
			/**
			 * @api {POST} paypal/make Create Checkout
			 * @apiParam {Number} post_id Module ID
			 * @apiSuccess (201) {String} redirect Link Payment of Paypal
			 * @apiError (400) {Object} messages Validator Error Laravel
			 * @apiGroup Paypal
			 */
			$router->post('make', [
				'uses' => 'VuleApps\LwcPortal\Controllers\PaypalController@postMake',
				'middleware' => 'jwt.auth'
			]);

			$router->get('redirect/{checkout_id}', [
				'as' => 'paypal.redirect',
				'uses' => 'VuleApps\LwcPortal\Controllers\PaypalController@getRedirect',
			])->where('checkout_id', '\d+');

			$router->get('cancel/{checkout_id}', [
				'as' => 'paypal.cancel',
				'uses' => 'VuleApps\LwcPortal\Controllers\PaypalController@getCancel',
			]);

			/**
			 * @api {POST} paypal/make Create Course Checkout
			 * @apiParam {Number} post_id Course ID
			 * @apiSuccess (201) {String} redirect Link Payment of Paypal
			 * @apiError (400) {Object} messages Validator Error Laravel
			 * @apiGroup Paypal
			 */
			$router->post('make-course', [
				'uses' => 'VuleApps\LwcPortal\Controllers\PaypalController@postMakeCourse',
				'middleware' => 'jwt.auth'
			]);

			$router->get('redirect-course/{checkout_id}', [
				'as' => 'paypal.redirect-course',
				'uses' => 'VuleApps\LwcPortal\Controllers\PaypalController@getRedirectCourse',
			])->where('checkout_id', '\d+');

		});

        //Get post news, announments, events
        $router->get('api/{slug}', [
            'middleware' => 'jwt.auth',
            'uses' => 'VuleApps\LwcPortal\Controllers\CommonController@getPostsBySlug'
        ])->where('slug', '(news|announcements|event)');

        $router->group([
            'prefix' => 'api/guest',
            'namespace' => 'VuleApps\LwcPortal\Controllers'
        ], function(Router $router) {
            $router->post('generate', 'GuestPaymentController@postGenerateLinkPayment');
            $router->post('choose-payment/{code}', 'GuestPaymentController@postSavePaymentMethod');

            $router->get('paypal/cancel/{code}', ['as' => 'guest.paypal.cancel', 'uses' => 'GuestPaymentController@getPaypalCancel']);
            $router->get('paypal/success/{code}', ['as' => 'guest.paypal.success', 'uses' => 'GuestPaymentController@getPaypalSuccess']);
            $router->post('paypal/{code}', 'GuestPaymentController@postPaypalGenerateLink');

            $router->get('{code}', 'GuestPaymentController@show');
        });

        // Pham Thong
        $router->group([
            'prefix' => 'api',
            'namespace' => 'VuleApps\LwcPortal\Controllers'
        ], function(Router $router) {
        	$router->group(['prefix' => 'guest-payments'], function(Router $router) {
				$router->post('info', 'GuestPaymentController@postCreateInfoGuestPayment');
				$router->post('upload/file', 'GuestPaymentController@postFile');
			});
        });

        $router->group([
            'prefix' => 'api',
            'middleware' => 'portal',
            'namespace' => 'VuleApps\LwcPortal\Controllers'
        ], function(Router $router) {
	        	$router->group(['prefix' => 'guest-payments'], function(Router $router) {
						$router->post('info/{code}', 'GuestPaymentController@postUpdateInfoGuestPayment');
						$router->get('info/{code?}', 'GuestPaymentController@getInfoGuestPayment');
						$router->post('send-mail/{code}', 'GuestPaymentController@postMailInfoGuestPayment');
            $router->post('process', 'GuestPaymentController@updateProcessGuestPayment');
			});
        });
    }
}
