<?php

namespace VuleApps\LwcPortal\Traits;

use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\Settings as WordSettings;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\SimpleType\Jc;
use PhpOffice\PhpWord\SimpleType\JcTable;
use PhpOffice\PhpWord\Style\ListItem;
use PhpOffice\PhpWord\Style\Font;

trait SetupMF
{
    public function master_file_one($folder, $params)
    {
        $phpWord = new PhpWord();
        WordSettings::setCompatibility(null);

        $fontStyleName = 'rStyle';
        $phpWord->addFontStyle($fontStyleName, array('bold' => true, 'italic' => true, 'size' => 10, 'allCaps' => true, 'doubleStrikethrough' => true));

        $paragraphStyleName = 'pStyle';
        $phpWord->addParagraphStyle($paragraphStyleName, array('alignment' => Jc::CENTER, 'spaceAfter' => 100));

        $phpWord->addTitleStyle(1, array('bold' => true, 'size' => 20), array('spaceAfter' => 240));

        $multilevelNumberingStyleName = 'multilevel';
        $phpWord->addNumberingStyle(
            $multilevelNumberingStyleName,
            array(
                'type'   => 'multilevel',
                'levels' => array(
                    array('format' => 'decimal', 'text' => '%1.', 'left' => 360, 'hanging' => 360, 'tabPos' => 360, 'bold' => true),
                    array('format' => 'upperLetter', 'text' => '%2.', 'left' => 720, 'hanging' => 360, 'tabPos' => 720, 'bold' => true),
                ),
            )
        );

        $predefinedMultilevelStyle = array('listType' => ListItem::TYPE_NUMBER_NESTED);

        $fancyTableStyleName = 'Fancy Table';
        $fancyTableStyle = array('borderSize' => 6, 'cellMargin' => 80, 'alignment' => JcTable::CENTER);
        $fancyTableCellStyle = array('valign' => 'center');
        $fancyTableFontStyle = array('bold' => true);
        $phpWord->addTableStyle($fancyTableStyleName, $fancyTableStyle);

        $section = $phpWord->addSection();

        $section->addTitle(
            'Student Submission', 1
        );

        $section->addListItem('PROGRAMME DETAILS', 0, null, $multilevelNumberingStyleName);

        $section->addTextBreak(1);

        $table = $section->addTable($fancyTableStyleName);
        $table->addRow();
        $table->addCell(1750)->addText("Name: ", $fancyTableFontStyle);
        $table->addCell(1750)->addText(isset($params['user']->name) ? htmlentities($params['user']->name) : null, $fancyTableCellStyle);
        $table->addCell(1750)->addText("ID No.", $fancyTableFontStyle);
        $table->addCell(1750)->addText(isset($params['user']->id) ? $params['user']->id : null, $fancyTableCellStyle);
        $table->addCell(1750)->addText("Enrolment date:", $fancyTableFontStyle);
        $table->addCell(1750)->addText(isset($params['module']->time_start) ? date('d/m/Y', strtotime($params['module']->time_start)) : null, $fancyTableCellStyle);

        $table->addRow();
        $table->addCell(1750)->addText("Assignment title:", $fancyTableFontStyle);
        $table->addCell(1750)->addText(isset($params['module']->title) ? $params['module']->title : null, $fancyTableCellStyle);
        $table->addCell(1750)->addText("Assignment No.", $fancyTableFontStyle);
        $table->addCell(1750)->addText(isset($params['module']->title) ? $params['module']->title : null, $fancyTableCellStyle);
        $table->addCell(1750)->addText("", $fancyTableFontStyle);
        $table->addCell(1750)->addText("", $fancyTableCellStyle);

        $table->addRow();
        $table->addCell(1750)->addText("Course Title:", $fancyTableFontStyle);
        $table->addCell(1750)->addText(isset($params['course']) ? htmlentities($params['course']) : null, $fancyTableCellStyle);
        $table->addCell(1750)->addText("Due date:", $fancyTableFontStyle);
        $table->addCell(1750)->addText(isset($params['module']->time_end) ? date('d/m/Y', strtotime($params['module']->time_end)) : null, $fancyTableCellStyle);
        $table->addCell(1750)->addText("", $fancyTableFontStyle);
        $table->addCell(1750)->addText("", $fancyTableCellStyle);

        $section->addTextBreak(1);

        $section->addTitle(
            'Student Declaration', 2
        );

        $source = url('lwcportal/images/logo.png');

        $section->addImage(
            $source,
            array(
                'width'            => \PhpOffice\PhpWord\Shared\Converter::cmToPixel(8),
                'height'           => \PhpOffice\PhpWord\Shared\Converter::cmToPixel(3),
                'positioning'      => \PhpOffice\PhpWord\Style\Image::POSITION_ABSOLUTE,
                'posHorizontal'    => \PhpOffice\PhpWord\Style\Image::POSITION_HORIZONTAL_RIGHT,
                'posHorizontalRel' => \PhpOffice\PhpWord\Style\Image::POSITION_RELATIVE_TO_PAGE,
                'posVerticalRel'   => \PhpOffice\PhpWord\Style\Image::POSITION_RELATIVE_TO_PAGE,
                'marginLeft'       => \PhpOffice\PhpWord\Shared\Converter::cmToPixel(1.5),
                'marginTop'        => \PhpOffice\PhpWord\Shared\Converter::cmToPixel(1.55),
                'marginRight'      => \PhpOffice\PhpWord\Shared\Converter::cmToPixel(100),
                'marginBottom'     => \PhpOffice\PhpWord\Shared\Converter::cmToPixel(1.55),
            )
        );

        $section->addText(
            'I am aware of and understand the policy on plagiarism and I certify that this assignment is my own work, except where indicated by referencing, and that I have followed good academic practices as detailed in the OBS Student Handbook.'
        );

        $section->addTextBreak(1);

        //Xem canh trái phải
        if(isset($params['user']->name)) {
            $section->addText(
                'Signed: ' . htmlentities($params['user']->name)  .'                                            Date: '. date('d/m/Y')
            );
        }

        $section->addTextBreak(1);

        $section->addTitle(
            'Mentor Declaration:', 2
        );

        $section->addText(
            'I declare that the above student is known to me in my capacity as Mentor and that this is the work of that student:'
        );

        $section->addText(
            'Signed: ' . $params['teacher']->name
        );

        $section->addText(
            'Designation:'
        );

        $section->addTextBreak(10);

        $section->addListItem('ASSESSOR', 0, null, $multilevelNumberingStyleName);

        $section->addText(
            'Grade Awarded: Pass/ Merit/ Diction/Referral'
        );

        $section->addText(
            'General Comments:'
        );

        $section->addTextBreak(10);

        $table2 = $section->addTable($fancyTableStyleName);
        $table2->addRow();
        $table2->addCell(1750)->addText("Hand in date:", $fancyTableFontStyle);
        $table2->addCell(1750)->addText(isset($params['last_task']['updated_at']) ? date('m/d/Y', strtotime($params['last_task']['updated_at'])) : null, $fancyTableCellStyle);
        $table2->addCell(1750)->addText("Date returned:", $fancyTableFontStyle);
        $table2->addCell(1750)->addText("", $fancyTableCellStyle);
        $table2->addCell(1750)->addText("Signed tutor:", $fancyTableFontStyle);
        $table2->addCell(1750)->addText(isset($params['teacher']->name) ? htmlentities($params['teacher']->name) : null, $fancyTableCellStyle);

        $this->createFileDocx($folder. '/masterfile01.docx', $phpWord);

        return true;
    }

    public function master_file_two($folder, $params)
    {
        $phpWord = new PhpWord();
        WordSettings::setCompatibility(null);

        $fontStyleName = 'rStyle';
        $phpWord->addFontStyle($fontStyleName, array('bold' => true, 'italic' => true, 'size' => 10, 'allCaps' => true, 'doubleStrikethrough' => true));

        $paragraphStyleName = 'pStyle';
        $phpWord->addParagraphStyle($paragraphStyleName, array('alignment' => Jc::CENTER, 'spaceAfter' => 100));

        $phpWord->addTitleStyle(1, array('bold' => true, 'size' => 20), array('spaceAfter' => 240));

        $fancyTableStyleName = 'Fancy Table';
        $fancyTableStyle = array('borderSize' => 6, 'cellMargin' => 80, 'alignment' => JcTable::CENTER);

        $cellCol2Span = array('gridSpan' => 2, 'valign' => 'center');
        $cellCol3Span = array('gridSpan' => 3, 'valign' => 'center');
        $cellCol5Span = array('gridSpan' => 5, 'valign' => 'center');

        $cellRowSpan = array('vMerge' => 'restart', 'valign' => 'center');
        $cellRowContinue = array('vMerge' => 'continue');

        $fancyTableCellStyle = array('valign' => 'center');
        $fancyTableFontStyle = array('bold' => true);
        $phpWord->addTableStyle($fancyTableStyleName, $fancyTableStyle);

        $section = $phpWord->addSection();

        $section->addTitle(
            'Assignment Front Sheet', 1
        );

        $source = url('lwcportal/images/logo.png');

        $section->addImage(
            $source,
            array(
                'width'            => \PhpOffice\PhpWord\Shared\Converter::cmToPixel(8),
                'height'           => \PhpOffice\PhpWord\Shared\Converter::cmToPixel(3),
                'positioning'      => \PhpOffice\PhpWord\Style\Image::POSITION_ABSOLUTE,
                'posHorizontal'    => \PhpOffice\PhpWord\Style\Image::POSITION_HORIZONTAL_RIGHT,
                'posHorizontalRel' => \PhpOffice\PhpWord\Style\Image::POSITION_RELATIVE_TO_PAGE,
                'posVerticalRel'   => \PhpOffice\PhpWord\Style\Image::POSITION_RELATIVE_TO_PAGE,
                'marginLeft'       => \PhpOffice\PhpWord\Shared\Converter::cmToPixel(1.5),
                'marginTop'        => \PhpOffice\PhpWord\Shared\Converter::cmToPixel(1.55),
                'marginRight'      => \PhpOffice\PhpWord\Shared\Converter::cmToPixel(1.55),
                'marginBottom'     => \PhpOffice\PhpWord\Shared\Converter::cmToPixel(1.55),
            )
        );

        $table = $section->addTable($fancyTableStyleName);

        $table->addRow();
        $table->addCell(4000, $cellCol2Span)
            ->addText("Learner Name: ". htmlentities($params['user']->name));
        $table->addCell(6000, $cellCol3Span)->addText("Learner Registration ID: ". $params['user']->id);

        $table->addRow();
        $cell = $table->addCell(10000, $cellCol5Span);
        $cell->addText("Qualification: " .htmlentities($params['module']->course->title));
        $cell->addTextBreak(0.1);
        $cell->addText("Unit Title: " . $params['module']->course->id);
        $cell->addTextBreak(0.1);
        $cell->addText("Assignment Title: ".  htmlentities($params['module']->title));
        $cell->addTextBreak(0.1);
        $cell->addText("Assignment issued: "  .date('d/m/Y', strtotime($params['module']->time_start)));
        $cell->addTextBreak(0.1);
        $cell->addText("Assignment submitted: " . date('d/m/Y', strtotime($params['module']->time_end)));

        $table->addRow();
        $table->addCell(2000)->addText("Learning Outcomes", $fancyTableFontStyle);
        $table->addCell(2000)->addText("Assessment Criteria", $fancyTableFontStyle);
        $table->addCell(2000)->addText("A", $fancyTableFontStyle);
        $table->addCell(2000)->addText("NYA", $fancyTableFontStyle);
        $table->addCell(2000)->addText("Evidence to show achievement of the LO", $fancyTableFontStyle);

        //Element modules
        foreach($params['los'] as $lokey => $lo) {

            $tasks = \VuleApps\LwcPortal\Models\Task::where('lo_id', $lo->id);

            foreach($tasks->get() as $taskkey => $task) {

                $user_task = \VuleApps\LwcPortal\Models\UserTask::where([
                    'user_id'   => $params['user']->id,
                    'task_id'   => $task->id
                ])->first();

                if($taskkey == 0) {
                    $table->addRow();
                    $table->addCell(2000, $cellRowSpan)->addText("LO ". ($lokey + 1) . " " . htmlentities($lo->title), $fancyTableCellStyle);
                    $table->addCell(2000)->addText(($lokey + 1) .".". ($taskkey + 1) ." " . htmlentities($task->title), $fancyTableCellStyle);
                    $table->addCell(2000, $cellRowSpan)->addText("", $fancyTableCellStyle);
                    $table->addCell(2000, $cellRowSpan)->addText("", $fancyTableCellStyle);
                    $table->addCell(2000, $cellRowSpan)->addText("", $fancyTableCellStyle);
                }
                else {
                    $table->addRow();
                    $table->addCell(null, $cellRowContinue);
                    $table->addCell(2000)->addText(($lokey + 1) .".". ($taskkey + 1) ." " . htmlentities($task->title), $fancyTableCellStyle);
                    $table->addCell(null, $cellRowContinue);
                    $table->addCell(null, $cellRowContinue);
                    $table->addCell(null, $cellRowContinue);
                }


                $table->addRow();
                $table->addCell(null, $cellRowContinue);
                $table->addCell(2000)->addText(isset($user_task->teacher_comment) ? htmlentities($user_task->teacher_comment) : null, $fancyTableCellStyle);
                $table->addCell(null, $cellRowContinue);
                $table->addCell(null, $cellRowContinue);
                $table->addCell(null, $cellRowContinue);
            }
        }
        //end element

        $section->addTextBreak(1);

        $table2 = $section->addTable($fancyTableStyleName);
        $table2->addRow();
        $table2->addCell(2000)->addText("Assessor:", $fancyTableFontStyle);
        $table2->addCell(6000)->addText(isset($params['module']->title) ? htmlentities($params['module']->title) : null, $fancyTableCellStyle);

        $table2->addRow();
        $table2->addCell(2000)->addText("Signature:", $fancyTableFontStyle);
        $table2->addCell(6000)->addText("", $fancyTableCellStyle);

        $table2->addRow();
        $table2->addCell(2000)->addText("Date:", $fancyTableFontStyle);
        $table2->addCell(6000)->addText(isset($params['last_task']['updated_at']) ? date('m/d/Y', strtotime($params['last_task']['updated_at'])) : null, $fancyTableCellStyle);

        $section->addTextBreak(3);
        $section->addText(
            'A = Achieved'
        );
        $section->addText(
            'NYA = Not Yet Achieved ( when choose preferal in 1st page)'
        );

        $this->createFileDocx($folder. '/masterfile02.docx', $phpWord);
    }

    public function master_file_three($folder)
    {
        $phpWord = new PhpWord();
        WordSettings::setCompatibility(null);

        $fontStyleName = 'rStyle';
        $phpWord->addFontStyle($fontStyleName, array('bold' => true, 'italic' => true, 'size' => 10, 'allCaps' => true, 'doubleStrikethrough' => true));

        $paragraphStyleName = 'pStyle';
        $phpWord->addParagraphStyle($paragraphStyleName, array('alignment' => Jc::CENTER, 'spaceAfter' => 100));

        $phpWord->addTitleStyle(1, array('bold' => true, 'size' => 20), array('spaceAfter' => 240));

        $section = $phpWord->addSection();

        $section->addTitle(
            'Presentation slide', 1
        );

        $this->createFileDocx($folder. '/masterfile03.docx', $phpWord);
    }

    public function master_file_four($folder, $params)
    {
        $phpWord = new PhpWord();
        WordSettings::setCompatibility(null);

        $fontStyleName = 'rStyle';
        $phpWord->addFontStyle($fontStyleName, array('bold' => true, 'italic' => true, 'size' => 10, 'allCaps' => true, 'doubleStrikethrough' => true));

        $paragraphStyleName = 'pStyle';
        $phpWord->addParagraphStyle($paragraphStyleName, array('alignment' => Jc::CENTER, 'spaceAfter' => 100));

        $phpWord->addTitleStyle(1, array('bold' => true, 'size' => 20), array('spaceAfter' => 240));

        $multilevelNumberingStyleName = 'multilevel';
        $phpWord->addNumberingStyle(
            $multilevelNumberingStyleName,
            array(
                'type'   => 'multilevel',
                'levels' => array(
                    array('format' => 'decimal', 'text' => '%1.', 'left' => 360, 'hanging' => 360, 'tabPos' => 360, 'bold' => true),
                    array('format' => 'upperLetter', 'text' => '%2.', 'left' => 720, 'hanging' => 360, 'tabPos' => 720, 'bold' => true),
                ),
            )
        );

        $section = $phpWord->addSection();

        $section->addTitle(
            htmlentities($params['module']->title), 1
        );

        foreach($params['tasks'] as $key => $task) {
            $section->addListItem('Question '.  ($key + 1)  .' : ' . htmlentities($task->task->question) , 1, null, $multilevelNumberingStyleName);
            $section->addText(
                'Answer:'
            );
            if(isset($task->answer)) {

                $answer = html_entity_decode(strip_tags($task->answer));

                $section->addText(
                    $answer
                );
            }
            $section->addTextBreak(1);
        }

        $this->createFileDocx($folder. '/masterfile04.docx', $phpWord);
    }

    public function master_file($folder, $params)
    {
        // dd($params);
        $phpWord = new PhpWord();

        $section = $phpWord->addSection();
        $header = $section->addHeader();
        $header->addImage(url('logo.png'), [
            'width'  => 150,
        ]);

        $section->addText('From SAF', [
            'bold' => true, 
            'size' => 13,
        ], [
            'alignment' => Jc::RIGHT
        ]);

        $phpWord->addTitleStyle(1, array(
            'bold' => true, 
            'size' => 14
        ), array(
            'spaceAfter' => 900,
            'alignment' => Jc::CENTER
        ));
        $section->addTitle('Student Submission and Feedback Form');
        
        $phpWord->addNumberingStyle(
            'multilevel',
            array(
                'type' => 'multilevel',
                'levels' => array(
                    array('format' => 'decimal', 'text' => '%1.', 'left' => 360, 'hanging' => 360, 'tabPos' => 360),
                    array('format' => 'upperLetter', 'text' => '%2.', 'left' => 720, 'hanging' => 360, 'tabPos' => 720),
                )
            )
        );
        $section->addListItem('PROGRAMME DETAILS', 0, [
            'bold' => true, 
            'size' => 12,
            'spaceAfter' => 1000
        ], 'multilevel');


        $fancyTableStyleName = 'Fancy Table';
        $fancyTableStyle = array('borderSize' => 6, 'cellMargin' => 10, 'alignment' => JcTable::CENTER);
        $fancyTableCellStyle = array('valign' => 'center');
        $fancyTableFontStyle = array('bold' => true);
        $phpWord->addTableStyle($fancyTableStyleName, $fancyTableStyle);

        $table = $section->addTable($fancyTableStyleName);
        $table->addRow();
        $table->addCell(1750)->addText("Name: ", $fancyTableFontStyle);
        $table->addCell(1750)->addText(isset($params['learner']->name) ? htmlentities($params['learner']->name) : null, $fancyTableCellStyle);
        $table->addCell(1750)->addText("ID No.", $fancyTableFontStyle);
        $table->addCell(1750)->addText(isset($params['learner']->id) ? $params['learner']->id : null, $fancyTableCellStyle);
        $table->addCell(1750)->addText("Enrolment date:", $fancyTableFontStyle);
        $table->addCell(1750)->addText(isset($params['module']->time_start) ? date('d/m/Y', strtotime($params['module']->time_start)) : null, $fancyTableCellStyle);

        $table->addRow();
        $table->addCell(1750)->addText("Programe title:", $fancyTableFontStyle);
        $table->addCell(1750)->addText(isset($params['module']->title) ? $params['module']->title : null, $fancyTableCellStyle);
        $table->addCell(1750)->addText("Subject Tutor", $fancyTableFontStyle);
        $table->addCell(1750, array('gridSpan' => 3))->addText(isset($params['teacher']->name) ? $params['teacher']->name : null, $fancyTableFontStyle);

        $table->addRow();
        $table->addCell(1750)->addText("Course Title:", $fancyTableFontStyle);
        $table->addCell(1750)->addText(isset($params['course']->title) ? $params['course']->title : null, $fancyTableCellStyle);
        $table->addCell(1750)->addText("Due date:", $fancyTableFontStyle);
        $table->addCell(1750, array('gridSpan' => 3))->addText(isset($params['module']->time_end) ? date('d/m/Y', strtotime($params['module']->time_end)) : null, $fancyTableCellStyle);    

        $section->addText(
            'Student Declaration:', [
                'bold' => true, 
                'size' => 11
            ], [
                'spaceBefore' => 500
            ]
        );
        $section->addText(
            'I declare that the work submitted is my own work', [
                'size' => 10
            ]
        );
        $section->addText(
            'Signed:', [
                'bold' => true, 
                'size' => 11
            ], [
                'spaceBefore' => 1000
            ]
        );
        $section->addText(
            'Mentor Declaration:', [
                'bold' => true, 
                'size' => 11
            ]
        );
        $section->addText(
            'I declare that the above student is known to me in my capacity as Mentor and that this is the work of that student:', [
                'size' => 10
            ]
        );
        $section->addText(
            'Signed:', [
                'bold' => true, 
                'size' => 10
            ], [
                'spaceBefore' => 500
            ]
        );
        $section->addText(
            'Designation:', [
                'bold' => true, 
                'size' => 10
            ], [
                'spaceBefore' => 500
            ]
        );
        $section->addPageBreak();


        $section->addListItem('ASSESSMENT FEEDBACK', 0, [
            'bold' => true, 
            'size' => 12
        ], 'multilevel');

        $section->addText(
            'Reference must be made to the suggested evidence provided for each assignmen', [
                'bold' => true, 
                'size' => 10
            ]
        );

        $assessmentTableStyleName = 'Assessment Table';
        $asessmentTableStyle = array('borderSize' => 6, 'cellMargin' => 10, 'alignment' => JcTable::CENTER);
        $assessmentTableCellStyle = array('valign' => 'center', 'width' => 100);
        $assessmentTableFontStyle = array('bold' => true);
        $phpWord->addTableStyle($assessmentTableStyleName, $asessmentTableStyle);

        $table = $section->addTable($assessmentTableStyleName);
        $table->addRow();
        $table->addCell(1750)->addText("Activity Title", $fancyTableFontStyle);
        $table->addCell(10000)->addText("", $fancyTableCellStyle);

        $table->addRow();
        $table->addCell(1750)->addText("LO", $fancyTableFontStyle);
        $table->addCell(10000)->addText("Feedback/evidence", $fancyTableCellStyle);

        foreach($params['tasks'] as $task) {
            $table->addRow();
            $table->addCell(1750)->addText(isset($task['task']['title']) ? $task['task']['title'] : null, $fancyTableFontStyle);
            $table->addCell(10000)->addText(isset($task['teacher_comment']) ? $task['teacher_comment'] : null, $fancyTableCellStyle);
        }

        $section->addText(
            'Grade Awarded: Pass/Fail/Referral (please circle)', [
                'bold' => true, 
                'size' => 13,
                ''
            ]
        );
        $section->addText(
            'General Comments:', [
                'bold' => true, 
                'size' => 13
            ], [
                'spaceAfter' => 3000
            ]
        );

        $commentTableStyleName = 'Comment Table';
        $commentTableStyle = array('borderSize' => 6, 'cellMargin' => 10, 'alignment' => JcTable::CENTER);
        $commentTableCellStyle = array('valign' => 'center', 'width' => 100);
        $commentTableFontStyle = array('bold' => true);
        $phpWord->addTableStyle($commentTableStyleName, $commentTableStyle);

        $table = $section->addTable($assessmentTableStyleName);
        $table->addRow();
        $table->addCell(1750)->addText("Hand in date:", $commentTableFontStyle);
        $table->addCell(1750)->addText(null, $commentTableCellStyle);
        $table->addCell(1750)->addText("Date returned:", $commentTableFontStyle);
        $table->addCell(1750)->addText(null, $commentTableCellStyle);
        $table->addCell(1750)->addText("Signed Tutor", $commentTableFontStyle);
        $table->addCell(1750)->addText("", $commentTableCellStyle);

        $section->addPageBreak();

        $phpWord->addTitleStyle(1, array(
            'bold' => true, 
            'size' => 14
        ), array(
            'spaceAfter' => 240
        ));
        $section->addTitle('Assignment');

        $assignmentTableStyleName = 'Assessment Table';
        $assignmentTableStyle = array('borderSize' => 6, 'cellMargin' => 10, 'alignment' => JcTable::CENTER);
        $assignmentTableCellStyle = array('valign' => 'center', 'width' => 100);
        $assignmentTableFontStyle = array('bold' => true);
        $phpWord->addTableStyle($assignmentTableStyleName, $assignmentTableStyle);

        $table = $section->addTable($assignmentTableStyleName);
        $table->addRow();
        $table->addCell(5000, array('gridSpan' => 3))->addText(isset($params['learner']->name) ? "Learner Name: " . $params['learner']->name : "Learner Name: ", $commentTableFontStyle);
        $table->addCell(5000, array('gridSpan' => 3))->addText(isset($params['learner']->id) ? "Learner Registration ID: " . $params['learner']->id : "Learner Registration ID: ", $commentTableFontStyle);

        $table->addRow();
        $table->addCell(5000, array('gridSpan' => 6))->addText("Qualification:", $commentTableFontStyle);

        $table->addRow();
        $tb = $table->addCell(5000, array('gridSpan' => 6));
        $tb->addText(isset($params['course']->title) ? "Unit Title/Level:" . $params['course']->title : "Unit Title/Level:", $commentTableFontStyle);
        $tb->addText("Assessment criteria covered", $commentTableFontStyle);
        $tb->addText("in the assignment:", $commentTableFontStyle);

        $table->addRow();
        $table->addCell(5000, array('gridSpan' => 6))
            ->addText("", $commentTableFontStyle);

        $table->addRow();
        $table->addCell(5000, array('gridSpan' => 6))
            ->addText("Assignment Submission Schedule: ", $commentTableFontStyle);

        $table->addRow();
        $table->addCell(5000, array('gridSpan' => 2))
            ->addText("Assignment/Tasks", $commentTableFontStyle);
        $table->addCell(5000, array('gridSpan' => 2))
            ->addText("Assignment/tasks hand out date", $commentTableFontStyle);
        $table->addCell(5000, array('gridSpan' => 2))
            ->addText("Assignment/tasks hand in date", $commentTableFontStyle);

        foreach($params['tasks'] as $task) {
            $table->addRow();
            $table->addCell(5000, array('gridSpan' => 2))
                ->addText(isset($task['task']['title']) ? $task['task']['title'] : null, $commentTableFontStyle);
            $table->addCell(5000, array('gridSpan' => 2))
                ->addText(isset($task['created_at']) ? date('m-d-Y', strtotime($task['created_at'])) : null, $commentTableFontStyle);
            $table->addCell(5000, array('gridSpan' => 2))
                ->addText(isset($task['updated_at']) ? date('m-d-Y', strtotime($task['updated_at'])) : null, $commentTableFontStyle);
        }
        
        $table->addRow();
        $table->addCell(5000, array('gridSpan' => 6))
            ->addText("Assessment Decisions: Criteria achieved or not achieved", $commentTableFontStyle);

        $i = 1;
        foreach($params['tasks'] as $task) {
            if($i == 1) {
                $table->addRow();
            }
            $table->addCell(5000)->addText(isset($task['task']['title']) ? $task['task']['title'] : null, $commentTableFontStyle);
            $table->addCell(5000)->addText(isset($task['grade']) ? $task['grade'] : null, $commentTableFontStyle);
            $i++;
            if($i == 3) {
               $i = 1;
            }
        }
        if($i > 1) {
            for($f = 1; $f <= $i; $f++) {
                $table->addCell(5000)->addText("", $commentTableFontStyle);
                $table->addCell(5000)->addText("", $commentTableFontStyle);
            }
        }
        
        $table->addRow();
        $table->addCell(5000, array('gridSpan' => 6))
            ->addText("Overall Comments:", array(
                'bold' => true,
            ), array(
                'spaceAfter' => 1000
            ));
    
        $objWriter = IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save($folder . '/master-file.docx');
    }

    private function createFileDocx($file_name, $phpWord)
    {
        $path = storage_path('tmp/master_files/'.$file_name);

        $dir = dirname($path);

        if (!is_dir($dir)) {
            @mkdir($dir, 0777, true);
        }

        $objWriter = IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save($path);
    }
}