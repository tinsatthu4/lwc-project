<?php
namespace VuleApps\LwcPortal\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;
use Mail;
use SettingRepository;

class SendEmailJob extends Job implements ShouldQueue {
	use InteractsWithQueue, SerializesModels;

    protected $subject;
    protected $data;
    protected $to;
    protected $from;
    protected $template;

    public function __construct($subject, $from, $to, $template, $data = []) {
        $this->subject = $subject;
        $this->data = $data;
        $this->to = $to;
        $this->from = $from;
        $this->template = $template;
    }


    public function handle() {
		try {
			Mail::send($this->template, $this->data, function ($message) {
				$message->from(config('mail.from.address'), config('mail.from.name'));
				$message->to($this->to)->subject($this->subject);
			});
		} catch (\Exception $e) {
			Log::error($e->getMessage());
		}
	}
}