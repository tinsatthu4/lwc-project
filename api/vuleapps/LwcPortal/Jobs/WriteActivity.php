<?php namespace VuleApps\LwcPortal\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use VuleApps\LwcPortal\Models\Activity;

class WriteActivity extends Job implements ShouldQueue {
	use InteractsWithQueue, SerializesModels;
	protected $user_id;
	protected $type;
	protected $data;

	function __construct($user_id, $type, $data) {
		$this->user_id = $user_id;
		$this->type = $type;
		$this->data = $data;
	}

	function handle() {
		Activity::create([
			'user_id' => $this->user_id,
			'type' => $this->type,
			'description' => $this->data
		]);
	}
}