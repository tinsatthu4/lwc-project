<?php
namespace VuleApps\LwcPortal;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as RouterServiceProviderLaravel;
use Route;

class RouterServiceProviderMockup extends RouterServiceProviderLaravel
{
	/**
	 * Define your route model bindings, pattern filters, etc.
	 *
	 * @param  \Illuminate\Routing\Router  $router
	 * @return void
	 */
	public function boot(Router $router)
	{
		parent::boot($router);
	}

	/**
	 * Define the routes for the application.
	 *
	 * @param  \Illuminate\Routing\Router  $router
	 * @return void
	 */
	public function map(Router $router)
	{
		$this->mapWebRoutesMockup($router);
	}

	/**
	 * Define the "web" routes for the application.
	 *
	 * These routes all receive session state, CSRF protection, etc.
	 *
	 * @param  \Illuminate\Routing\Router  $router
	 * @return void
	 */
	protected function mapWebRoutesMockup(Router $router)
	{
		$router->group(['prefix' => "api"], function(Router $router) {
			// $router->get('my/modules/{course_id}', 'VuleApps\LwcPortal\Controllers\GuestPaymentController@getMyModules');
			// $router->group(['prefix' => 'guest-payments'], function(Router $router) {
			// 	$router->get('info', function() {
			// 		return response()->json([
			// 			'data' => "Hello world"
			// 		], 200);
			// 	});
			// });
			// $router->group(['prefix' => 'guest-payments'], function(Router $router) {
			// 	$router->get('info/{code?}', function() {
			// 		return response()->json([
			// 			'data' => [
			// 	        {
			// 	            "id": 5,
			// 	            "code": "59d6f2c49d79b",
			// 	            "link": "",
			// 	            "payment_method": "unknown",
			// 	            "base64": "",
			// 	            "desc": "",
			// 	            "name": "Tek",
			// 	            "phone": "0986999999",
			// 	            "email": "cp.phamthong@gmail.com",
			// 	            "course": "",
			// 	            "price": "",
			// 	            "programme_name": "Postgraduate Diploma – Organisational Transformation",
			// 	            "surname_name": "Mr",
			// 	            "birthday": "1994-07-32",
			// 	            "country": "VietNam",
			// 	            "nationality_of": "VietNam",
			// 	            "gender": "Male",
			// 	            "highest_education_level": "Professor",
			// 	            "path_studen_photo": "http://via.placeholder.com/350x150",
			// 	            "path_transcript": "http://via.placeholder.com/350x150",
			// 	            "address": null
			// 	        },
			// 	        {
			// 	            "id": 4,
			// 	            "code": "59d6f2075677a",
			// 	            "link": "",
			// 	            "payment_method": "unknown",
			// 	            "base64": "",
			// 	            "desc": "",
			// 	            "name": "Tek",
			// 	            "phone": "0986999999",
			// 	            "email": "cp.phamthong@gmail.com",
			// 	            "course": "",
			// 	            "price": "",
			// 	            "programme_name": "Postgraduate Diploma – Organisational Transformation",
			// 	            "surname_name": "Mr",
			// 	            "birthday": "1994-07-32",
			// 	            "country": "VietNam",
			// 	            "nationality_of": "VietNam",
			// 	            "gender": "Male",
			// 	            "highest_education_level": "Professor",
			// 	            "path_studen_photo": "http://via.placeholder.com/350x150",
			// 	            "path_transcript": "http://via.placeholder.com/350x150",
			// 	            "address": null
			// 	        },
			// 	        {
			// 	            "id": 3,
			// 	            "code": "59d6f1d50d4f6",
			// 	            "link": "",
			// 	            "payment_method": "unknown",
			// 	            "base64": "",
			// 	            "desc": "",
			// 	            "name": "Tek",
			// 	            "phone": "",
			// 	            "email": "",
			// 	            "course": "",
			// 	            "price": "",
			// 	            "programme_name": "Postgraduate Diploma – Organisational Transformation",
			// 	            "surname_name": "Mr",
			// 	            "birthday": "1994-07-32",
			// 	            "country": "VietNam",
			// 	            "nationality_of": "VietNam",
			// 	            "gender": "Male",
			// 	            "highest_education_level": "Professor",
			// 	            "path_studen_photo": "http://via.placeholder.com/350x150",
			// 	            "path_transcript": "http://via.placeholder.com/350x150",
			// 	            "address": null
			// 	        },
			// 	        {
			// 	            "id": 2,
			// 	            "code": "59d5cbfe86680",
			// 	            "link": "",
			// 	            "payment_method": "unknown",
			// 	            "base64": "",
			// 	            "desc": "",
			// 	            "name": "",
			// 	            "phone": "",
			// 	            "email": "",
			// 	            "course": "",
			// 	            "price": "",
			// 	            "programme_name": "Postgraduate Diploma – Organisational Transformation",
			// 	            "surname_name": "Tek",
			// 	            "birthday": "1994-07-32",
			// 	            "country": "VietNam",
			// 	            "nationality_of": "VietNam",
			// 	            "gender": "Male",
			// 	            "highest_education_level": "Professor",
			// 	            "path_studen_photo": "http://via.placeholder.com/350x150",
			// 	            "path_transcript": "http://via.placeholder.com/350x150",
			// 	            "address": null
			// 	        },
			// 	        {
			// 	            "id": 1,
			// 	            "code": "59d5cbcdad467",
			// 	            "link": "",
			// 	            "payment_method": "paypal",
			// 	            "base64": "",
			// 	            "desc": "",
			// 	            "name": "",
			// 	            "phone": "",
			// 	            "email": "",
			// 	            "course": "",
			// 	            "price": "",
			// 	            "programme_name": null,
			// 	            "surname_name": "",
			// 	            "birthday": "",
			// 	            "country": "",
			// 	            "nationality_of": "",
			// 	            "gender": "",
			// 	            "highest_education_level": "",
			// 	            "path_studen_photo": "",
			// 	            "path_transcript": "",
			// 	            "address": null
			// 	        }
			// 		]], 200);
			// 	});
			// });
			
		});
	}
}