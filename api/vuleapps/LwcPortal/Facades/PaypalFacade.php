<?php
namespace VuleApps\LwcPortal\Facades;

use Illuminate\Support\Facades\Facade as LaravelFacade;
use VuleApps\LwcPortal\Repository\MyPayPal;

class PaypalFacade extends LaravelFacade{

	protected static function getFacadeAccessor()
	{
		return MyPayPal::class;
	}
}