<?php namespace VuleApps\LwcPortal\Models;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Checkout extends Model {
	const TYPE_MODULE = 'TYPE_MODULE';
	const TYPE_COURSE = 'TYPE_COURSE';

	protected $fillable = [
		'post_id', 'user_id',
		'checkout_type',
//		'payment_token',
		'price', 'description'
	];

	function scopeIsModule($q) {
		return $q->where('checkout_type', self::TYPE_MODULE);
	}

	function scopeIsCourse($q) {
		return $q->where('checkout_type', self::TYPE_COURSE);
	}

    function user() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    function course() {
        return $this->belongsTo(Counrse::class, 'post_id', 'id');
    }
}