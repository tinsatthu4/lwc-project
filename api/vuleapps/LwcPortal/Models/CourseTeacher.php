<?php namespace VuleApps\LwcPortal\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use VuleApps\LwcPortal\Models\Task;

class CourseTeacher extends Model {
	protected $table = 'course_teachers';
	public $timestamps = false;
}