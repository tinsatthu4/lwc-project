<?php namespace VuleApps\LwcPortal\Models;

use Illuminate\Database\Eloquent\Model;

class Task extends Model {
	const TYPE_MCQ = 'mcq';
	const TYPE_ASSIGNMENT = 'assignment';
	const TYPE_OF_GRADE_MERIT = 'merit';
	const TYPE_OF_GRADE_DICTION = 'diction';
	const TYPE_OF_GRADE_NONE = 'none';

	protected $fillable = [
		'title', 'type', 'keywords', 'preference', 'post_id', 'question', 'teacher_id', 'scenarios', 'sort',
		'type_of_grade', 'lo_id', 'lecture_recommendation'
	];

	public function getPreferenceAttribute() {
		if(!empty($this->attributes['preference']))
			return json_decode($this->attributes['preference'], true);

		return [];
	}

	public function setPreferenceAttribute($value) {
		if(!empty($value) && is_array($value)) {
			$this->attributes['preference'] = json_encode($value);
			return;
		}

		$this->attributes['preference'] = json_encode([]);
		return;
	}

	function scopeHasTypeOfGrade($q, $tog) {
        if($tog == 'none') return $q;
		return $q->where('type_of_grade', $tog);
	}

	function scopeHasTeacher($query, $teacher_id) {
		return $query->where('teacher_id', $teacher_id);
	}

	function scopeHasModule($query, $id) {
		return $query->where('post_id', $id);
	}

	function module() {
		return $this->belongsTo(Module::class, 'post_id');
	}

	function user_task() {
		return $this->hasMany(UserTask::class, 'task_id', 'id');
	}

	function lo() {
		return $this->belongsTo(LO::class, 'lo_id');
	}
}