<?php namespace VuleApps\LwcPortal\Models;

use App\User;
use VuleApps\LwcBackends\Models\DefaultPost;
use Illuminate\Database\Eloquent\Builder;

class Counrse extends DefaultPost {
	protected $table = 'posts';
	const TYPE = 'counrse';

	protected $fillable = [
		"title", "slug", "parent_id", "type", "image", "content", "excerpt", "sort", "status", "price",
		"meta_title", "meta_content", "meta_keywords", "teacher_id"
	];

	protected static function boot()
	{
		parent::boot();

		static::addGlobalScope('post_type', function(Builder $builder) {
			$builder->where('type', Counrse::TYPE);
		});
	}

	public function setSlugAttribute($value) {
		if(empty($value)) {
			$this->attributes['slug'] = str_slug($this->attributes['title']);
		}

		$this->attributes['slug'] = str_slug($value);
	}

    function user() {
        return $this->belongsToMany(User::class, 'user_counrses', 'post_id');
    }

	function teacher() {
		return $this->hasOne(CourseTeacher::class, 'post_id');
	}

    function module() {
        return $this->hasMany(Module::class, 'parent_id', 'id');
    }

    function modules() {
        return $this->hasMany(Module::class, 'parent_id', 'id');
    }

	function userCourse() {
		return $this->hasMany(UserCounrse::class, 'post_id');
	}
}