<?php namespace VuleApps\LwcPortal\Models;

use Illuminate\Database\Eloquent\Model;

class GuestPayment extends Model {
    protected $table = "guest_payments";

    const PAYMENT_PAYPAL    = "PAYPAL";
    const PROCESS_PENDING   = "pending"; // Mac Dinh
    const PROCESS_SEND_MAIL = "send_mail"; // KhiAdmin send mail success
    const PROCESS_PAYMENT   = "payment"; // Khi HS Da chon bat ky phuong thuc payment nao
    const PROCESS_COMPLETE  = "complete";
    
    protected $fillable = [
        "code",
        "name", "phone", "email", "course", "price",
        "link","payment_method", "base64", "desc",
        "programme_name", "given_name", "surname_name", "birthday", "country",
        "nationality_of", "gender", "highest_education_level", "path_studen_photo",
        "path_transcript", "process", "address", "note"
    ];

    public $timestamps = false;

    function scopeHasCode($q, $code) {
        if($code == 'all' || $code == null) return $q;
        return $q->where('code', $code);
    }

    function scopeHasPaymentMethod($q, $payment_method) {
        if($payment_method == 'all' || $payment_method == null) return $q;
        return $q->where('payment_method', $payment_method);
    }
 
    function scopeHasProcess($q, $process) {
        if($process == 'all' || $process == null) return $q;
        return $q->where('process', $process);
    }

    function scopeHasPhone($q, $phone) {
        if($phone == 'all' || $phone == null) return $q;
        return $q->where('phone', $phone);
    }

    function scopeHasEmail($q, $email) {
        if($email == 'all' || $email == null) return $q;
        return $q->where('email', $email);
    }

    function scopeHasCourse($q, $course) {
        if($course == 'all' || $course == null) return $q;
        return $q->where('course', $course);
    }

    function scopeHasPriceFrom($q, $price_from) {
        if($price_from == 'all' || $price_from == null) return $q;
        return $q->where('price_from', '>=',$price_from);
    }

    function scopeHasPriceTo($q, $price_from) {
        if($price_from == 'all' || $price_from == null) return $q;
        return $q->where('price_from', '<=',$price_from);
    }

}