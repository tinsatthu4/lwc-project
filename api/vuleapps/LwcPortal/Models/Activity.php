<?php namespace VuleApps\LwcPortal\Models;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model {
	const TYPE_USERTASK_SAVE = 'TYPE_USERTASK_SAVE';
	const TYPE_USERTASK_SUBMISSION = 'TYPE_USERTASK_SUBMISSION';
	const TYPE_USERMODULE_CREATE = 'TYPE_USERMODULE_CREATE'; //new checkout
	protected $fillable = [
		'user_id', 'type', 'description'
	];

	function scopeHasUser($q, $user_id) {
		return $q->where('user_id', $user_id);
	}

	function getDescriptionAttribute($value) {
		return json_decode($value, true);
	}

	function setDescriptionAttribute($value) {
		$this->attributes['description'] = json_encode($value);
	}
}