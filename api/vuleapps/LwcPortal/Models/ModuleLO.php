<?php namespace VuleApps\LwcPortal\Models;

use Illuminate\Database\Eloquent\Model;

class ModuleLO extends Model {
	protected $table = 'module_los';

	public $timestamps = false;
}