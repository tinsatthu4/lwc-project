<?php
namespace VuleApps\LwcPortal\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class UserMeta extends Model
{
	public    $timestamps = false;
	protected $table      = 'usermeta';
	protected $fillable   = ['user_id', 'meta_key', 'meta_value', 'serialize'];

	function user() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
