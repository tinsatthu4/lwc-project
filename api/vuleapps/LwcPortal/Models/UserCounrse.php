<?php namespace VuleApps\LwcPortal\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class UserCounrse extends Model {
    public $timestamps = false;
    protected $fillable = [
        'post_id', 'user_id'
    ];

	function scopeHasUser($q, $user_id) {
		$q->where('user_id', $user_id);
	}

    function user() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    function course() {
        return $this->belongsTo(Counrse::class, 'post_id', 'id');
    }
}