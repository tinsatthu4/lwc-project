<?php namespace VuleApps\LwcPortal\Models;

use Illuminate\Database\Eloquent\Model;

class LO extends Model {
	protected $table = 'los';

	protected $fillable = [
		"title", "scenarios", "module_id"
	];

	public $timestamps = false;


	function scopeHasModule($q, $module_id) {
		return $q->where('module_id', $module_id);
	}

	function tasks() {
		return $this->hasMany(Task::class, 'lo_id');
	}
}