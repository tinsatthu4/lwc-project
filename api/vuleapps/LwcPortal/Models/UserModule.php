<?php namespace VuleApps\LwcPortal\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use VuleApps\LwcBackends\Models\Post;

class UserModule extends Model {
	public    $timestamps = false;
	protected $fillable   = ['user_id', 'post_id', 'final_file'];

	function scopeHasUser($q, $user_id) {
		$q->where('user_id', $user_id);
	}

    function user() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    function module() {
        return $this->belongsTo(Module::class, 'post_id', 'id');
    }
    function course() {
        return $this->belongsTo(Post::class, 'post_id', 'parent_id');
    }
}