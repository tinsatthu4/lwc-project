<?php
namespace VuleApps\LwcPortal\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class TmpUserPaymentsMethod extends Model
{
	protected $table = 'tmp_user_payments_method';
	
    protected $fillable = [
        'course_id', 
        'user_id', 
        'payment_method'
    ];

    public function user() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
