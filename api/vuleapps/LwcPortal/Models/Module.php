<?php namespace VuleApps\LwcPortal\Models;

use App\User;
use VuleApps\LwcBackends\Models\DefaultPost;
use Illuminate\Database\Eloquent\Builder;

class Module extends DefaultPost {
	protected $table = 'posts';
	const TYPE = 'module';

	protected $fillable = [
		"title", "teacher_id", "slug", "parent_id", "type", "image", "content", "excerpt", "sort", "status", "price",
		"meta_title", "meta_content", "meta_keywords", "time_start", "time_end"
	];

	protected static function boot()
	{
		parent::boot();

		static::addGlobalScope('post_type', function(Builder $builder) {
			$builder->where('type', Module::TYPE);
		});
	}

	function scopeValidTimeStart($q, $dateYMD) {
		return $q->whereRaw("DATE(`time_start`) <= '{$dateYMD}'");
	}

	function scopeHasTeacher($query, $teacher_id) {
		return $query->where('teacher_id', $teacher_id);
	}

	function scopeHasCourse($query, $course_id) {
		return $query->where('parent_id', $course_id);
	}

	public function setSlugAttribute($value) {
		if(empty($value)) {
			$this->attributes['slug'] = str_slug($this->attributes['title']);
		}

		$this->attributes['slug'] = str_slug($value);
	}

	function tasks() {
		return $this->hasMany(Task::class, 'post_id');
	}

    function users() {
        return $this->belongsToMany(User::class, 'user_modules', 'post_id');
    }

	function userModules() {
		return $this->hasMany(UserModule::class, 'post_id');
	}

	function userModule() {
		return $this->hasOne(UserModule::class, 'post_id');
	}

    function course() {
        return $this->belongsTo(Counrse::class, 'parent_id');
    }

	function los() {
		return $this->hasMany(LO::class, 'module_id');
	}
}