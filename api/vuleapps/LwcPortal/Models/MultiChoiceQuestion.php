<?php namespace VuleApps\LwcPortal\Models;

use Illuminate\Database\Eloquent\Model;

class MultiChoiceQuestion extends Model {
	public $timestamps = false;

    protected $fillable = [
        'title', 'task_id', 'options', 'metadata', 'answer'
    ];

    public function getOptionsAttribute($value)
    {
        return json_decode($value, true);
    }

    public function setOptionsAttribute($value)
    {
        $this->attributes['options'] = json_encode($value);
    }
}