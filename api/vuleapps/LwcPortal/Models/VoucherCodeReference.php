<?php
namespace VuleApps\LwcPortal\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class VoucherCodeReference extends Model
{
	protected $table      = 'voucher_code_reference';
	protected $fillable   = ['user_id_give_code', 'user_id_use_code', 'status', 'note'];

	function user() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
