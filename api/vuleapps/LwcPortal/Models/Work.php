<?php namespace VuleApps\LwcPortal\Models;
use Illuminate\Database\Eloquent\Model;

class Work extends Model {
	protected $fillable = ['user_id', 'todo', 'start_date', 'end_date'];

	public $timestamps = false;

	function scopeUser($q, $id) {
		return $q->where('user_id', $id);
	}
}