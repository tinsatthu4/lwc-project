<?php namespace VuleApps\LwcPortal\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class UserTaskComment extends Model {

    protected $table = 'user_task_comments';
    protected $fillable = [
        'user_task_id', 'parent_id', 'message', 'user_id'
    ];

	function scopeHasUser($q, $user_id) {
		return $q->where('user_id', $user_id);
	}

    function child() {
        return $this->belongsTo(UserTaskComment::class, 'parent_id', 'id');
    }

    function parent() {
        return $this->hasMany(UserTaskComment::class, 'parent_id', 'id');
    }

	function user() {
		return $this->belongsTo(User::class);
	}
}