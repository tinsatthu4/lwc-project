<?php namespace VuleApps\LwcPortal\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use VuleApps\LwcPortal\Models\Task;

class UserTask extends Model {
	CONST STATUS_DRAFT = 'draft';
	CONST STATUS_SUBMISSION = 'submission';
	//pass | fail | merit | diction
	CONST GRADE_PASS = 'pass';
//	CONST GRADE_FAIL = 'fail';
    CONST GRADE_REDO = 'redo';
	CONST GRADE_MERIT = 'merit';
	CONST GRADE_DICTION = 'diction';
    protected $table = 'user_tasks';
    protected $fillable = [
        'task_id', 'user_id', 'teacher_id', 'answer', 'status', 'grade', 'teacher_comment',
		'reference_box', 'files'
    ];

	function scopeOnlyRedo($q) {
		return $q->where('grade', self::GRADE_REDO);
	}

	function scopeHasStatus($query, $type) {
		return $query->where('status', $type);
	}

	function scopeHasTeacher($query, $teacher_id) {
		return $query->where('teacher_id', $teacher_id);
	}

	function scopeHasUser($query, $user_id) {
		return $query->where('user_id', $user_id);
	}

	function setFilesAttribute($value) {
		$this->attributes['files'] = json_encode($value);
	}

	function getFilesAttribute() {
		if(isset($this->attributes['files']) && !empty($this->attributes['files']))
			return json_decode($this->attributes['files']);
		return [];
	}

	function task() {
		return $this->belongsTo(Task::class);
	}

	function user() {
		return $this->belongsTo(User::class);
	}

	function user_task() {
		return $this->belongsTo(UserTask::class, 'user_id', 'user_id');
	}

    function teacher() {
        return $this->belongsTo(User::class, 'teacher_id', 'id');
    }
}