<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use VuleApps\LwcBackends\Models\Comment;
use VuleApps\LwcBackends\Models\DefaultPost;
use VuleApps\LwcBackends\Models\UserMeta;
use VuleApps\LwcBackends\Models\UserUpload;
use VuleApps\Common\PresentModelTrait;
use VuleApps\LwcPortal\Models\Counrse;
use VuleApps\LwcPortal\Models\Module;
use VuleApps\LwcPortal\Models\VoucherCodeReference;

class User extends Authenticatable
{
    CONST TYPE_MASTER  = 'teacher_master_file';
    CONST TYPE_LEARNER = 'learner';
    CONST TYPE_ADMIN   = 'admin';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public $presenter = '\VuleApps\LwcBackends\Presenter\UserPresent';
    use PresentModelTrait;

	public function scopeIsTeacher($query) {
		return $query->where('type', 'teacher_1');
	}

	public function metavalue($key) {
		if(!$this->usermeta->isEmpty())
		{
			$this->metadata = $this->usermeta->pluck('meta_value', 'meta_key')->all();
			$this->metaSerialize = $this->usermeta->pluck('serialize', 'meta_key')->all();
		}

		if(isset($this->metadata[$key]))
		{
			if((int) $this->metaSerialize[$key])
				return unserialize($this->metadata[$key]);
			return $this->metadata[$key];
		}

		return null;
	}

	public function usermeta() {
		return $this->hasMany(UserMeta::class, 'user_id');
	}

    public function uservoucher() {
        return $this->hasMany(VoucherCodeReference::class, 'user_id');
    }

	function checkouts() {
		return $this->hasMany(Checkout::class);
	}

    public function comments(){
        return $this->hasMany(Comment::class);
    }

    public function posts(){
        return $this->hasMany(DefaultPost::class, 'author', 'id');
    }

	public function userUploads() {
		return $this->hasMany(UserUpload::class);
	}

    public function modules() {
        return $this->belongsToMany(Module::class, 'user_modules', 'user_id', 'post_id');
    }

    public function counrses() {
        return $this->belongsToMany(Counrse::class, 'user_counrses', 'user_id', 'post_id');
    }

}
