<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;
use VuleApps\LwcPortal\Jobs\SendEmailJob;

class TestSendMail extends Command
{
	use DispatchesJobs;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:sendmail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test Send Mail';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//		$subject = $this->argument('subject');
//		$form = $this->argument('from');
//		$to = $this->argument('to');
//		$template = $this->argument('template');
//		$json_data = $this->argument('json_data');
//		$data = json_decode($json_data, true);
//		$course = new \stdClass();
//		$course->title = "Course Title";
//		$course->price = 10.02;

        //Welcome
//        $user = new \stdClass();
//		$user->name = "Test User";
//		$data = [
//			'user' => $user
//		];

        //Redo
//        $learner = new \stdClass();
//        $learner->name = "Test Learner";
//
//        $teacher = new \stdClass();
//        $teacher->name = "Test Teacher";
//
//        $task = new \stdClass();
//        $task->title = 'Test Course Title';
//        $task->id = 1;
//        $task->post_id = 1;

//        $data = [
//            'learner' => $learner,
//            'teacher' => $teacher,
//            'task'  => $task
//        ];

//      Payment successful
        $user = new \stdClass();
        $user->name = "Test User";

        $course = new \stdClass();
        $course->price = "Test Course Price";
        $course->title = "Test Course Title";

        $data = [
            'user' => $user,
            'course' => $course
        ];

		$this->dispatchNow(new SendEmailJob('Welcome', null, 'tinsatthu4@gmail.com', 'email.payment_success', $data));
    }
}
