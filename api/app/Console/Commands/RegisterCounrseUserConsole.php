<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use VuleApps\LwcPortal\Jobs\SendEmailJob;
use VuleApps\LwcPortal\Models\Module;
use VuleApps\LwcPortal\Models\Counrse;
use Illuminate\Validation\ValidationException;
use VuleApps\LwcPortal\Models\Checkout;
use Paypal;
use VuleApps\LwcPortal\Models\UserCounrse;
use VuleApps\LwcPortal\Models\UserModule;
use App\User;

class RegisterCounrseUserConsole extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'register-course-user {user_id} {course_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $user_id = $this->argument('user_id');
        $course_id = $this->argument('course_id');
        if(! UserCounrse::where([
            'user_id' => $user_id,
            'post_id' => $course_id
        ])->count())
            UserCounrse::create([
                'user_id' => $user_id,
                'post_id' => $course_id
            ]);

        $user = User::find($user_id);

        $modules = Module::select('id')->hasCourse($course_id);
        $moduleIds = $modules->pluck('id')->all();
        foreach($moduleIds as $id) {

            if(! UserModule::where([
                'user_id' => $user_id,
                'post_id' => $id
            ])->count())
                UserModule::create([
                    'user_id' => $user_id,
                    'post_id' => $id
                ]);
        }
    }
}
