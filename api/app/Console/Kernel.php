<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [

        // Commands\Inspire::class,
//         Commands\generateDataExample::class,
//         Commands\gcloud::class,
//         Commands\gcloudTestRequest::class,
         Commands\RegisterCounrseUserConsole::class,
         \VuleApps\LwcBackends\Consoles\RunningCrawlerModuleConsole::class,
         \VuleApps\LwcBackends\Consoles\RunningCrawlerDetailModuleConsole::class,
         \VuleApps\LwcBackends\Consoles\RunningCrawlerLessonConsole::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
    }
}
