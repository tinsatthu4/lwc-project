<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class TeacherMasterFileMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect(route('portal.login'));
            }
        }
        $user = Auth::user();
        if(!in_array($user->type, [User::TYPE_MASTER]))
        {
            Auth::logout();
            if($request->ajax() || $request->wantsJson())
                return response('Unauthorized.', 401);
            else
                return redirect(route('portal.login'));
        }
        return $next($request);
    }
}
