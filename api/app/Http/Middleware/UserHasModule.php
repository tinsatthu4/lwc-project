<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use VuleApps\LwcPortal\Models\UserModule;

class UserHasModule
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		$user_id = Auth::user()->id;
		$module_id = $request->route('module_id');
		try {
			UserModule::where([
				'user_id' => $user_id,
				'post_id' => $module_id
			])->select('id')->firstOrFail();
			return $next($request);
		} catch (ModelNotFoundException $e) {
			return response(null, 404);
		}
    }
}
