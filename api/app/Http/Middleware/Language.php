<?php

namespace App\Http\Middleware;

use Closure;
use App;
use Session;

class Language
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->session()->has('lwclocale') &&
            array_key_exists($request->session()->get('lwclocale'), config('app.locales'))
        ){
            App::setLocale($request->session()->get('lwclocale'));
        }
        else {
            $request->session()->put('lwclocale', config('app.fallback_locale'));
            App::setLocale(config('app.fallback_locale'));
        }
        return $next($request);
    }
}
