<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class LoginForTestLeanerAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
		if(Auth::guard(null)->guest())
		{
			$credentials = [
				'email' => 'learner1@example.com',
				'password' => 'learner1'
			];
			Auth::attempt($credentials, 1);
		}
		$user = Auth::getUser();
        return $next($request);
    }
}
