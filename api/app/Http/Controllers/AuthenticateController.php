<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Auth;
use Validator;
use App\User;
use VuleApps\LwcPortal\Jobs\SendEmailJob;
use VuleApps\LwcPortal\Repository\PortalRepository;
use VuleApps\LwcPortal\Models\UserMeta;

class AuthenticateController extends Controller {

	public function authenticate(Request $request)
	{
		// grab credentials from the request
		$credentials = $request->only('email', 'password');

		try {

			// attempt to verify the credentials and create a token for the user
			if (! $token = JWTAuth::attempt($credentials)) {
				return response()->json(['error' => 'invalid_credentials'], 400);
			}
		} catch (JWTException $e) {
			// something went wrong whilst attempting to encode the token
			return response()->json(['error' => 'could_not_create_token'], 400);
		}

		// all good so return the token
		return response()->json(compact('token'));
	}

	public function postRegister(Request $request) {
		try {
			$this->validate($request, [
        		// 'g-recaptcha-response' => 'required|recaptcha',
				'name'     => 'required|max:255',
				'email'    => 'required|email|max:255|unique:users',
				'password' => 'required|min:6|confirmed',
			]);

			$input             = $request->only(['email', 'password', 'name']);
			$input['password'] = bcrypt($input['password']);
			$user              = User::create($input);

			app(PortalRepository::class)->generateReferenceCode($user->id);

			if($request->has('reference_code')) {
				app(PortalRepository::class)->checkUserReferenceCode($user->id, $request->reference_code);
			}

            $this->dispatch(new SendEmailJob(
                'Welcome email',
                '',
                $user->email,
                'email.welcome',
                [
                    'user'  => $user
                ]
            ));

			return $this->authenticate($request);

		} catch (ValidationException $e) {
			return response()->json(['messages' => $e->validator->messages()], 400);
		}
	}

	public function getLogout() {
		try {
			JWTAuth::invalidate(JWTAuth::getToken());
			return response(null, 204);
		} catch(JWTException $e) {
			return response(null, 204);
		}
	}

	public function getUserInfo() {
		return response()->json(['data' => Auth::user()]);
	}
}