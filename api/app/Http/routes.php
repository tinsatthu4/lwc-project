<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

//Route::auth();
/**
 * @api {POST} user/login Login
 * @apiGroup User
 * @apiDescription Uses App\Http\Controllers\AuthenticateController@authenticate
 * @apiSuccessExample {json} Response
 * {
"token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsImlzcyI6Imh0dHA6XC9cL2NybXB2LmxvY2FsXC91c2VyXC9sb2dpbiIsImlhdCI6MTQ3NzE5NjU1OCwiZXhwIjoxNDc3MjAwMTU4LCJuYmYiOjE0NzcxOTY1NTgsImp0aSI6IjFjZGFlMzVhODk2NGRlNGZkM2FhNWYyY2RiNmUyM2EzIn0.IfRi51H15c5cd24IyfjchFATWKDPYlO_hg9Y5NAE1zw"
}
 */
Route::post('api/user/login', 'AuthenticateController@authenticate');

/**
 * @api {GET} user/logout Logout
 * @apiGroup User
 * @apiDescription Uses App\Http\Controllers\AuthenticateController@getLogout
 * @apiSuccess (204) {NULL} Null
 */
Route::get('api/user/logout', [
	'uses' => 'AuthenticateController@getLogout'
]);

/**
 * @api {GET} user/info Logout
 * @apiGroup User
 * @apiDescription Uses App\Http\Controllers\AuthenticateController@getUserInfo
 * @apiSuccess (204) {NULL} Null
 */
Route::get('api/user/info', [
	'middleware' => 'jwt.auth',
	'uses' => 'AuthenticateController@getUserInfo'
]);

//Route::get('/', function() {
//	return view('app');
//});
