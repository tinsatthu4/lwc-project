<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
		\Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
		\Illuminate\Session\Middleware\StartSession::class,
		\Illuminate\View\Middleware\ShareErrorsFromSession::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\Language::class,
            \App\Http\Middleware\EncryptCookies::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
        ],
		'webadmin' => [
			\App\Http\Middleware\EncryptCookies::class,
			\App\Http\Middleware\AdminAuthenticate::class,
		],
		'portal' => [
//			\Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
//			\Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
//			\Illuminate\Session\Middleware\StartSession::class,
//			\Illuminate\View\Middleware\ShareErrorsFromSession::class,
//			\App\Http\Middleware\EncryptCookies::class,
            'Tymon\JWTAuth\Middleware\GetUserFromToken',
			\App\Http\Middleware\AdminAuthenticate::class,
		],
		'lernerApi' => [
			'Tymon\JWTAuth\Middleware\GetUserFromToken',
			\App\Http\Middleware\LearnerMiddleware::class
		],
		'teacherApi' => [
			'Tymon\JWTAuth\Middleware\GetUserFromToken',
			\App\Http\Middleware\Teacher01Middleware::class
		],
		'learner' => [
			\Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
			\Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
			\Illuminate\Session\Middleware\StartSession::class,
			\Illuminate\View\Middleware\ShareErrorsFromSession::class,
			\App\Http\Middleware\EncryptCookies::class,
			\App\Http\Middleware\LearnerMiddleware::class,
		],
		'learnerTest' => [
			\Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
			\Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
			\Illuminate\Session\Middleware\StartSession::class,
			\Illuminate\View\Middleware\ShareErrorsFromSession::class,
			\App\Http\Middleware\EncryptCookies::class,
			\App\Http\Middleware\LoginForTestLeanerAuthenticate::class,
		],
		'teacher_1' => [
			\Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
			\Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
			\Illuminate\Session\Middleware\StartSession::class,
			\Illuminate\View\Middleware\ShareErrorsFromSession::class,
			\App\Http\Middleware\EncryptCookies::class,
			\App\Http\Middleware\Teacher01Middleware::class,
		],
		'teacher_master_file' => [
			\Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
			\Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
			\Illuminate\Session\Middleware\StartSession::class,
			\Illuminate\View\Middleware\ShareErrorsFromSession::class,
			\App\Http\Middleware\EncryptCookies::class,
			\App\Http\Middleware\TeacherMasterFileMiddleware::class,
		],
		'teacher_@' => [
			\App\Http\Middleware\EncryptCookies::class,
			\App\Http\Middleware\Teacher02Middleware::class,
		],
        'api' => [
            'throttle:60,1',
        ],
    ];

    /**
     * The application's route middleware.
     *1
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \App\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'can' => \Illuminate\Foundation\Http\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
		'lwc.user_has_module' => 'App\Http\Middleware\UserHasModule',
		'jwt.auth' => 'Tymon\JWTAuth\Middleware\GetUserFromToken',
		'jwt.refresh' => 'Tymon\JWTAuth\Middleware\RefreshToken',
    ];
}
