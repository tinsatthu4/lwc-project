<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGuestPayment20170510Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('guest_payments', function(Blueprint $table) {
            $table->string('address')->nullable();
            $table->string('surname_name')->nullable();
            $table->string('birthday')->nullable();
            $table->string('country')->nullable();
            $table->string('nationality_of')->nullable();
            $table->string('gender')->nullable();
            $table->string('highest_education_level')->nullable();
            $table->string('path_studen_photo')->nullable();
            $table->string('path_transcript')->nullable();
            $table->string('process')->nullable()->default('pending');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("posts");
    }
}
