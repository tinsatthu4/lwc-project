<?php

use Illuminate\Database\Seeder;
use App\User;

class CreateUserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('users')->truncate();
		for($i = 1; $i<= 5; $i++)
			$user = User::create([
				'name' => "learner {$i}",
				'email' => "learner{$i}@example.com",
				'type' => User::TYPE_LEARNER,
				'password' => bcrypt("learner{$i}"),
			]);

		$user = User::create([
			'name' => 'Teacher ',
			'email' => 'teacher@example.com',
			'type' => 'teacher_1',
			'password' => bcrypt('admin'),
		]);

		$user = User::create([
			'name' => 'Teacher 2',
			'email' => 'teacher2@example.com',
			'type' => 'teacher_1',
			'password' => bcrypt('teacher2'),
		]);

		$user = User::create([
			'name' => 'admin',
			'email' => 'admin@example.com',
			'type' => User::TYPE_ADMIN,
			'password' => bcrypt('admin'),
		]);

		$user = User::create([
			'name' => 'Teacher Master',
			'email' => 'masterfile@example.com',
			'type' => User::TYPE_MASTER,
			'password' => bcrypt('admin'),
		]);
    }
}
