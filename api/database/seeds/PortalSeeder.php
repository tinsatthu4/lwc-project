<?php
use Illuminate\Database\Seeder;
use App\User;

class PortalSeeder extends Seeder {
	public function run() {
		$this->call('CreateUserSeed');
		DB::table('posts')->truncate();
		DB::table('terms')->truncate();
		DB::table('term_relations')->truncate();
		DB::table('user_tasks')->truncate();
		DB::table('user_counrses')->truncate();
		DB::table('course_teachers')->truncate();
		DB::table('tasks')->truncate();
		DB::table('multi_choice_questions')->truncate();

		//insert counrse
		$this->call('CoursesSeeder');

		//insert modules & tasks
//		$counrses = \VuleApps\LwcPortal\Models\Counrse::get();
//		foreach($counrses as $counrse) {
//			for($i = 1; $i < rand(2, 10); $i++) {
//				$module = \VuleApps\LwcPortal\Models\Module::create([
//					'title'         => "Lession {$i}",
//					'slug'          => "Lession {$i}",
//					'image'         => 'uploads/san-pham/2016/09/14/bg.jpg',
//					'type'          => 'module',
//					'status'        => 'publish',
//					'price' => rand(10, 50),
//					'content' => file_get_contents(storage_path('module-content.html')),
//					'parent_id'     => $counrse->id,
//					'teacher_id' => $counrse->teacher_id
//				]);
//
//				for($taski = 1; $taski <= 5; $taski++)
//				{
//					$task = \VuleApps\LwcPortal\Models\Task::create([
//						'post_id' => $module->id,
//						'type' => \VuleApps\LwcPortal\Models\Task::TYPE_ASSIGNMENT,
//						'title' => "Task {$module->id}.{$taski}",
//						'question' => "Question of Task {$taski} of {$module->title}",
//						'keywords' => "key a,key b,key c,key e",
//						'preference' => [
//							['name' => 'refer Lession', 'type' => 'refer', 'module_id' => $module->id, 'course_id' => $counrse->id],
//							['name' => 'Custom Url', 'type' => 'link', 'href' => 'http://custom-url.com'],
//							['name' => 'Download File', 'type' => 'upload', 'path' => 'uploads/document.txt'],
//						],
//						'teacher_id' => $module->teacher_id
//					]);
//					//Data For master Files
//					for($learner_id = 3; $learner_id <= 5; $learner_id++)
//					\VuleApps\LwcPortal\Models\UserTask::create([
//						'task_id' => $task->id,
//						'user_id' => $learner_id,
//						'teacher_id' => $task->teacher_id,
//						'status' => \VuleApps\LwcPortal\Models\UserTask::STATUS_SUBMISSION,
//						'answer' => 'example answer',
//						'teacher_comment' => "Teacher comment {$counrse->id}",
//						'grade' => \VuleApps\LwcPortal\Models\UserTask::GRADE_PASS
//					]);
//				}
//				for($taski = 6; $taski <= 9; $taski++)
//				{
//					$task = \VuleApps\LwcPortal\Models\Task::create([
//						'post_id' => $module->id,
//						'type' => \VuleApps\LwcPortal\Models\Task::TYPE_MCQ,
//						'title' => "Task {$module->id}.{$taski}",
//						'preference' => [
//							['name' => 'refer Lession', 'type' => 'refer', 'module_id' => $module->id, 'course_id' => $counrse->id],
//							['name' => 'Custom Url', 'type' => 'link', 'href' => 'http://custom-url.com'],
//							['name' => 'Download File', 'type' => 'upload', 'path' => 'uploads/document.txt'],
//						],
//						'teacher_id' => $module->teacher_id
//					]);
//					$options = json_encode([
//						[ 'key' => 'answer-a', 'title' => 'Choose A' ],
//						[ 'key' => 'answer-b', 'title' => 'Choose B' ],
//						[ 'key' => 'answer-c', 'title' => 'Choose C' ],
//					]);
//					DB::table('multi_choice_questions')->insert([
//						['title' => 'Question 1', 'options' => $options, 'answer' => 'answer-a', 'task_id' => $task->id],
//						['title' => 'Question 2', 'options' => $options, 'answer' => 'answer-b', 'task_id' => $task->id],
//						['title' => 'Question 3', 'options' => $options, 'answer' => 'answer-c', 'task_id' => $task->id],
//						['title' => 'Question 4', 'options' => $options, 'answer' => 'answer-c', 'task_id' => $task->id],
//						['title' => 'Question 5', 'options' => $options, 'answer' => 'answer-b', 'task_id' => $task->id],
//						['title' => 'Question 6', 'options' => $options, 'answer' => 'answer-a', 'task_id' => $task->id],
//						['title' => 'Question 7', 'options' => $options, 'answer' => 'answer-b', 'task_id' => $task->id],
//					]);
//				}
//			}
//		}

//		$this->call('UserModuleSeeder');

		$terms = [
			'event',
			'announcements',
			'news'
		];
		foreach ($terms as $term) {
			$termData = \VuleApps\LwcBackends\Models\Term::create([
				'title'         => $term,
				'slug'          => $term,
				'type'          => 'post',
				'meta_title'    => $term,
				'meta_content'  => $term,
				'meta_keywords' => $term,
				'parent_id'     => 0
			]);
			for($i = 1; $i < 6; $i ++) {
				$post = \VuleApps\LwcBackends\Models\Post::create([
					'title' => "{$term} {$i}",
					'image' => 'uploads/san-pham/2016/09/14/bg.jpg',
					'content' => file_get_contents(storage_path('module-content.html'))
				]);
				\VuleApps\LwcBackends\Models\TermRelation::create([
					'post_id'   => $post->id,
					'term_id'   => $termData->id,
				]);
			}
		}


		$this->call('WorkSeeder');
	}
}