<?php

use Illuminate\Database\Seeder;

class CoursesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
		DB::table('posts')->truncate();
		DB::table('course_teachers')->truncate();

		DB::table('posts')->insert([
			[
				'title' => 'Level 3 diploma in business management',
				'content' => file_get_contents(database_path('data/level-3-business.html')),
				'price' => 3000,
				'type' => \VuleApps\LwcPortal\Models\Counrse::TYPE,
				'teacher_id' => 6
			],
			[
				'title' => 'Level 4/5 Extended Diploma in management',
				'content' => '',
				'price' => 1095,
				'type' => \VuleApps\LwcPortal\Models\Counrse::TYPE,
				'teacher_id' => 6
			],
			[
				'title' => 'Level 6 Diploma business and administrative',
				'content' => '',
				'price' => 3000,
				'type' => \VuleApps\LwcPortal\Models\Counrse::TYPE,
				'teacher_id' => 6
			],
			[
				'title' => 'Level 7 Diploma in Strategic Management',
				'content' => '',
				'price' => 600,
				'type' => \VuleApps\LwcPortal\Models\Counrse::TYPE,
				'teacher_id' => 6
			],
			[
				'title' => 'Level 4/5 Diploma in IT & Computing',
				'content' => '',
				'price' => 895,
				'type' => \VuleApps\LwcPortal\Models\Counrse::TYPE,
				'teacher_id' => 6
			],
			[
				'title' => 'Level 4/5 Hotel & Hospitality',
				'content' => '',
				'price' => 995,
				'type' => \VuleApps\LwcPortal\Models\Counrse::TYPE,
				'teacher_id' => 6
			]
		]);

		//Insert Module
		DB::table('posts')->insert([
			[
				'title' => 'Managing Business Operations',
				'parent_id' => 1,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'Maximizing Resources',
				'parent_id' => 1,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'The Business Environment',
				'parent_id' => 1,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'Managing People in Organisations',
				'parent_id' => 1,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'Working in Teams',
				'parent_id' => 1,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'Effective Business Communication',
				'parent_id' => 1,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L4 Developing Personal Skills',
				'parent_id' => 2,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L4 Effective Communication',
				'parent_id' => 2,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L4 The Business Environment',
				'parent_id' => 2,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L4 The Marketing Mix',
				'parent_id' => 2,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L4 Strategic HRM',
				'parent_id' => 2,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L4 Managing Ethically',
				'parent_id' => 2,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L4 Culture and the Organisation',
				'parent_id' => 2,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L4 Customers and Customer Service',
				'parent_id' => 2,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L4 Fundamentals of Accounting',
				'parent_id' => 2,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L4 Financial Management and Control',
				'parent_id' => 2,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L5 The Entrepreneurial Manager',
				'parent_id' => 2,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L5 Organisation Structures',
				'parent_id' => 2,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L5 Practical Accounting Analysis',
				'parent_id' => 2,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L5 Business Planning and Goal Setting',
				'parent_id' => 2,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L5 Politics and Business',
				'parent_id' => 2,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L5 Business Law',
				'parent_id' => 2,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L5 Managing in Today’s World',
				'parent_id' => 2,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L5 Performance Management',
				'parent_id' => 2,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L5 Marketing and Sales Planning',
				'parent_id' => 2,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L5 Quantitative Skills',
				'parent_id' => 2,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'IT in Business',
				'parent_id' => 3,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'Effective Communications',
				'parent_id' => 3,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'High Performance Teams',
				'parent_id' => 3,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'Leadership Skills',
				'parent_id' => 3,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'Manager’s Toolkit',
				'parent_id' => 3,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'Managing and Using Finance',
				'parent_id' => 3,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'Managing and Using Marketing',
				'parent_id' => 3,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'Managing Organisations',
				'parent_id' => 3,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'Personal Effectiveness',
				'parent_id' => 3,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'Quality and Excellence',
				'parent_id' => 3,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'Qualities of Effective Leadership',
				'parent_id' => 4,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'Developing Interpersonal Skills',
				'parent_id' => 4,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'Motivating and Influencing People',
				'parent_id' => 4,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'Making Decisions',
				'parent_id' => 4,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'Creativity and Problem Solving',
				'parent_id' => 4,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L4 Introduction to Computing',
				'parent_id' => 5,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L4 Programming',
				'parent_id' => 5,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L4 Databases',
				'parent_id' => 5,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L4 E- Commerce Applications',
				'parent_id' => 5,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L4 Human Computer Interaction',
				'parent_id' => 5,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L4 IT and Society',
				'parent_id' => 5,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L4 Information Systems Project Management',
				'parent_id' => 5,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L4 Management Information Systems',
				'parent_id' => 5,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L5 Innovative Technologies in Computing',
				'parent_id' => 5,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L5 Ethical, Legal and Professional Issues in IT',
				'parent_id' => 5,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L5 Network Design and Management',
				'parent_id' => 5,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L5 Web Based Development',
				'parent_id' => 5,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L5 Cyber Security',
				'parent_id' => 5,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L5 Advanced Databases',
				'parent_id' => 5,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L4 Accounting and Cost Control',
				'parent_id' => 6,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L4 Hospitality and Hotel Housekeeping',
				'parent_id' => 6,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L4 Quality/health and Safety',
				'parent_id' => 6,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L4 Facilities Management',
				'parent_id' => 6,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L4 Culinary and Nutritional Aspects',
				'parent_id' => 6,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L4 Marketing in Hotel and Leisure',
				'parent_id' => 6,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L4 Primary Management Issues for Hospitality and Hotel Management',
				'parent_id' => 6,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L4 Organisational Behaviour and Social Aspects in Hospitality',
				'parent_id' => 6,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L4 Training and CPD',
				'parent_id' => 6,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L4 Teams',
				'parent_id' => 6,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L5 Food Safety and Hygiene',
				'parent_id' => 6,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L5 Food and Beverage Supply Chain Management',
				'parent_id' => 6,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L5 Events and Conference Management',
				'parent_id' => 6,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L5 Hotel Front Office Operations',
				'parent_id' => 6,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L5 Key Performance Indicators',
				'parent_id' => 6,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L5 Consumer Behaviour in the Hospitality and Hotel Industry',
				'parent_id' => 6,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L5 Working With Third Parties in the Sector',
				'parent_id' => 6,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L5 Economics of the Sector',
				'parent_id' => 6,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L5 Risk and Security in Hospitality and Hotel Management ',
				'parent_id' => 6,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
			[
				'title' => 'L5 The Use of Technology',
				'parent_id' => 6,
				'type' => \VuleApps\LwcPortal\Models\Module::TYPE
			],
		]);
		DB::table('posts')->where('type', \VuleApps\LwcPortal\Models\Module::TYPE)->update([
			'teacher_id' => 6
		]);
		DB::table('course_teachers')->insert([
			['post_id' => 1, 'user_id' => 6],
			['post_id' => 2, 'user_id' => 6],
			['post_id' => 3, 'user_id' => 6],
			['post_id' => 4, 'user_id' => 6],
			['post_id' => 5, 'user_id' => 6],
			['post_id' => 6, 'user_id' => 6],
		]);
		$module = new stdClass();
		$module->id = 7;
		$module->teacher_id = 6;
		$counrse = new stdClass();
		$counrse->id = 1;
		for($taski = 1; $taski <= 6; $taski++)
			{
				$task = \VuleApps\LwcPortal\Models\Task::create([
					'post_id' => $module->id,
					'type' => \VuleApps\LwcPortal\Models\Task::TYPE_MCQ,
					'title' => "Task {$module->id}.{$taski}",
					'preference' => [
						['name' => 'refer Lession', 'type' => 'refer', 'module_id' => $module->id, 'course_id' => $counrse->id],
						['name' => 'Custom Url', 'type' => 'link', 'href' => 'http://custom-url.com'],
						['name' => 'Download File', 'type' => 'upload', 'path' => 'uploads/document.txt'],
					],
					'teacher_id' => $module->teacher_id
				]);
				$options = json_encode([
					[ 'key' => 'answer-a', 'title' => 'Choose A' ],
					[ 'key' => 'answer-b', 'title' => 'Choose B' ],
					[ 'key' => 'answer-c', 'title' => 'Choose C' ],
				]);
				DB::table('multi_choice_questions')->insert([
					['title' => 'Question 1', 'options' => $options, 'answer' => 'answer-a', 'task_id' => $task->id],
					['title' => 'Question 2', 'options' => $options, 'answer' => 'answer-b', 'task_id' => $task->id],
					['title' => 'Question 3', 'options' => $options, 'answer' => 'answer-c', 'task_id' => $task->id],
					['title' => 'Question 4', 'options' => $options, 'answer' => 'answer-c', 'task_id' => $task->id],
					['title' => 'Question 5', 'options' => $options, 'answer' => 'answer-b', 'task_id' => $task->id],
					['title' => 'Question 6', 'options' => $options, 'answer' => 'answer-a', 'task_id' => $task->id],
					['title' => 'Question 7', 'options' => $options, 'answer' => 'answer-b', 'task_id' => $task->id],
				]);
			}
    }
}
