<?php

use Illuminate\Database\Seeder;
use App\User;

class CreateCustomUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
		User::create([
			'name' => "Vu Le",
			'email' => "tinsatthu4@gmail.com",
			'type' => User::TYPE_LEARNER,
			'password' => bcrypt("admin"),
		]);
    }
}
