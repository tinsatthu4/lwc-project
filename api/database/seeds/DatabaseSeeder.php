<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        $this->call(CreateUserSeed::class);
//        $this->call(CoursesTableSeeder::class);
//        $this->call(ModuleTableSeeder::class);
//        $this->call(TaskTableSeeder::class);
//        $this->call(LearnerTableSeeder::class);
//        $this->call(PostTableSeeder::class);
//        $this->call(TermTableSeeder::class);
          $this->call(PortalSeeder::class);
    }
}
