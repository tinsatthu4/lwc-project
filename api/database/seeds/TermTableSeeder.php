<?php

use Illuminate\Database\Seeder;

class TermTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
		DB::table('terms')->truncate();
        $terms = [
            'event',
            'announcements',
            'news'
        ];
        foreach ($terms as $term) {
            $termData = \VuleApps\LwcBackends\Models\Term::create([
                'title'         => $term,
                'slug'          => $term,
                'type'          => 'post',
                'meta_title'    => $term,
                'meta_content'  => $term,
                'meta_keywords' => $term,
                'parent_id'     => 0
            ]);
			for($i = 1; $i < 11; $i ++) {
				$post = \VuleApps\LwcBackends\Models\Post::create([
					'title' => "{$term} {$i}",
					'image' => 'uploads/san-pham/2016/09/14/bg.jpg',
				]);
				\VuleApps\LwcBackends\Models\TermRelation::create([
					'post_id'   => $post->id,
					'term_id'   => $termData->id,
				]);
			}
        }
    }
}
