<?php

use Illuminate\Database\Seeder;
use VuleApps\LwcPortal\Models\UserModule;
use VuleApps\LwcPortal\Models\Module;

class UserModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
		DB::table('user_modules')->truncate();
		DB::table('user_counrses')->truncate();
		$modules = Module::hasCourse(5)->select('id')->take(10)->get();
		$user_id = 1;
		foreach($modules as $module) {
			UserModule::create([
				'user_id' => $user_id,
				'post_id' => $module->id
			]);
		}
		DB::table('user_counrses')->insert(['user_id' => $user_id, 'post_id' => 5]);
    }
}
