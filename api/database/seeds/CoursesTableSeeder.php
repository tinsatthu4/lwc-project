<?php

use Illuminate\Database\Seeder;

class CoursesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        for($i = 0; $i < 5; $i++) {
            $id = time();
            \VuleApps\LwcPortal\Models\Module::create([
                'title'         => 'Course '. $id,
                'slug'          => 'course-'. $id,
                'excerpt'       => 'course '. $id,
                'content'       => '<p>Course '. $id .'</p>',
                'image'         => 'uploads/san-pham/2016/09/14/bg.jpg',
                'type'          => 'counrse',
                'status'        => 'publish',
                'meta_title'    => 'course '. $id,
                'meta_content'  => 'course '. $id,
                'meta_keywords' => 'course '. $id .',course'. $id,
                'parent_id'     => 0
            ]);
        }
    }
}
