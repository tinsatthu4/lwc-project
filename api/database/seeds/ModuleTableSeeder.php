<?php

use Illuminate\Database\Seeder;

class ModuleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $cournses = \VuleApps\LwcPortal\Models\Counrse::get();
        foreach($cournses as $cournse) {
            for($i = 0; $i < 10; $i++) {
                $id = time();
                \VuleApps\LwcPortal\Models\Module::create([
                    'title'         => 'Module '. $id,
                    'slug'          => 'module-'. $id,
                    'excerpt'       => 'module '. $id,
                    'content'       => '<p>Module '. $id .'</p>',
                    'image'         => 'uploads/san-pham/2016/09/14/bg.jpg',
                    'type'          => 'module',
                    'status'        => 'publish',
                    'meta_title'    => 'Module '. $id,
                    'meta_content'  => 'Module '. $id,
                    'meta_keywords' => 'module '. $id .',modules'. $id,
                    'parent_id'     => $cournse->id
                ]);
            }
        }
    }
}
