<?php

use Illuminate\Database\Seeder;

class ClearUserCourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
		DB::table('user_counrses')->truncate();
		DB::table('user_modules')->truncate();
    }
}
