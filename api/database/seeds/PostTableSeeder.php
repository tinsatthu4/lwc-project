<?php

use Illuminate\Database\Seeder;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        for($i = 0; $i < 10; $i++) {
            $id = time();
            \VuleApps\LwcBackends\Models\Post::create([
                'title'         => 'Post '. $id,
                'slug'          => 'post-'. $id,
                'excerpt'       => 'post '. $id,
                'content'       => '<p>post '. $id .'</p>',
                'image'         => 'uploads/san-pham/2016/09/14/bg.jpg',
                'type'          => 'post',
                'status'        => 'publish',
                'meta_title'    => 'post '. $id,
                'meta_content'  => 'post '. $id,
                'meta_keywords' => 'post '. $id .',post'. $id,
                'parent_id'     => 0
            ]);
        }
    }
}
