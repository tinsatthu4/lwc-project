<?php

use Illuminate\Database\Seeder;

class WorkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
		DB::table('works')->truncate();
		DB::table('works')->insert([
			[
				'user_id' => 1,
				'todo' => 'To Do Task 1',
				'start_date' => '2016-11-01',
				'end_date' => '2016-11-05',
				'color' => '#3c8dbc'
			],
			[
				'user_id' => 1,
				'todo' => 'Read document',
				'start_date' => '2016-11-01',
				'end_date' => '2016-11-05',
				'color' => '#01ff70'
			],
			[
				'user_id' => 1,
				'todo' => 'Do Homework',
				'start_date' => '2016-11-05',
				'end_date' => '2016-11-10',
				'color' => '#f39c12'
			],
		]);
    }
}
