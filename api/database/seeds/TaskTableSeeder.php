<?php

use Illuminate\Database\Seeder;

class TaskTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $modules = \VuleApps\LwcPortal\Models\Module::get();
        foreach($modules as $module) {
            for($i = 0; $i < 5; $i++) {
                $id = time();
                \VuleApps\LwcPortal\Models\Task::create([
                    'title'     => 'Task '. $id,
                    'post_id'   => $module->id,
                    'keywords'  => 'Task '. $id,
                    'type'      => ($i % 2 == 0) ? 'mqc' : 'assignment',
                ]);
            }
        }
    }
}
