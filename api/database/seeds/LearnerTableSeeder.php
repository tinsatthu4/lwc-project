<?php

use Illuminate\Database\Seeder;
use App\User;

class LearnerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
		User::where('email', 'learner@example.com')->delete();
        $user = User::create([
            'name' => 'learner@example.com',
            'email' => 'learner@example.com',
            'type' => 'learner',
            'password' => bcrypt('admin'),
        ]);
        $cournses = \VuleApps\LwcPortal\Models\Counrse::take(10)->get();
        foreach($cournses as $cournse) {
            \VuleApps\LwcPortal\Models\UserCounrse::create([
                'user_id'   => $user->id,
                'post_id'   => $cournse->id
            ]);
            $modules = \VuleApps\LwcPortal\Models\Module::where('parent_id', $cournse->id)
                ->take(10)->get();
            foreach($modules as $module) {
                \VuleApps\LwcPortal\Models\UserModule::create([
                    'user_id'   => $user->id,
                    'post_id'   => $module->id
                ]);
                $tasks = \VuleApps\LwcPortal\Models\Task::where('post_id', $module->id)
                    ->take(10)->get();
                foreach($tasks as $task) {
                    \VuleApps\LwcPortal\Models\UserTask::create([
                        'task_id'   => $task->id,
                        'user_id'   => $user->id
                    ]);
                }
            }
        }
    }
}
