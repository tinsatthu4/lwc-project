<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostMetaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		Schema::create("postmeta", function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->bigInteger('post_id');
			$table->string('meta_key');
			$table->longText('meta_value')->nullable();
			$table->index('post_id');
			$table->unique(['post_id', 'meta_key']);
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
		Schema::dropIfExists('postmeta');
    }
}
