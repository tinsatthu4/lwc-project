<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		Schema::create('settings', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->string('group', 50)->default('default');
			$table->string('key', 50);
			$table->text("value")->nullable();
			$table->string('autoload', 3)->default('yes');
			$table->string('serialize', 3)->default('no');
			$table->index('group');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
		Schema::dropIfExists('settings');
    }
}
