<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoSchemas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		Schema::create('los', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->bigInteger('module_id')->default(0);
			$table->string('title');
			$table->text('scenarios');
		});

		Schema::table('tasks', function(Blueprint $table) {
			$table->bigInteger('lo_id')->default(0);
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
		Schema::dropIfExists('los');
		Schema::table('tasks', function(Blueprint $table) {
			$table->dropColumn('lo_id');
		});
    }
}
