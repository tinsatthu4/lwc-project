<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEknowledgeModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ek_courses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('description')->nullable();
            $table->string('status')->default('pending');
            $table->timestamps();
        });

        Schema::create('ek_modules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('course_id');
            $table->string('code');
            $table->string('title');
            $table->text('description')->nullable();
            $table->text('learning_outcomes')->nullable();
            $table->text('introduction')->nullable();
            $table->string('status')->default('pending');
            $table->timestamps();
        });

        Schema::create('ek_lessons', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('module_id');
            $table->string('title');
            $table->text('description')->nullable();
            $table->string('status')->default('pending');
            $table->timestamps();
        });

        Schema::create('ek_lesson_sessions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lession_id');
            $table->string('title');
            $table->text('content')->nullable();
            $table->string('status')->default('pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("ek_modules");
        Schema::dropIfExists("ek_lessons");
        Schema::dropIfExists("ek_lesson_sessions");
    }
}
