<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		Schema::create("posts", function(Blueprint $table) {
			$table->bigIncrements("id");
			$table->string("title");
			$table->string("slug");
			$table->text("excerpt")->nullable();
			$table->longText("content")->nullable();
			$table->string("image")->nullable();
			$table->integer("sort")->default(0);
			$table->string("type")->default("post");
			$table->bigInteger("author")->default(0);
			$table->string("status", 10)->default("publish");
			$table->string("meta_title")->nullable();
			$table->text("meta_content")->nullable();
			$table->text("meta_keywords")->nullable();
			$table->timestamps();
			$table->softDeletes();
			$table->index(["type", "status"]);
		});

		DB::statement('ALTER TABLE `posts` ADD FULLTEXT search(title)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
		Schema::dropIfExists("posts");
    }
}
