<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlertCheckoutAddTokenColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		Schema::table("checkouts", function(Blueprint $table) {
			$table->string('payment_token')->nullable();
		});

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
		Schema::table("checkouts", function(Blueprint $table) {
			$table->dropColumn('payment_token');
		});
    }
}
