<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotifications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		Schema::create('notifications', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->bigInteger('user_id')->default(0);
			$table->string('type', 50)->nullable();
			$table->text('message');
			$table->timestamps();
			$table->string('status')->default('unread');
			$table->index(['type', 'status']);
			$table->index(['user_id', 'status']);
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
		Schema::dropIfExists('notifications');
    }
}
