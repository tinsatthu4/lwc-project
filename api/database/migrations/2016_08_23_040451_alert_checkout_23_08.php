<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlertCheckout2308 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		Schema::table("checkouts", function(Blueprint $table) {
			$table->string('checkout_type', 20)->default('buy_token');
			$table->integer('price_token')->default(0);
			$table->bigInteger('item_id')->default(0);
			$table->text('description')->nullable();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
		Schema::table("checkouts", function(Blueprint $table) {
			$table->dropColumn(['checkout_type', 'item_id', 'description', 'price_token']);
		});
    }
}
