<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlertTasks1482820018 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		Schema::table('tasks', function(Blueprint $table) {
			$table->string('type_of_grade', 50)->default('none');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
		Schema::table('tasks', function(Blueprint $table) {
			$table->dropColumn('type_of_grade');
		});
	}
}
