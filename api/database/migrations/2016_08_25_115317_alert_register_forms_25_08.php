<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlertRegisterForms2508 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		Schema::table('register_forms', function(Blueprint $table) {
			$table->bigInteger('user_id')->default(0);
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
		Schema::table('register_forms', function(Blueprint $table) {
			$table->dropColumn(['user_id']);
		});
    }
}
