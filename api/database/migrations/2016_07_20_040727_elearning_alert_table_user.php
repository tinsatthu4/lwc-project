<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ElearningAlertTableUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		Schema::table('users', function(Blueprint $table) {
			//type member , admin
			$table->string('type', 20)->default('member');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
		Schema::table('users', function(Blueprint $table) {
			$table->dropColumn(['type']);
		});
    }
}
