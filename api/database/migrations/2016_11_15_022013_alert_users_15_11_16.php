<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlertUsers151116 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

		Schema::table('users', function(Blueprint $table) {
			$table->dropColumn('type');
		});

		Schema::table('users', function(Blueprint $table) {
			$table->string('type', 50)->default('learner');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
