<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTmpUserPaymentsMethodTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("tmp_user_payments_method", function(Blueprint $table) {
            $table->bigIncrements("id");
            $table->integer("course_id")->nullable();
            $table->integer("user_id")->nullable();
            $table->string("payment_method")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("tmp_user_payments_method");
    }
}
