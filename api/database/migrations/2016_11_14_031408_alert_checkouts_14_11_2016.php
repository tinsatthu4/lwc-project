<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlertCheckouts14112016 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		Schema::table('checkouts', function(Blueprint $table) {
			$table->dropColumn('price');
		});

		Schema::table('checkouts', function(Blueprint $table) {
			$table->bigInteger('post_id');
			$table->string('payper_id')->nullable();
			$table->float('price')->default(0.00);
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
		Schema::table('checkouts', function(Blueprint $table) {
			$table->dropColumn('post_id');
		});
    }
}
