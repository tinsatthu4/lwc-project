<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Alert04102017 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('guest_payments', function(Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->string('link');
            $table->string('payment_method')->default("paypal");
            $table->text('base64');
            $table->text("desc");

            $table->string('name');
            $table->string('phone');
            $table->string('email');
            $table->string('course');
            $table->string('price');

            $table->unique('code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('guest_payments');
    }
}
