<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegisterForm extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		Schema::create('register_forms', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->string('name', 120);
			$table->string('email', 120)->nullable();
			$table->string('phone');
			$table->text('note')->nullable();
			$table->text('address')->nullable();
			$table->bigInteger('post_id');
			$table->index('post_id');
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
		Schema::dropIfExists('register_forms');
    }
}
