<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlertUserTask1481966845 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		Schema::table('tasks', function(Blueprint $table) {
			$table->text('scenarios');
		});

		Schema::table('user_tasks', function(Blueprint $table) {
			$table->text('reference_box');
			$table->text('files');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
		Schema::table('tasks', function(Blueprint $table) {
			$table->dropColumn('scenarios');
		});

		Schema::table('user_tasks', function(Blueprint $table) {
			$table->dropColumn('reference_box');
			$table->dropColumn('files');
		});
    }
}
