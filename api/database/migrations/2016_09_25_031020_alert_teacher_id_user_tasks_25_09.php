<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlertTeacherIdUserTasks2509 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		Schema::table('user_tasks', function(Blueprint $table) {
			$table->bigInteger('teacher_id')->default(0);
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
		Schema::table('user_tasks', function(Blueprint $table) {
			$table->dropColumn('teacher_id');
		});
    }
}
