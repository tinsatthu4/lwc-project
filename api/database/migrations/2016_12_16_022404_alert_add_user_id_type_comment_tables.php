<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlertAddUserIdTypeCommentTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		Schema::table('user_task_comments', function(Blueprint $table) {
			$table->integer('user_id')->default(0);
			$table->index('user_task_id');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
		Schema::table('user_task_comments', function(Blueprint $table) {
			$table->dropColumn('user_id');
			$table->dropIndex('user_task_id');
		});
    }
}
