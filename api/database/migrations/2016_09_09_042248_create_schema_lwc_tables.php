<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchemaLwcTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		Schema::create('tasks', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->bigInteger('post_id'); //detach belong to post_type = module
			$table->text('title');
			$table->text('preference'); // is json
			$table->text('keywords'); //total keywords
			$table->string('type')->default('assignment'); // assignment ||mcq
			$table->tinyInteger('sort')->default(0);
			$table->timestamps();
		});

		//for
		Schema::create('multi_choice_questions', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->text('title');
			$table->bigInteger('task_id');
			/**
			 * only for mcq with for
			 * is json_encode
			 * format
			 * [
			 * 	["key" => "option-a", "title" => "Option A" ],
			 * 	["key" => "option-b", "title" => "Option A" ],
			 * 	["key" => "option-c", "title" => "Option A" ],
			 * 	["key" => "option-d", "title" => "Option A" ],
			 * ]
			 */
			$table->longText('options');
			$table->text('metadata');
			$table->string('answer'); // example : option-a
		});

		Schema::create('user_tasks', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->bigInteger('task_id');
			$table->bigInteger('user_id');
			/**
			 * with task is mcq
			 * is a json_encode with format
			 * [
			 * 	['multi_choice_question_id' => 1, 'answer' => 'option-a']
			 * ]
			 */
			$table->longText('answer');
			$table->string('status')->default('draft'); // draft || submission
			$table->string('grade', 20)->default('none'); // score none | pass | fail | merit | diction
			$table->text('teacher_comment');
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
		Schema::dropIfExists('tasks');
		Schema::dropIfExists('multi_choice_questions');
		Schema::dropIfExists('user_tasks');
    }
}
