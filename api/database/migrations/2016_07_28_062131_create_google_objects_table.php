<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoogleObjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		Schema::create('google_objects', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->string('name', 150);
			$table->text('metadata')->default(null);
			$table->tinyInteger('status')->default(0);
			$table->unique('name');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
		Schema::dropIfExists('google_objects');
    }
}
