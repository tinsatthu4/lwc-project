<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTermsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		Schema::create("terms", function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->string('title');
			$table->string('slug');
			$table->string("type")->default("category");
			$table->bigInteger('parent_id')->default(0);
			$table->string("meta_title")->nullable();
			$table->text("meta_content")->nullable();
			$table->text("meta_keywords")->nullable();
			$table->tinyInteger('sort')->default(0);
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
		Schema::dropIfExists('terms');
    }
}
