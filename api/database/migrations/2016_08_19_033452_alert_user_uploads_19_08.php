<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlertUserUploads1908 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		Schema::table('user_uploads', function(Blueprint $table) {
			$table->string('status', 10)->default('new');
			$table->bigInteger('user_updated')->default(0);
			$table->integer('token')->default(0);
			$table->text('message');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
		Schema::table('user_uploads', function(Blueprint $table) {
			$table->dropColumn(['status', 'user_updated', 'token', 'message']);
		});
    }
}
