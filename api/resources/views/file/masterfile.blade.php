<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta http-equiv="Content-Style-Type" content="text/css" />
      <meta name="generator" content="Aspose.Words for .NET 17.8" />
      <title></title>
   </head>
   <body>
      <div>
         <div style="-aw-headerfooter-type:header-primary">
            <p style="margin-top:0pt; margin-bottom:0pt; font-size:11pt"><img src="Assignment submission form.001.png" width="233" height="95" alt="" style="-aw-left-pos:0pt; -aw-rel-hpos:column; -aw-rel-vpos:paragraph; -aw-top-pos:0pt; -aw-wrap-type:inline" /></p>
            <p style="margin-top:0pt; margin-bottom:0pt; font-size:11pt"><span style="height:0pt; display:block; position:absolute; z-index:-65536"><img src="Assignment submission form.002.png" width="597" height="531" alt="" style="margin-top:114.9pt; margin-left:2pt; -aw-left-pos:0pt; -aw-rel-hpos:page; -aw-rel-vpos:page; -aw-top-pos:0pt; -aw-wrap-type:none; position:absolute" /></span><span style="font-family:Arial">&#xa0;</span></p>
         </div>
         <p style="margin-top:0pt; margin-left:324pt; margin-bottom:0pt; text-align:right; page-break-after:avoid; font-size:12pt"><span style="font-family:Arial; font-weight:bold; background-color:#cccccc">Form SAF</span></p>
         <p style="margin-top:0pt; margin-bottom:0pt; text-align:center; font-size:14pt"><span style="font-family:Arial; font-weight:bold">Student Submission and Feedback Form</span></p>
         <p style="margin-top:0pt; margin-bottom:0pt; font-size:12pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
         <p style="margin-top:0pt; margin-bottom:0pt; font-size:12pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
         <p style="margin-top:0pt; margin-bottom:0pt; font-size:12pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
         <ol type="1" style="margin:0pt; padding-left:0pt">
            <li style="margin-left:33.01pt; padding-left:2.99pt; font-family:Arial; font-size:12pt; font-weight:bold"><span>PROGRAMME DETAILS</span></li>
         </ol>
         <p style="margin-top:0pt; margin-left:36pt; margin-bottom:0pt; font-size:12pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
         <p style="margin-top:0pt; margin-bottom:0pt; font-size:12pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
         <table cellspacing="0" cellpadding="0" style="width:450.75pt; border:0.75pt solid #000000; border-collapse:collapse">
            <tr>
               <td style="border-right-style:solid; border-right-width:0.75pt; border-bottom-style:solid; border-bottom-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top; background-color:#cccccc">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:8pt"><span style="font-family:Arial">Name:</span></p>
               </td>
               <td style="border-right-style:solid; border-right-width:0.75pt; border-left-style:solid; border-left-width:0.75pt; border-bottom-style:solid; border-bottom-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:8pt"><span style="font-family:Arial">&#xa0;</span></p>
               </td>
               <td style="border-right-style:solid; border-right-width:0.75pt; border-left-style:solid; border-left-width:0.75pt; border-bottom-style:solid; border-bottom-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top; background-color:#cccccc">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:8pt"><span style="font-family:Arial">ID No.</span></p>
               </td>
               <td style="border-right-style:solid; border-right-width:0.75pt; border-left-style:solid; border-left-width:0.75pt; border-bottom-style:solid; border-bottom-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:8pt"><span style="font-family:Arial">&#xa0;</span></p>
               </td>
               <td style="border-right-style:solid; border-right-width:0.75pt; border-left-style:solid; border-left-width:0.75pt; border-bottom-style:solid; border-bottom-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top; background-color:#cccccc">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:8pt"><span style="font-family:Arial">Enrolment date:</span></p>
               </td>
               <td style="border-left-style:solid; border-left-width:0.75pt; border-bottom-style:solid; border-bottom-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:8pt"><span style="font-family:Arial">&#xa0;</span></p>
               </td>
            </tr>
            <tr>
               <td style="border-top-style:solid; border-top-width:0.75pt; border-right-style:solid; border-right-width:0.75pt; border-bottom-style:solid; border-bottom-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top; background-color:#cccccc">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:8pt"><span style="font-family:Arial">Programme Title:</span></p>
               </td>
               <td style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:8pt"><span style="font-family:Arial">&#xa0;</span></p>
               </td>
               <td style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top; background-color:#cccccc">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:8pt"><span style="font-family:Arial">Subject tutor:</span></p>
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:8pt"><span style="font-family:Arial">&#xa0;</span></p>
               </td>
               <td style="border-top-style:solid; border-top-width:0.75pt; border-left-style:solid; border-left-width:0.75pt; border-bottom-style:solid; border-bottom-width:0.75pt; padding-right:5.75pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:8pt"><span style="font-family:Arial">&#xa0;</span></p>
               </td>
               <td style="border-top-style:solid; border-top-width:0.75pt; border-bottom-style:solid; border-bottom-width:0.75pt; padding-right:5.75pt; padding-left:5.75pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:8pt"><span style="font-family:Arial">&#xa0;</span></p>
               </td>
               <td style="border-top-style:solid; border-top-width:0.75pt; border-bottom-style:solid; border-bottom-width:0.75pt; padding-right:5.38pt; padding-left:5.75pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:8pt"><span style="font-family:Arial">&#xa0;</span></p>
               </td>
            </tr>
            <tr>
               <td style="border-top-style:solid; border-top-width:0.75pt; border-right-style:solid; border-right-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top; background-color:#cccccc">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:8pt"><span style="font-family:Arial">Course </span></p>
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:8pt"><span style="font-family:Arial">Title:</span></p>
               </td>
               <td style="border-top-style:solid; border-top-width:0.75pt; border-right-style:solid; border-right-width:0.75pt; border-left-style:solid; border-left-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:8pt"><span style="font-family:Arial">&#xa0;</span></p>
               </td>
               <td style="border-top-style:solid; border-top-width:0.75pt; border-right-style:solid; border-right-width:0.75pt; border-left-style:solid; border-left-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top; background-color:#cccccc">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:8pt"><span style="font-family:Arial">Assignment No.</span></p>
               </td>
               <td style="border-top-style:solid; border-top-width:0.75pt; border-right-style:solid; border-right-width:0.75pt; border-left-style:solid; border-left-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:8pt"><span style="font-family:Arial">&#xa0;</span></p>
               </td>
               <td style="border-top-style:solid; border-top-width:0.75pt; border-right-style:solid; border-right-width:0.75pt; border-left-style:solid; border-left-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top; background-color:#cccccc">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:8pt"><span style="font-family:Arial">Due date:</span></p>
               </td>
               <td style="border-top-style:solid; border-top-width:0.75pt; border-left-style:solid; border-left-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:8pt"><span style="font-family:Arial">&#xa0;</span></p>
               </td>
            </tr>
         </table>
         <p style="margin-top:0pt; margin-bottom:0pt; font-size:11pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
         <p style="margin-top:0pt; margin-bottom:0pt; font-size:11pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
         <p style="margin-top:0pt; margin-bottom:0pt; font-size:11pt"><span style="font-family:Arial; font-weight:bold">Student Declaration:</span></p>
         <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Arial">I declare that the work submitted is my own work:</span></p>
         <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Arial">&#xa0;</span></p>
         <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
         <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Arial; font-weight:bold">Signed: </span></p>
         <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Arial">&#xa0;</span></p>
         <p style="margin-top:0pt; margin-bottom:0pt; font-size:11pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
         <p style="margin-top:0pt; margin-bottom:0pt; font-size:11pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
         <p style="margin-top:0pt; margin-bottom:0pt; font-size:11pt"><span style="font-family:Arial; font-weight:bold">Mentor Declaration:</span></p>
         <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Arial">I declare that the above student is known to me in my capacity as Mentor and that this is the work of that student:</span></p>
         <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Arial">&#xa0;</span></p>
         <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
         <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Arial; font-weight:bold">Signed:</span></p>
         <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Arial">&#xa0;</span></p>
         <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
         <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Arial; font-weight:bold">Designation:</span></p>
         <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
         <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
         <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
         <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
         <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
         <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
         <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
         <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
         <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
         <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
         <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
         <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
         <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
         <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
         <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
         <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
         <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
         <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
         <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
         <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
         <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
         <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
         <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
         <p style="margin-top:0pt; margin-bottom:0pt; font-size:12pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
         <p style="margin-top:0pt; margin-bottom:0pt; font-size:12pt"><span style="font-family:Arial; font-weight:bold">2. ASSESSMENT FEEDBACK </span><span style="font-family:Arial; font-size:11pt">(Reference must be made to the suggested evidence provided for each assignment)</span></p>
         <p style="margin-top:0pt; margin-bottom:0pt; font-size:12pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
         <table cellspacing="0" cellpadding="0" style="width:427.15pt; border:0.75pt solid #000000; border-collapse:collapse">
            <tr>
               <td style="border-right-style:solid; border-right-width:0.75pt; border-bottom-style:solid; border-bottom-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:11pt"><span style="font-family:Arial; font-weight:bold">Activity Title</span></p>
               </td>
               <td style="border-left-style:solid; border-left-width:0.75pt; border-bottom-style:solid; border-bottom-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:11pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
               </td>
            </tr>
            <tr>
               <td style="border-top-style:solid; border-top-width:0.75pt; border-right-style:solid; border-right-width:0.75pt; border-bottom-style:solid; border-bottom-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:11pt"><span style="font-family:Arial; font-weight:bold">LO</span></p>
               </td>
               <td style="border-top-style:solid; border-top-width:0.75pt; border-left-style:solid; border-left-width:0.75pt; border-bottom-style:solid; border-bottom-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:11pt"><span style="font-family:Arial; font-weight:bold">Feedback/evidence</span></p>
               </td>
            </tr>
            <tr>
               <td style="border-top-style:solid; border-top-width:0.75pt; border-right-style:solid; border-right-width:0.75pt; border-bottom-style:solid; border-bottom-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:11pt"><span style="font-family:Arial; font-weight:bold">AC</span></p>
               </td>
               <td style="border-top-style:solid; border-top-width:0.75pt; border-left-style:solid; border-left-width:0.75pt; border-bottom-style:solid; border-bottom-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:11pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:11pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:11pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:11pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:11pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
               </td>
            </tr>
            <tr>
               <td style="border-top-style:solid; border-top-width:0.75pt; border-right-style:solid; border-right-width:0.75pt; border-bottom-style:solid; border-bottom-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:11pt"><span style="font-family:Arial; font-weight:bold">LO</span></p>
               </td>
               <td style="border-top-style:solid; border-top-width:0.75pt; border-left-style:solid; border-left-width:0.75pt; border-bottom-style:solid; border-bottom-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:11pt"><span style="font-family:Arial; font-weight:bold">Feedback/evidence</span></p>
               </td>
            </tr>
            <tr>
               <td style="border-top-style:solid; border-top-width:0.75pt; border-right-style:solid; border-right-width:0.75pt; border-bottom-style:solid; border-bottom-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:11pt"><span style="font-family:Arial; font-weight:bold">AC</span></p>
               </td>
               <td style="border-top-style:solid; border-top-width:0.75pt; border-left-style:solid; border-left-width:0.75pt; border-bottom-style:solid; border-bottom-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:11pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:11pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:11pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:11pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:11pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
               </td>
            </tr>
            <tr>
               <td style="border-top-style:solid; border-top-width:0.75pt; border-right-style:solid; border-right-width:0.75pt; border-bottom-style:solid; border-bottom-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:11pt"><span style="font-family:Arial; font-weight:bold">LO</span></p>
               </td>
               <td style="border-top-style:solid; border-top-width:0.75pt; border-left-style:solid; border-left-width:0.75pt; border-bottom-style:solid; border-bottom-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:11pt"><span style="font-family:Arial; font-weight:bold">Feedback/evidence</span></p>
               </td>
            </tr>
            <tr>
               <td style="border-top-style:solid; border-top-width:0.75pt; border-right-style:solid; border-right-width:0.75pt; border-bottom-style:solid; border-bottom-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:11pt"><span style="font-family:Arial; font-weight:bold">AC</span></p>
               </td>
               <td style="border-top-style:solid; border-top-width:0.75pt; border-left-style:solid; border-left-width:0.75pt; border-bottom-style:solid; border-bottom-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:11pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:11pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:11pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:11pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:11pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
               </td>
            </tr>
            <tr>
               <td style="border-top-style:solid; border-top-width:0.75pt; border-right-style:solid; border-right-width:0.75pt; border-bottom-style:solid; border-bottom-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:11pt"><span style="font-family:Arial; font-weight:bold">LO</span></p>
               </td>
               <td style="border-top-style:solid; border-top-width:0.75pt; border-left-style:solid; border-left-width:0.75pt; border-bottom-style:solid; border-bottom-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:11pt"><span style="font-family:Arial; font-weight:bold">Feedback/evidence</span></p>
               </td>
            </tr>
            <tr>
               <td style="border-top-style:solid; border-top-width:0.75pt; border-right-style:solid; border-right-width:0.75pt; border-bottom-style:solid; border-bottom-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:11pt"><span style="font-family:Arial; font-weight:bold">AC</span></p>
               </td>
               <td style="border-top-style:solid; border-top-width:0.75pt; border-left-style:solid; border-left-width:0.75pt; border-bottom-style:solid; border-bottom-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:11pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:11pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:11pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:11pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:11pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
               </td>
            </tr>
            <tr>
               <td style="border-top-style:solid; border-top-width:0.75pt; border-right-style:solid; border-right-width:0.75pt; border-bottom-style:solid; border-bottom-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:11pt"><span style="font-family:Arial; font-weight:bold">LO</span></p>
               </td>
               <td style="border-top-style:solid; border-top-width:0.75pt; border-left-style:solid; border-left-width:0.75pt; border-bottom-style:solid; border-bottom-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:11pt"><span style="font-family:Arial; font-weight:bold">Feedback/evidence</span></p>
               </td>
            </tr>
            <tr>
               <td style="border-top-style:solid; border-top-width:0.75pt; border-right-style:solid; border-right-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:11pt"><span style="font-family:Arial; font-weight:bold">AC</span></p>
               </td>
               <td style="border-top-style:solid; border-top-width:0.75pt; border-left-style:solid; border-left-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:11pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:11pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:11pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:11pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:11pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
               </td>
            </tr>
         </table>
         <p style="margin-top:0pt; margin-bottom:0pt; font-size:12pt"><span style="font-family:Arial">&#xa0;</span></p>
         <div style="border:0.75pt solid #000000; clear:both">
            <p style="margin-top:0pt; margin-bottom:0pt; padding-top:1pt; padding-right:4pt; padding-left:4pt; font-size:10pt"><span style="font-family:Arial; font-weight:bold">Grade Awarded: Pass/Fail/Referral </span><span style="font-family:Arial">(please circle)</span></p>
            <p style="margin-top:0pt; margin-bottom:0pt; padding-right:4pt; padding-left:4pt; font-size:10pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
            <p style="margin-top:0pt; margin-bottom:0pt; padding-right:4pt; padding-left:4pt; font-size:10pt"><span style="font-family:Arial; font-weight:bold">General Comments:</span></p>
            <p style="margin-top:0pt; margin-bottom:0pt; padding-right:4pt; padding-left:4pt; font-size:10pt"><span style="font-family:Arial">&#xa0;</span></p>
            <p style="margin-top:0pt; margin-bottom:0pt; padding-right:4pt; padding-left:4pt; font-size:10pt"><span style="font-family:Arial">&#xa0;</span></p>
            <p style="margin-top:0pt; margin-bottom:0pt; padding-right:4pt; padding-left:4pt; font-size:10pt"><span style="font-family:Arial">&#xa0;</span></p>
            <p style="margin-top:0pt; margin-bottom:0pt; padding-right:4pt; padding-left:4pt; font-size:10pt"><span style="font-family:Arial">&#xa0;</span></p>
            <p style="margin-top:0pt; margin-bottom:0pt; padding-right:4pt; padding-left:4pt; font-size:10pt"><span style="font-family:Arial">&#xa0;</span></p>
            <p style="margin-top:0pt; margin-bottom:0pt; padding-right:4pt; padding-left:4pt; font-size:10pt"><span style="font-family:Arial">&#xa0;</span></p>
            <p style="margin-top:0pt; margin-bottom:0pt; padding-right:4pt; padding-left:4pt; font-size:10pt"><span style="font-family:Arial">&#xa0;</span></p>
            <p style="margin-top:0pt; margin-bottom:0pt; padding-right:4pt; padding-left:4pt; font-size:10pt"><span style="font-family:Arial">&#xa0;</span></p>
            <p style="margin-top:0pt; margin-bottom:0pt; padding-right:4pt; padding-left:4pt; font-size:10pt"><span style="font-family:Arial">&#xa0;</span></p>
            <p style="margin-top:0pt; margin-bottom:0pt; padding-right:4pt; padding-left:4pt; padding-bottom:1pt; font-size:10pt"><span style="font-family:Arial">&#xa0;</span></p>
         </div>
         <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Arial">&#xa0;</span></p>
         <table cellspacing="0" cellpadding="0" style="width:435pt; border:0.75pt solid #000000; border-collapse:collapse">
            <tr style="height:18pt">
               <td style="border-right-style:solid; border-right-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top; background-color:#cccccc">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:8pt"><span style="font-family:Arial; font-weight:bold">Hand in date:</span></p>
               </td>
               <td style="border-right-style:solid; border-right-width:0.75pt; border-left-style:solid; border-left-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:12pt"><span style="font-family:Arial">&#xa0;</span></p>
               </td>
               <td style="border-right-style:solid; border-right-width:0.75pt; border-left-style:solid; border-left-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top; background-color:#cccccc">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:8pt"><span style="font-family:Arial; font-weight:bold">Date returned:</span></p>
               </td>
               <td style="border-right-style:solid; border-right-width:0.75pt; border-left-style:solid; border-left-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:12pt"><span style="font-family:Arial">&#xa0;</span></p>
               </td>
               <td style="border-right-style:solid; border-right-width:0.75pt; border-left-style:solid; border-left-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top; background-color:#cccccc">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:8pt"><span style="font-family:Arial; font-weight:bold">Signed tutor:</span></p>
               </td>
               <td style="border-left-style:solid; border-left-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:12pt"><span style="font-family:Arial">&#xa0;</span></p>
               </td>
            </tr>
         </table>
         <p style="margin-top:0pt; margin-right:2.15pt; margin-bottom:0pt; font-size:14pt"><span style="font-family:Arial; font-weight:bold">Assignment</span></p>
         <table cellspacing="0" cellpadding="0" style="width:509pt; border-collapse:collapse">
            <tr style="height:39pt">
               <td colspan="5" style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:12pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:12pt"><span style="font-family:Arial; font-weight:bold">Learner Name:</span><span style="width:23.96pt; display:inline-block">&#xa0;</span><span style="width:36pt; display:inline-block">&#xa0;</span></p>
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:12pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
               </td>
               <td colspan="4" style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:12pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:12pt"><span style="font-family:Arial; font-weight:bold">Learner Registration ID :</span></p>
               </td>
            </tr>
            <tr style="height:27pt">
               <td colspan="9" style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:12pt"><span style="font-family:Arial">&#xa0;</span></p>
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:12pt"><span style="font-family:Arial; font-weight:bold">Qualification:</span><span style="width:31.33pt; display:inline-block">&#xa0;</span></p>
               </td>
            </tr>
            <tr style="height:95pt">
               <td colspan="9" style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:12pt"><span style="font-family:Arial; font-weight:bold">Unit Title/Level :  </span></p>
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:12pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:12pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:12pt"><span style="font-family:Arial; font-weight:bold">Assessment criteria covered</span></p>
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:12pt"><span style="font-family:Arial; font-weight:bold">in the assignment:</span><span style="width:1.99pt; display:inline-block">&#xa0;</span></p>
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:12pt"><span style="width:36pt; display:inline-block">&#xa0;</span><span style="width:36pt; display:inline-block">&#xa0;</span></p>
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:12pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
               </td>
            </tr>
            <tr style="height:11pt">
               <td colspan="9" style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
               </td>
            </tr>
            <tr style="height:29pt">
               <td colspan="9" style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:14pt"><span style="font-family:Arial; font-weight:bold">Assignment Title: </span></p>
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:12pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
               </td>
            </tr>
            <tr style="height:11pt">
               <td colspan="9" style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
               </td>
            </tr>
            <tr style="height:34pt">
               <td colspan="9" style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Arial; font-weight:bold">Assignment Submission Schedule: </span></p>
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Arial">&#xa0;</span></p>
               </td>
            </tr>
            <tr style="height:13pt">
               <td colspan="2" style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:12pt"><span style="font-family:Calibri; font-weight:bold; font-style:italic">Assignment/Tasks  </span></p>
               </td>
               <td colspan="4" style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:12pt"><span style="font-family:Calibri; font-weight:bold; font-style:italic">Assignment/tasks hand out date</span></p>
               </td>
               <td colspan="3" style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:12pt"><span style="font-family:Calibri; font-weight:bold; font-style:italic">Assignment/tasks hand in date</span></p>
               </td>
            </tr>
            <tr style="height:13pt">
               <td colspan="2" style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:6pt"><span style="font-family:Calibri; font-weight:bold">&#xa0;</span></p>
               </td>
               <td colspan="4" style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; text-align:justify; font-size:12pt"><span style="font-family:Calibri">&#xa0;</span></p>
               </td>
               <td colspan="3" style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:12pt"><span style="font-family:Calibri">&#xa0;</span></p>
               </td>
            </tr>
            <tr style="height:13pt">
               <td colspan="2" style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:6pt"><span style="font-family:Calibri; font-weight:bold">&#xa0;</span></p>
               </td>
               <td colspan="4" style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; text-align:justify; font-size:12pt"><span style="font-family:Calibri">&#xa0;</span></p>
               </td>
               <td colspan="3" style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:12pt"><span style="font-family:Calibri">&#xa0;</span></p>
               </td>
            </tr>
            <tr style="height:14pt">
               <td colspan="2" style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:6pt"><span style="font-family:Calibri; font-weight:bold">&#xa0;</span></p>
               </td>
               <td colspan="4" style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; text-align:justify; font-size:12pt"><span style="font-family:Calibri">&#xa0;</span></p>
               </td>
               <td colspan="3" style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:12pt"><span style="font-family:Calibri">&#xa0;</span></p>
               </td>
            </tr>
            <tr style="height:13pt">
               <td colspan="2" style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:6pt"><span style="font-family:Calibri; font-weight:bold">&#xa0;</span></p>
               </td>
               <td colspan="4" style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; text-align:justify; font-size:12pt"><span style="font-family:Calibri">&#xa0;</span></p>
               </td>
               <td colspan="3" style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:12pt"><span style="font-family:Calibri">&#xa0;</span></p>
               </td>
            </tr>
            <tr style="height:13pt">
               <td colspan="2" style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:6pt"><span style="font-family:Calibri; font-weight:bold">&#xa0;</span></p>
               </td>
               <td colspan="4" style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; text-align:justify; font-size:12pt"><span style="font-family:Calibri">&#xa0;</span></p>
               </td>
               <td colspan="3" style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:12pt"><span style="font-family:Calibri">&#xa0;</span></p>
               </td>
            </tr>
            <tr style="height:13pt">
               <td colspan="2" style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:6pt"><span style="font-family:Calibri; font-weight:bold">&#xa0;</span></p>
               </td>
               <td colspan="4" style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; text-align:justify; font-size:12pt"><span style="font-family:Calibri">&#xa0;</span></p>
               </td>
               <td colspan="3" style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:12pt"><span style="font-family:Calibri">&#xa0;</span></p>
               </td>
            </tr>
            <tr style="height:13pt">
               <td colspan="2" style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:6pt"><span style="font-family:Calibri; font-weight:bold">&#xa0;</span></p>
               </td>
               <td colspan="4" style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; text-align:justify; font-size:12pt"><span style="font-family:Calibri">&#xa0;</span></p>
               </td>
               <td colspan="3" style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:12pt"><span style="font-family:Calibri">&#xa0;</span></p>
               </td>
            </tr>
            <tr style="height:14pt">
               <td colspan="9" style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:12pt"><span style="font-family:Calibri; font-weight:bold">Assessment Decisions:  Criteria achieved or not achieved</span></p>
               </td>
            </tr>
            <tr style="height:21pt">
               <td style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; text-align:center; font-size:10pt"><span style="font-family:Calibri">&#xa0;</span></p>
                  <p style="margin-top:0pt; margin-bottom:0pt; text-align:center; font-size:10pt"><span style="font-family:Calibri">AC 1.1</span></p>
               </td>
               <td colspan="2" style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Calibri">&#xa0;</span></p>
               </td>
               <td style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; text-align:center; font-size:10pt"><span style="font-family:Calibri">&#xa0;</span></p>
                  <p style="margin-top:0pt; margin-bottom:0pt; text-align:center; font-size:10pt"><span style="font-family:Calibri">AC 2.3</span></p>
               </td>
               <td colspan="3" style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Calibri">&#xa0;</span></p>
               </td>
               <td style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; text-align:center; font-size:10pt"><span style="font-family:Calibri">&#xa0;</span></p>
                  <p style="margin-top:0pt; margin-bottom:0pt; text-align:center; font-size:10pt"><span style="font-family:Calibri">AC 4.1</span></p>
               </td>
               <td style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:12pt"><span style="font-family:Calibri">&#xa0;</span></p>
               </td>
            </tr>
            <tr style="height:25pt">
               <td style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Calibri">&#xa0;</span></p>
                  <p style="margin-top:0pt; margin-bottom:0pt; text-align:center; font-size:10pt"><span style="font-family:Calibri">AC 1.2</span></p>
               </td>
               <td colspan="2" style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Calibri">&#xa0;</span></p>
               </td>
               <td style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Calibri">&#xa0;</span></p>
                  <p style="margin-top:0pt; margin-bottom:0pt; text-align:center; font-size:10pt"><span style="font-family:Calibri">AC 2.4</span></p>
               </td>
               <td colspan="3" style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Calibri">&#xa0;</span></p>
               </td>
               <td style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Calibri">&#xa0;</span></p>
                  <p style="margin-top:0pt; margin-bottom:0pt; text-align:center; font-size:10pt"><span style="font-family:Calibri">AC 4.2</span></p>
               </td>
               <td style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:12pt"><span style="font-family:Calibri">&#xa0;</span></p>
               </td>
            </tr>
            <tr style="height:25pt">
               <td style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Calibri">&#xa0;</span></p>
                  <p style="margin-top:0pt; margin-bottom:0pt; text-align:center; font-size:10pt"><span style="font-family:Calibri">AC 1.3</span></p>
               </td>
               <td colspan="2" style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Calibri">&#xa0;</span></p>
               </td>
               <td style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Calibri">&#xa0;</span></p>
                  <p style="margin-top:0pt; margin-bottom:0pt; text-align:center; font-size:10pt"><span style="font-family:Calibri">AC 3.1</span></p>
               </td>
               <td colspan="3" style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Calibri">&#xa0;</span></p>
               </td>
               <td style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Calibri">&#xa0;</span></p>
                  <p style="margin-top:0pt; margin-bottom:0pt; text-align:center; font-size:10pt"><span style="font-family:Calibri">AC 4.3</span></p>
               </td>
               <td style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:12pt"><span style="font-family:Calibri">&#xa0;</span></p>
               </td>
            </tr>
            <tr style="height:3pt">
               <td style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Calibri">&#xa0;</span></p>
                  <p style="margin-top:0pt; margin-bottom:0pt; text-align:center; font-size:10pt"><span style="font-family:Calibri">AC 2.1</span></p>
               </td>
               <td colspan="2" style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Calibri">&#xa0;</span></p>
               </td>
               <td style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Calibri">&#xa0;</span></p>
                  <p style="margin-top:0pt; margin-bottom:0pt; text-align:center; font-size:10pt"><span style="font-family:Calibri">AC 3.2</span></p>
               </td>
               <td colspan="3" style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Calibri">&#xa0;</span></p>
               </td>
               <td style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Calibri">&#xa0;</span></p>
                  <p style="margin-top:0pt; margin-bottom:0pt; text-align:center; font-size:10pt"><span style="font-family:Calibri">AC 5.1</span></p>
               </td>
               <td style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:12pt"><span style="font-family:Calibri">&#xa0;</span></p>
               </td>
            </tr>
            <tr style="height:25pt">
               <td style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Calibri">&#xa0;</span></p>
                  <p style="margin-top:0pt; margin-bottom:0pt; text-align:center; font-size:10pt"><span style="font-family:Calibri">AC 2.2</span></p>
               </td>
               <td colspan="2" style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Calibri">&#xa0;</span></p>
               </td>
               <td style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Calibri">&#xa0;</span></p>
                  <p style="margin-top:0pt; margin-bottom:0pt; text-align:center; font-size:10pt"><span style="font-family:Calibri">AC 3.3</span></p>
               </td>
               <td colspan="3" style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Calibri">&#xa0;</span></p>
               </td>
               <td style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Calibri">&#xa0;</span></p>
                  <p style="margin-top:0pt; margin-bottom:0pt; text-align:center; font-size:10pt"><span style="font-family:Calibri">AC 5.2</span></p>
               </td>
               <td style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Calibri">&#xa0;</span></p>
               </td>
            </tr>
            <tr style="height:88pt">
               <td colspan="9" style="border-style:solid; border-width:0.75pt; padding-right:5.38pt; padding-left:5.38pt; vertical-align:top">
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Arial; font-weight:bold">Overall Comments:</span></p>
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><span style="width:36pt; display:inline-block">&#xa0;</span><span style="width:36pt; display:inline-block">&#xa0;</span><span style="width:36pt; display:inline-block">&#xa0;</span></p>
                  <p style="margin-top:0pt; margin-bottom:0pt; font-size:10pt"><a name="_gjdgxs"></a><span style="font-family:Arial; font-weight:bold">&#xa0;</span></p>
               </td>
            </tr>
            <tr style="height:0pt">
               <td style="width:92.05pt"></td>
               <td style="width:91.05pt"></td>
               <td style="width:1pt"></td>
               <td style="width:92.05pt"></td>
               <td style="width:29.6pt"></td>
               <td style="width:62.15pt"></td>
               <td style="width:0.3pt"></td>
               <td style="width:92.15pt"></td>
               <td style="width:47.9pt"></td>
            </tr>
         </table>
         <p style="margin-top:0pt; margin-bottom:0pt; font-size:11pt"><span style="font-family:Arial">&#xa0;</span></p>
      </div>
   </body>
</html>