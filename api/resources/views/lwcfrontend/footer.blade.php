<!-- FOOTER -->
<footer class="tt-footer">
    <div class="container">
        <div class="tt-banner-2">
            <div class="row">
                <div class="col-md-6">
                    <h5 class="tt-banner-2-title c-h5">{{ trans('static.footer.stay_tuned_with_us') }}</h5>
                    <div class="simple-text color-6">
                        <p>{{ trans('static.footer.description_stay_tuned') }}</p>
                    </div>
                    <div class="empty-space marg-sm-b5"></div>
                </div>
                <div class="col-md-6">
                    <div class="tt-banner-2-right">
                        <form action="{{ route('subcribe') }}" method="post">
                            <div class="row">
                                <div class="col-sm-8">
                                    <input class="c-input type-2" type="text" name="email_subcribe" required="" placeholder="{{ trans('static.footer.enter_email') }}">
                                     {{ csrf_field() }}
                                    @if ($errors->has('email_subcribe'))
                                        <span style="color: red">
                                            <strong>{{ $errors->first('email_subcribe') }}</strong>
                                        </span>
                                    @endif
                                    @if ($errors->has('error'))
                                        <span style="color: red">
                                            <strong>{{ $errors->first('errorSubscribe') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="col-sm-4">
                                    <div class="c-btn type-1 color-3 full">
                                        <input type="submit" value="{{ trans('static.footer.subscribe') }}">
                                        <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @if(isset($config['webinfo']['footer']))
            {!! $config['webinfo']['footer'] or '' !!}
        @endif
    </div>
</footer>