<div class="tt-search-popup">
    <div class="tt-search-popup-vertical">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0">
                    <form action="{{ route('search') }}" method="get">
                        <h3 class="tt-search-popup-title">{{ trans('static.search.search') }}</h3>
                        <div class="tt-search-popup-field">
                            <input type="text" name="s" placeholder="{{ trans('static.search.enter_keyword') }}" class="input" required value="{{ request()->get('s') }}">
                            <div class="tt-search-popup-devider"></div>
                            <div class="tt-search-popup-submit">
                                <i class="fa fa-search"></i>
                                <input type="submit" value="">
                            </div>
                        </div>
                        <a href="javascript:void(0)" class="close"><span>+</span></a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="tt-header-margin"></div>