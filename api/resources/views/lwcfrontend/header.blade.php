<header class="tt-header">
    <div class="container">
        <div class="top-inner">
            <a class="logo" href="{{ route('home') }}"><img src="{{ asset('lwcfrontend/img/header/logo.png') }}" height="60" width="258" alt=""></a>
        </div>
        <button class="cmn-toggle-switch"><span></span></button>
        <div class="toggle-block">
            <nav class="main-nav clearfix">
                <ul>
                    <li class="active"><a href="{{ route('home') }}">{{ trans('static.header.home') }}</a></li>
                    <li><a href="{{ route('getPageBySlug', ['about-us']) }}">{{ trans('static.header.about_us') }}</a></li>
                    <li>
                        <a href="{{ route('getPageBySlug', ['courses']) }}">{{ trans('static.header.courses') }} <i class="menu-toggle fa fa-angle-down"></i></a>
                        <ul>
                            <li><a href="{{ route('getPageBySlug', ['tourism']) }}">{{ trans('static.header.tourism') }}</a></li>
                            <li><a href="{{ route('getPageBySlug', ['business']) }}">{{ trans('static.header.business') }}</a></li>
                            <li><a href="{{ route('getPageBySlug', ['accounting']) }}">{{ trans('static.header.accounting') }}</a></li>
                            <li><a href="{{ route('getPageBySlug', ['finance']) }}">{{ trans('static.header.finance') }}</a></li>
                            <li><a href="{{ route('getPageBySlug', ['hospitality']) }}">{{ trans('static.header.hospitality') }}</a></li>
                            <li><a href="{{ route('getPageBySlug', ['marketing']) }}">{{ trans('static.header.marketing') }}</a></li>
                        </ul>
                    </li>
                    <li><a href="{{ route('getPageBySlug', ['how-to-learn']) }}">{{ trans('static.header.how_to_learn') }}</a></li>
                </ul>
            </nav>
            <div class="nav-more">
                <a class="tt-top-search" href="#">
                    <i class="stroke-icon icon-Search"></i>
                </a>
            </div>
            <div class="top-line">
                <div class="container">
                    <div class="btn-group dropdown lang-dropdown">
                        <div class="dropdown-toggle fxac " data-toggle="dropdown">
                            <a class="gray-link" href="javascript:void(0)">
                                <i class="icon-globe fa fa-globe mr5"></i>
                                {{ config('app.locales.' . request()->session()->get('lwclocale')) }}
                                <i class="icon-caret-up fa fa-caret-down user-caret ml5"></i>
                            </a>
                        </div>
                        <div class="dropdown-menu">
                            <ul>
                                @foreach(config('app.locales') as $key => $language)
                                    <li>
                                        <a href="{{ route('lang.switch', $key) }}" class="locale-link">
                                            {{ $language }}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    @if(isset($config['webinfo']['topinfo']))
                        {!! $config['webinfo']['topinfo'] or '' !!}
                    @endif
                    @if(!Auth::check())
                        <div class="top-info">
                            <i class="fa fa-unlock-alt" aria-hidden="true"></i>
                            <a href="/register">{{ trans('static.header.register') }}</a>
                            <a href="/login">{{ trans('static.header.login') }}</a>
                        </div>
                    @else
                        <?php $user = Auth::getUser(); ?>
                        <div class="top-info">
                            <a href="javascript:void(0);">{{ trans('static.header.hi') }}! {{ $user->name }}</a>
                            <a href="{{ route('frontend.logout') }}">{{ trans('static.header.logout') }}</a>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</header>