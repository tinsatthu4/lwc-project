<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="vi"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="vi"> <![endif]-->
<html lang="vi">
<head>
<meta charset="UTF-8">
@include("lwcfrontend.composer.head")
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- FONTS -->
{{--<link href="{{ asset('frontend/css/font-awesome.min.css') }}" rel="stylesheet">--}}
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css" rel="stylesheet">
<link href="{{ asset('lwcfrontend/css/stroke-gap.min.css') }}" rel="stylesheet">
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Raleway:400,500,700,800' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=PT+Serif:400,400italic' rel='stylesheet' type='text/css'>
<!-- CSS -->
<link href="{{ asset('lwcfrontend/css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('lwcfrontend/css/idangerous.swiper.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('lwcfrontend/css/style.css') }}" rel="stylesheet" type="text/css" />
@yield('css')
<link href="{{ asset('lwcfrontend/style.css') }}" rel="stylesheet">
<link href="{{ asset('lwcfrontend/custom.css') }}" rel="stylesheet">
</head>
<body>
<!-- LOADER -->
<div id="loader-wrapper">
    <div id="loader-container">
        <p id="loadingText">Loading</p>
    </div>
</div>
<?php $config = SettingRepository::loadGroup('webinfo');?>
@include('lwcfrontend.header')
@include('lwcfrontend.search')
@yield('main')
@include('lwcfrontend.footer')
<script src="{{ asset('lwcfrontend/js/jquery.min.js') }}"></script>
<script src="{{ asset('lwcfrontend/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('lwcfrontend/js/idangerous.swiper.min.js') }}"></script>
<script src="{{ asset('lwcfrontend/js/jquery.countTo.js') }}"></script>
<script src="{{ asset('lwcfrontend/js/jquery.viewportchecker.min.js') }}"></script>
<script src="{{ asset('lwcfrontend/js/global.js') }}"></script>
<script src="{{ asset('lwcfrontend/custom.js') }}"></script>
@yield('custom_js')
</body>
</html>