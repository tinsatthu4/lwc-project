    <title>{!!  $theme_title or null  !!}</title>
    <meta name="keywords" content="{!! $theme_meta_keywords or null !!}" />
    <meta name="description" content="{!! $theme_meta_description or null !!}">
    {{--<meta name="author" content="{!! $theme_meta_author or null !!}">--}}
    <link rel="canonical" href="{!! $theme_canonical or null !!}" />
    <meta property="og:locale" content="vi_VN" />
    <meta property="og:type" content="{{ $theme_og_type or 'product' }}" />
    @if(isset($theme_image))
    <meta property="og:image" content="{{ $theme_image or null }}" />
    @endif
    <meta property="og:title" content="{!!  $theme_title or null  !!}" />
    <meta property="og:description" content="{!! $theme_meta_keywords or null !!}" />
    <meta property="og:url" content="{!! $theme_canonical or null!!}" />
    <meta property="og:site_name" content="{!! $theme_meta_description or null !!}" />