<p>Thank you for your interest applying on the course with LAA. We are happy to provide you the information and the price to enrol on your studies. The link provided below is a price summary and the payment methods available. </p>
<a href="{{ $link }}">Payment Link</a>
<br/>
<p>We look forward to hear from you again, if you have any queries to your course, please do not hesitate to contact me.</p>
<br/>
<p>Thank you</p>
<p>Miss Kahyan Teo</p>
<p>Centre Director</p>