<!DOCTYPE html>
<html ng-app="PortalApp">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>London Westminster College</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="components/AdminLTE/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Summernote -->
    <link rel="stylesheet" href="components/summernote/dist/summernote.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="components/AdminLTE/dist/css/AdminLTE.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="components/AdminLTE/plugins/select2/select2.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="components/AdminLTE/plugins/datepicker/datepicker3.css">
    <!-- fullCalendar 2.2.5-->
    <link rel="stylesheet" href="components/AdminLTE/plugins/fullcalendar/fullcalendar.min.css">
    <link rel="stylesheet" href="components/AdminLTE/plugins/fullcalendar/fullcalendar.print.css" media="print">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="components/AdminLTE/dist/css/skins/skin-blue.min.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="components/AdminLTE/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- PLugins-->
    <link rel="stylesheet" href="components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
    <style>
        .bootstrap-tagsinput {
            display: block;
        }
        .bootstrap-tagsinput span.tag {
            display: inline;
            padding: .2em .6em .3em;
            font-size: 100%;
            font-weight: 700;
            line-height: 2;
            color: #fff;
            text-align: center;
            white-space: nowrap;
            vertical-align: baseline;
            border-radius: .25em;
            text-align: center;
            white-space: nowrap;
            vertical-align: baseline;
            border-radius: .25em;
            background-color: #00c0ef !important;
            color: #fff !important;
        }
        body.background-london {
            background: url(http://awards-uk.com/wp-content/uploads/2017/01/banner-1-2.jpg) no-repeat center center;
        }
    </style>
</head>
<body>
<app></app>
<div id="page-loading" class="hide" style=" position: fixed; z-index: 99999; top: 20px; right: 20px;">
    <div class="alert alert-success alert-dismissible">
        <i class="fa fa-spinner fa-spin fa-1x fa-fw"></i>
        <span class="">Loading...</span>
        </div>
</div>
<script src="https://www.google.com/recaptcha/api.js?onload=vcRecaptchaApiLoaded&render=explicit" async defer ></script>
<script src="app-dist/vendor.js"></script>
<script src="app-dist/1482164006150.app.js"></script>
</body>
</html>
