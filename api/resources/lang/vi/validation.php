<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => ':attribute phải được chấp nhận.',
    'active_url'           => ':attribute không phải là một URL hợp lệ.',
    'after'                => ':attribute phải là một ngày sau :date.',
    'alpha'                => ':attribute chỉ có thể chứa các chữ cái.',
    'alpha_dash'           => ':attribute chỉ có thể chứa các chữ cái, số và dấu gạch ngang.',
    'alpha_num'            => ':attribute chỉ có thể chứa các chữ cái và số.',
    'array'                => ':attribute phải là một mảng.',
    'before'               => ':attribute phải là một ngày trước :date.',
    'between'              => [
        'numeric' => ':attribute phải giữa :min và :max.',
        'file'    => ':attribute phải giữa :min và :max kilobytes.',
        'string'  => ':attribute phải giữa :min và :max kí tự.',
        'array'   => ':attribute phải giữa :min và :max items.',
    ],
    'boolean'              => ':attribute field phải true hoặc false.',
    'confirmed'            => ':attribute xác nhận không phù hợp.',
    'date'                 => ':attribute không phải ngày hợp lệ.',
    'date_format'          => ':attribute không phù hợp với định dạng :format.',
    'different'            => ':attribute và :other phải khác nhau.',
    'digits'               => ':attribute phải :digits chữ số.',
    'digits_between'       => ':attribute phải giữa :min và :max chữ số.',
    'distinct'             => ':attribute field có giá trị trùng lặp.',
    'email'                => ':attribute phải là email hợp lệ.',
    'exists'               => 'selected :attribute không hợp lệ.',
    'filled'               => ':attribute field không để trống.',
    'image'                => ':attribute phải là hình ảnh.',
    'in'                   => 'selected :attribute không hợp lệ.',
    'in_array'             => ':attribute field không tồn tại trong :other.',
    'integer'              => ':attribute phải là số.',
    'ip'                   => ':attribute phải là địa chỉ IP hợp lệ.',
    'json'                 => ':attribute phải là chuỗi JSON hợp lệ.',
    'max'                  => [
        'numeric' => ':attribute không lớn hơn :max.',
        'file'    => ':attribute không lớn hơn :max kilobytes.',
        'string'  => ':attribute không lớn hơn :max ký tự.',
        'array'   => ':attribute không có nhiều hơn :max items.',
    ],
    'mimes'                => ':attribute phải là file dạng: :values.',
    'min'                  => [
        'numeric' => ':attribute phải ít nhất :min.',
        'file'    => ':attribute phải ít nhất :min kilobytes.',
        'string'  => ':attribute phải ít nhất :min ký tự.',
        'array'   => ':attribute phải có ít nhất :min items.',
    ],
    'not_in'               => 'selected :attribute không hợp lệ.',
    'numeric'              => ':attribute phải là số.',
    'present'              => ':attribute field phải được hiền thị.',
    'regex'                => ':attribute định dạng không hợp lệ.',
    'required'             => ':attribute field không để trống.',
    'required_if'          => ':attribute field được yêu cầu khi :other là :value.',
    'required_unless'      => ':attribute field được yêu cầu nếu :other trong :values.',
    'required_with'        => ':attribute field được yêu cầu khi :values hiển thị.',
    'required_with_all'    => ':attribute field được yêu cầu khi :values hiển thị.',
    'required_without'     => ':attribute field được yêu cầu khi :values không hiển thị.',
    'required_without_all' => ':attribute field được yêu cầu khi none of :values hiển thị.',
    'same'                 => ':attribute và :other phải trùng khớp.',
    'size'                 => [
        'numeric' => ':attribute phải :size.',
        'file'    => ':attribute phải :size kilobytes.',
        'string'  => ':attribute phải :size characters.',
        'array'   => ':attribute phải chứa :size items.',
    ],
    'string'               => ':attribute phải là chuỗi.',
    'timezone'             => ':attribute phải là zone hợp lệ.',
    'unique'               => ':attribute đã tồn tại.',
    'url'                  => ':attribute định dạng không hợp lệ.',
    'has_policy_task'      => ':attribute không hợp lệ.',
    'match_task_keyword'   => ':attribute không khớp với task keywords.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
