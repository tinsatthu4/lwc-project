<?php

return [
    'master'   => [],
    'header'   => [
        'home'       => 'Trang chủ',
        'about_us'   => 'Về chúng tôi',
        'courses'    => 'Khóa học',
        'tourism'    => 'Du lịch',
        'business'   => 'Kinh doanh',
        'accounting' => 'Kế toán',
        'finance'    => 'Tài chính',
        'hospitality' => 'Dịch vụ',
        'marketing' => 'Marketing',
        'how_to_learn' => 'Làm sao để học',
        'register'  => 'Đăng ký',
        'login'     => 'Đăng nhập',
        'hi'        => 'Chào',
        'logout'    => 'Đăng xuất'
    ],
    'footer'   => [
        'stay_tuned_with_us' => 'HÃY Ở LẠI VỚI CHÚNG TÔI',
        'description_stay_tuned' => 'Nhận thông tin cập nhật của chúng tôi về tài liệu giáo dục, khóa học mới và nhiều hơn nữa!',
        'enter_email'   => 'Nhập địa chỉ email',
        'subscribe'     => 'subscribe'
    ],
    'search'   => [
        'search'    => 'Tìm kiếm',
        'enter_keyword' => 'Nhập từ khóa',
        'title'     => 'Tìm kiếm với từ khóa '
    ],
    'login'    => [
        'login'     => 'Đăng nhập',
        'email'     => 'Địa chỉ E-Mail',
        'password'  => 'Mật khẩu',
        'remember_me' => 'Lưu mật khẩu',
        'forgot_your_password' => 'Quên mật khẩu?'
    ],
    'register' => [
        'register'  => 'Đăng ký',
        'name'      => 'Tên',
        'email'     => 'Địa chỉ E-Mail',
        'password'  => 'Mật khẩu',
        'confirm_password' => 'Xác nhận mật khẩu',
    ],
    'home'  => [
        'registration' => 'Đăng ký',
        'description_registration' => 'Số lượng có hạn! Nhanh lên nào',
        'days' => 'ngày',
        'hours' => 'giờ',
        'minutes' => 'phút',
        'seconds' => 'giây',
        'sign_up_now' => 'Đăng ký ngay',
        'description_courses' => 'Đăng ký khóa học',
        'name' => 'Tên của bạn',
        'email' => 'Email của bạn',
        'get_free_quote' => 'Nhận miễn phí trích dẫn'
    ],
    'member' => [
        'change_password' => [
            'old_password'  => 'Mật khẩu cũ',
            'new_password'  => 'Mật khẩu mới',
            'confirm_password' => 'Xác nhận mật khẫu mới',
            'enter_old_password' => 'Nhập mật khẩu cũ',
            'enter_new_password' => 'Nhập mật khẩu mới',
            'enter_confirm_password' => 'Nhập xác nhận mật khẩu mới',
            'save'  => 'Lưu'
        ],
    ],
    'widgets' => [
        'slider'    => [
            'read_more' => 'Xem thêm'
        ]
    ],
    'page'  => [
        'sidebar'    => [
            'all_courses' => 'Các khóa học',
            'tourism'     => 'Du lịch',
            'business'    => 'Kinh doanh',
            'accounting'  => 'Kế toán',
            'finance'     => 'Tài chính',
            'hospitality' => 'Dịch vụ',
            'marketing'   => 'Marketing',
            'tag_cloud'   => 'Tag Cloud'
        ],
        'free_courses' => [
            'free_courses'=> 'Đăng ký miễn phí',
            'courses'     => 'Khóa học',
            'name'        => 'Tên',
            'email'       => 'Email',
            'tourism'     => 'Du lịch',
            'business'    => 'Kinh doanh',
            'accounting'  => 'Kế toán',
            'finance'     => 'Tài chính',
            'hospitality' => 'Dịch vụ',
            'marketing'   => 'Marketing',
            'get_free_quote' => 'Nhận miễn phí trích dẫn',
        ]
    ]
];