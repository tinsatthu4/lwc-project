<?php

return [
    'master'   => [],
    'header'   => [
        'home'       => 'Home',
        'about_us'   => 'About us',
        'courses'    => 'Courses',
        'tourism'    => 'Tourism',
        'business'   => 'Business',
        'accounting' => 'Accounting',
        'finance'    => 'Finance',
        'hospitality' => 'Hospitality',
        'marketing' => 'Marketing',
        'how_to_learn' => 'How to learn',
        'register'  => 'Register',
        'login'     => 'Login',
        'hi'        => 'Hi',
        'logout'    => 'Logout'
    ],
    'footer'   => [
        'stay_tuned_with_us' => 'STAY TUNED WITH US',
        'description_stay_tuned' => 'Get our updates educational materials, new courses and more!',
        'enter_email'   => 'Enter your email address',
        'subscribe'     => 'subscribe'
    ],
    'search'   => [
        'search'    => 'search',
        'enter_keyword' => 'Enter keyword',
        'title'     => 'Search with keywords '
    ],
    'login'    => [
        'login'     => 'Login',
        'email'     => 'E-Mail Address',
        'password'  => 'Password',
        'remember_me' => 'Remember Me',
        'forgot_your_password' => 'Forgot Your Password?'
    ],
    'register' => [
        'register'  => 'Register',
        'name'      => 'Name',
        'email'     => 'E-Mail Address',
        'password'  => 'Password',
        'confirm_password' => 'Confirm Password',
    ],
    'home'  => [
        'registration' => 'Registration',
        'description_registration' => 'It’s limited seating! Hurry up',
        'days' => 'days',
        'hours' => 'hours',
        'minutes' => 'minutes',
        'seconds' => 'seconds',
        'sign_up_now' => 'Sign up now',
        'description_courses' => 'Get Free Courses',
        'name' => 'Your Name',
        'email' => 'Email',
        'get_free_quote' => 'Get Free Quote'
    ],
    'member' => [
        'change_password' => [
            'old_password'  => 'Old password',
            'new_password'  => 'New Password',
            'confirm_password' => 'Confirm password',
            'enter_old_password' => 'Enter old password',
            'enter_new_password' => 'Enter new password',
            'enter_confirm_password' => 'Enter confirm new password',
            'save'  => 'Save'
        ],
    ],
    'widgets' => [
        'slider'    => [
            'read_more' => 'Read more'
        ]
    ],
    'page'  => [
        'sidebar'    => [
            'all_courses' => 'All courses',
            'tourism'     => 'Tourism',
            'business'    => 'Business',
            'accounting'  => 'Accounting',
            'finance'     => 'Finance',
            'hospitality' => 'Hospitality',
            'marketing'   => 'Marketing',
            'tag_cloud'   => 'Tag Cloud'
        ],
        'free_courses' => [
            'free_courses'=> 'Free Courses',
            'courses'     => 'Courses',
            'name'        => 'Name',
            'email'       => 'Email',
            'tourism'     => 'Tourism',
            'business'    => 'Business',
            'accounting'  => 'Accounting',
            'finance'     => 'Finance',
            'hospitality' => 'Hospitality',
            'marketing'   => 'Marketing',
            'get_free_quote' => 'Get Free Quote',
        ]
    ]
];