<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

	'google_storage' => [
		'bucket' => env('GOOGLE_STORAGE_BUCKET', null)
	],

	'nganluong' => [
		'NGAN_LUONG_MERCHANT_ID' => env('NGAN_LUONG_MERCHANT_ID', null),
		'NGAN_LUONG_PASSWORD' => env('NGAN_LUONG_PASSWORD', null),
		'NGAN_LUONG_URL_API' => env('NGAN_LUONG_URL_API', null),
		'NGAN_LUONG_RECEIVER_EMAIL' => env('NGAN_LUONG_RECEIVER_EMAIL', null),
	],
];
