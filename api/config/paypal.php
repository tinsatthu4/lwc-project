<?php
return [
	'mode' => env('PAYPAL_MODE', 'sandbox'), //sandbox or live
	'api_user' => env('PAYPAL_API_USER', ''),
	'api_pass' => env('PAYPAL_API_PASS', ''),
	'api_signature' => env('PAYPAL_API_SIGNATURE', ''),
	'currency_code' => env('PAYPAL_CURRENCY_CODE', 'SGD')
];